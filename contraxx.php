<?php
session_start();
$conjugacionValidar=json_decode($_SESSION['NOMBRE_BOTON_VALIDAR_CONJUGADO'],true);

$ruta_raiz = ".";
if (!$_SESSION['dependencia'])
    header("Location: $ruta_raiz/cerrar_session.php");

foreach ($_GET as $key => $valor) ${$key} = $valor;
foreach ($_POST as $key => $valor) ${$key} = $valor;


$krd = $_SESSION["krd"];
$dependencia = $_SESSION["dependencia"];
$usua_doc = $_SESSION["usua_doc"];
$codusuario = $_SESSION["codusuario"];
$tip3Nombre = $_SESSION["tip3Nombre"];
$tip3desc = $_SESSION["tip3desc"];
$tip3img = $_SESSION["tip3img"];

include_once "$ruta_raiz/include/db/ConnectionHandler.php";
$db = new ConnectionHandler($ruta_raiz);

$numeroa =
$numero =
$numeros =
$numerot =
$numerop =
$numeroh = 0;

$isql = "select  
                 a.*
                 ,b.depe_nomb 
            from 
                 usuario a
                 ,dependencia b 
             where 
                a.depe_codi=b.depe_codi
                and a.USUA_CODI = $codusuario 
                and b.DEPE_CODI = $dependencia";
$rs = $db->query($isql);

$dependencianomb = $rs->fields["DEPE_NOMB"];
$usua_login = $rs->fields["USUA_LOGIN"];

?>
<html>
<head>
    <script language="JavaScript" type="text/JavaScript">
        function MM_findObj(n, d) {
            var p, i, x;
            if (!d) d = document;
            if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
                d = parent.frames[n.substring(p + 1)].document;
                n = n.substring(0, p);
            }
            if (!(x = d[n]) && d.all) x = d.all[n];
            for (i = 0; !x && i < d.forms.length; i++) x = d.forms[i][n];
            for (i = 0; !x && d.layers && i < d.layers.length; i++) x = MM_findObj(n, d.layers[i].document);
            if (!x && d.getElementById) x = d.getElementById(n);
            return x;
        }

        function MM_validateForm() {

            var i, p, q, nm, test, num, min, max, errors = '', args = MM_validateForm.arguments;
            if($('#usua_email').val()=='' || $('#usua_email').val()==null ||
                $('#usua_email').val()=='null' || $('#usua_email').val()=='NULL'
            || $('#usua_email').val()==0){

                errors='No tiene un email configurado'
                alertModal('Alerta!', 'No tiene un email configurado', 'warning', 5000)
                document.MM_returnValue = (errors == '');
                return false;
            }

            if($('#is_valid_email').val()=='' || $('#is_valid_email').val()==null ||
                $('#is_valid_email').val()=='null' || $('#is_valid_email').val()=='NULL'
                || $('#is_valid_email').val()==0){

                errors='El email actual, no tiene un formato válido'
                alertModal('Alerta!', errors, 'warning', 5000)
                document.MM_returnValue = (errors == '');
                return false;
            }



            for (i = 0; i < (args.length - 2); i += 3) {
                test = args[i + 2];
                val = MM_findObj(args[i]);
                if (val) {
                    nm = val.name;
                    if ((val = val.value) != "") {
                        if (test.indexOf('isEmail') != -1) {
                            p = val.indexOf('@');
                            if (p < 1 || p == (val.length - 1)) errors += '- ' + nm + ' must contain an e-mail address.\n';
                        } else if (test != 'R') {
                            num = parseFloat(val);
                            if (isNaN(val)) errors += '- ' + nm + ' must contain a number.\n';
                            if (test.indexOf('inRange') != -1) {
                                p = test.indexOf(':');
                                min = test.substring(8, p);
                                max = test.substring(p + 1);
                                if (num < min || max < num) errors += '- ' + nm + ' must contain a number between ' + min + ' and ' + max + '.\n';
                            }
                        }
                    } else if (test.charAt(0) == 'R') errors += '- ' + nm + ' es requerido.\n';
                }
            }
            if (errors) alert('Asegurese de entrar el password Correcto, No puede ser Vacio:\n');
            document.MM_returnValue = (errors == '');
        }
    </script>
    <title>Cambio de Contrase&ntilde;as</title>

    <link href="./app/resources/global/plugins/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="./app/resources/global/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet"
          type="text/css"/>

    <link href="./app/resources/global/plugins/font-awesome/css/fonts.googleapis.css" rel="stylesheet"
          type="text/css"/>
    <link href="./app/resources/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="./app/resources/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="./app/resources/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet"
          type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN THEME GLOBAL STYLES
    el siguiente archivo css, les quita la propiedad de borderradios a los input de form control, por eso lo comente.
    <link href="./app/resources/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    -->
    <link href="./app/resources/global/css/components-rounded.min.css" rel="stylesheet" id="style_components"
          type="text/css"/>
    <link href="./app/resources/global/css/plugins.min.css" rel="stylesheet" type="text/css"/>
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="./app/resources/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css"/>
    <link href="./app/resources/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css"/>


    <script src="./app/resources/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="./app/resources/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="./app/resources/global/plugins/bootstrap-table/bootstrap-table.js" type="text/javascript"></script>
    <link href="./app/resources/global/css/select2.min.css" rel="stylesheet"/>
    <link href="./app/resources/global/css/select2-bootstrap.min.css" rel="stylesheet"/>
    <link href="./app/resources/global/plugins/sweetalert2-9.10.2/package/dist/sweetalert2.min.css" rel="stylesheet"
          type="text/css"/>

    <link href="./app/resources/global/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css"/>


    <link href="./app/resources/global/plugins/progressladabtn/ladda-themeless.min.css" rel="stylesheet"
          type="text/css"/>

    <link href="./app/resources/global/plugins/x-editable/bootstrap-editable.css"
          rel="stylesheet"/>
    <link href="./app/resources/global/plugins/typeahead/typeahead.js-bootstrap.css"
          rel="stylesheet" type="text/css"/>

    <link href="./app/resources/global/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css"/>


    <script src="./app/resources/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="./app/resources/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="./app/resources/pages/scripts/toastr.min.js" type="text/javascript"></script>
    <script src="./app/resources/pages/scripts/ui-toastr.min.js" type="text/javascript"></script>
    <script src="./app/resources/apps/scripts/scriptconfiguraciones.js?v=<?= date('YmdHis') ?>"></script>
    <script src="./app/resources/apps/scripts/funciones_globales.js?v=<?= date('YmdHis') ?>"
            type="text/javascript"></script>

    <script type="text/javascript">
        var base_url = '<?php echo $_SESSION['base_url']; ?>';
    </script>
</head>

<body>

<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Contraseñas
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
            <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-3">
                <ul class="nav nav-tabs tabs-left">
                    <li class="<?= !isset($_GET['ventanaselected']) ? 'active' : '' ?>">
                        <a href="#passw_aplicativo" data-toggle="tab" aria-expanded="true">Contraseña Aplicativo</a>
                    </li>
                    <?php
                    if ($_SESSION['FUNCTION_VALIDAR_RADICADO'] == 'SI' &&
                        $_SESSION['TIPOS_RADICADO_VALIDAR'] != '' && json_decode($_SESSION['TIPOS_RADICADO_VALIDAR']) != NULL
                        && $_SESSION['METODO_VALIDACION'] == 'Clave única') { ?>
                        <li class="<?= isset($_GET['ventanaselected']) && $_GET['ventanaselected'] == 'passw_validacion' ? 'active' : '' ?>">
                            <a href="#passw_validacion" data-toggle="tab" aria-expanded="false">Contraseña
                                <?= $conjugacionValidar[10] ?> </a>
                        </li>
                    <?php } ?>
                    <li class="">
                        <a href="#ayuda" data-toggle="tab" aria-expanded="false">Ayuda</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-9 col-sm-9 col-xs-9">
                <div class="tab-content">
                    <div class="tab-pane <?= !isset($_GET['ventanaselected']) ? 'active in' : '' ?>"
                         id="passw_aplicativo">
                        <form action='usuarionuevo.php' method="post"
                              onSubmit="MM_validateForm('contradrd','','R','contraver','','R');return document.MM_returnValue">
                            <input type='hidden' name='<?= session_name() ?>' value='<?= session_id() ?>'>
                            <CENTER>
                                <table border=0 cellspacing="0">
                                    <br/><br/><br/>
                                    <tr>
                                        <td class=listado2 colspan="2">
                                            <center>Introduzca la nueva contrase&ntilde;a <b><?= $usua_login ?></b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class=listado2>
                                            <center>Contrase&ntilde;a
                                        </td>
                                        <td class=listado2><input type="password" name="contradrd"
                                                                  class="tex_area form-control"></td>
                                    </tr>
                                    <tr>
                                        <td class=listado2>
                                            <center>Re-escriba<br>la contrase&ntilde;a
                                        </td>
                                        <td class=listado2><input type="password" name="contraver"
                                                                  class="tex_area form-control"></td>

                                        <input type="hidden" value="<?= $_SESSION['usua_email']?>"
                                        id="usua_email">

                                        <input type="hidden"
                                               value="<?= filter_var($_SESSION['usua_email'],FILTER_VALIDATE_EMAIL) ?>"
                                        id="is_valid_email">
                                    </tr>
                                    <tr>
                                        <td class=listado2 colspan="2">
                                            <center>
                                                <input type="submit" value="Aceptar" class="botones btn btn-success">
                                        </td>
                                    </tr>
                                </table>
                            </CENTER>
                        </form>
                    </div>

                    <?php
                    if ($_SESSION['FUNCTION_VALIDAR_RADICADO'] == 'SI' &&
                        $_SESSION['TIPOS_RADICADO_VALIDAR'] != '' && json_decode($_SESSION['TIPOS_RADICADO_VALIDAR']) != NULL
                        && $_SESSION['METODO_VALIDACION'] == 'Clave única') { ?>
                        <div class="tab-pane <?= isset($_GET['ventanaselected']) && $_GET['ventanaselected'] == 'passw_validacion' ? 'active in' : 'fade' ?> fade"
                             id="passw_validacion">
                            <!--<form action='usuarionuevo.php' method="post" onSubmit="MM_validateForm('contradrd','','R','contraver','','R');return document.MM_returnValue">   el form dispara que se guarde la contraseña pidieron que no se guarde en el navegador -->
                            <input type='hidden' name='<?= session_name() ?>' value='<?= session_id() ?>'>
                            <CENTER>
                                <table border=0 cellspacing="0">

                                    <br/><br/><br/>
                                    <tr>
                                        <td class=listado2 colspan="2">
                                            <strong>Introduzca contrase&ntilde;a para <?= $conjugacionValidar[10] ?></strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class=listado2>
                                            <center>Contrase&ntilde;a
                                        </td>
                                        <td class=listado2>
                                            <input type="password" id="passvalidacion"
                                                   placeholder="Nueva Contraseña" autocomplete="off"
                                                   class="tex_area form-control"></td>
                                        <td class=listado2>
                                            <center><h5>&nbsp;Logintud mínima de 8 caracteres, letras, números,
                                                    mayúsculas, mínimo un caracter especial</h5>
                                        </td>

                                    </tr>
                                    <tr id="trRepeatPass">

                                        <td class=listado2>
                                            <center>Re-escriba<br>la contrase&ntilde;a
                                        </td>
                                        <td class=listado2>
                                            <input type="password" autocomplete="off" id="repeatvalidacion"
                                                   placeholder="Repita Contraseña" class="tex_area form-control"></td>
                                    </tr>
                                    <tr>
                                        <td class=listado2 colspan="2">
                                            <center>
                                                <button onclick="savePassvalid(event)" class="botones btn btn-success">
                                                    Guardar
                                                </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><a href="#" onclick="volver(event);"
                                               class="botones btn btn-default">Volver</a></td>
                                    </tr>
                                </table>
                            </CENTER>
                            <!--</form>-->
                        </div>

                    <?php } ?>
                    <div class="tab-pane fade" id="ayuda">
                        <p> Ayuda</p>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


</body>

<script type="text/javascript">


    function volver(e) {
        e.preventDefault();
        history.back()
    }

    function savePassvalid(e) {
        e.preventDefault();
        document.getElementById('passvalidacion').setAttribute('autocomplete', 'off');
        document.getElementById('repeatvalidacion').setAttribute('autocomplete', 'off');

        if ($('#passvalidacion').val() == '' || $('#repeatvalidacion').val() == '') {
            alertModal('Alerta!', 'Debe ingresar todos los campos', 'warning', 5000)
            return false;
        }

        var passvalidacion = $('#passvalidacion').val()
        var repeatvalidacion = $('#repeatvalidacion').val()

        if (passvalidacion.length < 8 || repeatvalidacion.length < 8) {
            alertModal('Alerta!', 'La contraseña debe contener al menos 8 caracteres', 'warning', 5000)
            return false;
        }

        if ($('#passvalidacion').val() !== $('#repeatvalidacion').val()) {
            alertModal('Alerta!', 'Las contraseñas no coinciden', 'warning', 5000)
            return false;
        }

        var patt = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{8,}$/
        var res = patt.test($('#passvalidacion').val());

        if (res == false) {
            alertModal('Alerta!', 'La contraseña, debe contener una  logintud mínima de 8 caracteres, letras, números, mayúsculas, mínimo un caracter especial', 'warning', 5000)
            return false;
        }
        var data = {
            passvalidacion: $('#passvalidacion').val(),
            repeatvalidacion: $('#repeatvalidacion').val(),
            function_call: "actualizaClaveModValidacion"
        }

        if ($('#codigoemail').val() != undefined) {

            if ($('#codigoemail').val() == '') {
                alertModal('Alerta!', 'Ingrese el código enviado a su email', 'warning', 5000)
                return false;
            }
            data.codigoemail = $('#codigoemail').val();
        }


        $.ajax({
            url: base_url + "app/configuraciones/generalconfig.php",
            method: "POST",
            data: data,
            dataType: 'json',
            async: false,
            success: function (response) {

                if (response.error != undefined) {
                    if (response.error == 'NO TIENE PASSWORD' || response.error == 'TIENE PASSWORD Y MODIFICA') {
                        alertModal('Alerta!', 'Fué enviado un código de verificación ' +
                            'a su correo para poder modificar la contraseña', 'warning', 5000);
                        var html = '<tr><td><br></td><td></td></tr>  <tr id="trcodigoemail">\n' +
                            '\n' +
                            '                                        <td class=listado2><center>Ingrese el código enviado a su email</td>\n' +
                            '                                        <td class=listado2>\n' +
                            '                                            <input type="text" id="codigoemail"\n' +
                            '                                                    placeholder="Código enviado a su email" class="tex_area form-control"></td>\n' +
                            '                                    </tr>';
                        $('#trRepeatPass').after(html)

                    } else {
                        alertModal('Alerta!', response.error, 'warning', 5000)
                        return false;
                    }

                } else {
                    alertModal('Guardado!', 'La contraseña ha sido guardada con éxito', 'success', 5000)
                    return false;
                }

            },
            error: function (response) {

            }
        });
    }

    $(document).ready(function () {

    })

</script>
</html>
