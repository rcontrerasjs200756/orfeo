<?php
ini_set('display_errors', 0);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(7);
$ruta_raiz = "../";

require_once "nusoap/nusoap.php";


//Asignacion del namespace  
$ns = "webServices/nusoap";

$usuaLoginMail="soporteorfeo@idartes.gov.co";

//Creacion del objeto soap_server
$server = new soap_server();

$server->configureWSDL('Sistema de Gestion Documental Orfeo',$ns);

/*******************************************************************************
Registro servicios a ofrecer, el metodo register tiene los sigientes parametros
********************************************************************************/

$server->register('crearAnexo',  						//nombre del servicio                 
    array(
		'file' => 'xsd:base64binary',					//archivo en base 64
		'fileName' => 'xsd:string',
		'descripcion'=>'xsd:string',					//descripcion del anexo
		'radicado' => 'xsd:string'					//numero de radicado
     ),											//fin parametros del servicio        	
    array('return' => 'xsd:string'),   					//retorno del servicio
    $ns                     							//Elemento namespace para el metod
);

$server->register('radicarDocumento',
	array(
		'file' => 'xsd:base64binary',					//archivo en base 64
		'fileName' => 'xsd:string',
		'destinatario'=>'tns:Vector',
		'asu'=>'xsd:string',
		'med'=>'xsd:string',
		'ane'=>'xsd:string',
		'coddepe'=>'xsd:string',
		'tpRadicado'=>'xsd:string',
		'cuentai'=>'xsd:string',
		'radi_usua_actu'=>'xsd:string',
		'tip_rem'=>'xsd:string',
		'tdoc'=>'xsd:string',
		'tip_doc'=>'xsd:string',
		'carp_codi'=>'xsd:string',
		'carp_per'=>'xsd:string'
	),
	array(
		'return' => 'xsd:string'
	),
	$ns,
	$ns."#radicarDocumento",
	'rpc',
	'encoded',
	'Radicacion de un documento en Orfeo'
);

/**************************************************
 Registro tipos complejos y/o estructuras de datos
***************************************************/

//Adicionando un tipo complejo MATRIZ
 
$server->wsdl->addComplexType(
    'Matriz',
    'complexType',
    'array',
    '',
    'SOAP-ENC:Array',
	array(),
    array(
    array('ref'=>'SOAP-ENC:arrayType','wsdl:arrayType'=>'tns:Vector[]')
    ),
    'tns:Vector'
);

//Adicionando un tipo complejo VECTOR

$server->wsdl->addComplexType(
    'Vector',
    'complexType',
    'array',
    '',
    'SOAP-ENC:Array',
	array(),
    array(
    array('ref'=>'SOAP-ENC:arrayType','wsdl:arrayType'=>'xsd:string[]')
    ),
    'xsd:string'
);

/**********
 Funciones
***********/

/**
 * UploadFile es una funcion que permite almacenar cualquier tipo de archivo en el lado del servidor
 * @param $bytes 
 * @param $fileName es el nombre del archivo con que queremos almacenar en el servidor
 * @author 
 * @return Retorna un String indicando si la operacion fue satisfactoria o no
 */
function UploadFile($path, $bytes, $filename) {
	$ruta_raiz = "../";

	$path = $ruta_raiz . "bodega" . $path;

	if (!file_exists($path))
		mkdir($path, 0777, true);

	if (!$fp = fopen("$path$filename", "w"))
		die("fallo");

	$bytes = base64_decode($bytes);
	$salida = true;

	if (is_array($bytes)) {
		foreach($bytes as $k => $v) {
			$salida = ($salida && fwrite($fp, $bytes));
		}
	} else
		$salida = fwrite($fp,$bytes);

	fclose($fp);

	if ($salida)
		return "exito";
	else
		return "error";	
}

/**
 * funcion encargada regenerar un archivo enviado en base64
 * @param string $ruta ruta donde se almacenara el archivo 
 * @param base64 $archivo archivo codificado en base64
 * @param string $nombre nombre del archivo
 * @return boolean retorna si se pudo decodificar el archivo
 */
function subirArchivo($ruta, $archivo, $nombre) {
	$fp = @fopen("{$ruta}{$nombre}", "w");
	$bytes = base64_decode($archivo);
	$salida = true;

	if (is_array($bytes)) {
		foreach($bytes as $k => $v){
			$salida = ($salida && fwrite($fp, $bytes));
		}
	} else {
		$salida = fwrite($fp,$bytes);
	}

	fclose($fp);

	return $salida;		
}

/**
 * funcion que crea un Anexo
 * @param string $radiNume numero del radicado al cual se adiciona el anexo
 * @param base64 $file archivo codificado en base64
 * @param string $filename nombre original del anexo, con extension
 * @param string $descripcion descripcion del anexo
 * @return string mensaje de error en caso de fallo o el numero del anexo en caso de exito
 */
function crearAnexo($file, $fileName, $descripcion, $radicado) {
	$ruta_raiz = "../";

	include($ruta_raiz . "/include/tx/Tx.php");
	include($ruta_raiz . "/include/tx/Radicacion.php") ;
	include($ruta_raiz . "/class_control/Municipio.php");

	$db = new ConnectionHandler($ruta_raiz);

	$usuario = getInfoUsuario();

	$error = ($usuario['error'] != "OK") ? true : false;

	$ruta = $ruta_raiz . "bodega/" . substr($radicado, 0, 4) . "/" . substr($radicado, 4, 3) . "/docs/";
     $_SESSION['usua_doc'] = $usuario['usua_doc'];

	$numAnexos = numeroAnexos($radicado, $db) + 1;
	$maxAnexos = maxRadicados($radicado, $db) + 1;
	$ext = explode('.' , $fileName);
	$numAnexo = ($numAnexos > $maxAnexos) ? $numAnexos:$maxAnexos;
	$nombreAnexo = $radicado.substr("00000" . $numAnexo,-5);
	$subirArchivo = subirArchivo($ruta, $file, $nombreAnexo . "." . $ext[1]);
	$tamanoAnexo = $subirArchivo / 1024;							//tamano en kilobytes
	$error = ($error && !$subirArchivo) ? true : false;
	$fechaAnexado = $db->conn->OffsetDate(0,$db->conn->sysTimeStamp);
	$tipoAnexo = tipoAnexo($ext[1], $db);

	if (!$error) {
		$tipoAnexo = ($tipoAnexo) ? $tipoAnexo : "NULL";

		$consulta = "INSERT INTO ANEXOS (ANEX_CODIGO,ANEX_RADI_NUME,ANEX_TIPO,ANEX_TAMANO,ANEX_SOLO_LECT,ANEX_CREADOR,
				ANEX_DESC,ANEX_NUMERO,ANEX_NOMB_ARCHIVO,ANEX_ESTADO,SGD_REM_DESTINO,ANEX_FECH_ANEX, ANEX_BORRADO) 
				VALUES ('$nombreAnexo', $radicado, $tipoAnexo, $tamanoAnexo, 'n','" . $usuario['usua_login'] .
				"','$descripcion', $numAnexo, '" . $nombreAnexo . "." . $ext[1] . "',0,1, current_date, 'N')";

		$rs = $db->query($consulta);

		if (!$rs)
			return "No se ha podido actualizar el anexo -- $consulta --";

		$consultaV = "SELECT ANEX_CODIGO FROM ANEXOS WHERE ANEX_CODIGO = '$nombreAnexo'";
		$rs = $db->query($consultaV);
		$cod = $rs->fields['ANEX_CODIGO'];
	}

	return $cod ? 'Anexo creado ' . $nombreAnexo : 'Error al anexar ' . $nombreAnexo;
}

/**
 * funcion que calculcula el numero de anexos que tiene un radicado
 *
 * @param int  $radiNume radicado al cual se realiza se adiciona el anexo
 * @param ConectionHandler $db
 * @return int numero de anexos del radicado
 */
function numeroAnexos($radiNume,$db){
	$consulta="SELECT COUNT(1) AS NUM_ANEX FROM ANEXOS WHERE ANEX_RADI_NUME={$radiNume}";
	$salida=0;	
	$rs=& $db->query($consulta);
		if($rs && !$rs->EOF)
			$salida=$rs->fields['NUM_ANEX'];
		return  $salida;	
}

/**
 * funcioncion que rescata el maxido del anexo de los radicados 
 *
 * @param int $radiNume numero del radicado
 * @param ConnectionHandler $db conexion con la db
 * @return int maximo
 */
function maxRadicados($radiNume,$db){
	$consulta=$consulta="SELECT max(ANEX_NUMERO) AS NUM_ANEX FROM ANEXOS WHERE ANEX_RADI_NUME={$radiNume}";
		$rs=& $db->query($consulta);
		if($rs && !$rs->EOF)
			$salida=$rs->fields['NUM_ANEX'];
		return  $salida;	
}

/**
 * funcion que consulta el tipo de anexo
 * @param $extension extencion del archivo
 * @param ConnectionHandler $db conexion con la DB
 * @return int
 */
function tipoAnexo($extension, $db) {
	$consulta = "SELECT ANEX_TIPO_CODI FROM ANEXOS_TIPO WHERE ANEX_TIPO_EXT='" . strtolower($extension) . "'";
	$salida = null;
	$rs = $db->query($consulta);

	if ($rs && !$rs->EOF)
		$salida = $rs->fields['ANEX_TIPO_CODI'];

	return $salida;		
}

/**
 * Esta funcion permite radicar un documento en Orfeo
 * @param $file, Archivo asociado al radicado codificado en Base64 
 * @param $filename, Nombre del archivo que se radica
 * @param $destinos, arreglo de destinatarios destinatarios,predio,esp
 * @param $asu, Asunto del radicado
 * @param $med, Medio de radicacion
 * @param $ane, descripcion de anexos
 * @param $coddepe, codigo de la dependencia
 * @param $tpRadicado, tipo de radicado
 * @param $cuentai, cuenta interna del radicado
 * @param $radi_usua_actu, el correo electronico del usuario
 * @param $tip_rem
 * @param $tdoc
 * @param $tip_doc
 * @param $carp_codi
 * @param $carp_per 
 * @author Donaldo Jinete Forero
 * @return El numero del radicado o un mensaje de error en caso de fallo
 */

function radicarDocumento($file, $filename, $destinatario, $asu, $med, $ane, $coddepe, $tpRadicado,
			  $cuentai, $radi_usua_actu, $tip_rem, $tdoc, $tip_doc, $carp_codi, $carp_per)
{
	$ruta_raiz = "../";

	$persona = array(
		'tipo_documento'=>$destinatario[0],
		'documento'=>$destinatario[1],
		'tipo_emp'=>$destinatario[2],
		'nombre'=>$destinatario[3],
		'prim_apel'=>$destinatario[4],
		'seg_apel'=>$destinatario[5],
		'telefono'=>$destinatario[6],
		'direccion'=>$destinatario[7],
		'mail'=>$destinatario[8],
		'otro'=>$destinatario[9],
		'idcont'=>$destinatario[10],
		'idpais'=>$destinatario[11],
		'codep'=>$destinatario[12],
		'muni'=>$destinatario[13]
	);

	$radi_usuaRad = getInfoUsuario();
	$radi_dep_radi = trim($radi_usuaRad['usua_depe']);
	$radi_usua_actu = trim($radi_usuaRad['usua_codi']);

	include($ruta_raiz . "/include/tx/Tx.php");
	include($ruta_raiz . "/include/tx/Radicacion.php") ;
	include($ruta_raiz . "/class_control/Municipio.php");

	$db = new ConnectionHandler($ruta_raiz);
	$hist = new Historico($db) ;
	$tmp_mun = new Municipio($db);

        $_SESSION['dependencia'] = "0";
        $_SESSION['usua_doc'] = "0";
        $_SESSION['nivelus'] = "0";
        $_SESSION['digitosDependencia'] = "3";
        $_SESSION['codusuario'] = "0";
	$_SESSION['nivelus'] = "0";

	$rad = new Radicacion($db) ;
	
	$tmp_mun->municipio_codigo($persona["codep"], $persona["muni"]);
	$rad->radiTipoDeri = $tpRadicado;
	$rad->radiCuentai = "'".trim($cuentai)."'";
	$rad->eespCodi = "0";
	$rad->mrecCodi =  $med;
	$rad->radiFechOfic = "";
	$rad->radiNumeDeri = 'null';
	$rad->radiPais =  $tmp_mun->get_pais_codi() ;
	$rad->descAnex = $ane ;
	$rad->raAsun = $asu ;
	$rad->radiDepeActu = $coddepe ; //'200'
	$rad->radiDepeRadi = $radi_dep_radi ;
	$rad->radiUsuaActu = 1; // $radi_usua_actu;
	$rad->trteCodi = $tip_rem ;
	$rad->tdocCodi = $tdoc ;
	$rad->tdidCodi = "0";
	$rad->carpCodi = $carp_codi ;
	$rad->carPer = $carp_per ;
	$rad->radiPath = 'null';
	$rad->sgd_apli_codi = "0";

	$codTx = 2;
	$flag = 1;
	$rad->usuaCodi = $radi_usua_actu;
	$rad->dependencia = "200";

	$noRad = $rad->newRadicado($tpRadicado, $radi_dep_radi);

	$nurad = trim($noRad) ;
	$sql_ret = $rad->updateRadicado($nurad,"/".date("Y")."/".$coddepe."/".$noRad.".pdf");
	
	if ($noRad == "-1")
		return "Error no genero un Numero de Secuencia o Inserto el radicado";		

	$radicadosSel[0] = $noRad;
	$hist->insertarHistorico($radicadosSel,  $coddepe , $radi_usua_actu, $coddepe, $radi_usua_actu, " ", $codTx);
	
	$conexion = $db;

	/** Preparacion de variables direcciones **/
	
	$tipo_emp_us1 = trim($persona['tipo_emp']);
	
	$muni_us1 = trim($persona['muni']);
	$codep_us1 = trim($persona['codep']);
	
	$grbNombresUs1 = trim($persona['nombre']) . " " . trim($persona['prim_apel']) . " ". trim($persona['seg_apel']);
	
	$tipo_documento_us1 = trim($persona['tipo_documento']);
	$documento_us1 = trim($persona['documento']);
	
	$direccion_us1 = trim($persona['direccion']);
	$telefono_us1 = trim($persona['telefono']);
	$mail_us1 = trim($persona['mail']);
	$otro_us1 = trim($persona['otro']);
	
	//** DIRECCIONES **
	
	if (!$muni_us1) $muni_us1 = NULL;
	
	// Creamos las valores del codigo del dpto y mcpio desglozando el valor del <SELECT> correspondiente.
	if (!is_null($muni_us1)) {
		$tmp_mun = new Municipio($conexion);
		$tmp_mun->municipio_codigo($codep_us1,$muni_us1);
		$tmp_idcont = $tmp_mun->get_cont_codi();
		$tmp_idpais = $tmp_mun->get_pais_codi();
		$muni_tmp1 = explode("-", $muni_us1);

		switch (count($muni_tmp1))
		{	
		case 4:
			{
				$idcont1 = $muni_tmp1[0];
				$idpais1 = $muni_tmp1[1];
				$dpto_tmp1 = $muni_tmp1[2];
				$muni_tmp1 = $muni_tmp1[3];
			}
			break;
		case 3:
			{
				$idcont1 = $tmp_idcont;
				$idpais1 = $muni_tmp1[0];
				$dpto_tmp1 = $muni_tmp1[1];
				$muni_tmp1 = $muni_tmp1[2];
			}
			break;
		case 2:
			{
				$idcont1 = $tmp_idcont;
				$idpais1 = $tmp_idpais;
				$dpto_tmp1 = $muni_tmp1[0];
				$muni_tmp1 = $muni_tmp1[1];
			}

			break;
		}

		unset($tmp_mun);
		unset($tmp_idcont);
		unset($tmp_idpais);
	}

	/** Ciudadano **/
	$isql = "select sgd_ciu_codigo from SGD_CIU_CIUDADANO where sgd_ciu_cedula='$documento_us1'";
	$rs = $conexion->query($isql);
	if ($rs && !$rs->EOF)
		$documento_us1 = ($rs->fields["SGD_CIU_CODIGO"]);
	else
	{
		$nextval = $conexion->nextId("sec_ciu_ciudadano");
		
		if ($nextval == -1)
			return "No se encontro secuencia ciudadano";

		$nombre = $persona['nombre'];
		$pa = $persona['prim_apel'];
		$sa = $persona['seg_apel'];

		$isql = "insert into SGD_CIU_CIUDADANO (sgd_ciu_codigo, tdid_codi, sgd_ciu_nombre, sgd_ciu_apell1, sgd_ciu_apell2, 
				MUNI_CODI,DPTO_CODI,ID_PAIS,ID_CONT,sgd_ciu_direccion,sgd_ciu_telefono,sgd_ciu_email,sgd_ciu_cedula)
			 values ($nextval, '$tipo_documento_us1', '$nombre', '$pa', '$sa',
				$muni_tmp1, $dpto_tmp1, $idpais1, $idcont1, '$direccion_us1', '$telefono_us1','$mail_us1','$documento_us1')";

		$rsg = $conexion->query($isql); 

		if (!$rsg)
			return "No se ha podido actualizar la informacion de SGD_CIU_CIUDADANO -- $isql --";
		else
			$documento_us1 = $nextval;
	}

	/* Direcciones */
	
	$newId = false;

	$nextval = $conexion->nextId("sec_dir_direcciones");
	if ($nextval == -1)
		return "No se encontro la secuencia sec_dir_direcciones ";
	
	global $ADODB_COUNTRECS;
	if ($documento_us1 != '')
	{
		$sgd_ciu_codigo=0;
		$sgd_oem_codigo=0;
		$sgd_esp_codigo=0;
		$sgd_fun_codigo=0;
  		if($tipo_emp_us1==0)
  		{	
  			$sgd_ciu_codigo=$documento_us1;
			$sgdTrd = "1";
		}
		if($tipo_emp_us1==1)
		{	
			$sgd_esp_codigo=$documento_us1;
			$sgdTrd = "3";
		}
		if($tipo_emp_us1==2)
		{	
			$sgd_oem_codigo=$documento_us1;
			$sgdTrd = "2";
		}
		if($tipo_emp_us1==6)
		{	
			$sgd_fun_codigo=$documento_us1;
			$sgdTrd = "4";
		}

		$ADODB_COUNTRECS = true;

		$record = array();
		$record['SGD_TRD_CODIGO'] = $sgdTrd;
		$record['SGD_DIR_NOMREMDES'] = $grbNombresUs1;
		$record['SGD_DIR_DOC'] = $documento_us1;
		$record['MUNI_CODI'] = $muni_tmp1;
		$record['DPTO_CODI'] = $dpto_tmp1;
		$record['ID_PAIS'] = $idpais1;
		$record['ID_CONT'] = $idcont1;
		$record['SGD_DOC_FUN'] = $sgd_fun_codigo;
		$record['SGD_OEM_CODIGO'] = $sgd_oem_codigo;
		$record['SGD_CIU_CODIGO'] = $sgd_ciu_codigo;
		$record['SGD_OEM_CODIGO'] = $sgd_oem_codigo;
		$record['SGD_ESP_CODI'] = $sgd_esp_codigo;
		$record['RADI_NUME_RADI'] = $nurad;
		$record['SGD_SEC_CODIGO'] = 0;
		$record['SGD_DIR_DIRECCION'] = $direccion_us1;
		$record['SGD_DIR_TELEFONO'] = trim($telefono_us1);
		$record['SGD_DIR_MAIL'] = $mail_us1;
		$record['SGD_DIR_TIPO'] = 1;//1
		$record['SGD_DIR_CODIGO'] = $nextval;
		$record['SGD_DIR_NOMBRE'] = $otro_us1;

		$insertSQL = $conexion->conn->Replace("SGD_DIR_DRECCIONES", $record, array('RADI_NUME_RADI','SGD_DIR_TIPO'), $autoquote = true);
        
		switch ($insertSQL)
		{
			case 1: {	//Insercion Exitosa
				$dir_codigo_new = $nextval;
				$newId=true;
			} break;
			case 2: {	//Update Exitoso
				$newId = false;
			} break;
			case 0: {	//Error Transaccion.
				return  "No se ha podido actualizar la informacion de SGD_DIR_DRECCIONES UNO -- $insertSQL --";
			} break;
		}
		unset($record);
		$ADODB_COUNTRECS = false;
	}
	// ** Fin direccion **

	$retval = $noRad;

	if ($filename != '') {
		$ext = explode('.' , $filename);
		$retval = cambiarImagenRad($retval, $ext[1], $file);

		if ($retval != "OK")
			return "ERROR al publicar el documento";
	}

	return $noRad;
}  

function Cambiarimagenrad($numRadicado, $ext, $file) {
	$ruta_raiz = "../";

	$db = new ConnectionHandler($ruta_raiz);
	$sql = "SELECT RADI_PATH FROM RADICADO WHERE RADI_NUME_RADI='{$numRadicado}'";
	$rs = $db->query($sql);

	if (!$rs->EOF) {

		$year = substr($numRadicado, 0, 4);
		$depe = substr($numRadicado, 4, 3);
		$path = "/{$year}/{$depe}/docs/";

		$update = "UPDATE RADICADO SET RADI_PATH='{$path}{$numRadicado}.{$ext}' where RADI_NUME_RADI='{$numRadicado}'";
		if (UploadFile($path, $file, $numRadicado . '.' . $ext) == "exito") {
			$db->query($update);
			return "OK";
		} else
			return "ERROR no se puede copiar el archivo";
	} else
		return "ERROR El radicado no existe";
}

function getInfoUsuario() {
	$ruta_raiz = "../";

	include_once($ruta_raiz."include/db/ConnectionHandler.php");

	$db = new ConnectionHandler($ruta_raiz);

        $sql = "SELECT USUA_LOGIN,USUA_DOC,DEPE_CODI,CODI_NIVEL,USUA_CODI,USUA_NOMB FROM USUARIO WHERE usua_login='MARIO.ORTIZ'";

	$rs = $db->conn->Execute($sql);
	if ($rs && !$rs->EOF) {
		$salida['usua_login']=($rs->fields["USUA_LOGIN"]);
                $salida['usua_doc'] =($rs->fields["USUA_DOC"]);
                $salida['usua_depe'] = ($rs->fields["DEPE_CODI"]);
                $salida['usua_nivel'] =($rs->fields["CODI_NIVEL"]);
                $salida['usua_codi'] =($rs->fields["USUA_CODI"]);
                $salida['usua_nomb'] =($rs->fields["USUA_NOMB"]);

		$salida['error'] = "OK";
        } else {
		$salida['error'] = "El usuario no existe o se encuentra deshabilitado";
        }
        return $salida; 
}

$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
$server->service($HTTP_RAW_POST_DATA);

//$server->service(file_get_contents("php://input"));
?>
