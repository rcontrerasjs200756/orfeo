<?php
	$sqlConcat = $db->conn->Concat("usua_doc","'-'","usua_login");
	if(!trim($busqRadicados)) $sqlDependencia = " u.depe_codi = " . $dep_sel . " ";
	switch($db->driver)
	{
	case 'mssql':
		$isql = "select 
			u.usua_nomb      		AS NOMBRE
			,u.usua_login	     	AS USUARIO
			,d.depe_nomb			AS DEPENDENCIA
			," . $sqlConcat  . " 	AS CHR_USUA_DOC
		from usuario u, dependencia d 
		where $sqlDependencia u.depe_codi = d.depe_codi " . $dependencia_busq2 . "
		order by " . $order . " " . $orderTipo;

		break;
	default:
		$isql = '
		SELECT 
		    u.usua_codi 													AS "Id"
		    ,(CASE WHEN u.usua_codi=1 THEN '."'Jefe'".'  END) 				AS "Rol"
			,u.usua_nomb      												AS "Nombre"
			,u.usua_login	 AS "Usuario"
			,CONCAT(CONCAT(d.depe_codi), d.depe_nomb)	AS "Dependencia"
			,u.usua_email AS "eMail"
			,u.usua_doc AS "Documento"
      ,U.CODI_NIVEL AS "Nivel"
            ,( CASE
          			WHEN u.usua_fech_sesion IS NULL THEN '."'Nunca ha ingresado :('".'
           			WHEN u.usua_fech_sesion IS NOT NULL THEN to_char(U.USUA_FECH_SESION, '."'YYYY-MM-DD HH24:MI'".')
         			END )   												AS "Ultimo_Ingreso"
			,(CASE WHEN u.usua_esta='."'1'".' THEN '."'Activo'".' 
				   WHEN  U.USUA_ESTA<>'."'1'".' THEN '."'Inactivo'".' END) 	AS "Estado"
            ,Count(r.radi_nume_radi)										AS "Radicados"
			,' . $sqlConcat  . ' AS "CHR_USUA_DOC"
		FROM usuario u 
		INNER JOIN dependencia d ON u.depe_codi = d.depe_codi 
        LEFT JOIN radicado r ON u.depe_codi = r.radi_depe_actu AND u.usua_codi = r.radi_usua_actu
		WHERE '.$sqlDependencia.' ' . $dependencia_busq2 . '
        GROUP BY  u.usua_codi
				, u.usua_nomb
				, u.usua_login
				, d.depe_codi
		ORDER BY "Rol", "Estado", "Radicados" Desc, "Nombre" Asc,' . $order . ' ' . $orderTipo;
	//echo $isql;
	break;
	}
?>
