<?php
// BDConnection extends MySQLConex
// By PaTcH 2007
class DBConnection{

	public $con;
	public $host;
	public $user;
	public $bd;
	public $nombresDB;

	function __destruct(){
		if(isset($this->con))$this->con->Close();
	}
	
	public function __construct($dbdriver="postgres", $host = 'localhost',$user = 'orfeobd',$pass = 'orfeobd', $bd   = 'orfeof'){
        $this->con = ADONewConnection($dbdriver); # eg 'mysql' or 'postgres'
        $this->con->debug = false;
        $this->con->Connect($host, $user, $pass, $bd);		
		if(!$this->con){			
			throw new Exception("Error de conexión:\n$host\nusuario = $user\nContrase�a = $pass");
		}				
	}
	// Activa o desactiva el debug de adodb
	public function setDebug($value){
		$this->con->debug=$value;
	}
		
	public function close(){
		if(isset($this->con)){
			if($this->con->Close()){
			}else{
				throw new Exception("Error inesperado al cerrar la conexión con el servidor");
			}
		}
	}
	
	public function getCon(){
		if(isset($this->con)){
			return $this->con;
		}else{
			throw new Exception("No ha se ha establecido conexi�n con el servidor");
			return NULL;
		}		
	}
	
	public function execQuery($query, $offset='', $limitrows=''){
		if(isset($this->con)){			
			if($limitrows!=''){ // Si trae parametro de cantidad de filas ejecuta la consulta con ese limite, el offset o punto de inicio es opcional
				if($offset==''){
					$offset=0;
				}
				$res = $this->con->SelectLimit($query, $limitrows, $offset);
			}else{ // Ejecuta select con limite
				$res = $this->con->query($query);
			}
			if($res==false){				
				throw new Exception("<b>Error al ejecutar la consulta:</b><br><br>$query<br><br>");				
				return NULL;
			}else{				
				return $res;
			}
		}else{
			throw new Exception("No ha se ha establecido conexi�n con el servidor");
			return NULL;
		}
	}
		
	public function getRow($res){
		if($res==FALSE){
			throw new Exception("No es un resultado valido");
			return NULL;
		}else{		
			$fila = $res->fields;			
			$res->moveNext();
			return $fila;
		}	
	}	
	
	public function disconnect(){
		if(isset($this->con))$this->con->Close();
	}
	
	public function num_rows($res){
		return $res->RecordCount();
	}
	
	public function insertId(){
		return $this->con->Insert_ID();
	}
}
?>
