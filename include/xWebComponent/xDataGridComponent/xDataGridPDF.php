<?php 
// xWebComponents
// xDataGrid Version 1.6
// Octubre de 2010
// Diseñado y Desarrollado por Francisco Javier Bravo V. -  franciscobravo3220@gmail.com
// Adaptado para su uso en Orfeo GPL

	session_start();	
	define('ADODB_ASSOC_CASE', 1);
	require_once ('../../pdf2/class.ezpdf.php');		
	require_once("xDataGrid.class.php");
	require_once("xDataColumn.class.php");	
	require('../../adodb/adodb.inc.php');
	require("../database/DBConnection.php");	
	require_once("../database/data.php");	
	$conex=new DBConnection($dbdriver, $server, $user, $password, $database);
	$id=$_POST['id'];
	$xDataGrid=unserialize($_SESSION["$id"]);		
	$xDataGrid->conex=$conex;
	if($xDataGrid->orientation=='v'){
		$pdf=new Cezpdf('letter','portait');	
	}else{
		$pdf=new Cezpdf('letter','landscape');
	}
	
	$pdf->selectFont('../../pdf2/fonts/Helvetica.afm');		
	$top=4;
	$bottom=3;
	$left=3;
	$right=3;
	$pdf->ezSetCmMargins($top, $bottom, $left, $right);

// NUMERO DE PAGINA	
	$pdf->ezStartPageNumbers($pdf->ez["pageWidth"]-$pdf->ez["leftMargin"],'50',12,'','',1);
// NO MOSTRAR LA OPCIÓN COPIAR AL DAR CLIC DERECHO
	//$pdf->setEncryption(); 
// DATOS INICIALES

//	PROPIEDADES DEL DOCUMENTO
	$docProperties=$xDataGrid->docProperties;	
	$pdf->code=$docProperties['headerPDF'].$docProperties['footerPDF'];			
	
// TITULO
	$options['showLines']=0;// 0=no, 1=externos, 2=todos
	$options['showHeadings']=0;
	$options['shaded']=0;
	$options['shadeCol']=array(0.93,0.93,0.93);
	$options['shadeCol2']=array(1,1,1);	
	$options['fontSize']='14';
	$options['rowGap']='2';
	$options['colGap']='2';
	$options['xpos']='center';
	$options['xorientation']='left';
	$options['cols'][0]['xPos']='center';	
	$data[0]=array("<b>".html_entity_decode($xDataGrid->title)."</b>");
		
	$pdf->ezTable($data,'','',$options);
	$pdf->ezSetDy(-10);	
	
	
// 	TEXTO INICIAL

	$options['showLines']=0;// 0=no, 1=externos, 2=todos
	$options['showHeadings']=0;
	$options['shaded']=0;
	$options['shadeCol']=array(0.93,0.93,0.93);
	$options['shadeCol2']=array(1,1,1);	
	$options['fontSize']='10';
	$options['rowGap']='2';
	$options['colGap']='2';
	$options['xpos']='center';
	$options['xorientation']='left';
	$options['cols'][0]['xPos']='center';
	$options['cols'][0]['width']=$pdf->ez["pageWidth"]-$pdf->ez["leftMargin"]-$pdf->ez["rightMargin"];
		
	$data[0]=array($docProperties['textTop']);
			
	$pdf->ezTable($data,'','',$options);
	$pdf->ezSetDy(-10);		
	$options['cols'][0]['width']='';
	
	//	CREANDO TABLA
	$options['showLines']=2;// 0=no, 1=externos, 2=todos
	$options['showHeadings']=2;
	$options['shaded']=2;
	$options['shadeCol']=array(0.93,0.93,0.93);
	$options['shadeCol2']=array(1,1,1);		
	$options['rowGap']='2';
	$options['colGap']='2';
	$options['xPos']='center';
	$options['xOrientation']='center';
	$options['protectRows']=1;
	$options['fontSize']=$docProperties['fontSize'];
	$options['titleFontSize']=$docProperties['fontSize'];	

	$data=$xDataGrid->getPDFData();
	$headers=$xDataGrid->getPDFHeaders();
	
	// Atributos de las columnas
	$cont=3;
	foreach($xDataGrid->columns as $column){
		$options['cols'][$cont]['justification']=$column->align;
		$cont++;
	}	
	$_SESSION['xDataGrid']=serialize($xDataGrid);	
	$pdf->ezTable($data, $headers, '', $options);		

	// 	TEXTO FINAL
	unset($data);
	unset($options);
	$pdf->ezSetDy(-10);	
	$options['showLines']=0;// 0=no, 1=externos, 2=todos
	$options['showHeadings']=0;
	$options['shaded']=0;
	$options['shadeCol']=array(0.93,0.93,0.93);
	$options['shadeCol2']=array(1,1,1);	
	$options['fontSize']='10';
	$options['rowGap']='2';
	$options['colGap']='2';
	$options['xpos']='center';
	$options['xorientation']='left';
	$options['cols'][0]['xPos']='center';
	$options['cols'][0]['width']=$pdf->ez["pageWidth"]-$pdf->ez["leftMargin"]-$pdf->ez["rightMargin"];
			
	$data[0]=array($docProperties['textBottom']);
	$pdf->ezTable($data, '', '', $options);
	$options['cols'][0]['width']='';
	$pdf->ezStream();
?>
