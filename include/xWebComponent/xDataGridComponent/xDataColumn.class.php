<?php 
// xWebComponents
// xDataGrid Version 1.6
// Octubre de 2010
// Dise�ado y Desarrollado por Francisco Javier Bravo V. -  franciscobravo3220@gmail.com
// Adaptado para su uso en Orfeo GPL

class xDataColumn {
	public $type;
	public $title;
	public $size;		
	public $hover;
	public $consolidate;
	public $icon;
	public $action;	

	public function __construct($type='show', $title='', $size='20px', $align='left', $hover='', $consolidate='none', $icon='', $action='', $field=''){
		$this->type=$type;		
		$this->title=$title;
		$this->size=$size;
		$this->align=$align;
		$this->hover=$hover;
		$this->consolidate=$consolidate;
		$this->icon=$icon;
		$this->action=$action;	
		$this->field=$field;		
	}

	public function __get($var){
		switch($var){
			case('type'): return ($this->type); break;
			case('title'): return ($this->title); break;
			case('size'): return ($this->size); break;
			case('align'): return ($this->align); break;			
			case('hover'): return ($this->hover); break;
			case('consolidate'): return ($this->consolidate); break;
			case('icon'): return ($this->icon); break;
			case('action'): return ($this->action); break;
			case('code'): return ($this->code); break;
			case('field'): return ($this->field); break;
		}
	}
	public function __set($var,$val){
		switch($var){
			case('type'): $this->type = $val; break;
			case('title'): $this->title = $val; break;			
			case('size'): $this->size = $val; break;			
			case('align'): $this->align= $val; break;						
			case('hover'): $this->hover = $val; break;
			case('consolidate'): $this->consolidate = $val; break;
			case('icon'): $this->icon = $val; break;
			case('action'): $this->action = $val; break;												
			case('code'): $this->code= $val; break;
			case('field'): $this->field= $val; break;			
		}
	}

	// Id=dato a imprimir
	// z = indice de la fila (Llave primaria)
	public function getHTML($id, $z){
		switch ($this->type){
			// Solo para mostrar										
			case 'show':
				?><div align="<?php echo $this->align; ?>"><?php if($id!=''){ echo utf8_decode($id); }else{ echo '&nbsp;';  } ?></div><?php 
			break;
			// boton de accion
			case 'action':
			?>
				<center><img src="<?php echo $this->icon; ?>" width="19px" height="19px" onClick="<?php echo $this->action."'".$z."')"; ?>"/> </center> 
			<?php
			break;
			//Columna en blanco
			case 'empty':
			?>&nbsp;
			<?php
			break;
			case 'code':			
				?><div> <?php echo $this->code."$z'>";?></div>
                <?php
			break;
			case 'image':
				?><div align="<?php echo $this->align; ?>"><img src='<?php if($id!=''){ echo $id; }else{ echo '../../icons/no_image.png';  } ?>' /></div>
                <?php
			break;
		}
	
	}
}
?>
