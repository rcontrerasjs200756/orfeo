<?php 
// xWebComponents
// xDataGrid Version 1.6
// Octubre de 2010
// Diseñado y Desarrollado por Francisco Javier Bravo V. -  franciscobravo3220@gmail.com
// Adaptado para su uso en Orfeo GPL

class xDataGrid{
	public $conex; 			// Conexion a BD
	public $type;  			// Tipo: 
							//	Unique->seleccion unica
							//	multiple->seleccion multiple
							//	show->Solo de presentaci�n
	public $title;			// Titulo 
	public $columns; 		// arrays de objetos tipos xDataColumn 	
	public $initQuery; 		// Query inicial 
	public $fieldOrder; 	// Indice del campo de orden de la tabla
	public $orientation; 	// Orden ASC o DESC
	public $sizeRows; 		// Numero de registros a mostrar por pagina
	public $initLimit; 		// Registro inicial de la consulta
	public $textButton; 	// Titulo del boton de texto
							// Solo aplica a type: Unique y Multiple 
	public $submitFunction;// Funcion que se ejecuta cuando dan clic en el boton del formulario
							// Solo aplica a type: Unique y Multiple 
	public $docs;	   	 	// visible o hidden permite mostrar o ocultar los botones de generaci�n de documentos
	public $id;
	public $search;			
	public $display_search; // Activar opción de búsqueda
	public $docProperties;	// Activar opción exportar reportes
	// Campos para formato condicional de las filas
	public $conditionalField; // Campo de filtro condicional
	public $conditional; //  Array de condiciones y colores ------ Ej: $conditional[0]['compare']=comparación php - $conditional[0]['color']='#6677888'
	
	public function __construct($conex, $type='show', $size='', $align='center', $title='', $columns='', $initQuery='', $fieldOrder='1', $orientation='ASC', $sizeRows='100', $initLimit='0', $textButton='', $submitFunction='', $docs='visible', $search='', $display_search='false'){
		$this->conex=$conex; // CONEXION A BASE DE DATOS		
		$this->type=$type; // TIPO: SHOW, UNIQUE, MULTIPLE
		$this->size=$size; // TAMA�O (ANCHO)
		$this->align=$align; // ALINEACI�N
		$this->title=$title; // TITULO DE LA GRID
		$this->columns=$columns; // ARRAY DE COLUMNAS DE TIPO XDATACOLUMN
		$this->initQuery=$initQuery; // CONSULTA INICIAL
		$this->fieldOrder=$fieldOrder; // CAMPO POR EL CUAL SE ORDENAR�
		$this->orientation=$orientation; // ORDEN DE LA BUSQUEDA ASC O DESC
		$this->sizeRows=$sizeRows; // CANTIDAD DE REGISTROS DE LA BUSQUEDA
		$this->initLimit=$initLimit; // LIMITE INICIAL DE LA BUSQUEDA
		$this->textButton=$textButton; // TEXTO DEL BOTON DE FUNCION
		$this->submitFunction=$submitFunction; // FUNCION QUE SE EJECUTARA AL PRESIONAR EL BOTON, SOLO SE USA EN GRID DE SELECCION UNICA O MULTIPLE
		$this->docs=$docs;	// MUESTRA LAS OPCIONES DE GENERAR DOCUMENTOS PDF XLS Y DOC
		$this->search=$search; // ANEXO A QUERY INICIAL PARA BUSQUEDAS ESPECIFICAS 
		$this->display_search=$display_search; // MUESTRA LA BARRA DE BUSQUEDA ESPECIFICA
		$this->docProperties=$docProperties; // PROPIEDADES DEL INFORME PDF		
		$this->conditionalField=$conditionalField; // Campo de filtro condicional
		$this->conditional=$conditional; // Array de condiciones y colores
	}

	public function __get($var){
		switch($var){
			case('conex'): return ($this->conex); break;
			case('type'): return ($this->type); break;
			case('size'): return ($this->size); break;
			case('align'): return ($this->align); break;
			case('title'): return ($this->title); break;
			case('columns'): return ($this->columns); break;
			case('initQuery'): return ($this->initQuery); break;
			case('fieldOrder'): return ($this->fieldOrder); break;
			case('orientation'): return ($this->orientation); break;
			case('sizeRows'): return ($this->sizeRows); break;
			case('initLimit'): return ($this->initLimit); break;
			case('textButton'): return ($this->textButton); break;
			case('submitFunction'): return ($this->submitFunction); break;				
			case('docs'): return ($this->docs); break;
			case('id'): return ($this->id); break;
			case('search'): return ($this->search); break;
			case('display_search'): return ($this->display_search); break;
			case('docProperties'): return ($this->docProperties); break;
			case('conditionalField'): return ($this->conditionalField); break;
			case('conditional'): return ($this->conditional); break;
		}
	}
	public function __set($var,$val){
		switch($var){
			case('conex'): $this->conex = $val; break;
			case('type'): $this->type = $val; break;
			case('size'): $this->size = $val; break;			
			case('align'): $this->align = $val; break;			
			case('title'): $this->title = $val; break;			
			case('columns'): $this->columns = $val; break;
			case('initQuery'): $this->initQuery = $val; break;
			case('fieldOrder'): $this->fieldOrder = $val; break;
			case('orientation'): $this->orientation = $val; break;
			case('sizeRows'): $this->sizeRows = $val; break;
			case('initLimit'): $this->initLimit = $val; break;
			case('textButton'): $this->textButton = $val; break;
			case('submitFunction'): $this->submitFunction = $val; break;
			case('docs'): $this->docs = $val; break;
			case('id'): $this->id = $val; break;
			case('search'):$this->search = $val; break; 
			case('display_search'):$this->display_search = $val; break; 
			case('docProperties'):$this->docProperties = $val; break; 						
			case('conditionalField'):$this->conditionalField = $val; break; 
			case('conditional'):$this->conditional = $val; break; 						
		}
	}

	// Ejecutar consulta
	public function execQuery(){
		try{			
			// BUSCA SI EXISTE WHERE EN LA CONSULTA INICIAL									
			if($this->search!=''){				
				if(strstr($this->initQuery,"WHERE")==FALSE){
					$this->initQuery.=" WHERE ";
				}else{
					$this->initQuery.=" AND ";
				}
				// CONCATENA LA CADENA DE BUSQUEDA ESPECIFICA
				$this->initQuery.=$this->search;				
			}			
			$completeQuery=$this->initQuery." ORDER BY ".$this->fieldOrder." ".$this->orientation;			
			$result=$this->conex->execQuery($completeQuery, $this->initLimit, $this->sizeRows);			
			return $result;
		}catch(Exception $e){
			throw($e);		
		}
	}	
	// Ejecuta la consulta del total de registros (sin Limit)
	public function execUnLimitQuery(){
		try{			
			$completeQuery=$this->initQuery." ORDER BY ".$this->fieldOrder." ".$this->orientation; // Consulta completa						
			$result=$this->conex->execQuery($completeQuery);			
			return $result;					
		}catch(Exception $e){
			throw($e);		
		}
	}
	
	// Ejecuta la consulta para saber el total de registros
	public function countQuery(){
		try{			
			$countQuery=strstr($this->initQuery, 'FROM RAD');
			if($countQuery==''){				
				$countQuery=strstr($this->initQuery, 'FROM INF');
			}
			$countQuery="SELECT COUNT(*) AS TOTAL $countQuery";			
			$result=$this->conex->execQuery($countQuery);			
			return $result->fields['TOTAL'];
		}catch(Exception $e){
			throw($e);		
		}
	}
	
	
	// Devuelve el codigo HTML de la tabla generada
	public function getHTML(){
		
		$result=$this->execQuery();		
		$column=$this->columns[0];		
		// Encuentro valores para paginación
		/*
		// Para saber cuantas páginas van a ser
		$PageQuery="SELECT FOUND_ROWS()"; 
		$resultPageQuery=mysql_query($PageQuery);
		$rowPageQuery=mysql_fetch_row($resultPageQuery);
		$totalRegistros=$rowPageQuery[0];	
		*/
		if($result){
			$numRows=$this->conex->num_rows($result);	
			$totalRegistros=$this->countQuery();			
		}		
		$ban=0;	
		$dataString=$this->conex->con->databaseType."-".$this->conex->con->host."-".$this->conex->con->user."-".$this->conex->con->password."-".$this->conex->con->database;
		ob_start();
		?>			
        	<form id="xFormGrid<?php echo $this->id; ?>" name="xFormGrid<?php echo $this->id; ?>" target="_blank" method="POST">
            <div id='tableZone' style="overflow:auto">
            <input type="hidden" id="id" name="id" value="<?php echo $this->id; ?>">
            <table width="<?php echo $this->size; ?>" align="<?php echo $this->align; ?>" class="xDataGrid" cellpadding="0px" cellspacing="4px">
                <!-- PINTO FILA DE TITULO -->				
				<?php if ($this->title!=''){ ?>
                <tr class="rowTitle">                    
                    <td class="cellGrid" colspan="<?php if($this->type=='unique' || $this->type=='multiple'){ 
															echo count($this->columns)+1; 
														}else{
															echo count($this->columns);
														}
									?>">
                        <b><?php echo $this->title; ?></b>
                    </td>        
                </tr>
                <?php }?>
                <!-- PINTO FILA DE BUSQUEDA ESPECIFICA-->
                <?php if ($this->display_search=='true'){ ?>
                    <tr class="rowHead">                
						<td class="cellGrid" style="padding:2px" align="right" colspan="<?php if($this->type=='unique' || $this->type=='multiple'){ 
                                                                echo count($this->columns)+1; 
                                                            }else{
                                                                echo count($this->columns);
                                                            }
                                        ?>">
						<strong>
							<a href="javascript:void(0)" onclick="xajax_xDataGridSearch('R.RADI_LEIDO', '0', '<?php echo $this->id ?>')" style="color:#006699; text-decoration:none; cursor:pointer">No Leidos</a>
							<a href="javascript:void(0)" onclick="xajax_xDataGridSearch('R.RADI_LEIDO', '1', '<?php echo $this->id ?>')" style="color:#006699; text-decoration:none; cursor:pointer">Leidos</a>
							
						</strong>
										
                        <strong><span style="color:black">Buscar por:</span></strong>
                        <select id="<?php echo $this->id; ?>_sel" name="<?php echo $this->id; ?>_sel" style="width:100px; font-size:10; margin:0; padding:0px" >
                            <?php 
                            foreach($this->columns as $key=>$columna){ 								
								if($columna->type=='show' && $key>=3){
							?>    
                                <option value="<?php echo $columna->field ?>"><?php echo $columna->title ?></option>
                            <?php 						
								}
                            }?>	
                        </select>
                        <input type="text" class="xText" style="font-size:10; margin:0; padding:2px" id="<?php echo $this->id; ?>_search" name="<?php echo $this->id; ?>_search" size="15"  onkeypress="tecla = (document.all) ? event.keyCode :event.which; return (tecla!=13);">
                        <img src="../include/xWebComponent/xIconsComponents/xDataGrid/find.png" width="15" onClick="xajax_xDataGridSearch(get('<?php echo $this->id; ?>_sel').value, get('<?php echo $this->id; ?>_search').value, '<?php echo $this->id; ?>' , '<?php echo $dataString ?>');" />
                        </td>
                    </tr>
                <?php } ?>                
                <!-- PINTO FILA DE CABECERAS -->
                <tr class="rowHead">
                <?php                										
					$cont=2;
					foreach($this->columns as $columna){ ?>    
                    <td class="cellGrid" width="<?php echo $columna->size; ?>">                                                
                        <table width="100%">
                            <tr>
                                <td align="center" class="cellHead">
									<b><?php echo $columna->title; ?></b>
                                </td>
                                <td align="right" width="16px">                             
                                    <?php if($columna->type=='show'){ ?>
                                    		<img src="../include/xWebComponent/xIconsComponents/xDataGrid/order_asc.png" onClick="xajax_xDataGrid('order', <?php echo $cont-1; ?>, '<?php echo $this->id; ?>' , '<?php echo $dataString ?>');" />
									<?php }?>
                                </td>                            
                            </tr>
                        </table>                  
                    </td>
                    <?php 
						$cont++;
					}
					if($this->type=='unique' || $this->type=='multiple'){
					?>
						<td class="cellGrid">&nbsp;
							
						</td>
					<?php
					}
					?>
                </tr>
              
                <!-- PINTO FILAS DE LA CONSULTA-->                
				<?php 				
				if(!$result){					
				?>				
                    <tr class="rowOdd">
                        <td class="cellGrid" colspan="<?php if($this->type=='unique' || $this->type=='multiple'){ 
                                                                echo count($this->columns)+1; 
                                                            }else{
                                                                echo count($this->columns);
                                                            }
                                        ?>">
                            
                            <center><br><b>No existen resultados con los criterios que usted eligi&oacute;.</b><br><br></center>
                                                        
                        </td>        
                    </tr>
                <?php	
				}				
				for($i=0; $i<$numRows; $i++){					
					$row=$this->conex->getRow($result);					
					$cont=0;
				?>
            		<tr class="row<?php if($ban==0){ echo "Eve"; $ban=1;}else{ echo "Odd"; $ban=0; } ?>" 
						onMouseOver="this.className='onRow'" 
						onMouseOut="this.className='Row<?php if($ban==0){ echo "Odd";}else{ echo "Eve";} ?>'"
						<?php 	// Formato condicional para filas								
								if($this->conditionalField>=0){									
									// Verifico que condición se cumple
									$conditionalField=$this->conditionalField;
									if($this->conditional!=''){										
										$color="";
										// es importante tener en cuenta que las condiciones se coloquen en orden dado que el sistema deja la última que aplique.
										foreach($this->conditional as $condition){											
											$compare="";
											foreach($condition["compare"] as $exp){
												$compare.="'".$row["$conditionalField"]."' $exp && ";
											}
											$compare=substr($compare, 0, strlen($compare)-3);											
											eval("if($compare){ \$xcompare='1'; }else{  \$xcompare='0'; }");
											
											$uno.="<br>".$compare;											
											if($xcompare==1){												
												$color=$condition['color'];
											}
										}										
										if($color!=''){										
								/*?>
										style="background-color:<?php echo "$color";?>"
							<?php 	*/	}
									}
								}
							?>
						>						
                    	<?php 
							reset($this->columns);							
							foreach($this->columns as $columna){
							?>
                            <td class="cellGrid"
								<?php
									if($color!=''){
										echo "style='color:$color'";
									}
									/*if($row[9]==1){
										echo "style='color:#006699'";
									}else{
										echo "style='color:#488F03'";
									}*/
								?>
							>
								<?php     
									switch($cont){
										case "3":
											if($row[10]!=''){
												echo "<a href='../bodega/".$row[10]."'>".$row[$cont]."</a>";
											}else{
												echo $row[$cont];
											}
										break;										
										case "4":
											echo "<a href='../verradicado.php?verrad=".$row[3]."'>".$row[$cont]."</a>";
										break;
										
										case "5":											
											echo "<div id='info".$row[3]."' onclick='xajax_showInfoRad(\"".$row[3]."\")'>".$row[5]."</div>";
										break;
										default:
											echo $columna->getHtml($row[$cont],$row[3]);
										break;									
									}		
                                ?>
                            </td>							
                         <?php							
							$cont++;
							}
							if($this->type=='unique'){
							?>
								<td class="cellGrid">
                                	<center><input type="radio" name='xrad' id="xrad" value="<?php echo $row[0]; ?>"/></center>
                                </td>
                            <?php
							}
							if($this->type=='multiple'){
							?>
								<td class="cellGrid"> 
                                	<center><input type="checkbox" name='checkValue[<?php echo $row[3]; ?>]' value="CHKANULAR"  id="xchk<?php echo $row[3]; ?>" value="<?php echo $row[3]; ?>"/></center>
                                </td>
                            <?php
							}
						?>
                    </tr>                    				
				<?php 
				}
				?>					
				</table>
                </div>
                <table width="<?php echo $this->size+40; ?>" align="<?php echo $this->align; ?>" class="xDataGrid" cellpadding="0px" cellspacing="0px">
                <!--BOTONES DE PAGINACI�N -->
                <tr class="xRowOptions">                    
                    <td class="cellGrid" style="border-right:0;">										
                    <?php 	$fin=($this->initLimit+$this->sizeRows);
							if($fin>$totalRegistros){
								$fin=$totalRegistros;
							}
							if($totalRegistros>0){
								echo ($this->initLimit+1)." - ".$fin." de ".$totalRegistros;
							}else{
								echo "0 de 0";
							}?>
                    </td>
                	<td align="right" class="cellGrid" colspan="<?php if($this->type=='unique' || $this->type=='multiple'){ 
															echo count($this->columns); 
														}else{
															echo count($this->columns)-1;
														}
									?>">                        
                        <?php 							
							echo "&nbsp;";
							if($numRows == $this->sizeRows ){  ?>
	                        <div class="xForwardButton" onMouseOver="this.className='xForwardButton_hover'" onMouseOut="this.className='xForwardButton'" onClick="xajax_xDataGrid('forward', '','<?php echo $this->id; ?>' , '<?php echo $dataString ?>');"></div>                         
                        <?php }?>
                        <?php if($this->initLimit != 0){ ?>
                        	
	                        <div class="xBackButton" onMouseOver="this.className='xBackButton_hover'" onMouseOut="this.className='xBackButton'" onClick="xajax_xDataGrid('back', '', '<?php echo $this->id; ?>' , '<?php echo $dataString ?>');"></div>
                        <?php }?>
                    </td>                            
                </tr>
				<?php								
				if ($this->type=='unique' || $this->type=='multiple' ||  $this->type=='action' ){ ?>
                <!-- FILA DE BOTON DE ENVIO DE FORMULARIO -->
                <tr class="xRowOptions">                    
                	<td class="cellGrid" colspan="<?php 	if($this->type=='unique' || $this->type=='multiple'){ 
											echo count($this->columns)+1; 
										}else{
											echo count($this->columns);
										}
									?>">
                        <!--<center><input type="button" class="xbutton" onMouseOver="this.className='xbutton_hover'" onMouseOut="this.className='xbutton'" value="<?php //echo $this->textButton; ?>" onClick="<?php //echo $this->submitFunction; ?>(xajax.getFormValues('xFormGrid<?php //echo $this->id; ?>'));" /></center>-->
                    </td>        
                </tr>
                <?php }
				if ($this->docs=='visible'){
				?>
                <!-- FILA DE BOTONES DE REPORTES EN DOCUMENTOS-->
                <tr class="xRowOptions">
                	<td class="cellGrid" colspan="<?php if($this->type=='unique' || $this->type=='multiple'){ 
															echo count($this->columns)+1; 
														}else{
															echo count($this->columns);
														}
									?>">
                        <table class="xRowOptions">
                            <tr class="xRowOptions">
                                <td>
                                	Generaci&oacute;n de reportes:
                                </td>
								<!--
                                <td>
                                    <div class="buttonPDF" onMouseOver="this.className='buttonPDF_hover'" onMouseOut="this.className='buttonPDF'" onClick="document.getElementById('xFormGridxdiv_entrada_orfeo').action='../include/xWebComponent/xDataGridComponent/xDataGridPDF.php'; document.getElementById('xFormGridxdiv_entrada_orfeo').submit();"></div>
                                </td>
                                <td>
                                	PDF 
                                </td>                                -->
                                <td>
                                    <div class="buttonXLS" onMouseOver="this.className='buttonXLS_hover'" onMouseOut="this.className='buttonXLS'" onClick="document.getElementById('xFormGridxdiv_entrada_orfeo').action='../include/xWebComponent/xDataGridComponent/xDataGridXLS.php'; document.getElementById('xFormGridxdiv_entrada_orfeo').submit();"></div>
                                </td>
                                <td>
	                                XLS 
                                </td>                           
                            </tr>
                        </table>
                    </td>
                </tr> 
                <?php }				
				?>                
            </table>			
		<?php				
		return ob_get_clean();
	}	
	
	
	// Genera archivo de Word y Excel
	public function getDOC(){		
		ob_start();	
		$result=$this->execUnLimitQuery();		
		if($result){
			$numRows=$this->conex->num_rows($result);	
		}		
		$ban=0;
		$k=0;
		foreach($this->columns as $column){
			if($column->type=='show'){
				$k++;
			}
		}	
		
		?>
        <html>
        <body>
        <form id="xFormGrid" name="xFormGrid" target="_blank">           
            <table width="<?php echo $this->size; ?>"  align="<?php echo $this->align; ?>" style="border-collapse:collapse;" cellpadding="0px" cellspacing="0px" border="1">
                <!-- PINTO FILA DE TITULO -->
				<?php if ($this->title!=''){ ?>
                <tr class="rowTitle">                    
                    <td class="cellGrid" align="center" colspan="<?php echo $k;	?>">
                        <b><?php echo $this->title; ?></b>
                    </td>        
                </tr>
                <?php }?>
                <!-- PINTO FILA DE CABECERAS -->
                <tr class="rowHead" style="visibility:visible;">
                	<?php																	
					$columna='';
					foreach($this->columns as $key=>$columna){                     	
						if($columna->type=='show' && $key>=3){						
					?>
                        <td class="cellGrid" align="center" style="visibility:visible;">                         						
                            <b><?php echo $columna->title; ?></b>   
                        </td>
                    <?php 						
						}
					}?>
                </tr>
                <!-- PINTO FILAS DE LA CONSULTA-->
                <?php 						
				for($i=0; $i<$numRows; $i++){	
					$row=$this->conex->getRow($result);					
					$cont=0;				
				?>
            		<tr class="row<?php if($ban==0){ echo "Eve"; $ban=1;}else{ echo "Odd"; $ban=0; } ?>">
                    	<?php 
							reset($this->columns);							
							foreach($this->columns as $key=>$columna){
								if($key>=3){
									if($columna->type=='show' || $columna->type=='noModify' || $columna->type=='text' || $columna->type=='select'){
									?>
									<td class="cellGrid">
										<div align="<?php echo $columna->align; ?>"><?php echo $row[$cont]; ?></div>
									</td>
									<?php                                     
									}                          
								}
							$cont++;
							}
						?>
                    </tr>                    				
				<?php 
				}
				?>								
            </table>
        </form>	               
		</body>
        </html>
		<?php				
		return ob_get_clean();
	}			


// RETORNA EL ARRAY DE DATOS DE LA TABLA
	public function getPDFData(){	
		$result=$this->execUnLimitQuery();
		if($result){
			$numRows=$this->conex->num_rows($result);	
		}
		if($numRows>0){
			for($i=0; $i<$numRows; $i++){
				$row=$this->conex->getRow($result);
				$cont=0;
				for($j=3; $j<count($row); $j++){				
					$array[$i][$cont]=html_entity_decode($row[$j]);
					$cont++;
				}
			}
		}	
		return $array;
	}		
	
// RETORNA EL ARRAY DE DATOS DE LAS CABECERAS
	public function getPDFHeaders(){			
		$cont=0;
		foreach($this->columns as $key=>$columna){
			if($key>=3){
				if($columna->type=='show' || $columna->type=='noModify' || $columna->type=='text' || $columna->type=='select'){
					$title=html_entity_decode($columna->title);				
					$array[$cont]='<b>'.$title.'</b>';
					$cont++;
				}
			}
		}
		return $array;
	}		
}
?>
