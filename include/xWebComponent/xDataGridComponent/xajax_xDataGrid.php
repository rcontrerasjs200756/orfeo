<?php 	
// xWebComponents
// xDataGrid Version 1.6
// Octubre de 2010
// Diseñado y Desarrollado por Francisco Javier Bravo V. -  franciscobravo3220@gmail.com
// Adaptado para su uso en Orfeo GPL
	
	function xDataGrid($function, $attrib, $id, $data=''){		
		$xres=new xajaxResponse();
		define('ADODB_ASSOC_CASE', 1);
		require_once('../include/adodb/adodb.inc.php');
		require_once("../include/xWebComponent/database/DBConnection.php");		
		if($data==''){			
			require_once("../include/xWebComponent/database/data.php");
		}else{
			$data=explode("-", $data);
			$dbdriver=$data[0];
			$server=$data[1];
			$user=$data[2];
			$password=$data[3];
			$database=$data[4];		
		}		
		require_once("../include/xWebComponent/xDataGridComponent/xDataGrid.class.php");
		require_once("../include/xWebComponent/xDataGridComponent/xDataColumn.class.php");		
		$con=new DBConnection($dbdriver, $server, $user, $password, $database);				
		
		// DesSerializo el grid					
		$xDataGrid=unserialize($_SESSION[$id]);			
		switch($function){
			case 'order':
				$xDataGrid->fieldOrder=$attrib;
				$xDataGrid->initLimit=0;				
			break;
			case 'back':				
				$xDataGrid->initLimit=$xDataGrid->initLimit-$xDataGrid->sizeRows;						
			break;
			case 'forward':
				$xDataGrid->initLimit=$xDataGrid->initLimit+$xDataGrid->sizeRows;
			break;
		}						
		$xDataGrid->conex=$con;					
		// Serializo el array de DataGrids y envio eso a la variable de sesi�n
		$_SESSION[$id]=serialize($xDataGrid);		
		$xres->addAssign("$id","innerHTML",$xDataGrid->getHTML());
		return utf8_encode($xres->getXML());
	}

	function xDataGridSearch($field, $value, $id, $data=""){
		require_once('../include/adodb/adodb.inc.php');	
		require_once("../include/xWebComponent/database/DBConnection.php");		
		require_once("../include/xWebComponent/xDataGridComponent/xDataGrid.class.php");
		require_once("../include/xWebComponent/xDataGridComponent/xDataColumn.class.php");		
		
		
		if($data==''){			
			require_once("../include/xWebComponent/database/data.php");
		}else{
			$data=explode("-", $data);
			$dbdriver=$data[0];
			$server=$data[1];
			$user=$data[2];
			$password=$data[3];
			$database=$data[4];		
		}
		
		
		$con=new DBConnection($dbdriver, $server, $user, $password, $database);
		$xres=new xajaxResponse();		
		
		// DesSerializo el grid
		$xDataGrid=unserialize($_SESSION[$id]);
		$xDataGrid->search="$field LIKE '%$value%'";		
		$xDataGrid->initLimit=0;
		// Serializo el array de DataGrids y envio eso a la variable de sesi�n
		$xDataGrid->conex=$con;
		$_SESSION[$id]=serialize($xDataGrid);				
		$xres->addAssign("$id","innerHTML",$xDataGrid->getHTML());
		return utf8_encode($xres->getXML());
	}
	
?>
