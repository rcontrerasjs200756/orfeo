<?php 
// xWebComponents
// xDataGrid Version 1.6
// Octubre de 2010
// Dise�ado y Desarrollado por Francisco Javier Bravo V. -  franciscobravo3220@gmail.com
// Adaptado para su uso en Orfeo GPL

	session_start();	
	define('ADODB_ASSOC_CASE', 1);
	require_once("xDataGrid.class.php");
	require_once("xDataColumn.class.php");	
	require_once("../xTabularDataAdmin/xTabularColumnAdmin.class.php");	
	require_once("../xTabularDataAdmin/xTabularDataAdmin.class.php");		
	require('../../../libraries/adodb/adodb.inc.php');
	require("../../../class/database/DBConnection.php");
	require_once("../../../class/database/data.php");			
	$nomfile="xReport-".date("Y-m-d").".doc";
	header("Content-type: application/msword; ");
	header("Content-Disposition: filename=\"$nomfile\";");	
	$conex=new DBConnection($dbdriver, $server, $user, $password, $database);
	$id=$_POST['id'];
	$xDataGrid=unserialize($_SESSION["$id"]);		
	$xDataGrid->conex=$conex;
	echo $xDataGrid->getDoc();
	$_SESSION['xDataGrid']=serialize($xDataGrid);
?>
