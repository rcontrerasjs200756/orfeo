<?php 
// xWebComponents
// xDataGrid Version 1.6
// Octubre de 2010
// Dise�ado y Desarrollado por Francisco Javier Bravo V. -  franciscobravo3220@gmail.com
// Adaptado para su uso en Orfeo GPL

	session_start();	
	define('ADODB_ASSOC_CASE', 1);
	require_once("xDataGrid.class.php");
	require_once("xDataColumn.class.php");	
	
	require('../../adodb/adodb.inc.php');
	require("../database/DBConnection.php");	
	require_once("../database/data.php");	
	$nomfile="xReport-".date("Y-m-d").".xls";
	header("Content-type: application/vnd.ms-excel");
	header("Content-Disposition: filename=\"$nomfile\";");	
	$conex=new DBConnection($dbdriver, $server, $user, $password, $database);
	$id=$_POST['id'];		
	$xDataGrid=unserialize($_SESSION["$id"]);
	$xDataGrid->conex=$conex;
	echo $xDataGrid->getDoc();	
	$_SESSION["$id"]=serialize($xDataGrid);	
	
?>
