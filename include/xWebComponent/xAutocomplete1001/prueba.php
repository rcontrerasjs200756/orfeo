<?php
	session_start();
	require_once("../../../libraries/xajax/xajax.inc.php");
	$xajax = new xajax(); // CREACION DEL OBJETO XAJAX
	// Funcion xWebComponents
	$xajax->registerExternalFunction("loadAutocomplete", "xajax_xAutocomplete.php");					
	$xajax->processRequests();

	require_once('../../../libraries/adodb/adodb.inc.php');
	require_once("../../../class/database/DBConnection.php");
	require_once("../../../class/database/data.php");
	include("xAutocomplete.class.php");
	
	$con=new DBConnection($dbdriver, $server, $user, $password, $database);
	
	
	$xAutocomplete=new xAutocomplete($con, "xAutocomplete");	
	$xAutocomplete->initQuery="SELECT 
								  I.INFBASCODIGO,
								  CONCAT(I.INFBASNOMBRE1, CONCAT(' ', I.INFBASNOMBRE2)) AS NOMBRE, 
								  CONCAT(I.INFBASAPELLIDO1, CONCAT(' ', I.INFBASAPELLIDO2)) AS APELLIDOS, 
								  I.INFBASNUMDOC AS DOCUMENTO,								  
								  F.FUENOMBRE AS FUERZA, 
								  A.ARMNOMBRE AS ARMA,
								  G.GRANOMBRE AS GRADO,
								  U.UNIMILNOMBRE AS UNIDAD								  
									FROM INFORMACION_BASICA I
									LEFT JOIN G_FUERZA F ON I.INFBASFUERZA=F.FUECODIGO
									LEFT JOIN G_GRADO G ON I.INFBASGRADO=G.GRACODIGO AND I.INFBASCATEGORIA=G.GRACATEGORIA AND I.INFBASFUERZA=G.GRAFUERZA
									LEFT JOIN G_ARMA A ON I.INFBASARMA=A.ARMCODIGO AND I.INFBASFUERZA=A.ARMFUERZA
									LEFT JOIN UNIDAD_MILITAR U ON I.INFBASBATUNI=U.UNIMILCODIGO AND I.INFBASFUERZA=U.UNIMILFUERZA";	
	
	$xAutocomplete->searchFields[0]="I.INFBASCODIGO";		
	$xAutocomplete->searchFields[1]="CONCAT(I.INFBASNOMBRE1, CONCAT(' ', I.INFBASNOMBRE2))";
	$xAutocomplete->searchFields[2]="CONCAT(I.INFBASAPELLIDO1, CONCAT(' ', I.INFBASAPELLIDO2))";
	$xAutocomplete->searchFields[3]="F.FUENOMBRE";
	
	$xAutocomplete->labelFields[0]="INFBASCODIGO";
	$xAutocomplete->labelFields[1]="NOMBRE";
	$xAutocomplete->labelFields[2]="APELLIDOS";
	$xAutocomplete->labelFields[3]="FUERZA";	
	
	$xAutocomplete->size=400;
	$xAutocomplete->order=2;
	$id=$xAutocomplete->id;	
	
	// OTRO	
	$xAutocomplete2=new xAutocomplete($con, "xAutocomplete2");
	
	$xAutocomplete2->initQuery="SELECT 
								  I.INFBASCODIGO,
								  CONCAT(I.INFBASNOMBRE1, CONCAT(' ', I.INFBASNOMBRE2)) AS NOMBRE, 
								  CONCAT(I.INFBASAPELLIDO1, CONCAT(' ', I.INFBASAPELLIDO2)) AS APELLIDOS, 
								  I.INFBASNUMDOC AS DOCUMENTO,								  
								  F.FUENOMBRE AS FUERZA, 
								  A.ARMNOMBRE AS ARMA,
								  G.GRANOMBRE AS GRADO,
								  U.UNIMILNOMBRE AS UNIDAD								  
									FROM INFORMACION_BASICA I
									LEFT JOIN G_FUERZA F ON I.INFBASFUERZA=F.FUECODIGO
									LEFT JOIN G_GRADO G ON I.INFBASGRADO=G.GRACODIGO AND I.INFBASCATEGORIA=G.GRACATEGORIA AND I.INFBASFUERZA=G.GRAFUERZA
									LEFT JOIN G_ARMA A ON I.INFBASARMA=A.ARMCODIGO AND I.INFBASFUERZA=A.ARMFUERZA
									LEFT JOIN UNIDAD_MILITAR U ON I.INFBASBATUNI=U.UNIMILCODIGO AND I.INFBASFUERZA=U.UNIMILFUERZA";	

									
	$xAutocomplete2->searchFields[0]="I.INFBASCODIGO";		
	$xAutocomplete2->searchFields[1]="CONCAT(I.INFBASNOMBRE1, CONCAT(' ', I.INFBASNOMBRE2))";
	$xAutocomplete2->searchFields[2]="CONCAT(I.INFBASAPELLIDO1, CONCAT(' ', I.INFBASAPELLIDO2))";
	$xAutocomplete2->searchFields[3]="F.FUENOMBRE";
	
	$xAutocomplete2->labelFields[0]="INFBASCODIGO";
	$xAutocomplete2->labelFields[1]="NOMBRE";
	$xAutocomplete2->labelFields[2]="APELLIDOS";
	$xAutocomplete2->labelFields[3]="FUERZA";	
	
	$xAutocomplete2->size=400;
	$xAutocomplete2->order=2;
	$id=$xAutocomplete2->id;	
	
	// OTRO	
	$xAutocomplete3=new xAutocomplete($con, "xAutocomplete3");
	
	$xAutocomplete3->initQuery="SELECT 
								  I.INFBASCODIGO,
								  CONCAT(I.INFBASNOMBRE1, CONCAT(' ', I.INFBASNOMBRE2)) AS NOMBRE, 
								  CONCAT(I.INFBASAPELLIDO1, CONCAT(' ', I.INFBASAPELLIDO2)) AS APELLIDOS, 
								  I.INFBASNUMDOC AS DOCUMENTO,								  
								  F.FUENOMBRE AS FUERZA, 
								  A.ARMNOMBRE AS ARMA,
								  G.GRANOMBRE AS GRADO,
								  U.UNIMILNOMBRE AS UNIDAD								  
									FROM INFORMACION_BASICA I
									LEFT JOIN G_FUERZA F ON I.INFBASFUERZA=F.FUECODIGO
									LEFT JOIN G_GRADO G ON I.INFBASGRADO=G.GRACODIGO AND I.INFBASCATEGORIA=G.GRACATEGORIA AND I.INFBASFUERZA=G.GRAFUERZA
									LEFT JOIN G_ARMA A ON I.INFBASARMA=A.ARMCODIGO AND I.INFBASFUERZA=A.ARMFUERZA
									LEFT JOIN UNIDAD_MILITAR U ON I.INFBASBATUNI=U.UNIMILCODIGO AND I.INFBASFUERZA=U.UNIMILFUERZA";	

									
	$xAutocomplete3->searchFields[0]="I.INFBASCODIGO";		
	$xAutocomplete3->searchFields[1]="CONCAT(I.INFBASNOMBRE1, CONCAT(' ', I.INFBASNOMBRE2))";
	$xAutocomplete3->searchFields[2]="CONCAT(I.INFBASAPELLIDO1, CONCAT(' ', I.INFBASAPELLIDO2))";
	$xAutocomplete3->searchFields[3]="F.FUENOMBRE";
	
	$xAutocomplete3->labelFields[0]="INFBASCODIGO";
	$xAutocomplete3->labelFields[1]="NOMBRE";
	$xAutocomplete3->labelFields[2]="APELLIDOS";
	$xAutocomplete3->labelFields[3]="FUERZA";	
	
	$xAutocomplete3->size=400;
	$xAutocomplete3->order=2;
	$id=$xAutocomplete3->id;	
	
	
?>
<html>
    <head>		
    	<link rel="stylesheet" type="text/css" href="xAutocomplete_gray.css"/>
		<?php $xajax->printJavascript('../../../libraries/xajax/'); ?>
		<script src="xAutocomplete.js"></script>
		<script src="../../../scripts/basicFunctions.js"></script>
    </head>
	<body>
			<b>Prueba de xAutocomplete</b>
		<br><br><br><br><br>
			<div id="xAutocomplete">
				<?php echo $xAutocomplete->getHTML(); ?>
			</div>
    	<br>
		<select>		
			<option>fdalkfjdlsfasd fasdfkal�sdfj al�fjlak�sdjfklas�jdfl</option>
		</select>		
		<br><br><br><br>        
		<br><br><br><br><br>			
			<div id="xAutocomplete2">
				<?php echo $xAutocomplete2->getHTML(); ?>
			</div>
    	<br>
		<br><br><br><br>        
		<br><br><br><br><br>			
			<div id="xAutocomplete3">
				<?php echo $xAutocomplete3->getHTML(); ?>
			</div>
    	<br>
    </body>
</html>
<?php
	$_SESSION["xAutocomplete"]=serialize($xAutocomplete);
	$_SESSION["xAutocomplete2"]=serialize($xAutocomplete2);	
	$_SESSION["xAutocomplete3"]=serialize($xAutocomplete3);	
?>
