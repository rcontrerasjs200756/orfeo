<?php 
	class xAutocomplete{
		public $conex; // Conexion a BD
		public $id; // id del objeto
		public $initQuery; // consulta sin where
		public $searchFields; // array de campos por los que se va a filtrar la b�squeda
		public $labelFields; // Campos que se van a mostrar en el combo
		public $size; // Tama�o del campo en pixeles
		public $order=0; // Campo por el cual se va a ordenar (N�mero)
		public $minCharacter=1; // minimo de caracteres para iniciar la b�squeda
		public $action; // Acci�n adicional que se ejecuta cuando selecciona un registro
		public $time; // yiempo de inactividad del teclado que se espera para iniciar la consulta
		public $posx; // Posici�n X
		public $posy; // Posici�n y

		
		function __construct($conex, $id, $initQuery="", $searchFields="", $labelFields="", $size='200px', $time=100){
			$this->conex=$conex;
			$this->id=$id;
			$this->initQuery=$initQuery;			
			$this->searchFields=$searchFields;			
			$this->labelFields=$labelFields;			
			$this->size=$size;			
			$this->time=$time;
		}
		
		public function __get($var){
			switch($var){
				case('conex'): return ($this->conex); break;
				case('id'): return ($this->id); break;
				case('initQuery'): return ($this->initQuery); break;
				case('searchFields'): return ($this->searchFields); break;
				case('labelFields'): return ($this->searchFields); break;				
				case('size'): return ($this->size); break;
				case('order'): return ($this->order); break;
				case('timeResponse'): return ($this->searchFields); break;
				case('minCharacter'): return ($this->searchFields); break;				
				case('action'): return ($this->action); break;				
				case('time'): return ($this->time); break;				
			}
		}
		public function __set($var, $val){
			switch($var){
				case('conex'): $this->conex = $val; break;
				case('id'): $this->id = $val; break;
				case('initQuery'): $this->initQuery = $val; break;
				case('searchFields'): $this->items = $val; break;
				case('labelFields'): $this->labelFields = $val; break;
				case('size'): $this->size = $val; break;
				case('order'): $this->order = $val; break;
				case('timeResponse'): $this->timeResponse = $val; break;
				case('minCharacter'): $this->minCharacter = $val; break;
				case('action'): $this->action = $val; break;
				case('time'): $this->time = $val; break;
			}
		}
		
		public function getSearch($value){
			if($this->searchFields!=''){			
				$where="";								
				foreach($this->searchFields as $element){
					$where.="UPPER($element) LIKE UPPER('%$value%') OR ";
				}				
				$where=substr($where, 0, strlen($where)-3);					
				
				if(strstr($this->initQuery, 'WHERE')){
					$completeQuery=$this->initQuery." AND ($where)";
				}else{
					$completeQuery=$this->initQuery." WHERE $where";				
				}				
				if($this->order !=''){
					$completeQuery.=" ORDER BY ".$this->order." ASC";					
				}				
				$result=$this->conex->execQuery($completeQuery, 0, 10);	
				if($result){
					$numRows=$this->conex->num_rows($result);					
				}				
				
				ob_start();						
				?>
				<table width="<?php echo $this->size?>px">
				<?php
				$class="even";				
				for($i=0; $i<$numRows; $i++){
					// Resalto la coincidencia en la b�squeda
					$fila=$this->conex->getRow($result);
					if($this->labelFields!=''){
						$label="";						
						foreach($this->labelFields as $element){
							$label.=$fila["$element"]." - ";
						}
						$label=substr($label, 0, strlen($label)-3);						
						$label=str_replace(strtoupper($value), "<b>".strtoupper($value)."</b>", $label);
					}				
					if($class=="Odd"){
						$class="Eve";
					}else{
						$class="Odd";
					}
					?>	
					<tr>
						<td class="row<?php echo $class ?>Autocomplete" 
							onmouseover="this.className='onRowAutocomplete'" 
							onmouseout="this.className='row<?php echo $class ?>Autocomplete'"
							onclick="document.getElementById('<?php echo $this->id ?>_hidden').value='<?php echo $fila[0]; ?>'; 
									 document.getElementById('<?php echo $this->id ?>_div').style.display='none';
									 document.getElementById('<?php echo $this->id ?>_text').value='<?php echo strip_tags($label); ?>';
									 <?php 	if($this->action!=''){
												echo $this->action."('".$fila[0]."');";
											}?>">
							<?php echo $label ?>
						</td>
					</tr>
				<?php
				}
				?>											
					</table>
				<?php
				return ob_get_clean();
			}			
		}	

		public function getHTML(){
			ob_start();
			?>
			<input type="hidden" id="<?php echo $this->id; ?>_hidden" name="<?php echo $this->id; ?>_hidden">
			<input type="hidden" id="<?php echo $this->id; ?>_cont" name="<?php echo $this->id; ?>_cont">
			<input type="text" autocomplete="off" style="width:<?php echo $this->size; ?>px;" id="<?php echo $this->id; ?>_text" name="<?php echo $this->id; ?>_text" 
					onkeyup="	if(this.value.length>2){ 
									xAutocompletePush('<?php echo $this->id; ?>', '<?php echo $this->time; ?>');
								}
								else{
									document.getElementById('<?php echo $this->id; ?>_div').style.display='none';
								}">
			
			<div id="<?php echo $this->id; ?>_div" style="position:absolute; font-family:arial; position:absolute;left:<?php echo $this->posx; ?>px;top:<?php echo $this->posy; ?>px;"></div>
			<?php
			if($this->posx=="" && $this->posy==""){
			?>
			<script>
				var x=findPosX(document.getElementById('<?php echo $this->id ?>_text'));
				var y=findPosY(document.getElementById('<?php echo $this->id ?>_text'));
				
				document.getElementById('<?php echo $this->id ?>_div').style.top=y+40;
				document.getElementById('<?php echo $this->id ?>_div').style.left=x;
			</script>
			<?php			
			}
			return ob_get_clean();
		}
	}
?>
