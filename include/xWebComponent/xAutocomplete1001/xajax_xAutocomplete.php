<?php
	function loadAutocomplete($id, $search, $data=''){
		$xres=new xajaxResponse();					
		if($search!=''){
			define('ADODB_ASSOC_CASE', 1);
			require_once('../include/adodb/adodb.inc.php');
			require_once("../include/xWebComponent/database/DBConnection.php");					
			//define('ADODB_ASSOC_CASE', 1); 	
			if($data==''){			
				require_once("../include/xWebComponent/database/data.php");
			}else{
				$data=explode("-", $data);
				$dbdriver=$data[0];
				$server=$data[1];
				$user=$data[2];
				$password=$data[3];
				$database=$data[4];		
			}			
			
			require("../include/xWebComponent/xAutocomplete/xAutocomplete.class.php");			
			$con=new DBConnection($dbdriver, $server, $user, $password, $database);				
			$xAutocomplete=unserialize($_SESSION["$id"]);			
			$xAutocomplete->conex=$con;			
			$xres->addAssign($id."_div","innerHTML", $xAutocomplete->getSearch(utf8_decode($search)));
			$xres->addAssign($id."_div","style.display", '');
		}else{
			$xres->addAssign($id."_div","innerHTML", "");
		}	
		return utf8_encode($xres->getXML());
	}

?>
