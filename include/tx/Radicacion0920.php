<?php 
class Radicacion
{
  /** Aggregations: */

  /** Compositions: */

   /*** Attributes: ***/
	 /**
   * Clase que maneja los Historicos de los documentos
   *
   * @param int Dependencia Dependencia de Territorial que Anula
   * @db Objeto conexion
   * @access public
   */

	/**
	  *  VARIABLES DE DATOS PARA LOS RADICADOS
		*/
	var $db;
	var $tipRad;
	var $radiTipoDeri;
	var $nivelRad;
	var $radiCuentai;
	var $eespCodi;
	var $mrecCodi;
	var $radiFechOfic;
	var $radiNumeDeri;
	var $tdidCodi;
	var $descAnex;
	var $radiNumeHoja;
	var $radiPais;
	var $raAsun;
	var $radiDepeRadi;
	var $radiUsuaActu;
	var $radiDepeActu;
	var $carpCodi;
	var $radiNumeRadi;
	var $trteCodi;
	var $radiNumeIden;
	var $radiFechRadi;
	var $sgd_apli_codi;
	var $tdocCodi;
   var	$nofolios;
   var	$noanexos;
	var $estaCodi;
	var $radiPath;
	var $nguia;
	var $tsopt;
	var $urgnt;
	var $dptcn;
	var $depe_codi;
	/**
	  *  VARIABLES DEL USUARIO ACTUAL
		*/
	var $dependencia;
	var $usuaDoc;
	var $usuaLogin;
	var $usuaCodi;
	var $codiNivel=1;
	var $noDigitosRad;

 function Radicacion($db)
 {

	/**
  * Constructor de la clase Historico
	* @db variable en la cual se recibe el cursor sobre el cual se esta trabajando.
	*
	*/
	global $HTTP_SERVER_VARS,$PHP_SELF,$HTTP_SESSION_VARS,$HTTP_GET_VARS,$krd;
	//global $HTTP_GET_VARS;
	$this->db=$db;

	  $this->noDigitosRad = 6;
		$curr_page = $id.'_curr_page';
		$this->dependencia= $_SESSION['dependencia'];
		$this->usuaDoc    = $_SESSION['usua_doc'];
		$this->depe_codi = $_SESSION['dependencia'];
		//$this->usuaDoc    =$_SESSION['nivelus'];
		$this->usuaLogin  = $krd;
		$this->usuaCodi   = $_SESSION['codusuario'];
		isset($_GET['nivelus']) ? $this->codiNivel = $_GET['nivelus'] : $this->codiNivel = $_SESSION['nivelus'];
		if($this->codiNivel==''){
			$this->codiNivel=1;
		}
 }
function newRadicado($tpRad, $tpDepeRad)
{
	/** FUNCION QUE INSERTA UN RADICADO NUEVO
		*
		*/

/**
	* Busca el Nivel de Base de datos.
	*
	*/
		$whereNivel = "";
		$sql = "SELECT CODI_NIVEL FROM USUARIO WHERE USUA_CODI = ".$this->radiUsuaActu." AND DEPE_CODI=".$this->radiDepeActu;
		# Busca el usuairo Origen para luego traer sus datos.
		$rs = $this->db->conn->query($sql); # Ejecuta la busqueda
		$usNivel = $rs->fields["CODI_NIVEL"];
		# Busca el usuairo Origen para luego traer sus datos.
		$SecName = "SECR_TP$tpRad"."_".$tpDepeRad;
		$secNew=$this->db->conn->nextId($SecName);
		if($secNew==0)
		{
			$this->db->conn->RollbackTrans();
			$secNew=$this->db->conn->nextId($SecName);
			if($secNew==0) die("<hr><b><font color=red><center>Error no genero un Numero de Secuencia<br>SQL: $secNew</center></font></b><hr>");
		}
		$newRadicado = date("Y") . $this->dependencia . str_pad($secNew,$this->noDigitosRad,"0", STR_PAD_LEFT) . $tpRad;
		if(!$this->radiTipoDeri)
		{
		    $recordR["RADI_TIPO_DERI"]= "0";
		}
		else
		{
			$recordR["RADI_TIPO_DERI"]= $this->radiTipoDeri;
		}
		if(!$this->carpCodi) $this->carpCodi = 0;
		if(!$this->radiNumeDeri) $this->radiNumeDeri = 0;
		$recordR["SGD_SPUB_CODIGO"] =  $this->nivelRad;
		$recordR["RADI_CUENTAI"] =  $this->radiCuentai;
		$recordR["EESP_CODI"]    =	$this->eespCodi?$this->eespCodi:0;
		$recordR["MREC_CODI"]    =	$this->mrecCodi;
		$recordR["RADI_FECH_OFIC"]=	$this->db->conn->DBDate($this->radiFechOfic);
		//$recordR["radi_tipo_deri"]=$this->radiTipoDeri;
		$recordR["RADI_NUME_DERI"]=	$this->radiNumeDeri;
		$recordR["RADI_USUA_RADI"]=	$this->usuaCodi;
		$recordR["RADI_PAIS"]    =	"'".$this->radiPais."'";
		/*
		$recordR["RA_ASUN"]="'".$this->raAsun."'";
		$recordR["radi_desc_anex"]="'".$this->descAnex."'";
		*/
		$recordR["RA_ASUN"]			= $this->db->conn->qstr($this->raAsun);
		$recordR["RADI_DESC_ANEX"]	= $this->db->conn->qstr($this->descAnex);
		$recordR["RADI_DEPE_RADI"]= $this->radiDepeRadi;
		$recordR["RADI_USUA_ACTU"]=$this->radiUsuaActu;
		//$recordR["DEPE_CODI"]=$this->depe_codi;
		$recordR["CARP_CODI"]=$this->carpCodi;
		$recordR["CARP_PER"]=0;
		$recordR["RADI_NUME_RADI"]=$newRadicado;
		$recordR["TRTE_CODI"]=$this->trteCodi;
		$recordR["RADI_FECH_RADI"]=$this->db->conn->OffsetDate(0,$this->db->conn->sysTimeStamp);
		$recordR["RADI_DEPE_ACTU"]=$this->radiDepeActu;
		$recordR["TDOC_CODI"]=$this->tdocCodi;
		$recordR["TDID_CODI"]=$this->tdidCodi;
		//$recordR["CODI_NIVEL"]=$usNivel;
		$recordR["CODI_NIVEL"]=$_SESSION['nivelus'];
		if($recordR["CODI_NIVEL"]==""){
			$recordR["CODI_NIVEL"]=1;
		}
		$recordR["SGD_APLI_CODI"]=$this->sgd_apli_codi;
		$recordR["RADI_PATH"] = "$this->radiPath";		
		/*
		 * Codigo de verificación
		 */
		$recordR["SGD_RAD_CODIGOVERIFICACION"] = "'" . substr(sha1(microtime()), 0 , 5) . "'";
		$recordR["depe_codi"]       = $this->dependencia;
		$recordR["sgd_trad_codigo"] = $tpRad;
		
		if(!empty($this->nofolios)){
			      $recordR["RADI_NUME_FOLIO"] = $this->nofolios;
		  }

		  if(!empty($this->noanexos)){
			      $recordR["RADI_NUME_ANEXO"] = $this->noanexos;
		  }
		$whereNivel = "";	
		$insertSQL = $this->db->insert("RADICADO", $recordR, "true");
		//$insertSQL = $this->db->conn->Replace("RADICADO", $recordR, "RADI_NUME_RADI", false);
		if(!$insertSQL)
		{
			echo "<hr><b><font color=red>Error no se inserto sobre radicado<br>SQL: ".$this->db->querySql."</font></b><hr>";
		}
		//$this->db->conn->CommitTrans();
		return $newRadicado;
  }

  function updateRadicado($radicado, $radPathUpdate = null)
  {
		$recordR["SGD_SPUB_CODIGO"] =  $this->nivelRad;
		$recordR["radi_cuentai"] = $this->radiCuentai;
		if (!$recordR["radi_cuentai"]){
			$recordR["radi_cuentai"]= "'".''."'";
		}
		$recordR["eesp_codi"] 	= $this->eespCodi;
		$recordR["mrec_codi"] 	= $this->mrecCodi;
		$recordR["radi_fech_ofic"] = $this->db->conn->DBDate($this->radiFechOfic);
		$recordR["radi_pais"]     = "'".$this->radiPais."'";
		$recordR["ra_asun"]       = $this->db->conn->qstr($this->raAsun);
		$recordR["radi_desc_anex"]= $this->db->conn->qstr($this->descAnex);
		$recordR["trte_codi"]	= $this->trteCodi;
		$recordR["tdid_codi"]	= $this->tdidCodi;
		$recordR["radi_nume_radi"] = $radicado;
		$recordR["SGD_APLI_CODI"] = $this->sgd_apli_codi;
		
		// Linea para realizar radicacion Web de archivos pdf
		if(!empty($radPathUpdate) && $radPathUpdate != ""){
			$archivoPath = explode(".", $radPathUpdate);
			// Sacando la extension del archivo
			$extension = array_pop($archivoPath);
			if($extension == "pdf"){
				$recordR["radi_path"] = "'" . $radPathUpdate . "'";
			}
		}
		$insertSQL = $this->db->conn->Replace("RADICADO", $recordR, "radi_nume_radi", false);
		return $insertSQL;
  }

    /** FUNCION DATOS DE UN RADICADO
    * Busca los datos de un radicado.
    * @param $radicado int Contiene el numero de radicado a Buscar
    * @return Arreglo con los datos del radicado
    * Fecha de creaci�n: 29-Agosto-2006
    * Creador: Supersolidaria
    * Fecha de modificaci�n:
    * Modificador:
    */
    function getDatosRad( $radicado )
    {
        $query  = 'SELECT RAD.RADI_FECH_RADI, RAD.RADI_PATH, TPR.SGD_TPR_DESCRIP,';
        $query .= ' RAD.RA_ASUN,';
		$query .= ' RAD.RADI_NUME_IDEN';
        $query .= ' FROM RADICADO RAD';
        $query .= ' LEFT JOIN SGD_TPR_TPDCUMENTO TPR ON TPR.SGD_TPR_CODIGO = RAD.TDOC_CODI';
        $query .= ' WHERE RAD.RADI_NUME_RADI = '.$radicado;
        // print $query;
        $rs = $this->db->conn->query( $query );
        
        $arrDatosRad['fechaRadicacion'] = $rs->fields['RADI_FECH_RADI'];
        $arrDatosRad['ruta'] = $rs->fields['RADI_PATH'];
        $arrDatosRad['tipoDocumento'] = $rs->fields['SGD_TPR_DESCRIP'];
        $arrDatosRad['asunto'] = $rs->fields['RA_ASUN'];
		$arrDatosRad['validadoresEst'] = $rs->fields['RADI_NUME_IDEN'];
        return $arrDatosRad;
    }




	function insertDireccion($radiNumeRadi, $dirTipo,$tipoAccion){
      if($tipoAccion==0) {
       $nextval = $this->db->conn->nextId("sec_dir_direcciones");
       $this->dirCodigo = $nextval;
      }
      $this->dirTipo = $dirTipo;
      $record = array();
      if($this->trdCodigo) $record['SGD_TRD_CODIGO'] = $this->trdCodigo;
      if($this->grbNombresUs) $record['SGD_DIR_NOMREMDES'] = $this->grbNombresUs;
      if($this->ccDocumento) $record['SGD_DIR_DOC']    = $this->ccDocumento;
      if($this->muniCodi) $record['MUNI_CODI']      = $this->muniCodi;
      if($this->dpto_tmp1) $record['DPTO_CODI']      = $this->dpto_tmp1;
      if($this->idPais) $record['ID_PAIS']        = $this->idPais;
      if($this->idCont) $record['ID_CONT']        = $this->idCont;
      if($this->funCodigo) $record['SGD_DOC_FUN']    = $this->funCodigo;
      if($this->oemCodigo) $record['SGD_OEM_CODIGO'] = $this->oemCodigo;
      if($this->ciuCodigo)$record['SGD_CIU_CODIGO'] = $this->ciuCodigo;
      if($this->espCodigo) $record['SGD_ESP_CODI']   = $this->espCodigo;
      $record['RADI_NUME_RADI'] = $radiNumeRadi;
      //$record['SGD_SEC_CODIGO'] = 0;
      if($this->direccion) $record['SGD_DIR_DIRECCION'] = $this->direccion;
      if($this->dirTelefono) $record['SGD_DIR_TELEFONO'] = $this->dirTelefono;
      if($this->dirMail) $record['SGD_DIR_MAIL']   = $this->dirMail;
      if($this->dirTipo and $tipoAccion==0) $record['SGD_DIR_TIPO']   = $this->dirTipo;
      if($this->dirCodigo) $record['SGD_DIR_CODIGO'] = $this->dirCodigo;
      if($this->dirNombre) $record['SGD_DIR_NOMBRE'] = $this->dirNombre;
	  
      $ADODB_COUNTRECS = true;
      //$insertSQL = $this->db->insert("SGD_DIR_DRECCIONES", $record, "true");
	  if($tipoAccion==0){
      $insertSQL = $this->db->conn->Replace("SGD_DIR_DRECCIONES",
                                                $record,
                                                array('RADI_NUME_RADI','SGD_DIR_TIPO'),
                                                $autoquote = true);
												$insertSQL = "ddddddddd ddccccwww ";
	  }else{
	  	$recordWhere['RADI_NUME_RADI'] = $radiNumeRadi;	
		$recordWhere['SGD_DIR_TIPO']   = $dirTipo;	
		$insertSQL = $this->db->update("SGD_DIR_DRECCIONES",
                                                $record,
                                                $recordWhere);
	  }
	  
      if(!$insertSQL) {
			  $this->errorNewRadicado .= "<hr><b><font color=red>Error no se inserto sobre sgd_dir_drecciones<br>SQL:". $this->db->querySql .">> $insertSQL </font></b><hr>";
			  $insertSQL =-1;
		  }else{
			  $this->errorNewRadicado .= "<hr><b><font color=green>0: Ok </font></b><hr>";
			  $insertSQL =1;
		  }
		  
      return $insertSQL;
    } 
	
  function getRadImpresos($radicado)
  {
	$sqlImp = "SELECT A.RADI_NUME_SALIDA
                   FROM ANEXOS A, RADICADO R
                   WHERE A.ANEX_RADI_NUME=R.RADI_NUME_RADI
                   AND ( A.ANEX_ESTADO=3 OR A.ANEX_ESTADO=4 )
                   AND R.RADI_NUME_RADI = ".$radicado;
    // print $sqlImp;
	$rsImp = $this->db->conn->query( $sqlImp );
    
	if ( $rsImp->EOF )
        {
	   $arrAnexos[0] = 0;
	}
           else
           {
             $e = 0;
             while( $rsImp && !$rsImp->EOF )
             {
                $arrAnexos[ $e ] = $rsImp->fields['RADI_NUME_SALIDA'];
                $e++;
                $rsImp->MoveNext();
             }
	  }
	return $arrAnexos;
  }
  
} // Fin de Class Radicacion
?>
