<?php
/* Modificado para nueva forma de asignacion y creacion de expedientes:
      * 2007 	YULLIE QUICANO 		yquicano@cra.gov.co	CRA
  * 2010 	IDELBER SÁNCHEZ 	idelbers@gmail.com 	Openous
      */
session_start();
/* Modificado para nueva forma de asignacion y creacion de expedientes
 * 2007 	YULLIE QUICANO 		yquicano@cra.gov.co	CRA
 * 2014 	IDELBER SANCHEZ 	idelbers@gmail.com 	Openous
 */
foreach ($_GET as $key => $valor)   ${$key} = $valor;
foreach ($_POST as $key => $valor)   ${$key} = $valor;

$krd = $_SESSION["krd"];
$krdold = $krd;
$dependencia = $_SESSION["dependencia"];
//$usua_doc = $_SESSION["usua_doc"];
$codusuario = $_SESSION["codusuario"];
$digitosDependencia=$_SESSION["digitosDependencia"];
$depesize=$digitosDependencia;
$ruta_raiz = "..";

$modold=$_GET['mod'];
$where = null;
if(!$modo)$modo = $modold;
//include "$ruta_raiz/rec_session.php";
include_once("../include/db/ConnectionHandler.php");
$db = new ConnectionHandler("$ruta_raiz");
//	$db->conn->debug = true;
//include_once ("../common.php");
$campoBuscar  = (isset($_POST['campoBuscar'])) ? $_POST['campoBuscar'] : null;
//$campoBuscar  = strtoupper(trim($campoBuscar));
$campoBuscar  = (trim($campoBuscar));
$params       = session_name()."=".session_id()."&krd=$krd"."&mod=$modo";
$fecha_hoy    = Date("Y-m-d");
$sqlFechaHoy  = $db->conn->DBDate($fecha_hoy);
$subStr       = $db->conn->substr;
$formatoFechaExp  = $db->conn->SQLDate('Y/m/d', 's.sgd_sexp_fech');
$depesize="3";

?>
<!--DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Consulta Expedientes Existentes</title>
	<link href="../estilos/orfeo.css" rel="stylesheet" type="text/css">


	<script language="JavaScript" type="text/JavaScript">
		function enviar(argumento) {
			document.formSeleccion.action=argumento+"&"+document.formSeleccion.params.value;
			document.formSeleccion.submit();
		}
		function pasar_info(num_exp){

			opener.document.insExp.numeroExpediente.value=num_exp;
			window.close();
		}
		function detail(num_exp){

			nombreventana="DETALLES_EXPEDIENTE";
			url="&num_exp=";
			window.open('detalles_exp.php?&krd=<?=$krd?>&codusuario=<?=$codusuario?>&dependencia=<?=$dependencia?>&carpeAnt=<?=$carpeta?>&verrad=<?=$verrad?>&s_Listado=VerListado&fechah=<?=$fecha_hoy?>&mostrar_opc_envio=<?=$mostrar_opc_envio?>&nomcarpeta=<?=$nomcarpeta?>&datoVer=<?=$datoVer?>&leido=<?=$leido?>&num_exp='+num_exp,nombreventana+num_exp,'height=1000,width=1300,scrollbars=yes');
			return;
		}
		function alerta(){


			alert(this.document.getElementById('desc_exp').value)
		}
	</script>
</head>
<body>
<link rel="stylesheet" href="../estilos/orfeo.css">


<script language="JavaScript" type="text/JavaScript">
	// Envia el formulario de acuerdo a la opcion seleccionada, que puede ser ver CSV o consultar
	function enviar(argumento) {
		document.formSeleccion.action=argumento+"&"+document.formSeleccion.params.value;
		document.formSeleccion.submit();
	}
</script>
<form action="conExp.php?<?=$params?>" method="post" enctype="multipart/form-data" name="formSeleccion" id="formSeleccion">
	<table  border="0" align="center"  width="100%" class="borde_tab">
		<tr>
			<td class="titulos4" align="center" height="15"  colspan="2">
				CONSULTA EXPEDIENTES EXISTENTES</td>
		</tr>
	</table>
	<table class="borde_tab" width="100%" align="center">
		<tr align = "left">
			<?php

			if($_POST['COD_DEP']!=''){
				$CODIGO=$_POST['COD_DEP'];
			}else{
				$CODIGO=$dependencia;
			}

			?>
			<td align = "left" class= "titulos2" colspan="3" ><br>DEPENDENCIA:&nbsp;&nbsp; <!--/td-->
				<!--td align = "left" class="listado2"--><select class= "select" id="COD_DEP" name="COD_DEP" onchange="submit();">
					<OPTION VALUE="0">-- Seleccione Dependencia --</OPTION>
					<?

					// SELECT QUE BUSCA LAS DEPENDENCIAS QUE TIENEN EXPEDIENTES CREADOS
					$sql = "SELECT DISTINCT
					D.DEPE_CODI AS COD_DEP,
					D.DEPE_NOMB AS NOM_DEP
					FROM    DEPENDENCIA D,
						SGD_SEXP_SECEXPEDIENTES S	
					WHERE   
						(substr(S.SGD_EXP_NUMERO,5,3)=cast(D.DEPE_CODI as text)
						OR S.DEPE_CODI = D.DEPE_CODI)
						/* and	substr(S.SGD_EXP_NUMERO,5,$depesize)<> 998 */ 
						order by d.depe_codi
						";
					//echo $sql;
					$rssq = $db->conn->Execute($sql);
					// para que no salgan las de prueba
					//
					if(count($rssq)>0){
						while(!$rssq->EOF){
							?>
							<option value="<?php echo $rssq->fields['COD_DEP'] ?>"
								<?php if($rssq->fields['COD_DEP']==$CODIGO){
									?>
									selected="selected" <?php } ?>><?php echo $rssq->fields['COD_DEP'], ' - ',$rssq->fields['NOM_DEP']  ?></option>
							<?php
							$rssq->moveNext();
						}
					}
					?>
				</select><br><br></td>

		</tr>
		<!--tr class="select" >
            <td class="titulos2">Serie</td>
            <td class="titulos2">Subserie</td>
        </tr-->
		<tr class="listado2">
			<td class="titulos2"><?php

				$sql = "Select Srd.Sgd_Srd_Descrip, Srd.Sgd_Srd_Codigo
				  From Sgd_Srd_Seriesrd Srd
				Where Srd.Sgd_Srd_Codigo in 
					(select distinct(Sexp.Sgd_Srd_Codigo) 
					from Sgd_Sexp_Secexpedientes sexp 
					 where SUBSTR(SEXP.SGD_EXP_NUMERO, 5, $depesize) = '$CODIGO')	
				ORDER BY SRD.SGD_SRD_DESCRIP";

				/*			$sql = "SELECT DISTINCT Srd.SGD_SRD_CODIGO ||'.'|| Srd.SGD_SRD_DESCRIP, Srd.SGD_SRD_CODIGO
                                    FROM Sgd_Srd_Seriesrd Srd
                                    INNER JOIN SGD_SEXP_SECEXPEDIENTES SEXP
                                    ON Srd.SGD_SRD_CODIGO = SEXP.SGD_SRD_CODIGO
                                    WHERE
                                        $subStr(sexp.sgd_exp_numero,5,$depesize)=$CODIGO
                                    ORDER BY SRD.SGD_SRD_DESCRIP";*/
				$rssrd = $db->conn->Execute($sql);

				if(!$s_SGD_SRD_CODIGO) $s_SGD_SRD_CODIGO = 0;
				echo "Serie: ".$rssrd->GetMenu2("tsrd",$tsrd,"0:-- Seleccione --",false,"","onChange='submit()' class='select'");
				echo '</td>' . "";
				echo '<td class="titulos2">' . "";
				//echo $rssd;
				include ("$ruta_raiz/include/query/trd/queryCodiDetalle.php");
				//echo $sql;
				/*	$querySub = "Select SU.SGD_SBRD_CODIGO ||'.'|| Su.Sgd_Sbrd_Descrip, SU.SGD_SBRD_CODIGO
                            From Sgd_Sbrd_Subserierd Su
                            WHERE SU.SGD_SRD_CODIGO = '$tsrd' AND
                            $sqlFechaHoy BETWEEN SU.SGD_SBRD_FECHINI AND SU.SGD_SBRD_FECHFIN";
                */
				$querySub = "SELECT Su.SGD_SBRD_DESCRIP,
			  Su.SGD_SBRD_CODIGO
			From Sgd_Sbrd_Subserierd Su
			Where Su.SGD_SBRD_CODIGO in 
					(select distinct(Sexp.Sgd_Sbrd_Codigo) 
					    from Sgd_Sexp_Secexpedientes sexp 
								 where SUBSTR(SEXP.SGD_EXP_NUMERO, 5, $depesize) = '$CODIGO' 
					    and sexp.SGD_SRD_CODIGO = '$tsrd')
			And SGD_SRD_CODIGO = '$tsrd'
			";
				//echo $querySub;
				$rsSub = $db->conn->Execute($querySub);
				include "$ruta_raiz/include/tx/ComentarioTx.php";
				echo "Suberie: ".$rsSub->GetMenu2("tsub", $tsub, "0:-- Seleccione --", false,"","onChange='submit()' class='select'" );
				echo '</td>' . "";
				//						echo '</tr>' . "";
				echo '<td class="titulos2">' . "";

				//< Inicio filtro por anio:
				$queryAnio = "
			SELECT DISTINCT Sexp.SGD_SEXP_ANO
			FROM Sgd_Sexp_Secexpedientes Sexp
			WHERE SUBSTR(Sexp.SGD_EXP_NUMERO, 5, $depesize) = '$CODIGO'
			AND Sexp.SGD_SRD_CODIGO                 = '$tsrd'
			AND Sexp.SGD_SBRD_CODIGO                = '$tsub' 
			ORDER BY Sexp.SGD_SEXP_ANO desc
			";
				// UNION SELECT CAST('0000' AS NUMBER (4,0)) AS ANO FROM DUAL
				/*SELECT Sexp.Sgd_Sexp_Ano
                From Sgd_Sbrd_Subserierd Su
                Where Su.SGD_SBRD_CODIGO in
                        (select distinct(Sexp.Sgd_Sbrd_Codigo) from Sgd_Sexp_Secexpedientes sexp where SUBSTR(SEXP.SGD_EXP_NUMERO, 5, $depesize) = $CODIGO)
                And SGD_SRD_CODIGO = '$tsrd'
                And $sqlFechaHoy BETWEEN SU.SGD_SBRD_FECHINI AND SU.SGD_SBRD_FECHFIN*/
				//echo $queryAnio;
				$rsAnio = $db->conn->Execute($queryAnio);
				//$maxAnio= $rsAnio->fields['SGD_SEXP_ANO'];
				//print_r($maxAnio);
				//$valor = "$maxAnio";
				//$itemBlanco = "$maxAnio";
				$valor = "%";
				$itemBlanco = " Todos los Años -- ";
				echo "<b>&nbsp;&nbsp;Año creación: </b>&nbsp;&nbsp;&nbsp;&nbsp;".$rsAnio->GetMenu2("tAnio", $tAnio, $blank1stItem = "$valor:$itemBlanco", false,0,"onChange='submit()' class='select'" );
				echo '</td>' . "\n";
				//Fin filtro anio >
				$sqlFechaExp = $db->conn->SQLDate('Y-m-d H:i', 's.SGD_SEXP_FECH');
				?> </td>
		</tr>
	</table>

		<!--	BUSCAR POR TEXTO DE ETIQUETAS  -->
		<table  border="1" align="center"  width="100%" >
			<tr align="center">
				<td class="listado2" align="center">
					<label align="center"> Buscar en el listado: </label>
					<input type="text" name="campoBuscar" value="<?php echo $campoBuscar?>"
					class="tex_area"  size="40" align="right"><!--Oprima Enter--><input type="submit" class="botones" value="Buscar">
				</td>
			</tr>
		</table>


		<!--td class="listado2">
			</td>
		<tr>
			<td class="titulos2">PALABRAS CLAVES (separadas por coma)</td>
			<td class="titulos2"> <input type="text" size="40" maxlength="50" id="desc_exp" name="desc_exp" value=<?$campoBuscar?>></td>
			<td><input type=button value='Buscar ' id=Buscar name=Buscar valign='middle' class='botones' onClick="alerta();"></td>
		</tr-->

		<?php
		// Con esto sirve para cualquier base de datos
		$camposConcatenar = "(" . $db->conn->Concat("s.sgd_sexp_parexp1","' / '",
				"s.sgd_sexp_parexp2","' / '",
				"s.sgd_sexp_parexp3","' '",
				"s.sgd_sexp_parexp4","' '",
				"s.sgd_sexp_parexp5") . ")";

		$where = (!empty($campoBuscar) && $campoBuscar != "") ?
			"AND (upper(s.sgd_sexp_parexp1) LIKE upper('%$campoBuscar%') OR
                        upper(s.sgd_sexp_parexp2) LIKE upper('%$campoBuscar%') OR
                        upper(s.sgd_sexp_parexp3) LIKE upper('%$campoBuscar%') OR
                        upper(s.sgd_sexp_parexp4) LIKE upper('%$campoBuscar%') OR
                        upper(s.sgd_sexp_parexp5) LIKE upper('%$campoBuscar%'))"
			:"";

		/*			$sql3 = "SELECT distinct s.sgd_exp_numero,
                            s.depe_codi,
                            d.depe_codi,
                            d.depe_nomb,
                            S.SGD_SEXP_FECH,
                            u.usua_nomb,
                            se.sgd_srd_codigo as CSERIE,
                            se.sgd_srd_descrip as SERIE,
                            su.sgd_sbrd_codigo as CSUBSERIE,
                            su.sgd_sbrd_descrip as SUBSERIE,
                    s.SGD_SEXP_ANO AS ANO,
                            $camposConcatenar as PARAMETRO
                    FROM
                            sgd_sexp_secexpedientes s,
                            dependencia d,
                            sgd_srd_seriesrd se,
                            sgd_sbrd_subserierd su ,
                            usuario u
                    WHERE   s.sgd_srd_codigo = '$tsrd' and
                            se.sgd_srd_codigo = '$tsrd' and
                            s.sgd_sbrd_codigo = su.sgd_sbrd_codigo and
                            s.sgd_srd_codigo  = su.sgd_srd_codigo and
                            su.sgd_sbrd_codigo= '$tsub' and
                            s.depe_codi = d.depe_codi and
                            u.usua_doc = s.usua_doc and
                    S.SGD_SEXP_ESTADO !=9 AND
                            $subStr(s.sgd_exp_numero,5,3) =$CODIGO
                            {$where}
                order by S.SGD_SEXP_FECH desc";
        */
		$exp1="<Sin Titulo>";
		$exp1b="<br>";
		$sql3 = "SELECT DISTINCT s.SGD_EXP_NUMERO, 
				  e.Sgd_Exp_Numero AS SIN,
				  s.DEPE_CODI,
				  d.DEPE_NOMB,
					  {$sqlFechaExp} 				AS FECHA_EXPEDIENTE,
					  s.sgd_sexp_fech,
				  u.USUA_NOMB,
				  se.SGD_SRD_CODIGO   AS CSERIE,
				  se.SGD_SRD_DESCRIP  AS SERIE,
				  su.SGD_SBRD_CODIGO  AS CSUBSERIE,
				  su.SGD_SBRD_DESCRIP AS Subserie,
				  s.SGD_SEXP_ANO      AS ANO,
						COALESCE(s.sgd_sexp_parexp1, '$exp1') || '$exp1b' ||
					COALESCE(s.sgd_sexp_parexp2, '') || '$exp1b' ||
					COALESCE(s.sgd_sexp_parexp3, '') || '$exp1b' ||
					COALESCE(s.sgd_sexp_parexp4, '') || COALESCE(s.sgd_sexp_parexp5, '') 
								AS Parametro,
				  s.SGD_SEXP_ESTADO AS Estado
				FROM sgd_sexp_secexpedientes s
				INNER JOIN dependencia d
				ON s.DEPE_CODI = d.DEPE_CODI
				INNER JOIN sgd_sbrd_subserierd su
				ON s.SGD_SRD_CODIGO   = su.SGD_SRD_CODIGO
				AND s.SGD_SBRD_CODIGO = su.SGD_SBRD_CODIGO
				INNER JOIN sgd_srd_seriesrd se
				ON se.SGD_SRD_CODIGO = su.SGD_SRD_CODIGO
				INNER JOIN usuario u
				On S.Usua_Doc = U.Usua_Doc
				LEFT JOIN Sgd_Exp_Expediente E
				ON s.SGD_EXP_NUMERO                = E.SGD_EXP_NUMERO
				WHERE se.SGD_SRD_CODIGO            = '$tsrd'
				AND su.SGD_SBRD_CODIGO             = '$tsub'
				AND s.SGD_SRD_CODIGO               = '$tsrd'
					AND cast(s.SGD_SEXP_ANO as text)				like '$tAnio'
					AND (s.SGD_SEXP_ESTADO != '9' OR s.SGD_SEXP_ESTADO is null  )
					And Substr(S.Sgd_Exp_Numero, 5, '$depesize') = '$CODIGO'
				{$where}
		order by s.SGD_SEXP_ANO desc, s.sgd_sexp_fech desc, s.SGD_EXP_NUMERO desc";
	//	$db->conn->debug = true;
		$rs3 = $db->conn->Execute($sql3);

		?>
	</table>
	<table align="center" class="borde_tab" width="100%">
		<tr>
			<td class="titulos4">Existen los siguientes expedientes:</td>
		</tr>
		<table border="0" align="center" class="borde_tab" width="100%">
			<tr class= "select">

				<td class="titulos2" align="center">Expediente</td>
				<td class="titulos2" align="center">Creado</td>
				<td class="titulos2" align="center">Dependencia</td>
				<!--td class="titulos2" align="center">Usuario Creador(a)</td-->
				<td class="titulos2" align="center">TRD</td>
				<td class="titulos2" align="center">Estado Exp.</td>
				<td class="titulos2" align="center">Año</td>
				<td class="titulos2" align="center">T&iacute;tulo y Descripci&oacute;n</td>
				<td class="titulos2" align="center">Detalles</td>

			</tr>
			<?php

			while(!$rs3->EOF){
			$exp            = $rs3->fields['SGD_EXP_NUMERO'];
			$depe_nomb2      = $rs3->fields['DEPE_NOMB'];
			$par            = $rs3->fields['PARAMETRO'];
			$fechaExp       = $rs3->fields['FECHA_EXPEDIENTE'];
			$usuarioc		= $rs3->fields['USUA_NOMB'];
			$serie_exp		= $rs3->fields['CSERIE'];
			$serie_exp.=".";
			$serie_exp.= $rs3->fields['CSUBSERIE'];
			$ano_exp= $rs3->fields['ANO'];
			$exp_sin_rad= $rs3->fields['SIN'];
			if ($rs3->fields['ESTADO']=="0") {
				$estadoExp="Abierto"; } else {
				$estadoExp="<label style=color:gray>Cerrado</label>"; }
			/*$linkInfGeneral = "<a class='vinculos' href='../verradicado.php?verrad=$fldRADI_NUME_RADI&".session_name()."=".session_id()."&krd=$krd&carpeta=8&nomcarpeta=Busquedas&tipo_carp=0'>"; //No se puede por ahora, se neceista 1 radicado */
			$parUrl = str_replace("@", "_arroba_", substr($par,0,100));
			?>

			<tr class="listado2">
				<!--td class="info" align="center"-->
				<?php /*
			$sqlER= "select count(radi_nume_radi) as RADIC from SGD_EXP_EXPEDIENTE where SGD_EXP_NUMERO='$exp' and SGD_EXP_ESTADO!=2";
			$rsC=$db->conn->Execute($sqlER);
			$radC=$rsC->fields['RADIC'];
			//echo "Radicados: ".$radC."";
			*/?>
				<!--/td-->
				<td class="info" align="center">
					<?php /*if ($radC==0) { echo $exp; } else { */ ?>
					<?php if ($exp_sin_rad=="") { echo $exp; } else { ?>
					<a class="vinculos" href="javascript:detail('<?echo $exp?>&par=<?=$parUrl?>&usuarioc=<?=$usuarioc?>')"><?=$exp?></td><?}?>
				<td class="info" align="center"><?=$fechaExp?></td>

				<td class="info" align="center"><?php echo $depe_nomb2 ?></td>
				<!--td class="info" align="center"><?php echo $usuarioc ?></td-->
				<td class="info" align="center"><?php echo $serie_exp ?></td>
				<td class="info" align="center"><?php echo $estadoExp ?></td>
				<td class="info" align="center"><?php echo $ano_exp ?></td>
				<td class="info" align="left"  ><?php echo $par ?></td>

				<?  if($modo==1){
					if ($rs3->fields['ESTADO']=="0") {
						$SELECCIONAR="SELECCIONAR";
						echo "<td class='info' align='center'><a href=javascript:pasar_info('$exp')>$SELECCIONAR</td>";
					} else {
						$SELECCIONAR="<label class='info' style=color:gray>Cerrado </label>";
						echo "<td class='info' align='center'>". $SELECCIONAR."</td>";
						}
					?>

				<td class="info" align="center"><a class="vinculos"
				<?php if ($exp_sin_rad==0) { echo "align=center> Sin Radicados "; } else { ?>
				href="javascript:detail('<?echo $exp?>')"> VER_EXPEDIENTE </td>
				<?										}
				} else { ?>
					<?php //if ($radC==0) { echo "<td align=center> Sin Radicados </td>"; } else { ?>
					<?php if ($exp_sin_rad=="") { echo "<td align=center> Sin Radicados </td>"; } else { ?>
						<td class="info" align="center"><a class="vinculos"
														   href="javascript:detail('<?php echo $exp?>&par=<?=$parUrl?>&usuarioc=<?=$usuarioc?>')"> VER_EXPEDIENTE </td>

						<?php
					}
				}
				$rs3->MoveNext();}
				?>
			</tr>
		</table>

</body>
</table>
<?
$xsql=serialize($sql3);
$_SESSION['xheader']="<center><b>EXPEDIENTES EXISTENTES</b></center><br><br>";
$_SESSION['xsql']=$xsql;
?>
<table align="center" >
	<tr align="center" colspan="5">
		<td align="center" colspan ="5"><b><!--Generar:--></b></td>
		<td><?
			//var_dump($_GET);
			/*echo "<a href='$ruta_raiz/adodb/adodb-doc.inc.php' target='_blank'><img src='$ruta_raiz/adodb/compfile.png'> </a> - ";*/
			echo "<a href='$ruta_raiz/adodb/adodb-xls.inc.php' target='_blank'><img src='$ruta_raiz/adodb/spreadsheet.png' width='32%'> </a>";
			?></td>
	</tr>
</table>

</html>
