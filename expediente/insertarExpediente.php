<?php
session_start();
/** Modulo de Expedientes o Carpetas Virtuales
 * Modificacion Variables
 *@autor Jairo Losada 2009/06
 *Licencia GNU/GPL
 */
foreach ($_GET as $key => $valor)   ${$key} = $valor;
foreach ($_POST as $key => $valor)   ${$key} = $valor;
$ruta_raiz = "..";
$krd = $_SESSION["krd"];
$dependencia = $_SESSION["dependencia"];
$usua_doc = $_SESSION["usua_doc"];
$codusuario = $_SESSION["codusuario"];
$tip3Nombre=$_SESSION["tip3Nombre"];
$tip3desc = $_SESSION["tip3desc"];
$tip3img =$_SESSION["tip3img"];
$tpNumRad = $_SESSION["tpNumRad"];
$tpPerRad = $_SESSION["tpPerRad"];
$tpDescRad = $_SESSION["tpDescRad"];
$tip3Nombre = $_SESSION["tip3Nombre"];
$tpDepeRad = $_SESSION["tpDepeRad"];
$usuaPermExpediente = $_SESSION["usuaPermExpediente"];

$dir_doc_us1 = $_SESSION['dir_doc_us1'] ;
$dir_doc_us2 = $_SESSION['dir_doc_us2'] ;
$digitosDependencia = $_SESSION["digitosDependencia"];
$depesize=$digitosDependencia;

if ( !$nurad )
{
    $nurad = $rad;
}

include_once( "$ruta_raiz/include/db/ConnectionHandler.php" );
$db = new ConnectionHandler( "$ruta_raiz" );
include_once( "$ruta_raiz/include/tx/Historico.php" );
include_once( "$ruta_raiz/include/tx/Expediente.php" );

$encabezado = "$PHP_SELF?".session_name()."=".session_id()."&opcionExp=$opcionExp&numeroExpediente=$numeroExpediente&nurad=$nurad&coddepe=$coddepe&codusua=$codusua&depende=$depende&ent=$ent&tdoc=$tdoc&codiTRDModi=$codiTRDModi&codiTRDEli=$codiTRDEli&codserie=$codserie&tsub=$tsub&ind_ProcAnex=$ind_ProcAnexcodiTRDEli&radasun=$radasun";
$expediente = new Expediente( $db );
//$db->conn->debug=true;

// Inserta el radicado en el expediente
if( $funExpediente == "INSERT_EXP" )
{
    // Consulta si el radicado est� incluido en el expediente.
    $arrExpedientes = $expediente->expedientesRadicado( $nurad);
    /* Si el radicado esta incluido en el expediente digitado por el usuario.
     * != No identico no se puede poner !== por que la funcion array_search
     * tambien arroja 0 o "" vacio al ver que un expediente no se encuentra
     */
    $arrExpedientes[] = "1";
    foreach ( $arrExpedientes as $line_num => $line){
        if ($line == $_POST['numeroExpediente']) {
            print '<center><hr><font color="red">El radicado ya est&aacute; incluido en el expediente.</font><hr></center>';
        }else {
            $resultadoExp = $expediente->insertar_expediente( $_POST['numeroExpediente'], $_GET['nurad'], $dependencia, $codusuario, $usua_doc );
            if( $resultadoExp == 1 )
            {
                $observa = "Incluir radicado en Expediente";
                include_once "$ruta_raiz/include/tx/Historico.php";
                $radicados[] = $_GET['nurad'];
                $tipoTx = 53;
                $Historico = new Historico( $db );
                $Historico->insertarHistoricoExp( $_POST['numeroExpediente']
                    , $radicados
                    , $dependencia
                    , $codusuario
                    , $observa
                    , $tipoTx
                    , 0 );

                ?>
                <script language="JavaScript">
                    opener.regresar();
                    window.close();
                </script>
                <?php
            }
            else
            {
                print '<hr><font color=red>No se incluyó este radicado al expediente. Verifique si ya está incluido en este expediente o que el mismo exista e intente de nuevo.</font><hr>';
            }
        }
    }



}
?>
<html>
<head>
    <title>Incluir en Expediente</title>
    <link href="../estilos/orfeo.css" rel="stylesheet" type="text/css">
    <script language="JavaScript">
        function validarNumExpediente()
        {
            numExpediente = document.getElementById( 'numeroExpediente' ).value;

            // Valida que se haya digitado el nombre del expediente
            // a�o dependencia serie subserie consecutivo E
            if( numExpediente.length != 0 && numExpediente != "" )
            {
                insertarExpedienteVal = true;
            }
            else if( numExpediente.length == 0 || numExpediente == "" )
            {
                alert( "Error. Debe especificar el nombre de un expediente." );
                document.getElementById( 'numeroExpediente' ).focus();
                insertarExpedienteVal = false;
            }

            if( insertarExpedienteVal == true )
            {
                document.insExp.submit();
            }
        }

        function confirmaIncluir()
        {
            document.getElementById( 'funExpediente' ).value = "INSERT_EXP";
            document.insExp.submit();
        }

        function cambia_Exp(expId, expNo){
            var exp_id = document.getElementById(expId);
            numExp = exp_id.value;
            var exp_no = document.getElementById(expNo);
            exp_no.value = numExp;
            document.insExp.numeroExpediente.focus();
        }

        function Busqueda(){
            nombreventana="BusquedaAvanzada";
            url="conExp.php?&krd=<?=$krd?>&codusuario=<?=$codusuario?>&dependencia=<?=$dependencia?>&carpeAnt=<?=$carpeta?>&verrad=<?=$verrad?>&s_Listado=VerListado&fechah=$fechah&mostrar_opc_envio=<?=$mostrar_opc_envio?>&nomcarpeta=<?=$nomcarpeta?>&datoVer=<?=$datoVer?>&leido=<?=$leido?>>&mod=1";
            window.open(url,nombreventana,'height=900,width=1224,scrollbars=yes');
            return;
        }

    </script>
</head>
<body bgcolor="#FFFFFF" onLoad="document.insExp.numeroExpediente.focus();">
<table border=0 width=80% align="center" class="borde_tab" cellspacing="1" cellpadding="0">
    <tr align="center" class="titulos2">
        <td height="15" class="titulos2" colspan="2">INCLUIR EN EL EXPEDIENTE</td>
    </tr>
</table>
<table border=0 width=80% align="center" class="borde_tab">
    <form method="post" action="<?=$_SERVER['PHP_SELF']?>?nurad=<?=$nurad?>&radasun=<?=$_POST['radasun']?>" name="insExpBus">
        <table width="80%" border="0" cellspacing="1" cellpadding="0" align="center" class="borde_tab">

            <td width="13%" height="30" class="titulos5" align="center" >
                <center>
                    <input name="avanzada" type="button" class="botones_largo" onClick="Busqueda();"value=" Buscar Expediente " ><!--font color="#FF0000"> * </font-->
                </center>
            </td>

        </table>

    </form>

    <form method="post" action="<?php print $encabezado; ?>" name="insExp">
        <input type="hidden" name='funExpediente' id='funExpediente' value="" >
        <input type="hidden" name='confirmaIncluirExp' id='confirmaIncluirExp' value="" >

        <table border=0 width=80% align="center" class="borde_tab">
            <tr align="center">
                <td class="titulos5" align="center" nowrap>
                    Nombre del Expediente </td>
                <td class="titulos5" align="left">
                    <input type="text" name="numeroExpediente" id="numeroExpediente" value="<?php print $_POST['numeroExpediente']; ?>" maxlength="19" size="26" >
                </td>
            </tr>
        </table>
        <table border=0 width=80% align="center" class="borde_tab">
            <tr align="rigth">

                <td width="33%" height="25" class="listado2" align="right">
                    <center>
                        <input name="btnIncluirExp" type="button" class="botones_funcion" id="btnIncluirExp" onClick="validarNumExpediente();" value="Incluir en Exp">
                    </center></TD>
                <td width="33%" class="listado2" height="25">
                    <center><input name="btnCerrar" type="button" class="botones_funcion" id="btnCerrar" onClick="opener.regresar(); window.close();" value=" Cerrar ">
                    </center></TD>
            </tr>
        </table>
        <table border=0 width=80% align="center" class="borde_tab">
            <?
            // Consulta si existe o no el expediente.
            if ( $expediente->existeExpediente( $_POST['numeroExpediente'] ) !== 0 )
            {
            /*****************DATOS DEL EXP is *******************************************/
            //Ini: Obtener Datos del Expediente
            //Dependencia del Expediente o Subseccion:
            $depe_expediente = substr($numeroExpediente, 4, $depesize);
            $sqlD = "Select d.depe_nomb, d.DEPE_CODI_PADRE from Dependencia d
		where d.depe_codi = '$depe_expediente'";
            $rtD = $db->query($sqlD);
            if (!$rtD->EOF) {
                $depe_expediente_nomb = $rtD->fields["DEPE_NOMB"];
                $depe_expediente_padre = $rtD->fields["DEPE_CODI_PADRE"];
            }

            //Información del encabezado del radicado
            $sql = "SELECT sexp.SGD_SRD_CODIGO,
	  sr.SGD_SRD_DESCRIP,
	  sexp.SGD_SBRD_CODIGO,
	  sbsr.SGD_SBRD_DESCRIP
	FROM sgd_sexp_secexpedientes sexp
	INNER JOIN sgd_sbrd_subserierd sbsr ON sexp.SGD_SBRD_CODIGO = sbsr.sgd_sbrd_codigo and sexp.sgd_srd_codigo=sbsr.sgd_srd_codigo
	INNER JOIN sgd_srd_seriesrd sr ON sexp.SGD_SRD_CODIGO = sr.SGD_SRD_CODIGO
	WHERE sexp.SGD_EXP_NUMERO='$numeroExpediente'";
            $rt = $db->query($sql);
            if (!$rt->EOF) {
                $titSerie = $rt->fields["SGD_SRD_CODIGO"];
                $serie = $titSerie . " - " . $rt->fields["SGD_SRD_DESCRIP"];
                $subserie = $rt->fields["SGD_SBRD_CODIGO"] . " - " . $rt->fields["SGD_SBRD_DESCRIP"];
            } else {
                $serie = "";
                $subserie = "";
            }
            //información del expediente
            //echo "Hii: ".$numeroExpediente;
            $sql = "select sgd_sexp_parexp1,sgd_sexp_parexp2,sgd_sexp_parexp3,sgd_sexp_parexp4,sgd_sexp_parexp5,SGD_SEXP_ESTADO from sgd_sexp_secexpedientes
	where sgd_exp_numero = '$numeroExpediente'";
            $rt = $db->query($sql);
            if (!$rt->EOF) {
                $titulo = $rt->fields["SGD_SEXP_PAREXP1"];
                $descripcion = $rt->fields["SGD_SEXP_PAREXP2"];
                $otrosDesc = $rt->fields["SGD_SEXP_PAREXP3"] . " " . $rt->fields["SGD_SEXP_PAREXP4"] . " " . $rt->fields["SGD_SEXP_PAREXP5"];
//	    $estadoRadicado = ($rt->fields["SGD_SEXP_ESTADO"] == 0 || $rt->fields["SGD_EXP_ESTADO"] == 1 ) ? "Abierto" : "Cerrado";
                $estadoExpedienteAC = ($rt->fields["SGD_SEXP_ESTADO"] == 0 || $rt->fields["SGD_EXP_ESTADO"] == 1 ) ? "Abierto" : "Cerrado";
                $estadoRadicado=$estadoExpedienteAC;
            } else {
                $titulo = "";
                $descripcion = "";
                $otrosDesc = "";
            }
            // Fin php - Ini html : Datos del Expediente";
            ?>

            <!--table class="borde_tab">
                <tbody-->
            <tr class="titulos4">
                <td colspan="2" > <span style="font-weight: bold">Se incluirá el radicado:
                        <?php
                        echo "".$_GET['nurad']." :".$_GET['radasun']."<br>";
                        echo "Al Expediente No. ".$numeroExpediente ?></span></td>
            </tr>
            <tr align="left" class="titulos5">
                <td width=25%><label>Dependencia: </label> </td>
                <td width=75%><span style="font-weight: normal"> <?php echo "<b>".strtoupper($depe_expediente_nomb)."</b>" ?></span>
                </td>
            </tr>

            <tr class="titulos5">
                <td><label >Serie:</label></td>
                <td><?php echo $serie ?></td>
            </tr>
            <tr class="titulos5">
                <td><label >Subserie:</label></td>
                <td><?php echo $subserie ?></td>
            </tr>
            <tr class="titulos5">
                <td><label >Titulo:</label></td>
                <td><?php echo $titulo ?></td>
            </tr>
            <tr class="listado2">
                <td><label >Descripción:</label></td>
                <td><?php echo $descripcion ?></td>
            </tr>
            <tr class="listado2">
                <td><label >Otros datos:</label></td>
                <td><?php echo $otrosDesc ?></td>
            </tr>

            <tr class="listado2">
                <td> <label style="margin-right: 34px">Estado: </label>
                </td>
                <td> <?php
                    if ($estadoRadicado!="Abierto") {
                        echo "<span class=titulosError> 
			Expediente $estadoRadicado  <br>
			No se puede incluir más radicados </span> <br>
			Más información en Gestión Documental: ".$soporte_gestion_documental;

                    } else {
                    echo "Expediente  ".$estadoRadicado;

                    ?>
                </td>
            </tr>
            <!--/2tbody>
        </table-->
        </table>

        <!-- Fin Datos Exp -->

        <table border=0 width=80% align="center" class="borde_tab">
            <tr align="center">
                <td width="33%" height="25" class="listado2" align="center">
                    <center class="titulosError2">
                        ESTA SEGURO DE INCLUIR ESTE RADICADO EN EL EXPEDIENTE:
                    </center>
                    <B>
                        <center class="style1"><b><?php print $numeroExpediente; ?></b></center>
                    </B>
                    <div align="justify"><br>
                        <strong><b>Recuerde:</b>No podr&aacute; modificar el numero de expediente si hay
                            un error en el expediente, m&aacute;s adelante tendr&aacute; que excluir este radicado del
                            expediente y si es el caso solicitar la anulaci&oacute;n del mismo. Adem&aacute;s debe
                            tener en cuenta que tan pronto coloca un nombre de expediente, en Archivo crean
                            una carpeta f&iacute;sica en el cual empezaran a incluir los documentos
                            pertenecientes al mismo.
                        </strong>
                    </div>
                </td>
            </tr>
        </table>
        <?php
        }
        ?>


        <?php

        if ($estadoExpedienteAC == "Abierto") {  ?>
        <table border="0" width="80%" align="center" class="borde_tab">
            <tr align="center">
                <td width="33%" height="25" class="listado2" align="center">
                    <center>
                        <input name="btnConfirmar" type="button" onClick="confirmaIncluir();" class="botones_funcion" value="Confirmar">
                    </center>
                </td>
                <?php
                } else {
                ?>
                </center>
                </td>
                <table border='0' width='80%' align='center' class="borde_tab"  >
                    <tr align='center' >
                        <?php
                        }
                        ?>

                        <td width="33%" class="listado2" height="25">
                            <center><input name="cerrar" type="button" class="botones_funcion" id="envia22" onClick="opener.regresar(); window.close();" value=" Cerrar "></center></TD>
                    </tr>
                </table>
                <?
                }
                else if ( $_POST['numeroExpediente'] != "" && ( $expediente->existeExpediente( $_POST['numeroExpediente'] ) === 0 ) )
                {
                    ?>
                    <script language="JavaScript">
                        alert( "Error. El nombre del Expediente en el que desea incluir este radicado \n\r no existe en el sistema. Por favor verifique e intente de nuevo." );
                        document.getElementById( 'numeroExpediente' ).focus();
                    </script>
                    <?php
                }
                ?>
    </form>
</body>
</html>
