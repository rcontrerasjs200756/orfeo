<?
	session_start(); 
/*
 * Lista Subseries documentales
 * @autor Jairo Losada SuperSOlidaria 
 * @fecha 2009/06 Modificacion Variables Globales.
 */
foreach ($_GET as $key => $valor)   ${$key} = $valor;
foreach ($_POST as $key => $valor)   ${$key} = $valor;
$krd = $_SESSION["krd"];
$dependencia = $_SESSION["dependencia"];
$usua_doc = $_SESSION["usua_doc"];
$codusuario = $_SESSION["codusuario"];
$entidad = $_SESSION["entidad"];
$indiTRD = $_SESSION["indiTRD"];
$ruta_raiz = ".."; 
include_once("$ruta_raiz/include/db/ConnectionHandler.php");

$db = new ConnectionHandler("$ruta_raiz");
	if (!defined('ADODB_FETCH_ASSOC'))	define('ADODB_FETCH_ASSOC',2);
   	$ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
$yy = 20;
$date_r = Date('Y-m-d');
$date_r = getdate(strtotime($date_r));
$date_result = date("Y-m-d", mktime(($date_r["hours"]+0),($date_r["minutes"]+0),($date_r["seconds"]+0),($date_r["mon"]+0),($date_r["mday"]+0),($date_r["year"]+$yy)));
    if (!$fecha_busq)  $fecha_busq=Date('Y-m-d');
	if (!$fecha_busq2)  $fecha_busq2=$date_result;
	?>
<html>
<head>
<link rel="stylesheet" href="../estilos/orfeo.css">
    <link href="./../app/resources/global/plugins/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="./../app/resources/global/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet"
          type="text/css"/>

    <link href="./../app/resources/global/plugins/font-awesome/css/fonts.googleapis.css" rel="stylesheet" type="text/css"/>
    <link href="./../app/resources/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="./../app/resources/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="./../app/resources/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet"
          type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN THEME GLOBAL STYLES
    el siguiente archivo css, les quita la propiedad de borderradios a los input de form control, por eso lo comente.
    <link href="./app/resources/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    -->
    <link href="./../app/resources/global/css/components-rounded.min.css" rel="stylesheet" id="style_components"
          type="text/css"/>
    <link href="./../app/resources/global/css/plugins.min.css" rel="stylesheet" type="text/css"/>
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="./../app/resources/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css"/>
    <link href="./../app/resources/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css"/>


    <script src="./../app/resources/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="./../app/resources/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="./../app/resources/global/plugins/bootstrap-table/bootstrap-table.js" type="text/javascript"></script>
    <link href="./../app/resources/global/css/select2.min.css" rel="stylesheet"/>
    <link href="./../app/resources/global/css/select2-bootstrap.min.css" rel="stylesheet"/>
    <link href="./../app/resources/global/plugins/sweetalert2-9.10.2/package/dist/sweetalert2.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="./../app/resources/global/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css"/>

    <style>

        .borde_tab {
            border: thin solid #377584 !important;
            border-collapse: initial !important;
            border-spacing: 4px 4px !important;
            margin: 0 auto !important;
        }

        .popover, .tooltip {
            z-index: 9999999 !important;
        }
        .fa-item i {
            color: #999494 !important;
        }
        .fa-item {
            font-size: 12px !important;
        }
        .swal2-radio{
            display: block !important;
        }
    </style>

    <script type="text/javascript">
        var GB_ROOT_DIR = "../js/greybox/";
        var WS_APPSERVICE_URL = '<?php echo $_SESSION["WS_APPSERVICE_URL"]?>';
        var usuario_id = '<?php echo $_SESSION["usuario_id"]?>';
        var configuracionSession= '<?= json_encode($_SESSION["CONFIG"],JSON_FORCE_OBJECT) ?>';
        var allSelecteds = new Array();
        var allUsers = new Array(); //arreglo que almacena todos los usuarios a mostrar
        var allDepend = new Array(); //arreglo que almacena todas las dependencias a mostrar
        var onlyUsersSelected = new Array();
        var onlyDependSelected = new Array();


    </script>
    <script type="text/javascript" src="../js/greybox/AJS.js"></script>
    <script type="text/javascript" src="../js/greybox/AJS_fx.js"></script>
    <script type="text/javascript" src="../js/greybox/gb_scripts.js"></script>
    <link href="../js/greybox/gb_styles.css" rel="stylesheet" type="text/css" media="all" / >
    <script src="./../app/resources/global/plugins/respond.min.js"></script>
    <script src="./../app/resources/global/plugins/excanvas.min.js"></script>
    <script src="./../app/resources/global/plugins/ie8.fix.min.js"></script>
    <![endif]-->
    <!-- BEGIN CORE PLUGINS -->
    <script src="./../app/resources/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="./../app/resources/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="./../app/resources/global/plugins/js.cookie.min.js" type="text/javascript"></script>
    <script src="./../app/resources/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="./../app/resources/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="./../app/resources/global/plugins/bootstrap-switch/js/bootstrap-switch.js" type="text/javascript"></script>


    <!-- END CORE PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="./../app/resources/global/scripts/app.min.js" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="./../app/resources/pages/scripts/components-bootstrap-switch.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->

    <!-- BEGIN THEME LAYOUT SCRIPTS -->
    <script src="./../app/resources/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
    <script src="./../app/resources/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
    <script src="./../app/resources/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
    <script src="./../app/resources/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
    <script src="./../app/resources/global/plugins/sweetalert2-9.10.2/package/dist/sweetalert2.js"
            type="text/javascript"></script>
    <script src="./../app/resources/global/plugins/chosen.jquery.min.js" type="text/javascript"></script>
    <!--  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>-->
    <script src="./../app/resources/global/plugins/select2.full.min.js" type="text/javascript"></script>
    <script src="./../app/resources/global/plugins/editor.js" type="text/javascript"></script>

    <script src="./../app/resources/global/plugins/tinymce/tinymce.min.js" type="text/javascript"></script>

    <script src="./../app/resources/global/plugins/html-docx.js" type="text/javascript"></script>
    <script src="./../app/resources/global/plugins/file-saver/FileSaver.js" type="text/javascript"></script>

    <script src="./../app/resources/global/plugins/bootstrap-table/bootstrap-table.js" type="text/javascript"></script>
    <script src="./../app/resources/pages/scripts/toastr.min.js" type="text/javascript"></script>
    <script src="./../app/resources/pages/scripts/ui-toastr.min.js" type="text/javascript"></script>

    <script src="./../app/resources/apps/scripts/scriptconfiguraciones.js?v=<?= date('YmdHis') ?>"></script>
    <script src="./../app/resources/apps/scripts/funciones_globales.js?v=<?= date('YmdHis') ?>"
            type="text/javascript"></script>
    <script>armarConfigsHasValuesMayus(JSON.parse(configuracionSession));</script>

    <script src="./../app/resources/apps/scripts/controllers/SeguridadEntidad.js?v=<?= date('YmdHis') ?>" type="text/javascript"></script>
    <script src="./../app/resources/apps/scripts/services/SeriesService.js?v=<?= date('YmdHis') ?>" type="text/javascript"></script>
    <script src="./../app/resources/apps/scripts/services/SubSerieService.js?v=<?= date('YmdHis') ?>" type="text/javascript"></script>
    <script src="./../app/resources/apps/scripts/services/SeguridadService.js?v=<?= date('YmdHis') ?>" type="text/javascript"></script>
    <script>
        $(document).ready(function (){

            Seguridad.entidad_tipo = 'subserie';
            $('#modal_seguridad').on('shown.bs.modal', function() {
                $(document).off('focusin.modal');
            });
            $('.aLink').on('click', function (e){
                e.preventDefault();
                Seguridad.consultarSeguridad(Seguridad.entidad_tipo,($(this).attr('id')).slice(3));
            });

            $('#swich_seguridad').on('switchChange.bootstrapSwitch', function (event, state) {
                Seguridad.swichChange(state);
            });
        })
    </script>
</head>
<body bgcolor="#FFFFFF">
 <div id="spiffycalendar" class="text"></div>
<link rel="stylesheet" type="text/css" href="../js/spiffyCal/spiffyCal_v2_1.css">
<script language="JavaScript" src="../js/spiffyCal/spiffyCal_v2_1.js"></script>
<script language="javascript"><!--
    var dateAvailable  = new ctlSpiffyCalendarBox("dateAvailable", "adm_subserie", "fecha_busq","btnDate1","<?=$fecha_busq?>",scBTNMODE_CUSTOMBLUE);
	var dateAvailable2 = new ctlSpiffyCalendarBox("dateAvailable2", "adm_subserie", "fecha_busq2","btnDate2","<?=$fecha_busq2?>",scBTNMODE_CUSTOMBLUE);
//--></script>
<form name="adm_subserie" id='adm_subserie' method='post'  action='admin_subseries.php?<?=session_name()."=".session_id()."&krd=$krd&tiem_ac=$tiem_ac&tiem_ag=$tiem_ag&fecha_busq=$fecha_busq&fecha_busq2=$fecha_busq2&codserie=$codserie&tsub=$tsub&detasub=$detasub&asu=$asu"?>'>      
<table class=borde_tab width='98%' cellspacing="5"><tr><td class=titulos2><center>SUBSERIES DOCUMENTALES</center></td></tr></table>
    <div id="after_title_main" style="margin: 0.5em;"></div>
    <table><tr><td></td></tr></table>
<center>
<table width='90%' class="borde_tab" cellspacing="5">
<tr>
    <td width="95" height="21"  class='titulos2'> C&oacute;digo Serie<br>
   <td colspan="3"  class="listado5">
  <?php
    if(!$codserie) $codserie = 0;
	$fechah=date("dmy") . " ". time("h_m_s");
	$fecha_hoy = Date("Y-m-d");
	$sqlFechaHoy=$db->conn->DBDate($fecha_hoy);
	$check=1;
	$fechaf=date("dmy") . "_" . time("hms");
	$num_car = 4;
	$nomb_varc = "sgd_srd_codigo";
	$nomb_varde = "sgd_srd_descrip";
   	include "$ruta_raiz/include/query/trd/queryCodiDetalle.php";
	$querySerie = "select distinct ($sqlConcat) as detalle, sgd_srd_codigo 
	         from sgd_srd_seriesrd 
			 order by detalle 
			  ";
	$rsD=$db->conn->query($querySerie);
	$comentarioDev = "Muestra las Series Docuementales";
	include "$ruta_raiz/include/tx/ComentarioTx.php";
	print $rsD->GetMenu2("codserie", $codserie, "0:-- Seleccione --", false,"","onChange='submit()' class='select'" );

 ?>
 </td>
 </td>
 <?
if($_POST['actua_subserie'])
  {
 ?>
 <td width="35" height="21"><input type=submit name=modi_subserie Value='Grabar Modificacion' class=botones_largo ></td>
 <?
 }
 ?>
	<tr>
		<td width="125" height="21"  class='titulos2'>C&oacute;digo Subserie</td>
		<td width="125" valign="top" align="left" class='listado2'>
			<input name="tsub" type="text" size="20" class="tex_area" value="<?=$tsub?>">
		</td>
		<td width="125" height="21"  class='titulos2'>Descripci&oacute;n</td>
		<td valign="top" align="left" class='listado2'>
			<input name="detasub" type="text" size="75" class="tex_area" value="<?=$detasub?>">
		</td>
	</tr>
  <tr> 
  <td height="26" class='titulos2'>Fecha desde<br></td>
   	<td width="100" valign="top" class='listado2'>
	  <script language="javascript">
	  	dateAvailable.dateFormat="yyyy-MM-dd";
	    dateAvailable.date ="<?=$fecha_busq?>";
		dateAvailable.writeControl();
    </script>
     </td>
  <td height="26" class='titulos2'>Fecha Hasta<br></td>
 	<td width="100" align="right" valign="top" class='listado2'>
    <script language="javascript">
		dateAvailable2.dateFormat="yyyy-MM-dd";
		dateAvailable2.date ="<?=$fecha_busq2?>";
		dateAvailable2.writeControl();
    </script>
   </td>
  </tr>
  <tr> 
      <TD width="125" height="21"  class='titulos2'> Tiempo Archivo de Gesti&oacute;n</td>
      <TD valign="top" align="left" class='listado2'>
	     <input name="tiem_ag" type="text" size="20" class="tex_area" value="<?=$tiem_ag?>"></td>
     <TD width="125" height="21"  class='titulos2'> Tiempo Archivo Central</td>
      <TD valign="top" align="left" class='listado2'>
	  <input name="tiem_ac" type="text" size="20" class="tex_area" value="<?=$tiem_ac?>"> 
  </td>
  </tr>
  <tr> 
    <td width="125" height="21"  class='titulos2'> Soporte</td>
      <TD valign="top" align="left" class='listado2'>
	  <select  name='soporte'  class='select'>
	<?php
	if($soporte==1){$datosel=" selected ";}else {$datosel=" ";}
		echo "<option value='1' $datosel><font>1. PAPEL     </font></option>";
	if($soporte==2){$datosel=" selected ";}else {$datosel=" ";}
		echo "<option value='2' $datosel><font>2. MAGNETICO </font></option>";
        if($soporte==3){$datosel=" selected ";}else {$datosel=" ";}
		echo "<option value='3' $datosel><font>3.PAPEL / MAGNETICO </font></option>";
		?>
    </select>
  </td>
     <TD width="125" height="21"  class='titulos2'>Disposici&oacute;n Final</td>
      <TD valign="top" align="left" class='listado2'>
	  <select  name='med'  class='select'>
	<?php
	if($med==1){$datosel=" selected ";}else {$datosel=" ";}
		echo "<option value='1' $datosel><font>CONSERVACION TOTAL</font></option>";
        if($med==4){$datosel=" selected ";}else {$datosel=" ";}
		echo "<option value='4' $datosel><font>SELECCION O MUESTREO</font></option>";
        if($med==3){$datosel=" selected ";}else {$datosel=" ";}		
                echo "<option value='3' $datosel><font>DIGITALIZACION / MICROFILMACION</font></option>";
	if($med==2){$datosel=" selected ";}else {$datosel=" ";}
		echo "<option value='2' $datosel><font>ELIMINACION</font></option>";
		?>
</select>
  </td>
  </tr>
 	<tr>
	<td  class="titulos5" width="25%" align="right" > <font color="" face="Arial, Helvetica, sans-serif" class="etextomenu">Observaciones
		</font></td>
	<td width="75%" class="listado5" >
	<textarea name="asu" cols="50"  class="tex_area" rows="2" ><?=trim($asu)?></textarea>
	</td>
	</tr>
 <tr>
       <td height="26" colspan="4" valign="top" class='titulos2'> 
	   <center>
	   <input type=submit name=buscar_subserie Value='Buscar' class=botones >
	  <input type=submit name=insertar_subserie Value='Insertar' class=botones >
	  <input type=submit name=actua_subserie Value='Modificar' class=botones >
      <input type="reset"  name=aceptar class=botones id=envia22  value='Cancelar'>
 </td>
    </tr>
  </table>
<?PHP
    if ($tiem_ag == '') $tiem_ag = 0;
    if ($tiem_ac == '') $tiem_ac = 0;
     $tiem_ag = (int) $tiem_ag;
     $tiem_ac = (int) $tiem_ac;
     
    if  ($tiem_ag < 0) $tiem_ag = 0;
    if ($tiem_ac < 0) $tiem_ac = 0;
 	if($indiTRD == "SI")
	  {
            $detasub = strtolower(trim($detasub));
            $detasub = ucfirst(trim($detasub));
          }
        else {
            $detasub = strtoupper(trim($detasub));}

	$sqlFechaD=$db->conn->DBDate($fecha_busq);	
	$sqlFechaH=$db->conn->DBDate($fecha_busq2);	
    //Buscar detalle subserie
	 if($buscar_subserie && $detasub !='')
	   {
		 if($codserie != 0)
		 {
		 	$detasub = strtoupper(trim($detasub));
		    $whereBusqueda = " and sgd_sbrd_descrip like '%$detasub%'";
		 }
		 else
		 {
		    echo "<script>alert('Debe seleccionar la Serie');</script>";
		 }
	   }
	   
	  if($insertar_subserie)
		{

		  if($tsub !=0 && $codserie !=0 && $detasub !='')
		   {
			  $isqlB = "select * from sgd_sbrd_subserierd 
					  where sgd_srd_codigo = '$codserie'
					  and sgd_sbrd_codigo = '$tsub'
					  ";
	
			# Selecciona el registro a actualizar
			$rs = $db->query($isqlB); # Executa la busqueda y obtiene el registro a actualizar.
			$radiNumero = $rs->fields["SGD_SRD_CODIGO"];
	        if ($radiNumero !='') {
			   $mensaje_err = "<HR><center><B><FONT COLOR=RED>EL CODIGO < $codserieI > YA EXISTE. <BR>  VERIFIQUE LA INFORMACION E INTENTE DE NUEVO</FONT></B></center><HR>";
			   } 
			   else 
			   {
				$isqlB = "select * from sgd_sbrd_subserierd 
					  			where sgd_srd_codigo = '$codserie'
						        and sgd_sbrd_descrip = '$detasub'
						  "; 
				$rs = $db->query($isqlB); # Executa la busqueda y obtiene el registro a actualizar.
				$radiNumero = $rs->fields["SGD_SRD_CODIGO"];
	        	if ($radiNumero !='') {
				   $mensaje_err = "<HR><center><B><FONT COLOR=RED>LA SERIE <$detasub > YA EXISTE. <BR>  VERIFIQUE LA INFORMACION E INTENTE DE NUEVO</FONT></B></center><HR>";
				  }
				else
					{
						$query="insert into SGD_SBRD_SUBSERIERD(SGD_SRD_CODIGO   , SGD_SBRD_CODIGO,SGD_SBRD_DESCRIP,SGD_SBRD_FECHINI,SGD_SBRD_FECHFIN,SGD_SBRD_TIEMAG ,SGD_SBRD_TIEMAC,SGD_SBRD_DISPFIN,SGD_SBRD_SOPORTE,SGD_SBRD_PROCEDI)
						VALUES ($codserie,$tsub,'$detasub'    ,".$sqlFechaD.",".$sqlFechaH.",$tiem_ag,$tiem_ac,'$med','$soporte','$asu')";
                                               	$rsIN = $db->conn->query($query);
						$tsub = '' ;
						$detasub = '';
						$tiem_ag = '';
						$tiem_ac = '';					
						if(!rsIN) $mensaje_err=" <HR><center><B><FONT COLOR=RED> Verifique todos los datos</FONT></B></center><HR>";
						?>
						<script language="javascript">
					        document.adm_subserie.elements['detasub'].value= '';
						document.adm_subserie.elements['tsub'].value= '';
						document.adm_subserie.elements['asu'].value= '';
						document.adm_subserie.elements['tiem_ag'].value= '';
						document.adm_subserie.elements['tiem_ac'].value= '';

					</script>
						<?
  					}
			}
			}
			else
			{
			 echo "<script>alert('Los campos Serie, Subserie y Detalle son OBLIGATORIOS');</script>";
			}
			 
    }
				
	if($_POST['actua_subserie'] )
	  {  
  		 if ($codserie !=0 && $tsub !=0 )
		   {
			 $isqlB = "select * from sgd_sbrd_subserierd 
					  where sgd_srd_codigo = $codserie
					  and sgd_sbrd_codigo = $tsub
					  "; 
			  # Selecciona el registro a actualizar
			  $rs = $db->query($isqlB); # Executa la busqueda y obtiene el registro a actualizar.
			  $radiNumero = $rs->fields["SGD_SRD_CODIGO"];
	          if ($radiNumero =='') {
			    $mensaje_err = "<HR><center><B><FONT COLOR=RED>EL CODIGO < $codserie >< $tsub > NO EXISTE. <BR>  VERIFIQUE LA INFORMACION E INTENTE DE NUEVO</FONT></B></center><HR>";
			   } 
			   else 
			   {
			   //Carga Valores actuales
			     $detasub   = $rs->fields["SGD_SBRD_DESCRIP"];
				 $sqlFechaD = $rs->fields["SGD_SBRD_FECHINI"];
				 $sqlFechaH = $rs->fields["SGD_SBRD_FECHFIN"];
				 $tiem_ag	= $rs->fields["SGD_SBRD_TIEMAG"];  
 				 $tiem_ac   = $rs->fields["SGD_SBRD_TIEMAC"];  
 				 $med       = $rs->fields["SGD_SBRD_DISPFIN"];
				 $soporte   = $rs->fields["SGD_SBRD_SOPORTE"];
				 $asu      = $rs->fields["SGD_SBRD_PROCEDI"];
				 $fecha_busq = $sqlFechaD;
				 $fecha_busq2 = $sqlFechaH;
				 $varFechaD = $fecha_busq;
			
				 ?>
					<script language="javascript">
					document.adm_subserie.elements['detasub'].value= "<?=$detasub?>";
					document.adm_subserie.elements['tsub'].value= "<?=$tsub?>";
					document.adm_subserie.elements['asu'].value= "<?=$asu?>";
					document.adm_subserie.elements['tiem_ag'].value= "<?=$tiem_ag?>";
					document.adm_subserie.elements['tiem_ac'].value= "<?=$tiem_ac?>";
					document.adm_subserie.elements['soporte'].value= "<?=$soporte?>";
					document.adm_subserie.elements['med'].value= "<?=$med?>";
					document.adm_subserie.elements['fecha_busq'].value= "<?=$fecha_busq?>";
					document.adm_subserie.elements['fecha_busq2'].value= "<?=$fecha_busq2?>";
					
					</script>
					<?
			   }
	 }
	else  {
		  echo "<script>alert('Debe seleccionar la Serie y la Subserie');</script>";
	}
  }
  else
  {
	?>
 	<script language="javascript">

        document.adm_subserie.elements['detasub'].value= '';
	document.adm_subserie.elements['tsub'].value= '';
        document.adm_subserie.elements['asu'].value= '';
	document.adm_subserie.elements['tiem_ag'].value= '';
	document.adm_subserie.elements['tiem_ac'].value= '';
		
	</script>
	<?
  }
     
//Selecciono Grabar Cambios
   if($_POST['modi_subserie'] )
     {  
       if ($codserie !=0 && $tsub !=0 && $detasub != '')
	     {
	        $isqlB = "select * from sgd_sbrd_subserierd 
				        where sgd_srd_codigo = $codserie
				        and sgd_sbrd_descrip = '$detasub'
				        and sgd_sbrd_codigo != $tsub
			          "; 
	  		$rs = $db->query($isqlB); # Executa la busqueda y obtiene el registro a actualizar.
	  		$radiNumero = $rs->fields["SGD_SRD_CODIGO"];
	  		if ($radiNumero !='') 
	    		{
		   			$mensaje_err = "<HR><center><B><FONT COLOR=RED>LA SUBSERIE <$detasub > YA EXISTE. <BR>  VERIFIQUE LA INFORMACION E INTENTE DE NUEVO</FONT></B></center><HR>";
	    		}
	  		else  
	    		{		   
	     			$isqlUp = "update sgd_sbrd_subserierd 
					   			set SGD_SBRD_DESCRIP= '$detasub' 
						  			,SGD_SBRD_FECHINI=$sqlFechaD 
						  			,SGD_SBRD_FECHFIN =$sqlFechaH 
						  			,SGD_SBRD_TIEMAG = $tiem_ag
 						  			,SGD_SBRD_TIEMAC = $tiem_ac
 						  			,SGD_SBRD_DISPFIN ='$med'
						  			,SGD_SBRD_SOPORTE ='$soporte'
						  			,SGD_SBRD_PROCEDI ='$asu'
                        		where sgd_srd_codigo = $codserie
									and sgd_sbrd_codigo = $tsub
								";
            		$rsUp= $db->query($isqlUp); 
					$tsub = '' ;
					$detasub = '';
					$tiem_ag = '';
					$tiem_ac = '';
					$mensaje_err ="<HR><center><B><FONT COLOR=RED>SE MODIFICO LA SUBSERIE</FONT></B></center><HR>";
					?>
					<script language="javascript">
				        document.adm_subserie.elements['detasub'].value= '';
					document.adm_subserie.elements['tsub'].value= '';
	    				document.adm_subserie.elements['asu'].value= '';
	    				document.adm_subserie.elements['tiem_ag'].value= '';
					document.adm_subserie.elements['tiem_ac'].value= '';
					</script>
					<?
	    		}
   			} 
   		else
    		{
	    		echo "<script>alert('La Serie, la Subserie y el Detalle son OBLIGATORIOS');</script>";
    		}
	}
	include_once "$ruta_raiz/trd/lista_subseries.php";

	?>
	
</form>
</span>
<p>
<?=$mensaje_err?>
</p>
</body>
</html>
