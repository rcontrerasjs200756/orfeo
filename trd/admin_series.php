<?php
session_start(); 
/*
 * Lista Subseries documentales
 * @autor Jairo Losada SuperSOlidaria 
 * @fecha 2009/06 Modificacion Variables Globales.
 */
foreach ($_GET as $key => $valor)   ${$key} = $valor;
foreach ($_POST as $key => $valor)   ${$key} = $valor;
$krd = $_SESSION["krd"];
$dependencia = $_SESSION["dependencia"];
$usua_doc = $_SESSION["usua_doc"];
$codusuario = $_SESSION["codusuario"];
$ruta_raiz = ".."; 
if (!$nurad) $nurad= $rad;
if($nurad)
{
	$ent = substr($nurad,-1);
}
$yy = 20;
$date_r = Date('Y-m-d');
$date_r = getdate(strtotime($date_r));
$date_result = date("Y-m-d", mktime(($date_r["hours"]+0),($date_r["minutes"]+0),($date_r["seconds"]+0),($date_r["mon"]+0),($date_r["mday"]+0),($date_r["year"]+$yy)));
if (!$fecha_busq)  $fecha_busq=Date('Y-m-d');
if (!$fecha_busq2)  $fecha_busq2=$date_result;
include_once("$ruta_raiz/include/db/ConnectionHandler.php");
$db = new ConnectionHandler("$ruta_raiz");
define('ADODB_FETCH_ASSOC',2);

$ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
$encabezadol = "$PHP_SELF?".session_name()."=".session_id()."&nurad=$nurad&fecha_busq=$fecha_busq&fecha_busq2=$fecha_busq2&codserieI=$codserieI&detaserie=$detaserie&codusua=$codusua&depende=$depende&ent=$ent";

?>
<html>
<head>
<link rel="stylesheet" href="../estilos/orfeo.css">
<link href="./../app/resources/global/plugins/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
<link href="./../app/resources/global/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet"
      type="text/css"/>

<link href="./../app/resources/global/plugins/font-awesome/css/fonts.googleapis.css" rel="stylesheet" type="text/css"/>
<link href="./../app/resources/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="./../app/resources/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet"
      type="text/css"/>
<link href="./../app/resources/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet"
      type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN THEME GLOBAL STYLES
el siguiente archivo css, les quita la propiedad de borderradios a los input de form control, por eso lo comente.
<link href="./app/resources/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
-->
<link href="./../app/resources/global/css/components-rounded.min.css" rel="stylesheet" id="style_components"
      type="text/css"/>
<link href="./../app/resources/global/css/plugins.min.css" rel="stylesheet" type="text/css"/>
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link href="./../app/resources/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css"/>
<link href="./../app/resources/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css"/>


<script src="./../app/resources/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="./../app/resources/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="./../app/resources/global/plugins/bootstrap-table/bootstrap-table.js" type="text/javascript"></script>
<link href="./../app/resources/global/css/select2.min.css" rel="stylesheet"/>
<link href="./../app/resources/global/css/select2-bootstrap.min.css" rel="stylesheet"/>
<link href="./../app/resources/global/plugins/sweetalert2-9.10.2/package/dist/sweetalert2.min.css" rel="stylesheet"
      type="text/css"/>
<link href="./../app/resources/global/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css"/>

<style>

    .borde_tab {
        border: thin solid #377584 !important;
        border-collapse: initial !important;
        border-spacing: 4px 4px !important;
        margin: 0 auto !important;
    }

    .popover, .tooltip {
        z-index: 9999999 !important;
    }
    .fa-item i {
        color: #999494 !important;
    }
    .fa-item {
        font-size: 12px !important;;
    }
    .swal2-radio{
                 display: block !important;
    }
</style>

<script type="text/javascript">
    var GB_ROOT_DIR = "../js/greybox/";
    var WS_APPSERVICE_URL = '<?php echo $_SESSION["WS_APPSERVICE_URL"]?>';
    var usuario_id = '<?php echo $_SESSION["usuario_id"]?>';
    var configuracionSession= '<?= json_encode($_SESSION["CONFIG"],JSON_FORCE_OBJECT) ?>';
    var allSelecteds = new Array();
    var allUsers = new Array(); //arreglo que almacena todos los usuarios a mostrar
    var allDepend = new Array(); //arreglo que almacena todas las dependencias a mostrar
    var onlyUsersSelected = new Array();
    var onlyDependSelected = new Array();


</script>
<script type="text/javascript" src="../js/greybox/AJS.js"></script>
<script type="text/javascript" src="../js/greybox/AJS_fx.js"></script>
<script type="text/javascript" src="../js/greybox/gb_scripts.js"></script>
<link href="../js/greybox/gb_styles.css" rel="stylesheet" type="text/css" media="all" / >
    <script src="./../app/resources/global/plugins/respond.min.js"></script>
    <script src="./../app/resources/global/plugins/excanvas.min.js"></script>
    <script src="./../app/resources/global/plugins/ie8.fix.min.js"></script>
    <![endif]-->
    <!-- BEGIN CORE PLUGINS -->
    <script src="./../app/resources/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="./../app/resources/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="./../app/resources/global/plugins/js.cookie.min.js" type="text/javascript"></script>
    <script src="./../app/resources/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="./../app/resources/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="./../app/resources/global/plugins/bootstrap-switch/js/bootstrap-switch.js" type="text/javascript"></script>


    <!-- END CORE PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="./../app/resources/global/scripts/app.min.js" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="./../app/resources/pages/scripts/components-bootstrap-switch.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->

    <!-- BEGIN THEME LAYOUT SCRIPTS -->
    <script src="./../app/resources/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
    <script src="./../app/resources/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
    <script src="./../app/resources/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
    <script src="./../app/resources/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
    <script src="./../app/resources/global/plugins/sweetalert2-9.10.2/package/dist/sweetalert2.js"
            type="text/javascript"></script>
    <script src="./../app/resources/global/plugins/chosen.jquery.min.js" type="text/javascript"></script>
    <!--  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>-->
    <script src="./../app/resources/global/plugins/select2.full.min.js" type="text/javascript"></script>
    <script src="./../app/resources/global/plugins/editor.js" type="text/javascript"></script>

    <script src="./../app/resources/global/plugins/tinymce/tinymce.min.js" type="text/javascript"></script>

    <script src="./../app/resources/global/plugins/html-docx.js" type="text/javascript"></script>
    <script src="./../app/resources/global/plugins/file-saver/FileSaver.js" type="text/javascript"></script>

    <script src="./../app/resources/global/plugins/bootstrap-table/bootstrap-table.js" type="text/javascript"></script>
    <script src="./../app/resources/pages/scripts/toastr.min.js" type="text/javascript"></script>
    <script src="./../app/resources/pages/scripts/ui-toastr.min.js" type="text/javascript"></script>

    <script src="./../app/resources/apps/scripts/scriptconfiguraciones.js?v=<?= date('YmdHis') ?>"></script>
    <script src="./../app/resources/apps/scripts/funciones_globales.js?v=<?= date('YmdHis') ?>"
            type="text/javascript"></script>
    <script>armarConfigsHasValuesMayus(JSON.parse(configuracionSession));</script>

    <script src="./../app/resources/apps/scripts/controllers/SeguridadEntidad.js?v=<?= date('YmdHis') ?>" type="text/javascript"></script>
    <script src="./../app/resources/apps/scripts/services/SeriesService.js?v=<?= date('YmdHis') ?>" type="text/javascript"></script>
    <script src="./../app/resources/apps/scripts/services/SeguridadService.js?v=<?= date('YmdHis') ?>" type="text/javascript"></script>
    <script>
function regresar(){   	
	document.adm_serie.submit();
}


function val_datos()
{
	bandera = true;
	err_msg = '';
	if(!isNonnegativeInteger(document.getElementById('codserieI').value,false))
	{
		err_msg = err_msg+'Digite n�meros C�digo.\n';
		bandera = false;
	}
	if(isWhitespace(document.getElementById('detaserie').value))
	{
		err_msg = err_msg+'Digite Descripci�n.\n';
		bandera = false;
	}
	if (dateAvailable.getSelectedDate() > dateAvailable2.getSelectedDate())
	if(isWhitespace(document.getElementById('detaserie').value))
	{
		err_msg = err_msg+'Escoja correctamente las fechas.\n';
		bandera = false;
	}
	if (!bandera) alert(err_msg);
	return bandera;
}

$(document).ready(function (){
    Seguridad.entidad_tipo = 'serie';
    $('#modal_seguridad').on('shown.bs.modal', function() {
        $(document).off('focusin.modal');
    });
    $('.aLink').on('click', function (e){
        e.preventDefault();
        console.log(($(this).attr('id')).slice(3));
        Seguridad.consultarSeguridad(Seguridad.entidad_tipo,($(this).attr('id')).slice(3));
        console.log(Seguridad.entidad_acceso);
    });

    $('#swich_seguridad').on('switchChange.bootstrapSwitch', function (event, state) {
        Seguridad.swichChange(state);
    });
})
</script>
</head>
<body bgcolor="#FFFFFF">
 <div id="spiffycalendar" class="text"></div>
<link rel="stylesheet" type="text/css" href="../js/spiffyCal/spiffyCal_v2_1.css">
<script language="JavaScript" src="../js/spiffyCal/spiffyCal_v2_1.js"></script>
<script language="JavaScript" src="../js/formchek.js"></script>
<script language="javascript">
<!--
  var dateAvailable = new ctlSpiffyCalendarBox("dateAvailable", "adm_serie", "fecha_busq","btnDate1","<?=$fecha_busq?>",scBTNMODE_CUSTOMBLUE);
	var dateAvailable2 = new ctlSpiffyCalendarBox("dateAvailable2", "adm_serie", "fecha_busq2","btnDate1","<?=$fecha_busq2?>",scBTNMODE_CUSTOMBLUE);
//-->
</script>
 <div id="title_main" style="margin: 1em;"></div>
<table class=borde_tab width='98%' cellspacing="5"><tr><td class=titulos2><center>SERIES DOCUMENTALES</center></td></tr></table>
 <div id="after_title_main" style="margin: 0.5em;"></div>
 <form method="post" action="<?=$encabezadol?>" name="adm_serie">
<center>
<TABLE width="550" class="borde_tab" cellspacing="5">       
<TR>
	<TD width="125" height="21"  class='titulos2'> C&oacute;digo</td>
	<TD valign="top" align="left" class='listado2'><input type=text id="codserieI" name=codserieI value='<?=$codserieI?>' class='tex_area' size=11 maxlength="4" ></td>
</tr>
<tr>
	<TD height="26" class='titulos2'> Descripci&oacute;n</td>
	<TD valign="top" align="left" class='listado2'><input type=text id="detaserie" name=detaserie value='<?=$detaserie?>' class='tex_area' size=75 maxlength="75" ></td>
</tr>
<tr>
	<TD height="26" class='titulos2'>Fecha desde<br></td>
	<TD width="225" align="right" valign="top" class='listado2'>
		<script language="javascript">
		dateAvailable.date = "<?=date('Y-m-d');?>";
		dateAvailable.writeControl();
		dateAvailable.dateFormat="yyyy-MM-dd";
		</script>
	</TD>
</TR>
<TR>
	<TD height="26" class='titulos2'> Fecha Hasta<br></td>
	<TD width="225" align="right" valign="top" class='listado2'>
		<script language="javascript">
		dateAvailable2.date = "<?=date('Y-m-d');?>";
		dateAvailable2.writeControl();
		dateAvailable2.dateFormat="yyyy-MM-dd";
		</script>
	</td>
</TR>
<tr>
	<td height="26" colspan="3" valign="top" class='titulos2'>
		<center>
		<input type=submit name=buscar_serie Value='Buscar' class=botones >
		<input type=submit name=insertar_serie Value='Insertar' class=botones onclick="return val_datos();" >
		<input type=submit name=actua_serie Value='Modificar' class=botones onclick="return val_datos();" >
		<input type="reset"  name=aceptar class=botones id=envia22  value='Cancelar'>
		</center>
	</td>
</tr>
</table>
    <div id="after_form_main" style="margin: 2em;"></div>
<?PHP
$sqlFechaD=$db->conn->DBDate($fecha_busq);	
$sqlFechaH=$db->conn->DBDate($fecha_busq2);	
$detaserie = strtoupper(trim($detaserie));
//Busca series que cumplen con el detalle
if($buscar_serie && $detaserie !='')
{
	$whereBusqueda = " where upper(sgd_srd_descrip) like '%".strtoupper($detaserie)."%'";
}
if($insertar_serie && $codserieI !=0 && $detaserie !='')
{
	$isqlB = "select * from sgd_srd_seriesrd where sgd_srd_codigo = $codserieI"; 
	# Selecciona el registro a actualizar
	$rs = $db->query($isqlB); # Executa la busqueda y obtiene el registro a actualizar.
	$radiNumero = $rs->fields["SGD_SRD_CODIGO"];
	if ($radiNumero !='')
	{
		$mensaje_err = "<HR><center><B><FONT COLOR=RED>EL CODIGO < $codserieI > YA EXISTE. <BR>  VERIFIQUE LA INFORMACION E INTENTE DE NUEVO</FONT></B></center><HR>";
	} 
	else 
	{
		$isqlB = "select * from sgd_srd_seriesrd where sgd_srd_descrip = '$detaserie'"; 
		$rs = $db->query($isqlB); # Executa la busqueda y obtiene el registro a actualizar.
		$radiNumero = $rs->fields["SGD_SRD_DESCRIP"];
	    if ($radiNumero !='')
	    {
	    	$mensaje_err = "<HR><center><B><FONT COLOR=RED>LA SERIE <$detaserie > YA EXISTE. <BR>  VERIFIQUE LA INFORMACION E INTENTE DE NUEVO</FONT></B></center><HR>";
	    }
	    else
		{
			$query="insert into SGD_SRD_SERIESRD(SGD_SRD_CODIGO   , SGD_SRD_DESCRIP,SGD_SRD_FECHINI,SGD_SRD_FECHFIN )
					VALUES ($codserieI,'$detaserie'    ,".$sqlFechaD.",".$sqlFechaH.")";
			$rsIN = $db->conn->query($query);
			$codserieI = 0 ;
			$detaserie = '';
?>
<script language="javascript">
document.adm_serie.codserieI.value ='';
document.adm_serie.detaserie.value ='';
</script>
<?php
  		}
	}
}
if($actua_serie && $codserieI !=0 && $detaserie !='')
{
	$isqlB = "select * from sgd_srd_seriesrd where sgd_srd_codigo = $codserieI"; 
	# Selecciona el registro a actualizar
	$rs = $db->query($isqlB); # Executa la busqueda y obtiene el registro a actualizar.
	$radiNumero = $rs->fields["SGD_SRD_CODIGO"];
	if ($radiNumero =='')
	{
		$mensaje_err = "<HR><center><B><FONT COLOR=RED>EL CODIGO < $codserieI > NO EXISTE. <BR>  VERIFIQUE LA INFORMACI&Oacute;N E INTENTE DE NUEVO</FONT></B></center><HR>";
	} 
	else 
	{
		$isqlB = "select * from sgd_srd_seriesrd 
				  where sgd_srd_descrip = '$detaserie'
				  and sgd_srd_codigo != $codserieI"; 
		$rs = $db->query($isqlB); # Executa la busqueda y obtiene el registro a actualizar.
		$radiNumero = $rs->fields["SGD_SRD_CODIGO"];
	    if ($radiNumero !='')
	    {
	    	$mensaje_err = "<HR><center><B><FONT COLOR=RED>LA SERIE <$detaserie > YA EXISTE. <BR>  VERIFIQUE LA INFORMACI&Oacute;N E INTENTE DE NUEVO</FONT></B></center><HR>";
		}
		else
		{
			$isqlUp =	"update sgd_srd_seriesrd 
						set SGD_SRD_DESCRIP= '$detaserie',
						SGD_SRD_FECHINI=$sqlFechaD,
						SGD_SRD_FECHFIN =$sqlFechaH
						where sgd_srd_codigo = $codserieI";
			$rsUp= $db->query($isqlUp);
			$codserieI = 0 ;
			$detaserie = '';
			$mensaje_err ="<HR><center><B><FONT COLOR=RED>SE MODIFIC&Oacute; LA SERIE</FONT></B></center><HR>";
?>
			<script language="javascript">
			document.adm_serie.codserieI.value ='';
			document.adm_serie.detaserie.value ='';
			</script>
<?php
		}
	}
}
include_once "$ruta_raiz/trd/lista_series.php";
?>
</form>
<p>
<?=$mensaje_err?>
</p>
 <div id="modal_seguridad" class="modal fade" tabindex="-1" aria-hidden="true">
     <div class="modal-dialog modal-lg">
         <div class="modal-content" style="">
             <div class="modal-header bg-blue-chambray bg-font-yellow text-center">
                 <button type="button" data-dismiss="modal" title="Cerrar" aria-hidden="true"
                         class="btn btn-lg "
                         style="float: right;background-color: #2C3E50!important; border-radius:20px !important">
                     <i class="fa fa-remove"></i></button>
                 <h4 class="modal-title"><strong>Control de Acceso a la Serie</strong></h4>
             </div>

             <div class="modal-body" id="body_seguridad">

                 <div class="row" id="search-filter-container" style="text-align: center">
                     <div class="col-md-12">
                         <div class="search-bar bordered">
                             <form action="#" class="form-horizontal">
                                 <div class="form-body">
                                     <div class="form-group" id="divprevioswiche" style="height: 40px !important">

                                         <input type="checkbox" id="swich_seguridad" class="make-switch "
                                                data-on-text="P&uacute;blica" data-off-text="Restringir"
                                                checked data-on-color="primary" data-off-color="danger"
                                         >

                                     </div>
                                     <label class=" col-md-12" style="color:#4B77BE !important" id="labelentidadestado">
                                         <span id="icBloq">gd<i style="padding-left: 0.5em;padding-right: 0.5em;" class="fa fa-2x fa-unlock font-green"></i></span>Serie p&uacute;blica, sin restricci&oacute;n de acceso</label>
                                 </div>
                             </form>
                         </div>
                     </div>
                 </div>

                 <br>
                 <div class="row divshiden" style="display:none">
                     <div class="col-md-12">Lista de control de acceso a USUARIOS autorizados para ver esta serie:</div>
                     <div class="col-md-12">
                         <div class="input-icon right" id="div_prev_select_users">

                             <select class="js-data-example-ajax" name="id_select_users[]" multiple id="id_select_users"
                                     style="width: 100%"></select>
                             <br>
                         </div>
                     </div>
                 </div>

                 <div class="row divshiden" style="display:none">
                     <div class="col-md-12" style="line-height: 4pt"><p>Lista de control de acceso a DEPENDENCIAS autorizadas para ver esta serie:</p>
                         <p style="font-size: 10pt;font-style: oblique">Todos los usuarios de las dependencias elegidas
                             podrán ver</p></div>
                     <div class="col-md-12">
                         <div class="input-icon right" id="div_prev_select_dependencias">
                             <select class="js-data-example-ajax" name="select_dependencias[]" multiple
                                     id="select_dependencias"
                                     style="width: 100%"></select>
                             <br>
                         </div>
                     </div>
                 </div>

             </div>

             <div class="modal-footer">
                 <button type="button" class="btn dark btn-outline" data-dismiss="modal">Cerrar</button>
             </div>
         </div>
     </div>

 </div>
</body>
</html>
