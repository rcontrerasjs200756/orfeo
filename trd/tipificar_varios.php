<?
  	error_reporting(0); 
  	$krdOld=$krd;
 	session_start(); 
 	if(!$krd) $krd = $krdOld;
 	$ruta_raiz = ".."; 

	foreach ($_GET as $key => $valor)   ${$key} = $valor;
	foreach ($_POST as $key => $valor)   ${$key} = $valor;
 	
	if (!$nurad) $nurad= $rad;
	
	include_once("$ruta_raiz/include/db/ConnectionHandler.php");
	$db = new ConnectionHandler("$ruta_raiz");
	
	if (!defined('ADODB_FETCH_ASSOC')) define('ADODB_FETCH_ASSOC',2);
   	$ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
    $coddepe = $dependencia;
	$codusua = $codusuario; 		
	$encabezadol = "$PHP_SELF?".session_name()."=".session_id()."&krd=$krd&nurad=$nurad&coddepe=$coddepe&codusuario=$codusua&codusua=$codusua&codusuario=$codusuario&depende=$depende&ent=$ent&tdoc=$tdoc&codiTRDModi=$codiTRDModi&codiTRDEli=$codiTRDEli&codserie=$codserie&tsub=$tsub&ind_ProcAnex=$ind_ProcAnex&texp=$texp";
	
	
	
	?>
<html>
<head>
<title>Tipificar Documento</title>
<link href="../estilos/orfeo.css" rel="stylesheet" type="text/css">
<script>

function regresar(){   	
	document.TipoDocu.submit();
}
</script>
</head>
<body bgcolor="#FFFFFF">
<form method="post" action="<?=$encabezadol?>" name="TipoDocu" id="TipoDocu"> 
  <?
  /*
  * Adicion nuevo Registro
  */
  if($_POST['checkValue']!=''){  	
	$ban=0;
	$numRads="";
  	foreach($_POST['checkValue'] as $key=>$element){
  		$numRads.=$key.",";	
		if($ban==0){
			$firstRad=$key;
			$ban=1;
		}
  	} 	
	// Compruebo si son de la misma dependencia
	$sql="SELECT COUNT(DISTINCT(RADI_DEPE_RADI)) AS NUM_DEPE FROM RADICADO WHERE radi_nume_radi IN (".substr($numRads,0,count($numRads)-2).")";
	$rs=$db->query($sql);	
  }
  if($rs->fields['NUM_DEPE']<=1){

  ?>
  <input 	type="hidden" id="numRads" name="numRads" 
  			value="<?php if($_POST['checkValue']!=''){ echo substr($numRads,0,count($numRads)-2);}else{ echo $_POST['numRads']; }?>">
  <?php
  

  // BUSCO SI LOS RADICADOS YA TIENEN TIPO DOCUMENTAL, SI YA TIENEN LA CLASIFICACIÓN DE TRD SE ADICIONARÁ
  if($numRads!=''){
	  $rads=explode(",", $numRads);
	  $cad="";
	  foreach($rads as $element){
//		$sql="SELECT T.SGD_TPR_DESCRIP FROM RADICADO R INNER JOIN SGD_TPR_TPDCUMENTO T ON R.TDOC_CODI=T.SGD_TPR_CODIGO WHERE R.RADI_NUME_RADI='$element'";
		$sql="SELECT R.TDOC_CODI, T.SGD_TPR_DESCRIP FROM RADICADO R INNER JOIN SGD_TPR_TPDCUMENTO T ON R.TDOC_CODI=T.SGD_TPR_CODIGO WHERE R.RADI_NUME_RADI='$element'";				
		$rs=$db->query($sql);
//		if(strcmp($rs->fields['SGD_TPR_DESCRIP'], "SIN CLASIFICAR TRD")!=0 && $rs->fields['SGD_TPR_DESCRIP']!=""){
		if(($rs->fields['TDOC_CODI'])!=0){
			$cad.="El radicado <b>$element</b> ya tiene asignado una TRD, con Tipo Documental:&nbsp;  <b>".$rs->fields['SGD_TPR_DESCRIP']."</b>";
		}
	  }
  }
  // INGRESO LA TRD CORRESPONDIENTE A LOS RADICADOS SELECIONADOS
 if ($insertar_registro && $tdoc !=0 && $tsub !=0 && $codserie !=0 )
{		
	// Reviso si cada radicado tiene asignado TRD si ya tiene asignado no se le modifica.
	$numRads=explode(',',$_POST['numRads']);	
	
	
	
	$depe_codi=$_POST['depe_codi'];
	$isql="SELECT SGD_MRD_CODIGO FROM SGD_MRD_MATRIRD 
			WHERE 
				SGD_MRD_ESTA='1'
				AND SGD_SRD_CODIGO='$codserie' 
				AND SGD_SBRD_CODIGO='$tsub' 
				AND SGD_TPR_CODIGO='$tdoc' 
				AND DEPE_CODI=$depe_codi";
	$rs4=$db->query($isql);
	if($rs4->fields['SGD_MRD_CODIGO']!=""){
		$mrd_codigo=$rs4->fields['SGD_MRD_CODIGO'];	
	}
	
	
	include_once("../include/tx/Tx.php");	
	foreach($numRads as $element){			
		$sql = "SELECT RADI_NUME_RADI AS RADI_NUME_RADI 
					FROM SGD_RDF_RETDOCF r 
					WHERE RADI_NUME_RADI = '$element'
				    AND  DEPE_CODI =  '$depe_codi'";
		$rs=$db->conn->query($sql);
		$radiNumero = $rs->fields["RADI_NUME_RADI"];
		// Si no tiene asignado una TRD la inserto		
		if($radiNumero==''){
			$query="insert into 
					  SGD_RDF_RETDOCF
					    (RADI_NUME_RADI,DEPE_CODI,USUA_CODI,USUA_DOC,SGD_MRD_CODIGO,SGD_RDF_FECH) 
					  values 
					    ($element, $depe_codi, ".$_SESSION['codusuario'].", ".$_SESSION['usua_doc'].", $mrd_codigo, current_timestamp) 
								";
								
			$db->conn->query($query);			
			//Actualizo la trd en la tabla radicado
			$query2="UPDATE RADICADO SET TDOC_CODI = '$tdoc' WHERE RADI_NUME_RADI='$element'";
			$db->conn->query($query2);
			// inserto transacción
			$tx=new Tx($db);	
			$rads=array();
			$rads[0]=$element;
			$tx->insertarHistorico($rads, $_SESSION['dependencia'], $_SESSION['codusuario'], $_SESSION['dependencia'] , $_SESSION['codusuario'], 'Asignaci&oacute;n TRD','32');										
		}else{ // de lo contrario nada
			
		}
	} 
	?>
	<script>
	if(confirm('TRD Asignada correctamente. Desea incluir los radicados en un expediente???')){
		document.getElementById("TipoDocu").action="../expediente/incluir_varios.php";				
		document.getElementById("TipoDocu").submit();
	}else{
		window.opener.location.reload(); window.close();
	}	
	</script>
	<?php	
  	/*
  	*Actualiza el campo tdoc_codi de la tabla Radicados
  	*/
 	$radiUp = $trd->actualizarTRD($codiRegH,$tdoc);

  	$codserie = 0 ;
  	$tsub = 0  ;
  	$tdoc = 0;
}
////////////////////////	
?>  
	<table border=0 width=70% align="center" class="borde_tab" cellspacing="0">
	  <tr align="center" class="titulos2">
	    <td height="15" class="titulos2">APLICACION DE LA TRD A VARIOS DOCUMENTOS</td>
      </tr>
	  <?php 
	  if($cad!=''){?>
	  <tr>
		<td class="titulosError">
			<?php echo $cad; exit (0)?>
	<!--<?php echo $cad."<br> <a class=listado2> &nbsp; Si desea modificarlo, use la opción Modificar TRD ++ </a> <br><br>" ?> -->
		</td>
	  </tr>
	  <?php
	  }?>
	</table> 
 	<table width="70%" border="0" cellspacing="1" cellpadding="0" align="center" class="borde_tab">
      <tr >
	  <td class="titulos5" >Dependencia:</td>
	  <td class=listado5 >
        <?php
  
    if(!$tdoc) $tdoc = 0;
    if(!$codserie) $codserie = 0;
	if(!$tsub) $tsub = 0;
	$fechah=date("dmy") . " ". time("h_m_s");
	$fecha_hoy = Date("Y-m-d");
	$sqlFechaHoy=$db->conn->DBDate($fecha_hoy);
	$check=1;
	$fechaf=date("dmy") . "_" . time("hms");
	$num_car = 4;
	$nomb_varc = "s.sgd_srd_codigo";
	$nomb_varde = "s.sgd_srd_descrip";
	if($_POST['depe_codi']!=''){
		$depeTRD=$_POST['depe_codi'];
	}else{
		$depeTRD=$dependencia;
	}
	if($depeTRD=='%'){
		// CONSULTO LA DEPENDENCIA DEL RADICADO RADI_DEPE_RADI
		$query="SELECT RADI_DEPE_RADI from RADICADO where RADI_NUME_RADI='$firstRad'";
		$rs=$db->query($query);
		$depeTRD=$rs->fields['RADI_DEPE_RADI'];

	}		
	// COMBO DE DEPENDENCIAS
	$queryDependencia = "SELECT concat(depe_codi,concat('- ',depe_nomb)), depe_codi FROM dependencia WHERE depe_estado=1";			  
	$rs=$db->conn->query($queryDependencia);
	
	print $rs->GetMenu2("depe_codi", $depeTRD, "0:-- Seleccione --", false,"","onChange='submit()' class='select'" );
 ?>   
      </td>
     </tr>
     <tr >
	  <td class="titulos5" >SERIE</td>
	  <td class=listado5 >
        <?php
  
    if(!$tdoc) $tdoc = 0;
    if(!$codserie) $codserie = 0;
	if(!$tsub) $tsub = 0;
	$fechah=date("dmy") . " ". time("h_m_s");
	$fecha_hoy = Date("Y-m-d");
	$sqlFechaHoy=$db->conn->DBDate($fecha_hoy);
	$check=1;
	$fechaf=date("dmy") . "_" . time("hms");
	$num_car = 4;
	$nomb_varc = "s.sgd_srd_codigo";
	$nomb_varde = "s.sgd_srd_descrip";
   	include "$ruta_raiz/include/query/trd/queryCodiDetalle.php";
	$querySerie = "select distinct ($sqlConcat) as detalle, s.sgd_srd_codigo 
	         from sgd_mrd_matrird m, sgd_srd_seriesrd s
			 where m.depe_codi = '$depeTRD'
			 	   and s.sgd_srd_codigo = m.sgd_srd_codigo
			       and ".$sqlFechaHoy." between s.sgd_srd_fechini and s.sgd_srd_fechfin
			 order by detalle
			  ";		  
	$rsD=$db->conn->query($querySerie);
	$comentarioDev = "Muestra las Series Docuementales";
	include "$ruta_raiz/include/tx/ComentarioTx.php";
	print $rsD->GetMenu2("codserie", $codserie, "0:-- Seleccione --", false,"","onChange='submit()' class='select'" );
 ?>   
      </td>
     </tr>     
   <tr>
     <td class="titulos5" >SUBSERIE</td>
	 <td class=listado5 >
	<?
	$nomb_varc = "su.sgd_sbrd_codigo";
	$nomb_varde = "su.sgd_sbrd_descrip";
	include "$ruta_raiz/include/query/trd/queryCodiDetalle.php"; 
   	$querySub = "select distinct ($sqlConcat) as detalle, su.sgd_sbrd_codigo 
	         from sgd_mrd_matrird m, sgd_sbrd_subserierd su
			 where m.depe_codi = '$depeTRD'
				and m.SGD_MRD_ESTA='1'
				and m.sgd_srd_codigo = '$codserie'
				and su.sgd_srd_codigo = '$codserie'
				and su.sgd_sbrd_codigo = m.sgd_sbrd_codigo
				and ".$sqlFechaHoy." between su.sgd_sbrd_fechini and su.sgd_sbrd_fechfin
			 order by detalle
			  ";
	$rsSub=$db->conn->query($querySub);
	include "$ruta_raiz/include/tx/ComentarioTx.php";
	print $rsSub->GetMenu2("tsub", $tsub, "0:-- Seleccione --", false,"","onChange='submit()' class='select'" );

?> 
     </td>
     </tr>
   <tr>
     <td class="titulos5" >TIPO DE DOCUMENTO</td>
 	 <td class=listado5 >
        <?
	$nomb_varc = "t.sgd_tpr_codigo";
	$nomb_varde = "t.sgd_tpr_descrip";
	include "$ruta_raiz/include/query/trd/queryCodiDetalle.php"; 
	$queryTip = "select distinct ($nomb_varde) as detalle, t.sgd_tpr_codigo 
	         from sgd_mrd_matrird m, sgd_tpr_tpdcumento t
			 where m.depe_codi = '$depeTRD'
			       and m.sgd_mrd_esta = '1'
 			       and m.sgd_srd_codigo = '$codserie'
			       and m.sgd_sbrd_codigo = '$tsub'
 			       and t.sgd_tpr_codigo = m.sgd_tpr_codigo	  			   
			 order by detalle
			 ";
	$rsTip=$db->conn->query($queryTip);
	$ruta_raiz = "..";
	include "$ruta_raiz/include/tx/ComentarioTx.php";
	print $rsTip->GetMenu2("tdoc", $tdoc, "0:-- Seleccione --", false,"","onChange='submit()' class='select'" );
	?>
    </td>
    </tr>
   </table>
<br>
	<table border=0 width=70% align="center" class="borde_tab">
	  <tr align="center">
		<td width="33%" height="25" class="listado2" align="center">
         <center><input name="insertar_registro" type=submit class="botones_funcion" value=" Insertar "></center></TD>		 
        <td width="33%" class="listado2" height="25">
		 <center><input name="Cerrar" type="button" class="botones_funcion" id="envia22" onClick="window.opener.location.reload(); window.close();" value="Cerrar"></center></TD>
	   </tr>
	</table>
</form>
</span>
<p>
<?php
}else{
	?>
		<center><b>
		<div color="red">Los radicados seleccionados deben pertenecer a la misma dependencia.
				<br><a href="javascript:void(0)" onclick="window.close();">Regresar</a>
		</div></b></center>
	<?php
	
}
echo $mensaje_err;
?>
</p>
</span>
</body>
</html>
