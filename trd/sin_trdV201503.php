<?php
session_start();
$verrad = "";
$ruta_raiz = "..";
$dependencia=$_SESSION['dependencia'];
$krd=$_SESSION['krd'];
//echo "LoginKrd: ".$krd;
//if (!$dependencia)   include "$ruta_raiz/rec_session.php";
//if(!$dependencia or !$tpDepeRad) include "$ruta_raiz/rec_session.php";
//if($_SESSION['usua_perm_envios'] !=1 ) die(include "$ruta_raiz/errorAcceso.php");
if (!$dep_sel) $dep_sel = $dependencia;
function microtime_float()
{
  list($usec, $sec) = explode(" ", microtime());
  return ((float)$usec + (float)$sec);
}
?>
<html>
<head>
<title>Documentos sin TRD. Orfeo...</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../estilos/orfeo.css">
</head>
<body bgcolor="#FFFFFF" topmargin="0" onLoad="window_onload();">
<div id="spiffycalendar" class="text"></div>
<link rel="stylesheet" type="text/css" href="../js/spiffyCal/spiffyCal_v2_1.css">
<?
 $ruta_raiz = "..";
 include_once "$ruta_raiz/include/db/ConnectionHandler.php";
 $db = new ConnectionHandler("$ruta_raiz");	 
 //$db->conn->debug=true;
 if(!$carpeta) $carpeta=0;
 if(!$estado_sal)   {$estado_sal=2;}
 if(!$estado_sal_max) $estado_sal_max=3;

 if($estado_sal==3) {
    $accion_sal = "Envio de Documentos";
	$pagina_sig = "cuerpoEnvioNormal.php";
	$nomcarpeta = "Radicados Para Envio";
	if(!$dep_sel) $dep_sel = $dependencia;

	$dependencia_busq1 = " and c.radi_depe_radi = $dep_sel "; //and depe_estado=1 ";
	$dependencia_busq2 = " and c.radi_depe_radi = $dep_sel "; //and depe_estado=1 ";
 }
 
  if ($orden_cambio==1)  {
 	if (!$orderTipo)  {
	   $orderTipo="desc";
	}else  {
	   $orderTipo="";
	}
 }
 $encabezado = "".session_name()."=".session_id()."&krd=$krd&estado_sal=$estado_sal&estado_sal_max=$estado_sal_max&accion_sal=$accion_sal&dependencia_busq2=$dependencia_busq2&dep_sel=$dep_sel&filtroSelect=$filtroSelect&tpAnulacion=$tpAnulacion&nomcarpeta=$nomcarpeta&orderTipo=$orderTipo&orderNo=";
 $linkPagina = "$PHP_SELF?$encabezado&orderNo=$orderNo";
 $swBusqDep = "si";
 $carpeta = "nada"; 
 $varBuscada = "radi_nume_salida";
 ?>
 <br>
 <?php   
 //if ($busqRadicados) {//GTS
 $pagina_sig = "../envios/envia.php"; 
 //$pagina_sig = "../envios/envia.php";
	/*  GENERACION LISTADO DE RADICADOS
	 *  Aqui utilizamos la clase adodb para generar el listado de los radicados
	 *  Esta clase cuenta con una adaptacion a las clases utiilzadas de orfeo.
	 *  el archivo original es adodb-pager.inc.php la modificada es adodb-paginacion.inc.php
	 */
?>
  <form name=formEnviar id="formEnviar" action='' method=post>  
  <table width="100%">
  	<tr>
  		<td colspan=5 class="tablas" align="left">
  			Radicados separados por coma ',' 
  			<input name="busqRadicados" type="text" size="80" class="tex_area" value="<?=$busqRadicados?>">
	<input type=button value='Buscar ' name=Buscar valign='middle' class='botones' onclick="document.getElementById('formEnviar').action='sin_trd.php'; document.getElementById('formEnviar').target=''; document.getElementById('formEnviar').submit(); ">
  		</td>
  	</tr>
  	<tr class="titulos2">
  		<td align="center">
  			<b>Dependencia:</b>
  		</td>
  		<td colspan="3">
  		<?php
  			$queryDependencia = "SELECT (depe_codi ||' - ' || depe_nomb), depe_codi FROM dependencia WHERE depe_estado=1 ORDER BY depe_nomb ASC";			  
			$rs=$db->conn->query($queryDependencia);
			
			//$depe1=$_SESSION['depecodi'];
			$depe1="%";
			if($_POST['depe_codi']!=""){
				$depe1=$_POST['depe_codi'];
			}
			print $rs->GetMenu2("depe_codi", $depe1, "%:-- Todas --", false,""," class='select'" );
  		?> 
  		</td>
  	</tr>
	<tr class="titulos2">
  		<td align="center">
  			<b>Tipo de Radicado:</b>
  		</td>
  		<td>
			<select id="tipo" name="tipo" class="select">
				<option value="%" selected="selected">Todos</option>
				<option value="1">Salida</option>
				<option value="2">Entrada</option>
				<option value="3">Interno</option>
			</select>
  		</td>
		<td colspan="2" align="right">
			<a href="../expediente/conExp.php" target="_blank"> Consulta Expedientes</a>
		</td>
  	</tr>
  	<tr class="titulos2">
  		<td>
  			<input type="checkbox" name="incluirTRD" id="incluirTRD" value="1" checked>
  			Documentos sin TRD
  		</td>
  		<td>
  			<input type="checkbox" name="incluirEXP" id="incluirEXP" value="1">
  			Documentos sin expediente
  		</td>
  		<td>
			<input class="botones" type="button" valign="middle" name="Buscar" value="Buscar " onclick="document.getElementById('formEnviar').action='sin_trd.php'; document.getElementById('formEnviar').target=''; document.getElementById('formEnviar').submit(); "/>
  		</td>
  		<td align="right">  			
	  		<input 	type="button" value="Asignar TRD" align="right" class='botones_largo' style="width:100px"
		  			onclick="var sw=0; for(var i=1; i<document.formEnviar.elements.length;i++){ if (document.formEnviar.elements[i].checked && document.formEnviar.elements[i].name!='incluirTRD' && document.formEnviar.elements[i].name!='incluirEXP'){ sw=1; } } if (sw==0){ alert ('Debe seleccionar uno o mas radicados'); return;} document.getElementById('formEnviar').action='tipificar_varios.php'; document.getElementById('formEnviar').target='_blank'; document.getElementById('formEnviar').submit();">			  			
		  	<input type="button" value="Incluir en Expediente" align="right" class='botones_largo' style="width:140px"
		  			onclick="var sw=0; for(var i=1; i<document.formEnviar.elements.length;i++){ if (document.formEnviar.elements[i].checked && document.formEnviar.elements[i].name!='incluirTRD' && document.formEnviar.elements[i].name!='incluirEXP'){ sw=1; } } if (sw==0){ alert ('Debe seleccionar uno o mas radicados'); return;}document.getElementById('formEnviar').action='../expediente/incluir_varios.php'; document.getElementById('formEnviar').target='_blank'; document.getElementById('formEnviar').submit();">			  			
<?php
//if($usuaPermExpediente > 1 ) {
if( 1 == 1 ) {
?>
		  	<input type="button" value="Crear Expediente" align="right" class='botones_largo' style="width:140px"
		  			onclick="var sw=0; for(var i=1; i<document.formEnviar.elements.length;i++){ if (document.formEnviar.elements[i].checked && document.formEnviar.elements[i].name!='incluirTRD' && document.formEnviar.elements[i].name!='incluirEXP'){ sw=1; } } if (sw==0){ alert ('Debe seleccionar uno o mas radicados'); return;}document.getElementById('formEnviar').action='../expediente/crear_varios.php'; document.getElementById('formEnviar').target='_blank'; document.getElementById('formEnviar').submit();">	  			
<?php
}
?>
  		</td>  
  	</tr>
  </table>	  
  
 <?
   	
 	if($_POST['incluirTRD']=='' && $_POST['incluirEXP']==''){
 		$where=" r.TDOC_CODI<=0"; 		
 	}else{
 		if($_POST['incluirTRD']!=''){
 			$where=" r.TDOC_CODI<=0";
	 		if($_POST['incluirEXP']!=''){
	 			$where.=" OR e.sgd_exp_numero is null";
	 		}
 		}elseif($_POST['incluirEXP']!=''){
	 			$where=" e.sgd_exp_numero is null";
	 	}	
 	}
 
 	if($_POST['depe_codi']!=''){ 		
 		$depe_codi=$_POST['depe_codi'];
 	}else{
		$depe_codi="%";
 		//$depe_codi=$_SESSION['dependencia'];
 	}	
	$sqlFecha = $db->conn->SQLDate("Y-m-d H:i A","r.RADI_FECH_RADI");
	
	$isql='SELECT 
			 distinct(r.radi_nume_radi) AS "IMG_Radicado_salida"	 ,
			 r.RADI_PATH as "HID_RADI_PATH" ,
			 (CASE WHEN(SELECT count(e2.SGD_EXP_NUMERO) FROM SGD_EXP_EXPEDIENTE e2 WHERE e2.radi_nume_radi= r.RADI_NUME_RADI AND e2.SGD_EXP_ESTADO!=2 group by e2.radi_nume_radi)>0 THEN 1 END) AS EXP, 
			 (CASE WHEN(SELECT r2.TDOC_CODI FROM radicado r2 where r.radi_nume_radi=r2.radi_nume_radi)<>0 THEN 1 END) AS TRD,

			d.DEPE_NOMB AS Generado_Por ,

			  cast(r.RADI_NUME_RADI as text) AS HID_RADI_NUME_RADI,
			  a.anex_nomb_archivo AS HID_Radicado_salida ,
			  r.ra_asun AS Descripcion ,
			  CONCAT(CONCAT(r.radi_depe_radi, 779),(SELECT u2.USUA_NOMB FROM usuario u2 
									WHERE r.radi_depe_radi=u2.depe_codi AND usua_codi=1)) AS DEPE_RADI,
			  CONCAT(CONCAT(r.radi_depe_actu, 778), u.USUA_NOMB) AS USUARIO_ACTUAL,	  
			  r.radi_nume_radi AS "CHK_RADI_NUME_SALIDA"			  
			FROM 
			  RADICADO r 
			  LEFT JOIN dependencia d ON r.RADI_DEPE_RADI=d.DEPE_CODI			  
			  LEFT JOIN anexos a ON a.radi_nume_salida=r.radi_nume_radi 
			  LEFT JOIN sgd_exp_expediente e ON r.radi_nume_radi=e.radi_nume_radi
			  LEFT JOIN usuario u ON u.usua_codi=r.radi_usua_actu and u.depe_codi=r.RADI_DEPE_ACTU
			WHERE ';
	$tipo=$_POST['tipo'];
	if($tipo==""){
		$tipo=2;
	}
	if($_POST['busqRadicados']!=''){
		 $isql.=" r.RADI_NUME_RADI IN (".$_POST['busqRadicados'].") AND to_char(r.RADI_NUME_RADI,'99999999999999999999') LIKE '%".$tipo."' ";		 
		 $isql3="$isql";
	}else{
		//		
		if($tipo=="2" || $tipo=="%"){
				$isql2=$isql;		
		}
		
		$isql.="(to_char(r.RADI_DEPE_RADI,'99999999999999999999') like '".$depe_codi."' AND to_char(r.RADI_NUME_RADI,'99999999999999999999') LIKE '%".$tipo."' AND a.ANEX_ESTADO>1 AND (		   
			  ".$where."))";
		if($tipo=="2" || $tipo=="%"){
			$isql2.="(to_char(r.RADI_DEPE_RADI,'99999999999999999999') like '".$depe_codi."' AND to_char(r.RADI_NUME_RADI,'99999999999999999999') LIKE '%2' AND (		   
				  ".$where."))";
			$isql3="($isql) UNION ALL($isql2)";
		}else{
			$isql3="$isql";
		}		
	}
//	echo "Tipo:  ".$tipo;
	$pager = new ADODB_Pager($db,$isql3,'adodb', true,$orderNo,$orderTipo);
	$pager->toRefLinks = $linkPagina;
	$pager->toRefVars = $encabezado;
	$pager->checkTitulo = true;		
	$pager->Render($rows_per_page=100,$linkPagina,$checkbox=chkEnviar);
?>
  </form>
<font size="1" color="White" ><?=$time?></font>
</body>

</html>

