<?php
session_start();
/*
 * Lista Subseries documentales
 * @autor Jairo Losada
 * @fecha 2009/06 Modificacion Variables Globales.
 */
foreach ($_GET as $key => $valor)   ${$key} = $valor;
foreach ($_POST as $key => $valor)   ${$key} = $valor;
$krd = $_SESSION["krd"];
$dependencia = $_SESSION["dependencia"];
$usua_doc = $_SESSION["usua_doc"];
$codusuario = $_SESSION["codusuario"];
if (!$ruta_raiz) $ruta_raiz= "..";
$sqlFechaDocto =  $db->conn->SQLDate("Y-m-D H:i:s A","mf.sgd_rdf_fech");
$sqlSubstDescS =  $db->conn->substr."(SGD_SBRD_DESCRIP, 0, 100)";
$sqlFechaD = $db->conn->SQLDate("Y-m-d H:i A","SGD_SBRD_FECHINI");
$sqlFechaH = $db->conn->SQLDate("Y-m-d H:i A","SGD_SBRD_FECHFIN");
$isqlC = 'select 
        ID          AS "ID",
		SGD_SBRD_CODIGO          AS "CODIGO",
	'. $sqlSubstDescS .  '    AS "SUBSERIE",
	CASE WHEN UPPER(ACCESO_SUBSERIE) = '.'\'P\''.' THEN '.'\'Publica\''.' WHEN UPPER(ACCESO_SUBSERIE) = '.'\'R\''.' THEN '.'\'Reservada\''.' END AS "ACCESO",
	'.$sqlFechaD.' 			  as "DESDE",
	'.$sqlFechaH.' 			  as "HASTA" 
	from 
		SGD_SBRD_SUBSERIERD
	where
			SGD_SRD_CODIGO = '.$codserie.
			$whereBusqueda .'
	order by  '. $sqlSubstDescS;
     error_reporting(7);
?>
    <br>
	<br>
	<table class=borde_tab width='98%' cellspacing="5"><tr><td class=titulos2><center>SUBSERIES DOCUMENTALES</center></td></tr></table>
<table><tr><td></td></tr></table>
<br>
<TABLE width="850" class="borde_tab" cellspacing="5">
  <tr class=tpar> 
 <td class=titulos3 align=center>CODIGO </td>
   <td class=titulos3 align=center>DESCRIPCION </td>
   <td class=titulos3 align=center>NIVEL SEGURIDAD </td>
   <td class=titulos3 align=center>DESDE </td>
   <td class=titulos3 align=center>HASTA </td>
  </tr>
  	<?php
	 	$rsC=$db->query($isqlC);
   		while(!$rsC->EOF)
			{
                $idsubserie       =$rsC->fields["ID"];
			    $tsub       =$rsC->fields["CODIGO"];
	  			$dsubserie  =$rsC->fields["SUBSERIE"];
                $dsubserie_acceso  =$rsC->fields["ACCESO"];
                $fini       =$rsC->fields["DESDE"];
				$ffin       =$rsC->fields["HASTA"];				
		?> 
		      <tr class=paginacion>
    			<td> <?=$tsub?> </td>
				<td align=left><?=$dsubserie?> </td>
                  <td align=center><a id="COD<?=$idsubserie?>" href="#" class="aLink"><img src='../imagenes/candado.jpg' alt='' border=0 width="25" height="25"><span id="spanID<?=$idsubserie?>" > <?=$dsubserie_acceso?> </span></a></td>
				<td> <?=$fini?> </td>
				<td> <?=$ffin?></td> 
	         </tr>
	<?
				$rsC->MoveNext();
  		}
    ?>
   </table>
<div id="modal_seguridad" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="">
            <div class="modal-header bg-blue-chambray bg-font-yellow text-center">
                <button type="button" data-dismiss="modal" title="Cerrar" aria-hidden="true"
                        class="btn btn-lg "
                        style="float: right;background-color: #2C3E50!important; border-radius:20px !important">
                    <i class="fa fa-remove"></i></button>
                <h4 class="modal-title"><strong>Control de Acceso a la Subserie</strong></h4>
            </div>

            <div class="modal-body" id="body_seguridad">

                <div class="row" id="search-filter-container" style="text-align: center">
                    <div class="col-md-12">
                        <div class="search-bar bordered">
                            <form action="#" class="form-horizontal">
                                <div class="form-body">
                                    <div class="form-group" id="divprevioswiche" style="height: 40px !important">

                                        <input type="checkbox" id="swich_seguridad" class="make-switch "
                                               data-on-text="P&uacute;blica" data-off-text="Restringir"
                                               checked data-on-color="primary" data-off-color="danger"
                                        >

                                    </div>
                                    <label class=" col-md-12" style="color:#4B77BE !important" id="labelentidadestado">
                                        <span id="icBloq">gd<i style="padding-left: 0.5em;padding-right: 0.5em;" class="fa fa-2x fa-unlock font-green"></i></span>SubSerie p&uacute;blica, sin restricci&oacute;n de acceso</label>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <br>
                <div class="row divshiden" style="display:none">
                    <div class="col-md-12">Lista de control de acceso a USUARIOS autorizados para ver esta subserie:</div>
                    <div class="col-md-12">
                        <div class="input-icon right" id="div_prev_select_users">

                            <select class="js-data-example-ajax" name="id_select_users[]" multiple id="id_select_users"
                                    style="width: 100%"></select>
                            <br>
                        </div>
                    </div>
                </div>

                <div class="row divshiden" style="display:none">
                    <div class="col-md-12" style="line-height: 4pt"><p>Lista de control de acceso a DEPENDENCIAS autorizadas para ver esta subserie:</p>
                        <p style="font-size: 10pt;font-style: oblique">Todos los usuarios de las dependencias elegidas
                            podrán ver</p></div>
                    <div class="col-md-12">
                        <div class="input-icon right" id="div_prev_select_dependencias">
                            <select class="js-data-example-ajax" name="select_dependencias[]" multiple
                                    id="select_dependencias"
                                    style="width: 100%"></select>
                            <br>
                        </div>
                    </div>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>

</div>