<?php
	// Funci�n que agrega un usuario a la lista de informados
	function addInfor($form){		
		$xres=new xajaxResponse();
		require('../include/adodb/adodb.inc.php');
		require_once("../include/xWebComponent/database/DBConnection.php");
		require_once("../include/xWebComponent/database/data.php");		
		$con=new DBConnection($dbdriver, $server, $user, $password, $database);					
		// Consulto los datos del usuario		
		if($form['xusua_infor']!=''){			
			$ban=0;
			$xarray=array();
			foreach($form as $key=>$element){
				if(strstr($key, "listInfor_")!=''){	
					// Reviso cuales est�n					
					foreach($form['xusua_infor'] as $element2){
						if($element2==$element){
							$xarray[]=$element;							
						}
					}					
				}
			}
			
			$xarray=array_diff($form['xusua_infor'],$xarray);			
			foreach($xarray as $element){
				$xusua_array.="'$element', ";
			}
			$xusua_array=substr($xusua_array, 0, strlen($xusua_array)-2);			
		}		
		
		
		ob_start();		
		?>
		<table width="270px" align="center">
		<?php
		if($xusua_array!=''){
			$query="SELECT U.TITULO, U.USUA_NOMB, D.DEPE_NOMB, USUA_LOGIN 
					FROM USUARIO U INNER JOIN DEPENDENCIA D ON U.DEPE_CODI=D.DEPE_CODI 
					WHERE U.USUA_LOGIN IN ($xusua_array)";		
			$res=$con->execQuery($query);
		
			while(!$res->EOF){	
				$fila=$res->fields;
				?>			
					<tr>						
						<td style="font-size:12px">
							<input type="hidden" id="listInfor_<?php echo $fila['USUA_LOGIN']; ?>" name="listInfor_<?php echo $fila['USUA_LOGIN']; ?>" 
									value="<?php echo $fila['USUA_LOGIN']; ?>">
							<b><?php echo $fila['TITULO']." ".$fila['USUA_NOMB']." - ".$fila['DEPE_NOMB'];?></b>
						</td>
						<td style="cursor:pointer;">
							<img src="icons/button_cancel.png" onclick="xajax_removeInfor('<?php echo $fila['USUA_LOGIN']; ?>', xajax.getFormValues('xFormReasig'))">
						</td>
					</tr>		
				<?php	
				$res->moveNext();
			}
		}
		foreach($form as $key=>$element){
			if(strstr($key, "listInfor_")!=''){				
				$query="SELECT U.TITULO, U.USUA_NOMB, D.DEPE_NOMB 
					FROM USUARIO U INNER JOIN DEPENDENCIA D ON U.DEPE_CODI=D.DEPE_CODI 
					WHERE U.USUA_LOGIN='$element'";
				$res=$con->execQuery($query);			
				$fila=$con->getRow($res);				
		?>
			<tr>						
				<td style="font-size:12px">
					<input type="hidden" id="listInfor_<?php echo $element ?>" name="listInfor_<?php echo $element; ?>" 
							value="<?php echo $element; ?>">
					<b><?php echo $fila['TITULO']." ".$fila['USUA_NOMB']." - ".$fila['DEPE_NOMB'];?></b>
				</td>
				<td style="cursor:pointer;">
					<img src="icons/button_cancel.png" onclick="xajax_removeInfor('<?php echo $element ?>', xajax.getFormValues('xFormReasig'))">
				</td>
			</tr>		
		<?php
			}
		}
		?>
		</table>
		<?php
		$xres->addAssign("usuaInfor","innerHTML",ob_get_clean());		
		$con->disconnect();
		$xres->addAssign("xAutocomplete2_text","value","");
		return utf8_encode($xres->getXML());
	}
	
	// Verifica los valores seleccionados por el usuario y realiza las transacciones respectivas	
	function realizarTx($form){
		$xres=new xajaxResponse();		
		require('../include/adodb/adodb.inc.php');
		require_once("../include/xWebComponent/database/DBConnection.php");
		require_once("../include/xWebComponent/database/data.php");				
		$con=new DBConnection($dbdriver, $server, $user, $password, $database);		
		
		// Usuarios a los que se informa
		$xlist="";
		$xdepelist="";
		
		foreach($form as $key=>$element){
			if(strstr($key, "listInfor_")){												
				$sql = "SELECT
						DEPE_CODI
						,USUA_CODI				
						FROM
							USUARIO
						WHERE
							USUA_LOGIN='$element'";
				# Busca el usuairo Origen para luego traer sus datos.
				$rs = $con->execQuery($sql);
				$xlist.=$rs->fields['DEPE_CODI'].", ";
				$xdepelist.=$rs->fields['USUA_CODI'].", ";				
			}			
		}	
		$xlist=substr($xlist, 0, strlen($xlist)-2);
		$xdepelist=substr($xdepelist, 0, strlen($xdepelist)-2);
		$login_select=$form['xAutocomplete_hidden'];
		if($login_select!=''){			
			$codTx=$form['codTx'];
			$krd=$_SESSION['krd'];
			$dependencia=$_SESSION['dependencia'];
			
			// REASIGNAR
			$sql = "SELECT
					DEPE_CODI
					,USUA_CODI				
					FROM
						USUARIO
					WHERE
						USUA_LOGIN='$login_select'";
			# Busca el usuairo Origen para luego traer sus datos.
			$rs = $con->execQuery($sql);
			$usCodSelect=$rs->fields['DEPE_CODI'];
			$usCodSelect.="-".$rs->fields['USUA_CODI'];
			if($form['checkValue']!=''){
				$checkValue=$form['checkValue'];
				$checkValue=explode(",", $checkValue);
				foreach($checkValue as $key=>$element){						
					$checkValue[]="$element";					
					$sql = "SELECT TDOC_CODI FROM RADICADO WHERE RADI_NUME_RADI='$element'";					
					$rs2 = $con->execQuery($sql);					
					if($rs2->fields['TDOC_CODI']==0){
						$cad.="$element ";
					}
				}	
				
				
				if($cad!=''){
					$xres->addAlert("Debe asignar TRD a los radicados $cad");
					return utf8_encode($xres->getXML());	
				}
			}	
			$observa=$form['xReasigObs'];
			$observaPlazo=$form['xPlazoObs'];		
			$fechaAgenda=$form['fechaAgenda'];
			$observaInfor=$form['xInforObs'];		
			ob_start();
			?>
			<input type="hidden" id="codTx" name="codTx" value="9">
			<input type="hidden" id="usCodSelect" name="usCodSelect" value="<?php echo $usCodSelect ?>"> 
			<input type="hidden" id="observaPlazo" name="observaPlazo" value="<?php echo $observaPlazo ?>">
			<input type="hidden" id="observaInfor" name="observaInfor" value="<?php echo $observaInfor ?>">
			<input type="hidden" id="enviara" name="enviara" value="9">
			<input type="hidden" id="checkValue" name="checkValue" value="<?php echo $checkValue ?>">
			<input type="hidden" id="fechaAgenda" name="fechaAgenda" value="<?php echo $fechaAgenda ?>">		
			<input type="hidden" id="usCodSelect2" name="usCodSelect2" value="<?php echo $xlist; ?>"> 
			<input type="hidden" id="depesel8" name="depesel8" value="<?php echo $xdepelist ?>"> 
			<?php		
			$code=ob_get_clean();
			$xres->addAssign("xvars","innerHTML", $code);			
			$xres->addAssign("xFormReasig","action", "../tx/realizarTx.php");
			$xres->addScript("get('xFormReasig').submit();");
		}else{
			$xres->addAlert("Debe seleccionar el funcionario a quien reasignar.");
		
		}
		return utf8_encode($xres->getXML());	
	}	
	
	// Cargo combo de usuario para informar
	function loadUsua_infor($depe_codi){
		$xres=new xajaxResponse();		
		require('../include/adodb/adodb.inc.php');
		require_once("../include/xWebComponent/database/DBConnection.php");
		require_once("../include/xWebComponent/database/data.php");				
		$con=new DBConnection($dbdriver, $server, $user, $password, $database);		
		// Busco los usuarios de la dependencia seleccionada
		ob_start();
			
		$query="SELECT U.USUA_LOGIN, CONCAT(U.USUA_CARGO, CONCAT('-', CONCAT(U.TITULO, CONCAT(' ', U.USUA_NOMB)))) AS NOMBRE
				FROM USUARIO U LEFT JOIN GRADOS G ON U.ID_GRADO=G.ID_GRADO
				WHERE DEPE_CODI=$depe_codi
				AND usua_esta=1
				ORDER BY G.NUMERICO ASC";
		$res=$con->execQuery($query);				
		?>
		<table>
		<tr>
			<td>
				<input type="checkbox" onclick="todosInfor(this.checked)">
			</td>
			<td style="font-size:10px">
			<b>Marcar todos/desmarcar todos</b>
			</td>
		</tr>
		<?php
		while (!$res->EOF){ ?>						
			<tr>
			<td>
			<input type="checkbox" id="xusua_infor" name="xusua_infor[]" title="<?php echo $res->fields['NOMBRE']?>" value="<?php echo $res->fields['USUA_LOGIN']?>">
			</td>
			<td style="font-size:10px">
			<?php echo $res->fields['NOMBRE']?>
			</td>
			</tr>
		<?php 
			$res->moveNext();
		}	
		?>
		</table>
		<?php
		$xres->addAssign("xusu_infor_div", "innerHTML", ob_get_clean());
		return utf8_encode($xres->getXML());	
	}	
?>
