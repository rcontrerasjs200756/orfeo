<?php
	function saveChanges($form){
		$xres=new xajaxResponse();
		define('ADODB_ASSOC_CASE', 1);
		require('../include/adodb/adodb.inc.php');
		require_once("../include/xWebComponent/database/DBConnection.php");
		require_once("../include/xWebComponent/database/data.php");				
		$con=new DBConnection($dbdriver, $server, $user, $password, $database);			
		
		$noExpediente=trim($form['xexpediente']);
			$sqlExp = " Select count(1) As EXPEXISTE 
						From sgd_exp_expediente expok
						Where expok.sgd_exp_numero='$noExpediente' ";
			$sqlExpExec = $con->execQuery($sqlExp);
			$expExiste = $sqlExpExec->fields["EXPEXISTE"];
			if (empty($expExiste)) {
				$xres->addAlert("$expExiste : El expediente $noExpediente NO EXISTE \n Ingrese un numero de expediente correcto ");
			} else {
		$radicados=$form['xradicados'];
		$hijos=$form['xhijos'];
		$check_hijos=$form['check_hijos'];
		$dependencia=$_SESSION['dependencia'];
		$sinTrd=explode(',', $form['radSinTrd']);
		$tieneTrd=explode(',', $form['radConTrd']);
		$usua_codi=$_SESSION['codusuario'];
		$usua_doc=$_SESSION['usua_doc'];
		$usua_nomb=$_SESSION['usua_nomb'];
		
		$arrayRadicados=explode(",",$radicados);
		if($check_hijos!='' && $hijos !=''){
			$arrayHijos=explode(",", $hijos);
			$arrayRadicados=array_unique(array_merge($arrayRadicados, $arrayHijos));
		}
		
		foreach ($arrayRadicados as $actual){    	
			// Consulta si el radicado esta incluido en el expediente.				
			$sqlCon = "	SELECT 
							COUNT(1) AS TOTAL
						FROM 
							SGD_EXP_EXPEDIENTE SE
						WHERE 
							SE.SGD_EXP_NUMERO like '$noExpediente'
							AND SE.RADI_NUME_RADI = $actual";
			//$xres->addAlert($sqlCon);
			$existeEnExp = $con->execQuery($sqlCon);			
			$cant = $existeEnExp->fields["TOTAL"];			
			if (empty($cant)){                    
				// Insertamos el expediente en la tabla sgd_exp_expediente
				$query="INSERT INTO SGD_EXP_EXPEDIENTE
								(sgd_exp_numero, radi_nume_radi, sgd_exp_fech, sgd_exp_fech_mod, depe_codi, usua_codi, usua_doc, sgd_exp_asunto) 
						VALUES	('$noExpediente', '$actual', current_date, current_date, $dependencia, $usua_codi, '$usua_doc', '++') ";
				//$xres->addAlert($query);
				$rs = $con->execQuery($query);
				// Insertamos observación en histórico
				$observa 	= "Incluido en Expediente (Masivo): $noExpediente - Usuario: $usua_nomb - Dependencia: $dependencia";				
				
				$query="INSERT INTO sgd_hfld_histflujodoc(
								sgd_fexp_codigo, sgd_exp_fechflujoant, sgd_hfld_fech, 
								sgd_exp_numero, radi_nume_radi, usua_doc, usua_codi, depe_codi, 
								sgd_ttr_codigo, sgd_hfld_observa)
						VALUES (0, current_date, current_date, 
								'$noExpediente', '$actual', '$usua_doc', '$usua_codi', '$dependencia', 
								'53', '$observa')";				
				$con->execQuery($query);				
			}			
		}

		ob_start();
	
		?>
		<table id="respuestaTrdMass"  class=""  width="100%" align="center" margin="4">
			<tr>
				<td  class="titulos4" colspan="2" align="center" valign="middle">
					<center><b>Se incluyo en el expediente <?php echo $noExpediente; ?> los Radicados:<b></center>
				</td>
			</tr>
			<tr bordercolor="white" height="40px">
				<td  valign="center" align="center">
					<?php if($check_hijos!='') echo $hijos ?><br>
					<?php foreach($arrayRadicados as $element){ echo $element."<br>"; }?><br>
				</td>				
			</tr>			
			<tr>
				<td align="center">
					<button class="botones" type="button" onclick="window.close();"> Cerrar </button>		
				</td>
			</tr>	
		</table>
		<?php
		} // Si el exp no existe.
		$xres->addAssign('divResultado', 'innerHTML', ob_get_clean());
		return utf8_encode($xres->getXML());		
	}
?>