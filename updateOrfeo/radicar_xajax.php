<?php
  
	// Funcion que pinta las pestanias iniciales de tipo de radicacion: Nuevo, Anexo, Asociado
	function showPestanias($pestSel, $ent){		
		$xres=new xajaxResponse();
		define('ADODB_ASSOC_CASE', 1);
		require('../include/adodb/adodb.inc.php');
		require_once("../include/xWebComponent/database/DBConnection.php");
		require_once("../include/xWebComponent/database/data.php");				
		$con=new DBConnection($dbdriver, $server, $user, $password, $database);
		if($ent==2){
			$ent=0;
		}
		$query="SELECT CARP_DESC FROM CARPETA WHERE CARP_CODI='$ent'";
		$rs=$con->execQuery($query);	
		ob_start();
		?>
		<table cellpadding="0px" cellspacing="0px">
			<tr>
				<td>
					&nbsp;
				</td>
		<?php
		for($i=1; $i<=3; $i++){
			switch($i){
				case 1:
					$label='Nuevo';
				break;
				case 2:
					$label='';
				break;
				case 3:
					$label='Consultar Radicado';
				break;
			}			
			if($i==$pestSel){
				?>
				<td id="pest<?php echo $i; ?>Izq" class="pestIzqSel" >
					&nbsp;
				</td>
				<td id="pest<?php echo $i; ?>Cen" class="pestCenSel">
					<b><?php echo $label; ?></b>
				</td>
				<td id="pest<?php echo $i; ?>Der" class="pestDerSel" >
					&nbsp;
				</td>
				<td>&nbsp;
				</td>
				<?php			
			}else{
				?>
				<td id="pest<?php echo $i; ?>Izq" class="pestIzq" 
													onmouseout=" 
																document.getElementById('pest<?php echo $i; ?>Izq').className='pestIzq'
																document.getElementById('pest<?php echo $i; ?>Cen').className='pestCen'
																document.getElementById('pest<?php echo $i; ?>Der').className='pestDer'
																" 
													onmouseover="
																document.getElementById('pest<?php echo $i; ?>Izq').className='pestIzqSel'
																document.getElementById('pest<?php echo $i; ?>Cen').className='pestCenSel'
																document.getElementById('pest<?php echo $i; ?>Der').className='pestDerSel'
																"
													onclick="<?php if($i==1){ ?>xajax_showBusqDest(); <?php }else{ 
																?>xajax_showBusqRad('<?php if($i==3){ echo 'c'; } ?>');
																<?php } ?>
																xajax_showPestanias('<?php echo $i ?>','<?php echo $ent ?>'); document.getElementById('tipoRad').value='<?php echo $i; ?>'">
					&nbsp;
				</td>
				<td id="pest<?php echo $i; ?>Cen" class="pestCen"
													onmouseout=" 
																document.getElementById('pest<?php echo $i; ?>Izq').className='pestIzq'
																document.getElementById('pest<?php echo $i; ?>Cen').className='pestCen'
																document.getElementById('pest<?php echo $i; ?>Der').className='pestDer'
																" 
													onmouseover="
																document.getElementById('pest<?php echo $i; ?>Izq').className='pestIzqSel'
																document.getElementById('pest<?php echo $i; ?>Cen').className='pestCenSel'
																document.getElementById('pest<?php echo $i; ?>Der').className='pestDerSel'
																"
													onclick="<?php if($i==1){ ?>xajax_showBusqDest(); <?php }else{ 
																?>xajax_showBusqRad('<?php if($i==3){ echo 'c'; } ?>','<?php echo $ent ?>');
																<?php } ?>
																xajax_showPestanias('<?php echo $i ?>','<?php echo $ent ?>'); document.getElementById('tipoRad').value='<?php echo $i; ?>'">
					<b><?php echo $label; ?></b>
				</td>
				<td id="pest<?php echo $i; ?>Der" class="pestDer"
													onmouseout=" 
																document.getElementById('pest<?php echo $i; ?>Izq').className='pestIzq'
																document.getElementById('pest<?php echo $i; ?>Cen').className='pestCen'
																document.getElementById('pest<?php echo $i; ?>Der').className='pestDer'
																" 
													onmouseover="
																document.getElementById('pest<?php echo $i; ?>Izq').className='pestIzqSel'
																document.getElementById('pest<?php echo $i; ?>Cen').className='pestCenSel'
																document.getElementById('pest<?php echo $i; ?>Der').className='pestDerSel'
																"
													onclick="<?php if($i==1){ ?>xajax_showBusqDest(); <?php }else{ 
																?> xajax_showBusqRad('<?php if($i==3){ echo 'c'; } ?>');
																<?php } ?>
																xajax_showPestanias('<?php echo $i ?>','<?php echo $ent ?>'); document.getElementById('tipoRad').value='<?php echo $i; ?>'">
					&nbsp;
				</td>
				<td>&nbsp;
				</td>
				<?php			
			}		
			
		}
		  	if ($rs->fields['CARP_DESC']=="Pagos") { 
				?>
				<td style="background:#FFBF00" width="57%" style="color:red" style="font-size:12px" align="center">
					 <b style="color:#3B170B" > &nbsp; &nbsp; &nbsp; Radicaci&oacute;n - 
					 		<?php   
					 			echo $rs->fields['CARP_DESC']." </b> </td> <td><img src='../imagenes/tux_falls.gif' /> </td>";
								} else { 
							?>
									<td style="background:#D8D8D8" width="60%" style="color:#207186; font-size: 12px" align="center">
					 				<b style="color:#045FB4"> &nbsp; &nbsp; &nbsp; Radicaci&oacute;n - 
									<?php
											  echo $rs->fields['CARP_DESC']."&nbsp; &nbsp; &nbsp; </b> </td>"; 
										} 
					 				?>
			</tr>
		</table>
		<?php
	/*	$xres->addAssign("contRemit","value",0);
		$xres->addAssign("xDivRemit", "innerHTML", "");
	*/
		$xres->addAssign('divPest', 'innerHTML', ob_get_clean());		
		return utf8_encode($xres->getXML());
	}
	
	// Muestra el formulario para buscar destinatarios
	function showBusqDest(){
		$xres=new xajaxResponse();
		if (!$_SESSION['dependencia'])  
	 	$xres->addRedirect('../cerrar_session.php', 0);	
		ob_start();		
		?>
		<table>
			<tr>
				<?php
				if( $_GET['ent']!=3 ) {
				?>
				<td width=40% style="padding-left: 30px">
					<input type="radio" id="tipoBusq" name="tipoBusq" value="1" checked="checked">
					<b>Ciudadano</b>
				</td>								
				<td>
					<input type="radio" id="tipoBusq" name="tipoBusq" value="2">
					<b>Empresa</b>
				</td>				

				 <td>
                                        <input align="left" type="radio" id="tipoBusq" name="tipoBusq" value="4" <?php if($_GET['ent']==3){ echo "checked"; }?>>
                                        <b>Funcionario</b>
                                </td>


				<?php
				} else {
				?>
				<td>
					<input align="left" type="radio" id="tipoBusq" name="tipoBusq" value="4" <?php if($_GET['ent']==3){ echo "checked"; }?>>
					<b>Funcionario</b>
				</td>
				
				<?php
				}
				?>
				<!--td>
					<input type="radio" id="tipoBusq" name="tipoBusq" value="3">
					<b><?php echo str_replace("_", " ", ucfirst(strtolower($_SESSION['tip3Nombre'][3][($_GET['ent'])]))); ?></b>
				</td>
				<td width="30%">
					&nbsp;
				</td-->
			</tr>
			<tr>
				<td style="font-size: 12px; padding-left:50px" colspan="4">
					<b>Buscar:</b>								
					<input  type="text" autocomplete="off" id="xbusq" name="xbusq"  style="width:450px" class="textarea"
							onkeyup="	if(this.value.length>=2){											
											pushTime(this.value);
										}else{
											document.getElementById('xdestinatario').innerHTML='';
										}">									
				</td>
			</tr>
			<tr>
				<td colspan="4" align="left" style="padding-left: 50px">
					<div id="divXdestinatario">
						<select size="10" style="width: 950px; color:gray" class="select" id="xdestinatario" name="xdestinatario" onchange="/*xajax_getDataDest(xajax.getFormValues('form1'))*/">
							<option>No se encontraron resultados</option>
						</select>
					</div>								
					
				</td>
			</tr>
			<!--tr>
				<td colspan="4" align="center" style="padding-left: 50px">					
					<input type="button" value="Agregar" class="botones" onclick="xajax_getDataDest(xajax.getFormValues('form1'))">
				 <br>	
				</td>
			</tr-->
			<tr>
				<?php if ($_SESSION['usua_admin_sistema'] || ($_GET['ent']== 2 || $_SESSION['tpPerRad']['2']>0)){
				
				$col ='colspan="0" align="center" style="padding-center: 50px"' ;
			}
			else 
				$col ='colspan="4" align="center" style="padding-center: 50px"';?>
				<td  <?php echo $col; ?> >					
					<input type="button" value="Agregar" class="botones" onclick="xajax_getDataDest(xajax.getFormValues('form1'))">
					
					<br>	
				</td>
				<?php if ($_SESSION['usua_admin_sistema'] || $_GET['ent']== 2 || $_SESSION['usua_perm_createrceros']== 1 ){
					?>
						<td <?php echo $col;?> >					
							<input type="button" value="Crear Contacto" class="botones" onclick="window.open('../Administracion/admContactos.php','Registro Contactos',target='_blank','width=400,height=100')">
						<br>	
						</td>	
					<?
				}?>
			</tr>
		</table>
		<?php		
		$xres->addAssign('xshowDest', 'innerHTML', ob_get_clean());
		return utf8_encode($xres->getXML());		
	}
	// Modificado PATCH 2012-12-06
	// Copia al formulario de radicacion los datos del destinatario dependiendo el tipo
	function getDataDest($form){
		$xres=new xajaxResponse();
		define('ADODB_ASSOC_CASE', 1);
		require('../include/adodb/adodb.inc.php');
		require_once("../include/xWebComponent/database/DBConnection.php");
		require_once("../include/xWebComponent/database/data.php");				
		$con=new DBConnection($dbdriver, $server, $user, $password, $database);				
		// Verifico si ya esta agregado
		$tipo=$form['tipoBusq'];
		$id=$form['xdestinatario'];				
		switch($tipo){
			case 1:
				$query="SELECT
							  C.SGD_CIU_CODIGO AS XDESTINATARIO,
							  C.SGD_CIU_CEDULA AS DOCUMENTO,
							  C.SGD_CIU_NOMBRE AS NOMBRE,
							  C.SGD_CIU_DIRECCION AS DIRECCION,
							  C.SGD_CIU_APELL1 AS APELL1,
							  C.SGD_CIU_APELL2 AS APELL2,
							  C.SGD_CIU_TELEFONO AS TELEFONO,
							  C.SGD_CIU_EMAIL AS EMAIL,
							  C.MUNI_CODI,
							  C.DPTO_CODI,
							  C.ID_CONT,
							  C.ID_PAIS,
							  C.SGD_CIU_TELCELULAR AS MOVIL
							FROM SGD_CIU_CIUDADANO C
							WHERE 
							  C.SGD_CIU_CODIGO='$id'";						
			break;
			case 2:
				$query="SELECT 
						  E.SGD_OEM_CODIGO AS XDESTINATARIO,
						  E.SGD_OEM_NIT AS DOCUMENTO,
						  E.SGD_OEM_OEMPRESA AS NOMBRE,
					/*	  E.SGD_OEM_REP_LEGAL AS APELL1, */
						  E.SGD_OEM_SIGLA,
						  E.SGD_OEM_TELEFONO AS TELEFONO,
						  E.SGD_OEM_DIRECCION AS DIRECCION,
						  E.MUNI_CODI,
						  E.DPTO_CODI,
						  E.ID_PAIS,
						  E.ID_CONT,
						  E.SGD_OEM_REP_LEGAL AS XDIGNATARIO,
						  E.SGD_OEM_TELCELULAR AS MOVIL,
						  E.SGD_OEM_EMAIL AS EMAIL
						FROM SGD_OEM_OEMPRESAS E
						WHERE E.SGD_OEM_CODIGO='$id'";					
			break;			
			case 3:
				$query="SELECT 
						  B.IDENTIFICADOR_EMPRESA AS XDESTINATARIO,
						  B.IDENTIFICADOR_EMPRESA AS ID,
						  B.NOMBRE_DE_LA_EMPRESA AS NOMBRE,
						  B.NIT_DE_LA_EMPRESA AS DOCUMENTO,
						  B.DIRECCION AS DIRECCION,
						  B.TELEFONO_1 AS TELEFONO,
						  B.EMAIL AS EMAIL,
						  B.SIGLA_DE_LA_EMPRESA AS APELL1,
						  B.CODIGO_DEL_DEPARTAMENTO AS DPTO_CODI,
						  B.CODIGO_DEL_MUNICIPIO AS MUNI_CODI,
						  B.ID_PAIS,
						  B.ID_CONT,
						  B.NOMBRE_REP_LEGAL as XDIGNATARIO
						FROM BODEGA_EMPRESAS B
						WHERE B.IDENTIFICADOR_EMPRESA='$id'";					
			break;
			case 4:
				$query="SELECT
						  U.USUA_DOC AS XDESTINATARIO,
						  D.DEPE_NOMB AS DIRECCION,
						  D.DEP_SIGLA,
						  U.USUA_NOMB AS NOMBRE,
						  U.USUA_DOC AS DOCUMENTO,
						  U.USUA_EXT AS TELEFONO,
						  U.USUA_EMAIL AS EMAIL,
						  U.USUA_LOGIN AS APELL2,
						  U.ID_CONT,
						  U.ID_PAIS						  
						FROM USUARIO U
						INNER JOIN DEPENDENCIA D
						ON D.DEPE_CODI    = U.DEPE_CODI
						WHERE U.USUA_DOC='$id' AND D.DEPE_CODI<>99998";					
			break;
		}
		
		$rs = $con->execQuery($query);		
		if($form['contRemit']==0){				
			$xres->addAssign('xdocumento', 	'value', trim($rs->fields['DOCUMENTO']));
			$xres->addAssign('xnombre', 	'value', utf8_decode(trim($rs->fields['NOMBRE'])));
			$xres->addAssign('xapell1', 	'value', utf8_decode(trim($rs->fields['APELL1'])));
			$xres->addAssign('xapell2', 	'value', utf8_decode(trim($rs->fields['APELL2'])));		
			$xres->addAssign('xdireccion', 	'value', utf8_decode(trim($rs->fields['DIRECCION'])));
			$xres->addAssign('xtelefono', 	'value', trim($rs->fields['TELEFONO']));
			$xres->addAssign('xmail', 		'value', utf8_decode(trim($rs->fields['EMAIL'])));
			$xres->addAssign('xtipo_dest',	'value', $tipo);			
			if($rs->fields['ID_CONT']!='' && $rs->fields['ID_PAIS']!='' && $rs->fields['DPTO_CODI']!='' && $rs->fields['MUNI_CODI']){			
				$query2="SELECT 		  
										COALESCE(M.MUNI_NOMB,' ')||' - '||COALESCE(D.DPTO_NOMB,' ')||' - '||COALESCE(P.NOMBRE_PAIS,' ') AS XCIUDAD
									FROM MUNICIPIO M
									INNER JOIN DEPARTAMENTO D
									ON D.DPTO_CODI = M.DPTO_CODI
									AND D.ID_CONT  = M.ID_CONT
									AND D.ID_PAIS  = M.ID_PAIS
									INNER JOIN SGD_DEF_PAISES P
									ON P.ID_CONT    = D.ID_CONT
									AND P.ID_PAIS   = D.ID_PAIS
									WHERE 
										M.MUNI_CODI='".$rs->fields['MUNI_CODI']."' AND
										D.DPTO_CODI='".$rs->fields['DPTO_CODI']."' AND
										P.ID_PAIS='".$rs->fields['ID_PAIS']."' AND
										P.ID_CONT='".$rs->fields['ID_CONT']."'								
									";								
				$rs2 = $con->execQuery($query2);	
				$xres->addAssign("xAutocompleteCiudad_text", "value", $rs2->fields['XCIUDAD']);
				if($rs->fields['MUNI_CODI']!=""){
					$xres->addAssign("xAutocompleteCiudad_hidden", "value", $rs->fields['ID_CONT']."-".$rs->fields['ID_PAIS']."-".$rs->fields['DPTO_CODI']."-".$rs->fields['MUNI_CODI']);
				}else{
					$xres->addAssign("xAutocompleteCiudad_hidden", "value", "1-170-11-1");
				}
			}
		}		
		$ban=0;
		for($i=0;$i<$form["contRemit"]; $i++){
				if($form["xdocumento_$i"]==trim($rs->fields['DOCUMENTO'])){
					$ban=1;
				}
		}
		
		if($ban==0){
			$xnombres=utf8_decode(trim($rs->fields['NOMBRE']." ".$rs->fields['APELL1']." ".$rs->fields['APELL2'] ." - ".$rs->fields['DOCUMENTO']));
 			$xdatos=utf8_decode(trim($rs->fields['DIRECCION']." ".$rs->fields['TELEFONO']." - ".$rs->fields['EMAIL']));
 			$xdignatario=utf8_decode(trim($rs->fields['XDIGNATARIO']));
			ob_start();
			if($rs->fields['MUNI_CODI']!=""){
				$xubicacion=$rs->fields['ID_CONT']."-".$rs->fields['ID_PAIS']."-".$rs->fields['DPTO_CODI']."-".$rs->fields['MUNI_CODI'];
			}else{
				$xubicacion="1-170-11-1";
			}		
			?>
				<tr>
					<input type="hidden" id="xdir_tipo_<?php echo $i; ?>" name="xdir_tipo_<?php echo $i; ?>" value="<?php echo $tipo; ?>">				
					<input type="hidden" id="xDestinatario_<?php echo $form['contRemit']; ?>" name="xDestinatario_<?php echo $form['contRemit']; ?>" value="<?php echo trim($rs->fields['XDESTINATARIO']); ?>">
					<input type="hidden" id="xAutocompleteCiudad_hidden_<?php echo $form['contRemit']; ?>" name="xAutocompleteCiudad_hidden_<?php echo $form['contRemit']; ?>" value="<?php echo $xubicacion?>">
					<input type="hidden" id="xmail_<?php echo $form['contRemit']; ?>" name="xmail_<?php echo $form['contRemit']; ?>" value="<?php echo utf8_decode(trim($rs->fields['EMAIL'])); ?>">
					<input type="hidden" id="xdireccion_<?php echo $form['contRemit']; ?>" name="xdireccion_<?php echo $form['contRemit']; ?>" value="<?php echo utf8_decode(trim($rs->fields['DIRECCION']));?>">
					<input type="hidden" id="xtelefono_<?php echo $form['contRemit']; ?>" name="xtelefono_<?php echo $form['contRemit']; ?>" value="<?php echo trim($rs->fields['TELEFONO']);?>">
					<input type="hidden" id="apell2_<?php echo $form['contRemit']; ?>" name="apell2_<?php echo $form['contRemit']; ?>" value="<?php echo utf8_decode(trim($rs->fields['APELL2']));?>">
					<input type="hidden" id="xapell1_<?php echo $form['contRemit']; ?>" name="xapell1_<?php echo $form['contRemit']; ?>" value="<?php echo utf8_decode(trim($rs->fields['APELL1']));?>">
					<input type="hidden" id="xnombre_<?php echo $form['contRemit']; ?>" name="xnombre_<?php echo $form['contRemit']; ?>" value="<?php echo utf8_decode(trim($rs->fields['NOMBRE'])).' '.utf8_decode(trim($rs->fields['APELL1'])).' '.utf8_decode(trim($rs->fields['APELL2']));?>">
					<input type="hidden" id="sgd_dir_codigo_<?php echo $form['contRemit']; ?>" name="sgd_dir_codigo_<?php echo $form['contRemit']; ?>">
					<input type="hidden" name="xdocumento_<?php echo $form['contRemit']; ?>" id="xdocumento_<?php echo $form['contRemit']; ?>" readonly value="<?php echo trim($rs->fields['DOCUMENTO']) ?>" >	
					<input type="hidden" name="xmovil_<?php echo $form['contRemit']; ?>" id="xmovil_<?php echo $form['contRemit']; ?>" value="<?php echo trim($rs->fields['MOVIL']) ?>" >	
					<td valign="top" align="center">
						<input type="text" class="textarea" style="width:300px" name="xnombres_<?php echo $form['contRemit']; ?>" id="xnombres_<?php echo $form['contRemit']; ?>" readonly value="<?php echo trim($xnombres) ?>">
					</td>							
					<td valign="top" align="center">
						<input type="text" class="textarea" style="width:300px" name="xdatos_<?php echo $form['contRemit']; ?>" id="xdatos_<?php echo $form['contRemit']; ?>" readonly value="<?php echo trim($xdatos) ?>">
					</td>
					<td valign="top" align="center">
						<input type="text" class="textarea" style="width:300px" name="xdignatario_<?php echo $form['contRemit']; ?>" id="xdignatario_<?php echo $form['contRemit']; ?>" value="<?php echo trim($xdignatario) ?>">
					</td>						
					<td valign="top" align="center">
						<input type="checkbox" id="xSelect_<?php echo $form['contRemit']; ?>" name="xSelect_<?php echo $form['contRemit']; ?>" value="<?php echo $form['contRemit']; ?>" checked>
					</td>							
				</tr>
			<?php
			$xres->addAssign("contRemit","value",$form['contRemit']+1);
			$xres->addAppend("xDivRemit", "innerHTML", ob_get_clean());
			$xres->addScript("document.getElementById('btnRadicar').scrollIntoView(true)");
		}
		return utf8_encode($xres->getXML());	
	}
	
	
	// Realiza la busqueda de destinatarios 
	function getSearchDest($busqueda, $form){
		$xres=new xajaxResponse();	
		define('ADODB_ASSOC_CASE', 1);	
		require('../include/adodb/adodb.inc.php');
		require_once("../include/xWebComponent/database/DBConnection.php");
		require_once("../include/xWebComponent/database/data.php");				
		$con=new DBConnection($dbdriver, $server, $user, $password, $database);			
		
		$tipo=$form['tipoBusq'];						
		// Modified by Patch		
/*		$tildes=utf8_decode($_SESSION['tildes']);
		$tildes2=utf8_decode($_SESSION['tildes2']);
		$busqueda=utf8_decode($busqueda);
		
		$tildes=($_SESSION['tildes']);
		$tildes2=($_SESSION['tildes2']);
		$busqueda=($busqueda);
*/
		switch($tipo){
			case 1:
				/*$query="SELECT C.SGD_CIU_NOMBRE as ".utf8_decode('ñ').", */
				$query="SELECT C.SGD_CIU_NOMBRE,
							  C.SGD_CIU_DIRECCION,
							  C.SGD_CIU_APELL1,
							  C.SGD_CIU_APELL2,
							  C.SGD_CIU_TELEFONO,
							  C.SGD_CIU_CEDULA AS DOCUMENTO,
							  C.SGD_CIU_EMAIL,
							  C.MUNI_CODI,
							  C.DPTO_CODI,
							  C.ID_CONT,
							  C.ID_PAIS,
							  C.SGD_CIU_CODIGO as ID
							  
							FROM SGD_CIU_CIUDADANO C
							WHERE 
							  	  upper(C.SGD_CIU_NOMBRE) LIKE '%' || upper('$busqueda') ||'%'
							  OR upper(C.SGD_CIU_APELL1) LIKE '%' || upper('$busqueda') ||'%'
							  OR upper(C.SGD_CIU_CEDULA) LIKE '%' || upper('$busqueda') ||'%'
							  OR upper(C.SGD_CIU_APELL2) LIKE '%' || upper('$busqueda') ||'%'
							  OR upper(C.SGD_CIU_NOMBRE||' '||C.SGD_CIU_APELL1||' '||C.SGD_CIU_APELL2) LIKE '%' || upper('$busqueda') ||'%'
							ORDER BY C.SGD_CIU_NOMBRE ASC";						
			break;
			case 2:
				$query="SELECT E.SGD_OEM_CODIGO AS ID,
						  E.SGD_OEM_OEMPRESA,
						  E.SGD_OEM_REP_LEGAL,
						  E.SGD_OEM_NIT AS DOCUMENTO,
						  E.SGD_OEM_SIGLA,
						  E.SGD_OEM_TELEFONO,
						  E.SGD_OEM_DIRECCION,
						  E.MUNI_CODI,
						  E.DPTO_CODI,
						  E.ID_PAIS,
						  E.ID_CONT
						FROM SGD_OEM_OEMPRESAS E
						WHERE 
						(
						     upper(E.SGD_OEM_OEMPRESA) LIKE '%' || upper('$busqueda') || '%'
						  or upper(E.SGD_OEM_REP_LEGAL) LIKE '%' || upper('$busqueda') || '%'
						  or upper(E.SGD_OEM_SIGLA) LIKE '%' || upper('$busqueda') || '%'
						  or upper(E.SGD_OEM_NIT) LIKE '%'|| upper('$busqueda') || '%'
						  or E.SGD_OEM_NIT='$busqueda'
						  ) And E.sgd_oem_inactivo is null
						ORDER BY E.SGD_OEM_OEMPRESA ASC";						
			break;
			case 3:
				$query="SELECT 
						  B.IDENTIFICADOR_EMPRESA AS ID,
						  B.NOMBRE_DE_LA_EMPRESA AS NOMBRE,
						  B.NIT_DE_LA_EMPRESA AS DOCUMENTO,
						  B.DIRECCION AS DIRECCION,
						  B.TELEFONO_1 AS TELEFONO,
						  B.EMAIL AS EMAIL,
						  B.SIGLA_DE_LA_EMPRESA AS APELL1,
						  B.CODIGO_DEL_DEPARTAMENTO AS DPTO_CODI,
						  B.CODIGO_DEL_MUNICIPIO AS MUNI_CODI,
						  B.ID_PAIS,
						  B.ID_CONT
						FROM BODEGA_EMPRESAS B
						WHERE B.ACTIVA=1
						AND ( upper(B.NOMBRE_DE_LA_EMPRESA) LIKE '%' || upper('$busqueda') || '%'
						  or upper(B.SIGLA_DE_LA_EMPRESA) LIKE '%' || upper('$busqueda') || '%'						  
						  or upper(B.NIT_DE_LA_EMPRESA) LIKE '%' || upper('$busqueda') || '%'						  
						)  
						ORDER BY B.NOMBRE_DE_LA_EMPRESA ASC
						";					
			break;
			case 4:
				//MODIFICADO PATCH 2012-11-28
				$query="SELECT U.USUA_DOC AS ID,
						  D.DEPE_NOMB,
						  D.DEP_SIGLA,
						  U.USUA_NOMB,
						  U.USUA_DOC as DOCUMENTO,
						  U.USUA_EXT,
						  U.USUA_EMAIL,
						  D.ID_CONT,
						  D.ID_PAIS,
						  D.MUNI_CODI,
						  D.DPTO_CODI,
						  U.TITULO
						FROM USUARIO U
						INNER JOIN DEPENDENCIA D
						ON D.DEPE_CODI    = U.DEPE_CODI					
						WHERE U.USUA_ESTA = '1'
						AND (
						  upper(U.USUA_LOGIN) LIKE '%' || upper('$busqueda') || '%'
						  OR upper(U.TITULO) LIKE '%' || upper('$busqueda') || '%'
						  or upper(D.DEPE_NOMB) LIKE '%' || upper('$busqueda') || '%'
						  or upper(D.DEP_SIGLA) LIKE '%' || upper('$busqueda') || '%'
						  or upper(U.USUA_NOMB) LIKE '%' || upper('$busqueda') || '%'  
						  OR upper(U.USUA_DOC) LIKE '%' || upper('$busqueda') || '%'
						)
						";//ORDER BY U.ID_GRADO, U.USUA_NOMB ASC";
			break;			
		}	
		$rs = $con->execQuery($query,0,50);						
		ob_start();
		?>
		<!--<select size="10" style="width: 950px" class="select" id="xdestinatario" name="xdestinatario" onchange="/*xajax_getDataDest(xajax.getFormValues('form1'))*/">	-->
		<table style="width: 950px; max-height:300px; overflow:auto; font-size:10px; color:Gray; " class="select">
		<?php
		if($rs->fields['ID']!=''){			
			while(!$rs->EOF){
				$value=$rs->fields['ID'];				
				switch($tipo){
					case 1:
						$cad[0]=$rs->fields['SGD_CIU_NOMBRE']." ".$rs->fields['SGD_CIU_APELL1']." ".$rs->fields['SGD_CIU_APELL2']."  - ".$rs->fields['DOCUMENTO'];
						//$xnombres=trim($rs->fields['NOMBRE']." ".$rs->fields['APELL1']." - ".$rs->fields['APELL2'] ." ".$rs->fields['DOCUMENTO']);
						$cad[1]=$rs->fields['SGD_CIU_DIRECCION'];
						$cad[2]=$rs->fields['SGD_CIU_TELEFONO'];

						/*$cad=$rs->fields['SGD_CIU_NOMBRE']." ".$rs->fields['SGD_CIU_APELL1']." ".$rs->fields['SGD_CIU_APELL2']." - ".$rs->fields['SGD_CIU_DIRECCION']." - TEL:".$rs->fields['SGD_CIU_TELEFONO'];*/
					break;
					case 2:
						$cad[0]=$rs->fields['SGD_OEM_OEMPRESA']."  - ".$rs->fields['DOCUMENTO'];
						$cad[1]=$rs->fields['SGD_OEM_DIRECCION'];
						$cad[2]=$rs->fields['SGD_OEM_SIGLA'];
						$cad[3]=$rs->fields['SGD_OEM_NIT'];
						$cad[4]=$rs->fields['SGD_OEM_REP_LEGAL'];						
						/*$cad=$rs->fields['SGD_OEM_OEMPRESA']." - ".$rs->fields['SGD_OEM_DIREC']." - ".$rs->fields['SGD_OEM_SIGLA']." - ".$rs->fields['SGD_OEM_NIT']." - ".$rs->fields['SGD_OEM_REP_LEGAL'];	*/
					break;					
					case 3:
						$cad[0]=$rs->fields['NOMBRE'];
						$cad[1]=$rs->fields['APELL1'];
						$cad[2]=$rs->fields['TELEFONO'];
						$cad[3]=$rs->fields['EMAIL'];
						/*$cad=$rs->fields['NOMBRE']." ".$rs->fields['APELL1']." - TEL: ".$rs->fields['TELEFONO']." - ".$rs->fields['EMAIL'];*/						break;
					case 4:
						//MODIFICADO PATCH 2012-11-28
						$cad[0]=$rs->fields['TITULO'].".".$rs->fields['USUA_NOMB'];
						$cad[1]=$rs->fields['DEPE_NOMB'];
						$cad[2]="EXT: ".$rs->fields['USUA_EXT'];
						$cad[3]=$rs->fields['USUA_EMAIL'];						
						/*$cad=$rs->fields['TITULO'].".".$rs->fields['USUA_NOMB']."-".$rs->fields['DEPE_NOMB']." - EXT: ".$rs->fields['USUA_EXT']." - ".$rs->fields['USUA_EMAIL'];		*/				
					break;					
				}
				?>
				<tr>	
				<td style="border:1px solid #FFFAF0" > <input type="radio" name="xdestinatario" value="<?php echo $rs->fields['ID']?>"></td>
				 	<td style="border:1px solid #FFFAF0">&nbsp;<?php echo utf8_decode($cad[0]);?></td>
				 	<td style="border:1px solid #FFFAF0">&nbsp;<?php echo utf8_decode($cad[1]);?></td>
				 	<td style="border:1px solid #FFFAF0">&nbsp;<?php echo utf8_decode($cad[2]);?></td>
				 	<td style="border:1px solid #FFFAF0">&nbsp;<?php echo utf8_decode($cad[3]);?></td>
				 	<td style="border:1px solid #FFFAF0">&nbsp;<?php echo utf8_decode($cad[4]);?></td>				
				<!--<option value="<?php //echo $rs->fields['ID']?>"><?php //echo $cad ?></option>-->
				</tr>
				<?php
				$rs->moveNext();				
			}				
		}else{
			?>
				<table style="width: 950px; max-height:300px; overflow:auto; font-size:10px; color:red" class="select">
					<tr><td><br>No se encontraron resultados<br></td></tr>
				</table>
			<?php
		}		
		?>
		</table>
		<!--</select>-->
		<?php		
		$xres->addAssign('divXdestinatario', 'innerHTML', ob_get_clean());
		return utf8_encode($xres->getXML());		
	}	
	
	// Muestra el formulario para buscar radicados
	function showBusqRad($tipo=''){
		$xres=new xajaxResponse();
		ob_start();		
		?>
		<table>			
			<?php
			if($tipo=='c'){
				?>				
				<tr>
				<td width="" style="padding-left: 30px">
					<input type="radio" id="1" name="xanex" value="1" checked="checked">
					<b>Anexo o Asociado</b>
				</td>								
				<!--td>
					<input type="radio" id="2" name="xanex" value="2">
					<b>Asociado</b>
				</td-->				
				<td width="50%">
					&nbsp;
				</td>
				</tr>
				<?php
			}
			?>
			<tr>
				<td style="font-size: 12px; padding-left:50px" colspan="4">
					<b>Buscar Radicado:</b>								
					<input  type="text" id="xbusq" name="xbusq"  style="width:450px" class="textarea">		
					<input type="button" value="Buscar" class="botones" onclick="xajax_getSearchRad(document.getElementById('xbusq').value)">
				</td>
			</tr>
			<tr>
				<td colspan="4" align="left" style="padding-left: 50px">
					<div id="divXdestinatario">
					<select multiple size="10" style="width: 950px; color:gray" class="SELECT" id="xdestinatario" name="xdestinatario" onchange="xajax_getDataRad(this.value, xajax.getFormValues('form1'))">
						<option>No se encontraron resultados</option>
					</select>					
					</div>
					<br><br>	
				</td>
			</td>
		</table>
		<?php
		$xres->addAssign('xshowDest', 'innerHTML', ob_get_clean());
		return utf8_encode($xres->getXML());
	}	
	
	// Realiza la bï¿½squeda de radicados
	function getSearchRad($busqueda){
		$xres=new xajaxResponse();
		define('ADODB_ASSOC_CASE', 1);
		require('../include/adodb/adodb.inc.php');
		require_once("../include/xWebComponent/database/DBConnection.php");
		require_once("../include/xWebComponent/database/data.php");				
		$con=new DBConnection($dbdriver, $server, $user, $password, $database);			
		$query="SELECT R.RADI_NUME_RADI,
				  R.RADI_FECH_RADI,
				  R.RA_ASUN,
				  D.SGD_DIR_DIRECCION,
				  D.SGD_DIR_TELEFONO
				FROM RADICADO R
				INNER JOIN SGD_DIR_DRECCIONES D
				ON R.RADI_NUME_RADI = D.RADI_NUME_RADI				
				WHERE TO_CHAR(R.RADI_NUME_RADI,'99999999999999999999999') LIKE '%".trim($busqueda)."%'";
		$rs = $con->execQuery($query);				
		ob_start();
		?>
		<select size="10" style="width: 950px; color:gray" class="select" id="xdestinatario" name="xdestinatario" onchange="xajax_getDataRad(this.value, xajax.getFormValues('form1'))">
		<?php
		if($rs->fields['RADI_NUME_RADI']!=''){						
			while(!$rs->EOF){				
				$value=$rs->fields['RADI_NUME_RADI'];
				$cad=$rs->fields['RADI_NUME_RADI']." - ".utf8_decode($rs->fields['RA_ASUN'])." - ".utf8_decode($rs->fields['SGD_DIR_DIRECCION']);
				?>
				<option value="<?php echo $rs->fields['RADI_NUME_RADI']?>"><?php echo $cad ?></option>				
				<?php		
				$rs->moveNext();				
			}				
		}		
		?>
		</select>
		<?php		
		$xres->addAssign('divXdestinatario', 'innerHTML', ob_get_clean());
		return utf8_encode($xres->getXML());		
	}	

	// MODIFICADO PATCH 2012-11-29 
	// Copia al formulario de radicacion los datos del radicado
	function getDataRad($id, $form){
		$xres=new xajaxResponse();
		define('ADODB_ASSOC_CASE', 1);
		require('../include/adodb/adodb.inc.php');
		require_once("../include/xWebComponent/database/DBConnection.php");
		require_once("../include/xWebComponent/database/data.php");						
		$con=new DBConnection($dbdriver, $server, $user, $password, $database);				
		//RADI_NUME_RADI IN(20129980000231,20129980000241,20129980000251) CIUDADANO, ENTE MILITAR, FUNCIONARIO			
		//$xres->addAlert("Datos iniciales cargados.");	
		$query="SELECT 
					D.SGD_DIR_DIRECCION AS DIRECCION, 
					D.SGD_DIR_TELEFONO AS TELEFONO, 
					D.SGD_DIR_MAIL AS EMAIL,
					O.SGD_OEM_OEMPRESA, 
					C.SGD_CIU_NOMBRE||' '||C.SGD_CIU_APELL1||' '||C.SGD_CIU_APELL2 AS SGD_CIU_CIUDADANO,
					B.NOMBRE_DE_LA_EMPRESA,
					U.USUA_NOMB,
					D.SGD_DIR_NOMBRE AS DIGNATARIO,
					D.SGD_DIR_DOC AS DOCUMENTO,
					D.ID_CONT, 
					D.ID_PAIS,
					D.MUNI_CODI,
					D.DPTO_CODI,
					D.SGD_TRD_CODIGO AS XCODIGO,
					R.RA_ASUN AS XASUNTO,
					C.SGD_CIU_CODIGO,
                    O.SGD_OEM_CODIGO,
                    D.SGD_ESP_CODI,
					D.SGD_DOC_FUN,
					R.RADI_NUME_FOLIO,
					R.RADI_NUME_ANEXO,
					R.RADI_DESC_ANEX,
					R.RADI_DEPE_ACTU,
					D.SGD_DIR_NOMREMDES AS XNOMBRE,
					R.MREC_CODI,
					R.RADI_CUENTAI,
					R.SGD_SPUB_CODIGO
				FROM 
					SGD_DIR_DRECCIONES D INNER JOIN RADICADO R ON D.radi_nume_radi=R.RADI_NUME_RADI
					LEFT JOIN SGD_OEM_OEMPRESAS O ON D.SGD_OEM_CODIGO=O.SGD_OEM_CODIGO
					LEFT JOIN SGD_CIU_CIUDADANO C ON D.SGD_CIU_CODIGO=C.SGD_CIU_CODIGO
					LEFT JOIN USUARIO U ON D.SGD_DOC_FUN=U.USUA_DOC
					LEFT JOIN BODEGA_EMPRESAS B ON D.SGD_ESP_CODI=B.IDENTIFICADOR_EMPRESA
				WHERE 
					d.RADI_NUME_RADI='$id'";					
		
		
		$rs = $con->execQuery($query);					
		
		/*DATOS PARA STICKER*/
		if($form['modify']=='m'){
			$asunto = utf8_decode(trim($rs->fields['XASUNTO']));
			$destino =  $rs->fields['RADI_DEPE_ACTU'];			
			$anexo = utf8_decode($rs->fields['RADI_NUME_ANEXO']." Folios : ".$rs->fields['RADI_NUME_FOLIO']." ".$rs->fields['RADI_DESC_ANEX']);
			$usuario = $_SESSION['krd'];
		}	
		
		ob_start();
		$i=0;
		while(!$rs->EOF){		
			if($i==0){
				$xres->addAssign('xdocumento', 	'value', trim($rs->fields['DOCUMENTO']));
				//$xres->addAssign('xnombre', 	'value', trim($rs->fields['NOMBRE']));
				$xres->addAssign('xapell1', 'value', utf8_decode(trim($rs->fields['APELL1'])));
			 	$xres->addAssign('xapell2', 'value', utf8_decode(trim($rs->fields['APELL2'])));  
 				$xres->addAssign('xdireccion', 'value', utf8_decode(trim($rs->fields['DIRECCION'])));
				$xres->addAssign('xtelefono', 	'value', trim($rs->fields['TELEFONO']));
				$xres->addAssign('xmail', 		'value', utf8_decode(trim($rs->fields['EMAIL'])));		
				$xres->addAssign('xcodigo', 	'value', trim($rs->fields['XCODIGO']));
//				$xres->addAssign('xasunto', 	'value',           trim($rs->fields['XASUNTO']));
				$xres->addAssign('xasunto', 'value', utf8_decode(trim($rs->fields['XASUNTO'])));
				$xres->addAssign('xfoliosDoc', 	'value', trim($rs->fields['RADI_NUME_FOLIO']));
				$xres->addAssign('xfoliosAnex', 	'value', trim($rs->fields['RADI_NUME_ANEXO']));
				$xres->addAssign('xdescAnex', 	'value', utf8_decode(trim($rs->fields['RADI_DESC_ANEX'])));
				$xres->addAssign('xcuentaInterna', 	'value', utf8_decode(trim($rs->fields['RADI_CUENTAI'])));
				$query="SELECT DEPE_NOMB, DEPE_CODI FROM DEPENDENCIA WHERE DEPE_CODI=".$rs->fields['RADI_DEPE_ACTU'];
				$rs_D=$con->execQuery($query);
				$depContent="<B>".$rs_D->fields['DEPE_NOMB']."</B>";
				$depContent.="<input type='hidden' id='xAutocompleteDep_hidden' name='xAutocompleteDep_hidden' value='".$rs->fields['RADI_DEPE_ACTU']."'>";								
				$xres->addAssign('xdivDependencia', 	'innerHTML', $depContent);
				$query="SELECT PARAM_CODI, PARAM_VALOR FROM SGD_PARAMETRO WHERE PARAM_NOMB='CLASIFICACION_SEGURIDAD'";
				$rs_S=$con->execQuery($query);
				$xdefault=$rs->fields['SGD_SPUB_CODIGO'];				
				ob_start();
				?>
				<select class="select" id="xseguridad" name="xseguridad">				
					<?php
					while(!$rs_S->EOF){
						?>
						<option value="<?php echo $rs_S->fields['PARAM_CODI'] ?>"
							<?php if($xdefault==$rs_S->fields['PARAM_CODI']){ echo "selected=selected"; }?>>
							<?php echo $rs_S->fields['PARAM_VALOR']; ?>
						</option>
						<?php							
						$rs_S->moveNext();	
					}
					?>
				</select>
				<?php			
				$xres->addAssign('div_xseguridad', 	'innerHTML', ob_get_clean());
				
				$query="SELECT MREC_CODI, MREC_DESC FROM MEDIO_RECEPCION ";										
				$rs_M=$con->execQuery($query);
				$xdefault=$rs->fields['MREC_CODI'];				
				ob_start();
				?>
				<select class="select" id="xmedio" name="xmedio">				
					<?
					while(!$rs_M->EOF){
						?>
						<option value="<?php echo $rs_M->fields['MREC_CODI'] ?>"
							<?php if($xdefault==$rs_M->fields['MREC_CODI']){ echo "selected=selected"; }?>>
							<?php echo $rs_M->fields['MREC_DESC']; ?>
						</option>
						<?php							
						$rs_M->moveNext();	
					}
					?>
				</select>
				<?php
				$xres->addAssign('div_xmedio', 	'innerHTML', ob_get_clean());
			}	
			
			if($rs->fields['SGD_CIU_CODIGO']!=''){
				$xtipoBusq2=1;
				$xdestinatario=$rs->fields['SGD_CIU_CODIGO'];
				$xnombre=utf8_decode($rs->fields['SGD_CIU_CIUDADANO']);
			}else if($rs->fields['SGD_OEM_CODIGO']!=''){
				$xtipoBusq2=2;
				$xdestinatario=$rs->fields['SGD_OEM_CODIGO'];
				$xnombre=utf8_decode($rs->fields['SGD_OEM_OEMPRESA']);
			}else if($rs->fields['SGD_ESP_CODI']!=''){
				$xtipoBusq2=3;
				$xnombre=utf8_decode($rs->fields['NOMBRE_DE_LA_EMPRESA']);
				$xdestinatario=$rs->fields['SGD_ESP_CODI']; 
			}else if($rs->fields['SGD_DOC_FUN']!=''){
				$xtipoBusq2=4;
				$xnombre=utf8_decode($rs->fields['USUA_NOMB']);
				$xdestinatario=$rs->fields['SGD_DOC_FUN'];
			}		
			$xres->addAssign('xnombre', 	'value', utf8_decode(trim($xnombre)));
			$xres->addAssign('xdestinatario2', 	'value', utf8_decode($xdestinatario));
			$xres->addAssign('xtipoBusq2', 		'value', utf8_decode($xtipoBusq2));
						
			if($rs->fields['ID_CONT']!='' && $rs->fields['ID_PAIS']!='' && $rs->fields['DPTO_CODI']!='' && $rs->fields['MUNI_CODI']){			
				$query2="SELECT 		  
									COALESCE(M.MUNI_NOMB,' ')||' - '||COALESCE(D.DPTO_NOMB,' ')||' - '||COALESCE(P.NOMBRE_PAIS,' ') AS XCIUDAD
									FROM MUNICIPIO M
									INNER JOIN DEPARTAMENTO D
									ON D.DPTO_CODI = M.DPTO_CODI
									AND D.ID_CONT  = M.ID_CONT
									AND D.ID_PAIS  = M.ID_PAIS
									INNER JOIN SGD_DEF_PAISES P
									ON P.ID_CONT    = D.ID_CONT
									AND P.ID_PAIS   = D.ID_PAIS
									WHERE 
										M.MUNI_CODI='".$rs->fields['MUNI_CODI']."' AND
										D.DPTO_CODI='".$rs->fields['DPTO_CODI']."' AND
										P.ID_PAIS='".$rs->fields['ID_PAIS']."' AND
										P.ID_CONT='".$rs->fields['ID_CONT']."'								
									";								
				$rs2 = $con->execQuery($query2);	
				$xres->addAssign("xAutocompleteCiudad_text", "value", $rs2->fields['XCIUDAD']);
				$xres->addAssign("xAutocompleteCiudad_hidden", "value", $rs->fields['ID_CONT']."-".$rs->fields['ID_PAIS']."-".$rs->fields['DPTO_CODI']."-".$rs->fields['MUNI_CODI']);
			}		
			
			//$xnombres=trim($rs->fields['NOMBRE']." ".$rs->fields['APELL1']." - ".$rs->fields['APELL2']);		
			$xdatos=utf8_decode(trim($rs->fields['DIRECCION']." ".$rs->fields['TELEFONO']." - ".$rs->fields['EMAIL']));
			$xdignatario=utf8_decode(trim($rs->fields['DIGNATARIO']));
			
			if($rs->fields['MUNI_CODI']!=""){
				$xubicacion=$rs->fields['ID_CONT']."-".$rs->fields['ID_PAIS']."-".$rs->fields['DPTO_CODI']."-".$rs->fields['MUNI_CODI'];
			}else{
				$xubicacion="1-170-11-1";
			}			
			?>
				<tr>
					<td valign="top" align="center">
					<input type="hidden" id="xdir_tipo_<?php echo $i; ?>" name="xdir_tipo_<?php echo $i; ?>" value="<?php echo $xtipoBusq2; ?>">
					<input type="hidden" id="xDestinatario_<?php echo $i; ?>" name="xDestinatario_<?php echo $i; ?>" value="<?php echo $xdestinatario?>"/>
					<input type="hidden" id="xAutocompleteCiudad_hidden_<?php echo $i; ?>" name="xAutocompleteCiudad_hidden_<?php echo $i; ?>" value="<?php echo $xubicacion?>"/>
					<input type="hidden" id="xmail_<?php echo $i; ?>" name="xmail_<?php echo $i; ?>" value="<?php echo trim($rs->fields['EMAIL']); ?>"/>
					<input type="hidden" id="xdireccion_<?php echo $i; ?>" name="xdireccion_<?php echo $i; ?>" value="<?php echo utf8_decode(trim($rs->fields['DIRECCION']));?>"/>
					<input type="hidden" id="xtelefono_<?php echo $i; ?>" name="xtelefono_<?php echo $i; ?>" value="<?php echo trim($rs->fields['TELEFONO']);?>"/>
					<input type="hidden" id="apell2_<?php echo $i; ?>" name="apell2_<?php echo $i; ?>" value="<?php echo utf8_decode(trim($rs->fields['APELL2']));?>"/>
					<input type="hidden" id="xapell1_<?php echo $i; ?>" name="xapell1_<?php echo $i; ?>" value="<?php echo utf8_decode(trim($rs->fields['APELL1']));?>"/>
					<input type="hidden" id="xnombre_<?php echo $i; ?>" name="xnombre_<?php echo $i; ?>" value="<?php echo utf8_decode(trim($rs->fields['XNOMBRE'])).' '.utf8_decode(trim($rs->fields['APELL1'])).' '.utf8_decode(trim($rs->fields['APELL2'])); ?>"/>
					<!-- trim($rs->fields['XNOMBRE']);  -->
					<input type="hidden" id="sgd_dir_codigo_<?php echo $i; ?>" name="sgd_dir_codigo_<?php echo $i; ?>"/>
					<input type="hidden" name="xdocumento_<?php echo $i; ?>" id="xdocumento_<?php echo $i; ?>" readonly value="<?php echo trim($rs->fields['DOCUMENTO']) ?>" />						
						<input type="text" class="textarea" style="width:300px" name="xnombres_<?php echo $i; ?>" id="xnombres_<?php echo $i; ?>" readonly value="<?php echo trim($xnombre) ?>" />
					</td>							
					<td valign="top" align="center">
						<input type="text" class="textarea" style="width:300px" name="xdatos_<?php echo $i; ?>" id="xdatos_<?php echo $i; ?>" readonly value="<?php echo trim($xdatos) ?>" />
					</td>
					<td valign="top" align="center">
						<input type="text" class="textarea" style="width:300px" name="xdignatario_<?php echo $i; ?>" id="xdignatario_<?php echo $i; ?>" value="<?php echo trim($xdignatario) ?>" />
					</td>
					<td valign="top" align="center">
							<input type="checkbox" id="xSelect_<?php echo $i; ?>" name="xSelect_<?php echo $i; ?>" value="<?php echo $i; ?>" checked />
					</td>					
				</tr>			
			<?php
			$i++;
			$rs->moveNext();
		}
		/*******************************/
		
		
		$xres->addAssign("contRemit","value", $i);
		$xres->addAppend("xDivRemit", "innerHTML", ob_get_clean());
		$xres->addScript("document.getElementById('btnRadicar').scrollIntoView(true)");		
		//$cad=ob_get_clean();
	
		if($form['xanex']!='' || $form['modify']=='m'){
			$query3="SELECT D.DEPE_NOMB, U.USUA_NOMB
						  FROM RADICADO R
						  INNER JOIN DEPENDENCIA D ON R.RADI_DEPE_ACTU=D.DEPE_CODI
						  LEFT JOIN USUARIO U ON R.RADI_USUA_ACTU=U.USUA_CODI AND R.RADI_DEPE_ACTU=U.DEPE_CODI
						  WHERE R.RADI_NUME_RADI='$id'	";								
			$rs3 = $con->execQuery($query3);	
		}
		// Consulto la dependencia y el usuario actual del radicado al que se esta anexando o asociando
		if($form['xanex']!=''){			
			ob_start();
			?>
			<table align="center" class="listado2">
				<tr>
					<td align="center" colspan="2">
						Datos radicado relacionado <b><?php echo $id;?></b>
					</td>					
				<tr>
				<tr>
					<td align="right">
						<b>Dependencia actual:</b>
					</td>
					<td>
					<?php echo $rs3->fields['DEPE_NOMB']; ?>
					</td>
				<tr>
				<tr>
					<td align="right">
						<b>Usuario actual:</b>
					</td>
					<td>
					<?php echo utf8_decode($rs3->fields['USUA_NOMB']); ?>
					</td>
				<tr>
			</table>
			<?php
			$xres->addAssign("xDatosRad", "innerHTML", ob_get_clean());
		}
		if($form['modify']=='m'){
			$xres->addAssign("hiddenRadicar", "value", "m");
			$xres->addAssign("xradicado", "value", "$id");			
			$remitente =  $xnombre;
			$tipoRad=substr($id, strlen($id)-1, strlen($id));			
			if($tipoRad==2){
				$textRemit="Remitente: ";
			}else{
				$textRemit="Destinatario: ";
			}
			ob_start();
			?>
			<table align="center" class="listado2">
				<tr>
					<td align="center" colspan="2">
						Radicado actual: <font style="color:green; font-size:18px"><b><?php echo $id;?></b></font>
					</td>					
			
					<td align="center">&nbsp;|||
						<a href="javascript:imprSelec('divRad')">Imprimir Sticker </a>|||
					</td>					
				</tr>
			</table>
			<div id="divRad" style="display:none">
			<?php	
				include("./paramSticker.php");				
			?>
			<table width="<?php echo $widthSticker; ?>">
			<tr>
			<!--td style="font-size:6px" align="left" valign="top"-->
			<td style="font-size:4px" align="center" >
			<img src="../png/logo2_<?php echo 'IDARTES'; ?>.jpg" valign="top" width="<?php echo $imageWidthStricker; ?>">
			<br>Orfeo GPL
			</td>
			<td style="font-size:<?php echo $fontSizeSticker; ?>">
			<!--font face="CCode39" size="2"> <?php echo $id //$noRad ?><br></font-->
			<font face="code3of9" size="5"> <?php echo $id //$noRad ?><br></font>
			<font size="3"> <?php echo $id /*$noRad*/ ." - ".date("d/m/Y"); ?></font><br>
			<?php echo "Asunto: ".ucfirst(strtolower(substr($asunto,0,45))).""; ?><br>
			<?php echo "$textRemit".ucfirst(strtolower(substr($remitente,0,40))).""; ?><br>
			<?php echo "Destino: ".substr($destino,0,9).""; ?>
			<?php echo " - Anexos: ".ucfirst(strtolower(substr($anexo,0,30))).""; ?><br>
			<?php echo "Radicado por: $usuario"; ?>
			<?php echo ", IDARTES, Tel.3795750"; ?>
			<br>
			</td>
			</tr>
			</table>									
			</div>		
			<?php
			$xres->addAssign("xDatosRad", "innerHTML", ob_get_clean());
		}
		$xres->addScript("document.getElementById('btnRadicar').scrollIntoView(true)");	
		return utf8_encode($xres->getXML());	
	}

	
	// Genera el radicado
	function radicar($form){
		$xres=new xajaxResponse();			
		$ruta_raiz = "..";
		$accion=$form['hiddenRadicar'];				
		include "$ruta_raiz/include/db/ConnectionHandler.php";		
		$db = new ConnectionHandler("$ruta_raiz");
		define('ADODB_FORCE_VALUE',3);
		error_reporting(7);				
		$db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
		//$db->conn->debug=true;		
		// extraigo continente, pais, depto y municipio		
		$arrayUbicacion=explode("-", $form['xAutocompleteCiudad_hidden']);		
		// CREO EL NUEVO RADICADO		
		include_once("../include/tx/Radicacion.php");		
		$rad = new Radicacion($db);	
		$rad->radiTipoDeri = 3;
		if($form['ent']==2){			
			$query="SELECT U.CODI_NIVEL FROM USUARIO U WHERE USUA_LOGIN LIKE '".$form["xapell2"]."'";
			$rs=$db->query($query);
			if($rs->fields['CODI_NIVEL']!=''){
				$rad->nivelRad=$rs->fields['CODI_NIVEL'];
			}else{
				$rad->nivelRad=1;
			}
		}else{
			$rad->nivelRad = $_SESSION['nivelus'];
		}


		$rad->radiCuentai = "'0'";	
		$usua_doc=$_SESSION['usua_doc'];		
		$rad->eespCodi =  0;
		$rad->mrecCodi =  $form['xmedio'];		
		//$fecha_gen_doc_YMD = substr($fecha_gen_doc,6 ,4)."-".substr($fecha_gen_doc,3 ,2)."-".substr($fecha_gen_doc,0 ,2);
		$rad->radiFechOfic =  date("YYYY-mm-dd");				 
		// Asociado radi_tipo_deri=2
		// Anexo radi_tipo_deri=0
		if($form['xanex']!=''){
			$rad->radiNumeDeri=$form['xdestinatario'];
			if($form['xanex']==1){
				//$xres->addAlert("Anexo");
				$rad->radiTipoDeri=0;
			}else{
				//$xres->addAlert("Asociado");				
				$rad->radiTipoDeri=2;
			}	
		}		
					
		$rad->radiPais =  $arrayUbicacion[1];
		
		// Modified by patch
		if($form['xdescAnex']!=''){
			$rad->descAnex = $form['xdescAnex'];			
		}
		if($form['xfoliosDoc']!=''){
			$rad->nofolios=$form['xfoliosDoc'];
		}
		if($form['xfoliosAnex']!=''){
			$rad->noanexos=$form['xfoliosAnex'];
		}				
		$rad->raAsun = trim($form['xasunto']);
		$coddepe="'".$_SESSION['depecodi']."'";
		$rad->radiDepeActu = $form['xAutocompleteDep_hidden'];
 		$rad->radiDepeRadi = $_SESSION['dependencia'];//$form['xAutocompleteDep_hidden'];
	

		// Si el radicado es de entrada busco el usuario jefe, de lo contrario se coloca el usuario de sesiÃ³n.
			$valido = $form['ent'];
			
			/*switch ($valido){
			case '2':
			break;
			}*/
		if($form['ent']!=2 || $form['ent']!=4 ){			
			$codusuario=$_SESSION['codusuario'];
			$usua_actu=$_SESSION['krd'];		
		}
		if(($form['ent']==2) || ($form['ent']==4 )){			
			$query="SELECT U.USUA_LOGIN FROM USUARIO U WHERE U.DEPE_CODI='".$form['xAutocompleteDep_hidden']."' AND U.USUA_CODI='1'";
			$rs=$db->query($query);			
			$codusuario=1;
			if($rs->fields['USUA_LOGIN']!=''){
				$usua_actu=$rs->fields['USUA_LOGIN'];
			}
		}				
		$rad->radiUsuaActu = "$codusuario";
		$rad->trteCodi =  "0";
		$rad->tdocCodi="0";
		$rad->tdidCodi="0";			
		if($form['ent']==2 ){
			$rad->carpCodi = "0";
		}else{
			$rad->carpCodi = $form['ent'];			
		}			
		
		$rad->carPer = "0";
		$rad->trteCodi="null";
		$rad->ra_asun = $rad->raAsun;
		$rad->radiPath = 'null';			
		$rad->sgd_apli_codi = 0;		
		$rad->radiCuentai="'".$form['xcuentaInterna']."'";
		if(!$rad->radiCuentai){
			$rad->radiCuentai=0;
		}
		$rad->radiFechRadi=$form['xfechaActual'];
		$rad->radiFechOfic=$form['xfechaRad'];
		$rad->nivelRad=$form['xseguridad'];	
		if($rad->nivelRad==""){
			$rad->nivelRad=0;	
		}
		$query="SELECT DEPE_RAD_TP".$form['ent']." AS SEC FROM DEPENDENCIA WHERE DEPE_CODI=".$_SESSION['dependencia'];
		$rs=$db->query($query);
		$DEPE_TP=$rs->fields['SEC'];	
		if($accion=='r' && $form['contRemit']>1){
			$xres->addAlert(utf8_decode("Radicado para los ".$form['contRemit']." destinatarios seleccionados."));		
		}
		
		if($accion!='r'){	
		// Elimina los datos actuales en sgd dir drecciones	
			$noRad=$form['xradicado'];		
			$query="DELETE FROM SGD_DIR_DRECCIONES WHERE RADI_NUME_RADI='$noRad'";
			$rs=$db->query($query);
		}
		// Modificado PATCH - 2012-12-05
		// Agregado para guardar varios remitentes en sgd_dir_drecciones
		for($i=0; $i<=($form['contRemit']-1); $i++){		
			// Ingresamos el dato a SGD_DIR_DIRECCIONES
			// consulto el codigo mayor en sgd_dir_direcciones		
			$arrayUbicacion=explode("-", $form["xAutocompleteCiudad_hidden_$i"]);
			/* $query="SELECT SEC_DIR_DIRECCIONES.NEXTVAL AS MAYOR FROM DUAL";						
			
			$rs3=$db->query($query);			
			$sgd_dir_codigo=$rs3->fields['MAYOR'];	*/

			
			$sgd_dir_codigo=$db->conn->NextId('SEC_DIR_DIRECCIONES');
			/*if($form['xtipoBusq2']!=''){
				$tipoBusq=$form['xtipoBusq2'];						
			}else{
				$tipoBusq=$form['tipoBusq'];
			}*/
			$tipoBusq=$form["xdir_tipo_$i"];
					
			/*if($form['xdestinatario2']!=''){
				$xdestinatario=$form['xdestinatario2'];			
			}else{*/
				$xdestinatario=$form["xDestinatario_$i"];				
			//}				
			switch($tipoBusq){			
				case 1:// Si es ciudadano				
					$sgd_ciu_codigo=$xdestinatario;	
					$sgd_doc_fun='null' ;
					$sgd_oem_codigo='null' ;
					$sgd_esp_codigo='null' ;					
				break;
				case 2:// si es empresa
					$sgd_oem_codigo=$xdestinatario;
					$sgd_doc_fun='null' ;
					$sgd_ciu_codigo='null' ;
					$sgd_esp_codigo='null' ;
				break;
				case 3:// Si es un concesionario
					$sgd_esp_codigo=$xdestinatario;				
					$sgd_oem_codigo='null' ;
					$sgd_ciu_codigo='null' ;
					$sgd_doc_fun='null' ;
				break;				
				case 4:// Si es funcionario
			 		$sgd_doc_fun=$xdestinatario;
					$sgd_oem_codigo='null' ;
					$sgd_ciu_codigo='null' ;
					$sgd_esp_codigo='null' ;
				break;			
			}	
			$sgd_dir_tipo=$tipoBusq;			
			include_once("../include/tx/Tx.php");			
			if($accion=='r'){	
				if($i==0){						
					$noRad = $rad->newRadicado($form['ent'], $DEPE_TP);					
					$hist = new Historico($db); 
					$codTx = 2;			
					$radicadosSel[0] = $noRad;
					$hist->insertarHistorico($radicadosSel,  $rad->radiDepeRadi , $_SESSION['codusuario'], $rad->radiDepeActu, $rad->radiUsuaActu , " ", $codTx);
					$xres->addAssign("xradicado", "value", $noRad);											
/*					if ($form['ent']==7){
					
					// MODIFICADO DIC-23-2012 PATCH
					$query="SELECT max(id) as MAYOR FROM sgd_pqr_radicados";
					$rs=$db->query($query);						
					$query="insert into sgd_pqr_radicados (id,radi_nume_radi,division,usua_login) 
						values(".($rs->fields['MAYOR']+1).",'".$noRad."','".$form['xAutocompleteDep_hidden']."','".$usua_actu."');";			
					$db->query($query);
					}
*/				}
				if ($form["xSelect_$i"]!=""){
					$nulos='NULL';
					//$sgd_dir_tipo //1 por defecto, 2 empresa?
					$query="INSERT INTO SGD_DIR_DRECCIONES 
						  (SGD_DIR_CODIGO, SGD_DIR_TIPO, RADI_NUME_RADI, MUNI_CODI, DPTO_CODI, 
						  SGD_DIR_DIRECCION, SGD_SEC_CODIGO, SGD_DIR_NOMBRE, SGD_DOC_FUN, 
						  SGD_DIR_NOMREMDES, SGD_TRD_CODIGO, ID_PAIS, ID_CONT, sgd_oem_codigo, sgd_ciu_codigo, sgd_esp_codi,
						  SGD_DIR_MAIL, SGD_DIR_TELEFONO, SGD_DIR_TELCELULAR, SGD_DIR_DOC) 
						VALUES 
						  ('$sgd_dir_codigo', '1', '$noRad', '".$arrayUbicacion[3]."', '".$arrayUbicacion[2]."', 
						   '".$form["xdireccion_$i"]."', '0', '".$form["xdignatario_$i"]."', $sgd_doc_fun, 
						   '".$form["xnombre_$i"]."', '1', '".$arrayUbicacion[1]."', '".$arrayUbicacion[0]."', $sgd_oem_codigo, $sgd_ciu_codigo, $sgd_esp_codigo,
						   '".$form["xmail_$i"]."', '".$form["xtelefono_$i"]."', '".$form["xmovil_$i"]."', '".$form["xdocumento_$i"]."')";						
					//$xres->addAlert($query);
					$xres->addAssign("sgd_dir_codigo_$i","value", $sgd_dir_codigo);
					$rs=$db->query($query);				
				}
				ob_start();			
				if($rs){				
					?>				
						<center>
							<img src='./icons/radicado_icon.png'/>
								Se gener&oacute; el radicado:
								<font size='4' face='Arial' color='red'><b><?php echo '['.$noRad.']' ?></b></font>&nbsp;&nbsp;|||								

<!--<a href="#"  class=vinculos onClick="window.open ('../radicacion/stickerWeb/index.php?<?/*=$varEnvio*/?>&alineacion=Center','sticker<?=$noRad?>','menubar=0,resizable=0,scrollbars=0,width=450,height=180,toolbar=0,location=0');">.</a>-->
		<a href="javascript:imprSelec('divRad')"> Imprimir Sticker </a>|||&nbsp;&nbsp;
			<div id="divRad" style="display:none">
			<?php	
				include("./paramSticker.php");
				$asunto = utf8_decode($rad->raAsun);
				$destino =  $rad->radiDepeActu;
				$remitente =  utf8_decode($form["xnombre_$i"]);
				$anexo =utf8_decode($rad->noanexos." Folios : ".$rad->nofolios."-".$rad->descAnex);
				$usuario = $_SESSION['krd'];
				if($form['ent']==2){
					$textRemit="Remitente: ";
				}else{
					$textRemit="Destinatario: ";
				}
			?>
			<table width="<?php echo $widthSticker; ?>">
			<tr>
			<!--td style="font-size:6px" align="left" valign="top"-->
			<td style="font-size:4px" align="center" >
			<img src="../png/logo2_<?php echo 'IDARTES'; ?>.jpg" valign="top" width="<?php echo $imageWidthStricker; ?>">
			<br>Orfeo GPL
			</td>
			<td style="font-size:<?php echo $fontSizeSticker; ?>">
			<!--font face="CCode39" size="2"> <?php echo $noRad ?><br></font-->
			<font face="code3of9" size="2"> <?php echo $noRad ?><br></font>
			<font size="3"> <?php echo $noRad." - ".date("d/m/Y"); ?></font><br>
			<?php echo "Asunto: ".ucfirst(strtolower(substr($asunto,0,45))).""; ?><br>
			<?php echo "$textRemit".ucfirst(strtolower(substr($remitente,0,40))).""; ?><br>
			<?php echo "Destino: ".substr($destino,0,9).""; ?>
			<?php echo " - Anexos: ".ucfirst(strtolower(substr($anexo,0,30))).""; ?><br>
			<?php echo "Radicado por: $usuario"; ?>
			<?php echo ", IDARTES, Tel.3795750"; ?>
			<br>
			</td>
			</tr>
			</table>									
			</div>
			<a href="../verradicado.php?verrad=<?php echo $noRad."&menu_ver_tmp=2&".session_name()."=".session_id()."&krd=".$_SESSION['krd']."&datoVer=985&verradPermisos='Full'"?>">								

<font size='2' face='Arial' color='darkblue'><b><u> Anexar Plantilla </u></b></font>
</a>

                            <style  >
                                .btn.yellow:not(.btn-outline) {
                                    color: #fff;
                                    background-color: #c49f47;
                                    border-color: #c49f47;
                                }
                                .btn.yellow:not(.btn-outline) {
                                    color: #fff;
                                    background-color: #c49f47;
                                    border-color: #c49f47;
                                }
                                .btn {
                                    outline: 0!important;
                                }
                                .btn, .form-control {
                                    box-shadow: none!important;
                                }
                                .btn {
                                    outline: 0!important;
                                }
                                .btn, .form-control {
                                    box-shadow: none!important;
                                }
                                .btn-lg, .btn-group-lg > .btn {
                                    padding: 10px 16px;
                                    font-size: 18px;
                                    line-height: 1.33333;
                                    border-radius: 6px;
                                }
                                .btn {
                                    display: inline-block;
                                    margin-bottom: 0;
                                    font-weight: normal;
                                    text-align: center;
                                    vertical-align: middle;
                                    touch-action: manipulation;
                                    cursor: pointer;
                                    background-image: none;
                                    border: 1px solid transparent;
                                    white-space: nowrap;
                                    padding: 6px 12px;
                                    font-size: 14px;
                                    line-height: 1.42857;
                                    border-radius: 4px;
                                    -webkit-user-select: none;
                                    -moz-user-select: none;
                                    -ms-user-select: none;
                                    user-select: none;
                                }
                                a, button, code, div, img, input, label, li, p, pre, select, span, svg, table, td, textarea, th, ul {
                                    -webkit-border-radius: 0!important;
                                    -moz-border-radius: 0!important;
                                    border-radius: 0!important;
                                }
                                input, button, select, textarea {
                                    font-family: inherit;
                                    font-size: inherit;
                                    line-height: inherit;
                                }
                                button, html input[type="button"], input[type="reset"], input[type="submit"] {
                                    -webkit-appearance: button;
                                    cursor: pointer;
                                }
                                button, select {
                                    text-transform: none;
                                }
                                button {
                                    overflow: visible;
                                }
                                button, input, optgroup, select, textarea {
                                    color: inherit;
                                    font: inherit;
                                    margin: 0;
                                }

                            </style>
                            <button type="button" id="div_expediente" class="btn btn-lg yellow  "
                                    style="
    border-radius: 20px !important;
    display: block;"
                                    onclick="redirecModalexpediente('<?php echo $noRad ?>')">
                                <i class="fa fa-folder-open-o" data-toggle="tooltip"
                                   title="Incluir en expediente"></i>Expediente
                            </button>
                            <br><br>
<?php 
	if($form['ent']==2 || $form['ent']==4 ){
	$query="SELECT DEPE_CODI, DEPE_NOMB FROM DEPENDENCIA WHERE DEPE_ESTADO=1 ORDER BY depe_nomb ASC";
	$rsDep=$db->query($query);						
?>
	<table class="listado2">
	<tr>
		<td align='right' width="200px">
		<b>Enviar copia:</b><br>
		Seleccione el usuario y dependencia, por defecto sino se selecciona el usuario se informar&aacute; al jefe.
		</td>
		<td>
		<select size="5" id="xdepeInfor" name="xdepeInfor" class="select" onchange="xajax_loadUsua(this.value);">
	<?php
		while(!$rsDep->EOF){
	?>
	<option value="<?php echo $rsDep->fields['DEPE_CODI']?>"> <?php echo utf8_decode($rsDep->fields['DEPE_NOMB'])?></option>

	<?php
																					
	$rsDep->moveNext();
				}
	?>
	</select>
		</td>
		<td width="200px">
		<div id="divXusuaInfor">
	<select size="5" id="xusuaInfor" name="xusuaInfor" class="select"> </select>
	</div>
		</td>
		</tr>
		<tr>
		<td align="center" colspan="3">
	<input type="button" value="Informar" class="botones" onclick="xajax_informar(xajax.getFormValues('form1'))">
	</td>
	</tr>
	</table>

	<?php
		$rsDep->moveNext();
				}
	?>
	</center>

	<?php								
					// modifico el boton radicar - modificar y muestro el cuadro para que se informe
					$xres->addAssign("btnRadicar", "value", "Modificar");
					$xres->addAssign("hiddenRadicar", "value", "m");
				}else{				
					?>
					<center>
						<font size='4' face='Arial' color='red'>
							<b><u>Error al radicar, verifique los datos del destinatario.</u></b>
						</font><br>					
					</center>
					<?php				
				}	
				$xres->addAssign("xDivRadicado","innerHTML", ob_get_clean());		
			}else{				
				if($i==0){
					// Comparo los valores del radicado con los que tiene actualmente
					$query="SELECT 
								R.RA_ASUN AS XASUNTO,
								R.RADI_NUME_FOLIO,
								R.RADI_NUME_ANEXO,
								R.RADI_DESC_ANEX,								
								R.MREC_CODI,
								R.RADI_CUENTAI,
								R.SGD_SPUB_CODIGO
							FROM 
								RADICADO R					
							WHERE 
								R.RADI_NUME_RADI='$noRad'";
					$rs_R=$db->query($query);					
										
					if(trim($rs_R->fields['XASUNTO'])!=trim($form['xasunto'])){
						$msg.="Asunto,";
					}
					if($rs_R->fields['RADI_NUME_FOLIO']!=$form['xfoliosDoc']){
						$msg.="Folios,";
					}
					if($rs_R->fields['RADI_NUME_ANEXO']!=$form['xfoliosAnex']){
						$msg.="Anexos,";
					}
					if($rs_R->fields['RADI_DESC_ANEX']!=$form['xdescAnex']){
						$msg.="Descripci&oacute;n Anexos,";
					}
					if($rs_R->fields['MREC_CODI']!=$form['xmedio']){
						$msg.="Medio de Recepci&oacute;n,";
					}
					if($rs_R->fields['RADI_CUENTAI']!=$form['xcuentaInterna']){
						$msg.="Referencia,";
					}
					if($rs_R->fields['SGD_SPUB_CODIGO']!=$form['xseguridad']){
						$msg.="Nivel de Seguridad,";
					}
					$msg="Modificado: ".substr($msg, 0, strlen($msg)-1);
					
					$rad->updateRadicado($noRad);					
					$hist = new Historico($db);
					$codTx = 11;
					$radicadosSel[0] = $form['xradicado'];
					$hist->insertarHistorico($radicadosSel,  $rad->radiDepeRadi , $_SESSION['codusuario'], $rad->radiDepeActu, $rad->radiUsuaActu , "$msg", $codTx);
				}
				// Inserta nuevamente los datos				
				if ($form["xSelect_$i"]!=""){
					$query="INSERT INTO SGD_DIR_DRECCIONES 
							  (SGD_DIR_CODIGO, SGD_DIR_TIPO, RADI_NUME_RADI, MUNI_CODI, DPTO_CODI, 
							  SGD_DIR_DIRECCION, SGD_SEC_CODIGO, SGD_DIR_NOMBRE, SGD_DOC_FUN, 
							  SGD_DIR_NOMREMDES, SGD_TRD_CODIGO, ID_PAIS, ID_CONT, sgd_oem_codigo, sgd_ciu_codigo, sgd_esp_codi,
							  SGD_DIR_MAIL, SGD_DIR_TELEFONO, SGD_DIR_TELCELULAR, SGD_DIR_DOC) 
							VALUES
								('$sgd_dir_codigo', '1', '$noRad', '".$arrayUbicacion[3]."', '".$arrayUbicacion[2]."', 
						 		'".$form["xdireccion_$i"]."', '0', '".$form["xdignatario_$i"]."', $sgd_doc_fun, 
								'".$form["xnombre_$i"]."', '1', '".$arrayUbicacion[1]."', '".$arrayUbicacion[0]."', $sgd_oem_codigo, $sgd_ciu_codigo, $sgd_esp_codigo,
 	 	 					   '".$form["xmail_$i"]."', '".$form["xtelefono_$i"]."', '".$form["xmovil_$i"]."', '".$form["xdocumento_$i"]."')";
					$xres->addAssign("sgd_dir_codigo_$i","value", $sgd_dir_codigo);
					//$xres->addAlert($query);
				}				
				$rs=$db->query($query);
				if($rs && $i==0){
					$xres->addAlert("Modificado correctamente.");
				}
			}
			
		}		
		return utf8_encode($xres->getXML());		
	}
	
	// Carga el combo de usuarios de una dependencia - Opcion informar despue de radicar.
	function loadUsua($depe_codi){
		$xres=new xajaxResponse();
		define('ADODB_ASSOC_CASE', 1);
		require('../include/adodb/adodb.inc.php');
		require_once("../include/xWebComponent/database/DBConnection.php");
		require_once("../include/xWebComponent/database/data.php");				
		$con=new DBConnection($dbdriver, $server, $user, $password, $database);			
		$query="SELECT 
					USUA_CODI,
					USUA_NOMB					
				FROM 
					USUARIO
				WHERE 
					DEPE_CODI='$depe_codi'
					AND USUA_ESTA= '1' ";						
		$rs = $con->execQuery($query);				
		ob_start();
		?>
		<select size="5" id="xusuaInfor[]" name="xusuaInfor[]" class="select" multiple="multiple">	
			<?php
			while(!$rs->EOF){
				?>
				<option value="<?php echo $rs->fields['USUA_CODI'] ?>"><?php echo utf8_decode($rs->fields['USUA_NOMB']) ?></option>
				<?php
				$rs->moveNext();
			}
			?>
		</select>
		<?php		
		$xres->addAssign('divXusuaInfor', 	'innerHTML', ob_get_clean());
		return utf8_encode($xres->getXML());	
	}
	
	// Realizo la transaccion de informar despues de radicar.
	function informar($form){
		$xres=new xajaxResponse();
		include "../include/db/ConnectionHandler.php";		
		$db = new ConnectionHandler("..");
		error_reporting(7);		
		$db->conn->SetFetchMode(ADODB_FETCH_ASSOC);		
		$ruta_raiz="..";		
		require "../include/tx/Radicacion.php";
		require "../include/tx/Tx.php";
		$xradicado[]=$form['xradicado'];		
		$xasunto=utf8_encode(trim($form['xasunto']));
		$xdependencia=$form['xdepeInfor'];		
		$xusuaInfor=$form['xusuaInfor'];
		// Hago el informado por cada usuario
		if($xdependencia!=''){
			$tx=new Tx($db);						
			if($xusuaInfor){ // informo a los usuarios seleccionados				
				foreach($xusuaInfor as $xusuario){		
					$usCodDestino = $tx->informar($xradicado, $_SESSION['krd'], $xdependencia, $_SESSION['dependencia'], $xusuario, $_SESSION['codusuario'], $xasunto, $_SESSION['usua_doc'],"..");					
				}			
			}else{ // informo al jefe
				$xusuario=1;
				$usCodDestino = $tx->informar($xradicado, $_SESSION['krd'], $xdependencia, $_SESSION['dependencia'], $xusuario, $_SESSION['codusuario'], $xasunto, $_SESSION['usua_doc']);		
			}
		}		
		$xres->addAlert(utf8_decode("Se informo correctamente."));
		return utf8_encode($xres->getXML());	
	}
		

	// Cargar datos del radicado en el formulario
	function cargarDatosRadicado($numRad){ 
		$xres=new xajaxResponse();
		include "../include/db/ConnectionHandler.php";
		$db = new ConnectionHandler("..");
		error_reporting(7);
		$db->conn->SetFetchMode(ADODB_FETCH_ASSOC);	
		
		$query="SELECT 
				  R.RADI_DESC_ANEX as XDESCANEX,   
				  R.MREC_CODI as XMEDIO, 
				  R.RADI_NUME_HOJA as xfoliosDoc, 
				  R.MEDIO_M as xfoliosAnex, 
				  R.RA_ASUN as xasunto,  
				  R.RADI_DEPE_ACTU as XAUTOCOMPLETEDEP_HIDDEN,
				  D.DEPE_NOMB,
				  R.RADI_CUENTAI as xcuentaInterna,
				  R.RADI_FECH_RADI as xfechaActual,
				  R.RADI_FECH_OFIC as xfechaRad,
				  R.SGD_SPUB_CODIGO as xseguridad
				FROM RADICADO R INNER JOIN DEPENDENCIA D ON R.RADI_DEPE_ACTU=D.DEPE_CODI
				WHERE RADI_NUME_RADI='$numRad'";

		$rs=$db->query($query);
		
		$xres->addAssign("xdescAnex","value", $rs->fields['XDESCANEX']);		
		$xres->addAssign("xfoliosDoc","value", $rs->fields['XFOLIOSDOC']);		
		$xres->addAssign("xfoliosAnex","value", $rs->fields['XFOLIOSANEX']);
		$xres->addAssign("xasunto","value", trim($rs->fields['XASUNTO']));
		$xres->addAssign("xcuentaInterna","value", $rs->fields['xcuentaInterna']);		
		$xres->addAssign("xAutocompleteDep_hidden","value", $rs->fields['XAUTOCOMPLETEDEP_HIDDEN']);
		$xres->addAssign("xAutocompleteDep_text","value", $rs->fields['DEPE_NOMB']);
		$xres->addAssign("xfechaActual","value", $rs->fields['XFECHAACTUAL']);
		$xres->addAssign("xfechaRad","value", $rs->fields['XFECHARAD']);
		
		$xres->addAssign("ent","value", substr($numRad, strlen($numRad)-1, 1));		
		$xres->addAssign("xDivRadicado","innerHTML", "<center><font size='4' face='Arial' color='red'><b><u>$numRad</u></b></font></center><br>");
		
		// Cargo el nivel de seguridad
		
		$query="SELECT PARAM_CODI, PARAM_VALOR FROM SGD_PARAMETRO WHERE PARAM_NOMB='CLASIFICACION_SEGURIDAD'";
		$rs2=$db->query($query);
		ob_start();
		?>
		<select class="select" id="xseguridad" name="xseguridad">
		<?php 
		while(!$rs2->EOF){
			?><option value="<?php echo $rs2->fields['PARAM_CODI'] ?>"
					<?php if($rs->fields['XSEGURIDAD']==$rs2->fields['PARAM_CODI']){ echo "SELECTED=SELECTED";} ?> >
				<?php echo $rs2->fields['PARAM_VALOR'] ?></option>		
			<?php							
			$rs2->moveNext();	
		}		
		?>
		</select>
		<?php
		$xres->addAssign("xseguridad","innerHTML", ob_get_clean());

		
		// Cargo el medio
		$query="SELECT MREC_CODI, MREC_DESC FROM MEDIO_RECEPCION";
		$rs3=$db->query($query);
		ob_start();
		?>
		<select class="select" id="xmedio" name="xmedio">
		<?php
		while(!$rs3->EOF){
			?>			
				<option value="<?php echo $rs3->fields['MREC_CODI'] ?>"
					<?php if($rs->fields['XMEDIO']==$rs3->fields['MREC_CODI']){ echo "selected=selected"; }?>>
					<?php echo $rs3->fields['MREC_DESC']; ?>
				</option>			
			<?php							
			$rs3->moveNext();	
		}
		?>
		</select>
		<?php 
		$xres->addAssign("xmedio","innerHTML", ob_get_clean());
		
		// Cargo la dependencia
		$xres->addAssign("xdivDependencia","innerHTML", $rs->fields['DEPE_NOMB']);		
		$xres->addAppend("xdivDependencia","innerHTML", "<input type='hidden' id='xAutocompleteDep_hidden' name='xAutocompleteDep_hidden' value='".$rs->fields['XAUTOCOMPLETEDEP_HIDDEN']."'>");		
		$xres->addScript("xajax_getDataRad('$numRad','')");
		
		$xres->addAssign("xradicado", "value", "$numRad");
		$xres->addAssign("btnRadicar", "value", "Modificar");
		$xres->addAssign("hiddenRadicar", "value", "m");		
		return utf8_encode($xres->getXML());
	}
?>
