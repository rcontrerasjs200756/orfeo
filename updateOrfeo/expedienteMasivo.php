<?php session_start();
////////////////////////////////////////////////////////////////////////////////////////
require_once("../include/xajax/xajax.inc.php"); // LLAMADO DE LIBRERIA XAJAX

$xajax = new xajax(); // CREACION DEL OBJETO XAJAX

/* FUNCIONES XAJAX */

$xajax->registerExternalFunction("saveChanges", "expedienteMasivo_xajax.php");
//$xajax->debugOn();
$xajax->processRequests();

define('ADODB_ASSOC_CASE', 1);
require_once('../include/adodb/adodb.inc.php');	
require_once("../include/xWebComponent/database/DBConnection.php");		
require_once("../include/xWebComponent/database/data.php");		
include("../include/xWebComponent/xAutocomplete/xAutocomplete.class.php");
$con=new DBConnection($dbdriver, $server, $user, $password, $database);		

// Verifico trd de radicados
//$_GET['radicados']="20131000000431,20131000000421,20131000000411";
//$_GET['radicados']="20131000000441";
$radicados=$_GET['radicados'];
$query="SELECT RADI_NUME_RADI FROM radicado WHERE radi_nume_deri IN ($radicados)";
$rs=$con->execQuery($query);
while(!$rs->EOF){
	$hijos.=$rs->fields['RADI_NUME_RADI'].",";
	$rs->moveNext();
}
if($hijos !=""){
	$hijos=substr($hijos, 0, strlen($hijos)-1);
}
?>
<html>
<head>
<title>Asignar Expediente de manera múltiple</title>
<link rel="stylesheet" href="../estilos/orfeo.css" />
<?php $xajax->printJavascript("../include/xajax/"); ?>

<!--script>
		function ventana(){
		 nombreventana="Busqueda Avanzada";
			url='../expediente/conExp2.php?mod=2';
			window.open(url,nombreventana,'height=600,width=770,scrollbars=yes');
			return;	
		};
</script-->
</head>
<body>
    <form id="masiva" method="POST">
		<table id="tabMasivaIncluir" width="95%" align="center" margin="4">       	
			<tr>
				<td  class="titulos4" colspan="2" align="center" valign="middle">
					<b>Asignar Expediente de manera múltiple</b>
				</td>
			</tr>        	
			<tr height="40px">
				<td>Expediente:</td>
				<td>
					<input type="text" class="textarea" id="xexpediente" name="xexpediente">
					<!--<button class="botones_largo" type="button" id="xavanzada"> B&uacute;squeda Avanzada</button>	-->
										
					 <!--button class="botones_largo" type="button" id="busAvancExp"> Busqueda Avanzada </button-->
					
				</td>
			</tr>		   
			<tr height="40px">
				<td valign="top" >Radicados:</td>
					<td>
						<textarea rows="5" cols="45" readonly="READONLY"  class="textarea" name="xradicados" id="xradicados"><?php echo $radicados ?></textarea>
					</td>
				</td>
			</tr>				
			<tr height="40px">
				<td valign="top" >Radicados hijos:</td>
					<td>
						<textarea rows="3" cols="45" readonly="READONLY"  class="textarea" name="xhijos" id="xhijos"><?php echo $hijos ?></textarea>
						<br>
						<input type="checkbox" name="check_hijos" value="111"> 
						Incluir radicados hijos en el Expediente.
					</td>
				</td>
			</tr>				
			<tr height="40px">		                
				<td colspan="2" valign="center" align="center">
					<button class="botones" type="button" onclick="xajax_saveChanges(xajax.getFormValues('masiva'))">Incluir </button>
					<button class="botones" type="button" onclick="window.close();"> Cerrar </button>												                    
				</td>
		    </tr>	
			<tr>
				<td colspan="2"><div id="divResultado"></div></td>
			</tr>
        </table>
    </form>
</body>
</html>
