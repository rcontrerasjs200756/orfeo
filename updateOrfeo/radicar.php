<?php
session_start();
$krd=$_GET['krd'];
////////////////////////////////////////////////////////////////////////////////////////
require_once("../include/xajax/xajax.inc.php"); // LLAMADO DE LIBRERIA XAJAX
/*if($_SERVER['HTTP_X_FORWARDED_HOST']!=''){
	$xorfeo=$_SERVER['HTTP_X_FORWARDED_HOST'];
}else{
	$xorfeo="orfeo";
}
$xajax = new xajax($_SESSION['XPROTOCOL']."://".$xorfeo.$_SERVER['REQUEST_URI']); // CREACION DEL OBJETO XAJAX*/
$xajax = new xajax(); // CREACION DEL OBJETO XAJAX
/* FUNCIONES XAJAX */

$xajax->registerExternalFunction("showPestanias", "radicar_xajax.php");
$xajax->registerExternalFunction("getSearchDest", "radicar_xajax.php");
$xajax->registerExternalFunction("getSearchRad", "radicar_xajax.php");
$xajax->registerExternalFunction("showBusqDest", "radicar_xajax.php");
$xajax->registerExternalFunction("showBusqRad", "radicar_xajax.php");
$xajax->registerExternalFunction("getDataDest", "radicar_xajax.php");
$xajax->registerExternalFunction("getDataRad", "radicar_xajax.php");
$xajax->registerExternalFunction("radicar", "radicar_xajax.php");
$xajax->registerExternalFunction("loadUsua", "radicar_xajax.php");
$xajax->registerExternalFunction("informar", "radicar_xajax.php");
$xajax->registerExternalFunction("cargarDatosRadicado", "radicar_xajax.php");
// Autocomplete
$xajax->registerExternalFunction("loadAutocomplete", "../include/xWebComponent/xAutocomplete/xajax_xAutocomplete.php");

//$xajax->debugOn();
$xajax->processRequests();
define('ADODB_ASSOC_CASE', 1);
require_once('../include/adodb/adodb.inc.php');
require_once("../include/xWebComponent/database/DBConnection.php");
require_once("../include/xWebComponent/database/data.php");
include("../include/xWebComponent/xAutocomplete/xAutocomplete.class.php");
$con=new DBConnection($dbdriver, $server, $user, $password, $database);

$xAutocomplete=new xAutocomplete($con, "xAutocompleteCiudad");
$xAutocomplete->initQuery="SELECT 
								D.ID_CONT||'-'||D.ID_PAIS||'-'||D.DPTO_CODI||'-'||M.MUNI_CODI AS ID,  
								M.MUNI_NOMB||' - '||D.DPTO_NOMB||' - '||P.NOMBRE_PAIS AS XCIUDAD
							FROM MUNICIPIO M
							INNER JOIN DEPARTAMENTO D
							ON D.DPTO_CODI = M.DPTO_CODI
							AND D.ID_CONT  = M.ID_CONT
							AND D.ID_PAIS  = M.ID_PAIS
							INNER JOIN SGD_DEF_PAISES P
							ON P.ID_CONT    = D.ID_CONT
							AND P.ID_PAIS   = D.ID_PAIS	 ";
$xAutocomplete->searchFields[0]="M.MUNI_NOMB";
$xAutocomplete->searchFields[1]="D.DPTO_NOMB";
$xAutocomplete->searchFields[2]="P.NOMBRE_PAIS";
$xAutocomplete->labelFields[0]="XCIUDAD";
$xAutocomplete->size=250;
$xAutocomplete->order=2;

if ($_GET['ent']==2 or $_GET['ent']==4 ){
    $xAutocomplete2=new xAutocomplete($con, "xAutocompleteDep");
    $xAutocomplete2->initQuery="SELECT DEPE_CODI, DEPE_NOMB, DEP_SIGLA 
									FROM DEPENDENCIA 
									WHERE DEPE_ESTADO=1 ";
    $xAutocomplete2->searchFields[0]="TO_CHAR(DEPE_CODI,'999999')";
    $xAutocomplete2->searchFields[1]="DEPE_NOMB";
    $xAutocomplete2->searchFields[2]="DEP_SIGLA";
    $xAutocomplete2->labelFields[0]="DEPE_NOMB";
    $xAutocomplete2->labelFields[1]="DEP_SIGLA";
    $xAutocomplete2->size=550;
    $xAutocomplete2->order=2;
    // Atributos de posición del Autocomplete
    $xAutocomplete2->posx="200px";
    $xAutocomplete2->posy="600px";
}

?>
<html>
<head>
    <title>
        Radicaci&oacute;n Orfeo
    </title>

    <script>
			var ent="<?php echo $ent;?>";
			var dataitem ;
			var targetitem;
        var timer;
			var isSearch=false;
        function pushTime(value){
            if(timer){
                clearTimeout(timer);
            }
				//window.setTimeout(destSearch(value),500);
            xajax_getSearchDest(value, xajax.getFormValues('form1'));

			}
			function destSearch(e)
			{
				//

				if(document.getElementById('contactoid')!=null){
					document.getElementById('contactoid').value='';
				}
				document.getElementById('divXdestinatario').innerHTML='Presione Enter para buscar.';
				var busqueda=document.getElementById('xbusq');
				if (typeof e == 'undefined' && window.event) { e = window.event; }

				if(busqueda.value.length>=3&&e.keyCode == 13){
                isSearch=true;
				document.getElementById('divXdestinatario').innerHTML='Buscando ...';
			    xajax_getSearchDest(busqueda.value, xajax.getFormValues('form1'));
				}
				/*if (typeof e == 'undefined' && window.event) { e = window.event; }
				var busqueda=document.getElementById('xbusq');
				if(busqueda.value.length>=3&&e.keyCode == 13){
					xajax_getSearchDest(busqueda.value, xajax.getFormValues('form1'));
				}
				else{
					document.getElementById('divXdestinatario').innerHTML='';
				}*/
				//xajax_getSearchDest(value, xajax.getFormValues('form1'));
			}
			function seleccionarTodo(si)
			{
				if(si)
				{
						var cont=0;
						var check=document.getElementById('xSelect_'+cont);
						while(check!=null)
						{
							check.checked=true;
							cont++;
							check=document.getElementById('xSelect_'+cont);
						}
				}
				else{
				var cont=0;
						var check=document.getElementById('xSelect_'+cont);
						while(check!=null)
						{
							check.checked=false;
							cont++;
							check=document.getElementById('xSelect_'+cont);
						}
				}
			}

			function ToUpperX(ctrl)
			{
				var t = ctrl.value;
				ctrl.value = t.toUpperCase();
			}

			function crearContacto()
			{

				contactoid = document.getElementById('contactoid');

				contactoid.value='';
				dataitem =window.open('../Administracion/admContactos.php','Registro Contactos',target='_blank','width=600px,height=100px');
				dataitem.contactoid = contactoid;

				dataitem.onbeforeunload  = function(){

					var dataspli=contactoid.value.split("-");

					if(ent!=3)
					{
					if(dataspli[0]=='C')
					{
						var elements = document.getElementsByName('tipoBusq');
						for (i=0;i<elements.length;i++) {
						if(elements[i].value == "1") {
							elements[i].checked = true;
							}
						}

						xajax_getDataDest(xajax.getFormValues('form1'));
					}
					else if(dataspli[0]=='E'){

						var elements = document.getElementsByName('tipoBusq');
						for (i=0;i<elements.length;i++) {
						if(elements[i].value == "2") {
							elements[i].checked = true;
							}
						}


						xajax_getDataDest(xajax.getFormValues('form1'));
					}
					}
				}



			}


			function cerrarContacto()
			{
                xmlHttp = new XMLHttpRequest();
                xmlHttp.open( "GET",  'nombreContacto.php', false );
                xmlHttp.send( null );
                var texto=xmlHttp.responseText;
                if(texto!=null && texto!='')
                {
                    var busqueda=document.getElementById('xbusq');
                    busqueda.value=texto;
                xajax_getSearchDest(texto, xajax.getFormValues('form1'));
                }
                    dataitem.close();
			}

			function seleccionarRemitente(remit)
			{
				var opciones = document.getElementsByName("xdestinatario");
				for(var i=0; i<opciones.length; i++) {
					opciones[i].checked=false;
					if(opciones[i].value==remit)
					{
						opciones[i].checked=true;
					}
				}

			}

            function loadDestinatarioRadiMail()
            {
                <?php if(isset($_POST['correoid']))
                {
                ?>
                    xajax_getSearchDest('<?php echo $_POST['correoemail'];?>',{tipoBusq:1});

                <?php
                }
                ?>

            }

            function pasarAFormularioRadiMail(variable){

                if(isSearch){
                    return;
                }

                <?php if(isset($_POST['correoid']))
                {
                ?>
                if(variable!=''){
                    seleccionarRemitente(variable);
                    xajax_getDataDest(xajax.getFormValues('form1'));
                }else{
                    alert('No se encontraron concidencias para el usuario que envía el correo electrónico');
                }

                <?php
                }
                ?>

        }
    </script>
    <style>
        body{
            font-family:Arial;
        }
        .pestIzq{
            background-image:url("./icons/azul_izq.gif"); background-repeat:no-repeat;
            color:white;
            cursor:pointer;
        }
        .pestIzqSel{
            background-image:url("./icons/azul_hover_izq.gif"); background-repeat:no-repeat;
            color:white;
            cursor:pointer;
        }
        .pestCen{
            background-image:url("./icons/azul_cen.gif"); background-repeat:repeat-x;
            color:white;
            text-align:center;
            padding-left:30px;
            padding-right:30px;
            cursor:pointer;
        }
        .pestCenSel{
            background-image:url("./icons/azul_hover_cen.gif"); background-repeat:repeat-x;
            color:white;
            text-align:center;
            padding-left:30px;
            padding-right:30px;
            cursor:pointer;
        }
        .pestDer{
            background-image:url("./icons/azul_der.gif"); background-repeat:no-repeat;
            color:white;
            cursor:pointer;
        }
        .pestDerSel{
            background-image:url("./icons/azul_hover_der.gif"); background-repeat:no-repeat;
            color:white;
            cursor:pointer;
			}
			.botonesadd{
				background-image: url("./icons/add.png");
				background-repeat: no-repeat;
				background-position: left;
				padding-left: 32px;
				text-align: center;
				height: 32px;
				border: thin solid #FFFFFF;

        }
    </style>
    <link rel="stylesheet" href="./cuerpo.css">
    <link rel="stylesheet" href="../estilos/orfeo.css">
    <link rel="stylesheet" type="text/css" href="../include/xWebComponent/xAutocomplete/xAutocomplete_gray.css"/>
    <script type="text/javascript" src="../js/funciones.js"></script>
    <script src="../include/xWebComponent/xAutocomplete/xAutocomplete.js"></script>
    <?php $xajax->printJavascript("../include/xajax/"); ?>
    <script type="text/javascript" src="../include/mootools/mootools.v1.11.js"></script>
    <!-- CALENDARIO -->
    <script type="text/javascript" src="../include/mootools/datepicker/pbbdatepicker.v1.1.js"></script>
    <link href="../include/mootools/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
    <div id="spiffycalendar" class="text"></div>
</head>
	<body onload="xajax_showPestanias(1, '<?php echo $_GET['ent'];?>'); xajax_showBusqDest();">
<form id="form1" name="form1" method="post">
    <input type="hidden" id="tipoRad" name="tipoRad" value="1">
    <div id="divPest"></div>
    <table width="100%" border="0" cellpadding="0px" cellspacing="0px">
        <tr>
            <td colspan="13" style="border:3px solid #207186">
                <div id="xshowDest"></div>
            </td>
        </tr>
    </table>
    <br>
    <table width="100%" border="0" cellpadding="0px" cellspacing="0px">
        <tr>
            <td class="pestIzqSel">
                &nbsp;
            </td>
            <td class="pestCenSel">
                <b><?php if($_GET['ent']==2 || $_GET['ent']==4) { echo "Remitente"; }else{ echo "Destinatario"; }?></b>
            </td>
            <td class="pestDerSel">
                &nbsp;
            </td>
            <td width="80%">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="13" style="border:3px solid #207186">
                <input type="hidden" id="xtipo_dest" name="xtipo_dest">
                <input type="hidden" id="xtipoBusq2" name="xtipoBusq2">
                <input type="hidden" id="xdestinatario2" name="xdestinatario2">
                <input type="hidden" id="xcodigo" name="xcodigo"><!-- Tipo de destinatario sgd_trd_codigo -->
                <input type="hidden" id="ent" name="ent" value="<?php echo $_GET['ent']; ?>">
                <input type="hidden" id="hiddenRadicar" name="hiddenRadicar" value="<?php if($_POST['nurad']==''){ echo "r"; }else{ echo "m";}?>">
                <input type="hidden" id="xradicado" name="xradicado" value="<?php echo $_POST['nurad']; ?>"/>

                <input type="hidden" id="xAutocompleteCiudad_hidden" name="xAutocompleteCiudad_hidden" value="1-170-11-1">
                <input type="hidden" id="xnomdest" name="xnomdest" value="">
                <input type="hidden" id="xmail" name="xmail" value="">
                <input type="hidden" id="xdireccion" name="xdireccion" value="">
                <input type="hidden" id="xtelefono" name="xtelefono" value="">
                <input type="hidden" id="apell2" name="apell2" value="">
                <input type="hidden" id="xapell1" name="xapell1" value="">
                <input type="hidden" id="xnombre" name="xnombre" value="">
                    <input type="hidden" id="xciudad" name="xciudad" value="">
                <input type="hidden" id="xdocumento" name="xdocumento" value="">
                <input type="hidden" id="contRemit" name="contRemit" value="0">
					<table id="xDivRemitc" name="xDivRemitc" border="0px" class="listado2" width="98%" align="center" style="border:1px solid #207186">
                    <tr>
                        <!--<td valign="top" align="center">
                            <b>Identificaci&oacute;n:</b>
                        </td>							-->
                        <td valign="top" align="center">
                            <b>Nombre:</b>
                        </td>
                        <td valign="top" align="center">
                            <b>Direcci&oacute;n, Tel&eacute;fonos, Ciudad, eMail:</b>
                        </td>
                        <td valign="top" align="center">
                            <b>
                                <?php

                                switch($_GET['ent']){
                                    case 1:
										echo "Persona o Cargo";
                                        break;
                                    case 2:
                                        echo "Dignatario/Firmante:";
                                        break;
                                    case 3:
                                        echo "Cargo";
                                        break;
                                    case 4:
                                        echo "Cargo";
                                        break;
                                    default:
                                        echo "Persona o Cargo";
                                        break;
                                }
                                ?>
                            </b>
                        </td>
                        <td valign="top" align="center">
                            &nbsp;
                        </td>
							<td valign="top" align="right">
								<input type="checkbox" id="xSelect_all" name="xSelect_all" value="" onclick="seleccionarTodo(this.checked);" />
							</td>
                    </tr>
                </table>
					<table id="xDivRemit" name="xDivRemit" border="0px" class="listado2" width="98%" align="center" style="border:1px solid #207186">
					</table>
                <br>
					<table border="0px" class="listado2" width="98%" align="center" style="border:1px solid #207186">
                    <tr>
                        <td align="right" width="130px;">
                            <b>Fecha Radicaci&oacute;n:</b>
                        </td>
                        <td align="right" width="130px;">
                            <input type="text" id='xfechaActual' name="xfechaActual" value="<?php echo date("Y-m-d");?>" readonly />
                        </td>
                        <td align="right" width="130px;">
                            <b>Fecha Documento:</b>
                        </td>
                        <td align="right" width="130px;">
                            <input 	type="text" class="xtext" id="xfechaRad" name="xfechaRad" value="<?php echo date("Y-m-d");?>"
                                      onFocus="xfechaRad = new PBBDatePicker($('xfechaRad'));" style="width:150px;">
                            <script language="javascript">
                                xfechaRad = new PBBDatePicker($('xfechaRad'));
                            </script>
                        </td>
                        <td align="right" width="130px;">
                            <b>Referencia:</b>
                        </td>
                        <td align="right" width="130px;">
                            <input type="text" id="xcuentaInterna" name="xcuentaInterna">
                        </td>
                    </tr>
                </table>
                <!--
					MODIFICADO PATCH 2012-11-28
					<table border="0px" class="listado2" width="950" align="center" style="border:1px solid #207186">
						<tr>
							<td align="right" width="130px;">
								<b>Documento/Nit:</b>
							</td>
							<td colspan="3">
								<input type="text" class="textarea" id="xdocumento" name="xdocumento">
							</td>
						</tr>
						<tr>
							<td align="right">
								<b>Nombres:</b>
							</td>
							<td>
								<input type="text" class="textarea" size="40" id="xnombre" name="xnombre">
							</td>
							<td align="right">
								<b>Primer Apellido:</b>
							</td>
							<td>
								<input type="text" class="textarea" size="40" id="xapell1" name="xapell1">
							</td>
						</tr>
						<tr>
							<td align="right">
								<b>Segundo Apellido:</b>
							</td>
							<td>
								<input type="text" class="textarea" size="40" id="apell2" name="apell2">
							</td>
							<td align="right">
								<b>Tel&eacute;fono:</b>
							</td>
							<td>
								<input type="text" class="textarea" size="40" id="xtelefono" name="xtelefono">
							</td>
						</tr>
						<tr>
							<td align="right">
								<b>Direcci&oacute;n:</b>
							</td>
							<td>
								<input type="text" class="textarea" size="40" id="xdireccion" name="xdireccion">
							</td>
							<td align="right">
								<b>Email:</b>
							</td>
							<td>
								<input type="text" class="textarea" size="40" id="xmail" name="xmail">
							</td>
						</tr>
						<tr>
							<td align="right">
								<b>Nombre Destinatario:</b>
							</td>
							<td colspan="3">
								<input type="text" class="textarea" size="70" id="xnomdest" name="xnomdest">
							</td>
						</tr>
						<tr>
							<td align="right">
								<b>Ciudad:</b>
							</td>
							<td colspan="3">
								<?php //echo $xAutocomplete->getHTML(); ?>
								<a href="javascript:void(0)" onclick="document.getElementById('xAutocompleteCiudad_hidden').value='1-170-11-1'; document.getElementById('xAutocompleteCiudad_text').value='BOGOTA - D.C. - COLOMBIA'; ">Bogot&aacute;</a>
							</td>
						</tr>
					</table>
					-->
					<table border="0px" class="listado2" width="98%" align="center" style="border:1px solid #207186">
                    <tr>
                        <td valign="top" align="right" width="130px;">
                            <b>Asunto:</b>
                        </td>
                        <td align="left">
								<textarea class="textarea" rows="3" style="width:650px" id="xasunto" name="xasunto"><?php echo (isset($_POST['correoasunto']))?strtoupper( $_POST['correoasunto']):"";  ?></textarea>
                        </td>
                        <!--<td valign="top" align="right" width="130px;">
                            <b>Digantarios:</b>
                        </td>
                        <td align="left">
                            <textarea class="textarea" rows="3" style="width:225px" id="xotro_us" name="xotro_us"></textarea>
                        </td>							-->
                    </tr>
                </table>
					<table border="0px" class="listado2" width="98%" align="center" style="border:1px solid #207186">
                    <tr>
                        <td align="right" width="130px;">
                            <b>Folios Comunicaci&oacute;n:</b>
                        </td>
                        <td align="left">
								<input type="text" class="textarea" id="xfoliosDoc" name="xfoliosDoc" style="width:30px" value="<?php echo (isset($_POST['correoid']))?"1":"";  ?>">
                        </td>
                        <td align="right">
                            <b>Folios Anexos:</b>
                        </td>
                        <td align="left">
                            <input type="text" class="textarea" id="xfoliosAnex" name="xfoliosAnex" style="width:30px" value="0">
                        </td>
                        <td align="right">
                            <b>Descripci&oacute;n Anexos:</b>
                        </td>
                        <td align="left">
                            <input type="text" class="textarea" id="xdescAnex" name="xdescAnex" style="width:350px">
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <b>Dependencia:</b>
                        </td>
                        <td align="left" colspan="5" id="xdivDependencia">
                            <?php
                            if ($_GET['ent']==2 || $_GET['ent']==4){
                                /*?>
                                <select id="xAutocompleteDep_hidden" name="xAutocompleteDep_hidden">
                                <?php*/
                                echo $xAutocomplete2->getHTML();
                                /*$query="SELECT DEPE_CODI, DEPE_NOMB, DEP_SIGLA
                                            FROM DEPENDENCIA
                                            WHERE DEPE_ESTADO=1 ORDER BY DEPE_NOMB ASC												";
                                $rs=$con->execQuery($query);
                                while(!$rs->EOF){
                                ?>
                                    <option value="<?php echo $rs->fileds['DEPE_CODI']; ?>">
                                    <?php echo utf8_decode($rs->fields['DEPE_NOMB'])." - ".$rs->fileds['DEPE_CODI'];?>
                                    </option>
                                    <?php
                                    $rs->moveNext();
                                }
                                ?>
                                </select>
                                <?php*/
                            }else{
									echo "<b>".utf8_decode($_SESSION['depe_nomb'])."</b>";
                                echo "<input type='hidden' id='xAutocompleteDep_hidden' name='xAutocompleteDep_hidden' value='".$_SESSION['dependencia']."'>";
                            }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <b>Clasificaci&oacute;n de seguridad:</b>
                        </td>
                        <td align="left" colspan="1" id="div_xseguridad">
                            <select class="select" id="xseguridad" name="xseguridad">
                                <?php
                                $query="SELECT PARAM_CODI, PARAM_VALOR FROM SGD_PARAMETRO WHERE PARAM_NOMB='CLASIFICACION_SEGURIDAD'";
                                $rs=$con->execQuery($query);
                                while(!$rs->EOF){
                                    ?>
                                    <option value="<?php echo $rs->fields['PARAM_CODI'] ?>"><?php echo $rs->fields['PARAM_VALOR'] ?></option>
                                    <?php
                                    $rs->moveNext();
                                }
                                ?>
                            </select>
                        </td>
                        <td align="right">
                            <?php
                            if($_GET['ent']==2 or $_GET['ent']==4){
                                echo "<b>Medio de recepci&oacute;n:</b>";
                            }else{
                                echo "<b>Medio de env&iacute;o:</b>";
                            }
                            ?>
                        </td>
                        <td align="left" colspan="2" id="div_xmedio">
                            <select class="select" id="xmedio" name="xmedio">
                                <?php
                                switch($_GET['ent']){
                                    case 1:
                                        $xdefault=1;
                                        break;
                                    case 2:
                                        $xdefault=1;
                                        break;
                                    case 3:
                                        $xdefault=7;
                                        break;
                                    default:
                                        $xdefault=1;
                                        break;
                                }
                                $query="SELECT MREC_CODI, MREC_DESC FROM MEDIO_RECEPCION ";
                                $rs=$con->execQuery($query);
                                while(!$rs->EOF){
                                    ?>
                                    <option value="<?php echo $rs->fields['MREC_CODI'] ?>"
                                        <?php if($xdefault==$rs->fields['MREC_CODI']){ echo "selected=selected"; }?>>
                                        <?php echo $rs->fields['MREC_DESC']; ?>
                                    </option>
                                    <?php
                                    $rs->moveNext();
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <div id="xDatosRad"></div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <div id="xDivRadicado"></div>
                        </td>
                    </tr>
						<tr> <!-- return checkSubmit(); -->
                        <td align="center" colspan="6">
                            <input type="button" class="botones" value="<?php if($_POST['nurad']=='' && $_GET['nurad']==''){ echo "Radicar"; }else{ echo "Modificar"; }?>" id="btnRadicar"
                                   onclick=" if(document.getElementById('xfechaRad').value==''){ alert('Debe seleccionar la fecha del documento.'); document.getElementById('xfechaRad').focus(); return false;}
												  if(document.getElementById('xnombre').value==''){ alert('Debe buscar y seleccionar el destinatario. Clic en (◕‿◕)+ ');  document.getElementById('xnombre').focus(); return false;}
												  if(document.getElementById('xdireccion').value==''){ alert('Debe ingresar la dirección');  document.getElementById('xdireccion').focus();  return false;}
												  if(document.getElementById('xasunto').value==''){ alert('Debe ingresar el asunto');  document.getElementById('xasunto').focus(); return false;}
												  if(document.getElementById('xAutocompleteCiudad_hidden').value==''){ alert('Seleccione la ciudad.');  document.getElementById('xAutocompleteCiudad_text').focus(); return false;}
												  if(document.getElementById('xasunto').value.length<=13){ alert('El asunto debe tener al menos 13 caractéres.');  document.getElementById('xasunto').focus(); return false;}
												  if(document.getElementById('xfoliosDoc').value==''){ alert('Debe ingresar el No.de folios del documento');  document.getElementById('xfoliosDoc').focus();  return false;}
												  if(isNaN(document.getElementById('xfoliosDoc').value)){ alert('El No.de folios debe ser un valor numérico');  document.getElementById('xfoliosDoc').focus(); return false; }
												  if(document.getElementById('xAutocompleteDep_hidden').value==''){ alert('Debe seleccionar la dependencia destino.'); return false;}
												  var cont=0;
												  for(i=0; i<document.getElementById('contRemit').value; i++){
														if(document.getElementById('xSelect_'+i).checked==true){
															cont=1;
														}
												  }
												  if(cont==0){ 	alert('Debe incluir al menos un destinatario'); return false;   }
												  if(cont>1){
													if(!confirm('El sistema generará un Radicado para los '+(cont+1)+' destinatarios/remitentes que usted ha seleccionado.')){ return false;}
												  }
                                                  this.disabled=true; this.value='Radicando...';
												  xajax_radicar(xajax.getFormValues('form1'));
												" />
                            <input type="button" onclick="location.reload();document.getElementById('form1').scrollIntoView(true) " class="botones" value="Limpiar">
                        </td>
                    </tr>
                </table>
                <br>
            </td>
        </tr>
    </table>
</form>


</body>
</html>
<?php
$_SESSION["xAutocompleteCiudad"]=serialize($xAutocomplete);
if ($_GET['ent']==2 or $_GET['ent']==4){
    $_SESSION["xAutocompleteDep"]=serialize($xAutocomplete2);
}

// Si se va a modificar un radicado se carga los datos en el formulario
if($_POST['nurad']!=''){
    ?>
    <script language="javascript">xajax_cargarDatosRadicado('<?php echo $_POST['nurad']; ?>');</script>
    <?php
}
?>
<script type="text/javascript">
    function imprSelec(muestra){
        var ficha=document.getElementById(muestra);
        var ventimp=window.open(' ','Orfeo');
        ventimp.document.write(ficha.innerHTML);
        ventimp.document.close();
        ventimp.print();
        ventimp.close();
    }
</script>
<script language="javascript">
    function setComboDest(busq1){
        xajax_getSearchRad(''+busq1);
    }
</script>


<script language="javascript">
    function checkSubmit() {
        document.getElementById("btnRadicar").value = "Radicando...";
        document.getElementById("btnRadicar").disabled = true;
        return true;
    }
</script>

<script language="javascript">
    function redirecModalexpediente(radicado){

        window.location.href = "../app/borradores/radicardesdeentrada.php?radicado="+radicado+","

    }



</script>



<?php
if($_GET['modify']!=''){
    ?>
    <script>
        var form=new Array();
        // Extraer correo
        form['contRemit']="1";
        form['modify']='m'
        xajax_getDataRad('<?php echo $_GET['nurad']?>',form);
    </script>
    <?php
    //$res=$con->execQuery($query);
}
?>
