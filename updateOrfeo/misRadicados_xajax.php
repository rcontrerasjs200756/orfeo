<?php
	// Funci�n que pinta la bandeja de documentos de entrada
	function showRads(){		
		$xres=new xajaxResponse();
		require('../include/adodb/adodb.inc.php');
		require_once("../include/xWebComponent/database/DBConnection.php");
		require_once("../include/xWebComponent/database/data.php");		
		require_once("../include/xWebComponent/xDataGridComponent/xDataGrid.class.php");
		require_once("../include/xWebComponent/xDataGridComponent/xDataColumn.class.php");	

		$con=new DBConnection($dbdriver, $server, $user, $password, $database);			
		$xDataGrid=new xDataGrid($con);
		
		$dependencia=$_SESSION['dependencia'];
		$codusuario=$_SESSION['codusuario'];
				
		$columns[0]=new xDataColumn('show','Dig','','center');	
		$columns[1]=new xDataColumn('show','Exp','','center');	
		$columns[2]=new xDataColumn('show','Obs','','center');	
		$columns[3]=new xDataColumn('show','Radicado','','center');	
		$columns[4]=new xDataColumn('show','Fecha','','center');				
		$columns[5]=new xDataColumn('show','Asunto','','center');
		$columns[6]=new xDataColumn('show','Tipo documento','','center');
		$columns[7]=new xDataColumn('show','Plazo','','center');
		$columns[8]=new xDataColumn('show','Enviado por','','center');		
		$columns[9]=new xDataColumn('show','Tipo','','center');			
		$xDataGrid->initQuery="SELECT
								  (CASE 
									WHEN UPPER(SUBSTR(RADI_PATH,LENGTH(RADI_PATH)-2,3))='TIF' THEN '<img src=icons/scan.gif width=25px>' 
									WHEN UPPER(SUBSTR(RADI_PATH,LENGTH(RADI_PATH)-2,3))='IFF' THEN '<img src=icons/scan.gif width=25px>' 
									WHEN UPPER(SUBSTR(RADI_PATH,LENGTH(RADI_PATH)-2,3))='PDF' THEN '<img src=icons/scan.gif width=25px>' 
									ELSE '-' END) AS DIGITALIZADO,
								  (CASE
									WHEN (SELECT COUNT(E.RADI_NUME_RADI) FROM SGD_EXP_EXPEDIENTE E WHERE E.RADI_NUME_RADI=R.RADI_NUME_RADI)>0 
									  THEN 	CONCAT('<a href=../verradicado.php?verrad=', 
											CONCAT( R.RADI_NUME_RADI, '&menu_ver_tmp=4?".session_name()."=".session_id()."&krd=".$_SESSION['krd']." ><img src=icons/folder_open.gif width=25px border=0></a>'
											))
									ELSE '-'
									END) AS EXPEDIENTE,
								  (CASE
									WHEN R.SGD_EANU_CODIGO<>0 THEN '<img src=icons/anulacionRad.gif title=Anulado width=25px>'
									WHEN R.RADI_NUME_DERI <>'' THEN '<img src=icons/anexos.gif title=Anexo width=25px>'
								   END) AS OBSERVACIONES,
								  R.RADI_NUME_RADI AS RADICADO,
								  TO_CHAR(R.RADI_FECH_RADI,'YYYY-MM-DD HH24:MI AM') AS FECHA, 
								  UPPER(R.RA_ASUN) AS ASUNTO, 
								  T.SGD_TPR_DESCRIP as TIPO_DOCUMENTO,
								  (ROUND((SELECT MIN(A.SGD_AGEN_FECHPLAZO) FROM SGD_AGEN_AGENDADOS A WHERE A.RADI_NUME_RADI=R.RADI_NUME_RADI)-SYSDATE)+1) AS PLAZO,
								  U.USUA_NOMB AS ENVIADO_POR,
								  (SELECT TR.SGD_TRAD_DESCR FROM SGD_TRAD_TIPORAD TR WHERE TR.SGD_TRAD_CODIGO = (substr(R.RADI_NUME_RADI,14,1)) ) AS TIPO,
								  R.RADI_PATH ,
								  R.RADI_LEIDO AS LEIDO
								   
								FROM RADICADO R
								  LEFT JOIN SGD_TPR_TPDCUMENTO T ON R.TDOC_CODI=T.SGD_TPR_CODIGO
								  LEFT JOIN USUARIO U ON R.RADI_USU_ANTE=U.USUA_LOGIN								  
								WHERE 
								  R.RADI_DEPE_ACTU='$dependencia'
								  AND R.RADI_USUA_ACTU='$codusuario'								  
								  AND R.CARP_PER=0 
								  AND R.CARP_CODI NOT IN(0,11,12) ";		
		$columns[0]->field="C.CARP_DESC";
		$columns[1]->field="(CASE 
									WHEN UPPER(SUBSTR(RADI_PATH,LENGTH(RADI_PATH)-2,3))='TIF' THEN 'DIG' 
									WHEN UPPER(SUBSTR(RADI_PATH,LENGTH(RADI_PATH)-2,3))='IFF' THEN 'DIG' 
									WHEN UPPER(SUBSTR(RADI_PATH,LENGTH(RADI_PATH)-2,3))='PDF' THEN 'DIG' 
									ELSE 'NO DIG' END)";
		$columns[2]->field="(CASE
									WHEN (SELECT COUNT(E.RADI_NUME_RADI) FROM SGD_EXP_EXPEDIENTE E WHERE E.RADI_NUME_RADI=R.RADI_NUME_RADI)>0 
									  THEN 'EXP'
									ELSE 'NO EXP'
									END)";
		$columns[3]->field="(CASE
									WHEN R.SGD_EANU_CODIGO<>0 THEN 'ANULADO'
									WHEN R.RADI_NUME_DERI <>'' THEN 'DERIVADO'
								   END)";
		$columns[4]->field="R.RADI_NUME_RADI";
		$columns[5]->field="TO_CHAR(R.RADI_FECH_RADI,'YYYY-MM-DD HH24:MI AM')";
		$columns[6]->field="UPPER(R.RA_ASUN)";
		$columns[7]->field="T.SGD_TPR_DESCRIP";
		$columns[8]->field="(ROUND((SELECT MIN(A.SGD_AGEN_FECHPLAZO) FROM SGD_AGEN_AGENDADOS A WHERE A.RADI_NUME_RADI=R.RADI_NUME_RADI)-SYSDATE)+1)";
		$columns[9]->field="(SELECT TR.SGD_TRAD_DESCR FROM SGD_TRAD_TIPORAD TR WHERE TR.SGD_TRAD_CODIGO = (substr(R.RADI_NUME_RADI,14,1)) ) AS TIPO,";

		$xDataGrid->display_search='true';		
		$xDataGrid->type='multiple';		
		$xDataGrid->align='center';		
		$xDataGrid->columns=$columns;		
		$xDataGrid->size="900px";		
		$xDataGrid->fieldOrder=5;		
		$xDataGrid->orientation='DESC';
		$xDataGrid->sizeRows=20;
		$xDataGrid->initLimit=0;
		$id="xdiv_entrada_orfeo";
		$xDataGrid->id=$id;  
		$xDataGrid->docs='visible';		
		$xDataGrid->title="";
		
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		//$xDataGrid->conditionalField=7;
		$xDataGrid->conditional[0]['compare'][0]="=='0'";
		$xDataGrid->conditional[0]['field']=11;
		$xDataGrid->conditional[0]['color']="GREEN";
				

		$xDataGrid->conditional[1]['compare'][0]="<0";
		$xDataGrid->conditional[1]['compare'][1]="!= ''";
		$xDataGrid->conditional[1]['field']=7;
		$xDataGrid->conditional[1]['color']="RED";
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		
		$_SESSION["$id"]=serialize($xDataGrid);
		ob_start();
		?>
		<table cellspacing="0px" cellpadding="0px" border="0px">
			<tr>				
				<td width="65%">
					&nbsp;
				</td>
				<td>					
					<img src="../imagenes/internas/principal_r4_c3.gif" style="padding:0px">
				</td>
				<td>
					<img src="../imagenes/internas/moverA.gif"
							onmouseover="this.src='../imagenes/internas/overMoverA.gif'" style="padding:0px"
							onmouseout="this.src='../imagenes/internas/moverA.gif'"
							onclick="xajax_showMoverA();">
				</td>
				<td>
					<img src="../imagenes/internas/reasignar.gif"
							onmouseover="this.src='../imagenes/internas/overReasignar.gif'" style="padding:0px"
							onmouseout="this.src='../imagenes/internas/reasignar.gif'"
							onclick="xajax_showReasignar();">
				</td>
				<td>
					<img src="../imagenes/internas/informar.gif"
							onmouseover="this.src='../imagenes/internas/overInformar.gif'" style="padding:0px"
							onmouseout="this.src='../imagenes/internas/informar.gif'"
							onclick="xajax_showInformar();">
				</td>
				<td>
					<img src="../imagenes/internas/devolver.gif"
							onmouseover="this.src='../imagenes/internas/overDevolver.gif'" style="padding:0px"
							onmouseout="this.src='../imagenes/internas/devolver.gif'"
							onclick="xajax_showDevolver();">
				</td>
				<td>
					<img src="../imagenes/internas/vobo.gif"
							onmouseover="this.src='../imagenes/internas/overVobo.gif'" style="padding:0px"
							onmouseout="this.src='../imagenes/internas/vobo.gif'"
							onclick="xajax_showVobo();">
				</td>
				<td>
					<img src="../imagenes/internas/archivar.gif"
							onmouseover="this.src='../imagenes/internas/overArchivar.gif'" style="padding:0px"
							onmouseout="this.src='../imagenes/internas/archivar.gif'"
							onclick="xajax_showArchivar();">
				</td>
				<td>
					<img src="../imagenes/internas/plazo.gif"
							onmouseover="this.src='../imagenes/internas/overPlazo.gif'" style="padding:0px"
							onmouseout="this.src='../imagenes/internas/plazo.gif'"
							onclick="xajax_showPlazo();">
				</td>
			</tr>
			<tr class="rowHead">
				<td colspan="9" align="right">
					<br>
					<div id="optionArea"><br>
					</div>
					<br>
					<input type="hidden" name="enviara" id="enviara" >
					<input type="hidden" name="EnviaraV" id="EnviaraV" >
					
				</td>
				<td>
					&nbsp;
				</td>
			</tr>
		</table>
		<div style="font-size:12" align="center"><b>
		Nuevo!! Los radicados marcados con color <span style="color:RED">ROJO</span> tienen su plazo vencido, los marcados con color <span style="color:GREEN">VERDE</span> son documentos que todav&iacute;a no han sido leidos, 
		<br>los documentos con color <span style="color: rgb(0, 102, 153);">AZUL</span> ya fueron leidos.		
		</b></div>
		<div id="xdiv_entrada_orfeo">		
		</div>  
		<?php
		$xres->addAssign("xdiv_entrada","innerHTML",ob_get_clean());
		$xres->addAssign("$id","innerHTML",$xDataGrid->getHTML());
		$xres->addAssign("xdiv_entrada","style.display","");
		$xres->addAssign("xdiv_infor","style.display","none");
		$con->disconnect();
		return utf8_encode($xres->getXML());
	}
	
	
	// Muestra las opciones de mover a
	function showMoverA(){
		$xres=new xajaxResponse();
		require('../include/adodb/adodb.inc.php');
		require_once("../include/xWebComponent/database/DBConnection.php");
		require_once("../include/xWebComponent/database/data.php");				
		$con=new DBConnection($dbdriver, $server, $user, $password, $database);		
		
		$datoPersonal = "(Personal)";
		$nombreCarpeta = $con->con->Concat("' $datoPersonal'",'nomb_carp');
		$codigoCarpetaGen = $con->con->Concat("10000","carp_codi");
		$codigoCarpetaPer = $con->con->Concat("11000","codi_carp");
		$dependencia=$_SESSION['dependencia'];
		$codusuario=$_SESSION['codusuario'];
		/*$sql = "select carp_desc  as nomb_carp
				,$codigoCarpetaGen as carp_codi, 0 as orden
				from carpeta
				where carp_codi <> 11
				union
				select $nombreCarpeta as nomb_carp
				,$codigoCarpetaPer as carp_codi
				,1 as orden
				from carpeta_per
				where
				usua_codi = $codusuario
				and depe_codi = $dependencia
				order by orden, carp_codi";*/
		$sql="select ' (Personal)'||nomb_carp as nomb_carp
				,11000||codi_carp as carp_codi
				,1 as orden
				from carpeta_per
				where
				usua_codi = 1
				and depe_codi = 998
        and codi_carp<50
				order by orden, carp_codi";
		$rs = $con->con->Execute($sql);
		ob_start();
		?>
		<select class="select" id="carpper" name="carpSel">
			<?php 
			while(!$rs->EOF){
			?>
			<option value="<?php echo $rs->fields['CARP_CODI']; ?>"> 			
				<?php echo $rs->fields['NOMB_CARP']; ?>
			</option>
			<?php
				$rs->moveNext();
			}
			?>
		</select>
		<input type="button" onclick="verifCheck('xFormGridxdiv_entrada_orfeo', '../tx/formEnvio.php?codTx=10', '10');" class="botones_2" valign="middle" id="Enviar" name="Enviar" value="&gt;&gt;" style="">
		<input type="hidden" id="depsel" name="depsel" value="<?php echo $_SESSION['dependencia']?>">
		<input type="hidden" id="depsel8" name="depsel8" value="<?php echo $_SESSION['dependencia']?>">
		<?php
		$xres->addAssign("optionArea","innerHTML", ob_get_clean());	
		return utf8_encode($xres->getXML());
	}
	
	// Muestra el combo de dependencias para reasignar
	function showReasignar(){
		$xres=new xajaxResponse();
		$xres->addScript("verifCheck('xFormGridxdiv_entrada_orfeo', 'reasignar.php?".session_name()."=".session_id()."&krd=".$_SESSION['krd']."', '9');");		
		return utf8_encode($xres->getXML());
	}
	
	
	// funcion que busca las dependencias hijas de una dependencia
	function getHijos($dep_search){
		require('../include/adodb/adodb.inc.php');
		require_once("../include/xWebComponent/database/DBConnection.php");
		require_once("../include/xWebComponent/database/data.php");				
		$con=new DBConnection($dbdriver, $server, $user, $password, $database);
		$query2="SELECT DEPE_CODI FROM DEPENDENCIA WHERE DEPE_CODI_PADRE='$dep_search'";			
		$rsDep = $con->con->execute($query2);			
		if($rsDep->fields['DEPE_CODI']!=''){
			while(!$rsDep->EOF){
				$array_s[]=$rsDep->fields['DEPE_CODI'];	
				$rsDep->moveNext();				
			}				
		}			
		return $array_s;
	}
	
	// Muestra el combo de dependencias para reasignar
	function showInformar(){
		$xres=new xajaxResponse();
		$xres->addScript("verifCheck('xFormGridxdiv_entrada_orfeo', 'informar.php?".session_name()."=".session_id()."&krd=".$_SESSION['krd']."', '8');");			
		return utf8_encode($xres->getXML());
	}
	
	// Muestra las �ltimas 3 transacciones (Reasiganar e informar)
	function showInfoRad($rad){
		$xres=new xajaxResponse();
		require('../include/adodb/adodb.inc.php');
		require_once("../include/xWebComponent/database/DBConnection.php");
		require_once("../include/xWebComponent/database/data.php");				
		$con=new DBConnection($dbdriver, $server, $user, $password, $database);		
		$query="SELECT H.HIST_OBSE, H.SGD_TTR_CODIGO
										FROM HIST_EVENTOS H 
										WHERE 
											H.RADI_NUME_RADI='$rad' 
											AND H.sgd_ttr_codigo IN ('8','9') 
										ORDER BY H.HIST_FECH DESC";
		$res=$con->execQuery("$query",0,3);											
		$cad="";											
		if($res->fields[0]!=''){
			for($k=0; $k<3; $k++){
				$row2=$con->getRow($res);
				if($row2[0]!=''){
					if($row2[1]==9){					
						$cad.=" - Reasig. -> ";
					}else{
						$cad.=" - Infor. -> ";
					}
					$cad.=$row2[0];
				}
			}										
		}
		$xres->addAssign("info$rad","title", "$cad");	
		return utf8_encode($xres->getXML());
	}	
	
	// Devolver un documento
	function showDevolver(){
		$xres=new xajaxResponse();		
		$xres->addScript("verifCheck('xFormGridxdiv_entrada_orfeo', '../tx/formEnvio.php?codTx=12', '12');");	
		return utf8_encode($xres->getXML());
	}	
	
	// Visto bueno de documento
	function showVobo(){
		$xres=new xajaxResponse();	
		ob_start();
		?>
		<br>
		<input type="hidden" id="depsel" name="depsel" value="<?php echo $_SESSION['dependencia']?>">
		<?php
		$xres->addAssign("optionArea","innerHTML", ob_get_clean());	
		//$xres->addAssign("EnviaraV","value", "VoBo");	
		$xres->addScript("verifCheck('xFormGridxdiv_entrada_orfeo', '../tx/formEnvio.php?codTx=9&EnviaraV=VoBo', '9');");	
		return utf8_encode($xres->getXML());
	}	
	
	// Archivar un documento
	function showArchivar(){
		$xres=new xajaxResponse();	
		ob_start();
		?>
		<br>
		<input type="hidden" id="depsel" name="depsel" value="<?php echo $_SESSION['dependencia']?>">
		<?php
		$xres->addAssign("optionArea","innerHTML", ob_get_clean());	
		//$xres->addAssign("Enviara","value", "9");	
		$xres->addScript("verifCheck('xFormGridxdiv_entrada_orfeo', '../tx/formEnvio.php?codTx=13', '13');");	
		return utf8_encode($xres->getXML());
	}	
	
	// Colocar plazo a un documento
	function showPlazo(){
		$xres=new xajaxResponse();	
		ob_start();
		?>
		<table border="0" style="color:white">
			<tr><td><b>Plazos personales</b></td></tr>
		<tr><td>
		<input 	type="text" class="xtext" id="fechaAgenda" name="fechaAgenda"				
				onFocus="fechaAgenda = new PBBDatePicker($('fechaAgenda'))">
				<input type="button" value='>>' name="Enviar" id="Enviar" valign='middle' class='botones_2' onclick="if(get('fechaAgenda').value==''){ alert('Debe seleccionar una fecha'); }else{ verifCheck('xFormGridxdiv_entrada_orfeo', '../tx/formEnvio.php?codTx=14', '14');}">
		</td></tr>
		</table>
		<input type="hidden" id="depsel" name="depsel" value="<?php echo $_SESSION['dependencia']?>">
		<?php
		$xres->addAssign("optionArea","innerHTML", ob_get_clean());			
		$xres->addScript("fechaAgenda = new PBBDatePicker($('fechaAgenda'))");
		return utf8_encode($xres->getXML());
	}
	
?>
