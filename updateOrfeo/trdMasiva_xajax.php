<?php
	function showSubSeries($serie){
		$xres=new xajaxResponse();
		define('ADODB_ASSOC_CASE', 1);
		require('../include/adodb/adodb.inc.php');
		require_once("../include/xWebComponent/database/DBConnection.php");
		require_once("../include/xWebComponent/database/data.php");				
		$con=new DBConnection($dbdriver, $server, $user, $password, $database);
		$dependencia=$_SESSION['dependencia'];
		$query="SELECT DISTINCT
				  m.sgd_sbrd_codigo, 
				  sb.sgd_sbrd_descrip				  
				FROM 
				  sgd_mrd_matrird m
  				INNER JOIN sgd_sbrd_subserierd sb ON sb.sgd_sbrd_codigo=m.sgd_sbrd_codigo 
  					AND m.depe_codi='$dependencia'
					AND m.sgd_srd_codigo=sb.sgd_srd_codigo
				WHERE
				  m.sgd_srd_codigo='$serie' 
				  AND m.depe_codi='$dependencia'
				  AND sb.sgd_sbrd_fechini <= current_date
				  AND sb.sgd_sbrd_fechfin >= current_date
				  ";		
		//echo "".$query;		  
		$rs = $con->execQuery($query);				
		ob_start();
		?>
		<select class="select" id="selectSubSerie" name="selectSubSerie" onchange="xajax_showTipoDoc(<?php echo $_SESSION['dependencia'] ?>, <?php echo $serie ?>, this.value)" style="width:250px">
			<option value="0" selected="selected"> Seleccione una subSerie </option>
		<?php
		while(!$rs->EOF){				
			?>
			<option value="<?php echo $rs->fields['SGD_SBRD_CODIGO']?>"><?php echo $rs->fields['SGD_SBRD_CODIGO']." - ".$rs->fields['SGD_SBRD_DESCRIP'] ?></option>
			<?php		
			$rs->moveNext();				
		}				
		?>
		</select>
		<?php		
		$xres->addAssign('div_selectSubSerie', 'innerHTML', ob_get_clean());
	//	return utf8_encode($xres->getXML());
		return $xres->getXML();			
	}

	function showTipoDoc($dependencia, $serie, $subserie ){
		$xres=new xajaxResponse();
		define('ADODB_ASSOC_CASE', 1);
		require('../include/adodb/adodb.inc.php');
		require_once("../include/xWebComponent/database/DBConnection.php");
		require_once("../include/xWebComponent/database/data.php");				
		$con=new DBConnection($dbdriver, $server, $user, $password, $database);			
		$query="SELECT 
				  m.sgd_tpr_codigo, 
				  t.sgd_tpr_descrip
				FROM 
				  sgd_mrd_matrird m, 
				  sgd_tpr_tpdcumento t
				WHERE 
				  m.sgd_tpr_codigo = t.sgd_tpr_codigo AND
				  m.depe_codi = $dependencia  AND 
				  m.sgd_srd_codigo = $serie AND 
				  m.sgd_sbrd_codigo = $subserie ";
/*		ob_start();
	 	print_r($rs->fields);
	 	$xres->addAlert($query);
	 	echo "Pruebas";
	 	$xres->addAlert(ob_get_clean());
*/ 		
		$rs = $con->execQuery($query);				
		ob_start();
		?>
		<select class="select" id="selectTipoDoc" name="selectTipoDoc" style="width:250px">
			<option value="0" selected="selected"> Seleccione un Tipo Documental </option>
		<?php
		while(!$rs->EOF){				
			?>
			<option value="<?php echo $rs->fields['SGD_TPR_CODIGO']?>"><?php echo $rs->fields['SGD_TPR_CODIGO']." - ".$rs->fields['SGD_TPR_DESCRIP'] ?></option>
			<?php		
			$rs->moveNext();				
		}				
		?>
		</select>
		<?php		
		$xres->addAssign('div_selectTipoDoc', 'innerHTML', ob_get_clean());
	//	return utf8_encode($xres->getXML());
		return $xres->getXML();		
	}	

	
	function saveChanges($form){
		$xres=new xajaxResponse();
		define('ADODB_ASSOC_CASE', 1);
		require('../include/adodb/adodb.inc.php');
		require_once("../include/xWebComponent/database/DBConnection.php");
		require_once("../include/xWebComponent/database/data.php");				
		$con=new DBConnection($dbdriver, $server, $user, $password, $database);			
		$serie=$form['selectSerie'];
		$subserie=$form['selectSubSerie'];
		$tipoDoc=$form['selectTipoDoc'];
		$dependencia=$_SESSION['dependencia'];
		$sinTrd=explode(',', $form['radSinTrd']);
		$tieneTrd=explode(',', $form['radConTrd']);
		$usua_codi=$_SESSION['codusuario'];
		$usua_doc=$_SESSION['usua_doc'];
		$usua_nomb=$_SESSION['usua_nomb'];
		
		// Extraemos el codigo de la matriz trd -> sgd_mrd_codigo
		$query="SELECT 
				  m.sgd_mrd_codigo
				FROM 
				  sgd_mrd_matrird m
				WHERE 
				  m.depe_codi =  $dependencia AND 
				  m.sgd_srd_codigo =  $serie AND 
				  m.sgd_sbrd_codigo = $subserie AND 
				  m.sgd_tpr_codigo = $tipoDoc";
		$rs = $con->execQuery($query);		
		$sgd_mrd_codigo=$rs->fields['SGD_MRD_CODIGO'];
/*		ob_start();
	 	print_r($rs->fields);
	 	$xres->addAlert($query);
	 	echo "Pruebas";
	 	$xres->addAlert(ob_get_clean());
*/	 		
		include_once("../include/tx/Historico.php");	
		$Historico 		= new Historico($con->con);
		// Inserto TRD a los radicados que no tiene
		if($form['radSinTrd']!=""){
			$sinTrd=array_unique($sinTrd);
			foreach($sinTrd as $rad){
				$query="INSERT INTO sgd_rdf_retdocf(sgd_mrd_codigo, radi_nume_radi, depe_codi, usua_codi, usua_doc, sgd_rdf_fech) 
											VALUES('$sgd_mrd_codigo', '$rad', '$dependencia', '$usua_codi', '$usua_doc', current_date)";														
				$con->execQuery($query);				
				// Actualiza en la tabla radicado
				$query="UPDATE 
						  RADICADO
						  SET  
							TDOC_CODI = $tipoDoc
						  WHERE 
							radi_nume_radi = $rad";
				$con->execQuery($query);			
				$observa 	= "	Insercion masiva TRD por: Usuario: $usua_nomb - Dependencia: $dependencia";				
				$query="INSERT INTO HIST_EVENTOS
								(RADI_NUME_RADI, DEPE_CODI, USUA_CODI, USUA_CODI_DEST, DEPE_CODI_DEST, USUA_DOC, HIST_DOC_DEST, SGD_TTR_CODIGO, HIST_OBSE, HIST_FECH) 
								VALUES('$rad', '$dependencia', '$usua_codi', '$usua_codi', '$dependencia', '$usua_doc', '$usua_doc', '32', '$observa', current_date) ";
				$con->execQuery($query);
			}
			
		}
		// Actualizo TRD a los radicados que si tienen
		if($form['radConTrd']!="" && $form['cambExiTrd']!=''){
			$sinTrd=array_unique($tieneTrd);
			foreach($tieneTrd as $rad){
				$query="UPDATE sgd_rdf_retdocf SET sgd_mrd_codigo='$sgd_mrd_codigo', depe_codi='$dependencia', usua_codi='$usua_codi', usua_doc='$usua_doc', sgd_rdf_fech=current_date
						WHERE radi_nume_radi='$rad'";				
				$con->execQuery($query);
				$query="UPDATE 
						  RADICADO
						  SET  
							TDOC_CODI = $tipoDoc
						  WHERE 
							radi_nume_radi = $rad";
				$con->execQuery($query);
				// Actualizo en la tabla radicado
				$query="UPDATE 
						  RADICADO
						  SET  
							TDOC_CODI = $tipoDoc
						  WHERE 
							radi_nume_radi = $rad";
				$con->execQuery($query);			
				$observa 	= "	Actualizacion masiva TRD por: Usuario: $usua_nomb - Dependencia: $dependencia";				
				$query="INSERT INTO HIST_EVENTOS
								(RADI_NUME_RADI, DEPE_CODI, USUA_CODI, USUA_CODI_DEST, DEPE_CODI_DEST, USUA_DOC, HIST_DOC_DEST, SGD_TTR_CODIGO, HIST_OBSE, HIST_FECH) 
								VALUES('$rad', '$dependencia', '$usua_codi', '$usua_codi', '$dependencia', '$usua_doc', '$usua_doc', '32', '$observa', current_date) ";
				$con->execQuery($query);
			}	
			
		}
		ob_start();
		?>
		<table id="respuestaTrdMass"  class=""  width="100%" align="center" margin="4">
			<tr>
				<td  class="titulos4" colspan="2" align="center" valign="middle">
					<center><b>Se cambio la TRD a los siguientes Radicados:<b></center>
				</td>
			</tr>
			<tr bordercolor="white" height="40px">
				<td  valign="center" align="center">
					<?php if($form['cambExiTrd']!='') echo $form['radConTrd'] ?><br>
					<?php echo $form['radSinTrd'] ?><br>
				</td>				
			</tr>			
			<tr>
				<td align="center">
					<button class="botones" type="button" onclick="window.close();"> Cerrar </button>		
				</td>
			</tr>	
		</table>
		<?php	
		$xres->addAssign('divResultado', 'innerHTML', ob_get_clean());
	//	return utf8_encode($xres->getXML());
		return $xres->getXML();		
	}
?>