<?php session_start();
////////////////////////////////////////////////////////////////////////////////////////
require_once("../include/xajax/xajax.inc.php"); // LLAMADO DE LIBRERIA XAJAX

$xajax = new xajax(); // CREACION DEL OBJETO XAJAX

/* FUNCIONES XAJAX */
$xajax->registerExternalFunction("showSubSeries", "trdMasiva_xajax.php");
$xajax->registerExternalFunction("showTipoDoc", "trdMasiva_xajax.php");
$xajax->registerExternalFunction("saveChanges", "trdMasiva_xajax.php");
//$xajax->debugOn();
$xajax->processRequests();

define('ADODB_ASSOC_CASE', 1);
require_once('../include/adodb/adodb.inc.php');	
require_once("../include/xWebComponent/database/DBConnection.php");		
require_once("../include/xWebComponent/database/data.php");		
include("../include/xWebComponent/xAutocomplete/xAutocomplete.class.php");
$con=new DBConnection($dbdriver, $server, $user, $password, $database);		

$dependencia=$_SESSION['dependencia'];
// Cargar combo Series
$query="SELECT 
		  distinct(s.sgd_srd_codigo), 
		  s.sgd_srd_descrip
		FROM 
		  sgd_srd_seriesrd s
		  INNER JOIN sgd_mrd_matrird m ON s.sgd_srd_codigo=m.sgd_srd_codigo AND m.depe_codi=$dependencia
		WHERE 
		  s.sgd_srd_fechini <= current_date AND 
		  s.sgd_srd_fechfin >= current_date";
$rs=$con->execQuery($query);

// Verifico trd de radicados
//$_GET['radicados']="20131000000431,20131000000431,20131000000421,20131000000411";
//$_GET['radicados']="20131000000441";
$radicados=$_GET['radicados'];

$query="SELECT 
			  r.radi_nume_radi, 
			  r.sgd_mrd_codigo
			  FROM 
			  sgd_rdf_retdocf r
			WHERE 
			  r.radi_nume_radi IN ($radicados)";
$rs2=$con->execQuery($query);
$arrayRadicados=explode(",", $radicados);

while(!$rs2->EOF){	
	$xarrayResul[]=$rs2->fields['RADI_NUME_RADI'];
	$rs2->moveNext();
}
foreach($arrayRadicados as $element){
	if(in_array($element, $xarrayResul)){
		$tieneTrd.=$element.",";		
	}else{
		$sinTrd.=$element.",";		
	}
}
if($tieneTrd!=""){
	$tieneTrd	=	substr($tieneTrd, 	0, strlen($tieneTrd)-1);
}
if($sinTrd!=""){
	$sinTrd		=	substr($sinTrd, 	0, strlen($sinTrd)-1);
}
?>
<html>
<head>
<title>Asignar TRD de manera múltiple</title>
<link rel="stylesheet" href="../estilos/orfeo.css" />
<?php $xajax->printJavascript("../include/xajax/"); ?>
</head>
<body>
    <form id="masiva" method="POST">
		<table id="tabMasivaIncluir" width="95%" align="center" margin="4">       	
			<tr>
				<td  class="titulos4" colspan="2" align="center" valign="middle">
					<b>Asignar TRD de manera múltiple</b>
				</td>
			</tr>        	
			<tr height="40px">
				<td>Serie:</td>
				<td>
					<select class="select" style="width:250px" name="selectSerie" id="selectSerie" onChange="xajax_showSubSeries(this.value)">
						<option value="0" selected="selected"> Seleccione una serie </option>
						<?php
						while(!$rs->EOF){
						?>
						<option value="<?php echo $rs->fields['SGD_SRD_CODIGO']; ?>"><?php echo $rs->fields['SGD_SRD_CODIGO']." - ".$rs->fields['SGD_SRD_DESCRIP']; ?></option>
						<?php
							$rs->moveNext();
						}
						?>
		            </select>
				</td>
			</tr>
			<tr height="40px">
				<td >SubSerie:</td>
				<td>
					<div id="div_selectSubSerie">
					<select name="selectSubSerie" style="width:250px" id="selectSubSerie" class="select">
						<option value="0" selected="selected"> Seleccione una subSerie </option>
		            </select>
					</div>
				</td>
			</tr>			
			<tr height="40px">
				<td >Tipo documental:</td>
				<td>
					<div id="div_selectTipoDoc">
					<select name="selectTipoDoc" id="selectTipoDoc" style="width:250px" class="select">
						<option value="0" selected="selected"> Seleccione un tipo documental </option>                            
		            </select>
					</div>
				</td>
			</tr>
			   <?php if($sinTrd!=""){?>
				<tr height="60px">
					<td valign="top" >Radicados SIN TRD:</td>
						<td>
							<textarea rows="5" cols="45px" readonly="READONLY" class="select_crearExp nombActuExp" name="radSinTrd" id="radSinTrd"><?php echo $sinTrd ?></textarea>
						</td>
					</td>
				</tr>
				<?php }
				if($tieneTrd!=""){?>
				<tr height="60px">
					<td valign="top" >Radicados CON TRD:</td>
						<td>
							<textarea rows="3" cols="45px" readonly="READONLY"  class="textarea" name="radConTrd" id="radConTrd"><?php echo $tieneTrd ?></textarea>
							<br> 
							<input type="checkbox" name="cambExiTrd" value="111">
							Modificar la TRD de los radicados que ya tienen una asignada 
						</td>
					</td>
				</tr>
				<?php } ?>
			<tr height="40px">		                
				<td colspan="2" valign="center" align="center">
					<button class="botones" type="button" id="modificarTrd" onclick=" if(document.getElementById('selectTipoDoc').value<1){ alert('Debe seleccionar el Tipo Documental'); return false;}
																						xajax_saveChanges(xajax.getFormValues('masiva'))"> Aplicar TRD </button>
					<button class="botones" type="button" id="cerrarTrd" onclick="window.close();"> Cerrar </button>												                    
				</td>
		    </tr>	
			<tr>
				<td colspan="2"><div id="divResultado"></div></td>
			</tr>
        </table>
    </form>
</body>
</html>
