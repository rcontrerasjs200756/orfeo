<?php
	// Funci�n que agrega un usuario a la lista de informados
	function addInfor($form){		
		$xres=new xajaxResponse();
		require('../include/adodb/adodb.inc.php');
		require_once("../include/xWebComponent/database/DBConnection.php");
		require_once("../include/xWebComponent/database/data.php");		
		$con=new DBConnection($dbdriver, $server, $user, $password, $database);					
		// Consulto los datos del usuario		
		if($form['xusua_infor']!=''){			
			$ban=0;
			$xarray=array();
			foreach($form as $key=>$element){
				if(strstr($key, "listInfor_")!=''){	
					// Reviso cuales est�n					
					foreach($form['xusua_infor'] as $element2){
						if($element2==$element){
							$xarray[]=$element;							
						}
					}					
				}
			}
			
			$xarray=array_diff($form['xusua_infor'],$xarray);			
			foreach($xarray as $element){
				$xusua_array.="'$element', ";
			}
			$xusua_array=substr($xusua_array, 0, strlen($xusua_array)-2);			
		}		
		
		
		ob_start();		
		?>
		<table width="270px" align="center">
		<?php
		if($xusua_array!=''){
			$query="SELECT U.TITULO, U.USUA_NOMB, D.DEPE_NOMB, USUA_LOGIN 
					FROM USUARIO U INNER JOIN DEPENDENCIA D ON U.DEPE_CODI=D.DEPE_CODI 
					WHERE U.USUA_LOGIN IN ($xusua_array)";		
			$res=$con->execQuery($query);
		
			while(!$res->EOF){	
				$fila=$res->fields;
				?>			
					<tr>						
						<td style="font-size:12px">
							<input type="hidden" id="listInfor_<?php echo $fila['USUA_LOGIN']; ?>" name="listInfor_<?php echo $fila['USUA_LOGIN']; ?>" 
									value="<?php echo $fila['USUA_LOGIN']; ?>">
							<b><?php echo $fila['TITULO']." ".$fila['USUA_NOMB']." - ".$fila['DEPE_NOMB'];?></b>
						</td>
						<td style="cursor:pointer;">
							<img src="icons/button_cancel.png" onclick="xajax_removeInfor('<?php echo $fila['USUA_LOGIN']; ?>', xajax.getFormValues('xFormReasig'))">
						</td>
					</tr>		
				<?php	
				$res->moveNext();
			}
		}
		foreach($form as $key=>$element){
			if(strstr($key, "listInfor_")!=''){				
				$query="SELECT U.TITULO, U.USUA_NOMB, D.DEPE_NOMB 
					FROM USUARIO U INNER JOIN DEPENDENCIA D ON U.DEPE_CODI=D.DEPE_CODI 
					WHERE U.USUA_LOGIN='$element'";
				$res=$con->execQuery($query);			
				$fila=$con->getRow($res);				
		?>
			<tr>						
				<td style="font-size:12px">
					<input type="hidden" id="listInfor_<?php echo $element ?>" name="listInfor_<?php echo $element; ?>" 
							value="<?php echo $element; ?>">
					<b><?php echo $fila['TITULO']." ".$fila['USUA_NOMB']." - ".$fila['DEPE_NOMB'];?></b>
				</td>
				<td style="cursor:pointer;">
					<img src="icons/button_cancel.png" onclick="xajax_removeInfor('<?php echo $element ?>', xajax.getFormValues('xFormReasig'))">
				</td>
			</tr>		
		<?php
			}
		}
		?>
		</table>
		<?php
		$xres->addAssign("usuaInfor","innerHTML",ob_get_clean());
		return utf8_encode($xres->getXML());
	}
	
	
	// Funci�n que borra un usuario de la lista de informados
	function removeInfor($usua_login, $form){		
		$xres=new xajaxResponse();
		require('../include/adodb/adodb.inc.php');
		require_once("../include/xWebComponent/database/DBConnection.php");
		require_once("../include/xWebComponent/database/data.php");		
		$con=new DBConnection($dbdriver, $server, $user, $password, $database);					
		// Consulto los datos del usuario	
		ob_start();	
		?>
		<table>
		<?php
		foreach($form as $key=>$element){
			if(strstr($key, "listInfor_")!=''){				
				$query="SELECT U.TITULO, U.USUA_NOMB, D.DEPE_NOMB, U.USUA_LOGIN 
					FROM USUARIO U INNER JOIN DEPENDENCIA D ON U.DEPE_CODI=D.DEPE_CODI 
					WHERE U.USUA_LOGIN='$element'";
				$res=$con->execQuery($query);			
				$fila=$con->getRow($res);
				if($usua_login!=$element){
		?>
			<tr>						
				<td style="font-size:12px">
					<input type="hidden" id="listInfor_<?php echo $element ?>" name="listInfor_<?php echo $element; ?>" 
							value="<?php echo $element; ?>">
					<b><?php echo $fila['TITULO']." ".$fila['USUA_NOMB']." - ".$fila['DEPE_NOMB'];?></b>
				</td>
				<td style="cursor:pointer;">
					<img src="icons/button_cancel.png" onclick="xajax_removeInfor('<?php echo $element ?>', xajax.getFormValues('xFormReasig'))">
				</td>
			</tr>		
		<?php
				}
			}
		}
		?>
		</table>
		<?php
		$xres->addAssign("usuaInfor","innerHTML",ob_get_clean());		
		
		$con->disconnect();
		$xres->addAssign("xAutocomplete2_text","value","");		
		return utf8_encode($xres->getXML());
	}
	
	// Verifica los valores seleccionados por el usuario y realiza las transacciones respectivas	
	function realizarTx($form){
		$xres=new xajaxResponse();		
		require('../include/adodb/adodb.inc.php');
		require_once("../include/xWebComponent/database/DBConnection.php");
		require_once("../include/xWebComponent/database/data.php");				
		$con=new DBConnection($dbdriver, $server, $user, $password, $database);				
		// Usuarios a los que se informa
		$xlist="";
		$xdepelist="";
		foreach($form as $key=>$element){
			if(strstr($key, "listInfor_")){												
				$sql = "SELECT
						DEPE_CODI
						,USUA_CODI				
						FROM
							USUARIO
						WHERE
							USUA_LOGIN='$element'";
				# Busca el usuairo Origen para luego traer sus datos.
				$rs = $con->execQuery($sql);
				$xlist.=$rs->fields['DEPE_CODI'].", ";
				$xdepelist.=$rs->fields['USUA_CODI'].", ";				
			}			
		}	
		$xlist=substr($xlist, 0, strlen($xlist)-2);
		$xdepelist=substr($xdepelist, 0, strlen($xdepelist)-2);
		if($xlist!=""){			
			$codTx=$form['codTx'];
			$krd=$_SESSION['krd'];
			$dependencia=$_SESSION['dependencia'];
			$login_select=$form['xAutocomplete_hidden'];			
			$observa=$form['xReasigObs'];
			$observaPlazo=$form['xPlazoObs'];		
			$fechaAgenda=$form['fechaAgenda'];
			$observaInfor=$form['xInforObs'];
			
			
			ob_start();
			?>
			<input type="hidden" id="codTx" name="codTx" value="8">
			<input type="hidden" id="usCodSelect" name="usCodSelect" value="<?php echo $xlist; ?>"> 
			<input type="hidden" id="depesel8" name="depesel8" value="<?php echo $xdepelist ?>"> 
			<input type="hidden" id="observa" name="observa" value="<?php echo $observa ?>">
			<input type="hidden" id="observaPlazo" name="observaPlazo" value="<?php echo $observaPlazo ?>">
			<input type="hidden" id="observaInfor" name="observaInfor" value="<?php echo $observaInfor ?>">
			<input type="hidden" id="enviara" name="enviara" value="9">			
			<input type="hidden" id="fechaAgenda" name="fechaAgenda" value="<?php echo $fechaAgenda ?>">		
			<?php		
			$code=ob_get_clean();
			$xres->addAssign("xvars","innerHTML", $code);
			$xres->addAssign("xFormReasig","action", "../tx/realizarTx.php");
			$xres->addScript("get('xFormReasig').submit();");
		}else{
			$xres->addAlert("Debe seleccionar al menos un funcionario y dar clic en el bot�n agragar para informar.");		
		}
		return utf8_encode($xres->getXML());	
	}	
?>
