<?php 
session_start();

    $ruta_raiz = ".";
    if (!$_SESSION['dependencia'])
        header ("Location: $ruta_raiz/cerrar_session.php");

foreach ($_POST as $key => $valor) ${$key} = $valor;

$pathHelp = "$ruta_raiz/bodega/manuales/";

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <title> Manuales Orfeo</title>
    <link rel="stylesheet" href="<?=$ruta_raiz."/estilos/".$_SESSION["ESTILOS_PATH"]?>/orfeo.css">
    </head>
    <body>
<table width="100%" border="0" cellspacing="5" cellpadding="0" align="center" class="borde_tab">

	<tr align="center"  class="titulos4"> 
		<td height="12">
			<b> Manuales de Usuario Orfeo</b>
		</td>
	</tr>
		<tr align="left" class="etextomenu"> 
			<td class='listado2'>
				<a href='<?php echo $pathHelp."1_Ingreso_a_Orfeo.pdf" ?>' >Ingreso a Orfeo </a>
			</td>
		</tr>
		<tr align="left" class="etextomenu"> 
			<td class='listado2'>
				<a href='<?php echo $pathHelp."2_Descripcion_Orfeo.pdf" ?>' >Descripci&oacute;n de Orfeo </a>
			</td>
		</tr>
		<tr align="left" class="etextomenu"> 
			<td class='listado2'>
				<a href='<?php echo $pathHelp."3_Radicacion_interna.pdf" ?>' >Radicaci&oacute;n Interna </a>
			</td>
		</tr>
		<tr align="left" class="etextomenu"> 
			<td class='listado2'>
				<a href='<?php echo $pathHelp."4_Radicacion_de_Salida(externa).pdf" ?>' >Radicaci&oacute;n de Salida </a>
			</td>
		</tr>
				<tr align="left" class="etextomenu"> 
			<td class='listado2'>
				<a href='<?php echo $pathHelp."6_Como_se_reasigna_un_documento.pdf" ?>' > C&oacute;mo reasignar un documento </a>
			</td>
		</tr>
		</tr>
				<tr align="left" class="etextomenu"> 
			<td class='listado2'>
				<a href='<?php echo $pathHelp."7_Guia_practica_para_jefes.pdf" ?>' > Gu&iacute;a para Jefes </a>
			</td>
		</tr>
		</tr>
		<tr align="left" class="etextomenu"> 
			<td class='listado2'>
				<a href='<?php echo $pathHelp."8_Clasificar_documentos_con_TRD.pdf" ?>' > C&oacute;mo aplicar TRD </a>
			</td>
		</tr>
		<tr align="left" class="etextomenu"> 
			<td class='listado2'>
				<a href='<?php echo $pathHelp."9_Expedientes.pdf" ?>' > Expedientes (Crear, Incluir, Excluir) </a>
			</td>
		</tr>
		<tr align="left" class="etextomenu"> 
			<td class='listado2'>
				<a href='<?php echo $pathHelp."10_marcar_documento_ como_impreso.pdf" ?>' > C&oacute;mo marcar un documento como impreso </a>
			</td>
		</tr>
		<tr align="left" class="etextomenu"> 
			<td class='listado2'>
				<a href='<?php echo $pathHelp."11_planillas_entrega_correspondencia.pdf" ?>' > C&oacute;mo generar planillas </a>
			</td>
		</tr>

		<tr align="left" class="etextomenu"> 
			<td class='listado2'>
				<a href='<?php echo $pathHelp."12_enviar_copias_a_otro_destinatario.pdf" ?>' > C&oacute;mo enviar copia a otro destinatario </a>
			</td>
		</tr>
		<tr align="left" class="etextomenu"> 
			<td class='listado2'>
				<a href='<?php echo $pathHelp."13_Consulta_de_documentos.pdf" ?>' > C&oacute;mo consultar un documento </a>
			</td>
		</tr>
		<tr align="left" class="etextomenu"> 
			<td class='listado2'>
				<a href='<?php echo $pathHelp."15_Modificacion_de_un_documento.pdf" ?>' > C&oacute;mo modificar un documento  </a>
			</td>
		</tr>
		<tr align="left" class="etextomenu"> 
			<td class='listado2'>
				<a href='<?php echo $pathHelp."16_Solicitud_documento_fisico.pdf" ?>' > C&oacute;mo solicitar un documento f&iacute;sico  </a>
			</td>
		</tr>
		<tr align="left" class="etextomenu"> 
			<td class='listado2'>
				<a href='<?php echo $pathHelp."17_Agendar_documentos.pdf" ?>' > C&oacute;mo agendar un documento </a>
			</td>
		</tr>
		<tr align="left" class="etextomenu"> 
			<td class='listado2'>
				<a href='<?php echo $pathHelp."18_Estadisticas_e_indicadores_de_gestion.pdf" ?>' > Estad&iacute;sticas - Reportes - Indicadores de Gesti&oacute;n </a>
			</td>

	<tr align="center"  class="titulos4"> 
		<td height="12">
			<b> Manuales Usuarios de Gesti&oacuten Documental</b>
		</td>
	</tr>
		<tr align="left" class="etextomenu"> 
			<td class='listado2'>
				<a href='<?php echo $pathHelp."5_Radicacion_de_entrada.pdf" ?>' >Radicaci&oacute;n de Entrada </a>
			</td>

		</tr>
		<tr align="left" class="etextomenu"> 
			<td class='listado2'>
				<a href='<?php echo $pathHelp."19_Digitalizacion_de_documentos.pdf" ?>' > Digitalizaci&oacute;n de documentos </a>
			</td>
		</tr>

		</tr>
		<tr align="left" class="etextomenu"> 
			<td class='listado2'>
				<a href='<?php echo $pathHelp."14_envio_de_documentos.pdf" ?>' > Env&iacute;o de documentos </a>
			</td>
		</tr>

	<tr align="center"  class="titulos4"> 
		<td height="12">
			<b> Manuales T&eacute;cnicos</b>
		</td>
	</tr>
<?php
	if ($_SESSION['usua_admin_sistema']=='1') { 
?>
		<tr align="left" class="etextomenu"> 
			<td class='listado2'>
				<a href='<?php echo $pathHelp."20_administracion_Orfeo.pdf" ?>' >Administrador Orfeo </a>
			</td>
		</tr>

		<tr align="left" class="etextomenu"> 
			<td class='listado2'>
				<a href='<?php echo $pathHelp."1_Manual_de_Instalacion_Digitalizador.pdf" ?>' >Instalaci&oacute;n del Digitalizador </a>
			</td>
		</tr>
		<tr align="left" class="etextomenu"> 
			<td class='listado2'>
				<a href='<?php echo $pathHelp."1_Guia_administrar_archivos_plantillas_en_el_servidor.pdf" ?>' >Administraci&oacute;n Bodega - Plantillas </a>
			</td>
		</tr>
		<tr align="left" class="etextomenu"> 
			<td class='listado2'>
				<a href='<?php echo $pathHelp."1_Administracion_Descripcion_Codigo_Fuente_Orfeo.pdf" ?>' >Descripci&oacute;n C&oacute;digo Fuente </a>
			</td>
		</tr>
		<tr align="left" class="etextomenu"> 
			<td class='listado2'>
				<a href='<?php echo $pathHelp."./bdschemaorfeo13/index.html" ?>' >Modelo Entidad Relaci&oacute;n BD </a>
			</td>
		</tr>


<?php
		}
?>

</table>
</body>
</html>

