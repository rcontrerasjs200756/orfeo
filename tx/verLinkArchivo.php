<?php

/**
 * verLinkArchivo es la clase encargada de
 * validar los permisos de acceso a un documento (imagen informacion)
 * @author Liliana Gomez Velasquez
 * @version     1.0
 * @fecha  09 sep 2009
 */
class verLinkArchivo {

    /**
     * Variable que se corresponde con su par
     * @db Objeto conexion
     * @access public
     */
    var $db;

    /**
     * Vector que almacena el resultado de la validacion
     * @var string
     * @access public
     */
    var $vecRads;

    /**
     * Vector que almacena el resultado de la validacion
     * de un Anexo
     * @var string
     * @access public
     */
    var $vecRadsA;

    /**
     * Constructor encargado de obtener la conexion
     * @param	$db	ConnectionHandler es el objeto conexion
     * @return   void
     */
    function verLinkArchivo($db) {
        /**
         * Constructor de la clase 
         * @db variable en la cual se recibe el cursor sobre el cual se esta trabajando.
         *
         */
        $this->db = $db;
    }

    /**
     * Retorna el valor correspondiente al 
     * resultado de la validacion
     * @numrad  Numero del Radicado a validar
     * @return   array  $vecRads resultado de la operacion de validacion
     */
    function valPermisoRadi($numradi, $numeExpe = NULL) {
        $ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
        $this->db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        $isql = "select r.RADI_PATH, r.SGD_SPUB_CODIGO, u.CODI_NIVEL, u.USUA_NOMB,u.USUA_DOC,
         r.RADI_USU_ANTE, r.RADI_DEPE_ACTU
         from RADICADO r, USUARIO u
         where r.RADI_NUME_RADI='$numradi'
         and r.RADI_USUA_ACTU= u.USUA_CODI
         and r.RADI_DEPE_ACTU= u.DEPE_CODI";

        $rs = $this->db->conn->query($isql);
        $rutaRad = $rs->fields['RADI_PATH'];;
        $valorExp = '';
        if (is_null($numeExpe)){
        $consultaExpediente = "SELECT SGD_EXP_NUMERO  FROM SGD_EXP_EXPEDIENTE 
				     WHERE radi_nume_radi= $numradi AND sgd_exp_fech=(SELECT MAX(SGD_EXP_FECH) minFech  
				     from sgd_exp_expediente where radi_nume_radi= $numradi  and sgd_exp_estado<>2)  
				     and sgd_exp_estado<>2";
        $rsE = $this->db->conn->selectLimit($consultaExpediente, 10000)->getArray();
            $valorExp = $rsE[0]["SGD_EXP_NUMERO"];
        }else{
            $valorExp = $numeExpe;
        }


        $login = $_SESSION['krd'];
        $consulta1 = "SELECT upper(a.usua_nomb) as USUA_NOMB, a.id, upper(b.depe_nomb) AS DEPE_NOMB ,b.id as depe_id 
			   FROM usuario a,dependencia b 
			   WHERE  a.depe_codi=b.depe_codi 
			   AND  USUA_LOGIN='$login' ";
        $res1 = $this->db->conn->query($consulta1);
        $id_usuario = $res1->fields["ID"];

        $curl = curl_init();

        $paramsGetService = 'usuario_id='.$id_usuario;
        $paramsGetService .= '&entidad_valor='.$numradi;
        $paramsGetService .= '&tipo_entidad=RADICADO';
        if (!(is_null($numeExpe))){
            $paramsGetService .= '&entidad_valor_extra='.$numeExpe;
            $paramsGetService .= '&tipo_entidad_extra=EXPEDIENTE';
        }

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://34.228.216.86/back/integration/seguridad/entidad/consultar?".$paramsGetService,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "authorization: Bearer 21497beba40b3b115589733b21f7a0c0b68dc7194f1c925c296a026dbfa761e6",
                "cache-control: no-cache"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $consultaServicio = "cURL Error #:" . $err;
        } else {
            $consultaServicio = json_decode($response) ;
        }



        $vecRadsD['verImg'] = $consultaServicio->acceso;
        $vecRadsD['verImgMsg'] = $consultaServicio->mensaje;
        $vecRadsD['pathImagen'] = $rutaRad;
        $vecRadsD['numExpe'] = $valorExp;
        $vecRadsD['segDep'] = (isset($consultaServicio->accesoConcededBy)) ? $consultaServicio->accesoConcededBy  : '';
        $vecRadsD['test'] = $consultaServicio->acceso;
        /*
        $vecRadsD['verImg'] = 'SI';
        $vecRadsD['verImgMsg'] = 'Acceso concedido';
        $vecRadsD['pathImagen'] = '';
        $vecRadsD['numExpe'] =  '';
        $vecRadsD['segDep'] =   '';
        $vecRadsD['test'] = '';*/
        return $vecRadsD;
    }

    /**
     * Retorna el valor correspondiente al 
     * resultado de la validacion
     * @numrad  Numero del Anexo a validar
     * @return   array  $vecRadsA resultado de la operacion de validacion
     */
    function valPermisoAnex($numAnex) {

        /// Busca el Documento del usuario Origen
        $ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
        $this->db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        $verImg = "SI";
        $pathImagen = "";
        $isqlAnex = "select ANEX_NOMB_ARCHIVO
        from ANEXOS
        where ANEX_CODIGO = '$numAnex'";
        $rsAnex = $this->db->conn->query($isqlAnex);
        if (!$rsAnex->EOF) {
            $pathImagen = trim($rsAnex->fields["ANEX_NOMB_ARCHIVO"]);
            $numeradi = trim($rsAnex->fields["RADI_NUME_SALIDA"]);
        } else {
            $verImg = "NO SE ENCONTRO INFORMACION DEL RADICADO";
        }
        $vecRadsA['verImg'] = $verImg;
        $vecRadsA['pathImagen'] = $pathImagen;

        return $vecRadsA;
    }

    function returnValPermRad($numradi, $numeExpe = NULL) {
        return json_encode($this->valPermisoRadi($numradi, $numeExpe));
    }

}

?>
