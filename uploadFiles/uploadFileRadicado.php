<?php
session_start();
/**
  * Se añadio compatibilidad con variables globales en Off
  * @autor Jairo Losada 2009-05
  * @licencia GNU/GPL V 3
  */
$krd = $_SESSION["krd"];
$dependencia = $_SESSION["dependencia"];
$usua_doc = $_SESSION["usua_doc"];
$codusuario = $_SESSION["codusuario"];
$tip3Nombre=$_SESSION["tip3Nombre"];
$tip3desc = $_SESSION["tip3desc"];
$tip3img =$_SESSION["tip3img"];

if (isset($_GET["carpeta"]))
    $nomcarpeta = $_GET["carpeta"];
else
    $nomcarpeta = "";

if (isset($_GET["tipo_carpt"]))
    $tipo_carpt = $_GET["tipo_carpt"];
else
    $tipo_carpt = "";
    
if (isset($_GET["adodb_next_page"]))
    $adodb_next_page = $_GET["adodb_next_page"];
else
    $adodb_next_page = "";

if(isset($_GET["dep_sel"])) $dep_sel=$_GET["dep_sel"];
if(isset($_GET["btn_accion"])) $btn_accion=$_GET["btn_accion"];
if(isset($_GET["orderNo"])) $orderNo=$_GET["orderNo"];
if(isset($_REQUEST["orderTipo"])) $orderTipo=$_GET["orderTipo"];
if(isset($_REQUEST["busqRadicados"])) $busqRadicados=$_REQUEST["busqRadicados"];
if(isset($_REQUEST["Buscar"])) $Buscar=$_REQUEST["Buscar"];
if(isset($busq_radicados_tmp) && isset($_REQUEST["$busq_radicados_tmp"])) $$busq_radicados_tmp=$_REQUEST["$busq_radicados_tmp"];

$ruta_raiz = "..";
if(!isset($_SESSION['dependencia']))	include "$ruta_raiz/rec_session.php";
require_once("$ruta_raiz/include/db/ConnectionHandler.php");
$db = new ConnectionHandler($ruta_raiz);
error_reporting(7);
$verrad = "";
?>
<HTML>
<head>
<link rel="stylesheet" href="<?=$ruta_raiz?>/estilos/orfeo.css">
<?php include_once "$ruta_raiz/js/funtionImage.php"; ?>
<!-- Adicionado Carlos Barrero SES 02/10/09-->
<script>
	function borrad(ruta)
		{
			if(document.formulario.valRadio.checked==false)
				{
					alert('Seleccione un radicado.');
					return false;
				}
			else
				{
					if(confirm("Esta seguro de borrar la imágen del radicado "+formulario.valRadio.value+" ?"))
						window.location=ruta+document.formulario.valRadio.value;
					else
						return false;
				}
		}
</script>

    <link href="../app/resources/global/plugins/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="../app/resources/global/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet"
          type="text/css"/>

    <link href="../app/resources/global/plugins/font-awesome/css/fonts.googleapis.css" rel="stylesheet" type="text/css"/>
    <link href="../app/resources/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="../app/resources/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="../app/resources/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet"
          type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN THEME GLOBAL STYLES
    el siguiente archivo css, les quita la propiedad de borderradios a los input de form control, por eso lo comente.
    <link href="./app/resources/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    -->
    <link href="../app/resources/global/css/components-rounded.min.css" rel="stylesheet" id="style_components"
          type="text/css"/>
    <link href="../app/resources/global/css/plugins.min.css" rel="stylesheet" type="text/css"/>
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="../app/resources/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css"/>
    <link href="../app/resources/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css"/>


    <script src="../app/resources/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="../app/resources/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../app/resources/global/plugins/bootstrap-table/bootstrap-table.js" type="text/javascript"></script>
    <link href="../app/resources/global/css/select2.min.css" rel="stylesheet"/>
    <link href="../app/resources/global/css/select2-bootstrap.min.css" rel="stylesheet"/>
    <link href="../app/resources/global/plugins/sweetalert2-9.10.2/package/dist/sweetalert2.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="../app/resources/global/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css"/>

</head>
<BODY>
<FORM ACTION="<?=$_SERVER['PHPSELF']?>?<?=session_name()?>=<?=session_id()?>" method="POST">
<?
$varBuscada = "RADI_NUME_RADI";
include "$ruta_raiz/envios/paEncabeza.php";
include "$ruta_raiz/envios/paBuscar.php";
$encabezado = "".session_name()."=".session_id()."&depeBuscada=$depeBuscada&filtroSelect=$filtroSelect&tpAnulacion=$tpAnulacion&carpeta=$carpeta&tipo_carp=$tipo_carp&chkCarpeta=$chkCarpeta&busqRadicados=$busqRadicados&nomcarpeta=$nomcarpeta&agendado=$agendado&";
$linkPagina = "$PHP_SELF?$encabezado&orderTipo=$orderTipo&orderNo=$orderNo";
$encabezado = "".session_name()."=".session_id()."&adodb_next_page=1&depeBuscada=$depeBuscada&filtroSelect=$filtroSelect&tpAnulacion=$tpAnulacion&carpeta=$carpeta&tipo_carp=$tipo_carp&nomcarpeta=$nomcarpeta&agendado=$agendado&orderTipo=$orderTipo&orderNo=";
?>
</FORM>
<!--
Modificación Carlos Barrero -SES- permite borrar imagen vinculadas
<FORM ACTION="formUpload.php?krd=<?=$krd?>&<?=session_name()?>=<?=session_id()?>" method="POST">
-->
<FORM ACTION="formUpload.php?krd=<?=$krd?>&<?=session_name()?>=<?=session_id()?>" method="POST" name="formulario">
<center>

    <input type="button" value="Asociar Imagen del Radicado"
           onclick="verificarEscaneado(event)"
           name=asocImgRad class="botones_largo">

  <input type="button" value="Borrar Imagen del Radicado" name=borraImgRad class="botones_largo"
         onClick="return borrad('borraPath.php?krd=<?=$krd?>&<?=session_name()?>=<?=session_id()?>&numrad=');">

<!--
<center><input type="submit" value="Asociar Imagen del Radicado" name=asocImgRad class="botones_largo"></center>
-->
<?

if($Buscar AND $busq_radicados_tmp)
{

	include "$ruta_raiz/include/query/uploadFile/queryUploadFileRad.php";

	$rs=$db->conn->Execute($query);

	if ($rs->EOF)  {
		echo "<hr><center><b><span class='alarmas'>No se encuentra ningun radicado con el criterio de busqueda</span></center></b></hr>";
	}
	else{
		$orderNo =1;
		$orderTipo=" Desc ";
		$pager = new ADODB_Pager($db,$query,'adodb', true,$orderNo,$orderTipo);
		$pager->checkAll = false;
		$pager->checkTitulo = true;
		$pager->toRefLinks = $linkPagina;
		$pager->toRefVars = $encabezado;
		$pager->descCarpetasGen=$descCarpetasGen;
		$pager->descCarpetasPer=$descCarpetasPer;
		$pager->Render($rows_per_page=100,$linkPagina,$checkbox=chkAnulados);
	}
}
?>
</FORM>
</BODY>

<script src="../app/resources/global/plugins/respond.min.js"></script>
<script src="../app/resources/global/plugins/excanvas.min.js"></script>
<script src="../app/resources/global/plugins/ie8.fix.min.js"></script>
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="../app/resources/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../app/resources/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../app/resources/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="../app/resources/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="../app/resources/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="../app/resources/global/plugins/bootstrap-switch/js/bootstrap-switch.js" type="text/javascript"></script>


<!-- END CORE PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="../app/resources/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="../app/resources/pages/scripts/components-bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="../app/resources/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
<script src="../app/resources/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
<script src="../app/resources/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
<script src="../app/resources/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
<script src="../app/resources/global/plugins/sweetalert2-9.10.2/package/dist/sweetalert2.js"
        type="text/javascript"></script>
<script src="../app/resources/global/plugins/chosen.jquery.min.js" type="text/javascript"></script>
<!--  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>-->
<script src="../app/resources/global/plugins/select2.full.min.js" type="text/javascript"></script>
<script src="../app/resources/global/plugins/editor.js" type="text/javascript"></script>

<script src="../app/resources/global/plugins/tinymce/tinymce.min.js" type="text/javascript"></script>

<script src="../app/resources/global/plugins/html-docx.js" type="text/javascript"></script>
<script src="../app/resources/global/plugins/file-saver/FileSaver.js" type="text/javascript"></script>

<script src="../app/resources/global/plugins/bootstrap-table/bootstrap-table.js" type="text/javascript"></script>
<script src="../app/resources/pages/scripts/toastr.min.js" type="text/javascript"></script>
<script src="../app/resources/pages/scripts/ui-toastr.min.js" type="text/javascript"></script>



<script type="text/javascript">
    var WS_APPSERVICE_URL = '<?php echo $_SESSION['WS_APPSERVICE_URL']; ?>';
    var WS_APPSERVICE_TOKEN = '<?php echo $_SESSION['WS_APPSERVICE_TOKEN']; ?>';
    var conjugacionValidar =<?php  echo $_SESSION['NOMBRE_BOTON_VALIDAR_CONJUGADO'] ?>;
</script>

<script src="../app/resources/apps/scripts/scriptconfiguraciones.js?v=<?= date('YmdHis') ?>"></script>
<script src="../app/resources/apps/scripts/funciones_globales.js?v=<?= date('YmdHis') ?>"
        type="text/javascript"></script>


<!-- Services -->
<script src="../app/resources/apps/scripts/services/ConfiguracionesService.js?v=<?= date('Ymdis') ?>"
        type="text/javascript"></script>

<script src="../app/resources/apps/scripts/services/RadicadoService.js?v=<?= date('Ymdis') ?>"
        type="text/javascript"></script>
<script src="../app/resources/apps/scripts/services/ValidacionService.js?v=<?= date('Ymdis') ?>"
        type="text/javascript"></script>


<!-- Controllers -->
<script src="../app/resources/apps/scripts/controllers/Configuraciones.js?v=<?= date('Ymdis') ?>"
        type="text/javascript"></script>

<script src="../app/resources/apps/scripts/controllers/AsociarImagenes.js?v=<?= date('Ymdis') ?>"
        type="text/javascript"></script>

<script src="../app/resources/apps/scripts/controllers/Validacion.js?v=<?= date('Ymdis') ?>"
        type="text/javascript"></script>


<script type="text/javascript">

    function verificarEscaneado(e){
        e.preventDefault();

        if(document.formulario.valRadio.checked == undefined || document.formulario.valRadio.checked==false)
        {
            alert('Seleccione un radicado.');
            return false;
        }
        else
        {

            RadicadoService.getRadicadoByNume(formulario.valRadio.value,0).success(function (response) {

                if (response.objects.sgd_eanu_codigo === null){
                    AsociarImagenes.validaEstaEscaneado(formulario,formulario.valRadio.value);
                }else if(response.objects.sgd_eanu_codigo == '1'){
                    Swal.fire("No Permitido", "El documento se encuentra en solicitud de anulación, no puede modificar", "warning");
                }else if(response.objects.sgd_eanu_codigo == '2'){
                    //@formatter:off
                    Swal.fire({
                        title: "Alerta",
                        text: "Este documento se encuentra Anulado<br> ¿Está seguro de subir el documento principal?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonText: "Si",
                        cancelButtonText: "No",
                        closeOnConfirm: true,
                        closeOnCancel: true
                    }).then((result) => {
                        if (result.value) {
                            AsociarImagenes.validaEstaEscaneado(formulario,formulario.valRadio.value);
                        } else if (
                            result.dismiss === Swal.DismissReason.cancel
                        ) {
                            console.log('no continuo');
                        }
                    });
                    //@formatter:on
                }


            }).error(function (error) {
                Swal.fire("Error", "Ha ocurrido un error al tratar de consultar el Radicado", "error");
            });
        }


    }

</script>

</HTML>

