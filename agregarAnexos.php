<?php

class agregarAnexos {

    var $db;
    public function asociarAnexosRadicado(RadicadoResponse $radicado)
    {
        $documentosSubidos = 0;
        $tipoSolicitud = 'POST';
        $ruta = '/api/integration/anexos/add';
        $headers = [
            'Authorization: Bearer '.$this->accesToken.''
        ];
        $curl = curl_init();
        #echo '<pre>'.print_r($radicado->getDocumentos(),true).'</pre>';
        #die();
        $datos = [];
        foreach($radicado->getDocumentos() as $numero=>$documento)
        {
            if(!$documento->getEsPrincipal())
            {
                $datos[$numero] = [
                    'radicado' => $radicado->getNumeroRadicado(),
                    'anexo_file'=> new \CURLFILE($documento->getRuta()),
                    'usuario_id' => $radicado->getInformePago()->getUsuarioRadicador()->getIdResponse(),
                    'tipo_radicado' => '4',
                    'descripcion' => $documento->getDescripcion(),
                    'codigo_aplicacion' => '9',
                    'solo_lectura' => 'S',
                    'codigo_verificacion' => $radicado->getCodigoVerificacion(),
                ];

                echo '<pre>'.print_r($datos[$numero],true).'</pre>';
                curl_setopt_array($curl, array(
                    CURLOPT_URL => $this->urlBase.$ruta,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => $tipoSolicitud,
                    CURLOPT_POSTFIELDS =>$datos[$numero],
                    CURLOPT_HTTPHEADER =>$headers,
                ));
                $response = curl_exec($curl);
                #curl_reset($curl);
                echo '<pre>'.print_r(json_decode($response),true).'</pre>';
                $documentosSubidos += json_decode($response)->isSuccess;
            }
        }
        curl_close($curl);
        #$response = $this->request('POST','/api/integration/anexos/add',$datos, false);
        #$documentosSubidos += $this->subirAnexoRadicado($radicado, $documento) ? 1 : 0;
        #die();
        return $documentosSubidos;
    }

}

?>
