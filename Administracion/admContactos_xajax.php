<?php
// Funcion que agrega un usuario a la lista de informados
function showForm($option){
	$xres=new xajaxResponse();
	define('ADODB_ASSOC_CASE', 1);
	require('../include/adodb/adodb.inc.php');
	require_once("../include/xWebComponent/database/DBConnection.php");
	require_once("../include/xWebComponent/database/data.php");
	$con=new DBConnection($dbdriver, $server, $user, $password, $database);

	include("../include/xWebComponent/xAutocomplete/xAutocomplete.class.php");

	$xAutocomplete2=new xAutocomplete($con, "xAutocomplete2");
	$xAutocomplete2->action="xajax_loadData";
	$xAutocomplete2->size=350;
	$xAutocomplete2->order=2;
	switch($option){
		case "E":
			$pest1="pest_naranja";
			$pest2="pest_gris";
			$pest3="pest_azul";
			$pest4="pest_azul";
// mssql			$query="SELECT 'E-' || CONVERT(VARCHAR,O.SGD_OEM_CODIGO) as SGD_OEM_CODIGO , O.SGD_OEM_OEMPRESA, O.SGD_OEM_SIGLA FROM SGD_OEM_OEMPRESAS O";
//postgresql:
			$query="SELECT 'E-' || CAST(O.SGD_OEM_CODIGO AS VARCHAR(23)) as SGD_OEM_CODIGO , O.SGD_OEM_OEMPRESA, O.SGD_OEM_SIGLA, O.SGD_OEM_OEMPRESA as EMPRESALABEL
					FROM SGD_OEM_OEMPRESAS O WHERE (O.SGD_OEM_INACTIVO=0 OR O.SGD_OEM_INACTIVO IS NULL)";
			$xAutocomplete2->searchFields[0]="CAST(SGD_OEM_CODIGO AS VARCHAR(99))";
			$xAutocomplete2->searchFields[1]="O.SGD_OEM_OEMPRESA";
			$xAutocomplete2->searchFields[2]="O.SGD_OEM_SIGLA";
			$xAutocomplete2->searchFields[3]="O.SGD_OEM_NIT";
			$xAutocomplete2->labelFields[0]="EMPRESALABEL";
			break;
		case "U":
			$pest1="pest_azul";
			$pest2="pest_naranja";
			$pest3="pest_azul";
			$pest4="pest_azul";
			$query="SELECT 'U-' || CAST(IDENTIFICADOR_EMPRESA AS VARCHAR(23)) as IDENTIFICADOR_EMPRESA, NOMBRE_DE_LA_EMPRESA, NIT_DE_LA_EMPRESA, SIGLA_DE_LA_EMPRESA, NOMBRE_REP_LEGAL
						FROM BODEGA_EMPRESAS WHERE ACTIVA=1 ";
			$xAutocomplete2->searchFields[0]="CAST(IDENTIFICADOR_EMPRESA AS VARCHAR(99))";
			$xAutocomplete2->searchFields[1]="NOMBRE_DE_LA_EMPRESA";
			$xAutocomplete2->searchFields[2]="NIT_DE_LA_EMPRESA";
			$xAutocomplete2->searchFields[3]="SIGLA_DE_LA_EMPRESA";
			$xAutocomplete2->searchFields[4]="NOMBRE_REP_LEGAL";
			$xAutocomplete2->labelFields[0]="NOMBRE_DE_LA_EMPRESA";
			break;
		case "C":
			$pest1="pest_azul";
			$pest2="pest_azul";
			$pest3="pest_naranja";
			$pest4="pest_azul";
			$query="SELECT
						  'C-' || CAST(SGD_CIU_CODIGO AS VARCHAR(23)) as SGD_CIU_CODIGO, 
						  COALESCE(SGD_CIU_NOMBRE,' ') || ' ' || COALESCE(SGD_CIU_APELL1, ' ') || ' ' || COALESCE(SGD_CIU_APELL2, ' ') || ' ' || COALESCE(SGD_CIU_CEDULA, ' ') || ' ' AS XNOMBRE,						  
						  SGD_CIU_NOMBRE,
						  SGD_CIU_APELL1, 
						  SGD_CIU_APELL2,
						  SGD_CIU_CEDULA
						FROM SGD_CIU_CIUDADANO 
						WHERE (sgd_ciu_inactivo=0 or sgd_ciu_inactivo is null)";
			$xAutocomplete2->searchFields[0]="CAST(SGD_CIU_CODIGO AS VARCHAR(99))";
			$xAutocomplete2->searchFields[1]="SGD_CIU_NOMBRE";
			$xAutocomplete2->searchFields[2]="SGD_CIU_APELL1";
			$xAutocomplete2->searchFields[3]="SGD_CIU_APELL2";
			$xAutocomplete2->searchFields[4]="SGD_CIU_CEDULA";
			$xAutocomplete2->searchFields[5]="(SGD_CIU_NOMBRE || ' ' || SGD_CIU_APELL1)";
			$xAutocomplete2->searchFields[6]="SGD_CIU_EMAIL";
			$xAutocomplete2->labelFields[0]="XNOMBRE";
			break;
		case "P":
			$pest1="pest_azul";
			$pest2="pest_azul";
			$pest3="pest_azul";
			$pest4="pest_naranja";
			$query="SELECT
						  'P-' || CAST(SGD_CIU_CODIGO AS VARCHAR(23)) as SGD_CIU_CODIGO, 
						  COALESCE(SGD_CIU_NOMBRE,' ') || ' ' || COALESCE(SGD_CIU_APELL1,' ') || ' ' || COALESCE(SGD_CIU_APELL2,' ') AS XNOMBRE,						  
						  SGD_CIU_NOMBRE,
						  SGD_CIU_APELL1, 
						  SGD_CIU_APELL2,
						  SGD_CIU_CEDULA
						FROM SGD_CIU_CIUDADANO WHERE PENSIONADO='S' ";
			$xAutocomplete2->searchFields[0]="CAST(SGD_CIU_CODIGO AS VARCHAR(99))";
			$xAutocomplete2->searchFields[1]="SGD_CIU_NOMBRE";
			$xAutocomplete2->searchFields[2]="SGD_CIU_APELL1";
			$xAutocomplete2->searchFields[3]="SGD_CIU_APELL2";
			$xAutocomplete2->searchFields[4]="SGD_CIU_CEDULA";
			$xAutocomplete2->searchFields[5]="(SGD_CIU_NOMBRE || ' ' || SGD_CIU_APELL1)";
			$xAutocomplete2->labelFields[0]="XNOMBRE";
			break;

	}

	$xAutocompleteUbi=new xAutocomplete($con, "xAutocompleteUbi");
	$xAutocompleteUbi->action="loadUbicacion";
	$xAutocompleteUbi->size=250;
	$xAutocompleteUbi->order=1;
	$queryUbi="SELECT * FROM (
					SELECT  CAST(ID_CONT AS VARCHAR(23)) || '-' || CAST(ID_PAIS AS VARCHAR(23)) || '-' || CAST(DPTO_CODI AS VARCHAR(23)) || '-' || CAST(MUNI_CODI AS VARCHAR(23)) as CODIGO,MUNI_NOMB || '/' || DPTO_NOMB || '/' || NOMBRE_PAIS AS UBICACION,ID_CONT,ID_PAIS, DPTO_CODI,ID_CONT || '-' || ID_PAIS || '-' || DPTO_CODI || '-' ||  MUNI_CODI as CODIGO2, MUNI_CODI FROM SGD_DEF_CONTINENTES
					LEFT  JOIN 
					(SELECT ID_PAIS ,NOMBRE_PAIS,ID_CONT AS CONT FROM SGD_DEF_PAISES)AS P 
					On P.CONT=SGD_DEF_CONTINENTES.ID_CONT
					LEFT  JOIN 
					(SELECT DPTO_CODI ,DPTO_NOMB,ID_CONT AS CO,ID_PAIS AS PA FROM DEPARTAMENTO)AS D 
					On   P.CONT=D.CO  AND P.ID_PAIS= D.PA
					LEFT  JOIN 
					(SELECT MUNI_CODI ,MUNI_NOMB,ID_CONT AS C,ID_PAIS AS P, DPTO_CODI AS DE FROM MUNICIPIO)AS M 
					On    P.CONT =M.C  AND D.PA= M.P AND DPTO_CODI=M.DE
					) U
				WHERE ID_CONT IS NOT NULL AND ID_PAIS IS NOT NULL AND DPTO_CODI IS NOT NULL AND MUNI_CODI IS NOT NULL";
	$xAutocompleteUbi->searchFields[0]="CODIGO";
	$xAutocompleteUbi->searchFields[1]="UBICACION";

	$xAutocompleteUbi->labelFields[0]="UBICACION";
	$xAutocompleteUbi->initQuery="$queryUbi";

	$xAutocomplete2->initQuery="$query";
	ob_start();
	?>

	<table>
		<td align="center">
			<div style="width:71px" class="<?php echo $pest1; ?>_cen"
				 onclick="xajax_showForm('E');"
				 onmouseover="this.className='<?php echo $pest1; ?>_hover_cen'"
				 onmouseout="this.className='<?php echo $pest1; ?>_cen'">
				<b>Empresas</b>
			</div>
		</td>
		<?php
		if($_SESSION['usua_admin_sistema']==199){
			?>
			<td align="center">
				<div style="width:71px" class="<?php echo $pest2 ?>_cen"
					 onclick="xajax_showForm('U');"
					 onmouseover="this.className='<?php echo $pest2 ?>_hover_cen'"
					 onmouseout="this.className='<?php echo $pest2 ?>_cen'">
					<b>Entidades</b>
				</div>
			</td>
			<?php
		}
		?>
		<td align="center">
			<div style="width:71px" class="<?php echo $pest3 ?>_cen"
				 onclick="xajax_showForm('C');"
				 onmouseover="this.className='<?php echo $pest3 ?>_hover_cen'"
				 onmouseout="this.className='<?php echo $pest3 ?>_cen'">
				<b>Ciudadanos</b>
			</div>
		</td>
		<!--<td align="center">
				<div style="width:71px" class="<?php echo $pest4 ?>_cen"
					onclick="xajax_showForm('P')"
					onmouseover="this.className='<?php echo $pest4 ?>_hover_cen'"
					onmouseout="this.className='<?php echo $pest4 ?>_cen'">
				<b>Pensionados</b>
				</div>
			</td>-->
	</table>
	<?php
	$xres->addAssign("xpestanias","innerHTML",ob_get_clean());

	ob_start();
	?>
	<table align="center" width="1050px">
		<tr>
			<?php
			if($option!='C' && $option!='P'){
				?>
				<td align="right" width="120px" class="titulos2">
					Nombre:
				</td>
				<td class="listado2">
					<div id="xnombre"></div>
				</td>
				<td align="right" width="120px" class="titulos2">
					NIT:
				</td>
				<td class="listado2">
					<input type="text" id="xnit" name="xnit">
				</td>
				<td align="right" width="120px" class="titulos2">
					Sigla:
				</td>
				<td class="listado2">
					<input type="text" id="xsigla" name="xsigla">
				</td>
				<?php
			}else{
			?>
			<td align="right" width="120px" class="titulos2">
				Buscar:
			</td>
			<td class="listado2" colspan="5" > Antes de crear un contacto, buscar si ya existe por nombre, cedula o mail
				<div id="xnombre"> </div>
			</td>
		</tr>
		<tr>
			<td align="right" width="120px" class="titulos2">
				Nombre:
			</td>
			<td class="listado2">
				<input type="text" id="xnombre2" name="xnombre2">
			</td>
			<td align="right" width="120px" class="titulos2">
				Apellido 1:
			</td>
			<td class="listado2">
				<input type="text" id="xapellido1" name="xapellido1">
			</td>
			<td align="right" width="120px" class="titulos2">
				Apellido 2:
			</td>
			<td class="listado2">
				<input type="text" id="xapellido2" name="xapellido2">
			</td>
		</tr><tr>
			<td align="right" width="120px" class="titulos2">
				C&eacute;dula:
			</td>
			<td class="listado2" colspan="5">
				<input type="text" id="xcedula" name="xcedula">
			</td>
			<?php
			}
			?>
		</tr>
		<tr>
			<td align="right" class="titulos2">
				Direcci&oacute;n:
			</td>
			<td class="listado2" colspan="5">
				<input type="text" id="xdireccion" name="xdireccion" size="80">
			</td>
		</tr>


		<tr>
			<td align="right" width="120px" class="titulos2" >
				Ciudad:	<input type="hidden" id="xubicacioncodi" name="xubicacioncodi" >
			</td>
			<td class="listado2" colspan="5">
				<div id="xubicacion"></div>
			</td>
		</tr>

		<tr>
			<?php
			if($option!='C' && $option!='P'){
			?>
			<td align="right" width="120px" class="titulos2">
				Tel&eacute;fonos:
			</td>
			<td class="listado2">
				<input type="text" id="xtelefono" name="xtelefono">
			</td>
			<!--<td align="right" width="120px" class="titulos2">
                Tel&eacute;fono 2:
            </td>
            <td class="listado2">
                <input type="text" id="xtelefono2" name="xtelefono2">
            </td>-->
			<td align="right" width="120px" class="titulos2">
				Representante Legal:
			</td>
			<td class="listado2" colspan="3">
				<input type="text" id="xreplegal" name="xreplegal">
			</td>
		</tr>
		<tr>
			<td align="right" width="120px" class="titulos2">
				E-Mail:
			</td>
			<td class="listado2" colspan="5">
				<input type="text" id="xmail" name="xmail" size="50">
			</td>
			<?php
			}else{
			?>
			<td align="right" width="120px" class="titulos2">
				Tel&eacute;fono:
			</td>
			<td class="listado2">
				<input type="text" id="xtelefono" name="xtelefono">
			</td>
			<td align="right" width="120px" class="titulos2">
				E-Mail:
			</td>
			<td class="listado2" colspan="3">
				<input type="text" id="xmail" name="xmail" size="50">
			</td>
		</tr>
		<?php
		}
		if($option=='P'){
		?>
		<tr>
			<td align="right" width="120px" class="titulos2">
				Entidad Referencia:
			</td>

			<td class="listado2">
				<div id="xentidad">
					<select id="entidad_referencia" name="entidad_referencia" class="select">
						<option value="ALCALIS">ALCALIS</option>
						<option value="INCORA">INCORA</option>
						<option value="CAJA AGRARIA">CAJA AGRARIA</option>
						<option value="FERROCARRILES">FERROCARRILES</option>
						<option value="PUERTOSNALES">PUERTOS NALES</option>
						<option value="ACCIONSOCIAL">ACCION SOLCIAL</option>
					</select>
				</div>
			</td>
			<td align="right" width="120px" class="titulos2">
				Observacion:
			</td>
			<td class="listado2" colspan="3">
				<input type="text" id="xobs" name="xobs" size="50">
			</td>
			<?php
			}
			?>
		</tr>
		<tr>
			<td align="right" width="120px" class="titulos2">
				Celular:
			</td>
			<td class="listado2" colspan="5">
				<input type="text" id="xmovil" name="xmovil">
			</td>
		</tr>
	</table>
	<?php
	$xres->addAssign("xcontent","innerHTML",ob_get_clean());
	$xres->addAssign("xnombre", "innerHTML", $xAutocomplete2->getHTML());
	$xres->addAssign("xubicacion", "innerHTML", $xAutocompleteUbi->getHTML());
	$xres->addAssign("xoption", "value", $option);

	$_SESSION["xAutocomplete2"]=serialize($xAutocomplete2);
	$_SESSION["xAutocompleteUbi"]=serialize($xAutocompleteUbi);
	$xres->addScript("xajax_setDepto('170-1','11-170-1')");
	$xres->addScript("xajax_setMpio('11-170-1','1-11-170-1')");
	$con->disconnect();
	return utf8_encode($xres->getXML());
}

function saveData($form){
	$xres=new xajaxResponse();
	define('ADODB_ASSOC_CASE', 1);
	require('../include/adodb/adodb.inc.php');
	require_once("../include/xWebComponent/database/DBConnection.php");
	require_once("../include/xWebComponent/database/data.php");
	$con=new DBConnection($dbdriver, $server, $user, $password, $database);
	// compruebo si hay o no id
	$nombre=$form['xAutocomplete2_text'];
	$xnombre2=$form['xnombre2'];
	$apellido1=$form['xapellido1'];
	$apellido2=$form['xapellido2'];
	$xcedula=$form['xcedula'];
	$xnit=$form['xnit'];
	$xsigla=$form['xsigla'];
	$xdireccion=$form['xdireccion'];
	$xtelefono1=$form['xtelefono'];
	$xtelefono2=$form['xtelefono2'];
	$xmail=str_replace(' ','',trim($form['xmail']));
	$entidad_referencia=$form['entidad_referencia'];
	$xobs=$form['xobs'];
	$xreplegal=$form['xreplegal'];
	/*$muni_codi=$form['muni_codi'];
    $muni_codi=explode("-", $muni_codi);
    $dpto_codi=$muni_codi[1];
    $id_pais=$muni_codi[2];
    $id_cont=$muni_codi[3];
    $muni_codi=$muni_codi[0];*/
	$xciudad=$form['xAutocompleteUbi_hidden'];
	$xciudadspli=explode("-",$xciudad);
	$dpto_codi=$xciudadspli[2];
	$id_pais=$xciudadspli[1];
	$id_cont=$xciudadspli[0];
	$muni_codi=$xciudadspli[3];
	$cel=$form['xmovil'];
//	if($form['xAutocomplete2_hidden']=='' ){ // Se crea un nuevo registro

	$xarray=explode("-", $form['xAutocomplete2_hidden']);
	$id=$xarray[1];
	switch($form['xoption']){
		case "E":
			/*$query="SELECT SGD_OEM_NIT
                                FROM SGD_OEM_OEMPRESAS
                                WHERE
                                SGD_OEM_NIT='$xnit'
                                ";
            //$xres->addAlert($query);
            $res=$con->execQuery($query);
            if($res->fields['SGD_OEM_NIT']==''){*/

//mssql						$query="SELECT id AS M FROM SEC_OEM_OEMPRESAS";
//postgresql:
//				$query="SELECT last_value AS M FROM SEC_OEM_OEMPRESAS";
//				$res=$con->execQuery($query);
//				$id=$res->fields['M']+1;
//mssql						$res=$con->execQuery("UPDATE SEC_OEM_OEMPRESAS SET ID=$id");
//Postgresql:
			$res=$con->execQuery("SELECT nextval('SEC_OEM_OEMPRESAS') As SECE");
			$id=$res->fields['SECE'];

			$query="INSERT INTO SGD_OEM_OEMPRESAS(
									SGD_OEM_CODIGO, 
									TDID_CODI,
									SGD_OEM_OEMPRESA, 
									SGD_OEM_REP_LEGAL,
									SGD_OEM_NIT,
									SGD_OEM_SIGLA,
									MUNI_CODI,
									DPTO_CODI, 
									SGD_OEM_DIRECCION,
									SGD_OEM_TELEFONO, 
									ID_CONT,
									ID_PAIS,
									SGD_OEM_TELCELULAR,
									SGD_OEM_EMAIL)
								VALUES(
									'$id',
									'4',
									'$nombre',
									'$xreplegal',
									'$xnit',
									'$xsigla',
									'$muni_codi',
									'$dpto_codi',
									'$xdireccion',
									'$xtelefono1',
									'$id_cont',
									'$id_pais',
									'$cel',
									'$xmail')";
			$busq=$nombre;
			$_SESSION['ADMCONTACTONOMBRE']=$nombre ;
			/*}else{
                $xres->addAlert("No se puede crear la Empresa. Existe una empresa con el mismo NIT");
                return utf8_encode($xres->getXML());
            }*/
			break;
		CASE "U":
			$query="SELECT last_value AS M FROM SEC_BODEGA_EMPRESAS";
			$res=$con->execQuery($query);
			$id=$res->fields['M']+1;
			$res=$con->execQuery("SELECT nextval('SEC_BODEGA_EMPRESAS')");
			/*$query="SELECT MAX(IDENTIFICADOR_EMPRESA) AS M FROM BODEGA_EMPRESAS";
            $res=$con->execQuery($query);
            $id=$res->fields['M']+1;*/
			$query="INSERT INTO  BODEGA_EMPRESAS (
								IDENTIFICADOR_EMPRESA, 
								NUIR,
								ARE_ESP_SECUE,
								NOMBRE_DE_LA_EMPRESA, 								
								NIT_DE_LA_EMPRESA, 
								SIGLA_DE_LA_EMPRESA, 
								DIRECCION, 
								CODIGO_DEL_DEPARTAMENTO, 
								CODIGO_DEL_MUNICIPIO, 
								TELEFONO_1, 
								TELEFONO_2, 
								EMAIL, 
								NOMBRE_REP_LEGAL, 
								ID_CONT, 
								ID_PAIS) 
								VALUES (
								'$id',
								'$id',
								'$id',
								'$nombre',
								'$xnit',
								'$xsigla',
								'$xdireccion',
								'$dpto_codi',
								'$muni_codi',
								'$xtelefono1',
								'$xtelefono2',
								'$xmail',
								'$xreplegal',
								'$id_cont',
								'$id_pais')";
			$_SESSION['ADMCONTACTONOMBRE']=$nombre ;
			break;
		CASE "C":
			$query="SELECT SGD_CIU_CODIGO
						FROM SGD_CIU_CIUDADANO 
						WHERE 
						SGD_CIU_CEDULA='$xcedula' ";

			$res=$con->execQuery($query);
			$ciuCod=$res->fields['SGD_CIU_CODIGO'];
			$queryC="SELECT count(*) As COUNTCIU
						FROM SGD_CIU_CIUDADANO
						WHERE SGD_CIU_CEDULA='$xcedula'
						AND SGD_CIU_NOMBRE='$xnombre2'
						AND SGD_CIU_APELL1='$apellido1'
						AND SGD_CIU_APELL2='$apellido2'
						AND SGD_CIU_DIRECCION='$xdireccion'
						AND (sgd_ciu_inactivo=0 or sgd_ciu_inactivo is null)
						";
			$resC=$con->execQuery($queryC);
			$countCiu=$resC->fields['COUNTCIU'];
			if(trim($xcedula)=='' or $ciuCod==''){
				if(($xnombre2)!='' And ($apellido1)!='' ) {
					if ($countCiu>0){
						$xres->addAlert("Ya existe un Ciudadano con el mismo Nombre, Identificacion y Direccion");
						return utf8_encode($xres->getXML());
					} else{

						$query = "SELECT last_value AS M FROM SEC_CIU_CIUDADANO";
						$res = $con->execQuery($query);
						$id = $res->fields['M'] + 1;
						$res = $con->execQuery("SELECT nextval('SEC_CIU_CIUDADANO')");

						/*$query="SELECT MAX(SGD_CIU_CODIGO) AS M FROM SGD_CIU_CIUDADANO";
                        $res=$con->execQuery($query);
                        $id=$res->fields['M']+1;*/
						$query = "INSERT INTO SGD_CIU_CIUDADANO(
									  SGD_CIU_CODIGO, 
									  TDID_CODI,
									  SGD_CIU_NOMBRE,
									  SGD_CIU_APELL1, 
									  SGD_CIU_APELL2,
									  SGD_CIU_CEDULA,
									  SGD_CIU_DIRECCION,
									  SGD_CIU_TELEFONO,
									  SGD_CIU_EMAIL,
									  MUNI_CODI,
									  DPTO_CODI,
									  ID_CONT,
									  ID_PAIS,
									 /* ENTIDAD_REFERENCIA, */
									 /* OBSERVACIONES, */
									  SGD_CIU_TELCELULAR)
								VALUES (
									'$id',
									'2',
									'$xnombre2',
									'$apellido1',
									'$apellido2',
									'$xcedula',
									'$xdireccion',
									'$xtelefono1',
									'$xmail',
									'$muni_codi',
									'$dpto_codi',
									'$id_cont',
									'$id_pais',
									/* --'$entidad_referencia', */
									/* --'$xobs', */
									'$cel')";
						$busq = $xnombre2 . " " . $apellido1 . " " . $apellido2;
						$_SESSION['ADMCONTACTONOMBRE'] = $xnombre2 . " " . $apellido1 . " " . $apellido2;
					}
				}else{
					$xres->addAlert("Debe ingresar el nombre de la persona");
					return utf8_encode($xres->getXML());
				}
			}else{
				$xres->addAlert("Ya Existe un ciudadano con el No.de Documento ingresado.");
				return utf8_encode($xres->getXML());
			}
			break;
		CASE "P":
			$query="SELECT MAX(SGD_CIU_CODIGO) AS M FROM SGD_CIU_CIUDADANO";
			$res=$con->execQuery($query);
			$id=$res->fields['M']+1;
			$query="INSERT INTO SGD_CIU_CIUDADANO(
								  SGD_CIU_CODIGO, 
								  TDID_CODI,
								  SGD_CIU_NOMBRE,
								  SGD_CIU_APELL1, 
								  SGD_CIU_APELL2,
								  SGD_CIU_CEDULA,
								  SGD_CIU_DIRECCION,
								  SGD_CIU_TELEFONO,
								  SGD_CIU_EMAIL,
								  MUNI_CODI,
								  DPTO_CODI,
								  ID_CONT,
								  ID_PAIS,
								 /* -- OBSERVACIONES, */
								 /* -- ENTIDAD_REFERENCIA, */
								  PENSIONADO)
							VALUES (
								'$id',
								'2',
								'$xnombre2',
								'$apellido1',
								'$apellido2',
								'$xcedula',
								'$xdireccion',
								'$xtelefono1',
								'$xmail',
								'$muni_codi',
								'$dpto_codi',
								'$id_cont',
								'$id_pais',
								--'$xobs',
								--'$entidad_referencia',
								'S')";
			$_SESSION['ADMCONTACTONOMBRE']=$xnombre2." ".$apellido1." ".$apellido2 ;
			break;
	}
	$res=$con->execQuery($query);
	//$xres->addScript("window.opener.document.getElementById('xbusq').value='$busq';");
	//$xres->addScript("window.opener.setComboDest('$busq');");
	$xres->addAlert("Contacto Creado correctamente.");
	//$xres->addScript("window.close();");
	$xres->addScript("get('xFormContact').reset();");


	$xres->addScript("get('xFormContact').reset();");

	return utf8_encode($xres->getXML());
}

function updateContacto($form){
	$xres=new xajaxResponse();
	define('ADODB_ASSOC_CASE', 1);
	require('../include/adodb/adodb.inc.php');
	require_once("../include/xWebComponent/database/DBConnection.php");
	require_once("../include/xWebComponent/database/data.php");
	$con=new DBConnection($dbdriver, $server, $user, $password, $database);
	// compruebo si hay o no id
	$nombre=$form['xAutocomplete2_text'];
	$xnombre2=$form['xnombre2'];
	$apellido1=$form['xapellido1'];
	$apellido2=$form['xapellido2'];
	$xcedula=$form['xcedula'];
	$xnit=$form['xnit'];
	$xsigla=$form['xsigla'];
	$xdireccion=$form['xdireccion'];
	$xtelefono1=$form['xtelefono'];
	$xtelefono2=$form['xtelefono2'];
	$xmail=str_replace(' ','',trim($form['xmail']));
	$entidad_referencia=$form['entidad_referencia'];
	$xobs=$form['xobs'];
	$xreplegal=$form['xreplegal'];
	$xciudad=$form['xAutocompleteUbi_hidden'];
	$xciudadspli=explode("-",$xciudad);
	$dpto_codi=$xciudadspli[2];
	$id_pais=$xciudadspli[1];
	$id_cont=$xciudadspli[0];
	$muni_codi=$xciudadspli[3];
	$cel=$form['xmovil'];
	// Es un registro ya existente
	$xarray=explode("-", $form['xAutocomplete2_hidden']);
	$id=$xarray[1];
	switch($form['xoption']){
		case "E":
			$query="UPDATE SGD_OEM_OEMPRESAS SET								
								SGD_OEM_OEMPRESA='$nombre',
								TDID_CODI='4',
						        SGD_OEM_REP_LEGAL='$xreplegal',
						        SGD_OEM_NIT='$xnit',
						        SGD_OEM_SIGLA='$xsigla',
						        MUNI_CODI='$muni_codi',
						        DPTO_CODI= '$dpto_codi',
						        SGD_OEM_DIRECCION='$xdireccion',
						        SGD_OEM_TELEFONO='$xtelefono1',
						        ID_CONT='$id_cont',
						        ID_PAIS='$id_pais',
								SGD_OEM_TELCELULAR='$cel',
								SGD_OEM_EMAIL='$xmail'
								WHERE
									SGD_OEM_CODIGO='$id'
								";
			$_SESSION['ADMCONTACTONOMBRE']=$nombre;
			break;
		CASE "U":
			$query="UPDATE BODEGA_EMPRESAS SET								
								NOMBRE_DE_LA_EMPRESA='$nombre',
								NIT_DE_LA_EMPRESA='$xnit', 
								SIGLA_DE_LA_EMPRESA='$xsigla', 
								DIRECCION='$xdireccion', 
								CODIGO_DEL_DEPARTAMENTO='$dpto_codi', 
								CODIGO_DEL_MUNICIPIO='$muni_codi', 
								TELEFONO_1='$xtelefono1', 
								TELEFONO_2='$xtelefono2', 
								EMAIL='$xmail', 
								NOMBRE_REP_LEGAL='$xreplegal', 
								ID_CONT='$id_cont', 
								ID_PAIS='$id_pais'
								WHERE 
								IDENTIFICADOR_EMPRESA='$id'
								";
			$_SESSION['ADMCONTACTONOMBRE']=$nombre;
			break;
		CASE "C":
			$query="UPDATE SGD_CIU_CIUDADANO SET								  
								  TDID_CODI='2',
								  SGD_CIU_NOMBRE='$xnombre2',
								  SGD_CIU_APELL1='$apellido1',
								  SGD_CIU_APELL2='$apellido2',
								  SGD_CIU_CEDULA='$xcedula',
								  SGD_CIU_DIRECCION='$xdireccion',
								  SGD_CIU_TELEFONO='$xtelefono1',
								  SGD_CIU_EMAIL='$xmail',
								  MUNI_CODI='$muni_codi',
								  DPTO_CODI='$dpto_codi',
								  ID_CONT='$id_cont',
								  ID_PAIS='$id_pais',
								SGD_CIU_TELCELULAR='$cel'
								WHERE SGD_CIU_CODIGO='$id'
								";
			$_SESSION['ADMCONTACTONOMBRE']=$xnombre2." ".$apellido1." ".$apellido2 ;
			break;
		CASE "P":
			$query="UPDATE SGD_CIU_CIUDADANO SET								  
								  TDID_CODI='2',
								  SGD_CIU_NOMBRE='$xnombre2',
								  SGD_CIU_APELL1='$apellido1',
								  SGD_CIU_APELL2='$apellido2',
								  SGD_CIU_CEDULA='$xcedula',
								  SGD_CIU_DIRECCION='$xdireccion',
								  SGD_CIU_TELEFONO='$xtelefono1',
								  SGD_CIU_EMAIL='$xmail',
								  MUNI_CODI='$muni_codi',
								  DPTO_CODI='$dpto_codi',
								  ID_CONT='$id_cont',
								  ID_PAIS='$id_pais',
								  OBSERVACIONES='$xobs',
								  ENTIDAD_REFERENCIA='$entidad_referencia'
								WHERE SGD_CIU_CODIGO='$id'
								";
			$_SESSION['ADMCONTACTONOMBRE']=$xnombre2." ".$apellido1." ".$apellido2 ;
			break;
	}
	$res=$con->execQuery($query);
	$xres->addAlert("Contacto Actualizado correctamente");


	$xres->addScript("get('xFormContact').reset();");

	return utf8_encode($xres->getXML());
}


function deleteContact($form){
	$xres=new xajaxResponse();
	define('ADODB_ASSOC_CASE', 1);
	require('../include/adodb/adodb.inc.php');
	require_once("../include/xWebComponent/database/DBConnection.php");
	require_once("../include/xWebComponent/database/data.php");
	$con=new DBConnection($dbdriver, $server, $user, $password, $database);
	// compruebo si hay o no id
	$xarray=explode("-", $form['xAutocomplete2_hidden']);
	$id=$xarray[1];
	switch($form['xoption']){
		case "E":
			// Verifico que se pueda eliminar
			$query="SELECT SGD_OEM_CODIGO FROM SGD_DIR_DRECCIONES WHERE SGD_OEM_CODIGO='$id' ";
			$res=$con->execQuery($query);
			if($res->fields['SGD_OEM_CODIGO']==''){
				$query="DELETE FROM SGD_OEM_OEMPRESAS WHERE SGD_OEM_CODIGO='$id' ";
			}else{
				$query="UPDATE sgd_oem_oempresas SET sgd_oem_inactivo=1 WHERE SGD_OEM_CODIGO='$id' ";
				$msj="No es posible eliminar por estar relacionado con radicados. Se ha deshabilitado";
			}
			break;
		CASE "U":
			$query="UPDATE BODEGA_EMPRESAS SET ACTIVA='0' WHERE IDENTIFICADOR_EMPRESA='$id'	";
			break;
		CASE "C":
			$query="SELECT SGD_DIR_CODIGO FROM SGD_DIR_DRECCIONES WHERE SGD_CIU_CODIGO='$id' ";
			$res=$con->execQuery($query);
			if($res->fields['SGD_DIR_CODIGO']==''){
				$query="DELETE FROM SGD_CIU_CIUDADANO WHERE SGD_CIU_CODIGO='$id' ";
			}else{
				$query="UPDATE sgd_ciu_ciudadano SET sgd_ciu_inactivo=1 WHERE sgd_ciu_codigo='$id' ";
				$msj="No es posible eliminar por estar relacionado con radicados. Se ha deshabilitado";
			}
			break;
		CASE "P":
			$query="SELECT SGD_DIR_CODIGO FROM SGD_DIR_DRECCIONES WHERE SGD_CIU_CODIGO='$id'";
			$res=$con->execQuery($query);
			if($res->fields['SGD_DIR_CODIGO']==''){
				$query="DELETE FROM SGD_CIU_CIUDADANO WHERE SGD_CIU_CODIGO='$id' ";
			}else{
				$msj="No es posible deshabilitar por estar relacionado con radicados.";
			}
			break;
	}
	if($msj==""){
		$res=$con->execQuery($query);
		$xres->addAlert("Contacto Eliminado");
		$xres->addScript("get('xFormContact').reset();");
	}else{
		$res=$con->execQuery($query);
		$xres->addAlert("Contacto Desactivado");
		$xres->addScript("get('xFormContact').reset();");
		$xres->addAlert("$msj");
	}
	return utf8_encode($xres->getXML());
}
function loadUbicacion($cad){


}

function loadData($cad){

	$xres=new xajaxResponse();
	define('ADODB_ASSOC_CASE', 1);
	require('../include/adodb/adodb.inc.php');
	require_once("../include/xWebComponent/database/DBConnection.php");
	require_once("../include/xWebComponent/database/data.php");
	$con=new DBConnection($dbdriver, $server, $user, $password, $database);
	$cad=explode("-", $cad);
	$option=$cad[0];
	$id=$cad[1];
	switch($option){
		case 'E':
			$query="SELECT  O.SGD_OEM_OEMPRESA, 
					        O.SGD_OEM_REP_LEGAL,
					        O.SGD_OEM_NIT,
					        O.SGD_OEM_SIGLA,
					        O.MUNI_CODI,
					        O.DPTO_CODI, 
					        O.SGD_OEM_DIRECCION,
					        O.SGD_OEM_TELEFONO, 
					        O.ID_CONT,
					        O.ID_PAIS,
							O.SGD_OEM_TELCELULAR,
							O.SGD_OEM_EMAIL,
							UBI.XCIUDAD
						FROM SGD_OEM_OEMPRESAS O 
						LEFT JOIN
							(
								SELECT   
										COALESCE(M.MUNI_NOMB,' ') || '/' || COALESCE(D.DPTO_NOMB,' ') || '/' || COALESCE(P.NOMBRE_PAIS,' ') AS XCIUDAD,M.MUNI_CODI,D.DPTO_CODI,P.ID_PAIS,P.ID_CONT
									FROM MUNICIPIO M
									INNER JOIN DEPARTAMENTO D
									ON D.DPTO_CODI = M.DPTO_CODI
									AND D.ID_CONT  = M.ID_CONT
									AND D.ID_PAIS  = M.ID_PAIS
									INNER JOIN SGD_DEF_PAISES P
									ON P.ID_CONT    = D.ID_CONT
									AND P.ID_PAIS   = D.ID_PAIS



							) UBI on
							UBI.MUNI_CODI=O.MUNI_CODI AND
							UBI.DPTO_CODI=O.DPTO_CODI AND
							UBI.ID_PAIS=O.ID_PAIS AND
							UBI.ID_CONT=O.ID_CONT
							
						WHERE SGD_OEM_CODIGO='$id'";
			$res=$con->execQuery($query);
			$xreplegal=$res->fields['SGD_OEM_REP_LEGAL'];
			$xnit=$res->fields['SGD_OEM_NIT'];
			$xsigla=$res->fields['SGD_OEM_SIGLA'];
			$xpais=$res->fields['ID_PAIS'];
			$xcont=$res->fields['ID_CONT'];
			$xmuni=$res->fields['MUNI_CODI'];
			$xdpto=$res->fields['DPTO_CODI'];
			$xdireccion=$res->fields['SGD_OEM_DIRECCION'];
			$xtelefono=$res->fields['SGD_OEM_TELEFONO'];
			$xmail=$res->fields['SGD_OEM_EMAIL'];
			$cel=$res->fields['SGD_OEM_TELCELULAR'];
			$xubicacion=$res->fields['XCIUDAD'];
			break;
		case 'U':
			$query="SELECT IDENTIFICADOR_EMPRESA, NOMBRE_DE_LA_EMPRESA, NUIR, NIT_DE_LA_EMPRESA, SIGLA_DE_LA_EMPRESA, DIRECCION, CODIGO_DEL_DEPARTAMENTO, 
CODIGO_DEL_MUNICIPIO, TELEFONO_1, TELEFONO_2, EMAIL, NOMBRE_REP_LEGAL, B.ID_CONT, B.ID_PAIS,UBI.XCIUDAD
FROM BODEGA_EMPRESAS B
LEFT JOIN
							(
								SELECT   
										COALESCE(M.MUNI_NOMB,' ') || '/'  || COALESCE(D.DPTO_NOMB,' ') || '/' || COALESCE(P.NOMBRE_PAIS,' ') AS XCIUDAD,M.MUNI_CODI,D.DPTO_CODI,P.ID_PAIS,P.ID_CONT
									FROM MUNICIPIO M
									INNER JOIN DEPARTAMENTO D
									ON D.DPTO_CODI = M.DPTO_CODI
									AND D.ID_CONT  = M.ID_CONT
									AND D.ID_PAIS  = M.ID_PAIS
									INNER JOIN SGD_DEF_PAISES P
									ON P.ID_CONT    = D.ID_CONT
									AND P.ID_PAIS   = D.ID_PAIS



							) UBI on
							UBI.MUNI_CODI=B.CODIGO_DEL_MUNICIPIO AND
							UBI.DPTO_CODI=B.CODIGO_DEL_DEPARTAMENTO AND
							UBI.ID_PAIS=B.ID_PAIS AND
							UBI.ID_CONT=B.ID_CONT

WHERE IDENTIFICADOR_EMPRESA='$id'";
			$res=$con->execQuery($query);
			$xreplegal=$res->fields['NOMBRE_REP_LEGAL'];
			$xnit=$res->fields['NIT_DE_LA_EMPRESA'];
			$xsigla=$res->fields['SIGLA_DE_LA_EMPRESA'];
			$xpais=$res->fields['ID_PAIS'];
			$xcont=$res->fields['ID_CONT'];
			$xmuni=$res->fields['CODIGO_DEL_MUNICIPIO'];
			$xdpto=$res->fields['CODIGO_DEL_DEPARTAMENTO'];
			$xdireccion=$res->fields['DIRECCION'];
			$xtelefono=$res->fields['TELEFONO_1'];
			$xtelefono2=$res->fields['TELEFONO_2'];
			$xmail=$res->fields['EMAIL'];
			$xubicacion=$res->fields['XCIUDAD'];
			break;
		case 'C':
			$query="SELECT
						  SGD_CIU_CODIGO, 
						  SGD_CIU_NOMBRE,
						  SGD_CIU_APELL1, 
						  SGD_CIU_APELL2,
						  SGD_CIU_CEDULA,
						  SGD_CIU_DIRECCION,
						  SGD_CIU_TELEFONO,
						  SGD_CIU_EMAIL,
						  C.MUNI_CODI,
						  C.DPTO_CODI,
						  C.ID_CONT,
						  C.ID_PAIS,
						  SGD_CIU_TELCELULAR,
						  UBI.XCIUDAD
						FROM SGD_CIU_CIUDADANO C
						LEFT JOIN
							(
								SELECT   
									COALESCE(M.MUNI_NOMB,' ') || '/' || COALESCE(D.DPTO_NOMB,' ') || '/' || COALESCE(P.NOMBRE_PAIS,' ') AS XCIUDAD,M.MUNI_CODI,D.DPTO_CODI,P.ID_PAIS,P.ID_CONT
									FROM MUNICIPIO M
									INNER JOIN DEPARTAMENTO D
									ON D.DPTO_CODI = M.DPTO_CODI
									AND D.ID_CONT  = M.ID_CONT
									AND D.ID_PAIS  = M.ID_PAIS
									INNER JOIN SGD_DEF_PAISES P
									ON P.ID_CONT    = D.ID_CONT
									AND P.ID_PAIS   = D.ID_PAIS
							) UBI on
							UBI.MUNI_CODI=C.MUNI_CODI AND
							UBI.DPTO_CODI=C.DPTO_CODI AND
							UBI.ID_PAIS=C.ID_PAIS AND
							UBI.ID_CONT=C.ID_CONT

						WHERE SGD_CIU_CODIGO='$id'
						";
			$res=$con->execQuery($query);
			$xreplegal="";
			$xnombre=$res->fields['SGD_CIU_NOMBRE'];
			$xapellido1=$res->fields['SGD_CIU_APELL1'];
			$xapellido2=$res->fields['SGD_CIU_APELL2'];
			$xpais=$res->fields['ID_PAIS'];
			$xcont=$res->fields['ID_CONT'];
			$xmuni=$res->fields['MUNI_CODI'];
			$xdpto=$res->fields['DPTO_CODI'];
			$xdireccion=$res->fields['SGD_CIU_DIRECCION'];
			$xtelefono=$res->fields['SGD_CIU_TELEFONO'];
			$xmail=$res->fields['SGD_CIU_EMAIL'];
			$xcedula=$res->fields['SGD_CIU_CEDULA'];
			$cel=$res->fields['SGD_CIU_TELCELULAR'];
			$xubicacion=$res->fields['XCIUDAD'];
			break;
		case 'P':
			$query="SELECT
						  SGD_CIU_CODIGO, 
						  COALESCE(SGD_CIU_NOMBRE,' ') as SGD_CIU_NOMBRE,
						  COALESCE(SGD_CIU_APELL1,' ') as SGD_CIU_APELL1, 
						  COALESCE(SGD_CIU_APELL2,' ') as SGD_CIU_APELL2,
						  COALESCE(SGD_CIU_CEDULA,' ') as SGD_CIU_CEDULA,
						  COALESCE(SGD_CIU_DIRECCION,' ') as SGD_CIU_DIRECCION,
						  COALESCE(SGD_CIU_TELEFONO,' ') as SGD_CIU_TELEFONO,
						  COALESCE(SGD_CIU_EMAIL,' ') as SGD_CIU_EMAIL,
						  C.MUNI_CODI,
						 C.DPTO_CODI,
						  C.ID_CONT,
						  C.ID_PAIS,
						  OBSERVACIONES,
						  ENTIDAD_REFERENCIA,
						  UBI.XCIUDAD
						FROM SGD_CIU_CIUDADANO C
						LEFT JOIN
							(
								SELECT   
										COALESCE(M.MUNI_NOMB,' ') || '/' || COALESCE(D.DPTO_NOMB,' ') || '/' || COALESCE(P.NOMBRE_PAIS,' ') AS XCIUDAD,M.MUNI_CODI,D.DPTO_CODI,P.ID_PAIS,P.ID_CONT
									FROM MUNICIPIO M
									INNER JOIN DEPARTAMENTO D
									ON D.DPTO_CODI = M.DPTO_CODI
									AND D.ID_CONT  = M.ID_CONT
									AND D.ID_PAIS  = M.ID_PAIS
									INNER JOIN SGD_DEF_PAISES P
									ON P.ID_CONT    = D.ID_CONT
									AND P.ID_PAIS   = D.ID_PAIS
							) UBI on
							UBI.MUNI_CODI=C.MUNI_CODI AND
							UBI.DPTO_CODI=C.DPTO_CODI AND
							UBI.ID_PAIS=C.ID_PAIS AND
							UBI.ID_CONT=C.ID_CONT
						WHERE SGD_CIU_CODIGO='$id'";
			$res=$con->execQuery($query);
			$xreplegal="";
			$xnombre=$res->fields['SGD_CIU_NOMBRE'];
			$xapellido1=$res->fields['SGD_CIU_APELL1'];
			$xapellido2=$res->fields['SGD_CIU_APELL2'];
			$xpais=$res->fields['ID_PAIS'];
			$xcont=$res->fields['ID_CONT'];
			$xmuni=$res->fields['MUNI_CODI'];
			$xdpto=$res->fields['DPTO_CODI'];
			$xdireccion=$res->fields['SGD_CIU_DIRECCION'];
			$xtelefono=$res->fields['SGD_CIU_TELEFONO'];
			$xmail=$res->fields['SGD_CIU_EMAIL'];
			$xcedula=$res->fields['SGD_CIU_CEDULA'];
			$xobs=$res->fields['OBSERVACIONES'];
			$entidad_referencia=$res->fields['ENTIDAD_REFERENCIA'];
			$xubicacion=$res->fields['XCIUDAD'];
			$xres->addAssign("xobs","value", $xobs);
			if($entidad_referencia !=''){
				ob_start();
				?>
				<select id="entidad_referencia" name="entidad_referencia" class="select">
					<option value="ALCALIS" <?php if($entidad_referencia=='ALCALIS'){ echo "selected=selected"; } ?>>ALCALIS</option>
					<option value="INCORA" <?php if($entidad_referencia=='INCORA'){ echo "selected=selected"; } ?>>INCORA</option>
					<option value="CAJA AGRARIA" <?php if($entidad_referencia=='CAJA AGRARIA'){ echo "selected=selected"; } ?>>CAJA AGRARIA</option>
					<option value="FERROCARRILES" <?php if($entidad_referencia=='FERROCARRILES'){ echo "selected=selected"; } ?>>FERROCARRILES</option>
				</select>
				<?php
				$xres->addAssign("xentidad","innerHTML", ob_get_clean());
			}
			break;
	}
	if($xnombre!=''){
		$xres->addAssign("xnombre","value", $xnombre);
	}

	if($xnombre!=''){
		$xres->addAssign("xnombre","value", utf8_decode($xnombre));
	}
	$xres->addAssign("xnombre2","value", utf8_decode($xnombre));
	$xres->addAssign("xnit","value", $xnit);
	$xres->addAssign("xreplegal","value", utf8_decode($xreplegal));
	$xres->addAssign("xsigla","value", utf8_decode($xsigla));
	$xres->addAssign("xdireccion","value", utf8_decode($xdireccion));
	$xres->addAssign("xtelefono","value", utf8_decode($xtelefono));
	$xres->addAssign("xtelefono2","value", utf8_decode($xtelefono2));
	$xres->addAssign("xapellido1","value", utf8_decode($xapellido1));
	$xres->addAssign("xapellido2","value", utf8_decode($xapellido2));
	$xres->addAssign("xmail","value", utf8_decode($xmail));
	$xres->addAssign("xcedula","value", utf8_decode($xcedula));
	$xres->addAssign("xmovil","value", utf8_decode($cel));
	$xres->addAssign("xAutocompleteUbi_hidden","value", "$xcont-$xpais-$xdpto-$xmuni");
	$xres->addAssign("xAutocompleteUbi_text","value", utf8_decode($xubicacion));
	$xres->addScript("xajax_setPais('$xpais-$xcont');  for(var i=0; i<100000; i++){ var a=0; }xajax_setDepto('$xpais-$xcont', '$xdpto-$xpais-$xcont'); for(var i=0; i<1000000; i++){ var a=0; } xajax_setMpio('$xdpto-$xpais-$xcont', '$xmuni-$xdpto-$xpais-$xcont'); ");
	$con->disconnect();
	return utf8_encode($xres->getXML());
}

function setPais($cod){
	$xres=new xajaxResponse();
	define('ADODB_ASSOC_CASE', 1);
	require('../include/adodb/adodb.inc.php');
	require_once("../include/xWebComponent/database/DBConnection.php");
	require_once("../include/xWebComponent/database/data.php");
	$con=new DBConnection($dbdriver, $server, $user, $password, $database);
	$cod=explode("-", $cod);
	$id_pais=$cod[0];
	$id_cont=$cod[1];
	$query="SELECT ID_PAIS, ID_CONT, NOMBRE_PAIS FROM SGD_DEF_PAISES ORDER BY NOMBRE_PAIS";
	$res=$con->execQuery($query);
	ob_start();
	while(!$res->EOF){
		?>
		<option value="<?php echo $res->fields['ID_PAIS']."-".$res->fields['ID_CONT']; ?>"
			<?php
			if($id_pais==$res->fields['ID_PAIS'] && $id_cont==$res->fields['ID_CONT']){
				echo "selected";
			}
			?>>
			<?php echo $res->fields['NOMBRE_PAIS']; ?></option>
		<?php
		$res->moveNext();
	}
	$xres->addAssign("id_pais","innerHTML",ob_get_clean());
	$con->disconnect();
	return utf8_encode($xres->getXML());
}

function setDepto($cod, $sel=""){
	define('ADODB_ASSOC_CASE', 1);
	$xres=new xajaxResponse();
	require('../include/adodb/adodb.inc.php');
	require_once("../include/xWebComponent/database/DBConnection.php");
	require_once("../include/xWebComponent/database/data.php");
	$con=new DBConnection($dbdriver, $server, $user, $password, $database);
	$cod=explode("-", $cod);
	$id_pais=$cod[0];
	$id_cont=$cod[1];
	$query="SELECT 0 AS DPTO_CODI, '0 - Elija Departamento' AS DPTO_NOMB UNION
				SELECT DPTO_CODI, DPTO_NOMB FROM DEPARTAMENTO WHERE ID_CONT=$id_cont AND ID_PAIS=$id_pais Order by 2";
	$res=$con->execQuery($query);
	ob_start();
	$ban=0;
	$sel=explode("-",$sel);
	while(!$res->EOF){
		?>
		<option value="<?php echo $res->fields['DPTO_CODI']."-".$id_pais."-".$id_cont; ?>"
			<?php
			if($sel==''){
				if($ban==0){
					$dpto_codi=$res->fields['DPTO_CODI'];
					$ban=1;
				}
				if($id_pais==170 && $id_cont==1 && $res->fields['DPTO_CODI']==11){
					echo "selected";
					$dpto_codi="11";
				}
			}else{
				if($id_pais==$sel[1] && $id_cont==$sel[2] && $res->fields['DPTO_CODI']==$sel[0]){
					echo "selected";
					$dpto_codi=$sel[0];
				}
			}
			?>>
			<?php echo $res->fields['DPTO_NOMB']; ?></option>
		<?php
		$res->moveNext();
	}
	$xres->addAssign("dpto_codi","innerHTML",ob_get_clean());
	$con->disconnect();
	if($dpto_codi!='' && $sel==''){
		$xres->addScript("xajax_setMpio('".$dpto_codi."-".$id_pais."-".$id_cont."')");
	}else{
		$xres->addScript("get('muni_codi').selectedIndex=-1;");
	}
	return utf8_encode($xres->getXML());
}

function setMpio($cod, $sel=""){
	$xres=new xajaxResponse();
	define('ADODB_ASSOC_CASE', 1);
	require('../include/adodb/adodb.inc.php');
	require_once("../include/xWebComponent/database/DBConnection.php");
	require_once("../include/xWebComponent/database/data.php");
	$con=new DBConnection($dbdriver, $server, $user, $password, $database);
	$cod=explode("-", $cod);
	$dpto_codi=$cod[0];
	$id_pais=$cod[1];
	$id_cont=$cod[2];
	$query="/*SELECT 0 AS MUNI_CODI, ' Elija la Ciudad' AS MUNI_NOMB FROM MUNICIPIO UNION*/
				SELECT MUNI_CODI, MUNI_NOMB FROM MUNICIPIO 
				WHERE ID_CONT=$id_cont AND ID_PAIS=$id_pais AND DPTO_CODI=$dpto_codi Order by 2";
	$res=$con->execQuery($query);
	ob_start();
	$sel=explode("-",$sel);
	while(!$res->EOF){
		?>
		<option value="<?php echo $res->fields['MUNI_CODI']."-".$dpto_codi."-".$id_pais."-".$id_cont; ?>"
			<?php
			if($sel==''){
				if($id_pais==170 && $id_cont==1 && $dpto_codi==11 && $res->fields['MUNI_CODI']==1){
					echo "selected";
				}
			}else{
				if($id_pais==$sel[2] && $id_cont==$sel[3] && $dpto_codi==$sel[1] && $res->fields['MUNI_CODI']==$sel[0]){
					echo "selected";
				}
			}
			?>>
			<?php echo $res->fields['MUNI_NOMB']; ?></option>
		<?php
		$res->moveNext();
	}
	$xres->addAssign("muni_codi","innerHTML",ob_get_clean());
	$con->disconnect();
	return utf8_encode($xres->getXML());
}
?>
