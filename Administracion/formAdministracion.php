<?php
use Philo\Blade\Blade;
if(!isset($_SESSION))
{
    session_start();
}

    $ruta_raiz = ".."; 
    if (!$_SESSION['dependencia'])
        header ("Location: $ruta_raiz/cerrar_session.php");

/**
  * Se anadio compatibilidad con variables globales en Off
  * @autor Jairo Losada 2009-05
  * @licencia GNU/GPL V 3
  */

foreach($_GET as $k=>$v) $$k=$v;

$krd         = $_SESSION["krd"];
$dependencia = $_SESSION["dependencia"];
$usua_doc    = $_SESSION["usua_doc"];
$codusuario  = $_SESSION["codusuario"];
$tip3Nombre  = $_SESSION["tip3Nombre"];
$tip3desc    = $_SESSION["tip3desc"];
$tip3img     = $_SESSION["tip3img"];
$ruta_raiz   = "..";

if (isset($_GET["carpeta"]))
    $nomcarpeta      = $_GET["carpeta"];
else
    $nomcarpeta = "";

if (isset($_GET["tipo_carpt"]))
    $tipo_carpt      = $_GET["tipo_carpt"];
else
    $tipo_carpt = "";
    
if (isset($_GET["adodb_next_page"]))
    $adodb_next_page = $_GET["adodb_next_page"];
else
    $adodb_next_page = "";



$sendSession     = session_name().'='.session_id();

?>
<html>
<head>
<title>Administracion de orfeo</title>
    <link rel="stylesheet" href="<?=$ruta_raiz."/estilos/".$_SESSION["ESTILOS_PATH"]?>/orfeo.css">
</head>

<link rel="stylesheet" href="<?= $_SESSION['base_url'] ?>/estilos/orfeo.css">

<link href="<?= $_SESSION['base_url'] ?>/app/resources/global/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet"
      type="text/css"/>

<link href="<?= $_SESSION['base_url'] ?>/app/resources/global/plugins/font-awesome/css/fonts.googleapis.css" rel="stylesheet"
      type="text/css"/>
<link href="<?= $_SESSION['base_url'] ?>/app/resources/global/plugins/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />
<link href="<?= $_SESSION['base_url'] ?>/app/resources/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="<?= $_SESSION['base_url'] ?>/app/resources/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet"
      type="text/css"/>
<link href="<?= $_SESSION['base_url'] ?>/app/resources/global/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css" />

<link href="<?= $_SESSION['base_url'] ?>/app/resources/global/css/select2.min.css" rel="stylesheet" />
<link href="<?= $_SESSION['base_url'] ?>/app/resources/global/css/select2-bootstrap.min.css"  rel="stylesheet"/>
<link href="<?= $_SESSION['base_url'] ?>/app/resources/global/plugins/chosen_v1.8.7/chosen.css" rel="stylesheet" type="text/css" />
<link href="<?= $_SESSION['base_url'] ?>/app/resources/global/plugins/sweetalert2-9.10.2/package/dist/sweetalert2.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
    var base_url = '<?php echo $_SESSION['base_url']; ?>';
    var anio_actual='<?= date('Y') ?>'
</script>

<style>
    .borde_tab {
        border: 1px solid #377584 !important;
        border-radius: 5px !important;
        -moz-border-radius: 5px !important;
        -webkit-border-radius: 5px !important;
    }
    .borde_tab {
        border: thin solid #377584 !important;
        border-radius: 10px!important;
        -moz-border-radius: 5px !important;
        -webkit-border-radius: 5px !important;
    }
    .borde_tab {
        border: 1px solid #377584 !important;
        border-radius: 5px !important;
        -moz-border-radius: 5px !important;
        -webkit-border-radius: 5px !important;
    }
    table[Attributes Style] {
        width: 71% !important;
        margin-inline-start: auto !important;
        margin-inline-end: auto !important;
        border-top-width: 0px !important;
        border-right-width: 0px !important;
        border-bottom-width: 0px !important;
        border-left-width: 0px !important;
        -webkit-border-horizontal-spacing: 5px !important;
        -webkit-border-vertical-spacing: 5px !important;
    }

    table {
        white-space: normal !important;
        line-height: normal !important;
        font-weight: normal !important;
        font-size: medium !important;
        font-style: normal !important;
        color: -internal-quirk-inherit !important;
        text-align: start !important;
        font-variant: normal !important;
    }

    table {
        display: table !important;
        border-collapse: separate !important;
        border-spacing: 2px !important;
        border-color: grey !important;
    }
</style>
<body>
<table width="71%" align="center" border="0" cellpadding="0" cellspacing="5" class="borde_tab">
<tr bordercolor="#FFFFFF">
	<td colspan="2" class="titulos4"><div align="center"><strong>M&Oacute;DULO DE ADMINISTRACI&Oacute;N</strong></div></td>
</tr>
<tr bordercolor="#FFFFFF">
<td align="center" class="listado2" width="48%">
    <a href='usuario/mnuUsuarios.php?<?=$sendSession?>&krd=<?=$krd?>' target='mainFrame' class="vinculos">1. USUARIOS Y PERFILES</a>
</td>
<td align="center" class="listado2" width="48%"><a href="tbasicas/adm_dependencias.php?<?=$sendSession?>" class="vinculos" target="mainFrame">2. DEPENDENCIAS</a></td>
</tr>
<tr bordercolor="#FFFFFF">
<td align="center" class="listado2" width="48%"> <a  href="tbasicas/adm_nohabiles.php?<?=$sendSession?>" class="vinculos" target='mainFrame'>3. DIAS NO HABILES</a></td>
<td align="center" class="listado2" width="48%"><a href="tbasicas/adm_fenvios.php?<?=$sendSession?>" class="vinculos" target='mainFrame'>4. ENV&Iacute;O DE CORRESPONDENCIA</a> </td>
</tr>
<tr bordercolor="#FFFFFF">
<td align="center" class="listado2" width="48%"><a href="tbasicas/adm_tsencillas.php?<?=$sendSession?>" class="vinculos" target='mainFrame'>5. TABLAS SENCILLAS</a></td>
<td align="center" class="listado2" width="48%"><a href="tbasicas/adm_trad.php?<?=$sendSession?>&krd=<?=$krd?>" class="vinculos" target='mainFrame'>6. TIPOS DE RADICACI&Oacute;N</a></td>
</tr>
<tr bordercolor="#FFFFFF">
<td align="center" class="listado2" width="48%"><a href="tbasicas/adm_paises.php?<?=$sendSession?>" class="vinculos" target='mainFrame'>7. PA&Iacute;SES</a></td>
<td align="center" class="listado2" width="48%"><a href="tbasicas/adm_dptos.php?<?=$sendSession?>" class="vinculos" target='mainFrame'>8. DEPARTAMENTOS</a></td>
</tr>
<tr bordercolor="#FFFFFF">
<td align="center" class="listado2" width="48%"><a href="tbasicas/adm_mcpios.php?<?=$sendSession?>" class="vinculos" target='mainFrame'>9. MUNICIPIOS</a></td>
<td align="center" class="listado2" width="48%"><a href="tbasicas/adm_tarifas.php?<?=$sendSession?>" class="vinculos" target='mainFrame'>10. TARIFAS</a></td>
</tr>
<tr bordercolor="#FFFFFF">
<td align="center" class="listado2" width="48%"><a href="tbasicas/adm_plantillas.php?<?=$sendSession?>" class="vinculos" target='mainFrame'>11. PLANTILLAS</a></td>
<td align="center" class="listado2" width="48%"><a href="tbasicas/adm_soportes.php?<?=$sendSession?>" class="vinculos" target='mainFrame'>12. SOPORTES</a></td>
</tr>
    <tr bordercolor="#FFFFFF">
        <td align="center" class="listado2" width="48%">
            <a  title="Configuraciones" href="#"
                 style="border-radius:20px !important; background-color:transparent" class="vinculos"
                 onclick="modalConfig()">
                13. CONFIGURACIONES</a>
        </td>
        <td align="center" class="listado2" width="48%">
            <?php if($_SESSION['usua_admin_sistema']==2){ ?>
                <a  title="Administrar acceso a configuraciones, segun perfil" href="#"
                    style="border-radius:20px !important; background-color:transparent" class="vinculos"
                    onclick="modalPerfilConfig()">
                   ADMINISTRAR CONFIGURACIONES CON PERFILES</a>
           <?php } ?>

        </td>
    </tr>
<tr bordercolor="#FFFFFF">
<!--
<td align="center" class="listado2" width="48%"><a href="adm_plantillas_gen.php?<?=$sendSession?>" class="vinculos" target='mainFrame'>15. CABECERAS DE COMBINACION</a></td>
</tr>
<tr bordercolor="#FFFFFF">
<td align="center" class="listado2" width="48%"><a href="tbasicas/adm_contactos.php?<?=$sendSession?>" class="vinculos" target='mainFrame'>11. CONTACTOS</a></td>
<td align="center" class="listado2" width="48%"><a href="tbasicas/adm_esp.php?<?=$sendSession?>&krd=<?=$krd?>" class="vinculos" target='mainFrame'>12. ENTIDADES</a></td>
</tr>
-->
</table>
<br>



<?
 // MODULO OPCIONAL DE ADMINISTRACION DE FUNCIONARIOS Y ENTIDADES
 /* Por SuperSolidaria
    Modifico y Adapto Jairo Losada 08/2009 */

?>
<!--Estas funciones estan activas pero no se sabe para que son
<table width="71%" align="center" border="0" cellpadding="0" cellspacing="5" class="borde_tab">
<tr bordercolor="#FFFFFF">
	<td colspan="2" class="titulos4" align="center">
          Opcional x SES
        </td>
</tr>
<tr bordercolor="#FFFFFF">
	<td align="center" class="listado2" width="48%">
		<a href='entidad/listaempresas.php?<?=$sendSession?>&krd=<?=$krd?>' target='mainFrame'  class="vinculos">12. ENTIDADES  V.SES</a>
	</td>
	<td align="center" class="listado2" width="48%">
	<a href='usuario/listafuncionarios.php?<?=$sendSession?>' target='mainFrame'  class="vinculos">12.1 FUNCIONARIO - ENTIDAD</a>
</td>
</tr>
</table>
-->

<?php
include_once "$ruta_raiz/include/db/ConnectionHandler.php";
$db = new ConnectionHandler("$ruta_raiz");
//Alerta cuando una Dependencia se ha quedado Sin Jefe
$sql_jefe = "SELECT D.DEPE_CODI, D.DEPE_NOMB AS DEPENDENCIAS
				FROM DEPENDENCIA D
				LEFT JOIN USUARIO U ON D.DEPE_CODI=U.DEPE_CODI
				AND U.USUA_CODI=1
				Where 
				u.usua_nomb is null
				AND D.DEPE_ESTADO=1
				ORDER BY 1";
$rsj = $db->conn->Execute($sql_jefe);
$c=$rsj->fields["DEPE_CODI"];
if ( $c ) {
    echo "<br><label align=left class=titulosError2> <center>Notificación: las siguientes dependencias están activas y No tienen Jefe: </center></label>";
    while (!$rsj -> EOF) {
        $depes=$rsj->fields["DEPE_CODI"]."-".$rsj->fields["DEPENDENCIAS"];
        echo "<label class=leidos> <center>".$depes."</center> </label>";
        $rsj->MoveNext();
    }
    echo "<label class=leidos2> <center>Debe asignar inmediatamente un usuario Jefe o Responsable <br>
					Para más información, solicite soporte técnico.</center>
				  </label>";

}
require '../vendor/autoload.php';
require_once '../config.php';

ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
error_reporting(E_ALL);

$configHelper = new \App\Helpers\Configuraciones($db,'../');
$titulos=$configHelper->getTitulosTratamiento();

?>
<div id="modal_show_configs" class="modal fade"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-blue-chambray bg-font-yellow text-center" id="header_vista_previa">

                <button type="button" title="Cerrar" aria-hidden="true" class="btn btn-lg " data-dismiss="modal"
                        style="float: right;background-color: #2C3E50!important; border-radius:20px !important"
                >
                    <i class="fa fa-remove" style="color:white"></i></button>
                <h4 class="modal-title"><strong>Configuraciones</strong></h4>
            </div>
            <div class="modal-body" id="modal_body_show_configs">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-lg blue"
                        style="border-radius:20px !important" onclick="guardarConfigs()">
                    Guardar
                </button>
                <button type="button" class="btn btn-lg" data-dismiss="modal"
                        style="border-radius:20px !important">
                    Cerrar
                </button>
            </div>
        </div>
    </div>
</div>

<div id="modal_editar_titulos" class="modal fade"   aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-blue-chambray bg-font-yellow text-center" id="header_vista_previa">

                <button type="button" title="Cerrar" aria-hidden="true" class="btn btn-lg " data-dismiss="modal"
                        style="float: right;background-color: #2C3E50!important; border-radius:20px !important"
                >
                    <i class="fa fa-remove" style="color:white"></i></button>
                <h4 class="modal-title"><strong id="strong_modal_edit_titulo">Editar T&iacute;tulo</strong></h4>
            </div>
            <div class="modal-body" id="modal_body_vista_previa">
                <div class="row">
                    <form>
                        <div class="form-body">
                            <div class="form-group">
                                <div class=" col-md-2">Nombre del T&iacute;tulo</div>
                                <div class=" col-md-4">
                                    <input type="text" class="form-control" id="inputedit_titulo">
                                </div>

                                <div class=" col-md-2">Abreviatura</div>
                                <div class=" col-md-4">
                                    <input type="text" class="form-control" id="input_abreviatura">
                                </div>

                                <div class=" col-md-2">Activo</div>
                                <div class=" col-md-4" style="display : inline-flex;">
                                    SI&nbsp;
                                    <input type="radio" name="radio_titulo_activo" value="1"
                                           id="radio_titulo_activo_si"
                                           checked >
                                    &nbsp; &nbsp; &nbsp;
                                    NO&nbsp;
                                    <input type="radio" name="radio_titulo_activo" value="0"
                                           id="radio_titulo_activo_no"  >
                                </div>
                                <div class="form-group">
                                    <div class=" col-md-2">Orden</div>
                                    <div class=" col-md-4">
                                        <input type="number" min="1" class="form-control" id="input_titulo_orden">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-lg blue"
                        style="border-radius:20px !important" onclick="guardarTitulo()">
                    Guardar
                </button>
                <button type="button" class="btn btn-lg" data-dismiss="modal"
                        style="border-radius:20px !important">
                    Cerrar
                </button>
            </div>
        </div>
    </div>
</div>

<div id="modal_agregar_titulo_memo" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-blue-chambray bg-font-yellow text-center" >

                <button type="button" title="Cerrar" aria-hidden="true" class="btn btn-lg " data-dismiss="modal"
                        style="float: right;background-color: #2C3E50!important; border-radius:20px !important"
                >
                    <i class="fa fa-remove"></i></button>
                <h4 class="modal-title"><strong >Agregar Titulo Oficios Internos</strong></h4>
            </div>
            <div class="modal-body" >
                <div class="row">
                    <form>
                        <div class="form-body">
                            <div class="form-group">
                                <div class=" col-md-2">Nombre del T&iacute;tulo</div>
                                <div class=" col-md-4">
                                    <input type="text" class="form-control" id="add_name_titulo_memo">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-lg blue"
                        style="border-radius:20px !important" onclick="guardarTituloMemo()">
                    Guardar
                </button>
                <button type="button" class="btn btn-lg" data-dismiss="modal"
                        style="border-radius:20px !important">
                    Cerrar
                </button>
            </div>
        </div>
    </div>
</div>


<div id="modal_perfil_configs" class="modal fade"  aria-hidden="true" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-blue-chambray bg-font-yellow text-center" id="header_vista_previa">

                <button type="button" title="Cerrar" aria-hidden="true" class="btn btn-lg " data-dismiss="modal"
                        style="float: right;background-color: #2C3E50!important; border-radius:20px !important"
                >
                    <i class="fa fa-remove" style="color:white"></i></button>
                <h4 class="modal-title"><strong>Administrar permisos con configuraciones</strong></h4>
            </div>



            <div class="modal-body" id="modal_body_perfil_configs">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-lg blue"
                        style="border-radius:20px !important" onclick="guardarPerfilConfigs()">
                    Guardar
                </button>
                <button type="button" class="btn btn-lg" data-dismiss="modal"
                        style="border-radius:20px !important">
                    Cerrar
                </button>
            </div>
        </div>
    </div>
</div>


</body>

<!--[if lt IE 9]>
<script src="<?= $_SESSION['base_url'] ?>app/resources/global/plugins/respond.min.js"></script>
<script src="<?= $_SESSION['base_url'] ?>app/resources/global/plugins/excanvas.min.js"></script>
<script src="<?= $_SESSION['base_url'] ?>app/resources/global/plugins/ie8.fix.min.js"></script>
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="<?= $_SESSION['base_url'] ?>app/resources/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?= $_SESSION['base_url'] ?>app/resources/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?= $_SESSION['base_url'] ?>app/resources/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="<?= $_SESSION['base_url'] ?>app/resources/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?= $_SESSION['base_url'] ?>app/resources/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?= $_SESSION['base_url'] ?>app/resources/global/plugins/bootstrap-switch/js/bootstrap-switch.js" type="text/javascript"></script>


<!-- END CORE PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?= $_SESSION['base_url'] ?>app/resources/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?= $_SESSION['base_url'] ?>app/resources/pages/scripts/components-bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="<?= $_SESSION['base_url'] ?>app/resources/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
<script src="<?= $_SESSION['base_url'] ?>app/resources/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
<script src="<?= $_SESSION['base_url'] ?>app/resources/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
<script src="<?= $_SESSION['base_url'] ?>app/resources/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
<script src="<?= $_SESSION['base_url'] ?>app/resources/global/plugins/sweetalert2-9.10.2/package/dist/sweetalert2.js" type="text/javascript"></script>
<script src="<?= $_SESSION['base_url'] ?>app/resources/global/plugins/chosen.jquery.min.js" type="text/javascript"></script>
<!--  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>-->
<script src="<?= $_SESSION['base_url'] ?>app/resources/global/plugins/select2.full.min.js" type="text/javascript"></script>
<script src="<?= $_SESSION['base_url'] ?>app/resources/global/plugins/editor.js" type="text/javascript"></script>

<script src="<?= $_SESSION['base_url'] ?>app/resources/global/plugins/tinymce/tinymce.min.js" type="text/javascript"></script>

<script src="<?= $_SESSION['base_url'] ?>app/resources/global/plugins/html-docx.js" type="text/javascript"></script>
<script src="<?= $_SESSION['base_url'] ?>app/resources/global/plugins/file-saver/FileSaver.js" type="text/javascript"></script>

<script src="<?= $_SESSION['base_url'] ?>app/resources/global/plugins/bootstrap-table/bootstrap-table.js"  type="text/javascript"></script>
<script src="<?= $_SESSION['base_url'] ?>app/resources/pages/scripts/toastr.min.js"  type="text/javascript"></script>
<script src="<?= $_SESSION['base_url'] ?>app/resources/pages/scripts/ui-toastr.min.js"  type="text/javascript"></script>
<script src="<?= $_SESSION['base_url'] ?>app/resources/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"  ></script>
<script src="<?= $_SESSION['base_url'] ?>app/resources/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.es.min.js"  ></script>

<script src="<?= $_SESSION['base_url'] ?>app/resources/global/plugins/chosen_v1.8.7/chosen.jquery.js" ></script>
<script src="<?= $_SESSION['base_url'] ?>app/resources/apps/scripts/funciones_globales.js?v=<?= date('YmdHis') ?>"  ></script>

<script src="<?= $_SESSION['base_url'] ?>app/resources/apps/scripts/scriptconfiguraciones.js?v=<?= date('YmdHis') ?>"  ></script>
<script>
    var modulosMostrar="TODOS";
    var titulos_tratamiento = new Array();
    /*funcion que almacena variables que vienene desde el servidor, generalmente arreglos*/
    function lleganDatosAdminist(titulos) {
        titulos_tratamiento = titulos;
    }

    $(document).ready(function () {
        lleganDatosAdminist(<?php  echo json_encode($titulos) ?>)
    })

</script>



</html>
