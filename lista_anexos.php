<?php
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);
//session_start();
$ruta_raiz = ".";
$ln = $_SESSION["digitosDependencia"];
$perm_tipif_anexo = $_SESSION["perm_tipif_anexo"];
$perm_borrar_anexo = $_SESSION["perm_borrar_anexo"];
//empieza Anexos   por  Julian Rolon
//lista los documentos del radicado y proporciona links para ver historicos de cada documento
//este archivo se incluye en la pagina verradicado.php
//print ("uno");
if (!$ruta_raiz) $ruta_raiz = ".";

include_once("$ruta_raiz/vendor/autoload.php");
include_once("$ruta_raiz/class_control/anexo.php");
include_once "$ruta_raiz/include/db/ConnectionHandler.php";
require_once("$ruta_raiz/class_control/TipoDocumento.php");
include_once "$ruta_raiz/class_control/firmaRadicado.php";
include "$ruta_raiz/config.php";
require_once("$ruta_raiz/class_control/ControlAplIntegrada.php");
require_once("$ruta_raiz/class_control/AplExternaError.php");
require_once("$ruta_raiz/app/Helpers/Radicados.php");

$db = new ConnectionHandler(".");
$objTipoDocto = new TipoDocumento($db);
$objTipoDocto->TipoDocumento_codigo($tdoc);
$objFirma = new  FirmaRadicado($db);
$objCtrlAplInt = new ControlAplIntegrada($db);
$helperRadi = new \App\Helpers\Radicados($db, $ruta_raiz);
$helperBorra = new \App\Helpers\Borradores($db, $ruta_raiz);
$configHelper = new \App\Helpers\Configuraciones($db, $ruta_raiz);
$configuraciones = $configHelper->getConfiguraciones(false, false, false);


//$db->conn->SetFetchMode(ADODB_FETCH_NUM);
$db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
//$db2->conn->SetFetchMode(ADODB_FETCH_ASSOC);
$num_archivos = 0;
$anex = &new Anexo($db);
$sqlFechaDocto = $db->conn->SQLDate("Y-m-D H:i:s A", "sgd_fech_doc");
$sqlFechaAnexo = $db->conn->SQLDate("Y-m-D H:i", "anex_fech_anex");
//$sqlFechaAnexo = "to_char(anex_fech_anex, 'YYYY/DD/MM HH:MI')";
$sqlSubstDesc = $db->conn->substr . "(anex_desc, 0, 150)";

include_once("include/query/busqueda/busquedaPiloto1.php");


// Modificado SGD 06-Septiembre-2007
$isql = "select a.id AS ANEXO_ID,
            anex_codigo AS DOCU
            ,anex_tipo_ext AS EXT
			,anex_tamano AS TAMA
			,anex_solo_lect AS RO
            ,usua_nomb AS CREA
			,$sqlSubstDesc AS DESCR
			,anex_nomb_archivo AS NOMBRE
			,anex_numero ANEXNUM
			,ANEX_CREADOR
			,ANEX_ORIGEN
			,ANEX_SALIDA
			,a.ANEX_UBIC
			,$radi_nume_salida as RADI_NUME_SALIDA
			,ANEX_ESTADO
			,anex_depe_creador AS ANEX_DEPE_CREADOR
			,SGD_PNUFE_CODI
			,SGD_DOC_SECUENCIA
			,SGD_DIR_TIPO
			,SGD_DOC_PADRE
			,SGD_TPR_CODIGO
			,SGD_APLI_CODI
			,SGD_TRAD_CODIGO
			,SGD_TPR_CODIGO
			,a.ANEX_TIPO
			,(CASE WHEN a.ANEX_FECH_ANEX IS NULL THEN (SELECT a2.ANEX_FECH_ANEX FROM ANEXOS A2 WHERE A2.RADI_NUME_SALIDA=A.RADI_NUME_SALIDA AND A2.ANEX_FECH_ANEX IS NOT NULL)
            ELSE a.ANEX_FECH_ANEX
       			END) as AANEX_FECH_ANEX
			,a.ANEX_FECH_ANEX
			,a.ANEX_RADI_NUME
			,$sqlFechaDocto as FECDOC
			,$sqlFechaAnexo as FEANEX
			,a.ANEX_TIPO as NUMEXTDOC
			,(SELECT d.sgd_dir_nomremdes from sgd_dir_drecciones d where (d.sgd_anex_codigo=a.anex_codigo  or d.radi_nume_radi=a.radi_nume_salida) AND (a.sgd_dir_tipo=d.sgd_dir_tipo ) limit 1) destino 
            ,a.ANEX_DESC
            ,a.ANEX_REGENERADO
            ,a.ANEX_BORRADO 
		from anexos a, anexos_tipo at ,usuario u
      where anex_radi_nume=$verrad and a.anex_tipo=at.anex_tipo_codi
		   and anex_creador=usua_login and anex_borrado='N'
	   order by ANEXNUM, AANEX_FECH_ANEX,sgd_dir_tipo,a.anex_radi_nume,a.radi_nume_salida";
error_reporting(7); 


$conjugacionValidar = json_decode($_SESSION['NOMBRE_BOTON_VALIDAR_CONJUGADO'], true);
?>

<script>
    swradics = 0;
    radicando = 0;

    function verDetalles(anexo, tpradic, aplinteg, ANEXO_ID=0, radicado, isAnexo) {
        radicados_selecteds_v[0]['radicado'] = radicado;
        anexos_selecteds_v[0] = {};
        anexos_selecteds_v[0]['ANEXO_ID'] = ANEXO_ID;
        if (isAnexo == 0) {
            getUsersValidaron();
        }

        /**

        Esta funcion  queda comentada mientras tanto. se agrego comentarios sobre el codigo php abierto en swal para evitar errores

        if (Object.keys(usersValidaron).length > 0) {
            var texto = Object.keys(usersValidaron).length > 1 ? 'usuarios' : 'usuario'
            Swal.fire('<?= /**$conjugacionValidar[1]**/null; ?>', "El documento ya fué <?=/** strtolower($conjugacionValidar[1])**/null; ?> por " + Object.keys(usersValidaron).length + " " + texto + ", si modifica deberán volver a <?= strtolower($conjugacionValidar[0]) ?>", "warning");
        }
        **/

        optAsigna = "";
        if (swradics == 0) {
            optAsigna = "&verunico=1";
        }
        if (validaEnBotones('modificar', ANEXO_ID, anexo, tpradic, aplinteg, radicado, isAnexo) == true) {

            todoOkModificar(anexo, tpradic, aplinteg, ANEXO_ID, radicado, isAnexo);
        }
    }

    function todoOkModificar(anexo, tpradic, aplinteg, ANEXO_ID=0, radicado, isAnexo) {
        contadorVentanas = contadorVentanas + 1;
        nombreventana = "ventanaDetalles" + contadorVentanas;
        url = "<?=$ruta_raiz?>/nuevo_archivo.php?codigo=" + anexo + "&<?=session_name() . "=" . trim(session_id()) ?>&usua=<?=$krd?>&numrad=<?=$verrad ?>&contra=<?=$drde?>&radi=<?=$verrad?>&tipo=<?=$tipo?>&ent=<?=$ent?><?=$datos_envio?>&ruta_raiz=<?=$ruta_raiz?>" + "&tpradic=" + tpradic + "&ANEXO_ID=" + ANEXO_ID + "&aplinteg=" + aplinteg + optAsigna;
        window.open(url, nombreventana, 'top=0,height=580,width=640,scrollbars=yes,resizable=yes');
        return true;
    }

    function borrarArchivo(anexo, linkarch, radicar_a, procesoNumeracionFechado, is_principal) {
        //alert(is_principal)
        if (confirm('Estas seguro de borrar este archivo anexo ?')) {
            contadorVentanas = contadorVentanas + 1;
            nombreventana = "ventanaBorrar" + contadorVentanas;
            //url="borrar_archivos.php?usua=<?=$krd?>&contra=<?=$drde?>&radi=<?=$verrad?>&anexo="+anexo+"&linkarchivo="+linkarch;

            url = "lista_anexos_seleccionar_transaccion.php?borrar=1&usua=<?=$krd?>&numrad=<?=$verrad?>&&contra=<?=$drde?>&radi=<?=$verrad?>&anexo=" + anexo + "&linkarchivo=" + linkarch + "&numfe=" + procesoNumeracionFechado + "&dependencia=<?=$dependencia?>&codusuario=<?=$codusuario?>";
            window.open(url, nombreventana, 'height=100,width=180');
        }
        return;
    }


    function radicarArchivo(anexo, linkarch, radicar_a, procesoNumeracionFechado, tpradic, aplinteg, numextdoc, ANEXO_ID=0) {

        if (radicando > 0) {
            alert("Ya se esta procesando una radicacion, para re-intentarlo haga click sobre la pesta�a de documentos");
            return;
        }

        if (validaEnBotones('re-generar', ANEXO_ID) == true) {
            radicando++;

            if (confirm('Se combinarán los datos de radicación a éste documento. Est\xe1 seguro  ?')) {
                contadorVentanas = contadorVentanas + 1;
                nombreventana = "mainFrame";
                url = "<?=$ruta_raiz?>/lista_anexos_seleccionar_transaccion.php?radicar=1&radicar_a=" + radicar_a + "&vp=n&<?="&" . session_name() . "=" . trim(session_id()) ?>&radicar_documento=<?=$verrad?>&numrad=<?=$verrad?>&anexo=" + anexo + "&linkarchivo=" + linkarch + "<?=$datos_envio?>" + "&ruta_raiz=<?=$ruta_raiz?>&numfe=" + procesoNumeracionFechado + "&tpradic=" + tpradic + "&aplinteg=" + aplinteg + "&numextdoc=" + numextdoc + "&ANEXO_ID=" + ANEXO_ID;
                window.open(url, nombreventana, 'height=450,width=600');
            }
            return;
        }


    }


    function numerarArchivo(anexo, linkarch, radicar_a, procesoNumeracionFechado) {
        if (confirm('Se asignar\xe1 un n\xfamero a \xe9ste documento. Est\xe1 seguro ?')) {
            contadorVentanas = contadorVentanas + 1;
            nombreventana = "mainFrame";
            url = "<?=$ruta_raiz?>/lista_anexos_seleccionar_transaccion.php?numerar=1" + "&vp=n&<?="krd=$krd&" . session_name() . "=" . trim(session_id()) ?>&radicar_documento=<?=$verrad?>&numrad=<?=$verrad?>&anexo=" + anexo + "&linkarchivo=" + linkarch + "<?=$datos_envio?>" + "&ruta_raiz=<?=$ruta_raiz?>&numfe=" + procesoNumeracionFechado;
            window.open(url, nombreventana, 'height=450,width=600');
        }
        return;
    }

    function asignarRadicado(anexo, linkarch, radicar_a, numextdoc, ANEXO_ID=0) {

        if (radicando > 0) {
            alert("Ya se esta procesando una radicacion, para re-intentarlo haga click sobre la pestaña de documentos");
            return;
        }

        radicando++;

        if (confirm('Esta seguro de asignarle el numero de Radicado a este archivo ?')) {
            contadorVentanas = contadorVentanas + 1;
            nombreventana = "mainFrame";
            url = "<?=$ruta_raiz?>/genarchivo.php?generar_numero=no&radicar_a=" + radicar_a + "&vp=n&<?="&" . session_name() . "=" . trim(session_id()) ?>&radicar_documento=<?=$verrad?>&numrad=<?=$verrad?>&anexo=" + anexo + "&linkarchivo=" + linkarch + "<?=$datos_envio?>" + "&ruta_raiz=<?=$ruta_raiz?>" + "&numextdoc=" + numextdoc + "&ANEXO_ID=" + ANEXO_ID;
            window.open(url, nombreventana, 'height=450,width=600');
        }
        return;

    }

    function ver_tipodocuATRD(anexo, codserie, tsub) {
        <?php
        $isqlDepR = "SELECT RADI_DEPE_ACTU,RADI_USUA_ACTU from radicado
		            WHERE RADI_NUME_RADI = '$numrad'";
        $rsDepR = $db->conn->Execute($isqlDepR);
        $coddepe = $rsDepR->fields['RADI_DEPE_ACTU'];
        $codusua = $rsDepR->fields['RADI_USUA_ACTU'];
        $ind_ProcAnex = "S";
        ?>
        window.open("./radicacion/tipificar_documento.php?<?=session_name() . "=" . session_id()?>&krd=<?=$krd?>&nurad=" + anexo + "&ind_ProcAnex=<?=$ind_ProcAnex?>&codusua=<?=$codusua?>&coddepe=<?=$coddepe?>&tsub=" + tsub + "&codserie=" + codserie + "&texp=<?=$texp?>", "Tipificacion_Documento_Anexos", "height=500,width=750,scrollbars=yes");
    }

    function noPermiso() {
        alert("No tiene permiso para acceder");
    }

    function ver_tipodocuAnex(cod_radi, codserie, tsub) {

        window.open("./radicacion/tipificar_anexo.php?krd=<?=$krd?>&nurad=" + cod_radi + "&ind_ProcAnex=<?=$ind_ProcAnex?>&codusua=<?=$codusua?>&coddepe=<?=$coddepe?>&tsub=" + tsub + "&codserie=" + codserie, "Tipificacion_Documento_Anexos", "height=300,width=1200,scrollbars=yes");
    }


    function vistaPreliminar(anexo, linkarch, linkarchtmp) {
        contadorVentanas = contadorVentanas + 1;
        nombreventana = "mainFrame";
        url = "<?=$ruta_raiz?>/genarchivo.php?vp=s&<?="krd=$krd&" . session_name() . "=" . trim(session_id()) ?>&radicar_documento=<?=$verrad?>&numrad=<?=$verrad?>&anexo=" + anexo + "&linkarchivo=" + linkarch + "&linkarchivotmp=" + linkarchtmp + "<?=$datos_envio?>" + "&ruta_raiz=<?=$ruta_raiz?>";
        window.open(url, nombreventana, 'height=450,width=600');
        return;
    }

    function nuevoArchivo(asigna) {
        contadorVentanas = contadorVentanas + 1;
        optAsigna = "";
        if (asigna == 1) {
            optAsigna = "&verunico=1";
        }
//alert (asigna);

        nombreventana = "ventanaNuevo" + contadorVentanas;
        url = "<?=$ruta_raiz?>/nuevo_archivo.php?codigo=&<?="krd=$krd&" . session_name() . "=" . trim(session_id()) ?>&usua=<?=$krd?>&numrad=<?=$verrad ?>&contra=<?=$drde?>&radi=<?=$verrad?>&tipo=<?=$tipo?>&ent=<?=$ent?>" + "<?=$datos_envio?>" + "&ruta_raiz=<?=$ruta_raiz?>&tdoc=<?=$tdoc?>" + optAsigna;
        window.open(url, nombreventana, 'height=630,width=780,scrollbars=yes,resizable=yes');
        return;
    }


    function nuevoEditWeb(asigna) {
        contadorVentanas = contadorVentanas + 1;
        optAsigna = "";
        if (asigna == 1) {
            optAsigna = "&verunico=1";
        }
//alert (asigna);

        nombreventana = "ventanaNuevo" + contadorVentanas;
        url = "<?=$ruta_raiz?>/edicionWeb/editorWeb.php?codigo=&<?="krd=$krd&" . session_name() . "=" . trim(session_id()) ?>&usua=<?=$krd?>&numrad=<?=$verrad ?>&contra=<?=$drde?>&radi=<?=$verrad?>&tipo=<?=$tipo?>&ent=<?=$ent?>" + "<?=$datos_envio?>" + "&ruta_raiz=<?=$ruta_raiz?>&tdoc=<?=$tdoc?>" + optAsigna;
        window.open(url, nombreventana, 'height=800,width=700,scrollbars=yes,resizable=yes');
        return;
    }

    function Plantillas(plantillaper1) {
        if (plantillaper1 == 0) {
            plantillaper1 = "";
        }
        contadorVentanas = contadorVentanas + 1;
        nombreventana = "ventanaNuevo" + contadorVentanas;
        urlp = "plantilla.php?<?="krd=$krd&" . session_name() . "=" . trim(session_id()); ?>&verrad=<?=$verrad ?>&numrad=<?=$numrad ?>&plantillaper1=" + plantillaper1;
        window.open(urlp, nombreventana, 'top=0,left=0,height=800,width=850');
        return;
    }

    function Plantillas_pb(plantillaper1) {
        if (plantillaper1 == 0) {
            plantillaper1 = "";
        }
        contadorVentanas = contadorVentanas + 1;
        nombreventana = "ventanaNuevo" + contadorVentanas;
        urlp = "crea_plantillas/plantilla.php?<?="krd=$krd&" . session_name() . "=" . trim(session_id()); ?>&verrad=<?=$verrad ?>&numrad=<?=$numrad ?>&plantillaper1=" + plantillaper1;
        window.open(urlp, nombreventana, 'top=0,left=0,height=800,width=850');
        return;
    }

    function regresar() {
        //window.history.go(0);
        window.location.reload();
        window.close();

    }
    <?php include_once "$ruta_raiz/js/funtionImage.php"; ?>
</script>
<link href="./app/resources/global/plugins/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
<link href="./app/resources/global/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet"
      type="text/css"/>

<link href="./app/resources/global/plugins/font-awesome/css/fonts.googleapis.css" rel="stylesheet"
      type="text/css"/>
<link href="./app/resources/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="./app/resources/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet"
      type="text/css"/>
<link href="./app/resources/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet"
      type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN THEME GLOBAL STYLES
el siguiente archivo css, les quita la propiedad de borderradios a los input de form control, por eso lo comente.
<link href="./app/resources/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
-->
<link href="./app/resources/global/css/components-rounded.min.css" rel="stylesheet" id="style_components"
      type="text/css"/>
<link href="./app/resources/global/css/plugins.min.css" rel="stylesheet" type="text/css"/>
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link href="./app/resources/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css"/>
<link href="./app/resources/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css"/>


<script src="./app/resources/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="./app/resources/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="./app/resources/global/plugins/bootstrap-table/bootstrap-table.js" type="text/javascript"></script>



<link href="./app/resources/global/css/select2.min.css" rel="stylesheet"/>
<link href="./app/resources/global/css/select2-bootstrap.min.css" rel="stylesheet"/>
<link href="./app/resources/global/plugins/sweetalert2-9.10.2/package/dist/sweetalert2.min.css" rel="stylesheet"
      type="text/css"/>
<link href="./app/resources/global/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css"/>


<link href="./app/resources/global/plugins/progressladabtn/ladda-themeless.min.css" rel="stylesheet" type="text/css"/>

<link href="./app/resources/global/plugins/x-editable/bootstrap-editable.css"
      rel="stylesheet"/>
<link href="./app/resources/global/plugins/typeahead/typeahead.js-bootstrap.css" rel="stylesheet" type="text/css"/>


<script type="text/javascript">
    var base_url = '<?php echo $_SESSION['base_url']; ?>';
</script>

<style>
    @font-face {
        font-family: 'Glyphicons Halflings';
        src: url("./app/resources/global/plugins/bootstrap/fonts/bootstrap/glyphicons-halflings-regular.eot");
        src: url("./app/resources/global/plugins/bootstrap/fonts/bootstrap/glyphicons-halflings-regular.eot?#iefix") format("embedded-opentype"), url("./app/resources/global/plugins/bootstrap/fonts/bootstrap/glyphicons-halflings-regular.woff2") format("woff2"), url("./app/resources/global/plugins/bootstrap/fonts/bootstrap/glyphicons-halflings-regular.woff") format("woff"), url("./app/resources/global/plugins/bootstrap/fonts/bootstrap/glyphicons-halflings-regular.ttf") format("truetype"), url("./app/resources/global/plugins/bootstrap/fonts/bootstrap/glyphicons-halflings-regular.svg#glyphicons_halflingsregular") format("svg");
    }

    .glyphicon {
        position: relative;
        top: 1px;
        display: inline-block;
        font-family: 'Glyphicons Halflings';
        font-style: normal;
        font-weight: normal;
        line-height: 1;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
    }

    .glyphicon-paperclip:before {
        content: "\e142";
    }

    .borde_tab {
        border: 1px solid #377584;
        border-radius: 5px;
        -moz-border-radius: 5px;
        -webkit-border-radius: 5px;
    }

    .select2-container .select2-selection__rendered > *:first-child.select2-search--inline {
        width: 100% !important;
    }
    .select2-container .select2-selection__rendered > *:first-child.select2-search--inline .select2-search__field {
        width: 100% !important;
    }

    .select2-selection__clear{
        display: none !important;
    }

    .fa-item i {
        color: #999494 !important;
    }

    #btn_historyVal .fa-angle-down:before {
        line-height: 1.4;
    }


</style>

<style>

    .borde_tab {
        border: thin solid #377584 !important;
        border-collapse: initial !important;
        border-spacing: 2px 2px !important;
    }

    .popover, .tooltip {
        z-index: 9999999 !important;
    }

    .select2-results__option[aria-selected=true] {
        display: none !important;
    }
    .select2-results__option--selected {
        display: none !important;
    }

</style>
<body bgcolor="#FFFFFF">
<!--table width="100%" border="0" cellspacing="1" cellpadding="0" class=borde_tab>
<tr>
<td  height="25" class="titulos2" colspan="10" width="60%">
<a href="http://orfeogpl.info/orfeogplVideos/radicacionSalida.ogv" target='VideosOrfeoGPL' alt='Descarga de Video Prueba de orfeogpl.org. Aprox 11 Megas'  title='Descarga de Video Prueba de orfeogpl.org. Aprox 11 Megas'>
<img src='imagenes/videoOrfeo.png' width=40 border=0>
</a>
</td>
</tr>
</table-->

<?php

/*
   * Ajuste validacion permisos unificados
   * @author Liliana Gomez Velasquez
   * @since 10 noviembre 2009
   */
include_once "$ruta_raiz/tx/verLinkArchivo.php";
$verLinkArchivo = new verLinkArchivo($db);

//Fin Modificacion
$rowan = array();
$rs = $db->query($isql);

if (!$ruta_raiz_archivo) $ruta_raiz_archivo = $ruta_raiz;
$directoriobase = "$ruta_raiz_archivo/bodega/";
//Flag que indica si el radicado padre fue generado desde esta Ã¡rea de anexos
$swRadDesdeAnex = $anex->radGeneradoDesdeAnexo($verrad);


$infoRadPrincipal = "select * from radicado where radi_nume_radi=" . $verrad;
$infoRadPrincipal = $db->query($infoRadPrincipal);

$rs = $db->query($isql);

$colspan = 17;
?>

<table WIDTH="100%" align="left" id="tabla"
       border="0" cellpadding="0" cellspacing="1" class="borde_tab" bgcolor="cdd0FF">
    <tr class="t_bordeGris" align="left">
        <td colspan="<?= $colspan ?>" class="titulos4" align="center"><img
                    src="<?= $ruta_raiz ?>/imagenes/estadoDocInfo.gif"
                    align='left'> <br>GENERACION DE DOCUMENTOS Y ANEXOS
        </td>
    </tr>

    <tr class='etextomenu' align='middle'>
        <th width='10%' class="titulos2" align="center">
            <img src="<?= $ruta_raiz ?>/imagenes/estadoDoc.gif" width="131" height="32" align="left">
        </th>
        <th width='10%' class="titulos2">Radicado</th>
        <th width='4%' class="titulos2">Tipo</th>
        <th width='3%' class="titulos2">EXP</font></th>
        <th width='0%' class="titulos2"></th>
        <th width='30%' class="titulos2">Descripción</th>
        <th width='10%' class="titulos2">Fecha Anexado/Rad</th>
        <th width='18%' class="titulos2">Usuario Crea</th>
        <th width='15%' class="titulos2">Destino</th>
        <th width='1%' class="titulos2" align="center">Tamaño (kb)</th>
        <?php
        if ($_SESSION["FUNCTION_VALIDAR_RADICADO"] == 'SI') { ?>
            <th width='1%' class='titulos2' align='center'><?= $conjugacionValidar[5] ?></th>
        <?php } ?>
        <th width='3%' class="titulos2" align="center">Solo Lectura</th>
        <!--th  width='13%' class="titulos2">NUMERADO</th-->
        <th width='35%' colspan="6" class="titulos2">ACCION</th>
    </tr>
    <?php

    $a = 0;
    $primervalidar = false;

    /**
     * $asuntosRadicados , almacenará los asuntos de los radicados que se puedan validar
     */
    $asuntosRadicados = array();

    if ($a == 0) {

        $i = 0;
        $a = 0;

        while (!$rs->EOF) {

            $tieneBorrador = array();
            if ($rs->fields['RADI_NUME_SALIDA'] != NULL && $rs->fields['RADI_NUME_SALIDA'] != '') {
                $tieneBorrador = $helperBorra->get_borradorByRadi($rs->fields['RADI_NUME_SALIDA']);
                
            }


            $tieneTransacDevolucion=0;
            $estaAnulado =0;
            if($rs->fields['RADI_NUME_SALIDA']!=null && $rs->fields['RADI_NUME_SALIDA']!='NULL'){
                $tieneTransacDevolucion = "SELECT COUNT(1) FROM radicado r INNER JOIN hist_eventos h ON r.radi_nume_radi=h.radi_nume_radi WHERE  h.sgd_ttr_codigo=28 AND r.radi_nume_radi=".$rs->fields['RADI_NUME_SALIDA'];
                $exeBuscarTrasnaccion28 = $db->conn->Execute($tieneTransacDevolucion);
                $tieneTransacDevolucion= $exeBuscarTrasnaccion28->fields["COUNT"];
                $estadoAnulado = "SELECT COUNT(1) FROM radicado rad where rad.sgd_eanu_codigo = 2 and rad.radi_nume_radi = ".$rs->fields['RADI_NUME_SALIDA'];
                $exeBuscarEstAnulado = $db->conn->Execute($estadoAnulado);
                $estaAnulado = $exeBuscarEstAnulado->fields["COUNT"];

            }
            if (is_null($rs->fields['RADI_NUME_SALIDA']) && $rs->fields['ANEX_RADI_NUME']!=null && $rs->fields['ANEX_RADI_NUME']!='NULL'){
                $estadoAnulado = "SELECT COUNT(1) FROM radicado rad where rad.sgd_eanu_codigo = 2 and rad.radi_nume_radi = ".$rs->fields['ANEX_RADI_NUME'];
                $exeBuscarEstAnulado = $db->conn->Execute($estadoAnulado);
                $estaAnulado = $exeBuscarEstAnulado->fields["COUNT"];
            }

            $ANEXO_ID = $rs->fields["ANEXO_ID"];

            $aplinteg = $rs->fields["SGD_APLI_CODI"];
            $numextdoc = $rs->fields["NUMEXTDOC"];
            $tpradic = $rs->fields["SGD_TRAD_CODIGO"];
            $coddocu = $rs->fields["DOCU"];
            $origen = $rs->fields["ANEX_ORIGEN"]; //-- 0 Anexo desde Orfeoscan-Uploader, 1 Anexo web desde Pestaña Documentos, 1 desde Formulario Web Ciudadanos, __ desde AsociarImagen como Anexo
            $anexDesc = $rs->fields["ANEX_DESC"];
            if ($rs->fields["ANEX_SALIDA"] == 1)
            if ($rs->fields["ANEX_SALIDA"] == 1) $num_archivos++;
            $puedeRadicarAnexo = $objCtrlAplInt->contiInstancia($coddocu, $MODULO_RADICACION_DOCS_ANEXOS, 2);
            $linkarchivo = $directoriobase . substr(trim($coddocu), 0, 4) . "/" . intval(substr(trim($coddocu), 4, $ln)) . "/docs/" . trim($rs->fields["NOMBRE"]);
            $linkarchivo_vista = "$ruta_raiz/bodega/" . substr(trim($coddocu), 0, 4) . "/" . intval(substr(trim($coddocu), 4, $ln)) . "/docs/" . trim($rs->fields["NOMBRE"]) . "?time=" . time();
            $linkarchivotmp = $directoriobase . substr(trim($coddocu), 0, 4) . "/" . intval(substr(trim($coddocu), 4, $ln)) . "/docs/tmp" . trim($rs->fields["NOMBRE"]);
            if (!trim($rs->fields["NOMBRE"])) $linkarchivo = "";
            ?>
            <tr class="listado1" bgcolor="cdd0FF">
                <?php
                if ($origen == 2) {    //echo " class='timpar' ";
                    if ($rs->fields["NOMBRE"] == "No") {
                        $linkarchivo = "";
                    }
                    echo "";
                }
                if ($rs->fields["RADI_NUME_SALIDA"] != 0) {
                    $cod_radi = $rs->fields["RADI_NUME_SALIDA"];
                } else {
                    $cod_radi = $coddocu;
                }


                $anex_estado = $rs->fields["ANEX_ESTADO"];
                if ($anex_estado <= 1) {
                    $img_estado = "<img src=$ruta_raiz/imagenes/docRecibido.gif ";
                }
                if ($anex_estado == 2) {
                    $estadoFirma = $objFirma->firmaCompleta($cod_radi);
                    if ($estadoFirma == "NO_SOLICITADA")
                        $img_estado = "<img src=$ruta_raiz/imagenes/docRadicado.gif  border=0>";
                    else if ($estadoFirma == "COMPLETA") {
                        $img_estado = "<img src=$ruta_raiz/imagenes/docFirmado.gif  border=0>";
                    } else if ($estadoFirma == "INCOMPLETA") {
                        $img_estado = "<img src=$ruta_raiz/imagenes/docEsperaFirma.gif border=0>";
                    }
                }
                if ($anex_estado == 3) {
                    $img_estado = "<img src=$ruta_raiz/imagenes/docImpreso.gif>";
                }
                if ($anex_estado == 4) {
                    $img_estado = "<img src=$ruta_raiz/imagenes/docEnviado.gif>";
                }
                ?>



                <?php

                //busco la info del radicado asociado
                $infoasociado = "select * from radicado where radi_nume_radi=" . $rs->fields["RADI_NUME_SALIDA"];
                $infoasociado = $db->query($infoasociado);
                $radi_path = "";
                if ($rs->fields["ANEX_SALIDA"] == 1 &&
                    $rs->fields["RADI_NUME_SALIDA"] != $rs->fields["ANEX_RADI_NUME"] &&
                    $rs->fields["RADI_NUME_SALIDA"] != "null" && $rs->fields["RADI_NUME_SALIDA"] != null &&
                    $rs->fields["ANEX_ESTADO"] == 4 &&
                    ($rs->fields["SGD_DOC_PADRE"] == null || $rs->fields["SGD_DOC_PADRE"] == "null" || $rs->fields["SGD_DOC_PADRE"] == "NULL") &&
                    $rs->fields["ANEX_UBIC"] == "BORRADOR" && $rs->fields["ANEX_ORIGEN"] == 3
                ) {
                    //aqui le actualizo el path SOLO a los radicados asociados, no a los hijos
                    $query = "UPDATE anexos SET anex_nomb_archivo ='/../../../" . $infoasociado->fields["RADI_PATH"] . "' 
WHERE anex_radi_nume=" . $verrad . " 
AND radi_nume_salida=" . $rs->fields["RADI_NUME_SALIDA"] . "
";
                    $update = $db->conn->Execute($query);

                }

                ?>

                <TD height="21" class="listado2"><font size=1> <?= $img_estado ?> </font>
                </TD>
                <TD><font size=1>
                        <?php
                        /*
                         * Ajuste validacion permisos unificados
                         * @author Liliana Gomez Velasquez
                         * @since 17 noviembre 2009
                        */
                        $total_digitos = 11 + $ln;
                        if (strlen($cod_radi) <= $total_digitos) {
                            //Se trata de un Radicado
                            $resulVali = $verLinkArchivo->valPermisoRadi($cod_radi);
                            $valImg = $resulVali['verImg'];
                        } else {
                            //Se trata de un Anexo sin Radicar
                            $resulValiA = $verLinkArchivo->valPermisoAnex($coddocu);
                            $valImg = $resulValiA['verImg'];
                        }


                        if (trim($linkarchivo)) {
                            if ($valImg == "SI") {

                                ///
                                if (isset($infoasociado->fields)) {
                                    $radi_path = $infoasociado->fields["RADI_PATH"];
                                    $principal = pathinfo($radi_path);
                                    $principal_ext = $principal['extension'];
                                    //dd([strtolower($principal_ext),strtolower($rs->fields["EXT"])]);
                                    //if (strtolower($principal_ext) == "pdf" && strtolower($rs->fields["EXT"]) != "pdf") {
                                    //Esta condicion no permitia que la descarga del pdf venga en la version validada
                                    //por lo que se esta dejando en solo que la principal sea PDF
                                    if (strtolower($principal_ext) == "pdf") {?>
                                        <a href="#2"
                                           onclick="funlinkArchivo('<?= $infoasociado->fields["RADI_NUME_RADI"] ?>','<?= $ruta_raiz ?>');">
                                            <?= $cod_radi ?> </a>
                                    <?php } else {
                                        echo "<b><a class=\"vinculos\" href=\"#2\" onclick=\"funlinkArchivo('$coddocu','$ruta_raiz');\"> $cod_radi </a>";
                                    }
                                } else {
                                    echo "<b><a class=\"vinculos\" href=\"#2\" onclick=\"funlinkArchivo('$coddocu','$ruta_raiz');\"> $cod_radi </a>";
                                }


                            } else {
                                echo "<a class='vinculos' href='javascript:noPermiso()' > $cod_radi </a>";
                            }
                        } else {
                            echo trim(strtolower($cod_radi));
                        }
                        $esRadAsociado = false;

                        if ($rs->fields["ANEX_SALIDA"] == 1 && $rs->fields["RADI_NUME_SALIDA"] != NULL &&
                            $rs->fields["RADI_NUME_SALIDA"] != $rs->fields["ANEX_RADI_NUME"] &&
                            $rs->fields["RADI_NUME_SALIDA"] != "null" && $rs->fields["RADI_NUME_SALIDA"] != null &&
                            $rs->fields["ANEX_ESTADO"] >= 2 &&
                            $rs->fields["SGD_DOC_PADRE"] == "1" &&
                            $rs->fields["ANEX_UBIC"] == "BORRADOR" && $rs->fields["ANEX_ORIGEN"] == 3
                        ) {
                            //Radicado Asociado Hijo *
                            ?>

                            &nbsp;<span class="glyphicon glyphicon-paperclip"
                                        style="font-size: 14px; color: black !important;"
                                        title="Radicado asociado como respuesta o alcance"> </span>

                        <?php }

                        if ($rs->fields["ANEX_SALIDA"] == 1 &&
                            $rs->fields["RADI_NUME_SALIDA"] != $rs->fields["ANEX_RADI_NUME"] &&
                            $rs->fields["RADI_NUME_SALIDA"] != "null" && $rs->fields["RADI_NUME_SALIDA"] != null
                            && $rs->fields["ANEX_ESTADO"] == 4 &&
                            ($rs->fields["SGD_DOC_PADRE"] == null || $rs->fields["SGD_DOC_PADRE"] == "null") &&
                            $rs->fields["ANEX_UBIC"] == "BORRADOR" && $rs->fields["ANEX_ORIGEN"] == 3
                        ) {
                            $esRadAsociado = true;
                            //Los demas radicados asociados
                            ?>

                            <span aria-hidden="true" class="icon-loop" style="font-size: 14px "
                                  title="Radicado asociado"></span>

                        <?php }
                        ?>

                    </font>
                </td>
                <TD><font size=1> <?php
                        if (trim($linkarchivo)) {

                            if (isset($infoasociado->fields)) {

                                $radi_path = $infoasociado->fields["RADI_PATH"];
                                $principal = pathinfo($radi_path);
                                $principal_ext = $principal['extension'];

                                $imagen = "./imagenes/PlantillaB.png";

                                if (strtolower($principal_ext) == "pdf" && strtolower($rs->fields["EXT"]) != "pdf") {

                                            if (strtolower($rs->fields["EXT"]) != "docx" && strtolower($rs->fields["EXT"]) != "doc") {
                                                $imagen = "./imagenes/PLANTILLArad.png";
                                            }
                                            if ($estaAnulado > 0){
                                                $imagen = "./iconos/anulacionRad.gif";


                                                ?>
                                                <a href="#2" title="Documento principal anulado"
                                                   onclick="funlinkArchivo('<?= $infoasociado->fields["RADI_NUME_RADI"] ?>','<?= $ruta_raiz ?>');">
                                                    <img src="<?= $imagen ?>"></a>

                                                <?php

                                            }else{

                                            ?>


                                            <a href="#2" title="Documento principal"
                                               onclick="funlinkArchivo('<?= $infoasociado->fields["RADI_NUME_RADI"] ?>','<?= $ruta_raiz ?>');">
                                                <img

                                                        src="./imagenes/PDFrad.png"></a>

                                            <?php

                                            if (count($tieneBorrador) > 0) {

                                                /**
                                                 * cuando es creado a partir de una plantilla docx o cualquier otra plantilla diferente
                                                 * a Editor Web, SI se mestra el icono de [Plantilla]
                                                 */
                                                if (
                                                    (
                                                        $tieneBorrador[0]['TIPO_BORRADOR_DEST'] == $_SESSION['INTERNA_COMUNICACION']
                                                        ||
                                                        $tieneBorrador[0]['TIPO_BORRADOR_DEST'] == $_SESSION['SALIDA_OFICIO']
                                                    )
                                                    && $tieneBorrador[0]['DOCUMENTO_FORMATO'] == 'EditorWeb') {

                                                } else {
                                                    ?>

                                                    <a title="Plantilla o formato" href='#2'
                                                       onclick="funlinkArchivo('<?= $coddocu ?>','<?= $ruta_raiz ?>');">
                                                        <img
                                                                src="<?= $imagen ?>"></a>

                                                <?php }
                                            }} ?>

                                        <?php
                                } else {
                                    if ($estaAnulado > 0) {
                                        $imagen = "./iconos/anulacionRad.gif";
                                        ?>
                                        <a title="Anulado" href='#2' > <img src="<?= $imagen ?>"></a>
                                        <?php
                                    }else {
                                        echo $rs->fields["EXT"];
                                    }
                                }

                            } else {
                                if ($estaAnulado > 0) {
                                    $imagen = "./iconos/anulacionRad.gif";
                                    ?>
                                    <a title="Anulado" href='#2' > <img src="<?= $imagen ?>"></a>
                                    <?php
                                }else {
                                    echo $rs->fields["EXT"];
                                }
                            }

                        } else {
                            echo $msg;
                        }
                        if ($rs->fields["SGD_DIR_TIPO"] == 7) $msg = " - "; else $msg = " - ";
                        ?>
                    </font>


                </td>
                <td width="1%" valign="middle"><font face="Arial, Helvetica, sans-serif" class="etextomenu">
                        <?
                        /*
                        * Indica si el Radicado Ya tiene asociado algun TRD  / Si ya tiene EXPediente
                        */
                        /*	$isql_TRDA = "SELECT *
                                    FROM SGD_RDF_RETDOCF
                                    WHERE RADI_NUME_RADI = '$cod_radi'
                                    ";
                        */
                        $isql_EXP = "SELECT e.radi_nume_radi AS Radicado, count(1) AS EXPEDIENTES 
						FROM sgd_exp_expediente e
						WHERE e.radi_nume_radi='$cod_radi'
						AND e.sgd_exp_estado!=2  --Diferente de excluido.
						GROUP BY e.radi_nume_radi
						ORDER BY 2 DESC";

                        //	$rs_TRA = $db->conn->Execute($isql_TRDA);
                        $rs_EXP = $db->conn->Execute($isql_EXP);
                        //  $radiNumero = $rs_TRA->fields["RADI_NUME_RADI"];
                        $countEXP = $rs_EXP->fields["EXPEDIENTES"];

                        //  if ($radiNumero !='') {
                        if ($countEXP > 0) {
                            $msg_TRD = "S";
                        } else {
                            $msg_TRD = "";
                        }
                        ?>
                        <center>
                            <?
                            echo $msg_TRD;
                            ?>
                        </center>
                    </font>
                </td>

                <td width="1%" valign="middle"><font face="Arial, Helvetica, sans-serif" class="etextomenu">
                        <?php
                        /**
                         *  $perm_radi_sal  Viene del campo PER_RADI_SAL y Establece permiso en la rad. de salida
                         *  1 Radicar documentos,  2 Impresion de Doc's, 3 Radicacion e Impresion.
                         *  (Por. Jh)
                         *  Ademas verifica que el documento no este radicado con $rowwan[9] y [10]
                         *  El jefe con $codusuario=1 siempre podra radicar
                         */
                        //LUPA
                        $aa2 = $rs->fields["DESCR"];
                        if (($rs->fields["EXT"] == "rtf" or $rs->fields["EXT"] == "doc" or $rs->fields["EXT"] == "odt" or $rs->fields["EXT"] == "xml") AND $rs->fields["ANEX_ESTADO"] <= 3) {
                            /*
                                * Incluir manejo de seguridad de imagenes
                            * @author Liliana Gomez Velasquez
                            * @since 10 noviembre 2009
                            */
                            if ($valImg == "SI") {

                                echo "<a class=\"vinculos\" style='cursor:pointer;cursor:hand;' onclick=\"vistaPreliminar('$coddocu','$linkarchivo','$linkarchivotmp');\">";

                            } else {

                                echo "<a class='vinculos' style='cursor:pointer;cursor:hand;' href='javascript:noPermiso()' >";
                            }
                            ?>
                            <!--img src="<?= $ruta_raiz ?>/iconos/vista_preliminar.gif" alt="Vista Preliminar" border="0">
	<font face="Arial, Helvetica, sans-serif" class="etextomenu"-->
                            <?
                            echo "</a>";
                            $radicado = "false";
                            $anexo = $cod_radi;
                        }
                        ?>
                    </font>
                </TD>
                <!-- -------------------------------------------------------------------------------------------------------->
                <!-- lo siguiente esla descripcion y lo que sigue es la fecha, solo mostrara como enlace, a los radicados asociados, no a los hijos -->
                <td><font size=1> <?php
                        if ($rs->fields["ANEX_SALIDA"] == 1 &&
                            $rs->fields["RADI_NUME_SALIDA"] != $rs->fields["ANEX_RADI_NUME"] &&
                            $rs->fields["RADI_NUME_SALIDA"] != "null" && $rs->fields["RADI_NUME_SALIDA"] != null &&
                            $rs->fields["ANEX_ESTADO"] == 4 &&
                            ($rs->fields["SGD_DOC_PADRE"] == null || $rs->fields["SGD_DOC_PADRE"] == "null" || $rs->fields["SGD_DOC_PADRE"] == "NULL") &&
                            $rs->fields["ANEX_UBIC"] == "BORRADOR" && $rs->fields["ANEX_ORIGEN"] == 3
                        ) {

                            if ($valImg == "SI") {
                                echo "<b><a class=\"vinculos\" target='_blank' href='./verradicado.php?verrad=" . $cod_radi . "&session_name()=session_id()&krd=" . trim($krd) . "'  >" . $rs->fields["DESCR"] . " </a>";
                            } else {
                                echo "<a class='vinculos' href='javascript:noPermiso()' >" . $rs->fields["DESCR"] . "</a>";
                            }
                        } else {
                            echo $rs->fields["DESCR"];
                        }

                        ?> </font>
                </td>
                <td><font size=1>

                        <?php
                        //solo mostrara como enlace a los radicados asociados, no a los hijos
                        if ($rs->fields["ANEX_SALIDA"] == 1 &&
                            $rs->fields["RADI_NUME_SALIDA"] != $rs->fields["ANEX_RADI_NUME"] &&
                            $rs->fields["RADI_NUME_SALIDA"] != "null" && $rs->fields["RADI_NUME_SALIDA"] != null &&
                            $rs->fields["ANEX_ESTADO"] == 4 &&
                            ($rs->fields["SGD_DOC_PADRE"] == null || $rs->fields["SGD_DOC_PADRE"] == "null" || $rs->fields["SGD_DOC_PADRE"] == "NULL") &&
                            $rs->fields["ANEX_UBIC"] == "BORRADOR" && $rs->fields["ANEX_ORIGEN"] == 3
                        ) {

                            if ($valImg == "SI") {
                                echo "<b><a class=\"vinculos\" target='_blank' href='./verradicado.php?verrad=" . $cod_radi . "&session_name()=session_id()&krd=" . trim($krd) . "' >" . $rs->fields["FEANEX"] . " </a>";
                            } else {
                                echo "<a class='vinculos' href='javascript:noPermiso()' >" . $rs->fields["FEANEX"] . "</a>";
                            }
                        } else {
                            echo $rs->fields["FEANEX"];
                        }

                        ?> </font>
                </td>
                <td><font size=1> <?= $rs->fields["CREA"] ?> </font></td>
                <td><font size=1> <?= substr($rs->fields["DESTINO"], 0, 32) ?> </font></td>
                <td><font size=1> <?= $rs->fields["TAMA"] ?> </font></td>
                <?php


                $query_document_met = array();
                if ($rs->fields['RADI_NUME_SALIDA'] != NULL && $rs->fields['RADI_NUME_SALIDA'] != '') {
                    $query_document_met = $helperRadi->getDocumentMetadata($rs->fields['RADI_NUME_SALIDA']);
                   
                }

                $pathinfo = pathinfo($rs->fields['NOMBRE']);


                $radicados_sin_regenerar = array();
                $texto = false;
                //con las dos siguientes condiciones, valido que no ingresen los radicados asociados,
                //ya que a esos no se realiza validacion
                if (($rs->fields["ANEX_ESTADO"] == 3 or $rs->fields["ANEX_ESTADO"] == 4)
                    and count($query_document_met) > 0) {
                    $texto = $conjugacionValidar[5];
                } else if ($rs->fields["ANEX_ESTADO"] == 2
                    OR ($rs->fields["ANEX_ESTADO"] == 3 AND count($query_document_met) < 1)) {
                    $texto = $conjugacionValidar[0];
                }

                if ($rs->fields['ANEX_REGENERADO'] < 1 && strtolower($rs->fields["EXT"]) != 'pdf') {
                    if ($texto != false) {
                        $radicados_sin_regenerar[] = $rs->fields['RADI_NUME_SALIDA'];
                    }
                }
                $valpermitir =1;
                if ($estaAnulado > 0 && $_SESSION["usua_admin_sistema"] != 1){
                    $valpermitir = 0;
                }

                if ($rs->fields['RADI_NUME_SALIDA'] != null && $_SESSION["FUNCTION_VALIDAR_RADICADO"] == 'SI' && $valpermitir == 1) {

                    //lo siguiente es para validar que el tipo de radicado que estoy recorriendo, este permitido
                    //en la variable de configuracion TIPOS_RADICADO_VALIDAR, que debe llegar como un string
                    //por lo tanto lo convierto a array, y lo recorro para comparar los permitidos, con el ultimo numero
                    //del radicado.
                    $permitetiporad = false;

                    if ($_SESSION['TIPOS_RADICADO_VALIDAR'] != '' && json_decode($_SESSION['TIPOS_RADICADO_VALIDAR']) != NULL) {
                        foreach (json_decode($_SESSION['TIPOS_RADICADO_VALIDAR']) as $tipo_val) {

                            if ($tipo_val == substr($rs->fields['RADI_NUME_SALIDA'],
                                    strlen($rs->fields['RADI_NUME_SALIDA']) - 1,
                                    strlen($rs->fields['RADI_NUME_SALIDA']))) {
                                $permitetiporad = true;
                                break;
                            }
                        }
                    }


                    $primervalidar = true;
                    $btnregenerar = false;

                    $texto = false;


                    //con las dos siguientes condiciones, valido que no ingresen los radicados asociados,
                    //ya que a esos no se realiza validacion
                    if (
                        ($rs->fields["ANEX_ESTADO"] == 3 or $rs->fields["ANEX_ESTADO"] == 4)
                        and count($query_document_met) > 0) {
                        $texto = $conjugacionValidar[5];
                    } else if (
                        $rs->fields["ANEX_ESTADO"] == 2
                        OR
                        (
                            $rs->fields["ANEX_ESTADO"] == 3 AND count($query_document_met) < 1
                        )
                    ) {
                        $texto = $conjugacionValidar[5];
                    }

                    $botonColor = 'blue';
                    if ($esRadAsociado){
                        $texto = $conjugacionValidar[5];
                        $botonColor = 'btn-default';
                    }


                    if ($permitetiporad == true) {


                        $permitetiext = false;

                        if ($_SESSION['VALIDAR_EXTENSIONES'] != '' && json_decode($_SESSION['VALIDAR_EXTENSIONES']) != NULL) {
                            foreach (json_decode($_SESSION['VALIDAR_EXTENSIONES']) as $tipo_ext) {

                                if (strtolower($tipo_ext) == strtolower($rs->fields["EXT"])) {
                                    $permitetiext = true;
                                    break;
                                }
                            }
                        }


                        if ($permitetiext == true) {

                            //ya aqui valido si el radicado fue creado desde borrador.
                            if (count($tieneBorrador) > 0) {
                                $asuntosRadicados[$rs->fields['RADI_NUME_SALIDA']] = '';
                                $asuntosRadicados[$rs->fields['RADI_NUME_SALIDA']] = $infoasociado->fields["RA_ASUN"];

                                ?>
                                <td>
                                    <button type="button" title="<?= $conjugacionValidar[0] ?>" id="div_enviar"
                                            style="border-top-right-radius:0px !important;
                                                    border-top-left-radius:20px !important;
                                                       border-bottom-left-radius:20px !important;"
                                            onclick="doc_clickvalidarraiz(<?= $rs->fields['RADI_NUME_SALIDA'] ?>,'<?= $ANEXO_ID ?>')"
                                            class="btn btn-sm <?php echo $botonColor; ?>">
                                        Ver <?= $texto ?> <i class="fa fa-edit"></i>

                                    </button>
                                </td>

                            <?php } else if(count($query_document_met) > 0 && $query_document_met[0]['STATUS_DOCUMENT'] == 'CERRADO'){
                                $asuntosRadicados[$rs->fields['RADI_NUME_SALIDA']] = '';
                                $asuntosRadicados[$rs->fields['RADI_NUME_SALIDA']] = $infoasociado->fields["RA_ASUN"];

                                ?>
                                <td>
                                    <button type="button" title="<?= $conjugacionValidar[0] ?>" id="div_enviar"
                                            style="border-top-right-radius:0px !important;
                                                    border-top-left-radius:20px !important;
                                                       border-bottom-left-radius:20px !important;"
                                            onclick="doc_clickvalidarraiz(<?= $rs->fields['RADI_NUME_SALIDA'] ?>,'<?= $ANEXO_ID ?>')"
                                            class="btn btn-sm <?php echo $botonColor; ?>">
                                        Ver <?= $texto ?> <i class="fa fa-edit"></i>

                                    </button>
                                </td>
                            <?php } else {  ?>
                                <td></td>
                            <?php }

                        } else { ?>
                            <td>
                                <button type="button" title="<?= $conjugacionValidar[0] ?>" id="div_enviar"
                                        style="border-top-right-radius:0px !important;
                                                    border-top-left-radius:20px !important;
                                                       border-bottom-left-radius:20px !important;"
                                        onclick="extNoPermit()"
                                        class="btn btn-sm btn-default">
                                    <?= $texto ?> <i class="fa fa-edit"></i>

                                </button>
                            </td>
                        <?php }

                    } else { ?>
                        <td>
                            <button type="button" title="Validar" id="div_enviar"
                                    style="border-top-right-radius:0px !important;
                                                    border-top-left-radius:20px !important;
                                                       border-bottom-left-radius:20px !important;"
                                    onclick="tipoRadNoPermit()"
                                    class="btn btn-sm btn-default">
                                <?= $texto ?> <i class="fa fa-edit"></i>

                            </button>
                        </td>
                    <?php }


                } else if ($_SESSION["FUNCTION_VALIDAR_RADICADO"] == 'SI') { ?>
                    <td></td>
                <?php } ?>
                <td><font size=1> <?= $rs->fields["RO"] ?> </font></td>
                <td><font size=1>
                        <?
                        if ($rs->fields["SGD_PNUFE_CODI"] && strcmp($cod_radi, $rs->fields["SGD_DOC_PADRE"]) == 0 && strlen($rs->fields["SGD_DOC_SECUENCIA"]) > 0) {
                            $anex->anexoRadicado($verrad, $rs->fields["DOCU"]);
                            echo($anex->get_doc_secuencia_formato($dependencia) . "<BR>" . $rs->fields["FECDOC"]);
                        }
                        ?>
                    </font>
                </td>

                <!-- -------------------------------------------------------------------------------------------------------->
                <!-- esto aparecio? -->    
                <td><font size=1>
                        <?php
                        
                        $anexTipo = $rs->fields["ANEX_TIPO"];
                        $anex_depe_creador = $rs->fields["ANEX_DEPE_CREADOR"];
                        /*
                        echo "verradPermisos: ".$verradPermisos;
                        echo "<br> perm_borrar_anexo: ".$perm_borrar_anexo;
                        echo "<br> linkarchivo: ".$linkarchivo;
                        echo "<br> anexTipo: ".$anexTipo;
                        echo "<br> origen: ".$origen;
                        */
                        //origen=1 cuando Anexo Web, =0 orfeoscan
                        //	if(  $origen!=1 and $linkarchivo  and $verradPermisos == "Full" )

                        $isAnexo = 0;
                        if (($origen != 0 and $linkarchivo and $verradPermisos == "Full") or ($origen != 1 and $linkarchivo and ($anexTipo == 4 or $anexTipo == 7) and $perm_borrar_anexo == 1)) {


                            if ($anex_estado < 4) {

                                if (
                                    ($rs->fields['RADI_NUME_SALIDA'] == 'NULL'
                                        || $rs->fields['RADI_NUME_SALIDA'] == NULL) and $_SESSION["FUNCTION_VALIDAR_RADICADO"] == 'SI') {
                                    $isAnexo = 1;
                                }



                                if (
                                        (count($query_document_met) < 1)
                                        or ($tieneTransacDevolucion>0)
                                        or (
                                        count($query_document_met) > 0 && isset($query_document_met[0])
                                        && $query_document_met[0]['STATUS_DOCUMENT'] == 'ABIERTO'
                                        )

                                ) {
                                    $radinumesalida = ($rs->fields['RADI_NUME_SALIDA'] == 'NULL' || $rs->fields['RADI_NUME_SALIDA'] == NULL) ? "'NULL'" : $rs->fields['RADI_NUME_SALIDA'];
                                    echo "<a class=vinculos href=javascript:verDetalles('$coddocu','$tpradic','$aplinteg',$ANEXO_ID,$radinumesalida,$isAnexo)> &nbspModificar&nbsp </a> ";

                                }
                            }
                        }
                        ?>
                    </font>
                </td>
                <?php
                //Estas variables se utilizan para verificar si se debe mostrar la opci�n de tipificaci�n de anexo .TIF

                $anexTPRActual = $rs->fields["SGD_TPR_CODIGO"];

                /* FULL */

                $is_principal = 'false';

                if (
                    $rs->fields["ANEX_ESTADO"] > 2 &&
                    $rs->fields["RADI_NUME_SALIDA"] == $rs->fields["ANEX_RADI_NUME"] &&
                    $rs->fields["ANEX_SALIDA"] == 1 &&
                    ($rs->fields["SGD_DOC_PADRE"] == 0 || $rs->fields["SGD_DOC_PADRE"] == null
                        || $rs->fields["SGD_DOC_PADRE"] == 'NULL') &&
                    $rs->fields["SGD_TRAD_CODIGO"] != null && $rs->fields["SGD_TRAD_CODIGO"] != 'NULL' &&
                    $rs->fields["ANEX_BORRADO"] == 'N' &&
                    $rs->fields["ANEX_ORIGEN"] == 3 &&
                    $rs->fields["ANEX_UBIC"] == 'BORRADOR'
                ) {
                    $is_principal = 'true';
                }

                     
                if ($verradPermisos == "Full") {
                    ?>
                    <td><font size=1>
                            <?php
                            $radiNumeAnexo = $rs->fields["RADI_NUME_SALIDA"];
                            if ((count($query_document_met) < 1) or (
                                    count($query_document_met) > 0 && isset($query_document_met[0])
                                    && $query_document_met[0]['STATUS_DOCUMENT'] == 'ABIERTO')) {
                                if ($radiNumeAnexo > 0 and trim($linkarchivo)) {
                                    if (!$codserie) $codserie = "0";
                                    if (!$tsub) $tsub = "0";
                                    echo "<a class=vinculos href=javascript:ver_tipodocuATRD($radiNumeAnexo,$codserie,$tsub);>Tipificar</a> ";
                                } elseif ($perm_tipif_anexo == 1 && ($anexTipo == 4 or $anexTipo == 7) && $anexTPRActual == '') { //Es un anexo de tipo tif (4) y el usuario tiene permiso para Tipificar, adem�s el anexo no ha sido tipificado
                                    if (!$codserie) $codserie = "0";
                                    if (!$tsub) $tsub = "0";
                                    echo "<a class=vinculoTipifAnex href=javascript:ver_tipodocuAnex('$cod_radi','$anexo',$codserie,$tsub);> &nbspTipificar&nbsp </a> ";
                                } elseif ($perm_tipif_anexo == 1 && ($anexTipo == 4 or $anexTipo == 7) && $anexTPRActual != '') { //Es un anexo de tipo tif (4) y el usuario tiene permiso para Tipificar, adem�s el anexo YA ha sido tipificado antes
                                    if (!$codserie) $codserie = "0";
                                    if (!$tsub) $tsub = "0";
                                    echo "<a class=vinculoTipifAnex href=javascript:ver_tipodocuAnex('$cod_radi','$anexo',$codserie,$tsub);> &nbspRe-Tipificar&nbsp </a> ";
                                }
                            }
                            ?>
                        </font>
                    </td>
                    <td><font size=1>
                            <?php

                            if ($rs->fields["RADI_NUME_SALIDA"] == 0 and $ruta_raiz != ".."
                                and trim($rs->fields["ANEX_CREADOR"]) == trim($krd) and ($origen != 0)) {

                                //$origen!=0 --> Anexo web, desde pestnia documentos (No orfeoscan)
                                if ($linkarchivo) {    /*

		         * Incluir manejo de seguridad de imagenes
			 * @author Liliana Gomez Velasquez
			 * @since 10 noviembre 2009
			 */

                                    $v = $rs->fields["SGD_PNUFE_CODI"];
                                    echo "<a class=\"vinculos\" href=\"#\" onclick=\"borrarArchivo('$coddocu','$linkarchivo','$cod_radi','$v',$is_principal);\"> &nbspBorrar&nbsp </a>";
                                }
                            }
                            ?>
                        </font>
                    </td>
                    <td><font size=1>
                            <?php
  
                            /**
                             *  $perm_radi_sal  Viene del campo PER_RADI_SAL y Establece permiso en la rad. de salida
                             *  1 Radicar documentos,  2 Impresion de Doc's, 3 Radicacion e Impresion.
                             *  (Por. Jh)
                             *  Ademas verifica que el documento no este radicado con $rowwan[9] y [10]
                             *  El jefe con $codusuario=1 siempre podra radicar
                             */

                            if ($tpPerRad[$tpradic] == 2 or $tpPerRad[$tpradic] == 3) {
                                if (!$rs->fields["RADI_NUME_SALIDA"]) {
                                    if (substr($verrad, -1) == 2 && $puedeRadicarAnexo == 1) {
                                        $rs->fields["SGD_PNUFE_CODI"] = 0;
                                        /* Incluir manejo de seguridad de imagenes
                                          * para que el link no muestre la ruta completa
                                          * @author Liliana Gomez Velasquez
                                          * @since 10 noviembre 2009
                                        */
                                        echo "<a class=\"vinculos\" href=\"#\" onclick=\"radicarArchivo('$coddocu','$linkarchivo','si'," . $rs->fields["SGD_PNUFE_CODI"] . ",'$tpradic','$aplinteg','$numextdoc',$ANEXO_ID);\"> &nbspRadicar(-$tpradic)&nbsp </a>";
                                        $radicado = "false";
                                        $anexo = $cod_radi;
                                    } else
                                        if ($puedeRadicarAnexo != 1) {
                                            $objError = new AplExternaError();
                                            $objError->setMessage($puedeRadicarAnexo);
                                            echo($objError->getMessage());
                                        } else {
                                            if ((substr($verrad, -1) != 2) and $num_archivos == 1 and !$rs->fields["SGD_PNUFE_CODI"] and $swRadDesdeAnex == false) {
                                                if ($anexTipo == 14 or $anexTipo == 15 or $anexTipo == 16 or $anexTipo == 18 or $anexTipo == 32 or $anexTipo == 7) {
                                                    /*
                                                       * Incluir manejo de seguridad de imagenes
                                                       * para que el link no muestre la ruta completa
                                                       * @author Liliana Gomez Velasquez
                                                       * @since 10 noviembre 2009
                                                       */
                                                    echo "<a class=\"vinculos\" href=\"#\" onclick=\"asignarRadicado('$coddocu','$linkarchivo','$cod_radi','$numextdoc',$ANEXO_ID);\"> &nbspAsignar Rad&nbsp </a>";
                                                    $radicado = "false";
                                                    $anexo = $cod_radi;

                                                } else {
                                                    echo "<a class='titulosError3'> Formato Plantilla incorrecto </a>";
                                                }

                                            } else if ($rs->fields["SGD_PNUFE_CODI"] && strcmp($cod_radi, $rs->fields["SGD_DOC_PADRE"]) == 0 && !$anex->seHaRadicadoUnPaquete($rs->fields["SGD_DOC_PADRE"])) {    /*
	   * Incluir manejo de seguridad de imagenes
	  * para que el link no muestre la ruta completa
	  * @author Liliana Gomez Velasquez
	  * @since 10 noviembre 2009
	  */

                                                echo "<a class=\"vinculos\" href=\"#\" onclick=\"radicarArchivo('$coddocu','$linkarchivo','si'," . $rs->fields["SGD_PNUFE_CODI"] . ",'$tpradic','$aplinteg','$numextdoc',$ANEXO_ID);\"> &nbspRadicar(-$tpradic) </a>";

                                                $radicado = "false";
                                                $anexo = $cod_radi;
                                            } else if ($puedeRadicarAnexo == 1) {
                                                $rs->fields["SGD_PNUFE_CODI"] = 0;
                                                /*
                                            * Incluir manejo de seguridad de imagenes
                                           * para que el link no muestre la ruta completa
                                           * @author Liliana Gomez Velasquez
                                           * @since 10 noviembre 2009
                                           */
                                                //ojo diff este radicar.
                                                echo "<a class=\"vinculos\" href=\"#\" onclick=\"radicarArchivo('$coddocu','$linkarchivo','si'," . $rs->fields["SGD_PNUFE_CODI"] . ",'$tpradic','$aplinteg','$numextdoc',$ANEXO_ID);\"> </a>";
                                                /* ||Radicar(-$tpradic) */
                                                $radicado = "false";
                                                $anexo = $cod_radi;
                                            }
                                        }
                                } else {

                                    if (!$rs->fields["SGD_PNUFE_CODI"]) $rs->fields["SGD_PNUFE_CODI"] = 0;
                                    if ($anex_estado < 4) {    /*
			          * Incluir manejo de seguridad de imagenes
			          * para que el link no muestre la ruta completa
			          * @author Liliana Gomez Velasquez
			          * @since 10 noviembre 2009
			          */



                                        if (
                                            (count($query_document_met) < 1)
                                            or ($tieneTransacDevolucion>0)
                                            or (
                                                count($query_document_met) > 0 && isset($query_document_met[0])
                                                && $query_document_met[0]['STATUS_DOCUMENT'] == 'ABIERTO')
                                        ) {
                                            //si tiene nada en document_metadata
                                            // o si tiene algo en document_metadata y se encuentre ABIERTO}, ya que puede estar CERRADO
                                            echo "<a class=vinculos href=javascript:radicarArchivo('$coddocu','$linkarchivo','$cod_radi'," . $rs->fields["SGD_PNUFE_CODI"] . ",'','',$numextdoc,$ANEXO_ID)> &nbspRe-Generar&nbsp </a>";
                                            $radicado = "true";
                                        }
                                    }
                                }
                            } else if ($rs->fields["SGD_PNUFE_CODI"] && ($usua_perm_numera_res == 1) && $ruta_raiz != ".." && !$rs->fields["SGD_DOC_SECUENCIA"] && strcmp($cod_radi, $rs->fields["SGD_DOC_PADRE"]) == 0) // SI ES PAQUETE DE DOCUMENTOS Y EL USUARIO TIENE PERMISOS
                            {    /*
			 * Incluir manejo de seguridad de imagenes
			 * para que el link no muestre la ruta completa
			 * @author Liliana Gomez Velasquez
			 * @since 10 noviembre 2009
			*/

                                echo "<a class=\"vinculos\" href=\"#\" onclick=\"numerarArchivo('$codocu','$linkarchivo','si'," . $rs->fields["SGD_PNUFE_CODI"] . ");\"> Numerar </a>";
                            }
                            if ($rs->fields["RADI_NUME_SALIDA"]) {
                                $radicado = "true";
                            }
                            
                            ?>
                        </font>
                    </td>

                    <?
                } // fin FULL
                else {
                    ?>
                    <td>
                        <font size=1>
                            <?php
                            //$origen!=1  ---> Orfeoscan
                            if ($origen != 1 and $linkarchivo and $perm_borrar_anexo == 1 && ($anexTipo == 4 or $anexTipo == 7)) {
                                /*
                                   * Incluir manejo de seguridad de imagenes
                                   * @author Liliana Gomez Velasquez
                                   * @since 10 noviembre 2009
                                  */

                                $v = $rs->fields["SGD_PNUFE_CODI"];

                                echo "<a class=\"vinculoTipifAnex\" href=\"#\" onclick=\"borrarArchivo('$coddocu','$linkarchivo','$cod_radi','$v',$is_principal);\"> &nbspBorrar&nbsp </a>";
                                echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                            }
                            if ($perm_tipif_anexo == 1 && ($anexTipo == 4 or $anexTipo == 7) && $anexTPRActual == '') { //Es un anexo de tipo tif (4) y el usuario tiene permiso para Tipificar, adem�s el anexo no ha sido tipificado
                                if (!$codserie) $codserie = "0";
                                if (!$tsub) $tsub = "0";
                                echo "<a class=vinculoTipifAnex href=javascript:ver_tipodocuAnex('$cod_radi','$anexo',$codserie,$tsub);> &nbspTipificar&nbsp </a> ";
                            } elseif ($perm_tipif_anexo == 1 && ($anexTipo == 4 or $anexTipo == 7) && $anexTPRActual != '') { //Es un anexo de tipo tif (4) y el usuario tiene permiso para Tipificar, ademas el anexo YA ha sido tipificado antes
                                if (!$codserie) $codserie = "0";
                                if (!$tsub) $tsub = "0";
                                echo "<a class=vinculoTipifAnex href=javascript:ver_tipodocuAnex('$cod_radi','$anexo',$codserie,$tsub);> &nbspRe-Tipificar&nbsp </a> ";
                            }


                            ?>
                        </font>
                    </td>

                    <?php

                }
                ?>

            </tr>
            <?php
            $rs->MoveNext();
        }
    }

    /*
    $mostrar_lista = 0;
    if($mostrar_lista==1)
    {
    ?>
    </TABLE>
    <?
    }*/
    ?>

</table>
<?php

if ($verradPermisos == "Full" || $_SESSION["perm_radi"] >= 1) {
    ?>
    <br>
    <table width="100%" align="center" border="0" cellpadding="0" cellspacing="5" class="borde_tab">
        <tr align="center">
            <td>
                <a class="vinculos"
                   href='javascript:nuevoArchivo(<? if ($num_archivos == 0 && $swRadDesdeAnex == false) echo "1"; else echo "0"; ?>)'
                   class="timpar">
                    Anexar Archivo</a>
 
            </td>
            <script>
                swradics =<?=$num_archivos?>;
            </script>
            <?
            /* Anexar plantillas, keda por ahora aplazado el proyecto
            <td class="celdaGris"> <a href='javascript:Plantillas(0)' class="timparr">Anexar
              Plantilla ...</a>
              <!-- <a href='plantilla.php?<?=SID ?>'>Anexar Plantilla ... </a> </td>-->
            </TD>

            <td class="celdaGris"> <a href='javascript:Plantillas_pb(0)' class="timparr">A</a>
              <!-- <a href='plantilla.php?<?=SID ?>'>Anexar Plantilla ... </a> </td>-->
            </TD>
            */

            ?>
        </tr>

    </table>

    <?php
}
?>

<div id="modal_reasignar" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog "  style="width:60% !important;">
        <div class="modal-content">
            <div class="modal-header bg-blue-chambray bg-font-yellow text-center">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><strong>REASIGNAR</strong></h4>
            </div>
            <div class="modal-body" id="radicated_info">
                <form action="" method="POST" class="form-horizonal" id="">
                    <div class="form-body modal-body todo-task-modal-body">
                        <div class="form-actions">
                            <div class="row"></div>
                        </div>
                        <div class="row" style="margin-top:40px">
                            <div class="col-md-12 ">
                                <div class="form-group has-error">
                                    <label class=" col-md-2">Reasignar a: </label>
                                    <div class="col-md-10">
                                        <div class="input-icon right">

                                            <select class="js-data-example-ajax" id="enviara" name="">

                                            </select>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 " style="margin-top:10px">
                                <div class="form-group has-error">
                                    <label class=" col-md-2">Comentario: </label>
                                    <div class="col-md-10">
                                        <div class="input-icon right">
                                                <textarea required="required" name="comentarioenviar"
                                                          id="comentarioenviar" maxlength="999" class="form-control"
                                                          style="border: solid grey 1px; height:90px"
                                                          placeholder=""></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>


                </form>
            </div>
            <div class="modal-footer">
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-4">
                            <button type="button" class="btn btn-lg blue"
                                    id="guardarEnviar" onclick="ReasignarRadicado.clickModalReasignar(event)"
                                    style="border-radius:20px !important">
                                <i class="fa fa-arrow-right"></i>
                                Reasignar
                            </button>

                        </div>

                        <div class="col-md-4">
                            <button type="button" class="btn btn-lg red" data-dismiss="modal"
                                    style="border-radius:20px !important">
                                Cancelar
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modCargosFirmValid" class="modal fade" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content modal-lg">
            <div class="modal-header bg-blue-chambray bg-font-yellow text-center">
                <button type="button" class="close"
                        onclick="cerrarModalFirmantes('VALIDAR','modCargosFirmValid');"></button>
                <h4 class="modal-title"><strong>Escriba el cargo y seleccione el firmante principal</strong></h4>
            </div>
            <div class="modal-body">

                <br>

                <table role="presentation" class="table table-bordered table-striped" border="1">
                    <thead>
                    <th>Nombre</th>
                    <th>Cargo</th>
                    <th>Remitente Principal</th>
                    <th>Firmantes</th>
                    </thead>
                    <tbody id="tbodyCargFirmValid"></tbody>
                </table>

            </div>
            <div class="modal-footer">
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-6">
                            <button type="button" class="btn btn-lg red"
                                    style="border-radius:20px !important"
                                    onclick="cerrarModalFirmantes('VALIDAR','modCargosFirmValid');">
                                Guardar y cerrar
                            </button>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modalnoestaincludexped" class="modal fade" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content modal-lg">
            <div class="modal-header bg-blue-chambray bg-font-yellow text-center">
                <button type="button" class="close"></button>
                <h4 class="modal-title"><strong>Radicado no incluido en expediente</strong></h4>
            </div>
            <div class="modal-body">
                <div class="col-md-12 " style="float: left" id="">
                    <div class="note note-info">
                        <h4 class="block">Aprobación!</h4>
                        <p><?= $_SESSION["NOTA_VALIDAR"] ?></p></div>
                </div>
                <div class="col-md-12 " style="float: left" id="">
                    <div class="note note-warning">
                        <h4 class="block">Advertencia!</h4>
                        <p id="padvertradnoincluexped"></p></div>
                </div>
                <div class="col-md-12 " style="float: left" id="">
                    <div class="note note-success">
                        <h4 class="block">Importante!</h4>
                        <p><?= $notav ?></p></div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-6">
                            <button type="button" class="btn btn-lg red" data-dismiss="modal"
                                    style="border-radius:20px !important">
                                cerrar
                            </button>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modal_vista_previa_validar" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-blue-chambray bg-font-yellow text-center" id="header_vista_previa_validar">

                <button type="button" title="Cerrar" aria-hidden="true" class="btn btn-lg " data-dismiss="modal"
                        style="float: right;background-color: #2C3E50!important; border-radius:20px !important"
                >
                    <i class="fa fa-remove"></i></button>
                <h4 class="modal-title"><strong>Vista Previa</strong></h4>
            </div>
            <div class="modal-body" id="modal_body_vista_previa_validar">

                <embed src="" id="embed_validar"
                       frameborder="0" style="width:100%; height:100%">
            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-lg btn-success" id="btn_descargar_pdf"
                        style="border-radius:20px !important">
                    Descargar
                </button>

                <button type="button" class="btn btn-lg" data-dismiss="modal"
                        style="border-radius:20px !important">
                    Cerrar
                </button>
            </div>
        </div>
    </div>
</div>

<div id="modal_aprobar_documento" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog " style="width:63%;">
        <div class="modal-content">

            <div class="modal-header bg-blue-chambray bg-font-yellow text-center">

                <button type="button" title="Cerrar" aria-hidden="true"
                        class="btn btn-lg "
                        style="float: right;background-color: #2C3E50!important; border-radius:20px !important"
                        onclick="cerrarmodalvalidar()">
                    <i class="fa fa-remove"></i></button>

                <h4 class="modal-title"><strong
                            id="titulomodal"><?= strtoupper($conjugacionValidar[10]) ?> DOCUMENTO</strong>
                </h4>
            </div>
            <div class="modal-body" id="modalbodyaprobar_documento">


                <div id="div_apend_aprob_doc">

                </div>

                <div class="progress progress-striped active" id="myProgress" style="display: none">
                    <div class="progress-bar progress-bar-success" role="progressbar" id="myBar"
                         aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 1%">
                        <span class="sr-only" id="label"> </span>
                    </div>
                </div>
                <div class="row">
                <div class="col-md-12 " style="float: left">
                    <div class="note note-info" style="padding: 2px 30px 15px 15px !important;">
                        <h4 class="block"><?= $conjugacionValidar[10] ?>!</h4>
                        <p><?= $_SESSION["NOTA_VALIDAR"] ?> </p>
                    </div>

                </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div id="modal_modAsunto" class="modal fade" tabindex="-1" aria-hidden="true" style="display: none;">
    <div class="modal-dialog " style="width:65%;">
        <div class="modal-content">
            <div class="modal-header bg-blue-chambray bg-font-yellow text-center">
                <button type="button" title="Cerrar"
                        class="btn btn-lg " onclick="cerrarModalmodAsunto()"
                        style="float: right;background-color: #2C3E50!important; border-radius:20px !important">
                    <i class="fa fa-remove"></i></button>
                <h4 class="modal-title"><strong id="titulomodalnuevoexped">Modificar Asunto de Radicado</strong></h4>

            </div>
            <div class="modal-body" id="radicated_info">

                <div id="btnAdvWar" style="display:none;" class="alert alert-warning alert-dismissable">

                    <strong>Advertencia!</strong>
                    <br> El asunto solo se debería modificar cuando el documento se encuentra en BORRADOR.
                    <br> Los cambios se guardarán en el historial del radicado. ¿Está seguro?
                    <div class="row">
                        <div class="col-md-12 ">
                            <span class="col-md-3"><button type="button"  class="btn btn-danger">Si, Continuar</button></span><span class="col-md-3"><button onclick="cerrarModalmodAsunto()" type="button" class="btn btn-info">Cancelar</button></span><span class="col-md-6"></span>
                        </div>
                    </div>
                </div>

                <div id="btnAdvDanger" class="alert alert-danger" style="display: none;">

                </div>

                <form action="" method="POST" class="form-horizonal" id="">
                    <div class="form-body modal-body todo-task-modal-body">

                        <div class="row" style="margin-top:40px">
                            <div class="col-md-12 ">
                                <div class="form-group">



                                    <label class=" col-md-12" style="display: none;" id="AsuntoActualLab">AsuntoActual :</label>
                                    <div class="col-md-12">
                                        <div class="input-icon right" style="position: relative;">
                                            <textarea  required="required" name="asuntoActual" id="AsuntoActual" class="form-control" maxlength="999" style="display: none;border: solid grey 1px; height:90px" placeholder=""></textarea>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div><br>

                                    <label style="display: none;" class=" col-md-12" id="addAsunto">Agregar al asunto:</label>
                                    <div class="col-md-12">
                                        <div class="input-icon right" style="position: relative;">
                                            <textarea required="required" name="asuntoAdd" id="asuntoAdd" class="form-control" maxlength="999" style="display: none;border: solid grey 1px; height:90px" placeholder=""></textarea>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div><br>

                                    <label style="display: none;" class=" col-md-12" id="newAsunto">Nuevo Asunto:</label>
                                    <div class="col-md-12">
                                        <div id="nuevoAs" style="display: none;" class="alert alert-info">

                                        </div>
                                    </div> <br>


                                </div>
                            </div>

                        </div>
                    </div>


                </form>
            </div>
            <div class="modal-footer">
                <div class="form-actions">
                    <div class="row">

                        <div class="col-md-6">

                        </div>
                        <div class="col-md-4" id="divBtnAccionCrearExped">
                            <button  type="button" class="btn btn-lg blue" id="btnModAsunto"
                                     style="display:none;border-radius:20px !important" onclick="modAsunto(event)">
                                Guardar Cambios
                            </button>
                        </div>
                        <div class="col-md-2">
                            <button type="button" class="btn btn-lg" onclick="cerrarModalmodAsunto()"
                                    style="border-radius:20px !important">
                                Cerrar
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modal_historico_comprobacion" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="min-width: 58em;">
        <div class="modal-content" style="">
            <div class="modal-header bg-blue-chambray bg-font-yellow text-center">
                <button type="button" data-dismiss="modal" title="Cerrar" aria-hidden="true"
                        class="btn btn-lg "
                        style="float: right;background-color: #2C3E50!important; border-radius:20px !important">
                    <i class="fa fa-remove"></i></button>
                <h4 class="modal-title"><strong>Historico Completo de Comprobación</strong></h4>
            </div>

            <div class="modal-body" id="body_historico_com">


                <div style="margin: 1em;" class="table-responsive  " id="open_table_historico"
                     style="position: relative;">
                    <table  class="table table-striped table-bordered table-hover
           table-checkable order-column dataTable no-footer" id="tablahistorico">
                        <thead class="bg-blue font-white bold">
                        <tr  role="row">



                            <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1"
                                colspan="1"
                                aria-label=" Fecha : activate to sort column ascending"
                                style=" text-align:center"> Fecha comprobacion
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="sample_2" rowspan="1"
                                colspan="1"
                                aria-label=" Resultado : activate to sort column ascending"
                                style=" text-align:center"> Resultado
                            </th>

                            <th class="sorting" tabindex="0" aria-controls="sample_3" rowspan="1"
                                colspan="1"
                                aria-label=" Estado : activate to sort column ascending"
                                style=" text-align:center"> Usuario
                            </th>


                        </tr>

                        </tr>
                        </thead>
                        <tbody id="tbodyhistorico"></tbody>
                    </table>
                </div>


            </div>

            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>

</div>





<script src="./app/resources/global/plugins/respond.min.js"></script>
<script src="./app/resources/global/plugins/excanvas.min.js"></script>
<script src="./app/resources/global/plugins/ie8.fix.min.js"></script>
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="./app/resources/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="./app/resources/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="./app/resources/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="./app/resources/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
        type="text/javascript"></script>
<script src="./app/resources/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="./app/resources/global/plugins/bootstrap-switch/js/bootstrap-switch.js"
        type="text/javascript"></script>


<!-- END CORE PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="./app/resources/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="./app/resources/pages/scripts/components-bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="./app/resources/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
<script src="./app/resources/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
<script src="./app/resources/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
<script src="./app/resources/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
<script src="./app/resources/global/plugins/sweetalert2-9.10.2/package/dist/sweetalert2.js"
        type="text/javascript"></script>
<script src="./app/resources/global/plugins/chosen.jquery.min.js" type="text/javascript"></script>
<!--  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>-->
<script src="./app/resources/global/plugins/select2.full.min.js" type="text/javascript"></script>
<script src="./app/resources/global/plugins/editor.js" type="text/javascript"></script>

<script src="./app/resources/global/plugins/tinymce/tinymce.min.js" type="text/javascript"></script>

<script src="./app/resources/global/plugins/html-docx.js" type="text/javascript"></script>
<script src="./app/resources/global/plugins/file-saver/FileSaver.js" type="text/javascript"></script>

<script src="./app/resources/global/plugins/bootstrap-table/bootstrap-table.js" type="text/javascript"></script>
<script src="./app/resources/pages/scripts/toastr.min.js" type="text/javascript"></script>
<script src="./app/resources/pages/scripts/ui-toastr.min.js" type="text/javascript"></script>

<!--  datatable -->
<script src="./app/resources/global/plugins/datatable/datatable.js" type="text/javascript"></script>
<script src="./app/resources/global/plugins/datatable/datatables.min.js" type="text/javascript"></script>
<script src="./app/resources/global/plugins/datatable/datatables.bootstrap.js" type="text/javascript"></script>
<!--  datatable -->

<script src="./app/resources/global/plugins/progressladabtn/spin.min.js" type="text/javascript"></script>
<script src="./app/resources/global/plugins/progressladabtn/ladda.min.js" type="text/javascript"></script>
<script src="./app/resources/global/plugins/progressladabtn/ui-buttons-spinners.min.js?v=<?= date('YmdHis') ?>"
        type="text/javascript"></script>

<script src="./app/resources/global/plugins/x-editable/bootstrap-editable.min.js"></script>
<script src="./app/resources/global/plugins/typeahead/typeaheadjs.js"></script>

<script type="text/javascript">
    var WS_APPSERVICE_URL = '<?php echo $_SESSION['WS_APPSERVICE_URL']; ?>';
    var WS_APPSERVICE_TOKEN = '<?php echo $_SESSION['WS_APPSERVICE_TOKEN']; ?>';
</script>

<script src="./app/resources/apps/scripts/scriptconfiguraciones.js?v=<?= date('YmdHis') ?>"></script>
<script src="./app/resources/apps/scripts/funciones_globales.js?v=<?= date('YmdHis') ?>"
        type="text/javascript"></script>
<?php
$phpsession = session_name() . "=" . trim(session_id());
$pestana_document = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";


?>
<script type="text/javascript">

    var session_name = '<?php  echo session_name() ?>'
    var phpsession = '<?php  echo $phpsession ?>'
    var session_id = '<?php echo session_id()  ?>';
    var pestana_document = '<?=$pestana_document ?>'
    var usua_login = '<?= $_SESSION['usua_login'] ?>'
    var usua_nomb = '<?= $_SESSION['usua_nomb'] ?>'
    var usua_codi = '<?= $_SESSION['codusuario'] ?>'
    var dependencia = '<?= $_SESSION['dependencia'] ?>'
    var dependencia_padre = '<?= $_SESSION['depe_codi_padre'] ?>'
    var depe_nomb = '<?= $_SESSION['depe_nomb'] ?>'
    var estatusValEscRad = 'none';

    var doc_allUsersSinAutorizar = new Array();
    var radi_padre = '<?= $verrad ?>' //siempre almaceno el padre
    var radiPrincipal = {};
    var anexos_selecteds_v = []; //lista de id de anexos seleccionados
    var radicados_selecteds_v = []; //lista de radicados seleccionados
    radicados_selecteds_v[0] = {};
    radicados_selecteds_v[0].radicado = '<?= $verrad ?>' //radicado sobre el cual se hara la validacion, por defecto le pongo el del padre
    var userConfigurados = [];
    var firmantesselected = new Array();
    var document_metadata = new Array(); //info de la tabla document_metadata
    var usersValidaron = new Array();  //usuarios que ya validaron el doc: document_authorized_by
    var doc_codusuario = '<?php echo $_SESSION["usuario_id"]?>';
    var usua_email = '<?php echo $_SESSION["usua_email"]?>';
    var codigo_aprobar_doc = '<?php echo $_SESSION["codigo_aprobar_doc"]?>'; //codigo que se tiene en session para el envio de email
    var expiracion_aprobar_doc = '<?php echo $_SESSION["expiracion_aprobar_doc"]?>'; //fecha de expiracion que se tiene en session para el envio de email
    var horaminuto_expira = '<?= $_SESSION["expiracion_aprobar_doc"] != '' ? date('H:i', strtotime($_SESSION["expiracion_aprobar_doc"])) : '' ?>'

    var solofecha_expira = '<?= $_SESSION["expiracion_aprobar_doc"] != '' ? date('d-m-Y', strtotime($_SESSION["expiracion_aprobar_doc"])) : '' ?>'
    var fechaahora = '<?= date('YmdHis') ?>';
    var codigo_email_enviado = '<?= $_SESSION['codigo_email_enviado'] ?>';// indicador de si el codigo email fue enviado y almacenado en bd garantia de mostrar opciones de validacion
    var METODO_VALIDACION = '<?php echo $_SESSION["METODO_VALIDACION"]?>';
    var NOMBRE_BOTON_VALIDAR = '<?= $_SESSION["NOMBRE_BOTON_VALIDAR"]?>';
    var datos_primera_validacion = new Array(); // cuando se le da click a validar, modificar o re-generar, se hac euna validacion ajax do
    //donde dice si fue escaneado etc, esta variable almacfena todos esos datos. function clickEnBtnsValidar
    var borrador_id = "";
    var radicados_sin_regenerar =<?php echo json_encode($radicados_sin_regenerar); ?>;

    var FECHA_PASW_DOC = '<?= date('YmdHis', strtotime($_SESSION["FECHA_PASW_DOC"])) ?>';
    var FECHA_PASW_DOC_MASEISMESES = '<?= date('YmdHis', strtotime($_SESSION["FECHA_PASW_DOC"] . '+6 months')) ?>';
    var tieneClaveValidacion =
        '<?= $_SESSION["USUA_PASW_DOC"] != '' && $_SESSION["USUA_PASW_DOC"] != null ? 'SI' : 'NO' ?>'; //para verificar que tenga algo en USUA_PASW_DOC

    var showBtnValidarModal = true;
    var disableSelectValidadores = false;

    var isAsociado = 'NO';

    console.log('navigator', navigator);
    console.log('VERSION NAVEGADOR', getBrowser());

    /**
     * variable usara para los metadatos
     * @type {string}
     */
    var url_app = window.location.href;


    var entidad_largo = '<?php echo $_SESSION["entidad_largo"]?>';
    var nit_entidad = '<?php echo $_SESSION["nit_entidad"]?>';
    var httpWebOficial = '<?php echo $_SESSION["httpWebOficial"]?>';

    var digitosDependencia = '<?= $_SESSION["digitosDependencia"] ?>';

    configuraciones =<?php  echo json_encode($configuraciones) ?>;

    var conjugacionValidar =<?php  echo $_SESSION['NOMBRE_BOTON_VALIDAR_CONJUGADO'] ?>;

    var asuntosRadicados = new Array();
    var historicoComprobacion = new Array();
    var tablaHistoricoC = null;
    var tablaHistoricoADefinir = null;
    var ultimaComH = [];
    var htmlTabla = null;
    var vusua_admin_sistema = <?php echo $_SESSION["usua_admin_sistema"]?>;

    <?php
    if(count($asuntosRadicados) > 0){ ?>
    asuntosRadicados =   <?php  echo json_encode($asuntosRadicados) ?>;
    <?php }
    ?>

    armarConfigsHasValues();

</script>
<?php
//archivo javascript donde se hacen todas las validaciones  ?>

<script src="./app/resources/apps/scripts/services/ConfiguracionesService.js?v=<?= date('Ymdis') ?>"
        type="text/javascript"></script>

<script src="./app/resources/apps/scripts/services/ValidacionService.js?v=<?= date('Ymdis') ?>"
        type="text/javascript"></script>

<script src="./app/resources/apps/scripts/services/RadicadoService.js?v=<?= date('Ymdis') ?>"
        type="text/javascript"></script>


<!-- Controllers -->


<script src="./app/resources/apps/scripts/controllers/Configuraciones.js?v=<?= date('Ymdis') ?>"
        type="text/javascript"></script>

<script src="./app/resources/apps/scripts/fun_validar_rad.js?v=<?= date('YmdHis') ?>"
        type="text/javascript"></script>

<script src="./app/resources/pages/scripts/bootstrap-maxlength.min.js" type="text/javascript"></script>


<script src="./app/resources/apps/scripts/controllers/ReasignarRadicado.js?v=<?= date('Ymdis') ?>"
        type="text/javascript"></script>

<script src="./app/resources/apps/scripts/services/OrdenesDePagoService.js?v=<?= date('Ymdis') ?>"
        type="text/javascript"></script>


<script src="./app/resources/apps/scripts/controllers/Validacion.js?v=<?= date('Ymdis') ?>"
        type="text/javascript"></script>


<?php
//esto es para recuperar en un string, todo lo que viene por post
$redirigirDocumentos = "";
foreach ($_GET as $key => $value) {
    $redirigirDocumentos .= $key . "=" . $value . "&";
}
$redirigirDocumentos = substr($redirigirDocumentos, 0, strlen($estatusin) - 1)
?>
<script type="text/javascript">

    var redirigirDocumentos = '<?= $redirigirDocumentos ?>'

    function redireccionaracarpeta() {
        //esta funcion es llamada cuando se radica un borrador desde la pestana documentos
        console.log(redirigirDocumentos)
        window.location.href = "../../verradicado.php?" + redirigirDocumentos
    }


</script>

<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require "$ruta_raiz/vendor/autoload.php";
require_once "$ruta_raiz/config.php";


if ($_SESSION["FUNCTION_BORRADORES"] == "SI") {
    ?>
    <iframe src="app/borradores/index.php?radicado=<?php echo $_GET['verrad'] ?>" id="iframeborradores"
            name="iframeborradores"
            width="100%" height="600px" scrolling="auto" frameborder="1">

    </iframe>


    <?php
}


?>
<br>


</body>
