<?php

session_start();

$ruta_raiz = ".";
if (!$_SESSION['dependencia'])
    header("Location: $ruta_raiz/cerrar_session.php");
/*************************************************************************************/
/* ORFEO GPL:Sistema de Gestion Documental		http://www.orfeogpl.org	     */
/*	Idea Original de la SUPERINTENDENCIA DE SERVICIOS PUBLICOS DOMICILIARIOS     */
/*				COLOMBIA TEL. (57) (1) 6913005  orfeogpl@gmail.com   */
/* ===========================                                                       */
/*                                                                                   */
/* Este programa es software libre. usted puede redistribuirlo y/o modificarlo       */
/* bajo los terminos de la licencia GNU General Public publicada por                 */
/* la "Free Software Foundation"; Licencia version 2. 			             */
/*                                                                                   */
/* Copyright (c) 2005 por :	  	  	                                     */
/* SSPS "Superintendencia de Servicios Publicos Domiciliarios"                       */
/*   Jairo Hernan Losada  jlosada@gmail.com                Desarrollador             */
/*   Sixto Angel Pinzón López --- angel.pinzon@gmail.com   Desarrollador             */
/* C.R.A.  "COMISION DE REGULACION DE AGUAS Y SANEAMIENTO AMBIENTAL"                 */
/*   Liliana Gomez        lgomezv@gmail.com                Desarrolladora            */
/*   Lucia Ojeda          lojedaster@gmail.com             Desarrolladora            */
/* D.N.P. "Departamento Nacional de Planeación"                                      */
/*   Hollman Ladino       hollmanlp@gmail.com                Desarrollador          */
/*                                                                                   */
/* Colocar desde esta lInea las Modificaciones Realizadas Luego de la Version 3.5    */
/*  Nombre Desarrollador   Correo     Fecha   Modificacion                           */
/*************************************************************************************/
include_once "class_control/AplIntegrada.php";
$objApl = new AplIntegrada($db);
$lkGenerico = "&usuario=$krd&nsesion=" . trim(session_id()) . "&nro=$verradicado" . "$datos_envio";

$conjugacionValidar = json_decode($_SESSION['NOMBRE_BOTON_VALIDAR_CONJUGADO'], true);
$pestana_document = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
?>
<script src="js/popcalendar.js"></script>
<script>
    function regresar() {	//window.history.go(0);
        window.location.reload();
    }

    function CambiarE(est, numeroExpediente) {
        window.open("<?=$ruta_raiz?>/archivo/cambiar.php?<?=session_name()?>=<?=session_id()?>&numRad=<?=$verrad?>&expediente=" + numeroExpediente + "&est=" + est + "&", "Cambio Estado Expediente", "height=100,width=100,scrollbars=yes");
    }

    function modFlujo(numeroExpediente, texp, codigoFldExp) {
        <?php
        $isqlDepR = "SELECT RADI_DEPE_ACTU,RADI_USUA_ACTU from radicado
                                                        WHERE RADI_NUME_RADI = '$numrad'";
        $rsDepR = $db->conn->Execute($isqlDepR);
        $coddepe = $rsDepR->fields['RADI_DEPE_ACTU'];
        $codusua = $rsDepR->fields['RADI_USUA_ACTU'];
        $ind_ProcAnex = "N";
        ?>
        window.open("<?=$ruta_raiz?>/flujo/modFlujoExp.php?<?=session_name()?>=<?=session_id()?>&codigoFldExp=" + codigoFldExp + "&numeroExpediente=" + numeroExpediente + "&numRad=<?=$verrad?>&texp=" + texp + "&ind_ProcAnex=<?=$ind_ProcAnex?>&codusua=<?=$codusua?>", "TexpE<?=$fechaH?>", "height=250,width=750,scrollbars=yes");
    }


    <?php



    include_once("$ruta_raiz/vendor/autoload.php");

    include "$ruta_raiz/config.php";

    //include_once("$ruta_raiz/include/db/ConnectionHandlerNew.php");
    $dbnew = new ConnectionHandler("$ruta_raiz");
    $dbnew->conn->SetFetchMode(ADODB_FETCH_ASSOC);
    $configHelper = new \App\Helpers\Configuraciones($dbnew, $ruta_raiz);
    $configuraciones = $configHelper->getConfiguraciones(false, false, false);


    ?>
</script>
<link href="./app/resources/global/plugins/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
<link href="./app/resources/global/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet"
      type="text/css"/>

<link href="./app/resources/global/plugins/font-awesome/css/fonts.googleapis.css" rel="stylesheet" type="text/css"/>
<link href="./app/resources/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="./app/resources/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet"
      type="text/css"/>
<link href="./app/resources/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet"
      type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN THEME GLOBAL STYLES
el siguiente archivo css, les quita la propiedad de borderradios a los input de form control, por eso lo comente.
<link href="./app/resources/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
-->
<link href="./app/resources/global/css/components-rounded.min.css" rel="stylesheet" id="style_components"
      type="text/css"/>
<link href="./app/resources/global/css/plugins.min.css" rel="stylesheet" type="text/css"/>
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link href="./app/resources/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css"/>
<link href="./app/resources/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css"/>


<script src="./app/resources/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="./app/resources/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="./app/resources/global/plugins/bootstrap-table/bootstrap-table.js" type="text/javascript"></script>
<link href="./app/resources/global/css/select2.min.css" rel="stylesheet"/>
<link href="./app/resources/global/css/select2-bootstrap.min.css" rel="stylesheet"/>
<link href="./app/resources/global/plugins/sweetalert2-9.10.2/package/dist/sweetalert2.min.css" rel="stylesheet"
      type="text/css"/>
<link href="./app/resources/global/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css"/>

<style>

    .borde_tab {
        border: thin solid #377584 !important;
        border-collapse: initial !important;
        border-spacing: 2px 2px !important;
    }

    .popover, .tooltip {
        z-index: 9999999 !important;
    }
    .fa-item i {
    color: #999494 !important;
    }
    .fa-item {
        font-size: 12px !important;;
    }
</style>

<script type="text/javascript">
    var GB_ROOT_DIR = "./js/greybox/";
</script>
<script type="text/javascript" src="js/greybox/AJS.js"></script>
<script type="text/javascript" src="js/greybox/AJS_fx.js"></script>
<script type="text/javascript" src="js/greybox/gb_scripts.js"></script>
<link href="js/greybox/gb_styles.css" rel="stylesheet" type="text/css" media="all" / >

<table width="100%" border="1" cellpadding="0" cellspacing="1" class="borde_tab">
    <tr class=titulos2>
        <td class="titulos2" colspan="6">INFORMACION GENERAL - RADICADO: <?= $verrad ?> </td>
    </tr>
</table>
<table border=0 cellspace=2 cellpad=2 style="" WIDTH=100% align="left" class="borde_tab" id=tb_general>
    <tr>
        <td align="right" bgcolor="#CCCCCC" height="25" class="titulos2">FECHA DE RADICADO</td>
        <td width="25%" height="25" class="listado2"><?= $radi_fech_radi ?></td>
        <td bgcolor="#CCCCCC" width="25%" align="right" height="25" class="titulos2">ASUNTO</td>
        <td class='listado2' colspan="3" width="25%"><?php echo '<span id="asuntoText">'.$ra_asun.'</span><span id="modAsuntosec"></span>' ?></td>
    </tr>
    <tr>
        <td align="right" bgcolor="#CCCCCC" height="25" class="titulos2"><?= $tip3Nombre[1][$ent] ?></td>
        <td class='listado2' width="25%" height="25"><?= $nomRemDes["x1"] ?> </td>
        <td bgcolor="#CCCCCC" width="25%" align="right" height="25" class="titulos2">DIRECCI&Oacute;N CORRESPONDENCIA
        </td>
        <td class='listado2' width="25%"><?= $dirDireccion["x1"] ?></td>
        <td bgcolor="#CCCCCC" width="25%" align="right" height="25" class="titulos2">MUN/DPTO</td>
        <td class='listado2' width="25%"><?= $dirDpto["x1"] . "/" . $dirMuni["x1"] ?></td>
    </tr>
    <!--tr>
	<td align="right" bgcolor="#CCCCCC" height="25" class="titulos2"><? $tip3Nombre[2][$ent] ?></td>
	<td class='listado2' width="25%" height="25"> <?= $nomRemDes["x2"] ?></td>
    <td bgcolor="#CCCCCC" width="25%" align="right" height="25" class="titulos2">DIRECCI&Oacute;N CORRESPONDENCIA </td>
    <td class='listado2' width="25%"> <?= $dirDireccion["x2"] ?></td>
    <td bgcolor="#CCCCCC" width="25%" align="right" height="25" class="titulos2">MUN/DPTO</td>
    <td class='listado2' width="25%"> <?= $dirDpto["x2"] . "/" . $dirMuni["x2"] ?></td>
</tr-->
    <!--tr>
	<td align="right" bgcolor="#CCCCCC" height="25" class="titulos2"><?= $tip3Nombre[3][$ent] ?></td>
	<td class='listado2' width="25%" height="25"> <?= $nombret_us3 ?> -- <?= $cc_documento_us3 ?></td>
    <td bgcolor="#CCCCCC" width="25%" align="right" height="25" class="titulos2">DIRECCI&Oacute;N CORRESPONDENCIA </td>
    <td class='listado2' width="25%"> <?= $direccion_us3 ?></td>
    <td bgcolor="#CCCCCC" width="25%" align="right" height="25" class="titulos2">MUN/DPTO</td>
    <td class='listado2' width="25%"> <?= $dpto_nombre_us3 . "/" . $muni_nombre_us3 ?></td>
</tr-->
    <!--tr>
	<td height="25" bgcolor="#CCCCCC" align="right" class="titulos2">CANTIDAD: </td>
    <td class='listado2' width="25%" height="25"> HOJAS: <?= $radi_nume_hoja ?> &nbsp;&nbsp;&nbsp FOLIOS: <?= $radi_nume_folio ?>  &nbsp;&nbsp;&nbsp;  ANEXOS: <?= $radi_nume_anexo ?></td>
    <td bgcolor="#CCCCCC" width="25%" height="25" align="right" class="titulos2"> DESCRIPCION ANEXOS </td>
    <td class='listado2'  width="25%" height="11"> <?= $radi_desc_anex ?></td>
    <td bgcolor="#CCCCCC" width="25%" align="right" height="25" class="titulos2">&nbsp;</td>
    <td class='listado2' width="25%">&nbsp;</td>
</tr-->
    <tr>
        <td height="25" bgcolor="#CCCCCC" align="right" class="titulos2">FOLIOS:</td>
        <td class='listado2' width="25%" height="25">  <?= $radi_nume_hoja ?>
            &nbsp;&nbsp;&nbsp <?= $radi_nume_folio + $radi_nume_anexo . ""; ?></td>
        <td bgcolor="#CCCCCC" width="25%" height="25" align="right" class="titulos2"> DESCRIPCION ANEXOS</td>
        <td class='listado2' width="25%" height="11"> <?= $radi_desc_anex ?></td>
        <td bgcolor="#CCCCCC" width="25%" align="right" height="25" class="titulos2">&nbsp;</td>
        <td class='listado2' width="25%">&nbsp;</td>
    </tr>

    <tr>
        <td align="right" bgcolor="#CCCCCC" height="25" class="titulos2">DOCUMENTO<br>Asociado</td>
        <td class='listado2' width="25%" height="25">
            <?
            if ($radi_tipo_deri != 1 and $radi_nume_deri) {
                echo $radi_nume_deri;
                /*
          * Modificacion acceso a documentos
          * @author Liliana Gomez Velasquez
          * @since 10 noviembre 2009
         */
                $resulVali = $verLinkArchivo->valPermisoRadi($radi_nume_deri);
                $verImg = $resulVali['verImg'];
                if ($verImg == "SI") {
                    echo "<br>(<a class='vinculos' href='$ruta_raiz/verradicado.php?verrad=$radi_nume_deri &session_name()=session_id()&krd=$krd' target='VERRAD$radi_nume_deri_" . date("Ymdhi") . "'>Ver Datos</a>)";
                } else {
                    echo "<br>(<a class='vinculos' href='javascript:noPermiso()'> Ver Datos</a>)";
                }
            }
            if ($verradPermisos == "Full" or $datoVer == "985") {
                ?>
                <input type=button name=mostrar_anexo value='...' class=botones_2 onClick="verVinculoDocto();">
                <?
            }
            ?>
        </td>
        <td bgcolor="#CCCCCC" width="25%" align="right" height="25" class="titulos2">REF/OFICIO/CUENTA INTERNA</td>
        <td class='listado2' colspan="3" width="25%"> <?= $cuentai ?>&#160;&#160;&#160;&#160;&#160;
            <?
            $muniCodiFac = "";
            $dptoCodiFac = "";
            if ($sector_grb == 6 and $cuentai and $espcodi) {
                if ($muni_us2 and $codep_us2) {
                    $muniCodiFac = $muni_us2;
                    $dptoCodiFac = $codep_us2;
                } else {
                    if ($muni_us1 and $codep_us1) {
                        $muniCodiFac = $muni_us1;
                        $dptoCodiFac = $codep_us1;
                    }
                }
                ?>
                <a href="./consultaSUI/facturacionSUI.php?cuentai=<?= $cuentai ?>&muniCodi=<?= $muniCodiFac ?>&deptoCodi=<?= $dptoCodiFac ?>&espCodi=<?= $espcodi ?>"
                   target="FacSUI<?= $cuentai ?>"><span class="vinculos">Ver Facturacion</span></a>
                <?
            }
            ?>
        </td>
    </tr>
    <tr>
        <td align="right" height="25" class="titulos2">IMAGEN</td>
        <td class='listado2' colspan="1"><span class='vinculos'><?= $imagenv ?></span></td>
        <td align="right" height="25" class="titulos2">...</td>
        <td class='listado2'>
            <span class=leidos2><?= $descFldExp ?></span>&nbsp;&nbsp;&nbsp;
            <?
            if ($verradPermisos == "Full" or $datoVer == "985") {
                ?>
                <!--input type=button name=mostrar_causal value='...' class=botones_2 onClick="modFlujo('<?= $numExpediente ?>',<?= $texp ?>,<?= $codigoFldExp ?>)"-->
                <?
            }
            ?>
        </td>
        <td align="right" height="25" class="titulos2">Nivel de Seguridad</td>
        <td class='listado2' colspan="3">
            <?
            if ($nivelRad == 1) {
                $estado = "Reservado";
            } else {
                $estado = "P&uacute;blico";
            }
            if ($verradPermisos == "Full" or $datoVer == "985") {
                $varEnvio = "krd=$krd&numRad=$verrad&nivelRad=$nivelRad";
                ?>

                <!--input type=button name=mostrar_causal value='...' class=botones_2 onClick="window.open('<?= $ruta_raiz ?>/seguridad/radicado.php?<?= $varEnvio ?>','Cambio Nivel de Seguridad Radicado', 'height=220, width=300,left=350,top=300')"-->
                <a onclick="cargarModal('<?= $ruta_raiz ?>/seguridad/radicado.php?<?= $varEnvio ?>')"
                   title="SEGURIDAD DEL RADICADO:"
                   rel="gb_page_center[600, 435]" id="btnLoadModalSegur">
                    <img src='imagenes/candado.jpg' alt='' border=0 width="25" height="25"> <?php echo $estado; ?> </a>
                <?
            } else {
                echo $estado;
            }
            ?>
        </td>
    </tr>
    <tr>
        <td align="right" height="25" class="titulos2">TRD</td>
        <td class='listado2' colspan="6">
            <?
            if (!$codserie) $codserie = "0";
            if (!$tsub) $tsub = "0";
            if (trim($val_tpdoc_grbTRD) == "///") $val_tpdoc_grbTRD = "";
            ?>
            <?= $serie_nombre ?><font color=black>/</font><?= $subserie_nombre ?><font
                    color=black>/</font><?= $tpdoc_nombreTRD ?>
            <?
            if ($verradPermisos == "Full" or $datoVer == "985") {
            ?>
            <input type=button name=mosrtar_tipo_doc2 value='...' class=botones_2
                   onClick="ver_tipodocuTRD(<?= $codserie ?>,<?= $tsub ?>);">
        </td>
    </tr>
    <tr>

        <td align="right" height="25" class="titulos2"></td>
        <td class='listado2' colspan="6">
            <?= $sector_nombre ?>
            <?
            $nombreSession = session_name();
            $idSession = session_id();
            if ($verradPermisos == "Full" or $datoVer == "985") {
                $sector_grb = (isset($sector_grb)) ? $sector_grb : 1;
                $causal_grb = (isset($causal_grb) || $causal_grb != '') ? $causal_grb : 0;
                $deta_causal_grb = (isset($deta_causal_grb) || $deta_causal_grb != '') ? $deta_causal_grb : 0;

                $datosEnviar = "'$ruta_raiz/causales/mod_causal.php?" . $nombreSession . "=" . $idSession .
                    "&krd=" . $krd .
                    "&verrad=" . $verrad .
                    "&sector=" . $sector_grb .
                    "&sectorCodigoAnt=" . $sector_grb .
                    "&sectorNombreAnt=" . $sector_nombre .
                    "&causal_grb=" . $causal_grb .
                    "&causal_nombre=" . $causal_nombre .
                    "&deta_causal_grb=" . $deta_causal_grb .
                    "&ddca_causal_grb=" . $ddca_causal .
                    "&ddca_causal_nombre=" . $ddca_causal_nombre . "'";
                ?>
                <input type=hidden name="mostrar_causal" value="..." class="botones_2"
                       onClick="window.open(<?= $datosEnviar ?>,'Tipificacion_Documento','height=300,width=750,scrollbars=no')">
                <input type="hidden" name="mostrarCausal" value="N">
                <?
            }
            ?>
        </td>
    </tr>
    <tr>
        <td align="right" height="25" class="titulos2"></td>
        <?
        $causal_nombre_grb = $causal_nombre;
        $dcausal_nombre_grb = $dcausal_nombre;
        $$ddca_causal_nombre_grb = $ddca_causal_nombre;
        ?>
        <td class='listado2' colspan="6">
            <?= $causal_nombre ?>

            <?= $dcausal_nombre ?>

            <?= $ddca_causal_nombre ?>

            <?
            if ($verradPermisos == "Full" or $datoVer == "985") {
                ?>
                <input type=hidden name="mostrar_causal" value="..." class='botones_2'
                       onClick="window.open(<?= $datosEnviar ?>,'Tipificacion_Documento','height=300,width=750,scrollbars=no')">
                <?
            }
            ?>
        </td>
    </tr>
    <tr>
        <td align="right" height="25" class="titulos2"></td>
        <td class='listado2' colspan="6">
            <?= $tema_nombre ?>
            <?
            if ($verradPermisos == "Full" or $datoVer == "985") {
                ?>
                <input type=hidden name="mostrar_temas" id='mostrar_temas' value='...' class=botones_2
                       onClick="ver_temas();">
                <?
            }
            }
            ?>
        </td>
    </tr>
</table>
</form>


<div id="modal_seguridad_rad" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="">
            <div class="modal-header bg-blue-chambray bg-font-yellow text-center">
                <button type="button" data-dismiss="modal" title="Cerrar" aria-hidden="true"
                        class="btn btn-lg "
                        style="float: right;background-color: #2C3E50!important; border-radius:20px !important">
                    <i class="fa fa-remove"></i></button>
                <h4 class="modal-title"><strong>SEGURIDAD DEL RADICADO</strong></h4>
            </div>

            <div class="modal-body" id="body_seguridad_rad">

                <div class="row" id="search-filter-container" style="text-align: center">
                    <div class="col-md-12">
                        <div class="search-bar bordered">
                            <form action="#" class="form-horizontal">
                                <div class="form-body">
                                    <div class="form-group" id="divprevioswiche" style="height: 40px !important">

                                        <input type="checkbox" id="swich_seguridad" class="make-switch "
                                               data-on-text="P&uacute;blico" data-off-text="Restringir"
                                               checked data-on-color="primary" data-off-color="danger"
                                        >

                                    </div>
                                    <label class=" col-md-12" style="color:#4B77BE !important" id="labeldocpublico">
                                        Documento p&uacute;blico, sin restricci&oacute;n de acceso</label>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <br>
                <div class="row divshiden" style="display:none">
                    <div class="col-md-12">Usuarios autorizados para ver este documento:</div>
                    <div class="col-md-12">
                        <div class="input-icon right" id="div_prev_select_users">

                            <select class="js-data-example-ajax" name="id_select_users[]" multiple id="id_select_users"
                                    style="width: 100%"></select>
                            <br>
                        </div>
                    </div>
                </div>

                <div class="row divshiden" style="display:none">
                    <div class="col-md-12" style="line-height: 4pt"><p>Dependencias autorizadas para ver este
                            documento:</p>
                        <p style="font-size: 10pt;font-style: oblique">Todos los usuarios de las dependencias elegidas
                            podrán ver</p></div>
                    <div class="col-md-12">
                        <div class="input-icon right" id="div_prev_select_dependencias">
                            <select class="js-data-example-ajax" name="select_dependencias[]" multiple
                                    id="select_dependencias"
                                    style="width: 100%"></select>
                            <br>
                        </div>
                    </div>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>

</div>


<div id="modal_modAsunto" class="modal fade" tabindex="-1" aria-hidden="true" style="display: none;">
    <div class="modal-dialog " style="width:65%;">
        <div class="modal-content">
            <div class="modal-header bg-blue-chambray bg-font-yellow text-center">
                <button type="button" title="Cerrar"
                        class="btn btn-lg " onclick="cerrarModalmodAsunto()"
                        style="float: right;background-color: #2C3E50!important; border-radius:20px !important">
                    <i class="fa fa-remove"></i></button>
                <h4 class="modal-title"><strong id="titulomodalnuevoexped">Modificar Asunto de Radicado</strong></h4>

            </div>
            <div class="modal-body" id="radicated_info">

                <div id="btnAdvWar" style="display:none;" class="alert alert-warning alert-dismissable">

                    <strong>Advertencia!</strong>
                    <br> El asunto solo se debería modificar cuando el documento se encuentra en BORRADOR.
                    <br> Los cambios se guardarán en el historial del radicado. ¿Está seguro?
                    <div class="row">
                        <div class="col-md-12 ">
                            <span class="col-md-3"><button type="button"  class="btn btn-danger">Si, Continuar</button></span><span class="col-md-3"><button onclick="cerrarModalmodAsunto()" type="button" class="btn btn-info">Cancelar</button></span><span class="col-md-6"></span>
                        </div>
                    </div>
                </div>

                <div id="btnAdvDanger" class="alert alert-danger" style="display: none;">

                </div>

                <form action="" method="POST" class="form-horizonal" id="">
                    <div class="form-body modal-body todo-task-modal-body">

                        <div class="row" style="margin-top:40px">
                            <div class="col-md-12 ">
                                <div class="form-group">



                                    <label class=" col-md-12" style="display: none;" id="AsuntoActualLab">AsuntoActual :</label>
                                    <div class="col-md-12">
                                        <div class="input-icon right" style="position: relative;">
                                            <textarea  required="required" name="asuntoActual" id="AsuntoActual" class="form-control" maxlength="999" style="display: none;border: solid grey 1px; height:90px" placeholder=""></textarea>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div><br>

                                    <label style="display: none;" class=" col-md-12" id="addAsunto">Agregar al asunto:</label>
                                    <div class="col-md-12">
                                        <div class="input-icon right" style="position: relative;">
                                            <textarea required="required" name="asuntoAdd" id="asuntoAdd" class="form-control" maxlength="999" style="display: none;border: solid grey 1px; height:90px" placeholder=""></textarea>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div><br>

                                    <label style="display: none;" class=" col-md-12" id="newAsunto">Nuevo Asunto:</label>
                                    <div class="col-md-12">
                                        <div id="nuevoAs" style="display: none;" class="alert alert-info">
                                            
                                        </div>
                                    </div> <br>


                                </div>
                            </div>

                        </div>
                    </div>


                </form>
            </div>
            <div class="modal-footer">
                <div class="form-actions">
                    <div class="row">

                        <div class="col-md-6">

                        </div>
                        <div class="col-md-4" id="divBtnAccionCrearExped">
                            <button  type="button" class="btn btn-lg blue" id="btnModAsunto"
                                    style="display:none;border-radius:20px !important" onclick="modAsunto(event)">
                                Guardar Cambios
                            </button>
                        </div>
                        <div class="col-md-2">
                            <button type="button" class="btn btn-lg" onclick="cerrarModalmodAsunto()"
                                    style="border-radius:20px !important">
                                Cerrar
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="./app/resources/global/plugins/respond.min.js"></script>
<script src="./app/resources/global/plugins/excanvas.min.js"></script>
<script src="./app/resources/global/plugins/ie8.fix.min.js"></script>
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="./app/resources/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="./app/resources/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="./app/resources/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="./app/resources/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="./app/resources/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="./app/resources/global/plugins/bootstrap-switch/js/bootstrap-switch.js" type="text/javascript"></script>


<!-- END CORE PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="./app/resources/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="./app/resources/pages/scripts/components-bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="./app/resources/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
<script src="./app/resources/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
<script src="./app/resources/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
<script src="./app/resources/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
<script src="./app/resources/global/plugins/sweetalert2-9.10.2/package/dist/sweetalert2.js"
        type="text/javascript"></script>
<script src="./app/resources/global/plugins/chosen.jquery.min.js" type="text/javascript"></script>
<!--  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>-->
<script src="./app/resources/global/plugins/select2.full.min.js" type="text/javascript"></script>
<script src="./app/resources/global/plugins/editor.js" type="text/javascript"></script>

<script src="./app/resources/global/plugins/tinymce/tinymce.min.js" type="text/javascript"></script>

<script src="./app/resources/global/plugins/html-docx.js" type="text/javascript"></script>
<script src="./app/resources/global/plugins/file-saver/FileSaver.js" type="text/javascript"></script>

<script src="./app/resources/global/plugins/bootstrap-table/bootstrap-table.js" type="text/javascript"></script>
<script src="./app/resources/pages/scripts/toastr.min.js" type="text/javascript"></script>
<script src="./app/resources/pages/scripts/ui-toastr.min.js" type="text/javascript"></script>

<script src="./app/resources/apps/scripts/scriptconfiguraciones.js?v=<?= date('YmdHis') ?>"></script>
<script src="./app/resources/apps/scripts/funciones_globales.js?v=<?= date('YmdHis') ?>"
        type="text/javascript"></script>

<script type="text/javascript">

    configuraciones =<?php  echo json_encode($configuraciones) ?>;
    armarConfigsHasValues();
    var radicado = '<?=$verrad?>'
    var allUsers = new Array(); //arreglo que almacena todos los usuarios a mostrar
    var allDepend = new Array(); //arreglo que almacena todas las dependencias a mostrar
    var onlyUsersSelected = new Array();
    var onlyDependSelected = new Array();
    var allSelecteds = new Array();
    var continueOnChanceSwitch = false;
    var radiPadre;
    var usua_codi = '<?= $_SESSION['codusuario'] ?>';
    var dependencia = '<?= $_SESSION['dependencia'] ?>';

    var seguridad_radicado = "true"; //variable que al momento de radicar el borrador, indica si es publico el radicado o no
    //tdo esto mediante el swiche, por defecto esta en true, es decir, Publico

    var estatusValEscRad = 'none';
    var pestana_document = '<?=$pestana_document ?>';
    var usualog_id = '<?=$_SESSION['usuario_id']?>';

    var usua_admin_sistema = '<?=$_SESSION["usua_admin_sistema"]?>';

    var languageseelct2 = {
        inputTooShort: function () {
            return 'Ingrese para buscar';
        },
        noResults: function () {
            return "Sin resultados";
        },
        searching: function () {
            return "Buscando...";
        },
        errorLoading: function () {
            return 'El resultado aún no se ha cargado.';
        },
        loadingMore: function () {
            return 'Cargar mas resultados...';
        },
    }

    function alertModal(title=false, message, type, cerrarantesde) {
        var titulo = '';
        if (type == 'error') {
            var loaderBg = '#ff6849';
            var icon = 'error';
            titulo = 'ERROR'
        }
        if (type == 'success') {
            titulo = 'FELICIDADES'
        }
        if (type == 'warning') {
            titulo = 'ATENCION'
        }
        if (type == 'info') {
            titulo = 'INFORMACION'
        }

        if (title != false) {
            titulo = title
        }

        var hideAfter = 4500;
        if (cerrarantesde != true) {
            hideAfter = cerrarantesde;
        }
        if (cerrarantesde == false) {
            hideAfter = false;
        }
        /* return  $.toast({
            heading: titulo,
            text: message,
            position: 'top-right',
            loaderBg: loaderBg,
            icon: icon,
            hideAfter: hideAfter,
            stack: 6

        });*/

        return toastr[type](message, titulo, {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "1000",
            "hideDuration": "1000",
            "timeOut": hideAfter,
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        })
    }

    function modAsunto(){
        $('#btnModAsunto').prop("disabled", true);
        var Asunto;
        if (estatusValEscRad == 'validado' || estatusValEscRad == 'escaneado'){
            Asunto = $('#asuntoAdd').val();
        }else{
            Asunto = $('#AsuntoActual').val();
        }
        $.ajax({
            url: configsHasValue['WS_APPSERVICE_URL'] +'/radicado/saveAsunto',
            headers: {
                "Authorization": 'Bearer '+configsHasValue['WS_APPSERVICE_TOKEN']
            },
            type: 'POST',
            dataType: 'json',
            async: false,
            data: {
                radicado_id: radicado,
                asunto: Asunto,
                user_id: usualog_id
            },
            success: function (response) {
                alertModal('Bien hecho !', response.data, 'success', '10000');
                if(pestana_document!=undefined && pestana_document!=false){
                    setTimeout(function () {
                        window.location.href = pestana_document;
                    }, 3000)
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {

                if(xhr.responseJSON.message){
                    Swal.fire("Error", xhr.responseJSON.message, "error");
                    $('#btnModAsunto').prop("disabled", false);
                }else{
                    Swal.fire("Error", "Ha ocurrido un error al modificar el asunto", "error");
                    $('#btnModAsunto').prop("disabled", false);
                }
            }
        });
    }

    function traerRadicado(){
        console.log(radicado);
        var Readonly = '';
        var radicadoarray = new Array();
        radicadoarray[0] = radicado;

        $.ajax({
            url: configsHasValue['WS_APPSERVICE_URL'] +'/radicado/validaEstaEscaneado',
            headers: {
            "Authorization": 'Bearer '+configsHasValue['WS_APPSERVICE_TOKEN']
            },
            type: 'POST',
            dataType: 'json',
            async: false,
            data: {
                radicados: radicadoarray
            },
            success: function (response) {
                 if (typeof(response.objects[0].radicado.radi_nume_iden) != 'Undefined' && (response.objects[0].radicado.radi_nume_iden == 1 || response.objects[0].radicado.radi_nume_iden == 2) ){

                     estatusValEscRad = 'validado';
                     Readonly = 'readonly';
                }else if(typeof(response.objects[0].fueescaneado) != 'Undefined' && (response.objects[0].fueescaneado != 0)){
                     estatusValEscRad = 'escaneado';
                     Readonly = 'readonly';
                }else{
                     estatusValEscRad = 'none';
                }

                $('#modal_modAsunto').modal('show');
                 if (estatusValEscRad == 'validado'){

                     $('#btnAdvDanger').html('<strong>Advertencia!</strong> El Radicado ya ha sido <?= $conjugacionValidar[9] ?>. Solo puede agregarle información al asunto.<br><br>El asunto solo se debería modificar cuando el documento se encuentra en BORRADOR.');
                    $('#btnAdvDanger').show();
                     showAsuntoInp(response.objects[0].radicado.ra_asun,Readonly);
                     $('#addAsunto').show();
                     $('#asuntoAdd').show();
                     $('#btnModAsunto').show();
                     combinarAsuntos();
                 }else if(estatusValEscRad == 'escaneado'){
                    $('#btnAdvDanger').html('<strong>Advertencia!</strong> El Radicado ya ha sido digitaliado. Solo puede agregarle información al asunto.<br><br>El asunto solo se debería modificar cuando el documento se encuentra en BORRADOR.');
                    $('#btnAdvDanger').show();
                     showAsuntoInp(response.objects[0].radicado.ra_asun,Readonly);
                     $('#addAsunto').show();
                     $('#asuntoAdd').show();
                     $('#btnModAsunto').show();
                     combinarAsuntos();
                 }else{
                     $('#btnAdvWar').click(function () {
                         showAsuntoInp(response.objects[0].radicado.ra_asun,'');
                     });
                    $('#btnAdvWar').show();

                 } 

            },
            error: function (xhr, ajaxOptions, thrownError) {

                if(xhr.responseJSON.message){
                    Swal.fire("Error", xhr.responseJSON.message, "error");
                }else{
                    Swal.fire("Error", "Ha ocurrido un error al buscar los datos", "error");
                }
            }
        });
    }

    function showAsuntoInp(texto,attread) {

        $("#AsuntoActual").html(texto);
        if (attread != ''){
        $("#AsuntoActual").attr('readonly',attread);
        }
        $('#AsuntoActualLab').show();
        $('#AsuntoActual').show();
        $('#btnModAsunto').show();
    }

    function combinarAsuntos() {
        $('#nuevoAs').html($('#AsuntoActual').val());
        $('#asuntoAdd').keyup(function () {
            $('#nuevoAs').html($('#AsuntoActual').val() + ' ' +$('#asuntoAdd').val());
        });

        $('#newAsunto').show();
        $('#nuevoAs').show();
    }

    //trae los usuarios para el select de enviar y para el select de firmantes
    function traerUsers(quebuscar) {

        //?type=public&radicado="+radicado+"&quebuscar="+quebuscar
        $.ajax({
            url: configsHasValue['WS_APPSERVICE_URL'] + '/seguridadRad/usersToAuthorize',
            headers: {
                "Authorization": 'Bearer '+configsHasValue['WS_APPSERVICE_TOKEN']
            },
            type: 'GET',
            dataType: 'json',

            async: false,
            data: {
                type: 'public',
                quebuscar: quebuscar,
                radicado: radicado

            },
            success: function (response) {
                if (quebuscar == "usuarios") {
                    allUsers = response.objects;
                } else {
                    allDepend = response.objects;
                }

            },
            error: function (xhr, ajaxOptions, thrownError) {

                if(xhr.responseJSON.message){
                    Swal.fire("Error", xhr.responseJSON.message, "error");
                }else{
                    Swal.fire("Error", "Ha ocurrido un error al buscar los datos", "error");
                }
            }
        });
    }

    //verifica si un firmante esta en el arreglo firmantesselected
    function yaEstaUserOrDepend(seleccion, temp) {
        var encontro = false;
        for (var i = 0; i < Object.keys(temp).length; i++) {
            if (temp[i].id == seleccion.id) {
                encontro = true;
            }
        }
        return encontro;
    }

    //cuando se selecciona un usuario o dependencia de select2
    function guardarUserSeg(data) {
        var temp = new Array();
        var tipo = 'USUARIO'
        console.log('data', data)
        if (data.usua_nomb != undefined) {
            temp = onlyUsersSelected
        } else {
            temp = onlyDependSelected
            tipo = 'DEPENDENCIA'
        }
        var yaesta = yaEstaUserOrDepend(data, temp)
        console.log('yaesta', yaesta)
        if (yaesta == false) {
            if (data.usua_nomb != undefined) {
                onlyUsersSelected[Object.keys(onlyUsersSelected).length] = data;
            } else {
                onlyDependSelected[Object.keys(onlyDependSelected).length] = data;
            }
            updateSeguridad('agregar', tipo, data)
        }

    }

    //cada vez que se desmarca a un firmante, vuelvo a rellner el arreglo se los selecteds
    function quitarUserSeg(data) {

        var tipo = 'USUARIO'
        var temp = new Array();

        if (data.usua_nomb != undefined) {
            temp = onlyUsersSelected
            onlyUsersSelected = new Array();
        } else {
            tipo = 'DEPENDENCIA'
            temp = onlyDependSelected
            onlyDependSelected = new Array();
        }

        var cont = 0;
        for (var i = 0; i < Object.keys(temp).length; i++) {
            if (temp[i].id == data.id) {
            } else {
                if (data.usua_nomb != undefined) {
                    onlyUsersSelected[cont] = temp[i]
                } else {
                    onlyDependSelected[cont] = temp[i]
                }
                cont++;
            }
        }
        updateSeguridad('quitar', tipo, data)

    }

    function updateSeguridad(accion, tipo, seleccion) {
        var transaccion = (accion == 'agregar') ? 'AUTORIZAR' : 'DESAUTORIZAR'


        var paramsAcceso = {
            'radicado': radicado,
            'tipo_entidad_autorizada': tipo,
            'tipo_entidad' : 'radicado',
            'entidad_autorizada' : seleccion.id,
            'usuario_id' : usualog_id,
            'transaccion' : transaccion
        }

        $.ajax({
            url: configsHasValue['WS_APPSERVICE_URL'] + '/integration/seguridad/autorizacion',
            headers: {
                "Authorization": 'Bearer '+configsHasValue['WS_APPSERVICE_TOKEN']
            },
            type: 'POST',
            dataType: 'json',
            data: paramsAcceso,
            async: false,
            success: function (response) {
                alertModal('Actualizado!', capitalizarPrimeraLetra(response.tipo_entidad_aut) +' '+ response.accion_entidad +': '+retornarParteCadenaSplit(seleccion.text,'(',0), 'success', '5000');

            },
            error: function (response) {
                Swal.fire("Error", "Ha ocurrido un error al buscar los datos", "error");
            }
        });
    }

    function buscarSelecteds() {
        /*var data = {
            function_call: "getAllSeguridad",
            radicado: radicado
        };*/
        var tipoC = 'radicado';
        var valorC = radicado;
        $.ajax({
            url: configsHasValue['WS_APPSERVICE_URL'] + '/integration/seguridad/consultar' +'?tipo_entidad='+tipoC+'&entidad='+valorC,
            headers: {
                "Authorization": 'Bearer '+configsHasValue['WS_APPSERVICE_TOKEN']
            },
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function (response) {
                if (response.isSuccess != undefined && response.isSuccess != false) {

                   /* if (response.radicado[0] != undefined && response.radicado[0]['SGD_SPUB_CODIGO'] != undefined) {
*/
                        if (response.acceso == "1") {
                            popoverPublico()
                            $("#swich_seguridad").bootstrapSwitch('state', false);
                        } else {
                            $("#swich_seguridad").bootstrapSwitch('state', true);
                        }
                    /* }*/
                    continueOnChanceSwitch=true;
                    allSelecteds = response.entidades;
                    //definirSelecteds();
                    definirSegSelecteds();
                    console.log(response);
                } else {
                    //Swal.fire("Error", response.error, "error");
                }

            },
            error: function (response) {
                Swal.fire("Error", "Ha ocurrido un error al buscar los datos", "error");
            }
        });
    }

    function cargarModal() {
        continueOnChanceSwitch=false;
        allSelecteds = new Array();

        if (Object.keys(allUsers).length < 1) {
            traerUsers('usuarios')
        }

        if (Object.keys(allDepend).length < 1) {
            traerUsers('dependencias')
        }

        $(".bootstrap-switch-label").html('Restringir')
        //esto es para el swiche de seguridad
        $('.bootstrap-switch-label').addClass('popovers');
        $('.bootstrap-switch-label').attr('data-original-title', 'Restringir acceso al documento')
        $('.bootstrap-switch-label').attr('data-content', 'Solo tendrá acceso al documento el usuario actual ' +
            'y usuarios o dependencias autorizadas!')
        $('.bootstrap-switch-label').attr('data-placement', 'top')
        $('.bootstrap-switch-label').attr('data-trigger', 'hover')
        $('.bootstrap-switch-label').attr('data-container', 'body')
        $('.bootstrap-switch-label').popover()

        $("#id_select_users").remove();
        $('#div_prev_select_users').html('')
        $('#div_prev_select_users').html('<select class="js-data-example-ajax" name="id_select_users[]" multiple id="id_select_users"\n' +
            '                                    style="width: 100%"></select>\n' +
            '                            <br>')
        $("#id_select_users").select2(
            {
                width: '100%',
                multiple: true,
                dropdownParent: $("#modal_seguridad_rad"),
                allowClear: true,
                data: allUsers,
                ajax: {
                    url: configsHasValue['WS_APPSERVICE_URL'] + '/seguridadRad/usersToAuthorize',
                    dataType: 'json',
                    delay: 250,
                    headers: {
                        "Authorization": 'Bearer '+configsHasValue['WS_APPSERVICE_TOKEN']
                    },
                    data: function (params) {
                        return {
                            search: params.term,
                            type: 'public',
                            quebuscar: 'usuarios',
                            radicado: radicado
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: data.objects,
                        };
                    },
                    cache: true,
                    error: function(xhr, ajaxOptions, thrownError){
                        //if(xhr.responseJSON.message){
                            //Swal.fire("Error", xhr.responseJSON.message, "error");
                        //}else{
                            //Swal.fire("Error", "Ha ocurrido un un error al buscar los datos", "error");
                        //}
                    }
                },
                placeholder: 'Buscar Usuarios',
                escapeMarkup: function (markup) {
                    return markup;
                }, // let our custom formatter work
                language: languageseelct2
            }).on('select2:selecting', function (e) {
            //cada vez que selecciono una opcion
            guardarUserSeg(e.params.args.data)

        }).on("select2:unselecting", function (e) {

            //cada vez que desmarco una opcion
            quitarUserSeg(e.params.args.data)

        }).trigger('change');


        $("#select_dependencias").remove();
        $('#div_prev_select_dependencias').html('')
        $('#div_prev_select_dependencias').html('<select class="js-data-example-ajax" ' +
            'name="select_dependencias[]" multiple id="select_dependencias"' +
            'style="width: 100%"></select> <br> ')

        $("#select_dependencias").select2(
            {
                width: '100%',
                multiple: true,
                dropdownParent: $("#modal_seguridad_rad"),
                allowClear: true,
                data: allDepend,
                ajax: {
                    url: configsHasValue['WS_APPSERVICE_URL'] + '/seguridadRad/usersToAuthorize',
                    dataType: 'json',
                    delay: 250,
                    headers: {
                        "Authorization": 'Bearer '+configsHasValue['WS_APPSERVICE_TOKEN']
                    },
                    data: function (params) {
                        return {
                            search: params.term,
                            type: 'public',
                            quebuscar: 'dependencias',
                            radicado: radicado
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: data.objects,
                        };
                    },
                    cache: true,
                    error: function(xhr, ajaxOptions, thrownError){
                        if(xhr.responseJSON.message){
                            Swal.fire("Error", xhr.responseJSON.message, "error");
                        }else{
                            Swal.fire("Error", "Ha ocurrido un un error al buscar los datos", "error");
                        }
                    }
                },
                placeholder: 'Buscar Dependencias',
                escapeMarkup: function (markup) {
                    return markup;
                }, // let our custom formatter work
                language: languageseelct2
            }).on('select2:selecting', function (e) {
            //cada vez que selecciono una opcion
            guardarUserSeg(e.params.args.data)

        }).on("select2:unselecting", function (e) {

            //cada vez que desmarco una opcion
            quitarUserSeg(e.params.args.data)

        }).trigger('change');

        buscarSelecteds();

        $("#modal_seguridad_rad").modal('show')
    }

    function definirSegSelecteds() {
        var selectedUserValues = new Array();
        var selectedDepenValues = new Array();

        var contUser = 0;
        var contDepen = 0;

        for (var i = 0; i < Object.keys(allSelecteds).length; i++) {

            if (allSelecteds[i].tipo_entidad_autorizada == "USUARIO") {
                selectedUserValues[contUser] = allSelecteds[i].entidad_autorizada;
                contUser++;
            } else {
                selectedDepenValues[contDepen] = allSelecteds[i].entidad_autorizada;
                contDepen++;
            }
        }

        $("#select_dependencias").val(selectedDepenValues)
        $('#select_dependencias').trigger('change.select2');

        $("#id_select_users").val(selectedUserValues)
        $('#id_select_users').trigger('change.select2');
    }

    function definirSelecteds() {
        var selectedUserValues = new Array();
        var selectedDepenValues = new Array();

        var contUser = 0;
        var contDepen = 0;

        for (var i = 0; i < Object.keys(allSelecteds).length; i++) {

            if (allSelecteds[i].TIPO_USU_PERMITIDO == "USUARIO") {
                selectedUserValues[contUser] = allSelecteds[i].USUARIOS_PERMITIDOS;
                contUser++;
            } else {
                selectedDepenValues[contDepen] = allSelecteds[i].USUARIOS_PERMITIDOS
                contDepen++;
            }
        }

        $("#select_dependencias").val(selectedDepenValues)
        $('#select_dependencias').trigger('change.select2');

        $("#id_select_users").val(selectedUserValues)
        $('#id_select_users').trigger('change.select2');
    }

    function guardarAllSelecteds(todosselected) {
        allSelecteds = todosselected;
    }

    function modalmodAsunto($radicado_id){
        traerRadicado();

    }

    function cerrarModalmodAsunto() {
        $("#modal_modAsunto").modal('hide');
    }

    function updateSeguridadRad(codigo) {

        //var transaccion = codigo == 0 ?  'AUTORIZAR' : 'DESAUTORIZAR';

        var paramsAcceso = {
            'radicado': radicado,
            'usuario_id' : usualog_id,
            'codigo' : codigo
        }

        //var RadicadoService.autorizacion(paramsAcceso);

        $.ajax({
            url: configsHasValue['WS_APPSERVICE_URL'] + '/radicado/seguridad/modificar',
            headers: {
                "Authorization": 'Bearer '+configsHasValue['WS_APPSERVICE_TOKEN']
            },
            type: 'POST',
            dataType: 'json',

            async: false,
            data:  paramsAcceso,
            success: function (response) {
                //alertModal('Actualizado!', capitalizarPrimeraLetra(response.tipo_entidad_aut) +' '+ response.accion_entidad +': '+retornarParteCadenaSplit(seleccion.text,'(',0), 'success', '5000');
                console.log('actualizo correctamente');
            },
            error: function (xhr, ajaxOptions, thrownError) {

                if(xhr.responseJSON.message){
                    Swal.fire("Error", xhr.responseJSON.message, "error");
                }else{
                    Swal.fire("Error", "Ha ocurrido un actualizar los datos", "error");
                }
            }
        });



        /*var data = {
            function_call: "updateOnlyRadSeg",
            radicado: radicado,
            codigo: codigo
        };
        $.ajax({
            url: "./app/seguridad/generalSeguridad.php",
            type: 'POST',
            dataType: 'json',
            data: data,
            async: false,
            success: function (response) {
                if (response.success != undefined) {
                } else {
                    //swal("Error", response.error, "error");
                }
            },
            error: function (response) {
                Swal.fire("Error", "Ha ocurrido un error al buscar los datos", "error");
            }
        });*/

    }

    function popoverPublico() {
        $(".divshiden").css('display', 'block');
        $(".bootstrap-switch-label").html('Público')
        $(".bootstrap-switch-label").attr('data-original-title', 'Quitar restricción')
        $(".bootstrap-switch-label").attr('data-content', 'Todos los usuarios tendrán acceso al documento')
        $(".bootstrap-switch-handle-off").html('Reservado')
        $("#btnLoadModalSegur").html('')
        $("#btnLoadModalSegur").html("<img src='imagenes/candado.jpg' alt='' border=0 width='25' height='25' >Reservado");
    }

    function popoverRestringido() {
        $(".divshiden").css('display', 'none');
        $(".bootstrap-switch-label").html('Restringir')
        $(".bootstrap-switch-label").attr('data-original-title', 'Restringir acceso al documento')
        $(".bootstrap-switch-label").attr('data-content', 'Solo tendrá acceso al documento el usuario actual ' +
            'y usuarios o dependencias autorizadas!')
        $(".bootstrap-switch-handle-off").html('Restringir')

        $("#btnLoadModalSegur").html('')
        $("#btnLoadModalSegur").html("<img src='imagenes/candado.jpg' alt='' border=0 width='25' height='25' >Público");
    }

    function borrarTodo() {

        var data = {
            function_call: "borrarTodoSeguridad",
            radicado: radicado,
        };
        $.ajax({
            url: "./app/seguridad/generalSeguridad.php",
            type: 'POST',
            dataType: 'json',
            data: data,
            async: false,
            success: function (response) {
                if (response.success != undefined) {
                    cargarModal()
                    Swal.fire("Felicidades", response.success, "success");
                } else {
                    Swal.fire("Error", response.error, "error");
                }
            },
            error: function (response) {
                Swal.fire("Error", "Ha ocurrido un error al actualizar los datos", "error");
            }
        });
    }

    function cargarPadre(){
        $.ajax({
            url: configsHasValue['WS_APPSERVICE_URL'] + '/radicado/getPrincRadbyRad/'+radicado,
            headers: {
                "Authorization": 'Bearer '+configsHasValue['WS_APPSERVICE_TOKEN']
            },
            type: 'GET',
            dataType: 'json',

            async: false,

            success: function (response) {

                console.log(usua_admin_sistema);
                if (((dependencia == response.objects.radi_depe_actu && usua_codi == response.objects.radi_usua_actu) && response.objects.sgd_trad_codigo != 2)  || usua_admin_sistema  > 0){

                    $('#modAsuntosec').html('<span class="fa-item"><a href="#" onclick="modalmodAsunto();"><i class="fa fa-pencil"></i> Modificar</a></span>');
                }

            },
            error: function (xhr, ajaxOptions, thrownError) {

                if(xhr.responseJSON.message){
                    if (xhr.responseJSON.Minlevel == "Error"){
                        Swal.fire(xhr.responseJSON.Minlevel, xhr.responseJSON.message, xhr.responseJSON.Minlevel.toLowerCase());
                    }
                    lastDigit = radicado % 10;
                    if ( lastDigit == 2 && usua_admin_sistema > 0){
                        $('#modAsuntosec').html('<span class="fa-item"><a href="#" onclick="modalmodAsunto();"><i class="fa fa-pencil"></i> Modificar</a></span>');
                    }
                }else{
                    Swal.fire("Error", "Ha ocurrido un error al buscar los datos", "error");
                }
            }
        });
    }

    function redirecModalexpediente(radicado){

        window.location.href = "./app/borradores/radicardesdeentrada.php?radicado="+radicado+","

    }

    (function ($) {
        continueOnChanceSwitch=false;
        //lo siguiente es el swiche de seguridad que aparece en el modal
        //de antes de radicar, el popover es dinamico para la opcion que esta deshabilitada
        $('#swich_seguridad').on('switchChange.bootstrapSwitch', function (event, state) {

            if (continueOnChanceSwitch == true) {


                seguridad_radicado = $('#swich_seguridad').bootstrapSwitch('state');
                if (seguridad_radicado == false) {
                    $("#labeldocpublico").html('Documento reservado, con restricción de acceso')
                    //lo puso a rstrigido
                    alertModal('Radicado Reservado', 'Acceso restringido', 'warning', 5000)
                    popoverPublico()
                    if (Object.keys(allSelecteds).length > 0) {
                        //@formatter:off
                        Swal.fire({
                            title: "Alerta",
                            text: "Desea conservar los usuarios y dependencias anteriores?",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonText: "SI",
                            cancelButtonText: "NO",
                            closeOnConfirm: true,
                            closeOnCancel: true
                        }).then((result)=> {
                            if(result.value
                    )
                        {
                            updateSeguridadRad(1)
                        }
                    else
                        if (
                            /* Read more about handling dismissals below */
                            result.dismiss === Swal.DismissReason.cancel
                        ) {
                            borrarTodo();
                        }
                    });
                        //@formatter:on

                    } else {

                        updateSeguridadRad(1)
                        popoverPublico()
                    }

                } else {
                    $("#labeldocpublico").html('Documento público, sin restricción de acceso')
                    alertModal('Radicado Público', 'Sin restricción de acceso', 'success', 5000)
                    //lo puso a publico, el radicado
                    updateSeguridadRad(0)
                    popoverRestringido()
                }

                $('.bootstrap-switch-label').attr('data-placement', 'top')
                $('.bootstrap-switch-label').attr('data-trigger', 'hover')
                $('.bootstrap-switch-label').attr('data-container', 'body')
                $('.bootstrap-switch-label').popover();
            }

        });
        // $('.bootstrap-switch-label') es la opcion que aparece en gris como deshabilitada
        $(".bootstrap-switch-label").html('Restringir')
        $(".bootstrap-switch-label").css('opacity', '0.5')

        guardarAllSelecteds(<?= json_encode($seguridadRad) ?>)

        cargarPadre();
    })(jQuery);

</script>
