<?php

namespace App\Helpers;


class PDOconfig extends \PDO
{
    private $engine;
    private $host;
    private $database;
    private $user;
    private $pass;

    public function __construct(){

        include '../../config.php';

        $this->engine = 'pgsql';
        $this->host = $dbhost;
        $this->database = $servicio;
        $this->user = $usuario;
        $this->pass = $contrasena;
        $dns = $this->engine.':dbname='.$this->database.";host=".$this->host;
        parent::__construct( $dns, $this->user, $this->pass );
    }
}