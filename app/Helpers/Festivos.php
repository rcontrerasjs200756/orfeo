<?php
namespace App\Helpers;

class Festivos {
    /**
     * Almacena la fecha del día actual.
     * @var Date
     */
    private $hoy;
    /**
     * Almacena festivos del año solicitado,
     * por defecto año en curso si el año
     * no es especificado.
     * @var Array
     */
    private $festivos;
    /**
     * Almacena el año solicitado,
     * por defecto año en curso
     * si el año no es
     * especificado.
     * @var Year
     */
    private $anio;
    /**
     * Almacena el Domingo de pascua
     * del año solicitado.
     * @var Day
     */
    private $pascuaDia;
    /**
     * Almacena el mes que corresponde
     * al mes de pascua del
     * año solicitado.
     * @var Month
     */
    private $pascuaMes;
    /**
     * Construye el objeto tipo Festivo.
     * @param string $anio Año para construir objeto,
     * por defecto es año actual
     * si año no es especificado.
     */


    public function __construct($anio = '')
    {
        $this->setFestivos($anio);
    }

    /**
     * Obtener miembros de la clase.
     * @param member Atributo de la clase Festivos.
     * @return member Contenido de atributo solicitado.
     */
    public function __get( $member )
    {
        return $this->$member;
    }
    /**
     * Construye el arreglo que contendrá los
     * festivos del año en curso, o el año
     * solicitado.
     * @param string $anio Año para construir objeto,
     * por defecto es año actual
     * si año no es especificado.
     * @return null
     */
    public function setFestivos($anio)
    {
// Obtener fecha actual.
        $this->hoy = date('d/m/Y');
// Si año no es proporcionado,
// se asigna a $anio año actual.
        $anio = ($anio == '' ? $anio = date('Y'): $anio);
        $this->anio=$anio;
        $this->pascuaMes=date("m", easter_date($this->anio));
        $this->pascuaDia=date("d", easter_date($this->anio));
        $this->festivos[$anio][1][1] = true; // Primero de Enero
        $this->festivos[$anio][5][1] = true; // Dia del Trabajo 1 de Mayo
        $this->festivos[$anio][7][20] = true; // Independencia 20 de Julio
        $this->festivos[$anio][8][7] = true; // Batalla de Boyacá 7 de Agosto
        $this->festivos[$anio][12][8] = true; // Maria Inmaculada 8 diciembre
        $this->festivos[$anio][12][25] = true; // Navidad 25 de diciembre
//Fechas móviles de primer lunes:
        $this->calculaEmiliani(1, 6); // Reyes Magos Enero 6
        $this->calculaEmiliani(3, 19); // San Jose Marzo 19
        $this->calculaEmiliani(6, 29); // San Pedro y San Pablo Junio 29
        $this->calculaEmiliani(8, 15); // Asunción Agosto 15
        $this->calculaEmiliani(10, 12); // Descubrimiento de América Oct 12
        $this->calculaEmiliani(11, 1); // Todos los santos Nov 1
        $this->calculaEmiliani(11, 11); // Independencia de Cartagena Nov 11
// Fechas calculadas a partir
// de fecha de pascua:
        $this->otrasFechasCalculadas(-3); // Jueves santo, 3 días antes de Domingo Pascua

        $this->otrasFechasCalculadas(-2); // Viernes santo, 2 días antes de Domingo Pascua
        $this->otrasFechasCalculadas(43, true); // Ascención de jesús, séptimo Lunes despúes de Domingo Pascua
        $this->otrasFechasCalculadas(64, true); // Corpus Cristi, décimo Lunes despúes de Domingo Pascua
        $this->otrasFechasCalculadas(71, true); // Sagrado Corazón, undécimo Lunes después de Domingo Pascua
    }
    /**
     * Recalcula la fecha de un festivo, si
     * el día festivo es un día diferente
     * a lunes.
     * @param Month $mesFestivo Mes del día festivo.
     * @param Day $diaFestivo Día del día festivo.
     * @return null
     */
    private function calculaEmiliani($mesFestivo, $diaFestivo)
    {
        $numDia = date("w",mktime(0, 0, 0, $mesFestivo, $diaFestivo, $this->anio));
        switch ($numDia) {
            case 0: // Domingo
                $diaFestivo = $diaFestivo + 1;
                break;
            case 2: // Martes.
                $diaFestivo = $diaFestivo + 6;
                break;
            case 3: // Miércoles
                $diaFestivo = $diaFestivo + 5;
                break;
            case 4: // Jueves
                $diaFestivo = $diaFestivo + 4;
                break;
            case 5: // Viernes
                $diaFestivo = $diaFestivo + 3;
                break;
            case 6: // Sábado
                $diaFestivo = $diaFestivo + 2;
                break;
            default:
                break;
        }
        $mes = date("n", mktime(0,0,0,$mesFestivo,$diaFestivo,$this->anio))+0;
        $dia = date("d", mktime(0,0,0,$mesFestivo,$diaFestivo,$this->anio))+0;
        $this->festivos[$this->anio][$mes][$dia] = true;
    }
/**
 * Calcula días relacionados a Domingo de Pascua,
 * Jueves Santo, Viernes Santo, Ascención,
 * Corpus Cristi, Sagrado Corazón.
 * * @param integer $cantidadDias Cantidad de días después de Domingo de Pascua.
 * @param boolean $siguienteLunes Verdadero si debe calcularse Fecha Emiliani.
 * @return null
 */
    private function otrasFechasCalculadas($cantidadDias=0,$siguienteLunes=false)
    {
        $mesFestivo = date("n", mktime(0, 0, 0,$this->pascuaMes, $this->pascuaDia+$cantidadDias, $this->anio));
        $diaFestivo = date("d", mktime(0, 0, 0,$this->pascuaMes, $this->pascuaDia+$cantidadDias, $this->anio));
        if ($siguienteLunes)
        {
            $this->calculaEmiliani($mesFestivo, $diaFestivo);
        }
        else
        {
            $this->festivos[$this->anio][$mesFestivo+0][$diaFestivo+0] = true;
        }
    }
    /**
     * Evalua si dia y mes corresponden
     * a una fecha de un festivo.
     * @param Day $dia Día de fecha festiva
     * @param Month $mes Mes de fecha festiva
     * @return null
     */
    public function esFestivo($dia,$mes)
    {
        if($dia == '' or $mes == '')
        {
            return false;
        }
        if (isset($this->festivos[$this->anio][(int)$mes][(int)$dia]))
        {
            return true;
        }
        else
        {
            return FALSE;
        }
    }
    /**
     * Muestra un listado de las
     * fecha festivas creadas.
     * @return html
     */
    public function getFestivos()
    {
        print '<pre>';
        foreach ($this->festivos as $festivo) {
            print_r($festivo);
        }
        print '</pre>';
    }
    /**
     * Calcula una fecha hábil siguiente, dada una
     * fecha inicial y un número de días hábiles.
     * @param Date $fecha Fecha inicial de cálculo de días hábiles
     * @param Int $diasHabiles Número de días hábiles que se debe sumar a fecha inicial
     * @return Date $fecha Fecha con días hábiles sumados
     */
    public function diasHabiles($fecha, $diasHabiles)
    {

// Digito que corresponde al día de la semana,
// 0 -> Domingo, 1 -> Lunes, ... 6 -> Sábado.
        $numDia = date('w', strtotime($fecha));
// Extraer día de la fecha.
        $dia = date("j",strtotime($fecha));
// Extraer mes de la fecha.
        $mes = date("n",strtotime($fecha));
// Contador par comparar contra días hábiles.
        $i = 0;
        while($i < $diasHabiles) {
// Mientras el contador $i sea menor que el número
// de días hábiles a contar, hacer..
            if($numDia == 6 || $numDia == 0) {
// Si día es Sábado o Domingo, sumar un día y agregar
// un día más al número de días hábiles.
                $fecha = date("Y-m-d", strtotime($fecha . '+1 day'));
                $numDia = date('w', strtotime($fecha));
                $dia = date("j",strtotime($fecha));
                $mes = date("n",strtotime($fecha));
                $diasHabiles ++;
                $i++;
            }
            else {
                if($this->esFestivo($dia, $mes))
                {
// Si el día es festivo se debe ignorar.
                    $fecha = date("Y-m-d", strtotime($fecha . '+1 day'));
                    $numDia = date('w', strtotime($fecha));
                    $dia = date("j",strtotime($fecha));
                    $mes = date("n",strtotime($fecha));

                    $diasHabiles ++;
                    $i++;
                }
                else
                {
                    $fecha = date("Y-m-d", strtotime($fecha . '+1 day'));
                    $numDia = date('w', strtotime($fecha));
                    $dia = date("j",strtotime($fecha));
                    $mes = date("n",strtotime($fecha));
                    $i++;
                }
            }
        }

        $numDia = date('w', strtotime($fecha));
// Verificar que la fecha retornada, después
// de que $i alcance número días hábiles, no
// sea fin de semana. Si lo es se agregan
// los días correspondientes.
        switch ($numDia) {
            case 0:
                $fecha = date("Y-m-d", strtotime($fecha . '+1 day'));
                break;
            case 6:
                $fecha = date("Y-m-d", strtotime($fecha . '+2 day'));
                break;
            default:
                $fecha = ($this->esFestivo($dia, $mes) ? date("Y-m-d", strtotime($fecha . '+1 day')) : $fecha);
                break;
        }
// Verificar que la fecha final
// no se festiva, si lo es
// sumamos un día más.
        $dia = date("j",strtotime($fecha));
        $mes = date("n",strtotime($fecha));
        $fecha = ($this->esFestivo($dia, $mes) ? date("Y-m-d", strtotime($fecha . '+1 day')) : $fecha);
        return $fecha;
    }

    /*
     *valida si el dia de la fecha recibida, es domingo o el sabado
     * */
    public function esFinDeSemana($fecha){
        // Digito que corresponde al día de la semana,
        // 0 -> Domingo, 1 -> Lunes, ... 6 -> Sábado.
        $numDia = date('w', strtotime($fecha));
        if($numDia==0 || $numDia==6){
            return true;
        }
        return false;
    }
}