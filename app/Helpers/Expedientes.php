<?php

namespace App\Helpers;

//error_reporting(E_ALL);

use Carbon\Carbon;

class Expedientes
{
    protected $query_factory;
    protected $pdo;
    protected $user;

    public function __construct($db = array())
    {
        //al manejar varios helper, arroja error al redeclarar lasconexion a la bd, por eso si existe la varible db, es porque le estoy pasando
        //los datos de conexion especifica y no vuelva a redeclarar

        session_start();
        require '../../vendor/autoload.php';
        $this->user = $_SESSION;

        include_once "../../config.php";

        if (count($db) < 1) {
            include_once "../../include/db/ConnectionHandlerNew.php";

            $this->db = new \ConnectionHandler("../..");
            $this->db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        } else {
            $this->db = $db;
        }
    }

    public function cantExpedByRadic($radicado)
    {
        try {
            //trae la cantidad de expedientes que tiene este radicado

            $query = "SELECT e.radi_nume_radi AS Radicado, count(1) AS Expedientes 
FROM sgd_exp_expediente e
WHERE e.radi_nume_radi=".$radicado." 
AND e.sgd_exp_estado!=2  --Diferente de excluido.
GROUP BY e.radi_nume_radi
ORDER BY 2 DESC";
            return $this->db->conn->selectLimit($query, 100000)->getArray();

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    function getExpedByRad($radicado){
        try {
            //trae los expedientes de este radicado

            $query = "SELECT se.sgd_exp_numero AS Expediente, se.sgd_sexp_parexp1 AS Titulo 
FROM sgd_exp_expediente e INNER JOIN sgd_sexp_secexpedientes se ON e.sgd_exp_numero=se.sgd_exp_numero 
WHERE e.radi_nume_radi=".$radicado."
AND e.sgd_exp_estado!=2  --Diferente de excluido.
ORDER BY se.sgd_sexp_fech DESC
";
            return $this->db->conn->selectLimit($query, 100000)->getArray();

        } catch (\Exception $e) {
            return $e->getMessage();
        }

    }

    public function getInfExpediente($expediente)
    {
        try {
            //trae la info del expediente seleccionado

            $query = "SELECT 
  sexp.sgd_sexp_parexp1  					AS Titulo,
  sexp.sgd_sexp_parexp2  					AS Descripcion,
  sexp.sgd_sexp_parexp3 
  ||' ' || sexp.sgd_sexp_parexp4
  ||' ' || sexp.sgd_sexp_parexp5 				AS Otros_Datos,
 (CASE WHEN sgd_sexp_estado=0 THEN 'Abierto'
       WHEN sgd_sexp_estado=1 THEN 'Cerrado' 
  END) 								AS Estado,
  sexp.sgd_srd_codigo || '.' || s.sgd_srd_descrip 		AS Serie,
  sexp.sgd_sbrd_codigo 	|| '.' || sb.sgd_sbrd_descrip 	AS Subserie,
  sexp.depe_codi ||'-'|| d.depe_nomb				AS Proceso, --Dependencia
  ur.usua_nomb 						AS Responsable,
  u.usua_nomb 							AS Creado_Por,
  to_char(sexp.sgd_sexp_fech,'DD/MM/YYYY HH24:MI')    	AS Fecha_Creacion
FROM sgd_sexp_secexpedientes sexp
INNER JOIN usuario u ON sexp.usua_doc=u.usua_doc
INNER JOIN usuario ur ON sexp.usua_doc_responsable=ur.usua_doc
INNER JOIN dependencia d ON sexp.depe_codi=d.depe_codi
INNER JOIN sgd_srd_seriesrd s ON  s.sgd_srd_codigo=sexp.sgd_srd_codigo
INNER JOIN sgd_sbrd_subserierd sb ON sb.sgd_srd_codigo=s.sgd_srd_codigo AND sb.sgd_sbrd_codigo=sexp.sgd_sbrd_codigo
WHERE sgd_sexp_estado !=2 
AND sexp.sgd_exp_numero = '".$expediente."'

";
            return $this->db->conn->selectLimit($query, 1)->getArray();

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    function tablaprinciexped($expediente,$start = false, $limit = false,$buscar=false){


        $query = "SELECT r.radi_depe_actu,
  e.radi_nume_radi     	AS RADICADO,
  r.radi_path,
  r.ra_asun            	AS ASUNTO,
  r.radi_fech_radi     	AS FECHA_RADICADO,
  (CASE WHEN r.sgd_eanu_codigo=2 THEN 'Anulado'
	WHEN r.sgd_eanu_codigo=1 THEN 'En solicitu de de Anulación'
	WHEN r.radi_depe_actu=999 THEN 'Finalizado'
	ELSE cast(u.usua_nomb AS Text) || ' ('||r.radi_depe_actu||')'
  End)			AS DEPENDENCIA_ACTUAL
FROM
  sgd_sexp_secexpedientes sexp
INNER JOIN sgd_exp_expediente e ON sexp.sgd_exp_numero=e.sgd_exp_numero
INNER JOIN radicado r ON e.radi_nume_radi=r.radi_nume_radi
INNER JOIN usuario u ON r.radi_usua_actu=u.usua_codi AND r.radi_depe_actu=u.depe_codi
";
        $query .= " Where sexp.sgd_exp_numero='".$expediente."'
And ( lower(CAST(e.radi_nume_radi  as text)) like('%".$buscar."%') 
or lower(CAST(r.radi_depe_actu  as text)) like('%".$buscar."%') 
Or lower(r.radi_path)  like lower('%".$buscar."%') 
Or lower(r.ra_asun)  like lower('%".$buscar."%') 
Or to_char(r.radi_fech_radi,'YYYY-MM-DD') like lower('%".$buscar."%')) ";

        $query .= " ORDER BY r.radi_fech_radi DESC ";

        if ($limit != false) {
            return $this->db->conn->SelectLimit($query, $limit, $start)->getArray();
        } else {
            return $this->db->conn->SelectLimit($query)->getArray();
        }
    }

    function tablahistorial($expediente,$start = false, $limit = false,$buscar=false){
        $query = "SELECT 
to_char(he.sgd_hfld_fech,'DD-MM-YYYY HH24:MI')	AS FECHA,
u.usua_nomb					AS USUARIO,
he.depe_codi,
d.depe_nomb,
he.depe_codi || '-'||d.depe_nomb		AS DEPENDENCIA,
sgd_ttr_descrip					AS TRANSACCION,
he.radi_nume_radi				AS RADICADO,
he.sgd_hfld_observa				AS OBSERVACION
  FROM sgd_hfld_histflujodoc he
  INNER JOIN dependencia d ON he.depe_codi=d.depe_codi
  INNER JOIN usuario u	ON u.usua_doc=he.usua_doc
  INNER JOIN sgd_ttr_transaccion t ON he.sgd_ttr_codigo=t.sgd_ttr_codigo
";
        $query .= " Where he.sgd_exp_numero='".$expediente."'
And ( lower(CAST(he.radi_nume_radi  as text)) like('%".$buscar."%') 
or lower(CAST(he.depe_codi  as text)) like('%".$buscar."%') 
Or lower(u.usua_nomb)  like lower('%".$buscar."%') 
Or lower(d.depe_nomb)  like lower('%".$buscar."%') 
Or lower(sgd_ttr_descrip)  like lower('%".$buscar."%') 
Or lower(he.sgd_hfld_observa)  like lower('%".$buscar."%') 
Or to_char(he.sgd_hfld_fech,'YYYY-MM-DD') like lower('%".$buscar."%')) ";

        $query .= " ORDER BY he.sgd_hfld_fech DESC ";

        if ($limit != false) {
            return $this->db->conn->SelectLimit($query, $limit, $start)->getArray();
        } else {
            return $this->db->conn->SelectLimit($query)->getArray();
        }
    }

    function tablaradselec($radicado,$start = false, $limit = false,$buscar=false){
        $query = "Select r.radi_nume_radi, r.radi_fech_radi, r.ra_asun, r.radi_path,
  (CASE WHEN r.sgd_eanu_codigo=2 THEN 'Anulado'
	WHEN r.sgd_eanu_codigo=1 THEN 'En solicitu de de Anulación'
	WHEN r.radi_depe_actu=999 THEN 'Finalizado'
	ELSE cast(u.usua_nomb AS Text) || ' ('||r.radi_depe_actu||')'
  End)    AS DEPENDENCIA_ACTUAL
From radicado r
INNER JOIN usuario u ON r.radi_usua_actu=u.usua_codi AND r.radi_depe_actu=u.depe_codi
Where r.radi_nume_radi In (
Select radi_nume_deri From radicado Where radi_nume_radi=".$radicado."
UNION
Select radi_nume_radi From radicado Where radi_nume_deri=".$radicado."
UNION
Select radi_nume_salida from anexos where anex_radi_nume=".$radicado." and radi_nume_salida is not null 
and radi_nume_salida!=".$radicado."
UNION
Select anex_radi_nume from anexos where radi_nume_salida=".$radicado." and radi_nume_salida is not null
 and anex_radi_nume!=".$radicado."
)
";
        $query .= "
And ( lower(CAST(r.radi_nume_radi  as text)) like('%".$buscar."%') 
Or lower(r.ra_asun)  like lower('%".$buscar."%') 
Or lower(r.radi_path)  like lower('%".$buscar."%') 
Or to_char(r.radi_fech_radi,'YYYY-MM-DD') like lower('%".$buscar."%')) ";

        if ($limit != false) {
            return $this->db->conn->SelectLimit($query, $limit, $start)->getArray();
        } else {
            return $this->db->conn->SelectLimit($query)->getArray();
        }
    }

    function escluirRadExped($radicado,$expediente){


        try {
            //excluye un radicado de un expediente
            $query = "
UPDATE sgd_exp_expediente  Set sgd_exp_estado='2'
WHERE sgd_exp_estado!=2
AND radi_nume_radi=".$radicado." 
AND sgd_exp_numero='".$expediente."'
";
            $guadar = $this->db->conn->execute($query);

            return $guadar;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function tienePermExcluir($radicado,$expediente){

        try {
            //trae la informacion de un expediente para luego validar si tiene permiso o no para excluirlo

            $query = "SELECT e.radi_nume_radi AS Radicado, e.sgd_exp_numero AS Expediente, e.sgd_exp_estado AS Estado, u.usua_doc, u.usua_nomb AS Usuario, se.depe_codi
FROM sgd_exp_expediente e 
INNER JOIN sgd_sexp_secexpedientes se ON e.sgd_exp_numero=se.sgd_exp_numero 
INNER JOIN usuario u ON e.usua_doc=u.usua_doc
WHERE e.sgd_exp_estado!=2  
AND e.radi_nume_radi=".$radicado."
AND e.sgd_exp_numero='".$expediente."'
";
            //--0=incluido  2=excluido

            return $this->db->conn->selectLimit($query, 1)->getArray();

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    //todos los expedientes de un radicado, sgd_exp_estado!=2
    function getExpedNotDeleted($radicado,$expediente=false)
    {
        try {
            $query = "SELECT  *
                FROM sgd_exp_expediente 
                where radi_nume_radi=" . $radicado . " ";
            if($expediente!=false){
                $query .= " and sgd_exp_numero='".$expediente."' ";
            }
            $query .= " AND sgd_exp_estado!=2  ORDER BY sgd_exp_fech ASC";

            $borraExped = $this->db->conn->selectLimit($query, 100000)->getArray();
            return $borraExped;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    //todos los expedientes de un radicado o un expediente especifico por radicado
    function getExpedientes($radicado,$expediente=false)
    {
        try {
            $query = "SELECT  *
                FROM sgd_exp_expediente 
                where radi_nume_radi=" . $radicado . " ";
            if($expediente!=false){
                $query .= " and sgd_exp_numero='".$expediente."' ";
            }
            $query .= " ORDER BY sgd_exp_fech ASC";

            $borraExped = $this->db->conn->selectLimit($query, 100000)->getArray();
            return $borraExped;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    function getAnexosByRadicado($numeradi){
        //trae todos los anexos  ordenados por fecha de creacion del anexos asc
        $query = "Select id,anex_ubic,anex_radi_nume, radi_nume_salida, anex_codigo, 
anex_fech_anex,anex_radi_fech,anex_depe_creador,anex_nomb_archivo,anex_desc,
                    anex_depe_creador
                    From anexos Where  anex_borrado='N' and
                    anex_radi_nume=$numeradi order by anex_numero asc";

        return $this->db->conn->SelectLimit($query, 20000)->getArray();
    }

    /*verifica si varios radicados estan en x expediente*/
    public function getRadicadosXexped($radicados,$expediente){

        try {

            $query="SELECT radi_nume_radi,COUNT(*) AS contadorporradicado
FROM sgd_exp_expediente 
WHERE 
    sgd_exp_estado!=2 AND sgd_exp_numero='".$expediente."' and radi_nume_radi IN 
        (".$radicados.")
GROUP BY radi_nume_radi";


            $borraExped = $this->db->conn->selectLimit($query, 100000)->getArray();
            return $borraExped;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /*trae las tipologias por expedientes*/
    public function tipolByExped($expedientes,$buscar){

        try {


            $query="
SELECT distinct m.sgd_tpr_codigo, td.sgd_tpr_descrip
       FROM sgd_mrd_matrird m
       INNER JOIN sgd_sexp_secexpedientes se ON m.sgd_srd_codigo=se.sgd_srd_codigo AND m.sgd_sbrd_codigo=se.sgd_sbrd_codigo AND se.depe_codi=m.depe_codi
       INNER JOIN sgd_tpr_tpdcumento td ON m.sgd_tpr_codigo=td.sgd_tpr_codigo
       WHERE m.sgd_mrd_esta In ('0','1')
       AND se.sgd_exp_numero IN  (".$expedientes.")
       AND  TRANSLATE(td.sgd_tpr_descrip,'ÁÉÍÓÚáéíóú','AEIOUaeiou') ilike translate('%" . $buscar . "%','ÁÉÍÓÚáéíóú','AEIOUaeiou')
       ORDER BY td.sgd_tpr_descrip ASC
    
";

            $buscarTip = $this->db->conn->selectLimit($query, 100000)->getArray();
            return $buscarTip;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function tipolByExpedAndRad($expedientes,$radicados,$buscar){

        try {

            $query="
       Select distinct * From (
               SELECT distinct m.sgd_tpr_codigo, td.sgd_tpr_descrip
               FROM sgd_mrd_matrird m
               LEFT JOIN sgd_sexp_secexpedientes se ON m.sgd_srd_codigo=se.sgd_srd_codigo AND m.sgd_sbrd_codigo=se.sgd_sbrd_codigo AND se.depe_codi=m.depe_codi
               LEFT JOIN sgd_tpr_tpdcumento td ON m.sgd_tpr_codigo=td.sgd_tpr_codigo
               WHERE m.sgd_mrd_esta In ('0','1')
               AND (se.sgd_exp_numero IN  (".$expedientes.") )
               AND  TRANSLATE(td.sgd_tpr_descrip,'ÁÉÍÓÚáéíóú','AEIOUaeiou') ilike translate('%" . $buscar . "%','ÁÉÍÓÚáéíóú','AEIOUaeiou')
         UNION ALL
               SELECT distinct td.sgd_tpr_codigo, td.sgd_tpr_descrip
               FROM sgd_tpr_tpdcumento td
               LEFT JOIN radicado r ON r.tdoc_codi=td.sgd_tpr_codigo
               WHERE r.radi_nume_radi IN (".$radicados.")
               AND   TRANSLATE(td.sgd_tpr_descrip,'ÁÉÍÓÚáéíóú','AEIOUaeiou') ilike translate('%" . $buscar . "%','ÁÉÍÓÚáéíóú','AEIOUaeiou')
               AND td.sgd_tpr_codigo !=0
       ) AS  tipologias
       ORDER BY sgd_tpr_descrip ASC";



            $buscarTip = $this->db->conn->selectLimit($query, 100000)->getArray();
            return $buscarTip;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    function getResponsables($dependenciaSeleccionada,$buscar){
        try {

            $query="SELECT id, usua_doc, usua_codi, depe_codi, usua_nomb, 
(Case When usua_codi=1 Then '(Jefe)' Else '' End) As orden
FROM usuario 
WHERE depe_codi=".$dependenciaSeleccionada."  
 AND  ( TRANSLATE(usua_nomb,'ÁÉÍÓÚáéíóú','AEIOUaeiou') ilike translate('%" . $buscar . "%','ÁÉÍÓÚáéíóú','AEIOUaeiou')
  or TRANSLATE(usua_doc,'ÁÉÍÓÚáéíóú','AEIOUaeiou') ilike translate('%" . $buscar . "%','ÁÉÍÓÚáéíóú','AEIOUaeiou')
  )
ORDER BY orden desc, usua_nomb
";


            $getResponsables = $this->db->conn->selectLimit($query, 100000)->getArray();
            return $getResponsables;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    function getTituloExpediente($TituloIngresado){
        try {

            $query="SELECT TRIM(sgd_sexp_parexp1), sgd_exp_numero FROM sgd_sexp_secexpedientes 
WHERE TRANSLATE(TRIM(sgd_sexp_parexp1),'ÑñÁÉÍÓÚáéíóú','NnAEIOUaeiou')=
translate('".$TituloIngresado."','ÑñÁÉÍÓÚáéíóú','NnAEIOUaeiou')
";


            $getTituloExpediente = $this->db->conn->selectLimit($query, 1)->getArray();
            return $getTituloExpediente;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}

