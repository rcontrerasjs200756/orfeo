<?php

namespace App\Helpers;

//error_reporting(E_ALL);

use Carbon\Carbon;
use DOMDocument;
use mPDF;

class Borradores
{
    protected $query_factory;
    protected $pdo;
    protected $user;

    public function __construct($db = array(), $rutaraiz = "../..")
    {
        //al manejar varios helper, arroja error al redeclarar lasconexion a la bd, por eso si existe la varible db, es porque le estoy pasando
        //los datos de conexion especifica y no vuelva a redeclarar

        if (!isset($_SESSION['usuario_id'])) {
            session_start();
        }
        require $rutaraiz . '/vendor/autoload.php';
        $this->user = $_SESSION;

        include_once "$rutaraiz/config.php";

        if (count($db) < 1) {
            include_once "$rutaraiz/include/db/ConnectionHandlerNew.php";

            $this->db = new \ConnectionHandler($rutaraiz);
            $this->db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        } else {
            $this->db = $db;
        }
    }

    public function get_all($buscar, $limit, $start, $order, $order_dir, $radicado = false, $estadoeliminado = '',
                            $estadocomentado = '')
    {
        try {

            $codusuario = $_SESSION["usuario_id"];

            $query = "select b.*,
 borrador_estados.nombre_estado,usuario.usua_login
    from borradores  b
join borrador_estados on (borrador_estados.id_borrador_estado=b.id_estado) ";
            if ($radicado == false) {
                $query .= "JOIN (select DISTINCT ON (borrador_historicos.id_borrador) borrador_historicos.id_borrador
	  FROM borrador_historicos where (id_usuario_origen='" . $codusuario . "' or id_usuario_destino='" . $codusuario . "' ) )
	   historico ON (historico.id_borrador = b.id_borrador) ";
            } else {
                $query .= "join borrador_radicados_asociados on (borrador_radicados_asociados.id_borrador=b.id_borrador) ";
            }
            $query .= "left join usuario on (usuario.id=b.id_usuario_anterior  )
 where 
    b.id_estado<>7";

            if ($estadoeliminado != '') {
                $query .= " and  b.id_estado<>" . $estadoeliminado;
            }

            if ($estadocomentado != '') {
                $query .= " and  b.id_estado<>" . $estadocomentado;
            }

            if ($radicado != false) {
                $query .= " and borrador_radicados_asociados.radicado_asociado='" . $radicado . "' ";
            }
            $query .= "  And ( 
TRANSLATE(b.asunto,'ÁÉÍÓÚáéíóú','AEIOUaeiou') ilike translate('%" . $buscar . "%','ÁÉÍÓÚáéíóú','AEIOUaeiou')
Or TRANSLATE(borrador_estados.nombre_estado,'ÁÉÍÓÚáéíóú','AEIOUaeiou') ilike translate('%" . $buscar . "%','ÁÉÍÓÚáéíóú','AEIOUaeiou') 
Or TRANSLATE(b.destinatario,'ÁÉÍÓÚáéíóú','AEIOUaeiou') ilike translate('%" . $buscar . "%','ÁÉÍÓÚáéíóú','AEIOUaeiou')
Or TRANSLATE(usuario.usua_login,'ÁÉÍÓÚáéíóú','AEIOUaeiou') ilike translate('%" . $buscar . "%','ÁÉÍÓÚáéíóú','AEIOUaeiou')
Or b.id_borrador::text ilike '%" . $buscar . "%'
) order by  ";


            if ($order != false) {
                $query .= $order . ' ' . $order_dir;
            } else {
                $query .= "b.id_borrador DESC";
            }

            if ($limit != false) {
                return $this->db->conn->SelectLimit($query, $limit, $start)->getArray();
            } else {
                return $this->db->conn->SelectLimit($query, 10000)->getArray();
            }



        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    function getAllByRadicado($radicadoid, $buscar, $limit, $start)
    {

        try {
            $query = "select DISTINCT ON (borrador_historicos.id_borrador) borrador_historicos.id_borrador, borrador_historicos.*,
borradores.nombre_persona, borradores.documento_ruta, borradores.destinatario, borradores.id_estado, borradores.id_borrador,
borradores.asunto,
borradores.id_usuario_anterior, borrador_estados.nombre_estado,usuario.usua_login,borradores.tipo_borrador_dest,
borrador_radicados_asociados.id_borrador as id_borrador_asociado, borrador_radicados_asociados.radicado_asociado,
borradores.tipo_borrador_dest,borradores.documento_ruta,borradores.id_usuario_actual,borradores.dependencia_usu_actual,
 borradores.documento_formato
  from borrador_historicos 
join borradores on (borradores.id_borrador=borrador_historicos.id_borrador)
join borrador_estados on (borrador_estados.id_borrador_estado=borradores.id_estado)
join borrador_radicados_asociados on (borrador_radicados_asociados.id_borrador=borradores.id_borrador)
left join usuario on (usuario.id=borradores.id_usuario_anterior  )
 where borrador_radicados_asociados.radicado_asociado='" . $radicadoid . "' and borradores.id_estado<>7 
 And ( 
TRANSLATE(borradores.asunto,'ÁÉÍÓÚáéíóú','AEIOUaeiou') ilike translate('%" . $buscar . "%','ÁÉÍÓÚáéíóú','AEIOUaeiou')
Or TRANSLATE(borrador_estados.nombre_estado,'ÁÉÍÓÚáéíóú','AEIOUaeiou') ilike translate('%" . $buscar . "%','ÁÉÍÓÚáéíóú','AEIOUaeiou') 
Or TRANSLATE(borradores.destinatario,'ÁÉÍÓÚáéíóú','AEIOUaeiou') ilike translate('%" . $buscar . "%','ÁÉÍÓÚáéíóú','AEIOUaeiou')
Or TRANSLATE(usuario.usua_login,'ÁÉÍÓÚáéíóú','AEIOUaeiou') ilike translate('%" . $buscar . "%','ÁÉÍÓÚáéíóú','AEIOUaeiou')
) ";

            if ($limit != false) {
                return $this->db->conn->SelectLimit($query, $limit, $start)->getArray();
            } else {
                return $this->db->conn->SelectLimit($query, 10000)->getArray();
            }

        } catch (\Exception $e) {
            return $e->getMessage();
        }

    }

    //reemplaza las comillas " por “”  y las ' por ‘’
    function reemplazarcomillas($cadena, $hacertrim = true, $hacersubstring = false)
    {


        if ($cadena == null) return 'NULL';

        $abrirsimple = true;
        $abrirdoble = true;
        $texto = is_string($cadena) ? "'" : (int)$cadena;
        if (is_string($cadena)) {
            for ($i = 0; $i < strlen($cadena); $i++) {
                $a = $cadena[$i];

                if (strpos($cadena[$i], "'") !== false) {
                    if ($abrirsimple == true) {
                        $a = str_replace("'", "‘", $cadena[$i]);
                        $abrirsimple = false;
                    } else {
                        $a = str_replace("'", "’", $cadena[$i]);
                        $abrirsimple = true;
                    }
                }

                if (strpos($cadena[$i], '"') !== false) {
                    if ($abrirdoble == true) {
                        $a = str_replace('"', "“", $cadena[$i]);
                        $abrirdoble = false;
                    } else {
                        $a = str_replace('"', "”", $cadena[$i]);
                        $abrirdoble = true;
                    }
                }
                $texto .= $a;
            }
        } else {
            $texto = $cadena;
        }

        if ($hacertrim == true) {
            $texto = trim($texto);
        }

        if ($hacersubstring) {
            $texto = substr($texto, 0, $hacersubstring);
        }

        //cuando es string, la cadena +0 es igualo a 0; contrario a cuando es entero
        $texto = is_string($cadena) ? $texto . "'" : (int)$cadena;

        return $texto;

    }

    public function updatecada60helper($datos)
    {
        try {

            $folios_comunicacion = $datos["folios_comunicacion_raiz"] != "" ? trim($datos["folios_comunicacion_raiz"]) : NULL;
            $folios_anexos = $datos["folios_anexos_raiz"] != "" ? $datos["folios_anexos_raiz"] : NULL;
            $descripcion_anexos = $datos["desc_anexos_raiz"] != "" ? $datos["desc_anexos_raiz"] : NULL;
            $editor = trim($datos["editoroplantilla"]);

            $tipo_destinatario = $datos["tipo_destinatario"];
            $tipo_radicado = $datos["tipo_radicado"];

            $dependencia = $_SESSION['dependencia'];
            $hacertrim = false;
            $tipopersona = NULL;

            if (isset($datos['tipopersona']) && $datos['tipopersona'] != "" && $datos['tipopersona'] != null) {
                $tipopersona = $datos['tipopersona'];
            }

            $persona = NULL;
            if (isset($datos['persona']) && $datos['persona'] != "" && $datos['persona'] != null) {
                $persona = $datos['persona'];
            }

            $datos['asunto'] = $this->reemplazarcomillas($datos['asunto']);

            $query = "update borradores
            set folios_comunicacion=" . $this->reemplazarcomillas($folios_comunicacion) . ", folios_anexos=" . $this->reemplazarcomillas($folios_anexos) . ",
             descripcion_anexos=" . $this->reemplazarcomillas($descripcion_anexos) . ",
             nombre_persona=" . $this->reemplazarcomillas($persona) . ", tipopersona=" . $this->reemplazarcomillas($tipopersona) . " , cargo_persona=" . $this->reemplazarcomillas($datos['cargo']) . ",
               asunto=" . $datos['asunto'] . ", tipo_borrador_dest=" . $this->reemplazarcomillas($datos['tipo_borrador_dest']) . ", ";

            $query .= "documento_formato=" . $this->reemplazarcomillas($editor) . "  ";

            if ($datos["tipo_destinatario"] != "") {

                $query .= ", tipo_destinatario=" . $this->reemplazarcomillas($tipo_destinatario);
                $query .= ", tipo_radicado=" . $this->reemplazarcomillas($tipo_radicado);
            }

            if (isset($datos['documento_ruta'])) {

                $query .= ",documento_ruta=" . $this->reemplazarcomillas($datos['documento_ruta']) . " ";

            }

            if ($datos["tipo_destinatario"] != "") {
                $query .= ",id_destinatario=" . $this->reemplazarcomillas($datos["id_destinatario"]) . ", 
                 destinatario=" . $this->reemplazarcomillas($datos["dest"]) . " ";
            }

            $query .= " where id_borrador=" . $this->reemplazarcomillas($datos['borrador_id']) . " ";


            if($datos['optSelectedNewBorrador']!=$_SESSION['SALIDA_OFICIO']){
                $this->actualizaCargoUsuario($datos["cargo"],$datos["id_destinatario"]);
            }

            //executing
            $response = $this->db->conn->execute($query);
            return $response;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    function nuevasComillas()
    {

    }

    public function save_all($post)
    {
        try {


            if ($post['borrador_id'] == "") {

                $folios_comunicacion = $post["folios_comunicacion_raiz"] != "" ? trim($post["folios_comunicacion_raiz"]) : NULL;
                $folios_anexos = $post["folios_anexos_raiz"] != "" ? trim($post["folios_anexos_raiz"]) : NULL;
                $descripcion_anexos = $post["desc_anexos_raiz"] != "" ? $post["desc_anexos_raiz"] : NULL;
                $tipo_comunicacion = $post["selecttipocomunicacion"] != "" ? $post["selecttipocomunicacion"] : NULL;

                $tipo_destinatario = $post["tipo_destinatario"];
                $tipo_radicado = $post["tipo_radicado"];


                //en el prceso de creacion de un borrador, no guardo los datos del editor nde una vez,
                // sino que lo actualizo en una peticion nueva, luego del insert
                $editor = $post["editoroplantilla"];
                $documento_ruta = "";
                if (isset($post['documento_ruta'])) {
                    $documento_ruta = $post['documento_ruta'];
                    $html = "";
                }


                $dependencia = $_SESSION['dependencia'];
                $codusuario = $_SESSION["usuario_id"];

                $tipopersona = NULL;
                if (isset($post['tipopersona']) && $post['tipopersona'] != "" && $post['tipopersona'] != null) {
                    $tipopersona = $post['tipopersona'];
                }

                $persona = NULL;
                if (isset($post['persona']) && $post['persona'] != "" && $post['persona'] != null) {
                    $persona = $post['persona'];
                }

                $query = "INSERT INTO  borradores (FOLIOS_COMUNICACION,FOLIOS_ANEXOS,DESCRIPCION_ANEXOS,
TIPOPERSONA,DEPENDENCIA_USU_ACTUAL,DEPENDENCIA_USU_ANTERIOR,
DESTINATARIO,FECHA_CREACION,ID_DESTINATARIO,NOMBRE_PERSONA,CARGO_PERSONA,ASUNTO,TIPO_BORRADOR_DEST";


                $query .= ", DOCUMENTO_FORMATO,DOCUMENTO_RUTA,ID_ESTADO,ID_USUARIO_ACTUAL,ID_USUARIO_ANTERIOR";

                if ($post["tipo_destinatario"] != "") {
                    $query .= ",TIPO_DESTINATARIO ";
                    $query .= ",TIPO_RADICADO ";

                }

                $query .= ",TIPO_COMUNICACION)
                 VALUES (" . trim($folios_comunicacion) . "," . $this->reemplazarcomillas($folios_anexos) . "," . $this->reemplazarcomillas($descripcion_anexos) . ", 
                 " . $this->reemplazarcomillas($tipopersona) . "," . $this->reemplazarcomillas($dependencia) . ",0," . $this->reemplazarcomillas($post["dest"]) . ",'" . date("Y-m-d H:i:s") . "',
                 " . $this->reemplazarcomillas($post["id_destinatario"]) . "," . $this->reemplazarcomillas($persona) . "," . $this->reemplazarcomillas($post["cargo"]) . ",
                 " . $this->reemplazarcomillas($post["asunto"]) . "," . $this->reemplazarcomillas($post["tipo_borrador_dest"]) . ",";

                /* if(strlen($html)>34) {  //61 es la longitud sin escribir nada en el editor
                     $query .= "'" . pg_escape_string($html) . "', ";
                 }*/

                $query .= "" . $this->reemplazarcomillas($editor) . "," . $this->reemplazarcomillas($documento_ruta) . ",'1'," . $this->reemplazarcomillas($codusuario) . ",'0' ";
                if ($post["tipo_destinatario"] != "") {
                    $query .= "," . $this->reemplazarcomillas($tipo_destinatario) . " ";
                    $query .= "," . $this->reemplazarcomillas($tipo_radicado) . " ";
                }
                $query .= "," . $this->reemplazarcomillas($tipo_comunicacion) . ")  RETURNING id_borrador";


                $guadar = $this->db->conn->execute($query);
                if ($guadar) {
                    $_SESSION['borradores'] = $_SESSION['borradores'] + 1;
                }

                if($post['optSelectedNewBorrador']!=$_SESSION['SALIDA_OFICIO']){
                    $this->actualizaCargoUsuario($post["cargo"],$post["id_destinatario"]);
                }

                return $guadar->fields['ID_BORRADOR'];
            } else {
                $this->updatecada60helper($post);
                return $post['borrador_id'];
            }


        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    function updateEditor($post)
    {

        //del editor , las comillas vienen asi:
        $html =  $post['editor'];
        //$html = str_replace("'", "&#39;", $html);

        /* $html = str_replace("<!DOCTYPE html>", "", $html);
         $html = str_replace("<html>", "", $html);
         $html = str_replace("</html>", "", $html);
        $html = str_replace('"', "^^", $html);
        $html = str_replace("'", "++", $html);*/


        if ($post['editoroplantilla'] == "EditorWeb") {

            $html=pg_escape_string(utf8_encode($html));
            $query = "update borradores
            set documento_web_cuerpo='" . $html . "',  documento_formato='EditorWeb' where id_borrador='" . $post['borrador_id'] . "' ";
            $guadar = $this->db->conn->execute($query);



            $query = "update borradores
            set documento_web_cuerpo=REPLACE (
     documento_web_cuerpo,
   '\&quot;',
   '\"'
   ) where id_borrador='" . $post['borrador_id'] . "' ";

            //$guadar = $this->db->conn->execute($query);
        }

        return true;

    }

    function updateDocumentoFormato($borrador_id, $formato)
    {
        $query = "update borradores set documento_formato='$formato' where id_borrador='" . $borrador_id . "' ";
        $guadar = $this->db->conn->execute($query);
    }

    function getDestEspecificSalida($destinatario)
    {
        $query = "Select '2' As TIPO_DESTINATARIO, 'Externo|Empresa' AS TIPO_RADICADO, 
emp.sgd_oem_codigo As ID, emp.SGD_OEM_OEMPRESA As NOMBRE,
(Case When emp.sgd_oem_nit !='' Then 'Nit '||emp.sgd_oem_nit End) As IDENTIFICACION,
emp.sgd_oem_direccion As DIRECCION, emp.sgd_oem_telefono As TELEFONO, emp.sgd_oem_email As EMAIL 
From sgd_oem_oempresas emp
Where
 (emp.sgd_oem_codigo in ($destinatario) )  
 UNION ALL 
Select '1' As TIPO_DESTINATARIO, 'Externo|Ciudadano' As TIPO_RADICADO, ciu.sgd_ciu_codigo As ID, 
ciu.sgd_ciu_nombre || ' ' || sgd_ciu_apell1 || ' ' || sgd_ciu_apell2 As NOMBRE,
(Case When ciu.sgd_ciu_cedula !='' Then 'CC '||ciu.sgd_ciu_cedula End) As IDENTIFICACION, 
ciu.sgd_ciu_direccion As DIRECCION, ciu.sgd_ciu_telefono As TELEFONO, ciu.sgd_ciu_email As EMAIL 
From sgd_ciu_ciudadano ciu
Where
 (ciu.sgd_ciu_codigo in ($destinatario) )";

        return $this->db->conn->selectLimit($query, 2)->getArray();
    }

    public function get_dest($vall, $opcionselected, $destinatario, $customB = false)
    {
        try {

            if ($opcionselected == $_SESSION['SALIDA_OFICIO']) {

                $query = "Select '2' As TIPO_DESTINATARIO, 'Externo|Empresa' AS TIPO_RADICADO, 
emp.sgd_oem_codigo As ID, emp.SGD_OEM_OEMPRESA As NOMBRE,
(Case When emp.sgd_oem_nit !='' Then 'Nit '||emp.sgd_oem_nit End) As IDENTIFICACION,
emp.sgd_oem_direccion As DIRECCION, emp.sgd_oem_telefono As TELEFONO, emp.sgd_oem_email As EMAIL  
From sgd_oem_oempresas emp
Where
(sgd_oem_inactivo='0' Or sgd_oem_inactivo is null )
And ( 
TRANSLATE(emp.sgd_oem_oempresa,'ÁÉÍÓÚáéíóú','AEIOUaeiou') ilike translate('%" . $vall . "%','ÁÉÍÓÚáéíóú','AEIOUaeiou')
Or lower(emp.sgd_oem_nit) Like Lower('%" . $vall . "%') Or lower(emp.sgd_oem_sigla) Like Lower('%" . $vall . "%')
Or TRANSLATE(emp.sgd_oem_direccion,'ÁÉÍÓÚáéíóú','AEIOUaeiou') ilike translate('%" . $vall . "%','ÁÉÍÓÚáéíóú','AEIOUaeiou')
Or lower(emp.sgd_oem_email) Like Lower('%" . $vall . "%')
)";

                if ($destinatario !== false) {
                    $query .= "and (emp.sgd_oem_codigo != " . $destinatario . " ) ";
                }
                $query .= "UNION ALL 
Select '1' As TIPO_DESTINATARIO, 'Externo|Ciudadano' As TIPO_RADICADO, ciu.sgd_ciu_codigo As ID, ciu.sgd_ciu_nombre || ' ' || sgd_ciu_apell1 || ' ' || sgd_ciu_apell2 As NOMBRE,
(Case When ciu.sgd_ciu_cedula !='' Then 'CC '||ciu.sgd_ciu_cedula End) As IDENTIFICACION, ciu.sgd_ciu_direccion As DIRECCION, ciu.sgd_ciu_telefono As TELEFONO, ciu.sgd_ciu_email As EMAIL 
From sgd_ciu_ciudadano ciu
Where
(sgd_ciu_inactivo='0' or sgd_ciu_inactivo is null)
And (
TRANSLATE(ciu.sgd_ciu_cedula,'ÁÉÍÓÚáéíóú','AEIOUaeiou') ilike translate('%" . $vall . "%','ÁÉÍÓÚáéíóú','AEIOUaeiou')
Or TRANSLATE(ciu.sgd_ciu_nombre,'ÁÉÍÓÚáéíóú','AEIOUaeiou') ilike translate('%" . $vall . "%','ÁÉÍÓÚáéíóú','AEIOUaeiou')
Or  TRANSLATE(ciu.sgd_ciu_apell1,'ÁÉÍÓÚáéíóú','AEIOUaeiou') ilike translate('%" . $vall . "%','ÁÉÍÓÚáéíóú','AEIOUaeiou')
Or  TRANSLATE(ciu.sgd_ciu_apell2,'ÁÉÍÓÚáéíóú','AEIOUaeiou') ilike translate('%" . $vall . "%','ÁÉÍÓÚáéíóú','AEIOUaeiou')
Or (TRANSLATE(lower(ciu.sgd_ciu_nombre) ||' '|| lower(ciu.sgd_ciu_apell1) ||' '||lower(ciu.sgd_ciu_apell2) ,'ÁÉÍÓÚáéíóú','AEIOUaeiou')
 ilike translate('%" . $vall . "%','ÁÉÍÓÚáéíóú','AEIOUaeiou') ))
 ";
                if ($destinatario !== false) {
                    $query .= "and (ciu.sgd_ciu_codigo !=" . $destinatario . " ) ";
                }


            } else if ($opcionselected == $_SESSION['INTERNA_COMUNICACION'] ||
                $opcionselected == $_SESSION['INTERNA_CON_PLANTILLA'] ||
                $opcionselected == $_SESSION['INTERNA_SIN_PLANTILLA'] ||
                $opcionselected == $_SESSION['REPORTE4'] || $customB == true  
            ) {

                $query = "Select '4' As TIPO_DESTINATARIO, 'Interno |' As TIPO_RADICADO, fun.id As ID,
 fun.usua_nomb As NOMBRE, fun.usua_nomb,
'CC '||fun.usua_doc IDENTIFICACION,d.depe_nomb,  d.depe_nomb As DIRECCION, fun.usua_login As LOGIN, fun.usua_cargo, fun.depe_codi, fun.usua_email As EMAIL 
From usuario fun Inner Join dependencia d On fun.depe_codi=d.depe_codi
Where fun.usua_esta='1'
And
(TRANSLATE(fun.usua_nomb,'ÁÉÍÓÚáéíóú','AEIOUaeiou') ilike translate('%" . $vall . "%','ÁÉÍÓÚáéíóú','AEIOUaeiou')
Or  TRANSLATE(fun.usua_login,'ÁÉÍÓÚáéíóú','AEIOUaeiou') ilike translate('%" . $vall . "%','ÁÉÍÓÚáéíóú','AEIOUaeiou')
Or  TRANSLATE(fun.usua_doc,'ÁÉÍÓÚáéíóú','AEIOUaeiou') ilike translate('%" . $vall . "%','ÁÉÍÓÚáéíóú','AEIOUaeiou')
Or  TRANSLATE(fun.usua_email,'ÁÉÍÓÚáéíóú','AEIOUaeiou') ilike translate('%" . $vall . "%','ÁÉÍÓÚáéíóú','AEIOUaeiou')
Or  TRANSLATE(d.depe_nomb,'ÁÉÍÓÚáéíóú','AEIOUaeiou') ilike translate('%" . $vall . "%','ÁÉÍÓÚáéíóú','AEIOUaeiou'))
";

                if ($destinatario !== false) {
                    $query .= "OR (fun.id in (" . $destinatario . ") ) ";
                    $query .= " ORDER BY fun.id =".$destinatario." desc";
                }
            }


            return $this->db->conn->selectLimit($query, 100)->getArray();

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function buscarUsuariosComen($coincide, $usuariosIn = false)
    {
        try {
            $dependencia = $_SESSION['dependencia'];
            $query = "SELECT
  u.usua_nomb || ' (' || COALESCE(u.usua_cargo||' - ','') ||''|| d.depe_nomb || ')' AS NOMBRE,
   u.id, u.usua_cargo,d.depe_codi,
  (CASE
	WHEN u.usua_codi=1 THEN 'Jefe'  
	ELSE ''
	END)	AS ROL
FROM
  usuario u INNER JOIN dependencia d ON u.depe_codi = d.depe_codi
WHERE  u.usua_esta='1' And u.depe_codi < 999  ";


            if ($_SESSION['LISTADO_FIRMANTES'] == 'Solo Jefes') {
                $query .= " and u.usua_codi='1' ";
            }

            if ($_SESSION['LISTADO_FIRMANTES'] == 'Jefes y Aprobadores') {
                $query .= " and (u.usua_codi=1 OR u.usua_perm_aprobar=1) ";
            }

            if ($_SESSION['LISTADO_FIRMANTES'] == 'Jefes, Aprobadores, Reasignadores, Supervisores y Públicos') {
                $query .= " and (u.usua_codi=1 OR u.usua_perm_aprobar=1 
                OR u.usuario_reasignar=1 
                OR usua_supervisor=1 OR u.usuario_publico=1) ";
            }
            if ($usuariosIn != false) {
                $query .= " and ( u.id in (" . $usuariosIn . ") or ";
            } else {
                $query .= " and ";
            }

            $query .= "  (
 TRANSLATE(u.usua_nomb,'ÁÉÍÓÚáéíóú','AEIOUaeiou') ilike translate('%" . $coincide . "%','ÁÉÍÓÚáéíóú','AEIOUaeiou')
Or TRANSLATE(d.depe_nomb,'ÁÉÍÓÚáéíóú','AEIOUaeiou') ilike translate('%" . $coincide . "%','ÁÉÍÓÚáéíóú','AEIOUaeiou')
) ";
            if ($usuariosIn != false) {
                $query .= " ) ";
            }
            $query .= "  ORDER BY ";

            if ($usuariosIn != false) {
                $usuExplode = explode(',', $usuariosIn);
                foreach ($usuExplode as $row) {
                    $query .= " u.id=" . $row . " desc,";
                }
            }

            $query .= " u.usua_codi ASC, u.depe_codi ASC ";


            return $this->db->conn->selectLimit($query, 20000)->getArray();

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function buscarRadicadoHelper($coincide)
    {
        try {
            $query = "select id,radi_nume_radi, ra_asun, radi_fech_radi, radi_depe_actu
from radicado
where lower(cast(radi_nume_radi as text)) like lower('%" . $coincide . "%') 
or TRANSLATE(ra_asun,'ÁÉÍÓÚáéíóú','AEIOUaeiou') ilike translate('%" . $coincide . "%','ÁÉÍÓÚáéíóú','AEIOUaeiou')
order by radi_fech_radi desc";
            return $this->db->conn->selectLimit($query, 100)->getArray();

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function get_borrador($vall)
    {
        try {
            $query = "SELECT  *
                FROM borradores  
                join borrador_estados on (borrador_estados.id_borrador_estado=borradores.id_estado)
                where id_borrador=" . $vall . "
                ORDER BY id_borrador ASC";

            return $this->db->conn->selectLimit($query, 10)->getArray();

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function get_borradorByRadi($vall)
    {
        try {
            $query = "SELECT  *
                FROM borradores  
                join borrador_estados on (borrador_estados.id_borrador_estado=borradores.id_estado)
                where radi_nume=" . $vall . "
                ORDER BY id_borrador ASC";

            return $this->db->conn->selectLimit($query, 1)->getArray();

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function aprobarborradorHelper($datos)
    {
        try {
            $query = "update borradores
            set id_estado=2 where id_borrador='" . $datos['borrador_id'] . "' ";
            //executing
            $response = $this->db->conn->execute($query);
            return $response;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function revisarBorradorHelper($datos)
    {
        try {
            $query = "update borradores
            set id_estado=3 where id_borrador='" . $datos['borrador_id'] . "' ";
            //executing
            $response = $this->db->conn->execute($query);
            return $response;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function devolverBorrador($datos)
    {
        try {
            $query = "update borradores
            set id_usuario_actual='" . $datos['anterior'] . "'  , id_usuario_anterior='" . $datos['actual'] . "' ,
            dependencia_usu_anterior='" . $datos['dependencia_actual'] . "', dependencia_usu_actual='" . $datos['dependencia_anterior'] . "'
             where id_borrador='" . $datos['borrador_id'] . "' ";
            //executing

            $response = $this->db->conn->execute($query);
            return $response;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function noaprobarBorrador($datos)
    {
        try {
            $query = "update borradores
            set id_estado=6, id_usuario_actual='" . $datos['anterior'] . "'  , id_usuario_anterior='" . $datos['actual'] . "' ,
            dependencia_usu_anterior='" . $datos['dependencia_actual'] . "', dependencia_usu_actual='" . $datos['dependencia_anterior'] . "'
             where id_borrador='" . $datos['borrador_id'] . "' ";
            //executing

            $response = $this->db->conn->execute($query);
            return $response;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function updateUsuActual($datos)
    {
        try {
            $query = "update borradores
            set id_usuario_actual='" . $datos['anterior'] . "'  , id_usuario_anterior='" . $datos['actual'] . "' ,
             dependencia_usu_anterior='" . $datos['dependencia_actual'] . "', dependencia_usu_actual='" . $datos['dependencia_anterior'] . "'
             where id_borrador='" . $datos['borrador_id'] . "' ";

            //executing
            $response = $this->db->conn->execute($query);
            return $response;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function borrarBorrador($datos)
    {
        try {
            $query = "delete from borradores where id_borrador='" . $datos['borrador_id'] . "' ";
            //executing
            $response = $this->db->conn->execute($query);
            return $response;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function guardarhistorico($id_borrador, $fecha_transaccion, $comentario_borrador, $id_usuario_origen,
                                     $depe_codi_origen, $id_usuario_destino, $depe_codi_destino, $id_estado, $sgd_ttr_codigo = '80')
    {
        try {

            if ($id_borrador != "") {
                $query = "INSERT INTO  borrador_historicos (ID_BORRADOR,FECHA_TRANSACCION,
COMENTARIO_BORRADOR,ID_USUARIO_ORIGEN,DEPE_CODI_ORIGEN,ID_USUARIO_DESTINO,DEPE_CODI_DESTINO,ID_ESTADO,SGD_TTR_CODIGO)
 VALUES ('" . $id_borrador . "',$fecha_transaccion,'" . $comentario_borrador . "','" . $id_usuario_origen . "','" . $depe_codi_origen . "',
 '" . $id_usuario_destino . "','" . $depe_codi_destino . "',$id_estado,'" . $sgd_ttr_codigo . "')";
                $guadar = $this->db->conn->execute($query);
                return $guadar;
            }

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    //trae un usuario por usua_codi y dependencia
    public function getUsuario($usua_codi, $dependencia)
    {
        try {
            $query = "SELECT  *
                FROM usuario where usua_codi='" . $usua_codi . "' and depe_codi='" . $dependencia . "' ";

            $response = $this->db->conn->selectLimit($query, 10000)->getArray();
            return $response;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function getUsuarioById($usuario_id)
    {
        try {
            $query = "SELECT  *
                FROM usuario where id='" . $usuario_id . "' ";
            $response = $this->db->conn->selectLimit($query, 1)->getArray();
            return $response;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function getUsuarioByNomb($nombre_usuario)
    {
        try {
            $query = "SELECT  *
                FROM usuario where usua_nomb='" . $nombre_usuario . "' ";
            $response = $this->db->conn->selectLimit($query, 2)->getArray();
            return $response;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function getUsuarioByUsuaLogin($usua_login)
    {
        try {
            $query = "SELECT  *
                FROM usuario where usua_login='" . $usua_login . "' ";
            $response = $this->db->conn->selectLimit($query, 2)->getArray();
            return $response;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }


    //busca el ultimo borrador guardado
    function getLastBorrador()
    {

        $query = "SELECT  *
                FROM borradores order by id_borrador desc ";
        $response = $this->db->conn->selectLimit($query, 1)->getArray();
        return $response;
    }


    //busca el ultimo registro de borrador asociado
    function getLastOrdenIndex($borrador_id)
    {

        $query = "SELECT  max(radicado_orden_anexo) as maximo
                FROM borrador_radicados_asociados where id_borrador='" . $borrador_id . "' ";
        $response = $this->db->conn->selectLimit($query, 1)->getArray();
        return $response;
    }

    //busca el ultimo registro en anexos borrador, para un id y un borrador
    function getLastAnexBorra($id_usuario, $borrador_id, $dependencia)
    {

        $query = "SELECT  *
                FROM borrador_anexos where id_usuario_anexo='" . $id_usuario . "' 
                 and id_borrador='" . $borrador_id . "'  
                 order by id_borrador_anexo ASC";
        $response = $this->db->conn->selectLimit($query, 1)->getArray();
        return $response;
    }

    function insertBorrador_radicados_asociados($id_borrador, $rad, $radicado_origen, $orden_anexo)
    {

        $query = "INSERT INTO  borrador_radicados_asociados (ID_BORRADOR,RADICADO_ASOCIADO,
RADICADO_ORIGEN,RADICADO_ORDEN_ANEXO)
                    VALUES ('" . $id_borrador . "','" . $rad . "',$radicado_origen,$orden_anexo)";


        $this->db->conn->execute($query);
    }

    //GUARDA EN LA TABLA borrador_anexos
    function saveAnexoBorrador($id_borrador_anexo, $ruta, $name,
                               $cont, $codusuario, $dependencia)
    {

        $query = "INSERT INTO  borrador_anexos (ID_BORRADOR,ANEXO_RUTA,ANEXO_NOMBRE,
ANEXO_FECHA,ANEXO_ORDEN,ID_USUARIO_ANEXO,USUA_DEPE_BORR_ANEX)
                    VALUES ($id_borrador_anexo,'" . $ruta . "','" . $name . "','" . date("Y-m-d H:i:s") . "',
                    $cont,$codusuario,$dependencia) RETURNING id_borrador_anexo";
        $guadar = $this->db->conn->execute($query);

        return $guadar->fields['ID_BORRADOR_ANEXO'];
    }

    function getAnexosBorrador($id_borrador_anexo)
    {

        $query = "SELECT  *
                FROM borrador_anexos where id_borrador='" . $id_borrador_anexo . "' order by id_borrador_anexo ASC";
        $response = $this->db->conn->selectLimit($query, 10000)->getArray();
        return $response;
    }

    //borra dela tabla un anexo
    function borrar_anexo($borrador, $nombre)
    {

        $query = "delete from borrador_anexos where id_borrador='" . $borrador . "' and
         id_borrador_anexo='" . $nombre . "' ";
        //executing
        $response = $this->db->conn->execute($query);
        return $response;
    }

    //borra dela tabla borrador_radicados_asociados
    function removeRadiAsoc($borrador, $radicado_asociado)
    {

        $query = "delete from borrador_radicados_asociados where id_borrador='" . $borrador . "' and
         radicado_asociado='" . $radicado_asociado . "' ";
        //executing
        $response = $this->db->conn->execute($query);
        return $response;
    }


    public function borradorRadAsoc($id_borrador, $radicados)
    {
        try {

            $yaexiste = array();
            $cont = 0;
            if ($id_borrador != "") {
                //asocio los radicados a el borrador

                foreach ($radicados as $rad) {  //$rad es el numero del radicado

                    $radicado = array();
                    $buscarSiyaExiste = $this->radicadoAsocByBorr($id_borrador, $rad);
                    $radicado = $this->getRadicadoByNume_radi($rad);
                    if (!isset($buscarSiyaExiste[0])) {
                        //busco el ultimo orden del anexo
                        $response = $this->getLastOrdenIndex($id_borrador);
                        /*if (count($response) > 0) {
                             $cont = $response[0]['ID_BORRADOR_RADICADO_ASOCIADO'] + 1;
                         }*/

                        $orden = 1;
                        if ($response[0]['MAXIMO'] != "") {
                            $orden = $response[0]['MAXIMO'] + 1;
                        }
                        //NO SE CUAL ES EL ORIGEN DEL RADICADO

                        $this->insertBorrador_radicados_asociados($id_borrador, $rad, 0, $orden);

                    } else {
                        $yaexiste[$cont] = $radicado[0]['RADI_NUME_RADI'];
                    }

                }
                return $yaexiste;
            }

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function get_radicadosAsociados($vall)
    {
        try {
            $query = "SELECT  *
                FROM borrador_radicados_asociados  
                join radicado on (radicado.radi_nume_radi=borrador_radicados_asociados.radicado_asociado)
                where borrador_radicados_asociados.id_borrador=" . $vall . "
                ORDER BY id_borrador ASC";


            return $this->db->conn->selectLimit($query, 10000)->getArray();

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    //verifica si existe un raicado asociado para un determinado borrador
    public function radicadoAsocByBorr($borrador_id, $radicado)
    {
        try {
            $query = "SELECT  *
                FROM borrador_radicados_asociados  
                where borrador_radicados_asociados.id_borrador='" . $borrador_id . "' and radicado_asociado='" . $radicado . "' ";

            return $this->db->conn->selectLimit($query, 1)->getArray();

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    //verifica si existe radicado asocaido origen, para un determinado borrador
    public function getRadicadoOrigen($borrador_id)
    {
        try {
            $query = "SELECT  *
                FROM borrador_radicados_asociados  
                where borrador_radicados_asociados.id_borrador='" . $borrador_id . "' and radicado_origen=1 ";

            return $this->db->conn->selectLimit($query, 1)->getArray();

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }


    public function get_comentarios($vall)
    {
        try {
            $query = "SELECT  borrador_historicos.*,borradores.*, usuario.usua_login,usuario.usua_nomb,dependencia.depe_nomb,
borrador_estados.nombre_estado,
borrador_estados.id_borrador_estado,borrador_estados.nombre_transaccion,
borrador_historicos.id_estado as id_estado_borrador
                FROM borrador_historicos  
                join borradores on (borradores.id_borrador=borrador_historicos.id_borrador)
               left join usuario on (usuario.id=borrador_historicos.id_usuario_origen )
                left join dependencia on (usuario.depe_codi=dependencia.depe_codi )
               left join borrador_estados on (borrador_estados.id_borrador_estado=borrador_historicos.id_estado)
                where borrador_historicos.id_borrador=" . $vall . "   
                ORDER BY borrador_historicos.fecha_transaccion desc";


            return $this->db->conn->selectLimit($query, 10000)->getArray();

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function get_comentariosLike($vall, $buscar)
    {
        try {
            $query = "SELECT  borradores.*,borrador_historicos.*,borrador_estados.*, usuario.usua_login, usuario.usua_login,
usuario.usua_nomb,dependencia.depe_nomb
                FROM borrador_historicos  
                join borradores on (borradores.id_borrador=borrador_historicos.id_borrador)
               left join usuario on (usuario.id=borrador_historicos.id_usuario_origen )
               left join dependencia on (dependencia.depe_codi=borrador_historicos.depe_codi_origen )
               left join borrador_estados on (borrador_estados.id_borrador_estado=borrador_historicos.id_estado)
                where borrador_historicos.id_borrador=" . $vall . " and comentario_borrador!='' ";

            if ($buscar != "") {
                $query .= "and ( 
                TRANSLATE(comentario_borrador,'ÁÉÍÓÚáéíóú','AEIOUaeiou') ilike translate('%" . $buscar . "%','ÁÉÍÓÚáéíóú','AEIOUaeiou')
                or TRANSLATE(usua_login,'ÁÉÍÓÚáéíóú','AEIOUaeiou') ilike translate('%" . $buscar . "%','ÁÉÍÓÚáéíóú','AEIOUaeiou')
                 )
               ";
            }

            $query .= "  ORDER BY borrador_historicos.id_borrador desc ";


            return $this->db->conn->selectLimit($query, 10)->getArray();

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    //busca el radicado por el numero de radicado
    public function getRadicadoByNume_radi($vall)
    {
        try {
            $query = "SELECT  * from radicado where radi_nume_radi='" . $vall . "' ";

            return $this->db->conn->selectLimit($query, 1)->getArray();

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    //busca el radicado por el numero ID
    public function getRadicadoById($vall)
    {
        try {
            $query = "SELECT  * from radicado where id=$vall ";

            return $this->db->conn->selectLimit($query, 1)->getArray();

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function getTransDevRad($radicado_id)
    {
        try {
            $query = "SELECT  hist_fech as FECHA_DEV from hist_eventos where radi_nume_radi = $radicado_id and sgd_ttr_codigo = 28 order by hist_fech desc";

            $rs = $this->db->conn->selectLimit($query, 1)->getArray();

            return $rs;

        } catch (\Exception $e) {

            $rse = array (
                array("FECHA_DEV",null)
            );
            return $rse;
        }
    }

    function cantidadBorradoresUsu()
    {

        $query = "SELECT  * FROM borrador_estados  where sgd_ttr_codigo=87";
        $estadoComentario = $this->db->conn->selectLimit($query, 1)->getArray();

        $query = "SELECT  * FROM borrador_estados  where sgd_ttr_codigo=88";
        $estadoEliminado = $this->db->conn->selectLimit($query, 1)->getArray();

        $codusuario = $_SESSION["usuario_id"];
        $dependencia = $_SESSION["dependencia"];
        $isql = "select count(borradores.id_borrador) as
 total from borradores where  dependencia_usu_actual=" . $dependencia . " and  id_usuario_actual=" . $codusuario . " 
 and  borradores.id_estado<>7 and borradores.id_estado<>" . $estadoComentario[0]['ID_BORRADOR_ESTADO'] . " 
 and borradores.id_estado<>" . $estadoEliminado[0]['ID_BORRADOR_ESTADO'];


        $rs = $this->db->conn->Execute($isql);
        $borradoresactuales = $rs->fields["TOTAL"];

        return $borradoresactuales;
    }

    public function borradoresAmiCargo()
    {

        $codusuario = $_SESSION["usuario_id"];
        $dependencia = $_SESSION["dependencia"];
        $isql = "select * from borradores where  dependencia_usu_actual=" . $dependencia . " and  id_usuario_actual=" . $codusuario . " ";
        $response = $this->db->conn->selectLimit($isql, 10000)->getArray();

        return $response;
    }

    public function get_expedientes($buscar = false, $expediente = false, $dependencia = false,
                                    $serie = false, $subserie = false, $anio = false,
                                    $menor_dos_anios = false, $limit = 1000, $soloabiertos = true)
    {
        try {


            $query = "SELECT
sexp.sgd_exp_numero AS NUMERO_EXPEDIENTE,
sexp.depe_codi,
sexp.sgd_srd_codigo,
sexp.sgd_sbrd_codigo,
sexp.sgd_sexp_ano,
sexp.sgd_sexp_acceso as ACCESO,
sexp.depe_codi ||'-'|| d.depe_nomb AS DEPENDENCIA,
sexp.sgd_sexp_parexp1 AS TITULO_NOMBRE,
sexp.sgd_sexp_parexp2 AS DESCRIPCION,
s.sgd_srd_descrip || ' / '|| sb.sgd_sbrd_descrip ||' ('|| sexp.sgd_srd_codigo || '.'|| sexp.sgd_sbrd_codigo ||')' AS TRD,
(Case When sexp.SGD_SEXP_ESTADO=0 Then 'Abierto'
When sexp.SGD_SEXP_ESTADO=1 Then 'Cerrado'
End) As ESTADO
From sgd_sexp_secexpedientes sexp
INNER JOIN sgd_sbrd_subserierd sb On sb.sgd_sbrd_codigo=sexp.sgd_sbrd_codigo And sb.sgd_srd_codigo=sexp.sgd_srd_codigo
INNER JOIN sgd_srd_seriesrd s On s.sgd_srd_codigo=sb.sgd_srd_codigo And sexp.sgd_srd_codigo=s.sgd_srd_codigo
INNER JOIN dependencia d On d.depe_codi=sexp.depe_codi
Where sexp.sgd_exp_numero is not NULL ";

            if ($soloabiertos == true) {
                $query .= "and sgd_sexp_estado=0 ";
            }

            if ($menor_dos_anios == true) {
                $query .= "and sexp.sgd_sexp_ano In (extract(year from current_timestamp),extract(year from current_timestamp)-1) ";
            }

            if ($expediente != false) {
                $query .= " and lower(sexp.sgd_exp_numero)=lower('" . $expediente . "') ";
            }
            if ($buscar != false) {
                $query .= " And (
                 TRANSLATE(sexp.sgd_exp_numero,'ÁÉÍÓÚáéíóú','AEIOUaeiou') ilike translate('%" . $buscar . "%','ÁÉÍÓÚáéíóú','AEIOUaeiou')
                Or TRANSLATE(sexp.sgd_sexp_parexp1,'ÁÉÍÓÚáéíóú','AEIOUaeiou') ilike translate('%" . $buscar . "%','ÁÉÍÓÚáéíóú','AEIOUaeiou')
                 or TRANSLATE(sexp.sgd_sexp_parexp2,'ÁÉÍÓÚáéíóú','AEIOUaeiou') ilike translate('%" . $buscar . "%','ÁÉÍÓÚáéíóú','AEIOUaeiou') ";

                if (isset($_GET['buscartodo']) && $_GET['buscartodo'] == 'true') {
                    $query .= "  Or TRANSLATE(sexp.sgd_sexp_parexp3,'ÁÉÍÓÚáéíóú','AEIOUaeiou') ilike translate('%" . $buscar . "%','ÁÉÍÓÚáéíóú','AEIOUaeiou')
                    Or TRANSLATE(sexp.sgd_sexp_parexp4,'ÁÉÍÓÚáéíóú','AEIOUaeiou') ilike translate('%" . $buscar . "%','ÁÉÍÓÚáéíóú','AEIOUaeiou')
                    Or  TRANSLATE(sexp.sgd_sexp_parexp5,'ÁÉÍÓÚáéíóú','AEIOUaeiou') ilike translate('%" . $buscar . "%','ÁÉÍÓÚáéíóú','AEIOUaeiou')
 Or  TRANSLATE(d.depe_nomb,'ÁÉÍÓÚáéíóú','AEIOUaeiou') ilike translate('%" . $buscar . "%','ÁÉÍÓÚáéíóú','AEIOUaeiou') 
 Or TRANSLATE(s.sgd_srd_descrip,'ÁÉÍÓÚáéíóú','AEIOUaeiou') ilike translate('%" . $buscar . "%','ÁÉÍÓÚáéíóú','AEIOUaeiou')
 Or TRANSLATE(sb.sgd_sbrd_descrip,'ÁÉÍÓÚáéíóú','AEIOUaeiou') ilike translate('%" . $buscar . "%','ÁÉÍÓÚáéíóú','AEIOUaeiou')
  ";
                }
                $query .= " )";
            }

            if ($dependencia != false) {
                $query .= " and sexp.depe_codi in (" . $dependencia . ") ";
            }

            if ($serie != false) {
                $query .= " and sexp.sgd_srd_codigo=$serie ";
            }

            if ($subserie != false) {
                $query .= " and sexp.sgd_sbrd_codigo=$subserie ";
            }

            if ($anio != false) {
                $query .= " and sexp.sgd_sexp_ano=$anio ";
            }


            $query .= " Order By sexp.sgd_sexp_ano Desc, sexp.sgd_sexp_fech Desc, sexp.depe_codi, s.sgd_srd_descrip";


            $response = $this->db->conn->selectLimit($query, $limit)->getArray();

            //executing
            //$response= $this->db->conn->execute($query);
            return $response;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }


    public function get_tipologias($buscar)
    {
        try {
            $query = "Select sgd_tpr_codigo,sgd_tpr_descrip From sgd_tpr_tpdcumento Where sgd_tpr_estado=1 
And sgd_tpr_codigo!=0 and TRANSLATE(sgd_tpr_descrip,'ÁÉÍÓÚáéíóú','AEIOUaeiou') ilike translate('%" . $buscar . "%','ÁÉÍÓÚáéíóú','AEIOUaeiou') ";


            $response = $this->db->conn->selectLimit($query, 1000)->getArray();
            //executing
            //$response= $this->db->conn->execute($query);
            return $response;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function tipologiasByCodigo($buscar)
    {  //busca una tipologia por su codigo unico
        try {
            $query = "Select sgd_tpr_codigo,sgd_tpr_descrip From sgd_tpr_tpdcumento Where sgd_tpr_estado In (1,0)
And sgd_tpr_codigo=$buscar ";

            $response = $this->db->conn->selectLimit($query, 1)->getArray();
            //executing
            //$response= $this->db->conn->execute($query);
            return $response;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    //un expediente especifico
    function getExpedBorra($borrador_id, $expediente, $tipologia = false)
    {
        try {
            $query = "SELECT  *
                FROM borrador_expedientes
                where id_borrador=" . $borrador_id . "  and sgd_exp_numero='" . $expediente . "' ";

            if ($tipologia != false) {
                $query .= " and sgd_tpr_codigo='" . $tipologia . "' ";
            }

            $query .= " ORDER BY id_borrador ASC";

            $borraExped = $this->db->conn->selectLimit($query, 1)->getArray();
            return $borraExped;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    //todos los expedientes de un borrador
    function getExpedBorrador($borrador_id)
    {
        try {
            $query = "SELECT  *
                FROM borrador_expedientes 
                where id_borrador=" . $borrador_id . "
                ORDER BY id_borrador ASC";

            $borraExped = $this->db->conn->selectLimit($query, 10000)->getArray();
            return $borraExped;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    //todos los expedientes de un radicado
    function getExpedBorradorEntrada($radicado, $expediente = false)
    {
        try {
            $query = "SELECT  *
                FROM sgd_exp_expediente 
                where radi_nume_radi=" . $radicado . " ";
            if ($expediente != false) {
                $query .= " and sgd_exp_numero='" . $expediente . "' ";
            }
            $query .= " ORDER BY sgd_exp_fech ASC";

            $borraExped = $this->db->conn->selectLimit($query, 100000)->getArray();
            return $borraExped;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    //todos los expedientes de un radicado, sgd_exp_estado!=2
    public function getExpedNotDeleted($radicado, $expediente = false)
    {
        try {
            $query = "SELECT  *
                FROM sgd_exp_expediente 
                where radi_nume_radi=" . $radicado . " ";
            if ($expediente != false) {
                $query .= " and sgd_exp_numero='" . $expediente . "' ";
            }
            $query .= " AND sgd_exp_estado!=2  ORDER BY sgd_exp_fech ASC";

            $borraExped = $this->db->conn->selectLimit($query, 100000)->getArray();
            return $borraExped;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function addExpeBorrador($borrador_id, $expedientenumero, $tipologia)
    {
        //agrega expedientes a un borrador, sin la tipologia
        try {
            $query = "insert into borrador_expedientes
            (id_borrador,sgd_exp_numero";
            if ($tipologia != false) {
                $query .= ",sgd_tpr_codigo";
            }
            $query .= ") VALUES 
            ($borrador_id,'" . $expedientenumero . "' ";

            if ($tipologia != false) {
                $query .= ",'" . $tipologia . "' ";
            }

            $query .= ") ";


            //executing
            $response = $this->db->conn->execute($query);
            return $response;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function updExpeTipBorrador($borrador_id, $expediente, $tipologia)
    {
        try {
            $query = "update borrador_expedientes set sgd_tpr_codigo='" . $tipologia . "'
           where id_borrador=$borrador_id and sgd_exp_numero='" . $expediente . "' ";

            //executing
            $response = $this->db->conn->execute($query);
            return $response;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    //busca los tipos de anexos  permitidos (extensiones)
    function getAnexosTipos($radicado)
    {
        $query = "select * from ANEXOS_TIPO";
        return $this->db->conn->selectLimit($query, 10000)->getArray();
    }

    public function updPath($borrador_id, $path)
    {
        try {
            $query = "update borradores set documento_ruta ='" . $path . "' where id_borrador= ".$borrador_id ;

            //executing
            $response = $this->db->conn->execute($query);
            return $response;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function updExpeTipRadicado($radicado, $expediente, $tipologia)
    {
        try {
            $query = "update borrador_expedientes set sgd_tpr_codigo='" . $tipologia . "'
           where id_borrador=$radicado and sgd_exp_numero='" . $expediente . "' ";

            //executing
            $response = $this->db->conn->execute($query);
            return $response;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }


    public function getDepenByDepeCodi($vall)
    {
        try {
            $query = "SELECT  *
                FROM dependencia  
                where depe_codi =$vall ";

            return $this->db->conn->selectLimit($query, 1)->getArray();

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function getDepeByNomb($vall)
    {
        try {
            $query = "SELECT  * FROM dependencia  where depe_nomb ='" . $vall . "' ";
            return $this->db->conn->selectLimit($query, 2)->getArray();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function getDepeBySigla($vall)
    {
        try {
            $query = "SELECT  * FROM dependencia  where dep_sigla ='" . $vall . "' ";
            return $this->db->conn->selectLimit($query, 2)->getArray();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function getDependExped()
    {
        //trae todas las dependencias
        try {
            $query = "SELECT distinct On (d.depe_nomb, sexp.depe_codi)
 sexp.depe_codi                       AS \"DEPE_CODI\",
        (Case When d.depe_estado=1 Then d.depe_nomb Else d.depe_nomb || ' (Inactiva)' End)     AS \"DEPENDENCIA\"
FROM sgd_sexp_secexpedientes sexp
INNER JOIN dependencia d On d.depe_codi=sexp.depe_codi
INNER JOIN sgd_srd_seriesrd s On  s.sgd_srd_codigo=sexp.sgd_srd_codigo
INNER JOIN sgd_sbrd_subserierd sb On sb.sgd_srd_codigo=s.sgd_srd_codigo And sb.sgd_sbrd_codigo=sexp.sgd_sbrd_codigo
Order By d.depe_nomb, sexp.depe_codi

";

            return $this->db->conn->selectLimit($query, 10000)->getArray();

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }


    public function getAnioExpedDepen($dependencia = "", $serie = "", $subserie = "")
    {
        //trae los anios, esto es usado en el modal de expediente, busqueda avanzada
        try {
            $query = "SELECT distinct On (sexp.sgd_sexp_ano)
  sexp.sgd_sexp_ano                 	AS \"AÑO\"
FROM sgd_sexp_secexpedientes sexp
INNER JOIN dependencia d On d.depe_codi=sexp.depe_codi
INNER JOIN sgd_srd_seriesrd s On  s.sgd_srd_codigo=sexp.sgd_srd_codigo
INNER JOIN sgd_sbrd_subserierd sb On sb.sgd_srd_codigo=s.sgd_srd_codigo And sb.sgd_sbrd_codigo=sexp.sgd_sbrd_codigo
WHERE sexp.sgd_sexp_ano is not null ";


            if ($dependencia != "") {
                $query .= " and  sexp.depe_codi = $dependencia ";
            }

            if ($serie != "") {
                $query .= " and  sexp.sgd_srd_codigo = $serie ";
            }
            if ($subserie != "") {
                $query .= " and  sexp.sgd_sbrd_codigo = $subserie ";
            }
            $query .= " Order By sexp.sgd_sexp_ano Desc";

            return $this->db->conn->selectLimit($query, 10000)->getArray();

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function getSerieExped($dependencia = "")
    {
        //trae los anios, esto es usado en el modal de expediente, busqueda avanzada
        try {
            $query = "SELECT distinct On (s.sgd_srd_descrip, sexp.sgd_srd_codigo)
  s.sgd_srd_descrip || ' - '|| sexp.sgd_srd_codigo  	AS \"SERIE\", sexp.sgd_srd_codigo 
FROM sgd_sexp_secexpedientes sexp
INNER JOIN dependencia d On d.depe_codi=sexp.depe_codi
INNER JOIN sgd_srd_seriesrd s On  s.sgd_srd_codigo=sexp.sgd_srd_codigo
WHERE sexp.sgd_srd_codigo is not null
 ";
            if ($dependencia != "") {
                $query .= " and  sexp.depe_codi = $dependencia ";
            }

            $query .= " Order By s.sgd_srd_descrip, sexp.sgd_srd_codigo";

            return $this->db->conn->selectLimit($query, 10000)->getArray();

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }


    public function getSubSerieExped($dependencia = "", $serie = "")
    {
        //trae los anios, esto es usado en el modal de expediente, busqueda avanzada
        try {
            $query = "SELECT distinct On (s.sgd_srd_descrip, sexp.sgd_srd_codigo, sb.sgd_sbrd_descrip, sexp.sgd_sbrd_codigo)
  s.sgd_srd_descrip || ' - '|| sexp.sgd_srd_codigo  	AS \"SERIE\",
  sb.sgd_sbrd_descrip || ' - '|| sexp.sgd_sbrd_codigo   AS \"SUBSERIE\", sb.sgd_sbrd_codigo
FROM sgd_sexp_secexpedientes sexp
INNER JOIN dependencia d On d.depe_codi=sexp.depe_codi
INNER JOIN sgd_srd_seriesrd s On  s.sgd_srd_codigo=sexp.sgd_srd_codigo
INNER JOIN sgd_sbrd_subserierd sb On sb.sgd_srd_codigo=s.sgd_srd_codigo And sb.sgd_sbrd_codigo=sexp.sgd_sbrd_codigo
WHERE sexp.sgd_sbrd_codigo is not null
 ";
            if ($dependencia != "") {
                $query .= " and  sexp.depe_codi = $dependencia ";
            }

            if ($serie != "") {
                $query .= " and  sexp.sgd_srd_codigo = $serie ";
            }

            $query .= " Order By s.sgd_srd_descrip, sexp.sgd_srd_codigo, sb.sgd_sbrd_descrip, sexp.sgd_sbrd_codigo";

            return $this->db->conn->selectLimit($query, 10000)->getArray();

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }


    //borra de la tabla borrador_expedientes
    function excluirExpediente($borrador, $expediente)
    {

        $query = "delete from borrador_expedientes where id_borrador='" . $borrador . "' and
         sgd_exp_numero='" . $expediente . "' ";
        //executing
        $response = $this->db->conn->execute($query);
        return $response;
    }


    //verifica si un radicado tiene un expediente
    function radiEstaEnExpediente($radicado)
    {
        try {
            $query = "SELECT  *
                FROM sgd_exp_expediente 
                where radi_nume_radi=" . $radicado . "
                ORDER BY id ASC";

            $borraExped = $this->db->conn->selectLimit($query, 10000)->getArray();
            return $borraExped;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function getHistRevBorrador($borrador_id,$id_usua,$depe)
    {
        try {
            $query = "SELECT *
                FROM borrador_historicos  
                 join borrador_estados on (borrador_estados.id_borrador_estado=borrador_historicos.id_estado)
                where borrador_historicos.sgd_ttr_codigo = '81' and  borrador_historicos.depe_codi_origen = ".$depe." and  borrador_historicos.id_usuario_origen = ".$id_usua."  and  borrador_historicos.id_borrador=" . $borrador_id . " ";
            return $this->db->conn->selectLimit($query, 10000)->getArray();

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function getHistoricoBorrador($borrador_id)
    {
        try {
            $query = "SELECT *
                FROM borrador_historicos  
                 join borrador_estados on (borrador_estados.id_borrador_estado=borrador_historicos.id_estado)
                where borrador_historicos.id_borrador=" . $borrador_id . " ";
            return $this->db->conn->selectLimit($query, 10000)->getArray();

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function getHistBorraJoinUser($borrador_id)
    {
        try {
            $query = "SELECT  borrador_historicos.*, usuario.usua_login,usuario.usua_codi, usuario.usua_cargo
                  FROM borrador_historicos  
                    join borradores on (borradores.id_borrador=borrador_historicos.id_borrador)
                    left join usuario on (usuario.id=borrador_historicos.id_usuario_origen)
                  where borrador_historicos.id_borrador=" . $borrador_id . " 
                  ORDER BY borrador_historicos.fecha_transaccion ASC";
            return $this->db->conn->selectLimit($query, 10000)->getArray();

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function actualizaCargoUsuario($cargo,$id_firmante){
        //leactualizo el cargo en la tabla usuario

        $queryusuario = "update usuario
            set usua_cargo=" . $this->reemplazarcomillas($cargo) . " where usuario.id=" . $id_firmante . " ";
        $this->db->conn->execute($queryusuario);

        return $this->db->conn->execute($queryusuario);
    }

    //guarda firmantes
    public function guardarFirmantes($id_borrador, $fecha_transaccion, $id_firmante, $cargo,
                                     $principal, $tipo_validador, $radi_nume = null)
    {
        try {
            if ($id_borrador != "") {
                if ($radi_nume == null) {
                    $radi_nume = "null";
                }
                $query = "INSERT INTO  borrador_firmantes (ID_BORRADOR,FECHA_REGISTRO,
ID_USUARIO_FIRMANTE,FIRMANTE_CARGO,FIRMANTE_PRINCIPAL,tipo_validador,radi_nume)
 VALUES (" . $id_borrador . ",$fecha_transaccion," . $id_firmante . ",'" . $cargo . "',
  " . $principal . ",'" . $tipo_validador . "'," . $radi_nume . " )";

                $this->actualizaCargoUsuario($cargo,$id_firmante);

                return $this->db->conn->execute($query);


            }

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }



    function guardarFirmantesRadicado($id_borrador, $fecha_transaccion, $id_firmante, $cargo,
                                      $principal, $radicado, $tipo_validador = 'null')
    {
        try {

            $query = "INSERT INTO  borrador_firmantes (ID_BORRADOR,FECHA_REGISTRO,
ID_USUARIO_FIRMANTE,FIRMANTE_CARGO,FIRMANTE_PRINCIPAL,radi_nume,tipo_validador)
 VALUES (";
            if ($id_borrador == false || $id_borrador == '') {
                $query .= "null";
            } else {
                $query .= $id_borrador;
            }
            $query .= ",$fecha_transaccion," . $id_firmante . ",'" . $cargo . "', " . $principal . "," . $radicado . ", " . $tipo_validador . " )";
            return $this->db->conn->execute($query);


        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    //actualiza el firmante principal
    public function updateFirmPrincip($id_borrador, $id_firmante)
    {
        try {
            if ($id_borrador != "") {
                $fecha_transaccion = $this->db->conn->OffsetDate(0, $this->db->conn->sysTimeStamp);
                $buscar = $this->getFirmByBorAndUser($id_borrador, $id_firmante);
                $usuario = $this->getUsuarioById($id_firmante);
                $cargo_firmante = $usuario[0]['USUA_CARGO'];

                if (count($buscar) > 0) {
                    $query = "update  borrador_firmantes set firmante_principal=0
 where id_borrador=" . $id_borrador . " ";
                    $this->db->conn->execute($query);
                } else {
                    //si entra aqui, es porque no consiguio el firmante, para este borrador, por lo cual, inserto una nueva fila
                    $this->guardarFirmantes($id_borrador, $fecha_transaccion, $id_firmante,
                        $cargo_firmante, 1, '', null);
                }


                $query = "update  borrador_firmantes set firmante_principal=1 
 where id_borrador=" . $id_borrador . " and id_usuario_firmante=" . $id_firmante . " ";

                $this->db->conn->execute($query);
            }

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    //actuliza firmantes por id_borrador y id_firmante
    public function updateFirmante($id_borrador, $id_firmante, $cargo)
    {
        try {
            if ($id_borrador != "") {
                $query = "update  borrador_firmantes set ID_BORRADOR=" . $id_borrador . " ,
ID_USUARIO_FIRMANTE=" . $id_firmante . ",FIRMANTE_CARGO='" . $cargo . "'
 where id_borrador=" . $id_borrador . " and id_usuario_firmante=" . $id_firmante . " ";

                //leactualizo el cargo en la tabla usuario

                $this->actualizaCargoUsuario($cargo,$id_firmante);

                return $this->db->conn->execute($query);
            }

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    //actualiza el firmante principal
    public function updateFirmtipo_validador($id_borrador, $id_firmante, $valor)
    {
        try {
            if ($id_borrador != "") {
                $fecha_transaccion = $this->db->conn->OffsetDate(0, $this->db->conn->sysTimeStamp);
                $buscar = $this->getFirmByBorAndUser($id_borrador, $id_firmante);
                $usuario = $this->getUsuarioById($id_firmante);
                $cargo_firmante = $usuario[0]['USUA_CARGO'];


                if (count($buscar) > 0) {
                    $query = "update  borrador_firmantes set tipo_validador='" . $valor . "'
 where id_borrador=" . $id_borrador . " and id_usuario_firmante=" . $id_firmante;
                    $this->db->conn->execute($query);

                } else {
                    //si entra aqui, guardo los valores nuevos
                    $this->guardarFirmantes($id_borrador, $fecha_transaccion, $id_firmante,
                        $cargo_firmante, 0, $valor, null);
                }

            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    //trae los firmantes d eun borrador, por borrador
    function getFirmByRadicado($radicado)
    {
        try {
            $query = "SELECT  borrador_firmantes.*,  u.usua_nomb || ' (' || d.depe_codi ||'-'|| d.depe_nomb || ')' AS NOMBRE,
u.usua_nomb,d.depe_codi,d.depe_nomb, u.id,u.usua_cargo
                FROM borrador_firmantes  join usuario u on  u.id=borrador_firmantes.id_usuario_firmante
                INNER JOIN dependencia d ON u.depe_codi = d.depe_codi
                where radi_nume=" . $radicado . "
                ORDER BY firmante_principal desc,  fecha_registro ASC";

            $borraExped = $this->db->conn->selectLimit($query, 10000)->getArray();

            return $borraExped;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    //trae los firmantes d eun borrador, por borrador
    function getFirmByBorrador($borrador)
    {
        try {
            $query = "SELECT  borrador_firmantes.*,  u.usua_nomb || ' (' || d.depe_codi ||'-'|| d.depe_nomb || ')' AS NOMBRE,
u.usua_nomb,d.depe_codi,d.depe_nomb, u.id,u.usua_cargo
                FROM borrador_firmantes  join usuario u on  u.id=borrador_firmantes.id_usuario_firmante
                INNER JOIN dependencia d ON u.depe_codi = d.depe_codi
                where id_borrador=" . $borrador . "
                ORDER BY id_borrador_firmante asc";

            $borraExped = $this->db->conn->selectLimit($query, 10000)->getArray();

            return $borraExped;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    //trae los firmantes d eun borrador, por borrador
    function getOnlyFirmnBorrador($borrador)
    {
        try {
            $query = "SELECT  borrador_firmantes.*, borrador_firmantes.id_usuario_firmante as id from  borrador_firmantes
                where id_borrador=" . $borrador . "
                ORDER BY firmante_principal desc,  fecha_registro ASC";

            $borraExped = $this->db->conn->selectLimit($query, 10000)->getArray();

            return $borraExped;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    //trae los firmantes d eun borrador, por borrador
    function getOnlyFirmByRad($radicado)
    {
        try {
            $query = "SELECT  borrador_firmantes.*, borrador_firmantes.id_usuario_firmante as id from  borrador_firmantes
                where radi_nume=" . $radicado . "
                ORDER BY firmante_principal desc,  fecha_registro ASC";


            $borraExped = $this->db->conn->selectLimit($query, 10000)->getArray();

            return $borraExped;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    //trae los firmantes d eun borrador, por borrador
    function getFirmByBorAndUser($borrador, $firmante)
    {
        try {
            $query = "SELECT  borrador_firmantes.*, u.usua_cargo, u.usua_nomb || ' (' || d.depe_codi ||'-'|| d.depe_nomb || ')' AS NOMBRE, u.id
                FROM borrador_firmantes  join usuario u on  u.id=borrador_firmantes.id_usuario_firmante
                INNER JOIN dependencia d ON u.depe_codi = d.depe_codi
                where id_borrador=" . $borrador . " and id_usuario_firmante=" . $firmante . "
                ORDER BY fecha_registro ASC";

            $borraExped = $this->db->conn->selectLimit($query, 10000)->getArray();

            return $borraExped;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    //ordena y retorna los usuarios que se usan para enviar comentarios en borradores
    function barmarUsuariosComen($usuarios)
    {
        $cont = 0;
        $usuarioeenviar = array();
        foreach ($usuarios as $usuario) {
            $usuarioeenviar[$cont] = $usuario;
            $usuarioeenviar[$cont]['id'] = $usuario['ID'];
            $usuarioeenviar[$cont]['text'] = $usuario['NOMBRE'];
            $cont++;
        }
        return $usuarioeenviar;
    }

    //borra todos los firmantes de un borrador
    public function borrarFirmanteByBorrador($borrador)
    {
        try {
            $query = "delete from borrador_firmantes where id_borrador=" . $borrador . " ";
            $response = $this->db->conn->execute($query);
            return $response;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    //borra todos los firmantes de un borrador
    public function borrarFirmanteByRad($radicado)
    {
        try {
            $query = "delete from borrador_firmantes where radi_nume=" . $radicado . " ";
            $response = $this->db->conn->execute($query);
            return $response;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    //trae el usuario o empresa o ciudadano encargado del radicado y la tabla donde puedo conseguir sus datos
    function getUserDelRadic($radicado)
    {
        try {
            $query = "Select
  ( Case When cast(min(d.sgd_oem_codigo) as text) is not null Then cast(min(d.sgd_oem_codigo) as text)   --Id Tabla: sgd_oem_oempresas
        	When cast(min(d.sgd_ciu_codigo) as text) is not null Then cast(min(d.sgd_ciu_codigo) as text)    --Id Tabla: sgd_ciu_ciudadano
        	When min(d.sgd_doc_fun) is not null Then min(d.sgd_doc_fun)                                      --Id Tabla: usuario
        	Else 'NotFound'
	End ) As Codigo
, ( Case 	When min(d.sgd_oem_codigo) is not null Then 'sgd_oem_oempresas'
    	When min(d.sgd_ciu_codigo) is not null Then 'sgd_ciu_ciudadano'
    	When min(d.sgd_doc_fun)  is not null Then 'usuario'
	End ) As Tabla, d.id as sgd_dir_drecciones_id
From radicado r Inner Join sgd_dir_drecciones d On r.radi_nume_radi=d.radi_nume_radi
Where
sgd_dir_tipo < 700
And r.radi_nume_radi=" . $radicado . " group by d.id
";

            $borraExped = $this->db->conn->selectLimit($query, 1)->getArray();
            return $borraExped;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }


    //trae un usuario especifico, tal cual como lo mestran en el campo de texto, en el campo destinatario, en borrrador
    function getUserForDest($usuario)
    {
        $query = "Select '4' As TIPO_DESTINATARIO, 'Interno |' As TIPO_RADICADO, fun.id As ID, fun.usua_nomb As NOMBRE,
'CC '||fun.usua_doc IDENTIFICACION, d.depe_nomb As DIRECCION, fun.usua_login As TELEFONO
From usuario fun Inner Join dependencia d On fun.depe_codi=d.depe_codi
Where fun.usua_esta='1' and  fun.usua_doc='" . $usuario . "' ";


        $borraExped = $this->db->conn->selectLimit($query, 1)->getArray();
        return $borraExped;
    }

    //trae un usuario especifico, tal cual como lo mestran en el campo de texto, en el campo destinatario, en borrrador
    function getEmpresaForDest($id, $dreccion_id)
    {
        $query = "Select '2' As TIPO_DESTINATARIO, 'Externo|Empresa' AS TIPO_RADICADO, emp.sgd_oem_codigo As ID, 
emp.SGD_OEM_OEMPRESA As NOMBRE,
(Case When emp.sgd_oem_nit !='' Then 'Nit '||emp.sgd_oem_nit End) As IDENTIFICACION,
emp.sgd_oem_direccion As DIRECCION, emp.sgd_oem_telefono As TELEFONO, 
sgd_dir_drecciones.sgd_dir_persona_cargo as PERSONACARGO,sgd_dir_drecciones.sgd_dir_nombre
From sgd_oem_oempresas emp 
left Join sgd_dir_drecciones On sgd_dir_drecciones.sgd_oem_codigo=emp.sgd_oem_codigo
where (sgd_oem_inactivo='0' Or sgd_oem_inactivo is null ) and  emp.sgd_oem_codigo=" . $id . " and sgd_dir_drecciones.id=$dreccion_id  ";

        $borraExped = $this->db->conn->selectLimit($query, 1)->getArray();
        return $borraExped;
    }

    //trae un usuario especifico, tal cual como lo mestran en el campo de texto, en el campo destinatario, en borrrador
    function getCiudadanoForDest($id)
    {
        $query = "Select '1' As TIPO_DESTINATARIO, 'Externo|Ciudadano' As TIPO_RADICADO, ciu.sgd_ciu_codigo As ID, ciu.sgd_ciu_nombre || ' ' || sgd_ciu_apell1 || ' ' || sgd_ciu_apell2 As NOMBRE,
(Case When ciu.sgd_ciu_cedula !='' Then 'CC '||ciu.sgd_ciu_cedula End) As IDENTIFICACION, ciu.sgd_ciu_direccion As DIRECCION, ciu.sgd_ciu_telefono As TELEFONO
From sgd_ciu_ciudadano ciu
Where (sgd_ciu_inactivo='0' or sgd_ciu_inactivo is null) and  ciu.sgd_ciu_codigo=" . $id . " ";
        $borraExped = $this->db->conn->selectLimit($query, 1)->getArray();
        return $borraExped;
    }


    //trae todos los datos de la tabla seguridad
    function getAllFromSeguridad()
    {
        try {
            $query = "SELECT  * from seguridad";
            $borraExped = $this->db->conn->selectLimit($query, 10000)->getArray();
            return $borraExped;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    //trae todos los datos de la tabla seguridad
    function getCountSeguridad()
    {
        try {
            $query = "SELECT count(ID) as contador from seguridad";
            $borraExped = $this->db->conn->selectLimit($query, 10000)->getArray();
            return $borraExped;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    //actualiza todos los punto y coma a coma, ya qu a veces se guardaban con ;
    function updatePuntoyComaToComa()
    {
        try {
            $query = "UPDATE seguridad
SET usuarios_permitidos = REPLACE(usuarios_permitidos, ';', ',')
WHERE usuarios_permitidos LIKE '%;%';";
            $response = $this->db->conn->execute($query);
            return $response;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function borrarAllSegurodad($datos)
    {
        try {
            $query = "delete from seguridad";
            //executing
            $response = $this->db->conn->execute($query);
            return $response;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    //actualiza as dos columas de la tabla seguridad a INTEGER
    public function updateColumSegToInt($datos)
    {
        try {
            $query = "ALTER TABLE seguridad ALTER COLUMN usuarios_permitidos TYPE integer USING (trim(usuarios_permitidos)::integer);";
            $response = $this->db->conn->execute($query);

            $query = "ALTER TABLE seguridad ALTER COLUMN usuario_asegurador TYPE integer USING (trim(usuario_asegurador)::integer);";
            $response = $this->db->conn->execute($query);
            return $response;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    function insertSeguridad($radi, $usuodepe, $asegurador, $tipo_usu)
    {
        try {
            $query = "INSERT INTO  seguridad (RADI_NUME_RADI,USUARIOS_PERMITIDOS,TIPO_USU_PERMITIDO ";

            if ($asegurador != "") {
                $query .= ",USUARIO_ASEGURADOR";
            }
            $query .= ") VALUES ('" . $radi . "', " . $usuodepe . ",'" . $tipo_usu . "' ";

            if ($asegurador != "") {
                $query .= ", " . $asegurador . " ";
            }
            $query .= ")";

            $response = $this->db->conn->execute($query);
            return $response;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    //rverifica si ya existe un anexo para un radicado padre e hijo
    function getAnexByRadByAsod($radicadopadre, $radicadohijo)
    {
        $query = "select * from anexos where anex_radi_nume=" . $radicadopadre . " and radi_nume_salida=" . $radicadohijo . " ";
        $borraExped = $this->db->conn->selectLimit($query, 10000)->getArray();
        return $borraExped;
    }

    //actualiza un radicado asociado a radicado origen
    public function seleccionarOrigen($borrador_id, $radicado_asociado)
    {
        try {
            //actualizo a los demas a 0 para el que venga lo pongo en 1
            $query = "update borrador_radicados_asociados set radicado_origen=0
                where borrador_radicados_asociados.id_borrador='" . $borrador_id . "' ";

            $this->db->conn->execute($query);

            $query = "update borrador_radicados_asociados set radicado_origen=1
                where borrador_radicados_asociados.id_borrador='" . $borrador_id . "' and radicado_asociado=" . $radicado_asociado . " ";

            return $this->db->conn->execute($query);

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    //verifica si existe radicado asocaido origen, para un determinado borrador
    public function getTitulosTratamiento()
    {
        try {
            $query = "SELECT  *
                FROM titulo_tratamiento where activo=1 order by orden ";

            return $this->db->conn->selectLimit($query, 10000)->getArray();

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    function getDropdownEnviarByBorr($dependencia,$borrador = NULL)
    {

        try {
            $query = "select * from (select usuario.id, usuario.usua_codi, usuario.depe_codi,usuario.usua_nomb,
 usuario.usua_nomb || ' (' || d.depe_codi ||'-'|| d.depe_nomb || ')' AS NOMBRE
 from usuario INNER JOIN dependencia d ON usuario.depe_codi = d.depe_codi
  where usuario.depe_codi= " . $dependencia . "
  And (usuario.usua_codi=1 Or usuario.usua_perm_aprobar=1) And usua_esta='1' 
  and usuario.id<>  " . $_SESSION["usuario_id"] . "
  OR (d.depe_codi=" . $_SESSION["depe_codi_padre"] . " AND usuario.usua_codi=1 AND usuario.usua_esta='1') 
union  
select usu.id, usu.usua_codi, usu.depe_codi,usu.usua_nomb,
 usu.usua_nomb || ' (' || d.depe_codi ||'-'|| d.depe_nomb || ')' AS NOMBRE 
from usuario usu INNER JOIN dependencia d ON usu.depe_codi = d.depe_codi 
inner join borrador_historicos bh on usu.id = bh.id_usuario_destino
where bh.id_borrador = " . $borrador . " 
union  
select usua.id, usua.usua_codi, usua.depe_codi,usua.usua_nomb,
 usua.usua_nomb || ' (' || d.depe_codi ||'-'|| d.depe_nomb || ')' AS NOMBRE 
from usuario usua INNER JOIN dependencia d ON usua.depe_codi = d.depe_codi 
inner join borradores br on usua.id = br.id_destinatario
where br.id_borrador = " . $borrador . " ) T
ORDER BY T.depe_codi DESC, T.usua_codi ASC";

            return $this->db->conn->selectLimit($query, 10000)->getArray();

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }


    function getDropdownEnviarA($dependencia )
    {

        try {
            $query = "select usuario.id, usuario.usua_codi, 
usuario.depe_codi,usuario.usua_nomb, usuario.usua_nomb || ' (' || d.depe_codi ||'-'|| d.depe_nomb || ')' AS NOMBRE 
from usuario INNER JOIN dependencia d ON usuario.depe_codi = d.depe_codi 
where usuario.depe_codi= " . $dependencia . " 
And (usuario.usua_codi=1 Or usuario.usua_perm_aprobar=1) 
And usua_esta='1' and usuario.id<>  " . $_SESSION["usuario_id"] . " OR (d.depe_codi=" . $_SESSION["depe_codi_padre"] . " AND usuario.usua_codi=1 AND usuario.usua_esta='1') 
            ORDER BY usuario.depe_codi DESC, usuario.usua_codi ASC";
            //return $query;
            return $this->db->conn->selectLimit($query, 10000)->getArray();

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    //busca un usuario especifico, para el select de usuarios Enviar
    function userAdicionalSelectEnviar($usuarios)
    {
        try {

            $query = "SELECT
  u.usua_nomb || ' (' || d.depe_codi ||'-'|| d.depe_nomb || ')' AS NOMBRE, u.id,usua_codi, u.usua_cargo,
  u.usua_doc, u.usua_nomb, u.usua_email, 
d.depe_nomb,d.depe_codi,
  (CASE
	WHEN u.usua_codi=1 THEN 'Jefe' 
	ELSE ''
	END)	AS ROL
FROM
  usuario u INNER JOIN dependencia d ON u.depe_codi = d.depe_codi
WHERE u.id in(" . $usuarios. ")
  AND u.usua_esta='1'
ORDER BY u.usua_codi ASC, u.depe_codi ASC
";
            return $this->db->conn->selectLimit($query, 10000)->getArray();

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    function getHistoricosByBorrador($id_borrador, $order_by = 'borrador_historicos.fecha_transaccion')
    {
        $query = "SELECT  borrador_historicos.*, usuario.usua_login,usuario.usua_codi, usuario.usua_cargo
                  FROM borrador_historicos  
                    join borradores on (borradores.id_borrador=borrador_historicos.id_borrador)
                    left join usuario on (usuario.id=borrador_historicos.id_usuario_origen)
                  where borrador_historicos.id_borrador=" . $id_borrador . " 
                  ORDER BY " . $order_by . " ASC";

        return $this->db->conn->selectLimit($query, 10000)->getArray();
    }

    //$quearmo: indica que estoy armando, por si es borrador, coloco el codigo de barra que tengo por defecto,
    //de resto, coloco el codigo de batrra segun el radicado.
    //$noRad: puede ser el numero de radicado o el id de borrador
    function armarPdfRadicado($quearmo, $noRad, $borrador, $datosDestinatario, $firmantes, $titulomemo, $PDFA = false, $imprimefirmantes = '',
                              $fecharadicado = false)
    {

        if ($fecharadicado == false) {
            $fecharadicado = date('Y-m-d H:i');
        }

        $firmante_principal = array();

        foreach ($firmantes as $firmante) {
            if ($firmante['FIRMANTE_PRINCIPAL'] == 1) {
                $firmante_principal = $firmante;
            }
        }

        /*
         $header->addPreserveText("Radicado: " . $noRad . " del " . date('d-m-Y H:i'),
             array('color' => '150603'), array('align' => 'right', 'size' => 8));*/

        $ruta_raiz = "../../";
        if (!class_exists('mPDF')) {
            include($ruta_raiz . "mpdf/mpdf.php");
        }

        ////////////////////////////////GENERO UN DOCUMENTO PARA EL RADICADO//////////////////////////////////////
        // Create an instance of the class:
        
        $mpdf = new mPDF('utf-8', 'Letter');
        $stylesheet = '<style> table > tbody > tr > td > div > p{ text-align : center !important; } </style>'; 
        $mpdf->WriteHTML($stylesheet,1);
        if ($PDFA == true) {
            $mpdf->SetImportUse();
            $mpdf->PDFAauto = true;
        }

        $mpdf->useFixedNormalLineHeight = true;
        $mpdf->useFixedTextBaseline = true;
        $mpdf->normalLineheight = 1.33;
        $fuente = $_SESSION['EDITOR_FUENTE_TIPO'] != 'SELECCIONE' ? "'" . $_SESSION['EDITOR_FUENTE_TIPO'] . "'" : "'Arial'";


        $html = '<table width="100%"><tbody><tr>
<td style="width: 33%" ></td>
<td style="width: 33%"><div style="text-align:center;float: right;  margin-bottom: -5px;" >
<img  style="width:190px;height: 120px" src="' . $ruta_raiz . 'app/img/encabezado_imagen_logo_s.png"/>
</div></td>

<td style="width: 33%; text-align:right;float: right">';

        $txt_ref_cod_barra = $noRad;
        if ($quearmo == 'borrador') {
            $txt_ref_cod_barra = '###Borrador' . $noRad . '###';

        } else if ($quearmo == 'radicado') {
            $html .= '<div style="text-align:right;float: right;  margin-bottom: -5px;" >
<span style="font-family:kode39;font-size:25px;width:300px;height: 25px">' . $noRad . '</span>
</div>';
        }

        $html .= '
<div style="text-align:right;float: right;  margin-right: -5px; margin-left: 5px" >
<span style="font-size: 8pt; font-family: ' . $fuente . '">Radicado: <strong style="font-size: 10pt;">' . $txt_ref_cod_barra . '
</strong></span><br>
<span style="font-size: 8pt; font-family: ' . $fuente . '">Fecha</span> ' . date('d-m-Y H:i', strtotime($fecharadicado)) . ' </div>
 <div style="text-align:right;float: right;  margin-right: -10px; font-size: 10" >P&aacute;g. {PAGENO} de {nb}</div>
 </td>
 </tr>
</tbody>
</table>';


        $mpdf->SetHTMLHeader($html);
        $mpdf->SetHTMLFooter('<img style="width:620px;height: 100px;margin-bottom: 20px" src="' . $ruta_raiz . 'app/img/pie_de_pagina_imagen.png"/>');
// Write some HTML code:

        $mpdf->AddPage('', // L - landscape, P - portrait
            '', '', '', '',
            20, // margin_left
            20, // margin right
            40, // margin top
            30, // margin bottom
            0, // margin header
            1); // margin footer
        // $mpdf->WriteHTML('');

        setlocale(LC_TIME, array('es_ES.UTF-8', 'es_ES', 'es-ES', 'es', 'spanish', 'Spanish'));
        $mes = strftime("%B");

        //busco el mes en espanl
        $mes = $this->mesespanol($mes);

        $mpdf->WriteHTML('<style type="text/css"> 
body { 
        font-family: Arial, Helvetica, sans-serif; 
 font-size: 12pt;
            text-align: justify !important; 
            line-height: 1 !important;
}
</style>');


        if ($borrador[0]['TIPO_BORRADOR_DEST'] == $_SESSION['INTERNA_COMUNICACION'] && $titulomemo != NULL
            && strtolower($titulomemo) != strtolower('Sin titulo')
            && strtolower($titulomemo) != strtolower('Sin título')) {

            $mpdf->WriteHTML("<div style='text-align: center; align-content: center'><span >" . $titulomemo . "</span></div><br>");
        }

        $mpdf->WriteHTML("Bogotá, D.C, " . date('d') . " de " . $mes . " de " . date("Y") . "");
        $mpdf->WriteHTML("<br>");
        $mpdf->WriteHTML("<br>");
        $mpdf->WriteHTML($borrador[0]['TIPOPERSONA']);
        if ($borrador[0]['TIPO_DESTINATARIO'] == 2) {

            if ($datosDestinatario['nombre_persona'] != "" && $datosDestinatario['nombre_persona'] != null) {
                $mpdf->WriteHTML($datosDestinatario['nombre_persona']);
            }

            if ($datosDestinatario['cargo'] != "" && $datosDestinatario['cargo'] != null) {
                $mpdf->WriteHTML($datosDestinatario['cargo']);
            }
            

        }


        if ($borrador[0]['TIPO_BORRADOR_DEST'] == $_SESSION['SALIDA_OFICIO']) {
            $mpdf->WriteHTML($datosDestinatario['nomdestinatario']);
            $mpdf->WriteHTML($datosDestinatario['direccion']);
            if ($datosDestinatario['email'] != "" && $datosDestinatario['email'] != null) {
                $mpdf->WriteHTML($datosDestinatario['email']);
            }
            if ($datosDestinatario['departamento'] != '' && $datosDestinatario['departamento'] != null) {
                $mpdf->WriteHTML($datosDestinatario['departamento']);
            }


        } else {

            $mpdf->WriteHTML("<table width=\"100%\" style='border-collapse:separate; 
                border-spacing:0 -5px; '><tbody><tr><td align=\"left\" width='10%'>PARA:</td><td align=\"left\" width='40%'>" . mb_strtoupper($datosDestinatario['nomdestinatario'], 'UTF-8') . "</td></tr>");

            $mpdf->WriteHTML("<tr><td align=\"left\" width='10%'></td><td align=\"left\" width='90%'>" . $borrador[0]['CARGO_PERSONA'] . " " . $datosDestinatario['dependencia_name'] . "</td></tr>");


            if (count($firmantes) > 0) {
                $cont = 0;

                foreach ($firmantes as $firmante) {
                    if ($firmante['TIPO_VALIDADOR'] == 'Remitente') {
                        $text = '';
                        $strpad_length = 15;
                        if ($cont < 1) {
                            $text = 'DE:';
                            $strpad_length = 11;
                        }

                        $mpdf->WriteHTML("<tr><td><br></tr></td><tr><td align=\"left\" width='10%'>" . str_pad($text, $strpad_length) . "</td>
<td align=\"left\" width='90%'>" . mb_strtoupper($firmante['USUA_NOMB'], 'UTF-8') . "</td></tr>");
                        $text = '';
                        $strpad_length = 15;
                        $mpdf->WriteHTML("<tr><td align=\"left\" width='10%'>" . str_pad($text, $strpad_length) . "</td><td align=\"left\" width='90%'>" . $firmante['USUA_CARGO'] . " " . $firmante['DEPE_NOMB'] . "</td></tr>");
                        $cont++;
                    }
                }
            }
            $mpdf->WriteHTML("</tbody></table>");
        }

        $mpdf->WriteHTML("<br>");
        $asunto = "ASUNTO: " . $borrador[0]['ASUNTO'];
        $mpdf->WriteHTML($asunto);
        $mpdf->WriteHTML("<br>");
        $html = "";
        if ($borrador[0]['DOCUMENTO_WEB_CUERPO'] != "") {
            $html = $borrador[0]['DOCUMENTO_WEB_CUERPO'];
        }


        $mpdf->WriteHTML($html);
        $mpdf->WriteHTML("<table width=\"100%\" style='border-collapse:separate; 
                border-spacing:0 -5px; '><tbody>");


        if ($this->validarImprimeFirmantes($quearmo, $noRad, $imprimefirmantes) == true) {
            $cont = 0;
            foreach ($firmantes as $firmante) {

                if (!isset($firmantes[$cont]['mostrado'])) {

                    if ($firmante['TIPO_VALIDADOR'] == 'Remitente') {
                        $mpdf->WriteHTML("<tr ><td align=\"left\" width='50%'>" . strtoupper($firmante['USUA_NOMB']) . "</td>");

                        if (isset($firmantes[$cont + 1]) && !isset($firmantes[$cont + 1]['mostrado'])) {
                            $mpdf->WriteHTML("<td align=\"left\" width='50%'>" . strtoupper($firmantes[$cont + 1]['USUA_NOMB']) . "</td></tr>");
                        } else {
                            $mpdf->WriteHTML("</tr>");
                        }

                        $mpdf->WriteHTML("<tr ><td align=\"left\" width='50%'>" . $firmante['FIRMANTE_CARGO'] . "</td>");

                        if (isset($firmantes[$cont + 1]) && !isset($firmantes[$cont + 1]['mostrado'])) {
                            $mpdf->WriteHTML("<td align=\"left\" width='50%'>" . $firmantes[$cont + 1]['FIRMANTE_CARGO'] . "</td></tr>");
                        } else {
                            $mpdf->WriteHTML("</tr>");
                        }

                        $htmlultimalinea = "<tr ><td align=\"left\" width='50%'>" . $firmante['DEPE_NOMB'] . "</td>";

                        if (isset($firmantes[$cont + 1]) && ($cont + 2) < count($firmantes)) {
                            $htmlultimalinea .= "<br><br><br><br>";
                        } else {
                            $htmlultimalinea .= "<br><br>";
                        }
                        $mpdf->WriteHTML($htmlultimalinea);

                        if (isset($firmantes[$cont + 1]) && !isset($firmantes[$cont + 1]['mostrado'])) {
                            //$mpdf->WriteHTML("<td align=\"left\" width='50%'>".$firmantes[$cont+1]['DEPE_NOMB']."</td><br><br><br><br></tr>");
                            $htmlultimalinea = "<td align=\"left\" width='50%'>" . $firmantes[$cont + 1]['DEPE_NOMB'] . "</td>";

                            if (isset($firmantes[$cont + 1]) && ($cont + 2) < count($firmantes)) {
                                $htmlultimalinea .= "<br><br><br><br>";
                            } else {
                                $htmlultimalinea .= "<br><br>";
                            }
                            $mpdf->WriteHTML($htmlultimalinea);
                        } else {

                            $mpdf->WriteHTML("</tr>");
                        }

                        $firmantes[$cont]['mostrado'] = true;
                        if (isset($firmantes[$cont + 1])) {
                            $firmantes[$cont + 1]['mostrado'] = true;
                        }
                    }
                }
                $cont++;
            }
        }


        $mpdf->WriteHTML("</tbody></table><br>");


        return $mpdf;

    }

    //genera el docx del radicado, que viene siendo el primer anexo
    function armarDocxRadicado($quearmo, $noRad, $borrador, $datosDestinatario,
                               $phpWord, $firmantes, $titulomemo, $imprimefirmantes = '')
    {

        $firmante_principal = array();

        foreach ($firmantes as $firmante) {
            if ($firmante['FIRMANTE_PRINCIPAL'] == 1) {
                $firmante_principal = $firmante;
            }
        }


        $asunto = "ASUNTO: " . $borrador[0]['ASUNTO'];
        $mes = strftime("%B");
        //busco el mes en espanlo
        $mes = mesespanol($mes);

        $section = $phpWord->addSection(["paperSize" => "Letter"]);

        if ($borrador[0]['TIPO_BORRADOR_DEST'] == $_SESSION['INTERNA_COMUNICACION']
            && strtolower($titulomemo) != strtolower('Sin titulo') && strtolower($titulomemo) != strtolower('Sin título')) {
            $phpWord->addParagraphStyle('p2Style', array('align' => 'center'));
            $section->addText($titulomemo, 'r2Style', 'p2Style');
        }
        $section->addTextBreak();
        $section->addText("Bogotá, D.C, " . date('d') . " de " . $mes . " de " . date("Y"));
        $section->addTextBreak();
        $section->addText($borrador[0]['TIPOPERSONA']);
        if ($borrador[0]['TIPO_DESTINATARIO'] == 2) {

            if ($datosDestinatario['nombre_persona'] != "" && $datosDestinatario['nombre_persona'] != null) {
                $section->addText($datosDestinatario['nombre_persona']);
            }

            if ($datosDestinatario['cargo'] != "" && $datosDestinatario['cargo'] != null) {
                $section->addText($datosDestinatario['cargo']);
            }

        }

        if ($borrador[0]['TIPO_BORRADOR_DEST'] == $_SESSION['SALIDA_OFICIO']) {
            $section->addText($datosDestinatario['nomdestinatario']);
            $section->addText($datosDestinatario['direccion']);
            if ($datosDestinatario['departamento_dest'] != '' && $datosDestinatario['departamento_dest'] != null) {
                $section->addText($datosDestinatario['departamento_dest']);
            }
        } else {
            $section->addText("PARA:       " . mb_strtoupper($datosDestinatario['nomdestinatario'], 'UTF-8'));//7 espacios en blanco
            $section->addText("                 " . $borrador[0]['CARGO_PERSONA'] . " " . $datosDestinatario['dependencia_name']); //17 espacios en blanco


            if (count($firmantes) > 0) {
                $cont = 0;
                $section->addTextBreak();

                foreach ($firmantes as $firmante) {
                    if ($firmante['TIPO_VALIDADOR'] == 'Remitente') {
                        $text = '';
                        $strpad_length = 16;
                        if ($cont < 1) {
                            $text = 'DE:';
                            $strpad_length = 14;
                        }
                        if ($cont > 0) {
                            $text = '';
                            $strpad_length = 17;
                        }
                        $section->addText(str_pad($text, $strpad_length) . "" . mb_strtoupper($firmante['USUA_NOMB'], 'UTF-8'));
                        $text = '';
                        $strpad_length = 16;
                        if ($cont > 0) {
                            $text = '';
                            $strpad_length = 17;
                        }
                        if ($firmante['USUA_CARGO'] == '' || $firmante['USUA_CARGO'] == null) {
                            $strpad_length = 14;
                            if ($cont > 0) {
                                $text = '';
                                $strpad_length = 15;
                            }
                        }


                        $section->addText(str_pad($text, $strpad_length) . " " . $firmante['USUA_CARGO'] . " " . $firmante['DEPE_NOMB']);
                        $cont++;
                    }

                }
            }
        }


        $section->addTextBreak();
        $section->addTextBreak();


        $section->addText($asunto);
        $section->addTextBreak();
        $section->addTextBreak();
        $html = "";
        if ($borrador[0]['DOCUMENTO_WEB_CUERPO'] != "") {

            $html = $borrador[0]['DOCUMENTO_WEB_CUERPO'];

            $html = stripslashes($html);

            $doc = new DOMDocument();
            $html = $doc->loadHTML($html);
            $html = $doc->getElementsByTagName('body')->item(0)->C14N();
        }

        \PhpOffice\PhpWord\Shared\Html::addHtml($section, $html);

        $section->addTextBreak();


        $tableStyle = array(
            'unit' => \PhpOffice\PhpWord\Style\Table::WIDTH_PERCENT,
            'width' => 100 * 100,
        );

        $phpWord->addTableStyle('myTable', $tableStyle);
        $table = $section->addTable();

        if ($this->validarImprimeFirmantes($quearmo, $noRad, $imprimefirmantes) == true) {
            $cont = 0;
            foreach ($firmantes as $firmante) {
                if (!isset($firmantes[$cont]['mostrado'])) {

                    if ($firmante['TIPO_VALIDADOR'] == 'Remitente') {
                        $table->addRow();
                        $table->addCell(5000)->addText(strtoupper($firmante['USUA_NOMB']));

                        if (isset($firmantes[$cont + 1]) && !isset($firmantes[$cont + 1]['mostrado'])) {
                            $table->addCell(5000)->addText(strtoupper($firmantes[$cont + 1]['USUA_NOMB']));
                        } else {
                        }
                        $table->addRow();
                        $table->addCell(5000)->addText(trim($firmante['FIRMANTE_CARGO']));

                        if (isset($firmantes[$cont + 1]) && !isset($firmantes[$cont + 1]['mostrado'])) {
                            $table->addCell(5000)->addText($firmantes[$cont + 1]['FIRMANTE_CARGO']);
                        } else {

                        }
                        $table->addRow();
                        $table->addCell(5000)->addText(trim($firmante['DEPE_NOMB']));

                        if (isset($firmantes[$cont + 1]) && !isset($firmantes[$cont + 1]['mostrado'])) {
                            $table->addCell(5000)->addText($firmantes[$cont + 1]['DEPE_NOMB']);
                        } else {
                        }

                        $firmantes[$cont]['mostrado'] = true;
                        if (isset($firmantes[$cont + 1])) {
                            $firmantes[$cont + 1]['mostrado'] = true;
                        }

                        if ((!isset($firmantes[$cont + 1])) || (isset($firmantes[$cont + 1]) && $cont + 2 >= count($firmantes))) {
                            $table->addRow();
                            $table->addCell(5000)->addText('');
                            $table->addCell(5000)->addText('');
                        } else {
                            $table->addRow();
                            $table->addCell(5000)->addText('');
                            $table->addCell(5000)->addText('');
                            $table->addRow();
                            $table->addCell(5000)->addText('');
                            $table->addCell(5000)->addText('');
                            $table->addRow();
                            $table->addCell(5000)->addText('');
                            $table->addCell(5000)->addText('');
                        }
                    }
                }

                $cont++;
            }
        }

        return $section;

    }


    function armarAnexosFolInfo($libreria, $name_library = 'mpdf', $borrador,$helperBorra)
    {

        $texto = "";

        $fuente_feditor = $_SESSION['EDITOR_FUENTE_TIPO'] != 'SELECCIONE' ? $_SESSION['EDITOR_FUENTE_TIPO'] : "Arial";
        if ($borrador[0]['FOLIOS_ANEXOS'] != ""
            && $borrador[0]['FOLIOS_ANEXOS'] != null
            && $borrador[0]['FOLIOS_ANEXOS'] > 0) {

            if ($name_library == 'mpdf') {
                $texto = "<br>";
            }

            $texto .= "<span style='font-size: 8pt; font-family: " . $fuente_feditor . " '>Anexos: " . $borrador[0]['FOLIOS_ANEXOS']  . " folios";

            if ($borrador[0]['DESCRIPCION_ANEXOS'] != null && $borrador[0]['DESCRIPCION_ANEXOS'] != "null" &&
                $borrador[0]['DESCRIPCION_ANEXOS'] != "NULL" && $borrador[0]['DESCRIPCION_ANEXOS'] != "") {

                $texto .= ", " . $borrador[0]['DESCRIPCION_ANEXOS'] . "</span>";
            } else {
                $texto .= "</span>";
            }

        } elseif ($borrador[0]['DESCRIPCION_ANEXOS'] != "" && $borrador[0]['DESCRIPCION_ANEXOS'] != null
            && $borrador[0]['DESCRIPCION_ANEXOS'] != "NULL" && $borrador[0]['DESCRIPCION_ANEXOS'] != "null") {
            if ($name_library == 'mpdf') {
                $texto = "<br>";
            }


            $texto .= "<span style='font-size: 8pt; font-family: " . $fuente_feditor . " '>Anexos: " . $borrador[0]['DESCRIPCION_ANEXOS'] . "</span>";
        }

        $texto=$helperBorra->TildesHtml($texto);

        if ($texto != '') {

            if ($name_library == 'mpdf') {
                $texto .= "<br>";
                $libreria->WriteHTML($texto);
            } else if ($name_library == 'phpdocx') {
                $texto .= "<br><br>";
                $libreria->embedHTML($texto);
            } else {
                $libreria->addTextBreak();
                //phpword
                $html = $texto;
                $doc = new DOMDocument();
                $html = $doc->loadHTML($html);
                $html = $doc->getElementsByTagName('body')->item(0)->C14N();
                \PhpOffice\PhpWord\Shared\Html::addHtml($libreria, $html);
                $libreria->addTextBreak();
            }
        }

        return $libreria;

    }

    function obtenerCiudad($tabla, $continente, $pais, $departamento, $municipio, $db)
    {
        $query = "SELECT  dir.id_cont, cont.nombre_cont, dir.id_pais, pais.nombre_pais, dir.dpto_codi, dep.dpto_nomb, dir.muni_codi, mun.muni_nomb
FROM $tabla dir
INNER JOIN sgd_def_continentes cont ON dir.id_cont=cont.id_cont
INNER JOIN sgd_def_paises pais ON dir.id_pais=pais.id_pais AND pais.id_cont=cont.id_cont
INNER JOIN departamento dep ON dep.dpto_codi=dir.dpto_codi AND dep.id_pais=pais.id_pais AND dep.id_cont=cont.id_cont
INNER JOIN municipio mun ON mun.muni_codi=dir.muni_codi AND mun.dpto_codi=dep.dpto_codi AND mun.id_pais=pais.id_pais AND mun.id_cont=cont.id_cont
WHERE 
mun.muni_codi=$municipio and dep.dpto_codi=$departamento and pais.id_pais=$pais and cont.id_cont=$continente 
";

        return $db->conn->selectLimit($query, 50)->getArray();

    }

    function datosDestinatario($borrador, $db)
    {
        ///////guardo la direccion, y busco los datos en las tablas, dependiendo del tipo de radicado
        $nomdestinatario = "";
        $direccion = "";
        $nombre_persona = $borrador[0]['NOMBRE_PERSONA'];
        $cargo = $borrador[0]['CARGO_PERSONA'];
        $departamento = "";
        $dependencia_id = '';
        $dependencia_name = '';
        $vemail = '';
        if ($borrador[0]['TIPO_DESTINATARIO'] == 2) {
            //Externo | Empresa sgd_oem_oempresas

            $query = "SELECT  *
                FROM sgd_oem_oempresas  
                where sgd_oem_codigo=" . $borrador[0]['ID_DESTINATARIO'] . " ";

            $usuario = $db->conn->selectLimit($query, 1)->getArray();

            $nomdestinatario = $usuario[0]['SGD_OEM_OEMPRESA'];
            $direccion = $usuario[0]['SGD_OEM_DIRECCION'] . "";
            if ($usuario[0]['SGD_OEM_TELCELULAR'] != "") {
                $direccion .= ", Tel. " . $usuario[0]['SGD_OEM_TELCELULAR'];
            }

            if ($usuario[0]['SGD_OEM_TELEFONO'] != "") {
                $direccion .= ", Tel. " . $usuario[0]['SGD_OEM_TELEFONO'];
            }

            $vemail  = $usuario[0]['SGD_OEM_EMAIL'];



            $buscarPaisDepEtc = $this->obtenerCiudad('sgd_oem_oempresas', $usuario[0]['ID_CONT'], $usuario[0]['ID_PAIS'],
                $usuario[0]['DPTO_CODI'], $usuario[0]['MUNI_CODI'], $db);

            if (count($buscarPaisDepEtc) > 0) {
                $departamento = $buscarPaisDepEtc[0]['MUNI_NOMB'];

                if ($buscarPaisDepEtc[0]['DPTO_NOMB'] != $buscarPaisDepEtc[0]['MUNI_NOMB']) {
                    $departamento = $buscarPaisDepEtc[0]['MUNI_NOMB'] . ', ' . $buscarPaisDepEtc[0]['DPTO_NOMB'];
                }
                $departamento = $departamento . ', ' . $buscarPaisDepEtc[0]['NOMBRE_PAIS'];
            }

        } elseif ($borrador[0]['TIPO_DESTINATARIO'] == 4) {
            //Interno | Funcionario usuario


            $query = "SELECT  *
                FROM usuario  
                where id=" . $borrador[0]['ID_DESTINATARIO'] . " ";

            $usuario = $db->conn->selectLimit($query, 1)->getArray();

            $nomdestinatario = $usuario[0]['USUA_NOMB'];

            $vemail  = $usuario[0]['USUA_EMAIL'];

            $query = "Select d.depe_nomb dir, d.id depe_id From usuario u
                join dependencia d on(d.depe_codi=u.depe_codi)
                Where u.depe_codi=d.depe_codi And
                u.usua_doc='" . $usuario[0]['USUA_DOC'] . "' ";

            $direc = $db->conn->selectLimit($query, 1)->getArray();

            $dependencia_id = $direc[0]['DEPE_ID'];
            $dependencia_name = $direc[0]['DIR'];

            $direccion = $direc[0]['DIR'] . "";
            if ($usuario[0]['USUA_CELULAR'] != "") {
                $direccion .= ", Tel. " . $usuario[0]['USUA_CELULAR'];
            }

            $query = "SELECT  *
                FROM departamento  
                where dpto_codi=11 ";

            $querydepartamento = $db->conn->selectLimit($query, 1)->getArray();
            $departamento = $querydepartamento[0]['DPTO_NOMB'] . ', Colombia';

        } else {
            //Externo | Ciudadano sgd_ciu_ciudadano 1 1

            $query = "SELECT  *
                FROM sgd_ciu_ciudadano  
                where sgd_ciu_codigo=" . $borrador[0]['ID_DESTINATARIO'] . " ";

            $usuario = $db->conn->selectLimit($query, 1)->getArray();

            $nomdestinatario = $usuario[0]['SGD_CIU_NOMBRE'] . " " . $usuario[0]['SGD_CIU_APELL1'] . " " .
                $usuario[0]['SGD_CIU_APELL2'];

            $vemail  = $usuario[0]['SGD_CIU_EMAIL'];

            $direccion = $usuario[0]['SGD_CIU_DIRECCION'] . "";
            if ($usuario[0]['SGD_CIU_TELEFONO'] != "") {
                $direccion .= ", Tel. " . $usuario[0]['SGD_CIU_TELEFONO'];
            }

            if ($usuario[0]['SGD_CIU_TELCELULAR'] != "") {
                $direccion .= ", Tel. " . $usuario[0]['SGD_CIU_TELCELULAR'];
            }

            $buscarPaisDepEtc = $this->obtenerCiudad('sgd_ciu_ciudadano', $usuario[0]['ID_CONT'], $usuario[0]['ID_PAIS'],
                $usuario[0]['DPTO_CODI'], $usuario[0]['MUNI_CODI'], $db);


            if (count($buscarPaisDepEtc) > 0) {

                $departamento = $buscarPaisDepEtc[0]['MUNI_NOMB'];
                if ($buscarPaisDepEtc[0]['DPTO_NOMB'] != $buscarPaisDepEtc[0]['MUNI_NOMB']) {
                    $departamento = $buscarPaisDepEtc[0]['MUNI_NOMB'] . ', ' . $buscarPaisDepEtc[0]['DPTO_NOMB'];
                }
                $departamento = $departamento . ', ' . $buscarPaisDepEtc[0]['NOMBRE_PAIS'];
            }
        }


        return array('departamento' => $departamento, 'direccion' => $direccion,
            'nombre_persona' => $nombre_persona, 'cargo' => $cargo,
            'nomdestinatario' => $nomdestinatario, 'dependencia_name' => $dependencia_name,
            'dependencia_id' => $dependencia_id,'email' => $vemail);
    }


    function validarImprimeFirmantes($quearmo, $noRad, $imprimir = '')
    {

        if ($imprimir === true) {
            return true;
        }

        if ($imprimir === false) {
            return false;
        }

        $imprimefirmantes = $_SESSION["FUNCTION_VALIDAR_RADICADO"] == 'NO' ? true : false;


        if ($quearmo == 'radicado' && $_SESSION['FUNCTION_VALIDAR_RADICADO'] == 'SI') {
            $encontrotiporad = false;
            if ($_SESSION['TIPOS_RADICADO_VALIDAR'] != '' && json_decode($_SESSION['TIPOS_RADICADO_VALIDAR']) != NULL) {
                foreach (json_decode($_SESSION['TIPOS_RADICADO_VALIDAR']) as $tipo_val) {
                    if ($tipo_val == substr($noRad, strlen($noRad) - 1, strlen($noRad))) {
                        $encontrotiporad = true;
                        break;
                    }
                }
            }
            if ($encontrotiporad == false) {
                $imprimefirmantes = true;
            }
        }

        return $imprimefirmantes;
    }


    function getFirmantePrincipalIdbyBorr($borrador_id)
    {
        try {
            $query = "Select * from borrador_firmantes where id_borrador_firmante= (SELECT  id_borrador_firmante FROM   borrador_firmantes WHERE  id_borrador =" . $borrador_id . " AND FIRMANTE_PRINCIPAL = 1 limit 1)";

            return $this->db->conn->selectLimit($query, 10)->getArray();
            //return $query;
        } catch (\Exception $e) {
            return $e->getMessage();
        }

    }

    //actualiza el primer firmante a firmante Principal.
    function putFirstFirmantePrinci($borrador_id)
    {
        $query = "update borrador_firmantes set firmante_principal = 1 where id_borrador=$borrador_id and id_borrador_firmante=
				(
         SELECT min(id_borrador_firmante)
         FROM   borrador_firmantes
         WHERE  id_borrador=$borrador_id
         LIMIT  1)
RETURNING id_borrador_firmante;";
        $guadar = $this->db->conn->execute($query);
    }

    //actualiza el primer firmante a tipo_validador='Remitente'.
    function putRemitenteFewID($borrador_id)
    {
        $query = "update borrador_firmantes set  tipo_validador='Remitente' where id_borrador=$borrador_id and id_borrador_firmante=
				(
         SELECT min(id_borrador_firmante)
         FROM   borrador_firmantes
         WHERE  id_borrador=$borrador_id
         LIMIT  1)
RETURNING id_borrador_firmante;";
        $guadar = $this->db->conn->execute($query);
    }

    function putFirstFirmanteReal($borrador_id, $id_firstreal_firmante)
    {
        $query = "update borrador_firmantes set firmante_principal=1 where id_borrador=$borrador_id and id_borrador_firmante= $id_firstreal_firmante";
        $guadar = $this->db->conn->execute($query);
    }

    //VERIFICA SI TIENE FIRMANTE PRINCIPAL UN BORRADOR, Y SI NO TIENE, SE LE ASIGNA COMO PRINCIPAL, EL PRIMER FIRMANTE
    function tieneFirmPrincipal($buscarfirmantes, $borrador_id, $realFirst = 0)
    {
        if (count($buscarfirmantes) > 0) {
            $consiguio_principal = false;
            foreach ($buscarfirmantes as $row) {
                if ($row['FIRMANTE_PRINCIPAL'] == 1) {
                    $consiguio_principal = true;
                }
            }
            if ($consiguio_principal == false && $borrador_id != false && $realFirst != 0) {
                $this->putFirstFirmanteReal($borrador_id, $realFirst);
            } else {
                if ($consiguio_principal == false && $borrador_id != false) {
                    $this->putFirstFirmantePrinci($borrador_id);
                }
            }
        }
    }


    function tieneRemitente($buscarfirmantes, $borrador_id)
    {
        if (count($buscarfirmantes) > 0) {
            $consiguio_remitente = false;
            foreach ($buscarfirmantes as $row) {
                if ($row['TIPO_VALIDADOR'] == 'Remitente') {
                    $consiguio_remitente = true;
                }
            }

            if ($consiguio_remitente == false && $borrador_id != false) {
                $this->putRemitenteFewID($borrador_id);
            }

        }

    }

    function terminarDocxRadicado($quearmo, $section, $noRad, $ruta, $nameDocumRadi, $phpWord, $fecharadicado = false)
    {

        if ($fecharadicado == false) {
            $fecharadicado = date('Y-m-d H:i');
        }

        $ruta_raiz = "../../";
        $fuente = $_SESSION['EDITOR_FUENTE_TIPO'] != 'SELECCIONE' ? "'" . $_SESSION['EDITOR_FUENTE_TIPO'] . "'" : "'Arial'";

        $html = '<table width="100%"><tbody>
<tr>
<td style="width: 33%" ></td>
<td style="width: 33%"><div style="text-align:center;float: right;  margin-bottom: -5px;" >
<img  style="width:190px;height: 120px" src="' . $ruta_raiz . 'app/img/encabezado_imagen_logo_s.png"/>
</div></td>

<td style="width: 33%; text-align:right;float: right">';

        $txt_ref_cod_barra = $noRad;
        if ($quearmo == 'borrador') {
            $txt_ref_cod_barra = '###Borrador' . $noRad . '###';

        } else if ($quearmo == 'radicado') {
            $html .= '<div style="text-align:right;float: right;  margin-bottom: -5px;" >
<span style="font-size:25px;width:300px;height: 25px">' . $noRad . '</span>
</div>';
        }

        $html .= '
<div style="text-align:right;float: right;  margin-right: -5px; margin-left: 5px" >
<span style="font-size: 8pt; font-family: ' . $fuente . '">Radicado: <strong style="font-size: 10pt;">' . $txt_ref_cod_barra . '
</strong></span><br></br>
<span style="font-size: 8pt; font-family: ' . $fuente . '">Fecha</span> ' . date('d-m-Y H:i', strtotime($fecharadicado)) . ' </div>
 <div style="text-align:right;float: right;  margin-right: -10px; font-size: 10" >P&aacute;g. {PAGENO} de {nb}</div>
 </td>
 </tr>
</tbody>
</table>';

        //\PhpOffice\PhpWord\Shared\Html::addHtml($header, $html);

        $styleTable = array('borderSize' => 0, 'borderColor' => '006699', 'cellMargin' => 80);
        $styleFirstRow = array('borderBottomSize' => 18, 'borderBottomColor' => '0000FF', 'bgColor' => '66BBFF');

        $styleCell = array('valign' => 'center', 'vMerge' => 'continue');
        $styleCellBTLR = array('valign' => 'center');
        $cellRowContinue = array('vMerge' => 'continue');
        $cellRowSpan = array('vMerge' => 'restart');
        $cellColSpan = array('gridSpan' => 2);
        // Define font style for first row

// Add table style


        //para agregar tablas al header
        //https://stackoverflow.com/questions/38124184/how-to-make-the-table-in-word-using-phpword-which-includes-multiple-rowspan-and
        $header = $section->createHeader();
        $table = $header->addTable();

        $fuente = $_SESSION['EDITOR_FUENTE_TIPO'] != 'SELECCIONE' ? $_SESSION['EDITOR_FUENTE_TIPO'] : 'Arial';
        $table->addRow();
        $table->addCell(4000, $cellRowSpan)->addText('');
        $table->addCell(2900, $cellRowSpan)->addImage($ruta_raiz . "app/img/encabezado_imagen_logo_s.png",
            array('width' => 180, 'height' => 100, 'align' => 'center'));
        $table->addCell(3000, $cellRowSpan)->addText('Radicado:', array('size' => '10', 'name' => $fuente), ['align' => 'right']);

        $table->addRow();
        $table->addCell(4000, $cellRowSpan)->addText('');
        $table->addCell(null, $cellRowContinue)->addText('');
        if ($quearmo == 'borrador') {
            $table->addCell(3000, $cellRowSpan)->addText($txt_ref_cod_barra, array('bold' => true, 'size' => '10', 'name' => $fuente), ['align' => 'right']);
        } else {
            $table->addCell(3000, $cellRowSpan)->addImage($ruta_raiz . "bodega/imagenbarra.png",
                array('width' => 150, 'height' => 25, 'align' => 'right'));
        }

        $table->addRow();
        $table->addCell(4000, $cellRowSpan);
        $table->addCell(null, $cellRowContinue)->addText('');
        $table->addCell(3000, $cellRowSpan)->addText('Fecha: ' . date('d-m-Y H:i', strtotime($fecharadicado)), array('size' => '10', 'name' => $fuente), ['align' => 'right']);


        $table->addRow();
        $table->addCell(4000, $cellRowSpan);
        $table->addCell(null, $cellRowContinue)->addText('');
        $table->addCell(3000, $cellRowSpan)->addPreserveText('Pág. {PAGE} de {NUMPAGES}.', array('color' => '150603', 'size' => 8), array('align' => 'right'));
// Add table
        $table = $section->addTable('myOwnTableStyle');


        /* $header->addImage($ruta_raiz . 'app/img/encabezado_imagen_logo_s.png', array(
             'width' => 450,
             'height' => 100,
             'align' => 'left'
         ));

         $header->addImage(
             $ruta_raiz . "bodega/imagenbarra.png",
             array(
                 'width' => 180,
                 'height' => 25,
                 'marginRight' => 1,
                 'align' => 'right'
             )
         );
         $section_style = $section->getStyle();
         $position =
             $section_style->getPageSizeW()
             - $section_style->getMarginRight()
             - $section_style->getMarginLeft();
         $phpWord->addParagraphStyle("leftRight", array("tabs" => array(
             new \PhpOffice\PhpWord\Style\Tab("right", $position)
         )));

         $header->addPreserveText("Radicado: " . $noRad . " del " . date('d-m-Y H:i'),
             array('color' => '150603'), array('align' => 'right', 'size' => 8));*/


        $footer = $section->createFooter();
        $footer->addImage($ruta_raiz . 'app/img/pie_de_pagina_imagen.png', array(
            'width' => 450,
            'height' => 76,
            'align' => 'left'
        ));
        //$footer->addPreserveText('Page {PAGE} of {NUMPAGES}.');
        //$header->addPreserveText('Pág. {PAGE} de {NUMPAGES}.', array('color' => '150603', 'size' => 8), array('align' => 'right'));
// Adding an empty Section to the document...
        //$section = $phpWord->addSection();

// Adding Text element to the Section having font styled by default...

// Saving the document as OOXML file...
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        //$temp_file = tempnam(sys_get_temp_dir(), 'helloWorld');

        $rutaDocRadi = $ruta . $nameDocumRadi . ".docx";


        $objWriter->save($rutaDocRadi);

        return $rutaDocRadi;

    }

    function updateDatosAnexosFolios($id_borrador, $folios_comunicacion, $folios_anexos, $descripcion_anexos)
    {
        try {
            $query = "update borradores set folios_comunicacion=" . $folios_comunicacion . ", folios_anexos=" . $folios_anexos . ",descripcion_anexos='" . $descripcion_anexos . "' where ID_BORRADOR =" . $id_borrador;
            $response = $this->db->conn->execute($query);
            return $response;

        } catch (\Exception $e) {
            return $e->getMessage();
        }

    }


//arma los reviso aprovo y proyecto, en ambos documentos, pdf y docx
    function armarAprobRevisProy($libreria, $historico, $helperBorra, $name_library = 'mpdf',
                                 $document_authorized_by = array(), $configHelper)
    {
        $proyectado = false;
        $aprobado = false;
        $revisado = false;

        $html = '';
        $entro=false;

        $fuente = $_SESSION['EDITOR_FUENTE_TIPO'] != 'SELECCIONE' ? "'" . $_SESSION['EDITOR_FUENTE_TIPO'] . "'" : "'Arial'";

        /**
         * La variable $valuesTable, es la que va a contener los valores para el caso que sea un docx (phpdocx)
         * Por lo tanto no creo la tabla como html, sino como $libreria->addTable.
         * Por lo tanto, la variable $textToDoc, va a almacenar los dos valores de las dos columnas que voy a mostrar
         */
        $valuesTable = array();
        foreach ($historico as $histor) {


            if (
                $configHelper->find_array_inside_array(
                    $document_authorized_by, array('ID' => $histor['ID_USUARIO_ORIGEN'])
                ) == false
            ) {

                if($entro==false){
                    if ($name_library == 'mpdf') {
                        $html = '<br>';
                        $html.='<table><tbody>';
                    }else{
                        $html.='<table   width="475px" border="1">';
                    }

                    $entro=true;
                }

                $html.='<tr>';

                $cargo = '';
                if ($histor['USUA_CARGO'] != null && $histor['USUA_CARGO'] != '') {
                    $cargo = ' - ' . $histor['USUA_CARGO'];
                }
                if ($histor['ID_ESTADO'] == 2 && $aprobado == false) {
                    $aprobo = $helperBorra->getUsuarioById($histor['ID_USUARIO_ORIGEN']);
                    $dependencia = $helperBorra->getDepenByDepeCodi($histor['DEPE_CODI_ORIGEN']);

                    $html .= $helperBorra->TildesHtml('<td><span style="font-size: 8pt; font-family: ' . $fuente . '">Aprobó: </span></td>
<td><span style="font-size: 8pt; font-family: ' . $fuente . '">' . $aprobo[0]['USUA_NOMB'] . $cargo . " - " . $dependencia[0]['DEPE_NOMB'] . "</span></td>");
                    $aprobado = true;


                    $textToDoc= array(
                        'Aprobó:', $aprobo[0]['USUA_NOMB'] . $cargo . " - " . $dependencia[0]['DEPE_NOMB']
                    );
                    $valuesTable[]=$textToDoc;
                }

                if ($histor['ID_ESTADO'] == 3) {

                    if (
                        ($configHelper->find_array_inside_array($historico, array(
                            'ID_ESTADO' => 2,
                            'ID_USUARIO_ORIGEN' => $histor['ID_USUARIO_ORIGEN']))) == false
                    ) {
                        $reviso = $helperBorra->getUsuarioById($histor['ID_USUARIO_ORIGEN']);
                        $dependencia = $helperBorra->getDepenByDepeCodi($histor['DEPE_CODI_ORIGEN']);


                        if ($revisado == false) {
                            $html .= $helperBorra->TildesHtml('<td><span style="font-size: 8pt; font-family: ' . $fuente . '">Revis&oacute;: </span></td>
<td><span style="font-size: 8pt; font-family: ' . $fuente . '">' . $reviso[0]['USUA_NOMB'] . $cargo . " - " . $dependencia[0]['DEPE_NOMB'] . "</span></td>");

                            $textToDoc= array(
                                'Revisó:',$reviso[0]['USUA_NOMB'] . $cargo . " - " . $dependencia[0]['DEPE_NOMB']
                            );

                        } else {
                            $textToDoc= array(
                                '',$reviso[0]['USUA_NOMB'] . $cargo . " - " . $dependencia[0]['DEPE_NOMB']
                            );

                            $html .= $helperBorra->TildesHtml('<td></td><td><span style="font-size: 8pt; font-family: ' . $fuente . '">' . $reviso[0]['USUA_NOMB'] . $cargo . " - " . $dependencia[0]['DEPE_NOMB'] . "</span></td>");
                        }
                        $revisado = true;

                        $valuesTable[]=$textToDoc;
                    }
                }

                if ($histor['ID_ESTADO'] == 1 && $proyectado == false) {

                    if (
                        ($configHelper->find_array_inside_array($historico, array(
                            'ID_ESTADO' => 2,
                            'ID_USUARIO_ORIGEN' => $histor['ID_USUARIO_ORIGEN']))) == false
                        &&
                        ($configHelper->find_array_inside_array($historico, array(
                            'ID_ESTADO' => 3,
                            'ID_USUARIO_ORIGEN' => $histor['ID_USUARIO_ORIGEN']))) == false
                    ) {
                        $proyecto = $helperBorra->getUsuarioById($histor['ID_USUARIO_ORIGEN']);
                        $dependencia = $helperBorra->getDepenByDepeCodi($histor['DEPE_CODI_ORIGEN']);
                        $html .= $helperBorra->TildesHtml('<td><span style="font-size: 8pt; font-family: ' . $fuente . '">Proyect&oacute;: </span></td>
<td><span style="font-size: 8pt; font-family: ' . $fuente . '">' . $proyecto[0]['USUA_NOMB'] . $cargo . " - " . $dependencia[0]['DEPE_NOMB'] . "</span></td>");
                        $proyectado = true;

                        $textToDoc= array(
                            'Proyectó:',$proyecto[0]['USUA_NOMB'] . $cargo . " - " . $dependencia[0]['DEPE_NOMB']
                        );
                        $valuesTable[]=$textToDoc;
                    }
                }
                $html.='</tr>';

            }

        }

        if($entro==true){
            $html.='</tbody></table>';
        }


        if ($html != '') {
            if ($name_library == 'mpdf') {
                $libreria->WriteHTML($html);
            } else if ($name_library == 'phpdocx') {
               // $libreria->embedHTML($html);

                $fuente = $_SESSION['EDITOR_FUENTE_TIPO'] != 'SELECCIONE' ?  $_SESSION['EDITOR_FUENTE_TIPO']  : "Arial";

                $tableProperties = array(
                    //'border' => 'single',
                    'borderWidth' => 0,
                    'tableAlign' => 'left',
                    'tableLayout'=>'fixed',
                    'textProperties' => array('font' => $fuente, 'fontSize' => 8,'textAlign','left'),
                    'columnWidths' => array(10, 90),
                    'tableWidth' => array('type' => 'pct', 'value' => 100),
                );

                $libreria->addTable($valuesTable, $tableProperties);
                $libreria->embedHTML('<br>');
            } else {
                //phpword
                $libreria->addTextBreak();
                $doc = new DOMDocument();
                $html = $doc->loadHTML($html);
                $html = $doc->getElementsByTagName('body')->item(0)->C14N();
                \PhpOffice\PhpWord\Shared\Html::addHtml($libreria, $html);
                $libreria->addTextBreak();
            }
        }
        return $libreria;

    }

    public function getBorradorEstadoByCodigo($vall)
    {
        try {
            $query = "SELECT  *
                FROM borrador_estados  
                where sgd_ttr_codigo=" . $vall . " ";

            return $this->db->conn->selectLimit($query, 10)->getArray();

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param $vall
     * @return string
     * retorna todos los estados posibles del borraodr
     */
    public function getBorradorEstados()
    {
        try {
            $query = "SELECT  *
                FROM borrador_estados ";
            return $this->db->conn->selectLimit($query, 100)->getArray();

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }


    public function getBorradorEstadoByNombre($vall)
    {
        try {
            $query = "SELECT  *
                FROM borrador_estados  
                where nombre_estado='" . $vall . "' ";

            return $this->db->conn->selectLimit($query, 1)->getArray();

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    function formatSizeUnits($bytes)
    {
        /*if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }*/
        if ($bytes >= 1024) {
            $bytes = number_format($bytes / 1024, 2);
        } elseif ($bytes > 1) {
            $bytes = $bytes;
        } elseif ($bytes == 1) {
            $bytes = $bytes;
        } else {
            $bytes = '0';
        }

        return $bytes;
    }

    //trae un mes en espaol
    function mesespanol($mes)
    {
        if ($mes == "January") {
            $mes = "Enero";
        }
        if ($mes == "February") {
            $mes = "Febrero";
        }
        if ($mes == "March") {
            $mes = "Marzo";
        }
        if ($mes == "April") {
            $mes = "Abril";
        }
        if ($mes == "May") {
            $mes = "Mayo";
        }
        if ($mes == "June") {
            $mes = "Junio";
        }
        if ($mes == "July") {
            $mes = "Julio";
        }
        if ($mes == "August") {
            $mes = "Agosto";
        }
        if ($mes == "September") {
            $mes = "Septiembre";
        }
        if ($mes == "October") {
            $mes = "Octubre";
        }
        if ($mes == "November") {
            $mes = "Noviembre";
        }
        if ($mes == "December") {
            $mes = "Diciembre";
        }

        return $mes;
    }

    public static function TildesHtml($cadena)
    {
        return str_replace(array("á", "é", "í", "ó", "ú", "ñ", "Á", "É", "Í", "Ó", "Ú", "Ñ"),
            array("&aacute;", "&eacute;", "&iacute;", "&oacute;", "&uacute;", "&ntilde;",
                "&Aacute;", "&Eacute;", "&Iacute;", "&Oacute;", "&Uacute;", "&Ntilde;"), $cadena);
    }

    function getTipoBorradorDest($constante)
    {

        if ($constante == 'SALIDA_OFICIO'):
            return 'Salida | Formato Oficio | Empresas - Ciudadanos';

        elseif ($constante == 'INTERNA_COMUNICACION'):
            return 'Interna | Formato Comunicación';

        elseif ($constante == 'INTERNA_SIN_PLANTILLA'):
            return 'Interna | Sin planilla';

        else:
            return 'Interna | Formato con plantilla';
        endif;

    }


}