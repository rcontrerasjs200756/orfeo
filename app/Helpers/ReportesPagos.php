<?php

namespace App\Helpers;

//error_reporting(E_ALL);

use Carbon\Carbon;

class ReportesPagos
{
    protected $query_factory;
    protected $pdo;
    protected $user;

    //al manejar varios helper, arroja error al redeclarar lasconexion a la bd, por eso si existe la varible db, es porque le estoy pasando
    //los datos de conexion especifica y no vuelva a redeclarar
    public function __construct($db = array())
    {

        session_start();
        include_once '../../vendor/autoload.php';

        $this->user = $_SESSION;
        include_once "../../config.php";

        if (count($db) < 1) {
            include_once "../../include/db/ConnectionHandlerNew.php";

            $this->db = new \ConnectionHandler("../..");
            $this->db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        } else {
            $this->db = $db;
        }

    }

    public function get_returned_payments($from_date = null, $to_date = null)
    {
        try {
            $from = (!is_null($from_date)) ? Carbon::parse($from_date)->format('Y-m-d H:i:s') : Carbon::now()->startOfMonth()->format('Y-m-d H:i:s');
            $to = (!is_null($to_date)) ? Carbon::parse($to_date)->format('Y-m-d H:i:s') : Carbon::now()->format('Y-m-d H:i:s');

            $query = "SELECT r.radi_nume_radi AS RADICADO,
                r.radi_fech_radi AS FECHA_RADICADO,
                r.ra_asun AS ASUNTO,
                r.radi_path AS RADI_PATH,
                h.sgd_ttr_codigo AS TRANSACCION,
                h.hist_fech AS FECHA_TRANSACCION,
                h.hist_obse AS HISTORICO_COMENTARIO,
                h.depe_codi || '.' || d1.depe_nomb AS Desde,
                h.depe_codi_dest || '.' || d2.depe_nomb AS Para,
                u.usua_nomb AS USUARIO_ACTUAL,
                r.radi_depe_actu,
                d.depe_nomb
                FROM radicado r
                INNER JOIN hist_eventos h ON r.radi_nume_radi=h.radi_nume_radi
                INNER JOIN dependencia d ON r.radi_depe_actu=d.depe_codi
                INNER JOIN dependencia d1 ON h.depe_codi=d1.depe_codi
                INNER JOIN dependencia d2 ON h.depe_codi_dest=d2.depe_codi
                INNER JOIN usuario u ON r.radi_usua_actu=u.usua_codi And u.depe_codi = d.depe_codi
                WHERE h.sgd_ttr_codigo IN (9,12)
                AND CAST(r.radi_nume_radi as VARCHAR(15)) LIKE '%4'
                AND h.depe_codi IN (420,421,422)
                AND h.depe_codi_dest NOT IN (420,421,422)
                AND r.radi_fech_radi BETWEEN TO_TIMESTAMP('" . $from . "', 'YYYY-MM-DD HH24:MI:SS')
                AND TO_TIMESTAMP('" . $to . "', 'YYYY-MM-DD HH24:MI:SS')
                ORDER BY r.radi_fech_radi ASC";

            return $this->db->conn->selectLimit($query, 10000)->getArray();

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function get_month_payments($from_date = null, $to_date = null)
    {
        try {
            $from = (!is_null($from_date)) ? Carbon::parse($from_date)->format('Y-m-d H:i:s') : Carbon::now()->startOfMonth()->format('Y-m-d H:i:s');
            $to = (!is_null($to_date)) ? Carbon::parse($to_date)->format('Y-m-d H:i:s') : Carbon::now()->format('Y-m-d H:i:s');


            $query = "SELECT r.radi_nume_radi AS RADICADO,
                r.radi_fech_radi AS FECHA_RADICADO,
                r.ra_asun AS ASUNTO,
                r.radi_path AS RADI_PATH,
                u.usua_nomb AS USUARIO_ACTUAL,
                r.radi_depe_actu,
                d.depe_nomb
                FROM radicado r
                INNER JOIN dependencia d ON r.radi_depe_actu = d.depe_codi
                INNER JOIN usuario u ON r.radi_usua_actu=u.usua_codi And u.depe_codi = d.depe_codi
                WHERE CAST(r.radi_nume_radi AS VARCHAR(15)) LIKE '%4'
                AND r.radi_fech_radi BETWEEN TO_TIMESTAMP('" . $from . "', 'YYYY-MM-DD HH24:MI:SS')
                AND TO_TIMESTAMP('" . $to . "', 'YYYY-MM-DD HH24:MI:SS')
                ORDER BY r.radi_fech_radi ASC";

            return $this->db->conn->selectLimit($query, 10000)->getArray();

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public static function get_transaction_label($code)
    {
        if ($code == 9) {
            return "Reasignado";
        } elseif ($code == 12) {
            return "Devuelto";
        } else {
            return $code;
        }
    }

    public static function get_subject($dep_code, $depe_nomb)
    {
        if ($dep_code == 999) {
            return "Finalizado";
        } else {
            return $dep_code . '.' . $depe_nomb;
        }
    }

    public static function get_scanned($radicated)
    {
        include_once "../../config.php";
        include_once "../../include/db/ConnectionHandlerNew.php";
        $db = new \ConnectionHandler("../..");
        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);

        $query = "SELECT COUNT(*) AS ESCANEADO
                    FROM radicado r
                    INNER JOIN hist_eventos h ON r.radi_nume_radi=h.radi_nume_radi
                    WHERE h.sgd_ttr_codigo IN (22, 23, 42)
                    AND (r.radi_path LIKE '%tif' OR r.radi_path LIKE '%pdf' )
                    AND r.radi_nume_radi=" . $radicated . "";

        $response = $db->conn->execute($query);
        $result = $response->fetchRow();

        return $result["ESCANEADO"];
    }

    public function get_rad_info($radicated)
    {
        $query = "SELECT r.radi_nume_radi AS RADICADO,
                     r.radi_fech_radi AS FECHA_RADICADO,
                     r.ra_asun AS ASUNTO,
                     u.usua_nomb AS USUARIO_ACTUAL,
                     r.radi_depe_actu,
                     d.depe_nomb
                     FROM radicado r
                     INNER JOIN dependencia d ON r.radi_depe_actu = d.depe_codi
                     INNER JOIN usuario u ON r.radi_usua_actu=u.usua_codi And u.depe_codi = d.depe_codi
                     AND r.radi_nume_radi=" . $radicated . "";

        $response = $this->db->conn->execute($query);
        $result = $response->fetchRow();

        return $result;
    }

    public function tablaprincipalOP($desde, $hasta, $swicheselected, $start = false, $limit = false, $buscar = false)
    {


        $query = "SELECT r.radi_nume_radi AS RADICADO,
                    r.radi_fech_radi AS FECHA_RADICADO,
                    r.ra_asun AS ASUNTO,
                    r.radi_path AS RADI_PATH,
                    dir.sgd_dir_nomremdes AS CONTRATISTA,
                    dir.sgd_dir_doc AS CONTRATISTA_IDENTIF,
                    u.usua_nomb AS USUARIO_ACTUAL,
                    r.radi_depe_actu AS DEPENDENCIA_ACTUAL_NUMERO,
                    d.depe_nomb AS DEPENDENCIA_ACTUAL,
                     Count( Case When a.anex_origen=4 And a.anex_borrado='N' Then a.anex_origen End ) AS OP
                    FROM radicado r
                    INNER JOIN sgd_dir_drecciones dir On r.radi_nume_radi=dir.radi_nume_radi
                    INNER JOIN usuario u ON r.radi_usua_actu=u.usua_codi And r.radi_depe_actu=u.depe_codi
                    INNER JOIN dependencia d ON u.depe_codi = d.depe_codi 
                    LEFT JOIN anexos a On r.radi_nume_radi=a.anex_radi_nume";

        if ($swicheselected == "Supervisor") {
            $query .= " INNER JOIN sgd_supervision_contratistas c ON c.contratista_doc=dir.sgd_dir_doc ";
        }
        $query .= " Where r.radi_fech_radi BETWEEN TO_TIMESTAMP('$desde', 'YYYY-MM-DD HH24:MI:SS') AND TO_TIMESTAMP('$hasta', 'YYYY-MM-DD HH24:MI:SS')
                    AND r.sgd_trad_codigo=4 
                    And ( lower(CAST(r.radi_nume_radi  as text)) like('%" . $buscar . "%') 
                    Or lower(r.ra_asun)  like lower('%" . $buscar . "%') 
                    Or lower(dir.sgd_dir_nomremdes)  like lower('%" . $buscar . "%') 
                    Or lower(u.usua_nomb)  like lower('%" . $buscar . "%')
                    Or lower(d.depe_nomb)  like lower('%" . $buscar . "%') 
                    Or lower(dir.sgd_dir_doc)  like lower('%" . $buscar . "%')) ";


        if ($swicheselected == "Contratista") {
            $query .= " AND dir.sgd_dir_doc='" . $_SESSION['usua_doc'] . "'";
        }
        if ($swicheselected == "Financiera") {
            $query .= " AND r.radi_depe_actu='" . $_SESSION['dependencia'] . "' AND r.radi_usua_actu=" . $_SESSION['codusuario'] . " ";
        }
        if ($swicheselected == "Supervisor") {
            $query .= " AND (c.supervisor_doc = '" . $_SESSION['usua_doc'] . "' OR c.supervisor_apoyo_doc='" . $_SESSION['usua_doc'] . "') ";

        }
        $query .= " GROUP BY r.radi_nume_radi, r.radi_fech_radi, r.ra_asun, r.radi_path, dir.sgd_dir_doc,
         dir.sgd_dir_nomremdes,u.usua_nomb, r.radi_depe_actu, d.depe_nomb
 ORDER BY ";

        if ($swicheselected == "Financiera") {
            $query .= " OP ASC, ";
        } else {
            $query .= " OP DESC, ";
        }

        $query .= " r.radi_fech_radi asc";


        if ($limit != false) {
            return $this->db->conn->SelectLimit($query, $limit, $start)->getArray();
        } else {
            return $this->db->conn->SelectLimit($query)->getArray();
        }

    }

    public function tblprincipalAdmin($start = false, $limit = false, $buscar = false)
    {
        $query = "Select 'Persona' as tipo, ciu.sgd_ciu_nombre ||' '|| ciu.sgd_ciu_apell1 ||' '|| ciu.sgd_ciu_apell2, 
                    ciu.sgd_ciu_cedula As identificacion, ciu.sgd_ciu_email, ciu.sgd_ciu_telefono, u.depe_codi, d.depe_nomb
                    From sgd_ciu_ciudadano ciu
                    left Join usuario u On (ciu.sgd_ciu_cedula=u.usua_doc)
                    Left Join dependencia d On (u.depe_codi=d.depe_codi)
                    Where ciu.ciu_contratista = 1  And ( lower(ciu.sgd_ciu_nombre) like lower('%" . $buscar . "%') Or lower(ciu.sgd_ciu_apell1) 
                    like lower('%" . $buscar . "%') Or lower(ciu.sgd_ciu_apell2)  like lower('%" . $buscar . "%') 
                    Or lower(ciu.sgd_ciu_cedula)  like lower('%" . $buscar . "%') Or lower(d.depe_nomb)  like lower('%" . $buscar . "%') 
                    Or lower(d.depe_nomb)  like lower('%" . $buscar . "%') )
                    UNION ALL
                    Select 'Empresa' as tipo, emp.SGD_OEM_OEMPRESA, emp.sgd_oem_nit As identificacion, emp.sgd_oem_email, emp.sgd_oem_telefono, null, ''
                    From sgd_oem_oempresas emp
                    Where emp.emp_contratista=1 And ( lower(emp.SGD_OEM_OEMPRESA) like lower('%" . $buscar . "%') 
                    Or lower(emp.sgd_oem_nit) like lower('%" . $buscar . "%') )";


        if ($limit != false) {
            return $this->db->conn->SelectLimit($query, $limit, $start)->getArray();
        } else {
            return $this->db->conn->SelectLimit($query)->getArray();
        }
    }

    public function getCiuCiudadanoByCedula($cedula)
    {
        $query = "Select 'Persona' as tipo, ciu.sgd_ciu_nombre ||' '|| ciu.sgd_ciu_apell1 ||' '|| ciu.sgd_ciu_apell2, 
                    ciu.sgd_ciu_cedula As identificacion, ciu.sgd_ciu_email, ciu.sgd_ciu_telefono, u.depe_codi, d.depe_nomb
                    From sgd_ciu_ciudadano ciu
                    left Join usuario u On (ciu.sgd_ciu_cedula=u.usua_doc)
                    Left Join dependencia d On (u.depe_codi=d.depe_codi)
                    Where ciu.ciu_contratista = 1 and sgd_ciu_cedula='" . $cedula . "'";

        return $this->db->conn->SelectLimit($query, 1)->getArray();

    }


    public function getOemOempresasByNit($nit)
    {
        $query = "Select 'Empresa' as tipo, emp.SGD_OEM_OEMPRESA, emp.sgd_oem_nit As identificacion, emp.sgd_oem_email, emp.sgd_oem_telefono, null, ''
                    From sgd_oem_oempresas emp
                    Where emp.emp_contratista=1 and sgd_oem_nit='" . $nit . "'";

        return $this->db->conn->SelectLimit($query, 1)->getArray();

    }

    public function getContraBySuperv($usua_doc, $identifContra)
    {
        //busca si el usuairoa ctual es supervisor de apoyo o principal
        $query = "Select *
                    From sgd_supervision_contratistas 
                    Where (supervisor_doc = $usua_doc or  supervisor_apoyo_doc=$usua_doc) and  contratista_doc='" . $identifContra . "' ";

        return $this->db->conn->SelectLimit($query, 1)->getArray();
    }

    public function getanex_numero($numeradi)
    {
        $query = "Select (Case When count(anex_numero)=0 Then 1
                  Else max(anex_numero)+1 End) as nume
                  From anexos Where
                anex_radi_nume=$numeradi";

        return $this->db->conn->SelectLimit($query, 10000)->getArray();

    }

    public function getExtencion($ext)
    {
        $query = "SELECT  *
                FROM anexos_tipo where anex_tipo_ext='" . $ext . "'";

        return $this->db->conn->SelectLimit($query, 10000)->getArray();

    }

    public function getHisEvent($radicado)
    {

        //retorna 3 historiales del radicado
        $query = "SELECT TO_CHAR(h.hist_fech,'DD-MM-YYYY HH24:MI:SS') AS hist_fech, u.usua_nomb, h.hist_obse, t.sgd_ttr_descrip
                    FROM hist_eventos h
                    Inner Join usuario u On h.usua_doc=u.usua_doc
                    Inner Join sgd_ttr_transaccion t On h.sgd_ttr_codigo=t.sgd_ttr_codigo
                    Where h.radi_nume_radi=$radicado
                    And h.sgd_ttr_codigo In (2,8,9,12,13,14,16,22,23,25,29,42,53,44, 47)
                    Order By h.hist_fech Desc";


        return $this->db->conn->SelectLimit($query, 3)->getArray();

    }

    public function verfifiEscan($radicado)
    {

        //verifica si el radicado ha sido ecaneado
        $query = "Select count(1) as contador
                    From radicado r Inner Join hist_eventos h On r.radi_nume_radi=h.radi_nume_radi
                    Where
                    h.sgd_ttr_codigo In (22,23,42)
                    And ( lower(r.radi_path) like lower('%.pdf') Or lower(r.radi_path) like lower('%.tif') )
                    And r.radi_nume_radi=$radicado";

        return $this->db->conn->SelectLimit($query, 1)->getArray();

    }

    public function verifiAddExped($radicado)
    {

        //verifica si el radicado ha sido agregado a un expediente
        $query = "Select count(1) as contador
                    From sgd_exp_expediente
                    Where sgd_exp_estado != 2
                    And radi_nume_radi=$radicado";

        return $this->db->conn->SelectLimit($query, 1)->getArray();

    }

    public function usuaLikeSupervi($coincide, $perfil)
    {
        //retorna los datos para el select del modal de asignar supervisores  principal
        $query = "Select u.id,u.usua_nomb, u.usua_doc, d.depe_codi ||'-'|| d.depe_nomb as texto, u.usua_email, d.depe_nomb
                    From usuario u Inner Join dependencia d On (u.depe_codi=d.depe_codi) Where u.usua_esta='1' 
                    AND (lower(u.usua_nomb) like lower('%" . $coincide . "%') Or lower(d.depe_nomb) like lower('%" . $coincide . "%')
                    Or lower(u.usua_email) like lower('%" . $coincide . "%') Or lower(u.usua_doc) like lower('%" . $coincide . "%'))
                     ";
        if ($perfil == "principal") {
            $query .= " and u.usua_doc='" . $_SESSION["usua_doc"] . "'";
        }

        if ($perfil == "apoyo") {
            $query .= " And (u.depe_codi=" . $_SESSION['dependencia'] . " Or d.depe_codi_padre=" . $_SESSION['dependencia'] . ")
            And (u.usua_supervisor=1 OR u.usua_codi=1)
And u.usua_doc !='" . $_SESSION['usua_doc'] . "'";
        }
        $query .= " Order By u.depe_codi, u.usua_codi, u.usua_nomb";


        return $this->db->conn->SelectLimit($query, 100)->getArray();

    }

    public function usuaLikeSuperviApoyo($coincide, $perfil)
    {
        //busca los datos para sl select de supervisores de apoyo

        $query = "Select u.id,u.usua_nomb, u.usua_doc, d.depe_codi ||'-'|| d.depe_nomb as texto, u.usua_email, d.depe_nomb
                    From usuario u Inner Join dependencia d On (u.depe_codi=d.depe_codi) Where u.usua_esta='1' 
                    AND (lower(u.usua_nomb) like lower('%" . $coincide . "%') Or lower(d.depe_nomb) like lower('%" . $coincide . "%')
                    Or lower(u.usua_email) like lower('%" . $coincide . "%') Or lower(u.usua_doc) like lower('%" . $coincide . "%'))
                     ";
        if ($perfil == "principal") {
            $query .= " And (u.depe_codi=" . $_SESSION['dependencia'] . " Or d.depe_codi_padre=" . $_SESSION['dependencia'] . ")
And u.usua_doc !='" . $_SESSION['usua_doc'] . "'";
        }

        if ($perfil == "apoyo") {
            $query .= " And (u.depe_codi=" . $_SESSION['dependencia'] . " Or d.depe_codi_padre=" . $_SESSION['dependencia'] . ")";
        }
        $query .= " Order By u.depe_codi, u.usua_codi, u.usua_nomb";
        return $this->db->conn->SelectLimit($query, 100)->getArray();

    }

    public function getAnexOp($numeradi)
    {
        //trae todos los anexos OP ordenados por fecha de creacion del anexos asc
        $query = "Select id,anex_ubic,anex_codigo, anex_fech_anex,anex_radi_fech,anex_depe_creador,anex_nomb_archivo,anex_desc,
                    anex_depe_creador
                    From anexos Where anex_origen=4  and anex_borrado='N' and
                    anex_radi_nume=$numeradi order by anex_fech_anex asc";

        return $this->db->conn->SelectLimit($query, 20000)->getArray();

    }

    public function getAnexOpById($idanexo)
    {
        //trae un anexo especifico por id
        $query = "Select *
                  From anexos 
                  Where  id=$idanexo ";

        return $this->db->conn->SelectLimit($query, 20000)->getArray();

    }

    public function guardarSupervisores($principalPost, $apoyoPost, $contratistas)
    {
        try {

            $usuario_modifica = $_SESSION["usuario_id"];
            $response = false;
            $fecha = $this->db->conn->OffsetDate(0, $this->db->conn->sysTimeStamp);

            foreach ($contratistas as $row) {

                $principal = $principalPost != false && $principalPost != "" ? $principalPost : "";
                $apoyo = $apoyoPost != false && $apoyoPost != "" ? $apoyoPost : "";

                if ($row['cedula'] == true) {

                    $tipo = 1;
                    if ($row['tipo'] == "Empresa") {
                        $tipo = 2;
                    }

                    $tienesuperv = $this->getSupervByContrat($row['cedula']);
                    $datosprincipal = $this->getUsuarioByDoc($principalPost);
                    $datosapoyo = $this->getUsuarioByDoc($apoyoPost);


                    if (!isset($tienesuperv[0])) {

                        $ultima_modificacion = "";
                        if ($principal != "" && $principal != null) {
                            $ultima_modificacion .= " Se creó supervisor principal " .
                                $datosprincipal[0]['USUA_NOMB'] . ".";
                        } else {
                            $principal = 'null';
                        }

                        if ($apoyo != "" && $apoyo != null) {
                            $ultima_modificacion .= " Se creó supervisor de apoyo" .
                                $datosapoyo[0]['USUA_NOMB'] . ".";
                        } else {
                            $apoyo = 'null';
                        }

                        $query = "insert into sgd_supervision_contratistas
                                  (contratista_doc,supervisor_doc,supervisor_apoyo_doc,contratista_tipo) VALUES 
                                  ('" . $row['cedula'] . "',$principal, $apoyo,$tipo) ";

                        $response = $this->db->conn->execute($query);

                        $query = "insert into hist_supervision_contratistas
                                  (usuario_modifica,fecha_modifica,identifi_contratista,contratista_tipo,supervisor_principal,supervisor_apoyo,
                                  ultima_modificacion) VALUES 
                                  ($usuario_modifica,$fecha,'" . $row['cedula'] . "',$tipo,$principal,$apoyo,'" . $ultima_modificacion . "') ";
                        $response = $this->db->conn->execute($query);

                    } else {
                        $tienesuperv = $this->getSupervByContrat($row['cedula']);
                        $principalviejo = $this->getUsuarioByDoc($tienesuperv[0]['SUPERVISOR_DOC']);
                        $apoyoviejo = $this->getUsuarioByDoc($tienesuperv[0]['SUPERVISOR_APOYO_DOC']);
                        $ultima_modificacion = "";
                        if ($principal != "" && $principal != null) {
                            $ultima_modificacion .= " Se cambió supervisor principal " . $principalviejo[0]['USUA_NOMB'] . " por " .
                                $datosprincipal[0]['USUA_NOMB'] . ".";
                        } else {
                            $principal = 'null';
                        }

                        if ($apoyo != "" && $apoyo != null) {
                            $ultima_modificacion .= " Se cambió supervisor de apoyo " . $apoyoviejo[0]['USUA_NOMB'] . " por " .
                                $datosapoyo[0]['USUA_NOMB'] . ".";
                        } else {
                            $apoyo = 'null';
                        }


                        $query = "update sgd_supervision_contratistas set";
                        if ($principal != "") {
                            $query .= " supervisor_doc=$principal, ";
                        }

                        if ($apoyo != "") {
                            $query .= " supervisor_apoyo_doc=$apoyo, ";
                        }
                        $query .= " contratista_tipo=$tipo 
                                  where contratista_doc='" . $row['cedula'] . "'   ";



                        $response = $this->db->conn->execute($query);

                        $query = "INSERT INTO hist_supervision_contratistas
                                  (usuario_modifica,fecha_modifica,identifi_contratista,contratista_tipo,supervisor_principal,supervisor_apoyo, ultima_modificacion) 
                                  VALUES ($usuario_modifica,$fecha,'" . $row['cedula'] . "',$tipo,$principal,$apoyo,'" . $ultima_modificacion . "') ";
                        $response = $this->db->conn->execute($query);
                    }
                }

            }

            return $response;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }


    function getSupervByContrat($cedula)
    {

        try {
            $query = "SELECT  *
                      FROM sgd_supervision_contratistas where contratista_doc='" . $cedula . "' ";

            $response = $this->db->conn->selectLimit($query, 10000)->getArray();
            return $response;

        } catch (\Exception $e) {
            return $e->getMessage();
        }

    }

    public function getHistSupervByContratista($identiContratista)
    {
        try {
            $query = "SELECT  *
                      FROM supervisor_contratistas where identifi_contratista='" . $identiContratista . "' ";

            $response = $this->db->conn->selectLimit($query, 10000)->getArray();
            return $response;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function getLastHistSuperContra($identiContratista)
    {
        //trae el ultimo historial que tiene
        try {
            $query = "SELECT  *
                      FROM supervisor_contratistas where identifi_contratista='" . $identiContratista . "' order by fecha_modifica desc";

            $response = $this->db->conn->selectLimit($query, 1)->getArray();
            return $response;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function getUsuarioByDoc($usua_doc)
    {
        //trae la info del usuario por dcumentacion
        try {
            $query = "SELECT  *
                     FROM usuario where usua_doc='" . $usua_doc . "' ";
            $response = $this->db->conn->selectLimit($query, 1)->getArray();
            return $response;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function soySupervisor($usua_doc)
    {
        //verifica si el usuario logueado es supervisor
        try {
            $query = "select count(1) from sgd_supervision_contratistas 
                        where supervisor_doc='" . $usua_doc . "' 
                        or supervisor_apoyo_doc='" . $usua_doc . "'";
            $response = $this->db->conn->selectLimit($query, 1)->getArray();
            return $response;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function soySupervisorPrincipal($usua_doc)
    {
        //verifica si el usuario es supervisor pero Principal
        try {
            $query = "select count(1) from sgd_supervision_contratistas 
                      where supervisor_doc='" . $usua_doc . "'";
            $response = $this->db->conn->selectLimit($query, 1)->getArray();
            return $response;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function soySupervisorApoyo($usua_doc)
    {
        //verifica si el usuario es supervisor pero de Apoyo
        try {
            $query = "select count(1) from sgd_supervision_contratistas 
                      where supervisor_apoyo_doc='" . $usua_doc . "'";
            $response = $this->db->conn->selectLimit($query, 1)->getArray();
            return $response;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function soyContratista($usua_doc)
    {
        //verifica si el usuario logueado es Contratista
        try {
            $query = "select count(1) from sgd_dir_drecciones 
                    where sgd_dir_doc='" . $usua_doc . "'
                    and cast(radi_nume_radi as text) like '%4' ";
            $response = $this->db->conn->selectLimit($query, 1)->getArray();
            return $response;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    //borra de la tabla anexo
    function deleteAnexo($radicado, $rutaarchivo)
    {

        $query = "update anexos set anex_borrado='S' where  anex_radi_nume='" . $radicado . "' and
                  anex_nomb_archivo='" . $rutaarchivo . "' ";
        //executing
        $response = $this->db->conn->execute($query);
        return $response;
    }

    //borra de la tabla anexo
    function updateprevios()
    {
        $query = "Update sgd_ciu_ciudadano ciu Set ciu_contratista=1 Where ciu.sgd_ciu_cedula In (
                    Select distinct On (dir.sgd_dir_doc) ciu.sgd_ciu_cedula
                    From sgd_ciu_ciudadano ciu Inner Join sgd_dir_drecciones dir On ciu.sgd_ciu_cedula=dir.sgd_dir_doc
                    Inner Join radicado r On (dir.radi_nume_radi=r.radi_nume_radi)
                    Where CAST(dir.radi_nume_radi as VARCHAR(14)) like '%4'
                    And r.radi_fech_radi > '2018/08/01'  --(para pruebas > '2014/04/01')
                    And ciu.ciu_contratista is NULL
                    And dir.sgd_dir_doc != '' 
                    And dir.sgd_dir_doc is not null
                    And dir.sgd_dir_doc != 'documento' )
                    ";
        $response = $this->db->conn->execute($query);

        $query = "Update sgd_oem_oempresas emp Set emp_contratista=1 Where emp.sgd_oem_nit In (
                    Select distinct On (dir.sgd_dir_doc) emp.sgd_oem_nit
                    From sgd_oem_oempresas emp Inner Join sgd_dir_drecciones dir On emp.sgd_oem_nit=dir.sgd_dir_doc --Pruebas: On trim(emp.sgd_oem_oempresa)=trim(dir.sgd_dir_nomremdes) -- 
                    Inner Join radicado r On (dir.radi_nume_radi=r.radi_nume_radi)
                    
                    Where CAST(dir.radi_nume_radi as VARCHAR(14)) like '%4'
                    And r.radi_fech_radi > '2018/08/01'  --(para pruebas > '2014/04/01')
                    And emp_contratista is NULL
                    And dir.sgd_dir_doc != ''
                    And dir.sgd_dir_doc is not null
                    And dir.sgd_dir_doc != 'documento' )
                    ";
        $response = $this->db->conn->execute($query);
        return $response;
    }
}
