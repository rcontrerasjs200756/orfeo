<?php

namespace App\Helpers;

error_reporting(E_ALL);

use Carbon\Carbon;

class PazYSalvo
{
    protected $query_factory;
    protected $pdo;
    protected $user;

    public function __construct(){
        session_start();
        require '../../vendor/autoload.php';
        $this->user= $_SESSION;

        include_once "../../config.php";
        include_once "../../include/db/ConnectionHandlerNew.php";
        $this->db = new \ConnectionHandler("../..");
        $this->db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
    }

    public function get_user($q)
    {
        try{
        $query= "SELECT u.usua_login AS login,
                u.usua_nomb AS nombre,
                d.depe_nomb AS depe
                FROM usuario u
                INNER JOIN dependencia d
                ON u.depe_codi=d.depe_codi
                WHERE LOWER(u.usua_login) ='".strtolower($q)."'
                OR LOWER(u.usua_nomb) LIKE '%".strtolower($q)."%'
                OR u.usua_doc LIKE '%".strtoupper($q)."%'
                OR EXISTS (SELECT * FROM dependencia d WHERE u.depe_codi=d.depe_codi AND LOWER(d.depe_nomb) LIKE '%".strtolower($q)."%') ORDER BY u.usua_nomb ASC
                ";

            return $this->db->conn->getAll($query);

        }catch(\Exception $e){
            return $e->getMessage();
        }
    }

    public function get_user_data($user)
    {
        try{
            $user_arr   = explode('-',$user);
            $user_login = strtoupper(trim($user_arr[1]));
            $user_name  = strtoupper(trim($user_arr[0]));

            $query= "SELECT u.usua_nomb AS NOMBRE,
                        u.usua_login AS USUARIO,
                        (d.depe_codi || ', ' || d.depe_nomb) AS DEPENDENCIA,
                        u.usua_codi AS ROL,
                        CASE
                        WHEN u.usua_esta = '1' THEN 'Activo'
                        WHEN U.usua_esta <> '1' THEN 'Inactivo'
                        END AS ESTADO,
                        u.usua_email AS EMAIL,
                        u.usua_doc AS DOCUMENTO,
                        u.codi_nivel AS NIVEL,
                        u.usua_fech_sesion AS ULTIMO_INGRESO,
                        d.depe_nomb AS DEPENDENCIA_NOMBRE,
                        (SELECT Count(r.radi_nume_radi)
                            FROM usuario u2,
                            radicado r,
                            dependencia d2
                            WHERE  u2.depe_codi = d2.depe_codi
                            AND u2.usua_codi = u.usua_codi
                            AND u2.depe_codi = u.depe_codi
                            AND r.radi_usua_actu = u2.usua_codi
                            AND r.radi_depe_actu = u2.depe_codi) AS RADICADOS,
                        (SELECT COUNT(i.radi_nume_radi)
                        FROM usuario u2, informados i, dependencia d2
                        WHERE u2.depe_codi = d2.depe_codi
                        AND u2.usua_codi=u.usua_codi
                        AND u2.depe_codi=u.depe_codi
                        AND i.usua_codi=u2.usua_codi
                        AND i.depe_codi=998) AS INFORMADOS
                        FROM   usuario u, dependencia d
                        WHERE  u.depe_codi = d.depe_codi
                        AND ( UPPER(usua_login) = '".$user_login."' )
                        ORDER  BY 1";
            $execute= $this->db->conn->execute($query);

            return $execute->fetchRow();

        }catch(\Exception $e){
            return $e->getMessage();
        }
    }

    public function get_loans($user)
    {
        try{
            $user_arr   = explode('-',$user);
            $user_login = strtoupper(trim($user_arr[1]));

            $query= "SELECT COUNT(1) AS PRESTAMOS
                    FROM prestamo P,
                    usuario U,
                    dependencia D,
                    sgd_parametro ES,
                    sgd_parametro RE,
                    dependencia DA
                    WHERE  U.usua_login = P.usua_login_actu
                    AND D.depe_codi = P.depe_codi
                    AND ES.param_codi = P.pres_estado
                    AND ES.param_nomb = 'PRESTAMO_ESTADO'
                    AND RE.param_codi = P.pres_requerimiento
                    AND RE.param_nomb = 'PRESTAMO_REQUERIMIENTO'
                    AND DA.depe_codi = P.pres_depe_arch
                    AND P.usua_login_actu LIKE '".$user_login."'
                    AND P.pres_estado IN (2,5)";

            $execute= $this->db->conn->execute($query);
            $result= $execute->fetchRow();

            return $result['PRESTAMOS'];

        }catch(\Exception $e){
            return $e->getMessage();
        }
    }

    public static function loan_state($user){
        try{
            include_once "../../config.php";
            include_once "../../include/db/ConnectionHandlerNew.php";
            $db = new \ConnectionHandler("../..");
            $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);

            $query= "SELECT COUNT(1) AS PRESTAMOS
                    FROM prestamo P,
                    usuario U,
                    dependencia D,
                    sgd_parametro ES,
                    sgd_parametro RE,
                    dependencia DA
                    WHERE  U.usua_login = P.usua_login_actu
                    AND D.depe_codi = P.depe_codi
                    AND ES.param_codi = P.pres_estado
                    AND ES.param_nomb = 'PRESTAMO_ESTADO'
                    AND RE.param_codi = P.pres_requerimiento
                    AND RE.param_nomb = 'PRESTAMO_REQUERIMIENTO'
                    AND DA.depe_codi = P.pres_depe_arch
                    AND P.usua_login_actu LIKE '".$user."'
                    AND P.pres_estado IN (1)";

            $execute= $db->conn->execute($query)->fetchRow();

            if($execute['PRESTAMOS'] > 0) {
                $result = 1;
            }else{
                $result = 0;
            }

            return $result;

        }catch(\Exception $e){
            return $e->getMessage();
        }
    }

    public static function userIsActive($user){
        try{
            include_once "../../config.php";
            include_once "../../include/db/ConnectionHandlerNew.php";
            $db = new \ConnectionHandler("../..");
            $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);

            $query= "SELECT USUA_ESTA AS ACTIVO FROM USUARIO u WHERE u.usua_login='$user'";

            $result = $db->conn->execute($query)->fetchRow();

            return $result["ACTIVO"];

        }catch(\Exception $e){
            return $e->getMessage();
        }
    }
}