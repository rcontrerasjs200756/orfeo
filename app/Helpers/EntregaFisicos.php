<?php

namespace App\Helpers;

use Aura\SqlQuery\Exception;
use Aura\SqlQuery\QueryFactory;

class EntregaFisicos
{
    protected $query_factory;
    protected $pdo;
    protected $db;

    public function __construct()
    {
        require '../../vendor/autoload.php';
        include_once "../../config.php";
        include_once "../../include/db/ConnectionHandlerNew.php";
        $this->db = new \ConnectionHandler("../..");
        $this->db->conn->SetFetchMode(ADODB_FETCH_ASSOC);

    }

    public function process_number($radic_number)
    {
        $query = "SELECT r.radi_nume_radi As radicado,
            r.ra_asun AS asunto,
            TO_CHAR(r.radi_fech_radi, 'YYYY-MM-DD HH24:MI:SS' ) AS fecha_radi,
            a.anex_codigo As anexo_codigo,
            a.anex_desc As descripcion_anexo,
            a.anex_estado As estado,
            a.anex_borrado,
            a.anex_solo_lect,
            a.anex_tipo
            FROM radicado r
            LEFT JOIN anexos a
            ON r.radi_nume_radi=a.anex_radi_nume
            WHERE r.radi_nume_radi=".$radic_number."
            AND (a.sgd_deve_codigo >= 90 OR a.sgd_deve_codigo=0 OR a.sgd_deve_codigo IS NULL)
            AND ((r.sgd_eanu_codigo !=2 AND r.sgd_eanu_codigo !=1) OR r.sgd_eanu_codigo IS NULL)
            ORDER BY a.anex_codigo ASC ";

        //Sending the results to evaluation function
        $radicated_data = $this->evaluate_search_response($this->db->conn->getAll($query));

        return json_encode($radicated_data);
    }

    public function evaluate_search_response($radicated_data)
    {
        $response = null;
        //verifying if the radicated data is not empty
        if (! empty($radicated_data)) {
            //veifying the first condition
            if (count($radicated_data) == 1 && is_null($radicated_data[0]['ANEXO_CODIGO'])) {
                $response["type"] = "a";
                $response["data"] = $radicated_data;
            } else {
                //Verifying the second condition
                foreach ($radicated_data as $key => $data) {
                    if ($radicated_data[$key]['ESTADO'] > 1
                        && $radicated_data[$key]['ANEX_SOLO_LECT'] == "S"
                        && in_array($radicated_data[$key]['ANEX_TIPO'], [0, 14, 15, 16, 18, 24])
                        && $radicated_data[$key]['ANEX_BORRADO'] == "N"
                    ) {
                        $response["type"] = "b1";
                        $response["data"] = $data;
                        break;
                    }
                }
                //verifying the third condition
                if ($response == null) {
                    $response["type"] = "b2";
                    foreach ($radicated_data as $key => $data) {
                        $response["data"][$key] = $data;
                    }
                }
            }
        }
        return $response;
    }

    public function get_anexed($number)
    {
        $query = "SELECT anex_radi_nume As radicado,
            anex_desc As asunto,
            anex_codigo As anexo_codigo,
            anex_desc As descripcion_anexo,
            anex_estado As estado,
            TO_CHAR(anex_radi_fech, 'YYYY-MM-DD HH24:MI:SS' ) As fecha_radi,
            anex_borrado,
            anex_solo_lect,
            anex_tipo
            FROM anexos
            WHERE anex_radi_nume='".$number."'";

        $radicated_data = $this->db->conn->getAll($query);

        return $radicated_data;
    }

    public function get_radicated($number)
    {
        $query = "SELECT r.radi_nume_radi As radicado,
            r.ra_asun AS asunto,
            TO_CHAR(r.radi_fech_radi, 'YYYY-MM-DD HH24:MI:SS' ) AS fecha_radi
            FROM radicado r
            WHERE r.radi_nume_radi=".$number."";


        $select = $this->db->conn->execute($query);
        $radicated_data = $select->fetchRow();

        return $radicated_data;
    }

    public function get_sgd_dir_tipo($radicated)
    {
        $query = "SELECT sgd_dir_tipo
                FROM sgd_dir_drecciones r
                WHERE r.radi_nume_radi= ".$radicated."";

        $select = $this->db->conn->execute($query);
        $value = $select->fetchRow();

        return $value["SGD_DIR_TIPO"];
    }

    public function process_a_data($data)
    {
        session_start();
        $sgd_dir = $this->get_sgd_dir_tipo($data[0]->RADICADO);

        $query = "INSERT INTO ANEXOS
          ( ANEX_RADI_NUME,
          ANEX_CODIGO,
          ANEX_TIPO,
          ANEX_TAMANO,
          ANEX_SOLO_LECT,
          ANEX_CREADOR,
          ANEX_DESC,
          ANEX_NUMERO,
          ANEX_NOMB_ARCHIVO,
          ANEX_BORRADO,
          ANEX_ORIGEN,
          ANEX_UBIC,
          ANEX_SALIDA,
          RADI_NUME_SALIDA,
          ANEX_RADI_FECH,
          ANEX_ESTADO,
          USUA_DOC,
          SGD_REM_DESTINO,
          SGD_DIR_TIPO,
          ANEX_DEPE_CREADOR,
          SGD_TPR_CODIGO,
          SGD_FECH_IMPRES,
          ANEX_FECH_ANEX,
          SGD_TRAD_CODIGO )
          VALUES
          ( ".$data[0]->RADICADO.",
          '".$data[0]->RADICADO."00001',
          0,
          0,
          'S',
          '".$_SESSION["krd"]."',
          'Radicado sin plantilla',
          1,
          '',
          'N',
          2,
          'Sin Plantilla',
          0,
          ".$data[0]->RADICADO.",
          TO_TIMESTAMP('".date('Y-m-d H:i:s')."','YYYY-MM-DD HH24:MI:SS'),
          3,
          '".$_SESSION["usua_doc"]."',
          1,
          ".$sgd_dir.",
          ".$_SESSION["dependencia"].",
          null,
          TO_TIMESTAMP('".date('Y-m-d H:i:s')."','YYYY-MM-DD HH24:MI:SS'),
          TO_TIMESTAMP('".date('Y-m-d H:i:s')."','YYYY-MM-DD HH24:MI:SS'),
          ".substr($data[0]->RADICADO, -1).")";

        $inserted_row = $this->db->conn->execute($query);
        if (! $inserted_row) {
            print_r($this->db->conn->errorMsg());
        }
        //Retrieve inserted record
        $select = "SELECT * FROM anexos
            WHERE anex_radi_nume=".$data[0]->RADICADO."
            ORDER BY anex_radi_nume DESC ";

        $result = $this->db->conn->execute($select);

        //executing & returning
        return $result->fetchRow();
    }

    public function get_b2_anexed($number)
    {
        $query = "SELECT anex_radi_nume As radicado,
            anex_desc As asunto,
            anex_codigo As anexo_codigo,
            anex_desc As descripcion_anexo,
            anex_estado As estado,
            TO_CHAR(anex_radi_fech, 'YYYY-MM-DD HH24:MI:SS' ) As fecha_radi,
            anex_borrado,
            anex_solo_lect,
            anex_tipo
            FROM anexos
            WHERE anex_codigo='".$number."'";

        $select = $this->db->conn->execute($query);
        $radicated_data = $select->fetchRow();

        return $radicated_data;
    }

    public function save_delivered($rad_number, $op, $obs, $user)
    {
        session_start();
        $ob_count = 0;
        $result = NULL;
        foreach ($rad_number as $key => $row) {
            $usua_entrega = $this->get_user_data($user);
            $process = $op[$key];
            if ($op[$key] == "anexed") {
                $observation = $obs[$ob_count];
                $ob_count++;
            }
            if ($op[$key] == "b1_state") {
                $process = "radicated";
                $observation = $obs[$ob_count];
                $ob_count++;
            }

            $query = "INSERT INTO ENTREGA_FISICOS
              (ANEX_CODIGO,
              PROCESS_TYPE,
              FECHA,
              USUA_LOGIN_ENTREGA,
              DEPE_CODI_ENTREGA,
              USUA_LOGIN_RECIBE,
              DEPE_CODI_RECIBE,
              OBSERVACION)
              VALUES
              ('$row',
              '$process',
              TO_TIMESTAMP('".date('Y-m-d H:i:s')."','YYYY-MM-DD HH24:MI:SS'),
              '".$usua_entrega['USUA_LOGIN']."',
              '".$usua_entrega['DEPENDENCIA']."',
              '".$_SESSION['usua_login']."',
              '".$_SESSION['dependencia']."',
              '$observation')";
            //executing
            $response= $this->db->conn->execute($query);
            if (! $response) {
                print_r($this->db->conn->errorMsg());
            }
            $result = "saved";
        }
        return $result;
    }

    public function signature_check($user, $pass)
    {
        $query = "SELECT usua_login, usua_pasw
            FROM usuario
            WHERE usua_login='" . strtoupper($user) . "'
            AND usua_pasw='" . substr(md5($pass), 1, 26) . "'";

        $user = $this->db->conn->getAll($query);

        if (count($user) > 0) {
            $response = TRUE;
        } else {
            $response = FALSE;
        }
        return $response;
    }

    public function update_doc_mng()
    {
        $query = "SELECT e.anex_codigo AS anex_codigo,
            e.process_type AS proceso,
            TO_CHAR(e.fecha, 'YYYY-MM-DD HH24:MI:SS' ) AS fecha_entrega,
            e.usua_login_entrega AS usuario,
            a.anex_radi_nume AS radicado,
            a.anex_desc AS descripcion
            FROM entrega_fisicos e
            LEFT JOIN anexos a
            ON e.anex_codigo=a.anex_codigo
            WHERE e.delivered=0";

        $data = $this->db->conn->getAll($query);

        return $data;
    }

    public function receive_docs($data)
    {
        session_start();
        $history = null;
        foreach ($data as $row) {
            $result = $this->get_anexed_data($row);
            if ($result['PROCESO'] == "radicated") {
                $user   = $this->get_user_data($result['USUA_ENTREGA']);
                $update = $this->anex_update($row);
                $observacion = "Documento físico del radicado, entregado por " . $user['USUA_NOMBRE'] . " y recibido por " . $_SESSION['usua_nomb'];
                if (!empty($result['OBSERVACION'])) {
                    $observacion .= ". Documento principal del radicado, entregado para modificar:" . $result['OBSERVACION'];
                }

                $history = $this->new_historico(
                    substr($row, 0, 14),
                    $user['DEPENDENCIA'],
                    $user['USUA_CODI'],
                    $_SESSION['dependencia'],
                    $_SESSION['codusuario'],
                    5,
                    $observacion,
                    $user['USUA_DOC']
                );
            } else {
                $user = $this->get_user_data($result['USUA_ENTREGA']);
                $observacion = "Documento físico anexo, entregado por " . $user['USUA_NOMBRE'] . " y recibido por " . $_SESSION['usua_nomb'] . " , " . $result['OBSERVACION'];
                $history = $this->new_historico(
                    substr($row, 0, 14),
                    $user['DEPENDENCIA'],
                    $user['USUA_CODI'],
                    $_SESSION['dependencia'],
                    $_SESSION['codusuario'],
                    5,
                    $observacion,
                    $user['USUA_DOC']
                );
            }
            $update_delivered = $this->update_delivered($result['ID']);
        }
        return $history;
    }
    public function update_delivered($id)
    {
        $record = "SELECT * FROM entrega_fisicos WHERE id=".$id."";
        $result = $this->db->conn->execute($record);
        $data = [
            "delivered" => 1
        ];
        $query = $this->db->conn->getUpdateSql($result, $data);

        //executing
        $this->db->conn->execute($query);

        return "updated";
    }

    public function get_anexed_data($code)
    {
        $query = "SELECT e.id AS id,
            e.anex_codigo AS anex_codigo,
            e.process_type AS proceso,
            e.usua_login_entrega AS usua_entrega,
            e.observacion AS observacion,
            a.anex_desc AS descripcion
            FROM entrega_fisicos e
            LEFT JOIN anexos a
            ON e.anex_codigo=a.anex_codigo
            WHERE e.anex_codigo='".$code."'
            AND e.delivered=0";

        $select = $this->db->conn->execute($query);

        $data = $select->fetchRow();

        return $data;
    }

    public function anex_update($number)
    {
        session_start();
        $data = [];
        $rad_type = json_decode($this->process_number(substr($number, 0, 14)));

        if ($rad_type->type == 'b1' || $rad_type->type == 'a') {
            $query = "UPDATE anexos
              SET
              ANEX_ESTADO=3,
              SGD_FECH_IMPRES= TO_TIMESTAMP('".date("Y-m-d H:i:s")."','YYYY-MM-DD HH24:MI:SS')
              WHERE anex_codigo='".$number."'";
        }
        if ($rad_type->type == 'b2') {
            $query = "UPDATE anexos
              SET
              ANEX_ESTADO=3,
              ANEX_SOLO_LECT='S',
              ANEX_SALIDA=1,
              ANEX_RADI_FECH= TO_TIMESTAMP('".date("Y-m-d H:i:s")."','YYYY-MM-DD HH24:MI:SS'),
              RADI_NUME_SALIDA= ".substr($number, 0, 14).",
              SGD_DIR_TIPO= 1,
              SGD_FECH_IMPRES= TO_TIMESTAMP('".date("Y-m-d H:i:s")."','YYYY-MM-DD HH24:MI:SS'),
              ANEX_DEPE_CREADOR= ".$_SESSION['dependencia']."
              WHERE anex_codigo='".$number."'";
        }
        echo "q1: ".$query;
        //executing
        $this->db->conn->execute($query);
        return "updated";
    }

    public function get_user_data($usua_login)
    {
        $query= "SELECT e.usua_login AS usua_login,
            e.usua_nomb AS usua_nombre,
            e.depe_codi AS dependencia,
            e.usua_codi AS usua_codi,
            e.usua_doc AS usua_doc
            FROM usuario e
            WHERE e.usua_login='".$usua_login."'";

        $select = $this->db->conn->execute($query);
        $data = $select->fetchRow();

        return $data;
    }

    public function new_historico(
        $rad_num,
        $depeOrigen,
        $usCodOrigen = null,
        $depeDestino = null,
        $usCodDestino = null,
        $tipoTx,
        $observacion,
        $usuaDocOrigen
    ){
        $query = "INSERT INTO HIST_EVENTOS
          ( DEPE_CODI,
          HIST_FECH,
          USUA_CODI,
          RADI_NUME_RADI,
          HIST_OBSE,
          USUA_CODI_DEST,
          USUA_DOC,
          SGD_TTR_CODIGO,
          DEPE_CODI_DEST )
          VALUES
          ( ".$depeOrigen.",
          TO_TIMESTAMP('".date("Y-m-d H:i:s")."','YYYY-MM-DD HH24:MI:SS'),
          ".$usCodOrigen.",
          ".$rad_num.",
          '".$observacion."',
          ".$usCodDestino.",
          '".$usuaDocOrigen."',
          ".$tipoTx.",
          ".$depeDestino." )";

        echo "q2: ".$query;
        //executing
        $this->db->conn->execute($query);

        return "saved";
    }

    //Cancel the receive proccess deleting the undelivered records
    public function cancel_receive()
    {
        $query = "SELECT
            e.id AS id,
            e.anex_codigo AS anex_codigo,
            a.anex_radi_nume AS radi_nume
            FROM entrega_fisicos e
            LEFT JOIN anexos a
            ON e.anex_codigo=a.anex_codigo
            WHERE e.delivered = 0";

        $result = $this->db->conn->getAll($query);
        foreach ($result as $row) {
            $q = "SELECT * FROM anexos
                WHERE anex_radi_nume=" . $row['RADI_NUME'] . "
                AND anex_ubic= 'Sin Plantilla'";
            $attachment = $this->db->conn->execute($q);
            if (!empty($attachment)) {
                $att = $attachment->fetchRow();
                $delete_attach = "DELETE FROM anexos WHERE id=" . $att["ID"];
                $this->db->conn->execute($delete_attach);
            }
            $del_ent_fis = "DELETE FROM entrega_fisicos WHERE id=" . $row['ID'];
            $this->db->conn->execute($del_ent_fis);
        }
        return "Processed";
    }
}