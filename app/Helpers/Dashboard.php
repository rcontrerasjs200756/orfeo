<?php

namespace App\Helpers;


use Aura\SqlQuery\Exception;
use Aura\SqlQuery\QueryFactory;
use Carbon\Carbon;

class Dashboard
{
    protected $query_factory;
    protected $pdo;
    protected $user;
    protected $driver;

    public function __construct($db = array(),$rutaraiz="../..")
    {
        if(!isset($_SESSION['usuario_id']))
        {
            session_start();
        }
        require $rutaraiz.'/vendor/autoload.php';
        $this->user = $_SESSION;

        include_once "$rutaraiz/config.php";

        if (count($db) < 1) {
            include_once "$rutaraiz/include/db/ConnectionHandlerNew.php";

            $this->db = new \ConnectionHandler($rutaraiz);
            $this->db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        } else {
            $this->db = $db;
        }
    }

    /**
     * Permite obtener el conteo de radicados que no han sido clasificados
     * @return string
     */
    public function getUnclassified()
    {
        $query = "SELECT
            r.radi_nume_radi AS radicado
            FROM radicado r
            LEFT JOIN sgd_rdf_retdocf t
            ON r.radi_nume_radi=t.radi_nume_radi
            INNER JOIN usuario u
            ON (r.radi_usua_actu=u.usua_codi AND r.radi_depe_actu=u.depe_codi)
            WHERE r.radi_depe_actu = ".$this->user['dependencia']."
            AND r.radi_usua_actu = ".$this->user['codusuario']."
            AND t.sgd_mrd_codigo IS NULL";

        return $this->db->conn->getAll($query);
    }

    /**
     * Corresponde a los radicados que No se han incluido en un Expediente
     * @return string
     */
    public function getWithoutFile()
    {
        $query= "SELECT
            r.radi_nume_radi AS radicado
            FROM radicado r
            LEFT JOIN sgd_exp_expediente e
            ON r.radi_nume_radi=e.radi_nume_radi
            INNER JOIN usuario u
            ON (r.radi_usua_actu=u.usua_codi AND r.radi_depe_actu=u.depe_codi)
            WHERE r.radi_depe_actu = ".$this->user['dependencia']."
            AND r.radi_usua_actu = ".$this->user['codusuario']."
            AND e.sgd_exp_numero IS NULL";

        return $this->db->conn->getAll($query);
    }

    /**
     * Permite obtener el conteo de radicados que no se ha finalizado su trámite
     * @return string
     */
    public function getUnfinished()
    {
        $query = "SELECT r.radi_nume_radi AS radicado
            FROM radicado r
            WHERE r.radi_depe_actu = ".$this->user['dependencia']."
            AND r.radi_usua_actu = ".$this->user['codusuario'];

        return $this->db->conn->getAll($query);
    }

    /**
     * Permite obtener el conteo de radicados que no se ha finalizado su trámite
     * @return string
     */
    public function getPqr()
    {
        $query = "SELECT
            r.radi_nume_radi AS radicado
            FROM radicado r
            LEFT JOIN sgd_rdf_retdocf t
            ON r.radi_nume_radi=t.radi_nume_radi
            INNER JOIN usuario u
            ON (r.radi_usua_actu=u.usua_codi AND r.radi_depe_actu=u.depe_codi)
            LEFT JOIN sgd_mrd_matrird m
            ON t.sgd_mrd_codigo=m.sgd_mrd_codigo
            WHERE r.radi_depe_actu = ".$this->user['dependencia']."
            AND r.radi_usua_actu = ".$this->user['codusuario']."
            AND (m.sgd_srd_codigo In ( 5,56 ) OR UPPER(r.ra_asun) LIKE UPPER('%Derecho%Petici%') OR UPPER(r.ra_asun) LIKE UPPER('%PQR%'))";

        return $this->db->conn->getAll($query);
    }

    /*
     * Datos para grafica de dias de radicado
     */
    public function getStatsData()
    {
        $query = "SELECT r.ra_asun AS asunto,
                r.radi_nume_radi AS radicado,
                r.radi_fech_radi AS fecha_radicado,
                EXTRACT(DAY FROM (current_timestamp - r.radi_fech_radi)) AS dias_de_radicado
                FROM radicado r
                WHERE r.radi_depe_actu = ".$this->user['dependencia']."
                AND r.radi_usua_actu = ".$this->user['codusuario']."
                GROUP BY r.radi_nume_radi, r.ra_asun, r.radi_fech_radi
                ORDER BY dias_de_radicado ASC";

        return $this->db->conn->getAll($query);
    }

    public function recentRadicated()
    {
        $query = "SELECT
            t.sgd_ttr_descrip as transaccion,
            t.sgd_ttr_codigo as transaccion_codigo,
            h.radi_nume_radi as radicado,
            max(h.hist_obse) as comentario,
            r.ra_asun as asunto,
            r.radi_path as path,
            max(h.hist_fech) as fecha,
            (current_timestamp - max(h.hist_fech)) as min_hora_dias
            FROM radicado r
            INNER JOIN hist_eventos h
            ON r.radi_nume_radi=h.radi_nume_radi
            INNER JOIN sgd_ttr_transaccion t
            ON h.sgd_ttr_codigo=t.sgd_ttr_codigo
            WHERE h.depe_codi = ".$this->user['dependencia']."
            AND h.usua_codi = ".$this->user['codusuario']."
            AND h.sgd_ttr_codigo IN (2,8,9,12,13,14,15,16)
            GROUP BY
            r.ra_asun,
            t.sgd_ttr_descrip,
            t.sgd_ttr_codigo,
            h.radi_nume_radi,
            t.sgd_ttr_descrip,
            r.radi_path
            ORDER BY fecha DESC";

        //    return $this->db->conn->getAll($query);
        return $this->db->conn->selectLimit($query, 30)->getArray();

    }

    public function assembleGraphData($data)
    {
        $graph_data = [
            ["Menor a 1 día", 0],
            ["Entre 1 y 5 días", 0],
            ["Entre 5 y 10 días", 0],
            ["Entre 10 y 15 días", 0],
            ["Más de 15 días", 0],
        ];
        $today = Carbon::now();
        foreach ($data as $key => $row) {
            $created = Carbon::parse($row["FECHA_RADICADO"]);
            if ($created->diffInDays($today) == 0) {
                $graph_data[0][1] = $graph_data[0][1]+1;
            }
            if ($created->diffInDays($today) >= 1 && $created->diffInDays($today) <= 5) {
                $graph_data[1][1] = $graph_data[1][1]+1;
            }
            if ($created->diffInDays($today) >= 5 && $created->diffInDays($today) <= 10) {
                $graph_data[2][1] = $graph_data[2][1]+1;
            }
            if ($created->diffInDays($today) >= 10 && $created->diffInDays($today) <= 15) {
                $graph_data[3][1] = $graph_data[3][1]+1;
            }
            if ($created->diffInDays($today) > 15) {
                $graph_data[4][1] = $graph_data[4][1]+1;
            }
        }

        return $graph_data;
    }

    public function getRadicatedIndicator($from, $to)
    {
        $query = "SELECT
            COUNT(*) AS TOTAL
            FROM radicado
            WHERE
            radi_fech_radi
            BETWEEN TO_TIMESTAMP('".$from."', 'YYYY-MM-DD HH24:MI:SS')
            AND TO_TIMESTAMP('".$to."', 'YYYY-MM-DD HH24:MI:SS')
            AND radi_depe_radi < 999";

        $select = $this->db->conn->execute($query);
        if (! $select) {
            exit ($this->db->conn->errorMsg());
        }
        $value = $select->fetchRow();

        return $value;
    }

    public function getFinishedIndicator($from, $to)
    {
        $query = "SELECT COUNT(DISTINCT(h.radi_nume_radi)) AS TOTAL
            FROM radicado r
            INNER JOIN hist_eventos h
            ON r.radi_nume_radi=h.radi_nume_radi
            WHERE r.radi_depe_radi < 999
            AND h.sgd_ttr_codigo=13
            AND h.hist_fech
            BETWEEN TO_TIMESTAMP('".$from."', 'YYYY-MM-DD HH24:MI:SS')
            AND TO_TIMESTAMP('".$to."', 'YYYY-MM-DD HH24:MI:SS')";

        $select = $this->db->conn->execute($query);
        if (! $select) {
            exit ($this->db->conn->errorMsg());
        }
        $value = $select->fetchRow();

        return $value;
    }

    public function getWithoutFileIndicator($from, $to)
    {
        $query = "SELECT COUNT(DISTINCT(r.radi_nume_radi)) AS TOTAL
            FROM radicado r
            LEFT JOIN sgd_exp_expediente e On r.radi_nume_radi=e.radi_nume_radi
            INNER JOIN usuario u ON r.radi_usua_actu=u.usua_codi AND r.radi_depe_actu=u.depe_codi
            WHERE (r.sgd_eanu_codigo is null or r.sgd_eanu_codigo Not IN (1,2))
            AND r.radi_depe_radi < 999
            AND e.sgd_exp_numero IS NULL
            AND r.radi_fech_radi
            BETWEEN TO_TIMESTAMP('".$from."', 'YYYY-MM-DD HH24:MI:SS')
            AND TO_TIMESTAMP('".$to."', 'YYYY-MM-DD HH24:MI:SS')";

        $select = $this->db->conn->execute($query);
        if (! $select) {
            exit ($this->db->conn->errorMsg());
        }
        $value = $select->fetchRow();

        return $value;
    }

    public function getTrdIndicator($from, $to)
    {
        $query = "SELECT
            COUNT(DISTINCT (r.radi_nume_radi)) AS TOTAL
            FROM radicado r
            LEFT JOIN SGD_RDF_RETDOCF t ON r.radi_nume_radi=t.radi_nume_radi
            INNER JOIN usuario u ON r.radi_usua_actu=u.usua_codi AND r.radi_depe_actu=u.depe_codi
            WHERE
            (r.sgd_eanu_codigo IS NULL OR r.sgd_eanu_codigo NOT IN (1,2))
            AND r.radi_depe_radi < 999
            AND t.sgd_mrd_codigo IS NULL
            AND r.radi_fech_radi
            BETWEEN TO_TIMESTAMP('".$from."', 'YYYY-MM-DD HH24:MI:SS')
            AND TO_TIMESTAMP('".$to."', 'YYYY-MM-DD HH24:MI:SS')";

        $select = $this->db->conn->execute($query);
        if (! $select) {
            exit ($this->db->conn->errorMsg());
        }
        $value = $select->fetchRow();

        return $value;
    }

    public function getCanceledIndicator($from, $to)
    {
        $query= "SELECT COUNT(1) AS TOTAL
            FROM radicado r
            WHERE r.sgd_eanu_codigo IN (1,2)
            AND r.radi_fech_radi
            BETWEEN TO_TIMESTAMP('".$from."', 'YYYY-MM-DD HH24:MI:SS')
            AND TO_TIMESTAMP('".$to."', 'YYYY-MM-DD HH24:MI:SS')";

        $select = $this->db->conn->execute($query);
        if (! $select) {
            exit ($this->db->conn->errorMsg());
        }

        $value = $select->fetchRow();

        return $value;
    }
}