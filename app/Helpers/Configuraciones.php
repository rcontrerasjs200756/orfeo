<?php

namespace App\Helpers;

//error_reporting(E_ALL);

use Carbon\Carbon;
use http\Exception\BadQueryStringException;

class Configuraciones
{
    protected $query_factory;
    protected $pdo;
    protected $user;

    public function __construct($db = array(), $rutaraiz = "../..")
    {
        //al manejar varios helper, arroja error al redeclarar lasconexion a la bd, por eso si existe la varible db, es porque le estoy pasando
        //los datos de conexion especifica y no vuelva a redeclarar
        if (!isset($_SESSION)) {
            session_start();
        }
        require $rutaraiz . '/vendor/autoload.php';
        $this->user = $_SESSION;

        include_once $rutaraiz . "/config.php";

        if (count($db) < 1) {
            include_once $rutaraiz . "/include/db/ConnectionHandlerNew.php";

            $this->db = new \ConnectionHandler($rutaraiz);
            $this->db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        } else {
            $this->db = $db;
        }
    }

    function getConfiguraciones($constante = false, $valor = false, $modulo = false)
    {

        try {
            $query = "select * from configuraciones where id is not null ";

            //los paso como IN para poder retornar varias si asi lo deseo
            if ($constante != false) {
                $query .= " and nombre_constante in(" . $constante . ") ";
            }
            if ($valor != false) {
                $query .= " and valor = '" . $valor . "' ";
            }
            if ($modulo != false) {
                $query .= " and modulo in(" . $modulo . ") ";
            }
            $query .= " order by  CASE
        WHEN tipo LIKE 'f%' THEN 1
        WHEN valor_tipo ='lista_desplegable_multiple' THEN 2
        ELSE 3
         END, id";


            return $this->db->conn->selectLimit($query, 10000)->getArray();

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    function updateOpciones($constante, $valor, $filaconfig)
    {
        try {

            $valor_previo = $this->getConfiguraciones("'" . $constante . "'");
            if (count($valor_previo) > 0) {

                if ($valor_previo[0]['VALOR'] != $valor) {
                    $anterior = "";
                    if ($valor_previo[0]['VALOR'] == null) {
                        $anterior = "null";
                    } else {
                        $anterior = $valor_previo[0]['VALOR'];
                    }
                    //si es lista_desplegable_multiple, entonces lo convierto a string
                    if ($filaconfig[0]['VALOR_TIPO'] == 'lista_desplegable_multiple') {
                        $newvalue = array();
                        if (is_array($valor)) {
                            $newvalue = json_encode($valor);
                        }
                        $valor = $newvalue;
                    }

                    //guardo el historial
                    $this->insertHistorial($_SESSION['usuario_id'], $valor_previo[0]['ID'], $valor, $anterior);

                    $query = "update configuraciones set opciones=";
                    if ($valor == null) {
                        $query .= "null";
                    } else {
                        $query .= "'" . $valor . "'";
                    }
                    $query .= ", fecha_actualizacion=CURRENT_TIMESTAMP,
            actualizado_por=" . $_SESSION["usuario_id"] . ", valor_anterior=";
                    if ($valor_previo[0]['VALOR'] == null) {
                        $query .= "null";
                    } else {
                        $query .= "'" . $valor_previo[0]['VALOR'] . "'";
                    }

                    $query .= " where nombre_constante='" . $constante . "'  ";

                    $response = $this->db->conn->execute($query);
                    return $response;
                }
            }

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function updateValorConfig($constante, $valor, $filaconfig)
    {
        try {

            $valor_previo = $this->getConfiguraciones("'" . $constante . "'");
            if (count($valor_previo) > 0) {

                if ($valor_previo[0]['VALOR'] != $valor) {
                    $anterior = "";
                    if ($valor_previo[0]['VALOR'] == null) {
                        $anterior = "null";
                    } else {
                        $anterior = $valor_previo[0]['VALOR'];
                    }
                    //si es lista_desplegable_multiple, entonces lo convierto a string
                    if ($filaconfig[0]['VALOR_TIPO'] == 'lista_desplegable_multiple') {
                        $newvalue = array();
                        if (is_array($valor)) {
                            $newvalue = json_encode($valor);
                        }
                        $valor = $newvalue;
                    }

                    //guardo el historial
                    $this->insertHistorial($_SESSION['usuario_id'], $valor_previo[0]['ID'], $valor, $anterior);

                    $query = "update configuraciones set valor=";
                    if ($valor == null) {
                        $query .= "null";
                    } else {
                        $query .= "'" . $valor . "'";
                    }
                    $query .= ", fecha_actualizacion=CURRENT_TIMESTAMP,
            actualizado_por=" . $_SESSION["usuario_id"] . ", valor_anterior=";
                    if ($valor_previo[0]['VALOR'] == null) {
                        $query .= "null";
                    } else {
                        $query .= "'" . $valor_previo[0]['VALOR'] . "'";
                    }

                    $query .= " where nombre_constante='" . $constante . "'  ";

                    $response = $this->db->conn->execute($query);
                    return $response;
                }
            }

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    //verifica si existe radicado asocaido origen, para un determinado borrador
    public function getTitulosTratamiento($id = false, $titulo = false, $activo = 1, $orden = false)
    {
        try {
            $query = "SELECT  *
                FROM titulo_tratamiento where activo=" . $activo;

            if ($id != false) {
                $query .= " and id=" . $id . " ";
            }
            if ($titulo != false) {
                $query .= " and titulo = '" . $titulo . "' ";
            }
            if ($orden != false) {
                $query .= " and orden=" . $orden . " ";
            }
            $query .= "order by orden ";


            return $this->db->conn->selectLimit($query, 10000)->getArray();

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function insertTitulo($titulo, $abreviatura, $activo = 1, $orden)
    {
        try {
            $json = array();
            $buscarOrden = $this->getTitulosTratamiento(false, false, 1, $orden);
            if (count($buscarOrden) > 0 && $buscarOrden[0]['ID'] < 11) {

                $json['error'] = "El orden ya se encuentra utilizado, no puede guardar este título";
                return $json;
            }
            if (count($buscarOrden) > 0) {
                $titulos = $this->getTitulosTratamiento(false, false, 1, false);
                $this->updateOrdenTitulo($buscarOrden[0]['ID'], count($titulos) + 1);
            }
            $query = "insert  into titulo_tratamiento (titulo,abreviatura,activo,orden)
values('" . $titulo . "'," . $abreviatura . "," . ($activo + 0) . "," . ($orden + 0) . ")";

            $response = $this->db->conn->execute($query);
            return 1;


        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function insertTituloMemo($titulo)
    {
        try {
            $json = array();
            $buscarConfig = $this->getConfiguraciones("'TITULO_MEMO'");

            if (count($buscarConfig) > 0) {

                $opciones = $buscarConfig[0]['OPCIONES'];
                $opciones = json_decode($opciones);
                $opciones = (array)$opciones;
                $cont = count($opciones);
                $opciones[$cont] = $titulo;

                $actualizar = $this->updateOpciones('TITULO_MEMO', $opciones, $buscarConfig);
            }

            return 1;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    //inserta en historial
    public function insertHistorial($usaurio, $configuracion_id, $valor, $valor_anterior)
    {
        try {
            $query = "insert  into historial_configuracion (usuario_id,fecha,configuracion_id,valor,valor_anterior)
values(" . $usaurio . ",CURRENT_TIMESTAMP," . $configuracion_id . ",'" . $valor . "','" . $valor_anterior . "')";

            $response = $this->db->conn->execute($query);
            return 1;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function updateOrdenTitulo($id, $orden)
    {
        try {

            $query = "update titulo_tratamiento set orden=" . $orden . "  where id=" . $id . "  ";
            $response = $this->db->conn->execute($query);
            return $response;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function updateTitulo($id, $titulo, $abreviatura, $activo = 1, $orden)
    {
        try {

            $buscarOrden = $this->getTitulosTratamiento(false, false, 1, $orden);
            if (count($buscarOrden) > 0 && $buscarOrden[0]['ID'] != $id) {
                if ($buscarOrden[0]['ID'] < 11) {
                    $json['error'] = "El orden ya se encuentra utilizado, no puede editar este título";
                    return $json;
                }

                $titulo_queviene = $this->getTitulosTratamiento($id, false, 1, false);
                $this->updateOrdenTitulo($buscarOrden[0]['ID'], $titulo_queviene[0]['ORDEN']);
            }

            $query = "update titulo_tratamiento set titulo='" . $titulo . "', ";
            $query .= " abreviatura=" . $abreviatura . ",
            activo=" . $activo . ",  orden=" . $orden . "";
            $query .= " where id=" . $id . "  ";

            $response = $this->db->conn->execute($query);
            return 1;


        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    function getConfisPerfil($config_id = false, $perfil = false)
    {

        try {
            $query = "select * from config_has_perfil where id is not null ";

            //los paso como IN para poder retornar varias si asi lo deseo
            if ($config_id != false) {
                $query .= " and config_id in(" . $config_id . ") ";
            }
            if ($perfil != false) {
                $query .= " and perfil in(" . $perfil . ") ";
            }
            $query .= " order by id ";

            return $this->db->conn->selectLimit($query, 10000)->getArray();

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    function deleteAllPerfilConfigs()
    {

        try {
            $query = "delete from config_has_perfil where id is not null ";
            return $this->db->conn->execute($query);

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function insertPerfilConfigs($config, $perfil)
    {
        try {
            $query = "insert  into config_has_perfil (config_id,perfil)
values(" . $config . ",'" . $perfil . "')";
            $response = $this->db->conn->execute($query);
            return 1;


        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function updateVisibleConfig($id, $visible)
    {
        try {
            $query = "update configuraciones set visible=" . $visible . "  where id=" . $id . "  ";

            $response = $this->db->conn->execute($query);
            return $response;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    function savePassValidac($usuario, $clave)
    {
        try {
            $query = "update usuario set usua_pasw_doc='" . $clave . "',
               fecha_passw_doc =current_timestamp where id=" . $usuario . "  ";

            $response = $this->db->conn->execute($query);
            return $response;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    function saveHistCambioClaveValidac()
    {
        //esto solo se hace una vez,
        $query = " INSERT INTO sgd_usm_usumodifica(sgd_usm_modcod, sgd_usm_moddescr) VALUES (126, 'Cambio clave de validación'); ";

        $response = $this->db->conn->execute($query);
        return $response;
    }

    function saveHistCambioClaveValidacDos()
    {

        $query = "INSERT INTO sgd_ush_usuhistorico(
            sgd_ush_admcod, sgd_ush_admdep, sgd_ush_admdoc, sgd_ush_usucod, sgd_ush_usudep, sgd_ush_usudoc, sgd_ush_modcod, sgd_ush_fechevento, sgd_ush_usulogin)
	VALUES (" . $_SESSION['codusuario'] . "," . $_SESSION['dependencia'] . ", " . $_SESSION['usua_doc'] . ", 
	" . $_SESSION['codusuario'] . ", " . $_SESSION['dependencia'] . ",
        " . $_SESSION['usua_doc'] . ", '126', current_timestamp,'" . $_SESSION['usua_login'] . "');";


        $response = $this->db->conn->execute($query);
        return $response;
    }

    //busca si existe un arreglo ,dentro de otro arreglo
    function find_array_inside_array($array, $arraysearch = array())
    {
        $lengh_search = count($arraysearch);
        foreach ($array as $item) {
            $result_found = 0;
            foreach ($arraysearch as $key => $value) {
                if (isset($item[$key]) && $item[$key] == $value) $result_found++;
            }
            if ($result_found === $lengh_search) return true;
        }
        return false;
    }


    function pruebarad()
    {

        $this->db->conn->StartTrans();
        $query = " insert into tabla_prueba (id,texto) values(5,1);";

        $response = $this->db->conn->execute($query);

        $query = "insert into radicado (esta_codi) values(1); ";

        $response = $this->db->conn->execute($query);

        $this->db->conn->CompleteTrans();
    }


}