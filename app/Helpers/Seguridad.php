<?php

namespace App\Helpers;

//error_reporting(E_ALL);

use Carbon\Carbon;

class Seguridad
{
    protected $query_factory;
    protected $pdo;
    protected $user;

    public function __construct($db = array(),$rutaraiz="../..")
    {
        //al manejar varios helper, arroja error al redeclarar lasconexion a la bd, por eso si existe la varible db, es porque le estoy pasando
        //los datos de conexion especifica y no vuelva a redeclarar
        /*ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);*/
        session_start();
        if (!isset($_SESSION['usuario_id'])) {
            header ("Location: $rutaraiz/cerrar_session.php");
        }
        require $rutaraiz.'/vendor/autoload.php';
        $this->user = $_SESSION;

        include_once $rutaraiz."/config.php";

        if (count($db) < 1) {
            include_once $rutaraiz."/include/db/ConnectionHandlerNew.php";

            $this->db = new \ConnectionHandler("../..");
            $this->db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        } else {
            $this->db = $db;
        }
    }

    public function getSeguridadByRadi($radi)
    {
        try {
            $query = "SELECT seguridad.*
				FROM seguridad where radi_nume_radi=".$radi." ";

            return $this->db->conn->selectLimit($query, 10000)->getArray();

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function buscarUsuarios($coincide)
    {
        try {
            $query = "SELECT u.usua_nomb || ' (' || d.depe_codi ||'-'|| d.depe_nomb || ')' AS NOMBRE,  u.*
				FROM USUARIO u  Inner Join dependencia d On u.depe_codi=d.depe_codi
				WHERE u.usua_esta='1' and UPPER(u.USUA_NOMB) LIKE upper('%".$coincide."%')";
            return $this->db->conn->selectLimit($query, 50)->getArray();

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function buscarDependencia($coincide)
    {
        try {
            $query = "SELECT dependencia.*, dependencia.depe_nomb as NOMBRE
				FROM dependencia 
				WHERE UPPER(DEPE_NOMB) LIKE upper('%".$coincide."%')";
            return $this->db->conn->selectLimit($query, 50)->getArray();

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    //ordena y retorna los usuarios que se usan para enviar comentarios en borradores
    function barmarUsuariosComen($usuarios)
    {
        $cont = 0;
        $usuarioeenviar = array();
        foreach ($usuarios as $usuario) {
            $usuarioeenviar[$cont] = $usuario;
            $usuarioeenviar[$cont]['id'] = $usuario['ID'];
            $usuarioeenviar[$cont]['text'] = $usuario['NOMBRE'];
            $cont++;
        }
        return $usuarioeenviar;
    }

    //actualiza la seguridad de un radicado
    function updateSeguridadRad($seguridad,$radicado)
    {
        try {
            $query = "UPDATE radicado
SET sgd_spub_codigo =".$seguridad." where radi_nume_radi=".$radicado." ";
            $response = $this->db->conn->execute($query);
            return $response;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    //actualiza la seguridad de un radicado
    function addSeguridad($numRad,$usuario,$asegurador,$tipo)
    {
        try {
            $query = "INSERT INTO seguridad (RADI_NUME_RADI, USUARIOS_PERMITIDOS, USUARIO_ASEGURADOR,TIPO_USU_PERMITIDO)
VALUES ('$numRad',".$usuario.",".$asegurador.",'".$tipo."') ";
            $response = $this->db->conn->execute($query);
            return $response;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function deleteSegById($radicado,$id,$tipo)
    {
        try {
            $query = "delete from seguridad where radi_nume_radi=".$radicado." and usuarios_permitidos=".$id." and 
            tipo_usu_permitido='".$tipo."'  ";
            //executing
            $response = $this->db->conn->execute($query);
            return $response;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function deleteSegByRad($radicado)
    {
        try {
            $query = "delete from seguridad where radi_nume_radi=".$radicado."  ";
            //executing
            $response = $this->db->conn->execute($query);
            return $response;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

}

