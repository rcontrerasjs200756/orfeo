<?php

namespace App\Helpers;

//error_reporting(E_ALL);

use Carbon\Carbon;

class Radicados
{
    protected $query_factory;
    protected $pdo;
    protected $user;

    public function __construct($db = array(), $rutaraiz = "../..")
    {
        //al manejar varios helper, arroja error al redeclarar lasconexion a la bd, por eso si existe la varible db, es porque le estoy pasando
        //los datos de conexion especifica y no vuelva a redeclarar
        /*ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);*/
        if (!isset($_SESSION['usuario_id'])) {
            session_start();
        }
        require $rutaraiz . '/vendor/autoload.php';
        $this->user = $_SESSION;

        include_once $rutaraiz . "/config.php";

        if (count($db) < 1) {
            include_once "/include/db/ConnectionHandlerNew.php";

            $this->db = new \ConnectionHandler("../..");
            $this->db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        } else {
            $this->db = $db;
        }
    }

    public function radPorAnular($fechahoy, $ANULA_DIAS_TRAMITE = 30, $datos_session = false)
    {
        try {
            //trae la cantidad de expedientes que tiene este radicado

            $query = "SELECT 
  r.radi_nume_radi AS RADICADO
  ,r.radi_fech_radi AS FECHA_RADICADO
  ,r.ra_asun AS ASUNTO
  ,d.depe_codi
  ,d.depe_nomb AS DEPENDENCIA_ACTUAL
  ,u.usua_nomb AS USUARIO_ACTUAL
  ,u.usua_doc AS USUA_DOC_ACTUAL
  ,u.usua_codi AS USUA_CODI
  ,r.radi_path AS DOCUMENTO
  ,r.sgd_trad_codigo as TIPO_RADICADO
FROM radicado r
INNER JOIN usuario u ON u.depe_codi=r.radi_depe_actu AND u.usua_codi=r.radi_usua_actu
INNER JOIN dependencia d ON d.depe_codi=u.depe_codi
LEFT JOIN anexos a ON a.anex_radi_nume=r.radi_nume_radi
LEFT JOIN sgd_exp_expediente e ON r.radi_nume_radi=e.radi_nume_radi AND e.sgd_exp_estado !=2
LEFT JOIN entrega_fisicos ef ON r.radi_nume_radi=cast(left((ef.anex_codigo),14) as numeric)
LEFT JOIN sgd_dir_drecciones dir ON r.radi_nume_radi=dir.radi_nume_radi --AND dir.sgd_dir_tipo < 3

WHERE
dir.radi_nume_radi is not null --Que tenga destinatario
AND (r.sgd_eanu_codigo is null or r.sgd_eanu_codigo Not in (1,2)) --En solicitud de anulación a anulados (Comentar para incluir anulados)
AND r.sgd_trad_codigo NOT IN (2,4,6) --Los radicados de Entrada No se anulan. ";

            if ($datos_session == true) {
                $query .= "AND r.radi_depe_radi = " . $_SESSION["dependencia"] . " --Que haya sido radicado por la dependencia del usuario actual
AND r.radi_depe_actu = " . $_SESSION["dependencia"] . " --Que lo tenga la dependencia del usuario actual
AND r.radi_usua_actu = " . $_SESSION["codusuario"] . " --Que lo tenga el usuario actual";
            }

            $query .= "--AND a.anex_radi_nume is null --No se ha anexado nada.
AND (a.anex_radi_nume IS null OR (a.anex_radi_nume IS NOT null AND a.anex_estado < 3)) --No se ha anexado nada ó ningún anexo tiene 3 o 4 visto-chulo
AND ef.anex_codigo IS null --Que no haya sido entregado el impreso fisico.
AND (Select count(1) from hist_eventos h2 Where r.radi_nume_radi=h2.radi_nume_radi AND sgd_ttr_codigo IN (22,42)) = 0 --Que este Sin escanear
AND r.radi_fech_radi BETWEEN  to_date('" . $fechahoy . "','YYYY/MM/DD HH24:MI') and  (current_timestamp - INTERVAL '$ANULA_DIAS_TRAMITE DAY') 
GROUP BY r.radi_nume_radi, r.radi_nume_deri, d.depe_codi,d.depe_nomb, u.usua_nomb, r.ra_asun, r.radi_fech_radi, r.radi_path,u.usua_doc,u.usua_codi
ORDER BY 2 DESC, d.depe_codi --, u.usua_codi Asc, u.usua_nomb, r.radi_fech_radi asc
";

            return $this->db->conn->selectLimit($query, 100000)->getArray();

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    function selectNextVal()
    {
        $query = "(Select nextval('sgd_anu_id'))";
        //executing
        return $this->db->conn->selectLimit($query, 100000)->getArray();

    }

    //SQL 3: Insertar radicados en expediente anual de anulados
    function insertSgdAnulados($sgd_anu_id, $radi_nume_radi, $tipo_radicado, $depe_codi, $usua_doc, $usua_codi)
    {
        $query = "INSERT INTO sgd_anu_anulados (sgd_anu_id, sgd_anu_desc, radi_nume_radi, sgd_eanu_codi, sgd_anu_sol_fech,
 depe_codi, usua_doc, usua_codi, sgd_trad_codigo, sgd_anu_fech, depe_codi_anu, usua_doc_anu, usua_codi_anu, usua_anu_acta,
  sgd_anu_path_acta, notificacion_estado, notificacion_fecha) 
  VALUES (" . $sgd_anu_id . ", 'Solicitud Anulación: anulación automática por exceder el tiempo de trámite.'," . $radi_nume_radi . " , '1', 
  current_timestamp, " . $depe_codi . " , '" . $usua_doc . "' , " . $usua_codi . " , 
  " . $tipo_radicado . ", null, null, null, null, null, null, 0, null) ";


        //executing
        $response = $this->db->conn->execute($query);
        return $response;
    }

    //Insertar el Histórico de la anulación:
    function insertHistAnulacion($radi_nume_radi, $depe_codi, $usua_doc, $usua_codi)
    {
        $query = "Insert Into hist_eventos (depe_codi, hist_fech, usua_codi, radi_nume_radi, hist_obse,
 usua_codi_dest, usua_doc, usua_doc_old, sgd_ttr_codigo, hist_usua_autor, hist_doc_dest, depe_codi_dest) 
 VALUES (" . $depe_codi . ", current_timestamp, " . $usua_codi . ", " . $radi_nume_radi . ", 
 'Solicitud Anulación: anulación automática por exceder el tiempo de trámite.', " . $usua_codi . ", '" . $usua_doc . "',
  '', '25', '', '" . $usua_doc . "', " . $depe_codi . ") ";

        //executing
        $response = $this->db->conn->execute($query);
        return $response;
    }

    function excluirRadDeExped($expediente, $radi_nume_radi, $depe_codi, $usua_doc, $usua_codi)
    {
        //SQL 4:
        $query = "Insert Into sgd_exp_expediente (sgd_exp_numero, radi_nume_radi, sgd_exp_fech, depe_codi, 
usua_codi, usua_doc, sgd_exp_estado, sgd_exp_asunto) VALUES ('" . $expediente . "', " . $radi_nume_radi . ", 
current_timestamp, " . $depe_codi . ", " . $usua_codi . ", '" . $usua_doc . "', 0, 
'Insertado desde anulación automática por exceder el tiempo de trámite.' ) ";

        $response = $this->db->conn->execute($query);
        return $response;
    }

    //Insertar en el Histórico del Expediente:
    function insertHistExped($expediente, $radi_nume_radi, $depe_codi, $usua_doc, $usua_codi)
    {
        //SQL 4: excluir radicados de expedientes
        $query = "Insert Into sgd_hfld_histflujodoc (sgd_exp_numero, sgd_fexp_codigo, sgd_hfld_fech, radi_nume_radi,
 usua_doc, usua_codi, depe_codi, sgd_ttr_codigo, sgd_hfld_observa) 
 VALUES ('" . $expediente . "', 0, current_timestamp, " . $radi_nume_radi . ", '" . $usua_doc . "', " . $usua_codi . ",
  " . $depe_codi . ", '53', 'Incluido desde anulación automática por exceder el tiempo de trámite.' ) ";

        $response = $this->db->conn->execute($query);
        return $response;
    }

    //Insertar en el Histórico del Radicado:
    function insertHistRad($expediente, $radi_nume_radi, $depe_codi, $usua_doc, $usua_codi)
    {
        // SQL 4
        $query = "Insert Into hist_eventos (depe_codi, hist_fech, usua_codi, radi_nume_radi, hist_obse, usua_codi_dest, 
usua_doc, usua_doc_old, sgd_ttr_codigo, hist_usua_autor, hist_doc_dest, depe_codi_dest)
 VALUES (" . $depe_codi . ", current_timestamp, " . $usua_codi . ", " . $radi_nume_radi . ",
  'Radicado  incluido en expediente de ANULADOS: " . $expediente . "', " . $usua_codi . ", '" . $usua_doc . "', 
  '', '53', '', '" . $usua_doc . "', " . $depe_codi . ") ";

        $response = $this->db->conn->execute($query);
        return $response;
    }

    function updateRadicAnular($radi_nume_radi)
    {

        $query = "Update radicado Set sgd_eanu_codigo=1, tdoc_codi=3, radi_depe_actu=999 , radi_usua_actu=1, carp_codi=0
Where radi_nume_radi In ('" . $radi_nume_radi . "')";


        //executing
        $response = $this->db->conn->execute($query);
        return $response;
    }

    //SQL 5: excluir radicados de expedientes
    function updateExcluirRadExped($radi_nume_radi)
    {
        $query = "UPDATE sgd_exp_expediente Set sgd_exp_estado='2', sgd_exp_asunto='Excluido por anulación automática'
WHERE sgd_exp_estado!=2
AND radi_nume_radi=" . $radi_nume_radi . " --Numero de radicado a anular (solicitar anulación)
";

        $response = $this->db->conn->execute($query);
        return $response;
    }

    //SQL 5: excluir radicados de expedientes
    function insertHistExpedExclus($expediente, $radi_nume_radi, $depe_codi, $usua_doc, $usua_codi)
    {
        $query = "Insert Into sgd_hfld_histflujodoc (sgd_exp_numero, sgd_fexp_codigo, sgd_hfld_fech, radi_nume_radi, 
usua_doc, usua_codi, depe_codi, sgd_ttr_codigo, sgd_hfld_observa)
 VALUES ('" . $expediente . "', 0, current_timestamp, 
 " . $radi_nume_radi . ", '" . $usua_doc . "', " . $usua_codi . ", " . $depe_codi . ", '52', 
 'Radicado excluido del expediente por anulación automática.' ); ";

        $response = $this->db->conn->execute($query);
        return $response;
    }

    /*primero los actualizo la fecha de notificados*/
    function actNotifFechaRadAnul()
    {
        $query = "update sgd_anu_anulados  set notificacion_fecha=current_timestamp
where 
 depe_codi=" . $_SESSION['dependencia'] . " and  usua_doc='" . $_SESSION['usua_doc'] . "' and usua_codi= " . $_SESSION['codusuario'] . " 
 and  notificacion_fecha is null";

        $response = $this->db->conn->execute($query);
        return $response;

    }

    /*trae los tradicados anulados, para mostrar hoy*/
    function getRadicAnulNotifHoy()
    {
        $query = "select sgd_anu_anulados.radi_nume_radi as radicado, sgd_anu_sol_fech as fecha_anulado, radicado.ra_asun as asunto
 from sgd_anu_anulados INNER join radicado on radicado.radi_nume_radi=sgd_anu_anulados.radi_nume_radi  
where sgd_anu_anulados.depe_codi=" . $_SESSION['dependencia'] . " and  sgd_anu_anulados.usua_doc='" . $_SESSION['usua_doc'] . "' 
and sgd_anu_anulados.usua_codi= " . $_SESSION['codusuario'] . " 
 and notificacion_estado=0 and  notificacion_fecha > '" . date("Y-m-d H:i", strtotime(date("Y-m-d") . " 23:59" . ' -1day')) . "'  order by sgd_anu_sol_fech desc
 ";

        return $this->db->conn->selectLimit($query, 100000)->getArray();

    }

    /*trae los tradicados anulados, que no han sido notificados*/
    function getRadicSinNotif()
    {
        $query = "select sgd_anu_anulados.radi_nume_radi as radicado, sgd_anu_sol_fech as fecha_anulado, radicado.ra_asun as asunto
 from sgd_anu_anulados INNER join radicado on radicado.radi_nume_radi=sgd_anu_anulados.radi_nume_radi  
where sgd_anu_anulados.depe_codi=" . $_SESSION['dependencia'] . " and  sgd_anu_anulados.usua_doc='" . $_SESSION['usua_doc'] . "' 
and sgd_anu_anulados.usua_codi= " . $_SESSION['codusuario'] . " 
 and notificacion_estado=0 and  notificacion_fecha is null  order by sgd_anu_sol_fech desc
 ";
        return $this->db->conn->selectLimit($query, 100000)->getArray();

    }
    
    function getFechaRadicadobyCod($cod){
        $query = "select radi_fech_radi from radicado where radi_nume_radi = ".$cod;
        $response = $this->db->conn->selectLimit($query, 1)->getArray();
        return $response;
    }

    /*primero los actualizo la fech radi_fech_radia de notificados*/
    function actNotifEstadoRadAnul($fechahoy)
    {
        $query = "update sgd_anu_anulados  set notificacion_estado=1
where notificacion_fecha is not null and notificacion_fecha< '" . $fechahoy . "' ";

        $response = $this->db->conn->execute($query);
        return $response;

    }

    function updateRadi_nume_iden($radicado, $vRadi_nume_iden)
    {
        $query = "update radicado  set radi_nume_iden = '" . $vRadi_nume_iden . "' where radi_nume_radi = " . $radicado . " ";

        $response = $this->db->conn->execute($query);
        return $response;

    }

    /**
     * @param $coincide
     * @param bool $firmantes
     * @param bool $nobuscar_users
     * @return string
     *
     * $usua_esta define si busca o no, usuarios inactivo o activos, ya que para el modal  de validacion,
     * se tienen que traer los usuarios inactivos tambien
     */
    function getUsersWithPermAprob(
        $coincide,
        $firmantes = false,
        $nobuscar_users = false,
        $usua_esta = 1
    )
    {

        try {
            $query = "select u.id, usua_codi, u.usua_doc, u.usua_nomb,
 u.usua_nomb || ' (' || COALESCE(u.usua_cargo||' - ','') ||''|| d.depe_nomb || ')' AS NOMBRE, u.usua_cargo, u.usua_email, 
d.depe_nomb,d.depe_codi,  (CASE
	WHEN u.usua_codi=1 THEN 'Jefe'  
	ELSE ''
	END)	AS ROL From usuario u 
Inner Join dependencia d On u.depe_codi=d.depe_codi where ";

            if ($usua_esta == 'TODOS') {
                $query .= " d.depe_codi < 999 ";
            } else {
                $query .= " usua_esta='" . $usua_esta . "' And d.depe_codi < 999 ";
            }

            if ($_SESSION["dependencia"] == '998' || $_SESSION["dependencia"] == '993' ||
                $_SESSION["dependencia"] == '990') {
                //$query.=' and d.depe_codi < 999 ';
            } else {
                $query .= ' and d.depe_codi < 990 ';
            }

            if ($_SESSION['LISTADO_FIRMANTES'] == 'Solo Jefes') {
                $query .= " and u.usua_codi='1' ";
            }

            if ($_SESSION['LISTADO_FIRMANTES'] == 'Jefes y Aprobadores') {
                $query .= " and (u.usua_codi=1 OR u.usua_perm_aprobar=1) ";
            }

            if ($_SESSION['LISTADO_FIRMANTES'] == 'Jefes, Aprobadores, Reasignadores, Supervisores y Públicos') {
                $query .= " and (u.usua_codi=1 OR u.usua_perm_aprobar=1 
                OR u.usuario_reasignar=1 
                OR usua_supervisor=1 OR u.usuario_publico=1) ";
            }

            if ($nobuscar_users != false) {
                $query .= "  and u.id not in (" . $nobuscar_users . ")  ";
            }

            $query .= " and u.usua_esta = '1'  and (
 TRANSLATE(lower(u.usua_nomb),'ÁÉÍÓÚáéíóú','AEIOUaeiou') like translate(lower('%" . $coincide . "%'),'ÁÉÍÓÚáéíóú','AEIOUaeiou')
Or TRANSLATE(lower(d.depe_nomb),'ÁÉÍÓÚáéíóú','AEIOUaeiou') like translate(lower('%" . $coincide . "%'),'ÁÉÍÓÚáéíóú','AEIOUaeiou')
) ";


            if ($firmantes != false) {
                $query .= " OR ( ";

                if ($usua_esta == 'TODOS') {
                    $query .= "u.id in (" . $firmantes . ") ) ";
                } else {
                    $query .=  "u.usua_esta='" . $usua_esta . "' and u.id in (" . $firmantes . ") ) ";
                }

            }

            $query .= "  ORDER BY ";

            if ($firmantes != false) {
                $usuExplode = explode(',', $firmantes);
                foreach ($usuExplode as $row) {
                    $query .= " u.id=" . $row . " desc,";
                }
            }

            $query .= " u.usua_codi ASC, u.depe_codi ASC ";

            return $this->db->conn->selectLimit($query, 10000)->getArray();

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    function getDocumentMetadata($radicado)
    {
        $query = "select * from document_metadata where radi_nume_radi=$radicado";

        return $this->db->conn->selectLimit($query, 1)->getArray();
    }

    function getMetadataValidados()
    {
        $query = "select * from document_metadata";

        return $this->db->conn->selectLimit($query, 1000000)->getArray();
    }

    function getUsersValidaron($id_radicado_metadata)
    {
        $query = "select document_authorized_by.*, 
 u.id, usua_codi, u.usua_doc, u.usua_nomb,
u.usua_nomb || ' (' || d.depe_nomb || ')' AS NOMBRE, u.usua_cargo, u.usua_email, 
d.depe_nomb From document_authorized_by
 Inner Join usuario u  On u.id=document_authorized_by.usuario_id 
Inner Join dependencia d On u.depe_codi=d.depe_codi
where id_radicado_metadata=$id_radicado_metadata";

        return $this->db->conn->selectLimit($query, 100000)->getArray();
    }

    //busca los usuarios que validaron, un radicado especifico
    function getUsersValidByRad($radicado)
    {
        $query = "select document_authorized_by.*, document_metadata.id_radicado_metadata,
 u.id, usua_codi, u.usua_doc, u.usua_nomb,
u.usua_nomb || ' (' || d.depe_codi || ')' AS NOMBRE, u.usua_cargo, u.usua_email, 
d.depe_nomb From document_authorized_by
Inner Join document_metadata  On document_metadata.id_radicado_metadata=document_authorized_by.id_radicado_metadata 
 Inner Join usuario u  On u.id=document_authorized_by.usuario_id 
Inner Join dependencia d On u.depe_codi=d.depe_codi
where document_metadata.radi_nume_radi=$radicado";
        return $this->db->conn->selectLimit($query, 100000)->getArray();
    }

    //Insertar en document_metadata
    function insertdocument_metadata(
        $radi_nume_radi,
        $anex_codigo,
        $radi_path_original = 'null',
        $radi_path_validado = 'null',
        $clasification,
        $trd, $expediente,
        $expediente_titulo,
        $transacciones,
        $hash_doc_original = 'null',
        $hash_metadata = 'null',
        $codigoverificacion,
        $powered_by,
        $filetype = 'null',
        $filetype_ext = 'null',
        $mimetype = 'null',
        $type_version = 'null',
        $size = 'null',
        $pages = 'null',
        $radi_path_fuente = 'null',
        $pngbarra = 'null',
        $status_document,
        $ra_asun,
        $id_borrador
    )
    {
        $query = "INSERT INTO document_metadata (timestamp_m,radi_nume_radi, anex_codigo,
radi_path_original, radi_path_validado, clasification,
 trd, expediente, expediente_titulo, transacciones, hash_doc_original, hash_metadata, codigoverificacion, powered_by, filetype,
  filetype_ext, mimetype, type_version, size, pages,radi_path_fuente,path_img_barra,
  status_document,ra_asun,id_borrador) 
  VALUES (current_timestamp,$radi_nume_radi, $anex_codigo,'" . $radi_path_original . "', '" . $radi_path_validado . "',
   " . $clasification . " ,
  '" . $trd . "' , '" . $expediente . "' , '" . $expediente_titulo . "', null, '" . $hash_doc_original . "', '" . $hash_metadata . "',
  '" . $codigoverificacion . "', '" . $powered_by . "', '" . $filetype . "', '" . $filetype_ext . "','" . $mimetype . "','" . $type_version . "'
  ,'" . $size . "',$pages,'" . $radi_path_fuente . "','" . $pngbarra . "',
  '" . $status_document . "','" . $ra_asun . "'," . $id_borrador . " )
  RETURNING id_radicado_metadata ";


        $response = $this->db->conn->execute($query);
        return $response;
    }

    /**
     * @param $radi_nume_radi
     * @param $anex_codigo
     * @param string $radi_path_original
     * @param string $radi_path_validado
     * @param $clasification
     * @param $trd
     * @param $expediente
     * @param $expediente_titulo
     * @param $transacciones
     * @param string $hash_doc_original
     * @param string $hash_metadata
     * @param $codigoverificacion
     * @param $powered_by
     * @param string $filetype
     * @param string $filetype_ext
     * @param string $mimetype
     * @param string $type_version
     * @param string $size
     * @param string $pages
     * @param string $radi_path_fuente
     * @param string $pngbarra
     * @param $status_document
     * @param $ra_asun
     * @param $id_borrador
     * @param $id_radicado_metadata
     * @return mixed
     * actualiza los datos de la tabla document_metadata
     */
    function updateDocumentMetadata(
        $radi_nume_radi,
        $anex_codigo,
        $clasification,
        $trd,
        $expediente,
        $expediente_titulo,
        $transacciones,
        $codigoverificacion,
        $powered_by,
        $filetype = 'null',
        $filetype_ext = 'null',
        $mimetype = 'null',
        $status_document,
        $ra_asun,
        $id_borrador,
        $id_radicado_metadata
    )
    {
        $query = "update document_metadata set 
timestamp_m =current_timestamp, 
radi_nume_radi=$radi_nume_radi, 
anex_codigo = $anex_codigo,
clasification=$clasification,
 trd='" . $trd . "', 
 expediente='" . $expediente . "',
expediente_titulo='" . $expediente_titulo . "',
   transacciones='" . $transacciones . "', 
     codigoverificacion= '" . $codigoverificacion . "',
      powered_by= '" . $powered_by . "', 
      filetype= '" . $filetype . "',
  filetype_ext= '" . $filetype_ext . "',
   mimetype= '" . $mimetype . "',
     status_document= '" . $status_document . "',
      ra_asun= '" . $ra_asun . "',
       id_borrador= " . $id_borrador . " 
     where id_radicado_metadata=$id_radicado_metadata";

        $response = $this->db->conn->execute($query);

        return $response;
    }

    function insertdocument_authorized_by($id_radicado_metadata, $id_usuario, $username,
                                          $autor, $usermail, $country, $city, $organization, $department
        , $nit_organization, $website, $so, $webbrowser, $ipaddress, $hash_pin, $url_app, $navigator_appversion,
                                          $geolocation)
    {


        $query = "INSERT INTO document_authorized_by (timestamp_f,id_radicado_metadata, usuario_id, username, autor, usermail,
 country, city, organization, department, nit_organization, website, so, webbrowser, ipaddress,
  hash_pin,url_app, navigator_appversion,geolocation) 
  VALUES (current_timestamp,$id_radicado_metadata,
   $id_usuario,
   '" . $username . "',
   '" . $autor . "', 
   '" . $usermail . "' , 
  '" . $country . "' , 
  '" . $city . "' ,
   '" . $organization . "',
    '" . $department . "',
     '" . $nit_organization . "',
      '" . $website . "', 
  '" . $so . "', '" . $webbrowser . "', '" . $ipaddress . "', '" . $hash_pin . "', '" . $url_app . "',
   '" . $navigator_appversion . "', null)  
  RETURNING id_authorized ";


        $response = $this->db->conn->execute($query);
        return $response;
    }

    function getMetaDataHash($radicado, $anexCodigo)
    {
        $query = " Select r.radi_nume_radi || '' ||  r.radi_fech_radi || '' ||  a.anex_codigo || '' ||  dm.hash_doc_original || '' ||  dm.timestamp_m || '' ||  dm.size as texto
From radicado r 
Inner Join anexos a On r.radi_nume_radi=a.radi_nume_salida
Inner Join document_metadata dm On dm.radi_nume_radi=r.radi_nume_radi
Where r.radi_nume_radi=" . $radicado . " and a.anex_codigo='" . $anexCodigo . "' ";

        return $this->db->conn->selectLimit($query, 1)->getArray();
    }


    function getSgdDrecciones($radicado)
    {
        $query = " Select * From sgd_dir_drecciones where radi_nume_radi=$radicado";
        return $this->db->conn->selectLimit($query, 1)->getArray();
    }

    function getHistEventByRad($radicado)
    {
        $query = "select * from hist_eventos where radi_nume_radi=$radicado order by id";
        return $this->db->conn->selectLimit($query, 100000)->getArray();

    }

    function tiposradicadovalidar()
    {
        $query = " SELECT sgd_trad_codigo, sgd_trad_descr FROM sgd_trad_tiporad
WHERE (sgd_trad_externo='0' OR (sgd_trad_externo='0' AND sgd_trad_descr != 'Entrada'))
ORDER BY sgd_trad_orden ASC";
        return $this->db->conn->selectLimit($query, 100000)->getArray();
    }
    
    function tiposradicadovalidarcustom()
    {
    $query = "SELECT sgd_trad_codigo, sgd_trad_descr FROM sgd_trad_tiporad Where sgd_trad_codigo != 1";
        return $this->db->conn->selectLimit($query, 100000)->getArray();
    }

    //metodo que verifica si un radicado fue escaneado
    function radiescaneadouno($radicado)
    {
        $query = " SELECT COUNT(1)
FROM radicado r
INNER JOIN hist_eventos h ON r.radi_nume_radi=h.radi_nume_radi
WHERE h.sgd_ttr_codigo IN (22,42)
AND (lower(r.radi_path) LIKE '%.pdf' OR lower(r.radi_path) LIKE '%.tif')
AND (r.radi_nume_radi=$radicado)
";
        return $this->db->conn->selectLimit($query, 1)->getArray();
    }

    //metodo que verifica si un radicado fue escaneado
    function radiescaneadodos($radicado)
    {
        $query = "SELECT COUNT(1)
FROM radicado r
INNER JOIN entrega_fisicos f ON r.radi_nume_radi=cast(substr(f.anex_codigo,1,14) as numeric)
INNER JOIN anexos a ON f.anex_codigo=a.anex_codigo
Where r.radi_nume_radi='$radicado' ";
        return $this->db->conn->selectLimit($query, 1)->getArray();
    }

    //busca el anexo por ID
    function buscarAnexoById($id)
    {
        $query = "select * from ANEXOS where ID=$id";
        return $this->db->conn->selectLimit($query, 1)->getArray();
    }

    //busca el anexo por ID
    function getAnexosByRadicadoPadre($radicado)
    {
        $query = "select * from ANEXOS where anex_radi_nume=$radicado";
        return $this->db->conn->selectLimit($query, 10000)->getArray();
    }

    /**
     * @param $radicado
     * @return mixed
     * estos datos son usados para los metadatos al momento de validar
     */

    function getTrdAndExpedMetadata($radicado)
    {
        $query = "SELECT 
  e.radi_nume_radi     	AS RADICADO,
  sexp.sgd_exp_numero   AS EXPEDIENTE_NUMERO,
  sexp.sgd_sexp_parexp1 AS EXPEDIENTE_TITULO,
  sexp.sgd_sexp_ano ||'.'|| sexp.depe_codi ||'.'|| LPAD(cast(sexp.sgd_srd_codigo as text),3,'0') ||'.'|| LPAD(cast(sexp.sgd_sbrd_codigo  as text),3,'0') ||'.'|| r.tdoc_codi AS TRD,
  r.ra_asun            	AS ASUNTO,
  r.sgd_rad_codigoverificacion	AS RAD_COD_VERI
FROM
  sgd_sexp_secexpedientes sexp
INNER JOIN sgd_exp_expediente e ON sexp.sgd_exp_numero=e.SGD_EXP_NUMERO
INNER JOIN radicado r ON e.radi_nume_radi=r.radi_nume_radi
INNER JOIN usuario u ON sexp.usua_doc=u.usua_doc
WHERE
sgd_exp_estado !=2
AND r.radi_nume_radi=$radicado
ORDER BY sexp.sgd_exp_numero desc
";
        return $this->db->conn->selectLimit($query, 10000)->getArray();
    }

    /**
     * @param $otraInfoMetadata
     * transforma los campos sgd_exp_titulo, sgd_exp_numero, y el alias TRD, de los
     *  datos que retorna la funcion de arriba "getTrdAndExpedMetadata"
     */

    function transformToJsonStringTrdAndExpedMetadata($otraInfoMetadata)
    {

        $trd = array();
        $expedientes = array();
        $expediente_titulo = array();
        if (count($otraInfoMetadata) > 0) {
            foreach ($otraInfoMetadata as $row) {

                $trd[] = $row['TRD'];
                $expedientes[] = $row['EXPEDIENTE_NUMERO'];
                $expediente_titulo[] = $row['EXPEDIENTE_TITULO'];
            }
        }

        if (count($trd) > 0) {
            $trd = json_encode($trd);
        }

        if (count($expedientes) > 0) {
            $expedientes = json_encode($expedientes);
        }

        if (count($expediente_titulo) > 0) {
            $expediente_titulo = json_encode($expediente_titulo);
        }

        return array('trd' => $trd, 'expedientes' => $expedientes, 'expediente_titulo' => $expediente_titulo);

    }


    /**
     * @param $usuario_id
     * @return mixed
     * retorna los datos de las ciudades del usuario, usado al momento de validar, para los metadatos
     */
    function getCiudadUsuario($usuario_id)
    {
        $query = "SELECT u.id, u.usua_nomb, u.usua_login, u.usua_doc, d.id_cont, cont.nombre_cont, d.id_pais, pais.nombre_pais, d.dpto_codi, dep.dpto_nomb, d.muni_codi, mun.muni_nomb
FROM usuario u
INNER JOIN dependencia d ON u.depe_codi=d.depe_codi
INNER JOIN sgd_def_continentes cont ON u.id_cont=cont.id_cont
INNER JOIN sgd_def_paises pais ON u.id_pais=pais.id_pais AND pais.id_cont=cont.id_cont
INNER JOIN departamento dep ON dep.dpto_codi=d.dpto_codi AND dep.id_pais=pais.id_pais AND dep.id_cont=cont.id_cont
INNER JOIN municipio mun ON mun.muni_codi=d.muni_codi AND mun.dpto_codi=dep.dpto_codi AND mun.id_pais=pais.id_pais AND mun.id_cont=cont.id_cont
WHERE
u.usua_esta = '1'
And u.id=$usuario_id
ORDER BY 1";

        return $this->db->conn->selectLimit($query, 1)->getArray();
    }

    function pdfVersion($filename)
    {
        $filecontent = file_get_contents($filename);
        /**
         * el $filecontent devuelve un string, y en el busco la posicion de el string: "%PDF-",
         * a eso le sumo 5 caracteres mas, y alli esta la version del pdf
         */
        $pos = strpos($filecontent, '%PDF-');
        $version = substr($filecontent, $pos + 5, 3);
        return $version;
    }


    /**
     * @param $radicado
     * @return mixed
     * Verifica si un radiacdo esta en cualquiera de los 4 estatus de anulacion
     * y retorna el estado actual
     */
    function checkIfRadicadoAnulado($radicado)
    {
        $query = "Select a.sgd_anu_id as id,
	a.depe_codi || '-' || d.depe_nomb As Dependencia
	, r.radi_nume_radi As Radicado
	, ea.sgd_eanu_desc As Estado
	, ea.sgd_eanu_codi As EstadoN
	, r.radi_fech_radi AS Fecha_Radicacion
	, a.sgd_anu_sol_fech AS Fecha_Solicitud, a.sgd_anu_fech As Fecha_Anulacion
	, r.ra_asun As Asunto
	, a.sgd_anu_desc As Justificacion_Anulacion
	, u.usua_nomb As Usuario_Solicita_Anulacion
	, u2.usua_nomb As Usuario_Acta_Anulacion
	, a.usua_anu_acta As Acta
From sgd_anu_anulados a 
	Inner Join radicado r On (r.radi_nume_radi=a.radi_nume_radi)
	Inner Join sgd_eanu_estanulacion ea On (a.sgd_eanu_codi=ea.sgd_eanu_codi)
	Inner Join dependencia d On (a.depe_codi=d.depe_codi)
	Left Join usuario u On (a.usua_doc=u.usua_doc)
	Left Join usuario u2 On (a.usua_doc_anu=u2.usua_doc)
Where 
	a.sgd_eanu_codi In (1,2,3)
And a.usua_anu_acta Is Not null
And r.radi_nume_radi = " . $radicado . "
Order By r.radi_fech_radi desc,
    a.usua_anu_acta desc,
	a.depe_codi Asc,
	a.sgd_anu_id desc,
	a.sgd_anu_sol_fech desc,
	ea.sgd_eanu_desc Asc, 
	r.radi_nume_radi Desc, 
	u.usua_nomb Asc
";

        return $this->db->conn->selectLimit($query, 100000)->getArray();
    }

    function updateCodigoAprobado($codigo,$fecha_exp,$id_usuario){


        $query = 'update usuario set usua_codigo_apro = '.$codigo.', fecha_exp_cod = '.$fecha_exp.' where id = '.$id_usuario;

        $response = $this->db->conn->execute($query);

        return $response;
    }

    function devolverEstValidaIcon($id_radicado){
        $query = "update  RADICADO set radi_nume_iden = '1'  where radi_nume_radi = " . $id_radicado . " and radi_nume_iden = '2' ";

        $response = $this->db->conn->execute($query);

        return $response;
    }

    function insertarHistoricoComp($id_radicado ,$resultado ,$ip_address,$formato,$hashdoc){
        $query = "Insert Into document_validity_history (document, date_verification, result, user_type, user_id, ip_address, format_doc, hash_doc ) VALUES (".$id_radicado.", current_timestamp , '".$resultado."', 'INTERNO_GD', ".$_SESSION['usuario_id']." , '".$ip_address."', '".$formato."', '".$hashdoc."') ";
        $response = $this->db->conn->execute($query);

        $query2 = "select max(id) as idhistorico from document_validity_history where document = $id_radicado ";
        $response2 = $this->db->conn->selectLimit($query2, 100000)->getArray();
        $responseReturnal = array();
        $responseReturnal['response'] = $response2[0];



        return $responseReturnal;
    }

}

