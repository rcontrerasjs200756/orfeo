<?php

namespace App\Helpers;

use Carbon\Carbon;

class AlarmaExpediente
{
    protected $query_factory;
    protected $pdo;
    protected $user;

    public function __construct()
    {
        session_start();
        require '../../vendor/autoload.php';
        $this->user = $_SESSION;

        include_once "../../config.php";
        include_once "../../include/db/ConnectionHandlerNew.php";

        $this->db = new \ConnectionHandler("../..");
        $this->db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
    }

    public function getReportData()
    {
        try {
            $query = "SELECT
                e.sgd_exp_estado AS ALERTA,
                u.usua_nomb AS INCLUIDO_POR,
                e.radi_nume_radi AS RADICADO,
                r.radi_fech_radi AS FECHA_RADICADO,
                r.ra_asun AS ASUNTO,
                e.sgd_exp_numero AS NUMERO_EXPEDIENTE,
                se.SGD_SEXP_PAREXP1 AS TITULO,
                se.SGD_SEXP_PAREXP2 AS DESCRIPCION,
                e.sgd_exp_fech AS FECHA_ALERTA,
                a.USUARIO_CREA AS USUARIO_CREA,
                a.FECHA_CREACION AS FECHA_CREACION,
                a.USUARIO_ACTUALIZA AS USUARIO_ACTUALIZA,
                a.FECHA_ACTUALIZACION AS FECHA_ACTUALIZACION
                FROM sgd_exp_expediente e
                LEFT JOIN sgd_archivo_ubica a ON e.sgd_exp_numero=a.SGD_EXP_NUMERO AND e.RADI_NUME_RADI=a.RADI_NUME_RADI
                INNER JOIN SGD_SEXP_SECEXPEDIENTES se ON e.sgd_exp_numero=se.sgd_exp_numero
                INNER JOIN radicado r ON e.radi_nume_radi=r.radi_nume_radi
                INNER JOIN usuario u ON e.usua_doc=u.usua_doc
                ORDER BY e.sgd_exp_fech DESC";

            return $this->db->conn->selectLimit($query, 100)->getArray();

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function getRecords($q, $included, $excluded)
    {

        try{
            $query = "SELECT
                e.sgd_exp_estado AS ALERTA,
                u.usua_nomb AS INCLUIDO_POR,
                e.radi_nume_radi AS RADICADO,
                r.radi_fech_radi AS FECHA_RADICADO,
                r.ra_asun AS ASUNTO,
                e.sgd_exp_numero AS NUMERO_EXPEDIENTE,
                se.SGD_SEXP_PAREXP1 AS TITULO,
                se.SGD_SEXP_PAREXP2 AS DESCRIPCION,
                e.sgd_exp_fech AS FECHA_ALERTA,
                a.USUARIO_CREA AS USUARIO_CREA,
                a.FECHA_CREACION AS FECHA_CREACION,
                a.USUARIO_ACTUALIZA AS USUARIO_ACTUALIZA,
                a.FECHA_ACTUALIZACION AS FECHA_ACTUALIZACION
                FROM sgd_exp_expediente e
                LEFT JOIN sgd_archivo_ubica a ON e.sgd_exp_numero=a.SGD_EXP_NUMERO AND e.RADI_NUME_RADI=a.RADI_NUME_RADI
                INNER JOIN SGD_SEXP_SECEXPEDIENTES se ON e.sgd_exp_numero=se.sgd_exp_numero
                INNER JOIN radicado r ON e.radi_nume_radi=r.radi_nume_radi
                INNER JOIN usuario u ON e.usua_doc=u.usua_doc AND";

            if ($included) {
                $query .= " (e.sgd_exp_estado IN(0,1) OR";
            } else {
                $query .= " (e.sgd_exp_estado NOT IN(0,1) AND";
            }
            if ($excluded) {
                $query .= " e.sgd_exp_estado IN(2))";
            } else {
                $query .= " e.sgd_exp_estado NOT IN(2))";
            }
            if (strlen($q) > 0) {
                $query .= " AND (CAST(r.radi_nume_radi as VARCHAR(14)) LIKE '%" . $q . "%' OR r.ra_asun LIKE '%" . $q . "%' OR e.sgd_exp_numero LIKE '%" . $q . "%' OR se.sgd_sexp_parexp1 LIKE '%" . $q . "%' OR se.sgd_sexp_parexp2 LIKE '%" . $q . "%')";
            }

            $query .= " ORDER BY e.sgd_exp_fech DESC";

            return $this->db->conn->selectLimit($query, 100)->getArray();

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public static function getRecordTime($created)
    {
        $date = '';
        $today = Carbon::now();
        $created = Carbon::parse($created);

        if ($created->diffInDays($today) != 0) {
            $date = $created->diffInDays($today)." días";
        }
        if ($created->diffInDays($today) == 0) {
            if ($created->diffInHours($today) != 0) {
                $date = $created->diffInHours($today)." hrs";
            }
            if ($created->diffInHours($today) == 0) {
                if ($created->diffInMinutes($today) != 0) {
                    $date = $created->diffInMinutes($today)." min";
                }
                if ($created->diffInMinutes($today) == 0) {
                    $date = $created->diffInSeconds($today)." seg";
                }
            }
        }
        return $date;
    }

    public static function getRadicatedDate($date)
    {
        $day = Carbon::parse($date)->format('d');
        $month = Carbon::parse($date)->format('n');
        $year = Carbon::parse($date)->format('Y');

        switch ($month) {
            case 1:
                return 'Enero '.$day.', '.$year;
                break;
            case 2:
                return 'Febrero '.$day.', '.$year;
                break;
            case 3:
                return 'Marzo '.$day.', '.$year;
                break;
            case 4:
                return 'Abril '.$day.', '.$year;
                break;
            case 5:
                return 'Mayo '.$day.', '.$year;
                break;
            case 6:
                return 'Junio '.$day.', '.$year;
                break;
            case 7:
                return 'Julio '.$day.', '.$year;
                break;
            case 8:
                return 'Agosto '.$day.', '.$year;
                break;
            case 9:
                return 'Septiembre '.$day.', '.$year;
                break;
            case 10:
                return 'Octubre '.$day.', '.$year;
                break;
            case 11:
                return 'Noviembre '.$day.', '.$year;
                break;
            case 12:
                return 'Diciembre '.$day.', '.$year;
                break;
            default:
                return '-';
        }
    }
}