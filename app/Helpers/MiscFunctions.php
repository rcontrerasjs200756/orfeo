<?php

namespace App\Helpers;

use Aura\SqlQuery\QueryFactory;
use Carbon\Carbon;

class MiscFunctions
{

    public static function clear_view_cache(){
        $files = glob('../cache/*');
        foreach ($files as $file) {
            if (is_file($file)) {
                unlink($file);
            }
        }
        return TRUE;
    }

    public static function get_anex_type($type){
        include_once "../../config.php";
        include_once "../../include/db/ConnectionHandlerNew.php";
        $db = new \ConnectionHandler("../..");
        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);

        $query= "SELECT anex_tipo_ext
                FROM anexos_tipo
                WHERE anex_tipo_codi=".$type."";

        $select = $db->conn->execute($query);
        $value  = $select->fetchRow();

        //Returning the result
        return $value["ANEX_TIPO_EXT"];
    }

    public static function get_user_data($usua_login= NULL){
        include_once ("../../config.php");
        include_once ("../../include/db/ConnectionHandlerNew.php");

        if(!isset($usua_login)){
            session_start();
            $usua_login= $_SESSION['krd'];
        }

        $db = new \ConnectionHandler("../..");
        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);

        $query= "SELECT e.usua_login AS usua_login,
            e.usua_nomb AS usua_nombre,
            e.depe_codi AS dependencia,
            e.usua_codi AS codigo_usuario
            FROM usuario e
            WHERE e.usua_login='".$usua_login."'";

        $select = $db->conn->execute($query);

        $data= $select->fetchRow();

        return $data;
    }

    public static function getDelivered(){
        try {
            include_once "../../config.php";
            include_once "../../include/db/ConnectionHandlerNew.php";
            $db = new \ConnectionHandler("../..");
            $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);

            $query= "SELECT '1' AS total,
                min(e.anex_codigo)       AS anex_codigo,
                min(e.process_type)      AS proceso,
                max(e.fecha)             AS fecha_entrega,
                max(e.usua_login_recibe) AS usuario,
                a.anex_radi_nume AS radicado,
                a.anex_desc AS descripcion
                FROM entrega_fisicos e
                LEFT JOIN anexos a
                ON e.anex_codigo=a.anex_codigo
                WHERE e.delivered=0
                GROUP BY e.id, a.anex_radi_nume, a.anex_desc";

            $delivered= $db->conn->getAll($query);

            return $delivered;
        }catch(\Exception $e){
            return "ERROR: ".$e->getMessage();
        }
    }

    public static function get_radicated_notif_color($code){
        $color= "";
        if($code == 8){
            $color= "yellow";
        }
        if($code == 9){
            $color= "green-jungle";
        }
        if($code == 14){
            $color= "red-thunderbird";
        }
        if($code == 15){
            $color= "grey";
        }
        if($code == 12){
            $color= "red-thunderbird";
        }
        if($code == 2){
            $color= "yellow-gold";
        }
        if($code == 13){
            $color= "grey";
        }
        if($code == 16){
            $color= "blue";
        }
        return $color;
    }

    public static function get_radicated_notif_icon($code){
        $icon= "";
        if($code == 8){
            $icon= "info";
        }
        if($code == 9){
            $icon= "mail-reply-all";
        }
        if($code == 14){
            $icon= "calendar-plus-o";
        }
        if($code == 15){
            $icon= "calendar-check-o";
        }
        if($code == 12){
            $icon= "mail-reply";
        }
        if($code == 2){
            $icon= "plus";
        }
        if($code == 13){
            $icon= "archive";
        }
        if($code == 16){
            $icon= "mail-reply";
        }

        return $icon;
    }

    public static function get_radicated_notif_date($created){
        $today      = Carbon::now();
        $created    = Carbon::parse($created);
        $date       = '';

        if($created->diffInDays($today) != 0) {
            $date= $created->diffInDays($today)." días";
        }
        if($created->diffInDays($today) == 0) {
            if($created->diffInHours($today) != 0){
                $date= $created->diffInHours($today)." hrs";
            }
            if($created->diffInHours($today) == 0){
                if($created->diffInMinutes($today) != 0){
                    $date= $created->diffInMinutes($today)." min";
                }
                if($created->diffInMinutes($today) == 0){
                    $date= $created->diffInSeconds($today)." seg";
                }
            }

        }

        return $date;
    }

    public static function get_radicated_status($code){
        $label= "";
        if($code == 8){
            $label= "Informado";
        }
        if($code == 9){
            $label= "Reasignado";
        }
        if($code == 14){
            $label= "Agendado";
        }
        if($code == 15){
            $label= "Agenda Cerrada";
        }
        if($code == 12){
            $label= "Devuelto";
        }
        if($code == 2){
            $label= "Radicado";
        }
        if($code == 13){
            $label= "Finalizado";
        }
        if($code == 16){
            $label= "Visto Bueno";
        }

        return $label;
    }

    public static function check_scanned($number){
        include_once "../../config.php";
        include_once "../../include/db/ConnectionHandlerNew.php";
        $db = new \ConnectionHandler("../..");
        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        $r= 0;
        $query= "SELECT COUNT(*) as scanned
                  FROM radicado r
                  INNER JOIN hist_eventos h
                  ON r.radi_nume_radi=h.radi_nume_radi
                  WHERE h.sgd_ttr_codigo IN (22, 23, 42)
                  AND (LOWER (r.radi_path) LIKE '%tif' OR LOWER(r.radi_path) LIKE '%pdf')
                  AND r.radi_nume_radi= ".$number."";
        $result= $db->conn->execute($query);
        if (!empty($result)) {
            $r = $result->fetchRow();
        }

        return $r['SCANNED'];
    }

    public static function getPercentage($count, $total)
    {
        $percentage = ($count * 100)/($total);
        return $percentage;
    }
}