<?php

use App\Helpers\PDOconfig;
use Aura\SqlQuery\QueryFactory;
use Philo\Blade\Blade;

require '../../vendor/autoload.php';

$json = array();

try {
    require_once '../../config.php';

    $reportespago = new \App\Helpers\ReportesPagos();
    $perfil = "";
    //aqui busco el perfil que tiene el usuario que esta logueado
    $soyapoyo = $reportespago->soySupervisorApoyo($_SESSION['usua_doc']);
    $soyprincipal = $reportespago->soySupervisorPrincipal($_SESSION['usua_doc']);

    if ($soyapoyo[0]['COUNT'] > 0 || $_SESSION['usua_supervisor_apoyo']==1) {
        $perfil = "apoyo";
    }
    if ($soyprincipal[0]['COUNT'] > 0 || $_SESSION['usua_supervisor']==1) {
        $perfil = "principal";
    }

    if ($_SESSION["usua_admin_sistema"] == 1) {
        $perfil = "admin";
    }

    $usuarioeenviar = array();
    if ($perfil != "") {
        $supervisores = $reportespago->usuaLikeSupervi($_GET['search'], $perfil);


        $usuarioeenviar = array();
        $cont = 0;

        foreach ($supervisores as $supervisor) {
            $usuarioeenviar[$cont]['id'] = $supervisor['USUA_DOC'];
            $usuarioeenviar[$cont]['text'] = $supervisor['USUA_NOMB']." (".$supervisor['DEPE_NOMB'].")";
            $cont++;
        }

    }
    echo json_encode($usuarioeenviar);
} catch (\Exception $e) {
    return $e->getMessage();
}


?>

