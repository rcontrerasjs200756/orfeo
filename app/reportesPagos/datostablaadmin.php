<?php

use App\Helpers\PDOconfig;
use Aura\SqlQuery\QueryFactory;
use Philo\Blade\Blade;

require '../../vendor/autoload.php';

$array = array();

try {

    /* ini_set('display_errors', 1);
     ini_set('display_startup_errors', 1);
     error_reporting(E_ALL);*/

    require_once '../../config.php';

    $ruta_raiz = "../../";
    include_once("$ruta_raiz/include/db/ConnectionHandler.php");
    $db = new ConnectionHandler("$ruta_raiz");
    $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);

    $reporte_pagos = new \App\Helpers\ReportesPagos($db);
    $helperborra = new \App\Helpers\Borradores($db);

        $datajson = array();
        $data = $_GET['data'];

        // Pagination Result
        $array = array();
        $start = 0;
        $limit = false;

        $draw = $_GET['draw'];

        if (!empty($draw)) {
            $start = $_GET['start'];
            $limit = $_GET['length'];
            if ($limit == '-1') {
                $limit = false;
            }
        }


        $search = $_GET['search'];
        $buscar = $search['value'];


        $total=$contratistas = $reporte_pagos->tblprincipalAdmin(false,false,$buscar);

    $contratistas = $reporte_pagos->tblprincipalAdmin($start,$limit,$buscar);
         $cont=0;
        if($data['swichetodos']=="true" || $_SESSION['usua_admin_sistema']){
        //si  es admin


            $array['contratistas'] = array();

            if (count($contratistas) > 0) {
                foreach ($contratistas as $contratista) {

                    $identificacion=$contratista['IDENTIFICACION'];
                    $tienesuperv=$reporte_pagos->getSupervByContrat($identificacion);


                    $nombre=$contratista['?COLUMN?'];
                    $dependencia=$contratista['DEPE_NOMB'];

                    $array['contratistas'][$cont] = array();
                    $array['contratistas'][$cont]['identificacion'] = $identificacion;
                    $array['contratistas'][$cont]['tipo'] = $contratista['TIPO'];
                    $cont++;

                    $CONTRATISTA_json = array();
                    $con = 0;

                    $string="'".$identificacion."'";
                    $stringtipo="'".$contratista['TIPO']."'";

                    $CONTRATISTA_json[$con]="";
                    if($_SESSION['usua_admin_sistema']==1 || !isset($tienesuperv[0])) {
                        $CONTRATISTA_json[$con] = '<input type="checkbox" id="identif_' . $identificacion . '" 
                onclick="addSupervisores(' . $string . ',this,' . $stringtipo . ')">';

                    }
                    $con++;

                    $CONTRATISTA_json[$con] = $nombre;
                    $con++;

                    $CONTRATISTA_json[$con] = $identificacion;
                    $con++;

                    $CONTRATISTA_json[$con]="";

                    if(isset($tienesuperv[0])){

                        $principal=$reporte_pagos->getUsuarioByDoc($tienesuperv[0]['SUPERVISOR_DOC']);
                        $apoyo=$reporte_pagos->getUsuarioByDoc($tienesuperv[0]['SUPERVISOR_APOYO_DOC']);
                        $CONTRATISTA_json[$con] = '<a href="#">'.$principal[0]['USUA_NOMB'].',</a><br>
<a href="#" >Apoyo: '.$apoyo[0]['USUA_NOMB'].'</a>';
                    }else{
                        $CONTRATISTA_json[$con] = "<p style='color:red;'>Sin Asignar</p>";
                    }

                    $con++;


                    $CONTRATISTA_json[$con] = $dependencia;
                    $con++;
                    $datajson[] = $CONTRATISTA_json;

                }
            }

        }else{
            //si no es admin, es supervisor el perfil
            $usua_doc = $_SESSION["usua_doc"];
            $array['contratistas'] = array();
            if (count($contratistas) > 0) {

                foreach ($contratistas as $contratista) {

                    $identificacion=$contratista['IDENTIFICACION']; //identificacio del cotratista
                    $tienesuperv=$reporte_pagos->getContraBySuperv($usua_doc,$identificacion);
                    $nombre="";

                    if(count($tienesuperv)>0){
                        //solo va a entrar aqui cuando este usuario sea supervisor principal o de apoyo, para este contratista
                        $nombre=$contratista['?COLUMN?'];
                        $dependencia=$contratista['DEPE_NOMB'];

                    $array['contratistas'][$cont] = array();
                    $array['contratistas'][$cont]['identificacion'] = $identificacion;
                    $array['contratistas'][$cont]['tipo'] = $contratista['TIPO'];
                    $cont++;

                    $CONTRATISTA_json = array();
                    $con = 0;

                    $string="'".$identificacion."'";
                    $stringtipo="'".$contratista['TIPO']."'";

                    $CONTRATISTA_json[$con] = ''; //aqui no tiene swiche

                    $con++;

                    $CONTRATISTA_json[$con] = $nombre;
                    $con++;

                    $CONTRATISTA_json[$con] = $identificacion;
                    $con++;


                    $CONTRATISTA_json[$con]="";

                    if(isset($tienesuperv[0])){

                        $principal=$reporte_pagos->getUsuarioByDoc($tienesuperv[0]['SUPERVISOR_DOC']);
                        $apoyo=$reporte_pagos->getUsuarioByDoc($tienesuperv[0]['SUPERVISOR_APOYO_DOC']);
                        $CONTRATISTA_json[$con] = '<a href="#" >'.$principal[0]['USUA_NOMB'].',</a><br>
<a href="#">Apoyo: '.$apoyo[0]['USUA_NOMB'].'</a>';
                    }else{
                        $CONTRATISTA_json[$con] = "<p style='color:red;'>Sin Asignar</p>";
                    }

                    $con++;


                    $CONTRATISTA_json[$con] = $dependencia;
                    $con++;
                    $datajson[] = $CONTRATISTA_json;
                    }
                }
            }


        }
        $cont = 0;





        $array['data'] = $datajson;
        $array['draw'] = $draw;//esto debe venir por post
        $array['recordsTotal'] = count($total);
        $array['recordsFiltered'] = count($total);

} catch (Exception $e) {
    echo "Error: " . $e->getMessage();
    $response = "ERROR";
    $array['error'] = true;
}
echo json_encode($array);


?>

