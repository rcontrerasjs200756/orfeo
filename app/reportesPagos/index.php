<?php

use Philo\Blade\Blade;

require '../../vendor/autoload.php';
require_once '../../config.php';

$views = $ABSOL_PATH.'app/views';
$cache = $ABSOL_PATH.'app/cache';

//Clearing the views cache
\App\Helpers\MiscFunctions::clear_view_cache();

$blade          = new Blade($views, $cache);
$reporte_pagos  = new \App\Helpers\ReportesPagos();

$returned_payments  =   $reporte_pagos->get_returned_payments();
$month_payments     =   $reporte_pagos->get_month_payments();
$returned_payments=array();
$month_payments=array();
/*
echo '<pre>';
var_dump($month_payments);
echo '</pre>';
exit;
*/

$swichedefault="";
$financiero=$_SESSION['usua_financiero'];
$supervisor= $reporte_pagos->soySupervisor($_SESSION['usua_doc'] );
$contratista= $reporte_pagos->soyContratista($_SESSION['usua_doc'] );
$contratista=$contratista[0]['COUNT'];
$supervisor=$supervisor[0]['COUNT'];

if($financiero==1){
    $swichedefault="Financiera";
}elseif($supervisor>0 || $_SESSION['usua_supervisor_apoyo']==1 || $_SESSION['usua_supervisor']==1){
    $supervisor=1;
    $swichedefault="Supervisor";
}elseif($contratista>0){
    $swichedefault="Contratista";
}elseif($_SESSION["usua_admin_sistema"]==1){
    $swichedefault="Administracion";
}

$updateprevios=$reporte_pagos->updateprevios();

echo $blade->view()->make('reportesPagos.index', compact(
    'returned_payments','swichedefault','supervisor','contratista',
    'month_payments',
    'include_path'
))->render();

?>