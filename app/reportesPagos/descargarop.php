<?php

use App\Helpers\PDOconfig;
use Aura\SqlQuery\QueryFactory;
use Philo\Blade\Blade;
require '../../vendor/autoload.php';

$json=array();

try {

    //var_dump($_GET);
    //var_dump($_POST);
    descargarop();

}catch(Exception $e){
    echo "Error: ".$e->getMessage();
    $response= "ERROR";
    $json['error']=true;
}
echo json_encode($json);


function descargarop(){
    try {
        //guarda los supervisores a los contratistas
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        require_once '../../config.php';

        $reporte_pagos = new \App\Helpers\ReportesPagos();
        $anexoid = $_GET['anexoid'];

        $tieneop = $reporte_pagos->getAnexOpById($anexoid);

        $festivos = new \App\Helpers\Festivos(date('Y'));

        $cont=0;
        if(count($tieneop)>0){

            foreach ($tieneop as $rowAnexo) {

                    $fechamastres = $festivos->diasHabiles(date("Y-m-d", strtotime($rowAnexo['ANEX_FECH_ANEX'])), 3);

                    if (date("Y-m-d") >= $fechamastres) {

                        $ruta = "../../bodega";
                        $ruta .= "/" . date("Y", strtotime($rowAnexo['ANEX_RADI_FECH']));
                        $ruta .= "/" . $rowAnexo['ANEX_DEPE_CREADOR'];
                        $ruta .= "/docs/" . $rowAnexo['ANEX_NOMB_ARCHIVO'];


                        //header('Content-Description: File Transfer');
                        header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
                        //header('Expires: 0');
                        header('Cache-Control: must-revalidate');
                        header('Content-Disposition: attachment; filename=' . $rowAnexo['ANEX_NOMB_ARCHIVO']);
                        header('Pragma: no-cache');
                        ///header('Pragma: public');
                        readfile($ruta);


                    }
                    break;

                $cont++;
            }

        }

    }catch(\Exception $e){
        return $e->getMessage();
    }
}

?>

