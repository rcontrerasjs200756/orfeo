<?php

use App\Helpers\PDOconfig;
use Aura\SqlQuery\QueryFactory;
use Philo\Blade\Blade;
require '../../vendor/autoload.php';

$json=array();

ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
error_reporting(E_ALL);
function convertBytes( $value ) {
    if ( is_numeric( $value ) ) {
        return $value;
    } else {
        $value_length = strlen($value);
        $qty = substr( $value, 0, $value_length - 1 );
        $unit = strtolower( substr( $value, $value_length - 1 ) );
        switch ( $unit ) {
            case 'k':
                $qty *= 1024;
                break;
            case 'm':
                $qty *= 1048576;
                break;
            case 'g':
                $qty *= 1073741824;
                break;
        }
        return $qty;
    }
}

try {

   /* ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);*/

    function formatSizeUnits($bytes)
    {
        /*if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }*/
        if ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2);
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes ;
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes;
        }
        else
        {
            $bytes = '0';
        }

        return $bytes;
    }


    require_once '../../config.php';

    $ruta_raiz = "../../";
    include_once("$ruta_raiz/include/db/ConnectionHandler.php");
    require_once("$ruta_raiz/include/tx/Tx.php");
    $db = new ConnectionHandler("$ruta_raiz");
    $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);

    $maxFileSizeStr=ini_get('upload_max_filesize');
    $root = $_SERVER['HTTP_ORIGIN'];
    $base = explode("/",$BASE_PATH);
    $base=$root."/".$base[count($base)-1];

    $reporte_pagos     = new \App\Helpers\ReportesPagos($db);
    $borra     = new \App\Helpers\Borradores($db);

        if (!empty($_FILES) and count($_FILES['files'])>0) {

            $ruta="../../bodega";
            $base.="/bodega";
            $rutaFinalArchivo="/bodega";  //esta va a ser la ruta que se va a enviar para eliminar
            if(!is_dir("../../bodega"))
            {
                $json['error']="Error";
            }
            else
            {
                $Tx = new Tx($db);
                require_once($ruta_raiz . "class_control/anexo.php");
                $numeroradicado=$_POST['numeroradicado'];
                $numeroOP=$_POST['numeroop'];
                //busco la informacion del radicado
                $radicadocompleto=$borra->getRadicadoByNume_radi($numeroradicado);

                if(isset($radicadocompleto[0]['RADI_NUME_RADI'])) {

                    $bien2=false;
                $contador=0;
                $maxFileSize = convertBytes(ini_get('upload_max_filesize'));
                $cantidadFiles=count($_FILES);


                $filejson = new stdClass;

                $conterror=0;
                for ($i=0; $i<count($_FILES['files']['name']); $i++) {

                    $fileArray=array();

                    if($_FILES['files']['size'][$i]>$maxFileSize){
                        $json['error']="El tamaño de algunos archivos excede el limite permitido: ".$maxFileSizeStr;
                        $json['conterror'][$conterror]=$_FILES['files']['name'][$i];
                        $conterror++;
                        continue;
                    }

                    $ext = explode(".", $_FILES['files']['name'][$i]);

                    $cont=0;
                    $ext = $ext[count($ext) - 1];
                    $ext=strtolower($ext);
                    $codusuario = $_SESSION["usuario_id"];
                    ///////////////

                    if (!is_dir($ruta . "/" . date("Y"))) {

                        mkdir("$ruta", 0777);
                    }
                    $ruta .= "/" . date("Y",strtotime($radicadocompleto[0]['RADI_FECH_RADI']));
                    $base.="/".date("Y",strtotime($radicadocompleto[0]['RADI_FECH_RADI']));
                    $rutaFinalArchivo.="/".date("Y",strtotime($radicadocompleto[0]['RADI_FECH_RADI']));
                    $dependencia = $radicadocompleto[0]['DEPE_CODI'];


                    if (!is_dir($ruta . "/" . $dependencia)) {

                        mkdir("$ruta", 0777);
                    }
                    $ruta .= "/" . $dependencia;
                    $base.="/".$dependencia;
                    $rutaFinalArchivo.="/".$dependencia;
                    if (!is_dir($ruta . "/docs")) {

                        $ruta .= "/docs";
                        mkdir("$ruta", 0777);
                    }
                    $ruta .= "/docs/";
                    $base.="/docs/";
                    $rutaFinalArchivo.="/docs/";

                    $anexo = new Anexo($db);

                    //obtengo el ultimo numero del anexo disponible
                    $anexnumero=$reporte_pagos->getanex_numero($numeroradicado);

                    $anexo->anex_nomb_archivo = trim($numeroradicado) .'_'. trim(str_pad($anexnumero[0]['NUME'],
                            5, "0", STR_PAD_LEFT)).".".$ext;
                    $name = $anexo->anex_nomb_archivo;

                    $userfile1_Temp =  $_FILES['files']['tmp_name'][$i];
                    $bien2 = move_uploaded_file($userfile1_Temp, $ruta . $anexo->anex_nomb_archivo);


                    $bytes=filesize($ruta.$name);
                    $kb=formatSizeUnits($bytes);
                    $kb=str_replace(",", "", $kb);
                    //si es una imagen le hago un thumbnail
                    if($ext=="jpg" || $ext=="jpeg" || $ext=="png" || $ext=="gif" ) {
                        //thumbnail
                        $file = $ruta . $name;
                        $width = 80;

// Ponemos el . antes del nombre del archivo porque estamos considerando que la ruta está a partir del archivo thumb.php
                        $file_info = getimagesize($file);
// Obtenemos la relación de aspecto
                        $ratio = $file_info[0] / $file_info[1];

// Calculamos las nuevas dimensiones
                        $newwidth = $width;
                        $newheight = round($newwidth / $ratio);

// Sacamos la extensión del archivo

                        $nombrethumnail="";
                        if ($ext == "jpeg") $ext = "jpg";

// Dependiendo de la extensión llamamos a distintas funciones
                        switch ($ext) {
                            case "jpg":
                                $img = imagecreatefromjpeg($file);
                                break;
                            case "png":
                                $img = imagecreatefrompng($file);
                            case "gif":
                                $img = imagecreatefromgif($file);
                                break;
                        }

// Creamos la miniatura
                        $thumb = imagecreatetruecolor($newwidth, $newheight);
// La redimensionamos
                        imagecopyresampled($thumb, $img, 0, 0, 0, 0, $newwidth, $newheight, $file_info[0], $file_info[1]);
// La mostramos como jpg

                        $rutathumnail = $ruta . "80x80".$name;

                        $nombrethumnail =  "80x80".$name;
                        imagejpeg($thumb, $rutathumnail, 80);
                    }

                    if ($bien2==true) {


                        $anexoAno = date('Y');
                        $anexo->anex_radi_nume = $numeroradicado;
                        $anexo->sgd_trad_codigo = NULL;
                        $anexo->anex_desc="Órden de Pago OP  ".$numeroOP;
                       /// $anexo->anex_numero = $anexnumero[0]['NUME']; esto es automatico
                       //
                        $anexo->anex_codigo = "";//es automatico

                        //BUSCO EL TIPO DE ANEXO
                        $tipo_anexo=$reporte_pagos->getExtencion($ext);
                        $anexo->anex_tipo = $tipo_anexo[0]['ANEX_TIPO_CODI'];

                        $anexo->anex_salida = 0;
                        $anexo->radi_nume_salida = null;
                        $anexo->anex_estado = '0';
                        $anexo->anex_solo_lect = "'S'";

                        $anexo->anex_radi_fech = $radicadocompleto[0]['RADI_FECH_RADI'];
                        $anexo->anex_creador = "'" . $_SESSION['usua_login'] . "'";
                        $anexo->sgd_rem_destino = 1;
                        $anexo->sgd_dir_tipo = 1;
                        $anexo->anex_tamano = $kb;
                        $anexo->anex_borrado = "'N'";
                        $anexo->anex_depe_creador = $dependencia;
                        $anexo->sgd_tpr_codigo = 0;
                        $anexo->anex_origen = 4;
                        $anexo->anex_ubic = "'OP'";
                        $anexo->usua_doc = $_SESSION['usua_doc'];


                        /////////*datos del historico*/////////////
                        $radicadoarray = Array();
                        $radicadoarray[0] = $numeroradicado;
                        $anexo->radicado_array_hist=$radicadoarray;
                        $anexo->depeOrigen_hist= $_SESSION['dependencia'];
                        $anexo->usCodOrigen_hist=$_SESSION['codusuario'];
                        $anexo->depeDestino_hist=$_SESSION['dependencia'];
                        $anexo->usCodDestino_hist= $_SESSION['codusuario'];
                        $anexo->observacion_hist="Anexo ".$anexnumero[0]['NUME']." creado. ". "Órden de Pago OP  ".$numeroOP;
                        $anexo->tipoTx_hit= 43;
                        $anexo->fecha_hist= NULL;

                        $anexoCodigo = $anexo->anexarFilaRadicado();

                        ///////////////////////////////////////////////////////////
                        if($anexoCodigo=="-1"){
                            $json['error'] = "Ha ocurrido un error al guarda el registro en la base de datos";
                        }else{
                            $json['success'] = $anexoCodigo;
                        }

                        $fileArray=array(
                            'url'  => $base.$name,
                            'thumbnailUrl' => $base.$nombrethumnail,
                            'name' => $name,
                            'type' => $_FILES['files']['type'][$i],
                            'size' => $_FILES['files']['size'][$i],
                            'delete_url'   => $rutaFinalArchivo.$name,
                            'delete_urlthumbnail'   => $rutaFinalArchivo.$nombrethumnail,
                            'delete_type' => 'DELETE' // method for destroy action
                        );

                        if($ext=="jpg" || $ext=="jpeg" || $ext=="png" || $ext=="gif"){
                            $fileArray['gallery']=true;
                        }

                        $filejson->files[] = $fileArray;



                    }

                }

                if($bien2==true && !isset($json['conterror'])){


                    $query = "UPDATE radicado SET radi_leido=1 
WHERE radi_depe_actu=".$_SESSION['dependencia']." AND radi_usua_actu=".$_SESSION["codusuario"]." 
AND radi_nume_radi  = ".$numeroradicado;
                    $response = $db->conn->execute($query);

                    $json=$filejson;

                }else{
                    $json['error']="Ha ocurrido un error al guardar el anexo";
                }
                }else{
                    $json['error'] = "El radicado no existe";
                }

            }

        }else{
            $json['error']="Debe ingresar un archivo";
        }


}catch(Exception $e){
    echo "Error: ".$e->getMessage();
    $response= "ERROR";
    $json['error']=true;
}
echo json_encode($json);



?>

