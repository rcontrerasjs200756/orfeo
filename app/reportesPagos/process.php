<?php

use App\Helpers\PDOconfig;
use Aura\SqlQuery\QueryFactory;
use Philo\Blade\Blade;

require '../../vendor/autoload.php';

try {
    session_start();

    if ($_POST['operation'] == "returned_payments") {
        $response = get_returned_payments($_POST['from'], $_POST['to']);
    }
    if ($_POST['operation'] == "month_payments") {
        $response = get_month_payments($_POST['from'], $_POST['to']);
    }
    if ($_POST['operation'] == "get_radicated_info") {
        $response = get_radicated_info($_POST['rad_num']);
    }

    if ($_POST['operation'] == "tablaprincipalOP") {

        $response = tablaprincipalOP();
    }

    if ($_POST['operation'] == "inforadicado") {

        $response = inforadicado();
    }

    if ($_POST['operation'] == "guardarSupervisores") {

        $response = guardarSupervisores();
    }

    if ($_POST['operation'] == "descargarop") {

        $response = descargarop();
    }

    if ($_POST['operation'] == "borrarFileOp") {

        $response = borrarFileOp();
    }

    if ($_POST['operation'] == "buscarAnexosOP") {

        $response = buscarAnexosOP();
    }


} catch (Exception $e) {
    echo "Error: " . $e->getMessage();
    $response = "ERROR";
}
echo $response;
exit;


function formatSizeUnits($bytes)
{
    /*if ($bytes >= 1073741824)
    {
        $bytes = number_format($bytes / 1073741824, 2) . ' GB';
    }
    elseif ($bytes >= 1048576)
    {
        $bytes = number_format($bytes / 1048576, 2) . ' MB';
    }*/
    if ($bytes >= 1024) {
        $bytes = number_format($bytes / 1024, 2);
    } elseif ($bytes > 1) {
        $bytes = $bytes;
    } elseif ($bytes == 1) {
        $bytes = $bytes;
    } else {
        $bytes = '0';
    }

    return $bytes;
}

function get_returned_payments($from_date = null, $to_date = null)
{
    try {
        require_once '../../config.php';
        $views = $ABSOL_PATH . 'app/views';
        $cache = $ABSOL_PATH . 'app/cache';

        //Clearing the views cache
        \App\Helpers\MiscFunctions::clear_view_cache();

        $blade = new Blade($views, $cache);

        $reporte_pagos = new \App\Helpers\ReportesPagos();
        $returned_payments = $reporte_pagos->get_returned_payments($from_date, $to_date);
        $session_id=trim(session_id());
        $session_name=session_name();

        return $blade->view()->make('reportesPagos.partials.returned_payments_table',
            compact('returned_payments','session_id','session_name')
        )->render();

    } catch (\Exception $e) {
        return $e->getMessage();
    }
}

function get_month_payments($from_date = null, $to_date = null)
{
    try {

        require_once '../../config.php';
        $views = $ABSOL_PATH . 'app/views';
        $cache = $ABSOL_PATH . 'app/cache';

        //Clearing the views cache
        \App\Helpers\MiscFunctions::clear_view_cache();

        $blade = new Blade($views, $cache);


        $reporte_pagos = new \App\Helpers\ReportesPagos();
        $month_payments = $reporte_pagos->get_month_payments($from_date, $to_date);
        $session_id=trim(session_id());
        $session_name=session_name();

        return $blade->view()->make('reportesPagos.partials.month_payments_table',
            compact('month_payments','session_id','session_name')
        )->render();

    } catch (\Exception $e) {
        return $e->getMessage();
    }
}

function get_radicated_info($rad_number)
{
    try {
        require_once '../../config.php';
        $views = $ABSOL_PATH . 'app/views';
        $cache = $ABSOL_PATH . 'app/cache';

        //Clearing the views cache
        \App\Helpers\MiscFunctions::clear_view_cache();

        $blade = new Blade($views, $cache);

        $reporte_pagos = new \App\Helpers\ReportesPagos();
        $rad_info = $reporte_pagos->get_rad_info($rad_number);

        return $blade->view()->make('reportesPagos.partials.radicated_info',
            compact('rad_info')
        )->render();

    } catch (\Exception $e) {
        return $e->getMessage();
    }
}


function tablaprincipalOP()
{
    //datos de la tabla principal para los primeros 3 swiches
    try {

        require_once '../../config.php';
        require_once '../../config.php';
        $views = $ABSOL_PATH . 'app/views';
        $cache = $ABSOL_PATH . 'app/cache';

        //Clearing the views cache
        \App\Helpers\MiscFunctions::clear_view_cache();

        $blade = new Blade($views, $cache);

        $reporte_pagos = new \App\Helpers\ReportesPagos();
        $festivos = new \App\Helpers\Festivos(date('Y'));

        $desde = date("Y-m-d H:i:s", strtotime($_POST['from']));
        $hasta = date("Y-m-d H:i:s", strtotime($_POST['to']));
        $swicheselected = $_POST['swiche'];



        if ($swicheselected == "Contratista" || $swicheselected == "Financiera" || $swicheselected == "Supervisor") {
            $datos = array();

            return $blade->view()->make('reportesPagos.partials.tablaprincipalop',
                compact('datos', 'swicheselected')
            )->render();


        } else {
            $datos = "";
            return $blade->view()->make('reportesPagos.partials.tablaprincipaladmin',
                compact('datos', 'swicheselected')
            )->render();

        }


    } catch (\Exception $e) {
        return $e->getMessage();
    }
}

function inforadicado()
{

    try {

        require_once '../../config.php';
        $ruta_raiz = "../../";
        if (isset($db)) unset($db);
        include_once("$ruta_raiz/include/db/ConnectionHandlerNew.php");
        $db = new ConnectionHandler("$ruta_raiz");
        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        $expedHelper = new \App\Helpers\Expedientes($db);
        include_once "$ruta_raiz/tx/verLinkArchivo.php";
        $verLinkArchivo = new verLinkArchivo($db); //para validar si puede abrir los archivos de radicado

        $reporte_pagos = new \App\Helpers\ReportesPagos($db);
        $helperRadi = new \App\Helpers\Radicados($db, $ruta_raiz);
        $helperBorra = new \App\Helpers\Borradores($db, $ruta_raiz);

        $radicado = $_POST['radicado'];

        $json = array();

        $json['historial'] = $reporte_pagos->getHisEvent($radicado);

        $escaneado = $reporte_pagos->verfifiEscan($radicado);

        $json['escaneado'] = $escaneado[0]['CONTADOR'];

        $addexpediente = $reporte_pagos->verifiAddExped($radicado);
        $json['expediente'] = $addexpediente[0]['CONTADOR'];

        $supervisores = $reporte_pagos->getSupervByContrat($_POST['contratista_identifi']);
        $json['supervisor'] = "";
        $json['apoyo'] = "";

        if (isset($supervisores[0])) {
            $datosprincipal = $reporte_pagos->getUsuarioByDoc($supervisores[0]['SUPERVISOR_DOC']);

            $datosapoyo = $reporte_pagos->getUsuarioByDoc($supervisores[0]['SUPERVISOR_APOYO_DOC']);
            $json['supervisor'] ="";
            if(isset($datosprincipal[0])){
                $json['supervisor'] = $datosprincipal[0]['USUA_NOMB'];
            }

            $json['apoyo'] = "";
            if(isset($datosapoyo[0])){
                $json['apoyo'] = $datosapoyo[0]['USUA_NOMB'];
            }
        }


        $resulVali = $verLinkArchivo->valPermisoRadi( $radicado);
        $valImg = $resulVali['verImg'];

        $anexos = $expedHelper->getAnexosByRadicado($radicado);


        /**
         * $asuntosRadicados , almacenará los asuntos de los radicados que se puedan validar
         */
        $asuntosRadicados = array();

        for($i=0; $i<count($anexos);$i++){

            $infoasociado = "select ra_asun from radicado where radi_nume_radi=" . $anexos[$i]["RADI_NUME_SALIDA"];
            $infoasociado = $db->query($infoasociado);
            $anexos[$i]['RA_ASUN']= $infoasociado->fields["RA_ASUN"];

            $explode = explode("/", $anexos[$i]['ANEX_NOMB_ARCHIVO']); //esto va a asi sin la extennsion
            $anexos[$i]['ANEX_NOMB_ARCHIVO']=$explode[count($explode)-1];
            $anexos[$i]['TIENE_PERMISO']="NO";
            if($valImg == "SI") {
                $anexos[$i]['TIENE_PERMISO']="SI";
            }

            $asuntosRadicados[$anexos[$i]["RADI_NUME_SALIDA"]] = '';
            $asuntosRadicados[$anexos[$i]["RADI_NUME_SALIDA"]] = $anexos[$i]['RA_ASUN'];

            $anexos[$i]["anio"] = '';
            $anexos[$i]["anio"] = substr($radicado, 0, 4);

            $anexos[$i]["dependencia"] = '';
            $anexos[$i]["dependencia"] = substr($radicado, 4, 3);

            $pathinfo = $anexos[$i]['ANEX_NOMB_ARCHIVO'];
            $pathinfo = pathinfo($pathinfo);

            $anexos[$i]["extension"] ='';
            $anexos[$i]["extension"] =  $pathinfo['extension'];


        }
        $json['anexos']=$anexos;


        $json['document_metadata']=array();
        $fuevalidado = $helperRadi->getDocumentMetadata($radicado);
        if (count($fuevalidado) > 0) {
            $json['document_metadata']=$fuevalidado[0];
        }

        $json['expedientes']=array();
        $expedientes = $helperBorra->getExpedNotDeleted($radicado);
        if(count($expedientes)>0){
            $json['expedientes']=$expedientes;
        }

        $json['anulado']=array();
        $anulado = $helperRadi->checkIfRadicadoAnulado($radicado);
        if(count($anulado)>0){
            $json['anulado']=$anulado;
        }


        $json['asuntosRadicados']=$asuntosRadicados;

        echo json_encode($json);

    } catch (\Exception $e) {
        return $e->getMessage();
    }
}


function guardarSupervisores()
{

    try {

        //guarda los supervisores a los contratistas
        ini_set('display_errors', 0);
        ini_set('display_startup_errors', 0);
        error_reporting(E_ALL);
        require_once '../../config.php';

        $reporte_pagos = new \App\Helpers\ReportesPagos();

        $json = array();

        $principal = $_POST['principal'];
        $apoyo = $_POST['apoyo'];
        $contratistas = $_POST['contratistas'];
        $guardar = $reporte_pagos->guardarSupervisores($principal, $apoyo, $contratistas);

        echo json_encode($guardar);

    } catch (\Exception $e) {
        return $e->getMessage();
    }
}


function descargarop()
{
        //al presionar el boton verde de descargar
    try {

        ini_set('display_errors', 0);
        ini_set('display_startup_errors', 0);
        error_reporting(E_ALL);
        require_once '../../config.php';

        $reporte_pagos = new \App\Helpers\ReportesPagos();
        $numeroradicado = $_POST['numeroradicado'];

        $tieneop = $reporte_pagos->getAnexOp($numeroradicado);
        $festivos = new \App\Helpers\Festivos(date('Y'));
        echo json_encode($tieneop);


    } catch (\Exception $e) {
        return $e->getMessage();
    }

}

function borrarFileOp()
{
    //borra un archivo de un OP

    try {

        //guarda los supervisores a los contratistas
        ini_set('display_errors', 0);
        ini_set('display_startup_errors', 0);
        error_reporting(E_ALL);
        require_once '../../config.php';

        $reporte_pagos = new \App\Helpers\ReportesPagos();
        $numeroradicado = $_POST['radicado'];
        $ruta = $_POST['ruta'];
        $rutathumbnail = $_POST['rutathumbnail'];
        $nombrearchivo = $_POST['nombrearchivo'];
        $borrar = false;
        if (file_exists($ruta)) {
            $borraranexo = $reporte_pagos->deleteAnexo($numeroradicado, $nombrearchivo);
            $borrar=true;
        }

        $retorno = array();
        if ($borrar) {
            $retorno['success'] = "El archivo ha sido eliminado";
        } else {
            $retorno['error'] = "Ha ocurrido un error al eliminar el archivo";
        }
        echo json_encode($retorno);


    } catch (\Exception $e) {
        return $e->getMessage();
    }
}

function buscarAnexosOP()
{
    //busca los anexos de un OP por numero de radicado
    try {
        ini_set('display_errors', 0);
        ini_set('display_startup_errors', 0);
        error_reporting(E_ALL);
        require_once '../../config.php';

        $ruta_raiz = "../../";
        include_once("$ruta_raiz/include/db/ConnectionHandler.php");
        $db = new ConnectionHandler("$ruta_raiz");
        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);

        $reporte_pagos = new \App\Helpers\ReportesPagos($db);
        $borraHelper     = new \App\Helpers\Borradores($db);

        $radicado = $_POST['radicado'];
        $radicadocompleto=$borraHelper->getRadicadoByNume_radi($radicado);//busco la info del radicado
        $tieneop = $reporte_pagos->getAnexOp($radicado);

        $root = $_SERVER['HTTP_ORIGIN'];
        $cont = 0;

        foreach ($tieneop as $rowAnexo) {
            $base = explode("/", $BASE_PATH);
            $base = $root . "/" . $base[count($base) - 1];
            $ruta = "../../bodega";
            $base .= "/bodega";
            $rutaFinalArchivo = "/bodega";  //esta va a ser la ruta que se va a enviar para eliminar

            $ruta .= "/" . date("Y", strtotime($radicadocompleto[0]['RADI_FECH_RADI']));
            $base .= "/" . date("Y", strtotime($radicadocompleto[0]['RADI_FECH_RADI']));
            $rutaFinalArchivo .= "/" . date("Y", strtotime($radicadocompleto[0]['RADI_FECH_RADI']));

            $ruta .= "/" . $radicadocompleto[0]['DEPE_CODI'];
            $base .= "/" . $radicadocompleto[0]['DEPE_CODI'];
            $rutaFinalArchivo .= "/" . $radicadocompleto[0]['DEPE_CODI'];

            $ruta .= "/docs/";
            $base .= "/docs/";
            $rutaFinalArchivo .= "/docs/";

            $ext = explode(".", $rowAnexo['ANEX_NOMB_ARCHIVO']);
            $name = $rowAnexo['ANEX_NOMB_ARCHIVO'];
            $nombrethumnail = "80x80" . $rowAnexo['ANEX_NOMB_ARCHIVO'];

            if (file_exists($ruta . $name)) {
                $bytes = filesize($ruta . $name);
                $kb = formatSizeUnits($bytes);

                $tieneop[$cont]['url'] = $base . $name;
                $tieneop[$cont]['thumbnailUrl'] = $base . $nombrethumnail;
                $tieneop[$cont]['name'] = $name;
                $tieneop[$cont]['type'] = $ext[count($ext) - 1];
                $tieneop[$cont]['size'] = $kb . " KB";
                $tieneop[$cont]['delete_url'] = $rutaFinalArchivo . $name;
                $tieneop[$cont]['delete_urlthumbnail'] = $rutaFinalArchivo . $nombrethumnail;
                $tieneop[$cont]['delete_type'] = 'DELETE'; // method for destroy action

            }

            $cont++;
        }

        echo json_encode($tieneop);

    } catch (\Exception $e) {
        return $e->getMessage();
    }

}


?>

