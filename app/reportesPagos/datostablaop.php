<?php

use App\Helpers\PDOconfig;
use Aura\SqlQuery\QueryFactory;
use Philo\Blade\Blade;

require '../../vendor/autoload.php';

$array = array();

try {
    session_start();
    require_once '../../config.php';
    $views = $ABSOL_PATH . 'app/views';
    $cache = $ABSOL_PATH . 'app/cache';

    //Clearing the views cache
    \App\Helpers\MiscFunctions::clear_view_cache();

    $blade = new Blade($views, $cache);

    $reporte_pagos = new \App\Helpers\ReportesPagos();
    $festivos = new \App\Helpers\Festivos(date('Y'));

    $data = $_GET['data'];
    $desde = date("Y-m-d H:i:s", strtotime($data['from']));
    $hasta = date("Y-m-d H:i:s", strtotime($data['to']));
    $swicheselected = $data['swiche'];


    $datajson = array();


    // Pagination Result
    $array = array();
    $start = 0;
    $limit = false;

    $draw = $_GET['draw'];

    if (!empty($draw)) {
        $start = $_GET['start'];
        $limit = $_GET['length'];
        if ($limit == '-1') {
            $limit = false;
        }
    }

    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    $search = $_GET['search'];
    $buscar = $search['value'];


    $total = $reporte_pagos->tablaprincipalOP($desde, $hasta, $swicheselected,
        false, false, $buscar);


    $cont = 0;
    $detailarray = array();
    if ($swicheselected == "Contratista" || $swicheselected == "Financiera" || $swicheselected == "Supervisor") {

        $buscarsaltos=array(chr(13).chr(10), "\r\n", "\n", "\r");
        $reemplazar=array("", "", "", "");

        $datos = $reporte_pagos->tablaprincipalOP($desde, $hasta, $swicheselected, $start, $limit, $buscar);
        $contadordetail=0;

        if (count($datos) > 0) {
            foreach ($datos as $row) {

                $tieneop = $reporte_pagos->getAnexOp($row['RADICADO']);
                $tieneopcount = count($tieneop); //guardo el contador de las OP que tenga

                    //busco todos los anexos que sean OP
                    $OPHABILITADAS = 0;
                    $PRIMERAFECHAPAGO = "";//primera fecha habilitada para pagar
                    $PRIMERFALTANDIAS = 0; //dias habiles que faltan para poder pagar
                    if (isset($tieneop[0])) {
                        //recorro todos los anexos OP a ver si tengo alguno que se peuda descargar, para habilitar el boton
                        //en la vista
                        foreach ($tieneop as $rowAnexo) { //recorro todos los anexos OP
                            //aqui verifico si la fecha de este anexo, +3 dias habiles, es mayor a la fecha de hoy,
                            //para poderlos descargar

                            $fechaRespuesta =
                                $festivos->diasHabiles($rowAnexo['ANEX_FECH_ANEX'], 3);
                            $PRIMERAFECHAPAGO = date("d-m-Y", strtotime($fechaRespuesta));

                            //var_dump($rowAnexo['ANEX_FECH_ANEX']);

                            if (date("Y-m-d") >= $fechaRespuesta) {
                                $OPHABILITADAS++; //sumo
                            } else {
                                //entra aqui si la fecha de hoy es menor a la fecha retornada
                                $fecha = date("Y-m-d");
                                //cuento la cantidad de dias habiles que faltan, desde la fecha de hoy, hasta la fecha retornada
                                while ($fecha < $fechaRespuesta) {

                                    $fecha = date("Y-m-d", strtotime($fecha . '+1 day'));

                                    $esfestivo = $festivos->esFestivo(date("d", strtotime($fecha)), date("m", strtotime($fecha)));
                                    $esFinDeSemana = $festivos->esFinDeSemana($fecha);//valido si es fin de semana

                                    if ($esfestivo == false && $esFinDeSemana == false) {
                                        $PRIMERFALTANDIAS++;
                                    }
                                }

                            }
                            break;
                            //esto es porque voy a tomar el primer indice, es decir, el anexo mas viejo, para habilitar el boton
                        }
                    }

                    $CONTRATISTA_json = array();
                    $con = 0;

                    $radicado = "'" . $row["RADICADO"] . "'";
                    $dependencia = "'" . $row["DEPENDENCIA_ACTUAL"] . "'";
                    $usuarioactual = "'" . $row["USUARIO_ACTUAL"] . "'";
                    $contratistaidentif = "'" . $row["CONTRATISTA_IDENTIF"] . "'";

                    //ultimo td que contiene el boton
                    $CONTRATISTA_json[$con] = '<a class="detail-icon" href="javascript:"><i class="glyphicon glyphicon-plus icon-plus"></i></a>';
                    $con++;

                    $CONTRATISTA_json[$con] = '';
                    if (strlen($row["RADI_PATH"]) > 0 && file_exists('../../bodega' . $row["RADI_PATH"])) {
                        $CONTRATISTA_json[$con] .= '<a href="../../bodega' . $row["RADI_PATH"] . '" target="_blank">' . $row["RADICADO"] . '</a>';
                    } else {
                        $CONTRATISTA_json[$con] .= $row["RADICADO"];
                    }
                    $CONTRATISTA_json[$con] .= '<br>' . date("d-m-Y H:i:s", strtotime($row["FECHA_RADICADO"]));
                    $con++;


                    $CONTRATISTA_json[$con] = '<a href="../../verradicado.php?verrad=' . $row["RADICADO"] . '&' . session_name() . '=' . trim(session_id()) . '&menu_ver_tmp=3"
                        target="_blank">' . $row["ASUNTO"] . '</a>';
                    $con++;

                    $CONTRATISTA_json[$con] = $row["CONTRATISTA"];
                    $con++;

                    $CONTRATISTA_json[$con] = $row["USUARIO_ACTUAL"] . ' - ' . $row["DEPENDENCIA_ACTUAL_NUMERO"];
                    $con++;

                    $CONTRATISTA_json[$con] = ' <button type="button" class="btn btn-circle btn-sm';

                    if ($swicheselected == "Financiera") {
                        if ($tieneopcount> 0) {
                            $CONTRATISTA_json[$con] .= " green-jungle";
                        } else {
                            $CONTRATISTA_json[$con] .= " yellow-gold";
                        }
                    } else {
                        // tiene existe OPHABILITADAS y es <1 quiere decir que no tiene OP habilitadas para descargar
                        if ($tieneopcount < 1) {
                            $CONTRATISTA_json[$con] .= " default";
                        } else {
                            //si entra aqui es porque tiene dispoible para descargar
                            $CONTRATISTA_json[$con] .= " green-jungle";
                        }

                    }

                    $CONTRATISTA_json[$con] .= '" style="';
                    if ($swicheselected == "Supervisor" || $swicheselected == "Contratista") {
                        // tiene existe TIENEOP y es >0 y tiene 0 OPHABILITADAS quiere decir que todavia no es la fecha de
                        //descargar los OP pero si tiene OP
                        if ($tieneopcount> 0 && isset($OPHABILITADAS) &&
                            $OPHABILITADAS < 1
                        ) {
                            $CONTRATISTA_json[$con] .= "opacity: 0.6;";
                        }

                    }
                    $CONTRATISTA_json[$con] .= '" ';

                $asuntostrin=str_ireplace($buscarsaltos,$reemplazar,$row["ASUNTO"]);
                    $asuntostrin="'".$asuntostrin."'";

                    if ($swicheselected == "Financiera") {
                        $CONTRATISTA_json[$con] .= ' onclick="modalsubirop(' . $row["RADICADO"] . ','.$asuntostrin.')" ';
                    } else {
                        $primerafechapago = "'" . $PRIMERAFECHAPAGO . "'";
                        $CONTRATISTA_json[$con] .= ' onclick="descargarop(event,' . $row["RADICADO"] . ',' . $OPHABILITADAS . ',' . $tieneopcount . ',' . $PRIMERFALTANDIAS . ',' . $primerafechapago . ')" ';
                    }

                    $CONTRATISTA_json[$con] .= '<i class="fa ';
                    $swicheselected == "Financiera" ? $CONTRATISTA_json[$con] .= "fa-upload" : $CONTRATISTA_json[$con] .= "fa-download";
                    $CONTRATISTA_json[$con] .= ' "></i><span>';
                    if ($swicheselected == "Financiera") {
                        if ($tieneopcount > 0) {
                            $CONTRATISTA_json[$con] .= "Editar OP";
                        } else {
                            $CONTRATISTA_json[$con] .= "Subir OP";
                        }
                    } else {
                        $CONTRATISTA_json[$con] .= "Descargar OP";
                    }
                    $CONTRATISTA_json[$con] .= ' </span></button>';

if ($swicheselected == "Financiera") {
    $CONTRATISTA_json[$con] .= '<button type="button" class="btn btn-circle btn-success"
onclick="OrdenesDePago.showModalReasignar(event,' . $row["RADICADO"] . ')">Reasignar</button>';
}
                $con++;

                $CONTRATISTA_json['RADICADO'] = $row["RADICADO"];

                $CONTRATISTA_json["DEPENDENCIA_ACTUAL"] = $row["DEPENDENCIA_ACTUAL"];

                $CONTRATISTA_json["USUARIO_ACTUAL"] = $row["USUARIO_ACTUAL"];

                $CONTRATISTA_json["CONTRATISTA_IDENTIF"] = $row["CONTRATISTA_IDENTIF"];

                $CONTRATISTA_json["DEPENDENCIA_ACTUAL_NUMERO"] = $row["DEPENDENCIA_ACTUAL_NUMERO"];



                /////
                $datajson[] = $CONTRATISTA_json;

            }
        }

    }

    $array['data'] = $datajson;
    $array['draw'] = $draw;//esto debe venir por post
    $array['recordsTotal'] = count($total);
    $array['recordsFiltered'] = count($total);

} catch (Exception $e) {
    echo "Error: " . $e->getMessage();
    $response = "ERROR";
    $array['error'] = true;
}
echo json_encode($array);


?>

