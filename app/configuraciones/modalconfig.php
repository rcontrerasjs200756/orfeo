<?php

use App\Helpers\PDOconfig;
use Aura\SqlQuery\QueryFactory;
use Philo\Blade\Blade;

require '../../vendor/autoload.php';
if (!isset($_SESSION)) {
    session_start();
}
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once '../../config.php';
$ruta_raiz = "../../";
if (isset($db)) unset($db);
include_once("$ruta_raiz/include/db/ConnectionHandlerNew.php");
$db = new ConnectionHandler("$ruta_raiz");
$db->conn->SetFetchMode(ADODB_FETCH_ASSOC);


$configHelper = new \App\Helpers\Configuraciones($db, $ruta_raiz);
$helperRadi = new \App\Helpers\Radicados($db);
$borra = new \App\Helpers\Borradores($db);

$titulos = $configHelper->getTitulosTratamiento();
$tiposradval = $helperRadi->tiposradicadovalidar();
$tiporadicustom = $helperRadi->tiposradicadovalidarcustom();

//si son varios modulos, deben venir separados por coma
$modulos_ini = isset($_POST['modulos']) && $_POST['modulos'] != "TODOS" ? $_POST['modulos'] : false;
$modulo_Borrador = false;
$modulos = $modulos_ini;
$usuarios = $helperRadi->getUsersWithPermAprob('',
    false,false,'TODOS');

$usuarioeenviar = array();
if (count($usuarios) > 0) {
    $usuarioeenviar = $borra->barmarUsuariosComen($usuarios);
}

if ($modulos != false) {
    $explode = explode(',', $modulos);
    $modulos = "";

    foreach ($explode as $row) {

        if ($row == "BORRADORES") {
            $modulo_Borrador = true;
        }

        $modulos = "'" . $row . "',";
    }
    $modulos = substr($modulos, 0, strlen($modulos) - 1);
}


$configuraciones = $configHelper->getConfiguraciones(false, false, $modulos);
$miperfil = 'NORMAL';
if ($_SESSION["usua_admin_archivo"] == 5) {
    $miperfil = 'GESTION';
}
if ($_SESSION["codusuario"] == 1) {
    $miperfil = 'JEFE';
}
if ($_SESSION["usua_admin_sistema"] == 1) {
    $miperfil = 'ADMINISTRADOR';
}
if ($_SESSION["usua_admin_sistema"] == 2) {
    $miperfil = 'SUPERADMINISTRADOR';
}

$selectsmultiples = array();
?>

<form id="formulario_config">
    <table width="100%" class="table-bordered">
        <?php

        $funciones = array();

        if (count($configuraciones)) {
            foreach ($configuraciones as $row) {
                if ($row['VISIBLE'] == 1) {
                    $constante = $row['NOMBRE_CONSTANTE'];
                    $config_perfil = $configHelper->getConfisPerfil("'" . $row['ID'] . "'", "'" . $miperfil . "'");
                    $habilitar_mostrar = true;
                    if (count($config_perfil) > 0 || $miperfil == "SUPERADMINISTRADOR") {
                        //si es funcion, y como viene ordenado por funciones primero, guardo todas las funciones
                        if ($row['TIPO'] == "f") {
                            $funciones[] = $row;
                        } else {
                            //entonces cuando no sean funciones, pregunto si el valor de la funcion permite mostrar las demas configuraciones,
                            //que dependen de la funcion
                            foreach ($funciones as $function) {
                                if ($function['MODULO'] == $row['MODULO'] && $function['VALOR'] == "NO") {
                                    $habilitar_mostrar = false;
                                    break;
                                }
                            }
                        }
                        $valor = $row['VALOR'];
                        if ($habilitar_mostrar == true) {

                            ?>
                            <tr>
                                <td><?= $row['NOMBRE_FUNCION'] ?>
                                    <?php if ($constante == 'TITULO_MEMO') { ?>
                                        <button type="button" class="btn blue"
                                                style="float:right;margin-right: 10px; border-radius: 20px !important;"
                                                title="Agregar Nuevo" onclick="crearTituloMemo()">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    <?php } ?>


                                </td>
                                <td><?php if (json_decode($row['OPCIONES']) == NULL && $row['VALOR_TIPO'] != 'lista_desplegable_multiple') {
                                        $claseFecha = "";

                                        if ($row['VALOR_TIPO'] == "fecha") {
                                            $claseFecha = "date-picker";
                                            $valor = date('d-m-Y H:i', strtotime($valor));
                                        }
                                        ?>
                                        <input type="text" name="<?= $row['NOMBRE_CONSTANTE'] ?>"
                                               id="<?= $row['NOMBRE_CONSTANTE'] ?>"
                                               class="form-control <?= $claseFecha ?>" value="<?= $valor ?>"
                                               data-content="<?= $row['DESCRIPCION_AYUDA'] ?>" data-trigger="hover"
                                        >
                                    <?php } else {
                                        $opciones = json_decode($row['OPCIONES']);

                                        if (count($opciones) > 0 || $row['VALOR_TIPO'] == 'lista_desplegable_multiple') {


                                            if ($row['VALOR_TIPO'] == 'lista_desplegable_multiple') {

                                                $selectsmultiples[] = $row['NOMBRE_CONSTANTE'];
                                                if ($constante == 'TIPOS_RADICADO_VALIDAR') {
                                                    ?>

                                                    <select class="js-example-basic-multiple selecmultiple" multiple="multiple"
                                                            name="<?= $row['NOMBRE_CONSTANTE'] ?>[]"
                                                            id="<?= $row['NOMBRE_CONSTANTE'] ?>"
                                                            onchange="changeselect(this,'<?= $constante ?>')">

                                                        <?php foreach ($tiposradval as $tiposrad) {

                                                            $selected = '';

                                                            if (json_decode($row['VALOR']) != NULL && count(json_decode($row['VALOR'])) > 0) {
                                                                $opciones = json_decode($row['VALOR']);

                                                                foreach ($opciones as $opcion) {
                                                                    if ($opcion == $tiposrad['SGD_TRAD_CODIGO']) {
                                                                        $selected = 'selected';
                                                                    }
                                                                }
                                                            } ?>
                                                            <option value="<?= $tiposrad['SGD_TRAD_CODIGO'] ?>" <?= $selected ?>><?= $tiposrad['SGD_TRAD_DESCR'] ?> </option>
                                                        <?php } ?>
                                                    </select>

                                                    <?php
                                                } elseif ($constante == 'TITULO_MEMO') { ?>

                                                    <select class="js-example-basic-multiple selecmultiple" multiple="multiple"
                                                            name="<?= $row['NOMBRE_CONSTANTE'] ?>[]"
                                                            id="<?= $row['NOMBRE_CONSTANTE'] ?>">

                                                        <?php foreach ($opciones as $opcion) {
                                                            $selected = '';
                                                            if (json_decode($row['VALOR']) != NULL && count(json_decode($row['VALOR'])) > 0) {
                                                                $valores = json_decode($row['VALOR']);
                                                                foreach ($valores as $v) {
                                                                    if ($opcion == $v) {
                                                                        $selected = 'selected';
                                                                    }
                                                                }
                                                            } ?>
                                                            <option value="<?= $opcion ?>" <?= $selected ?> ><?= $opcion ?> </option>
                                                        <?php } ?>

                                                    </select>

                                                <?php
                                                } elseif ($constante == 'USUARIO_RECIBE_REPORTE4') {  ?>
                                                    <select class="js-example-basic-multiple selecmultiple" multiple="multiple"
                                                            name="<?= $row['NOMBRE_CONSTANTE'] ?>[]"
                                                            id="<?= $row['NOMBRE_CONSTANTE'] ?>"
                                                            onchange="changeselect(this,'<?= $constante ?>')">
                                                        <?php foreach ($usuarioeenviar as $usuario) {

                                                            $selected = '';

                                                            if (json_decode($row['VALOR']) != NULL && count(json_decode($row['VALOR'])) > 0) {
                                                                $opciones = json_decode($row['VALOR']);

                                                                foreach ($opciones as $opcion) {
                                                                    if ($opcion == $usuario['ID']) {
                                                                        $selected = 'selected';
                                                                    }
                                                                }
                                                            } ?>
                                                            <option value="<?= $usuario['ID'] ?>" <?= $selected ?>><?= $usuario['NOMBRE'] ?> </option>
                                                        <?php } ?>
                                                    </select>

                                                    <?php

                                                }else if($constante == 'TIPO_RADICADO_5'){ 
                                                    ?>

                                                    <select class="js-example-basic-multiple selecmultiple" multiple="multiple"
                                                            name="<?= $row['NOMBRE_CONSTANTE'] ?>[]"
                                                            id="<?= $row['NOMBRE_CONSTANTE'] ?>"
                                                            onchange="changeselect(this,'<?= $constante ?>')">

                                                        <?php foreach ($tiporadicustom as $tiposrad) {

                                                            $selected = '';
                                                            $valorJSON = '';        
                                                            if (json_decode($row['VALOR']) != NULL && count(json_decode($row['VALOR'])) > 0) {
                                                                $opciones = json_decode($row['VALOR']);

                                                                foreach ($opciones as $opcion) {
                                                                    if ($opcion == $tiposrad['SGD_TRAD_CODIGO']) {
                                                                        $selected = 'selected';
                                                                    }
                                                                }
                                                            } ?>
                                                            <option value='<?= $tiposrad['SGD_TRAD_CODIGO']  ?>' <?= $selected ?>><?= $tiposrad['SGD_TRAD_DESCR'] ?> </option>
                                                        <?php } ?>
                                                    </select>

                                                    <?php
                                            
                                                }else {


                                                    ?>

                                                    <select class="js-example-basic-multiple selecmultiple" multiple="multiple"
                                                            name="<?= $row['NOMBRE_CONSTANTE'] ?>[]"
                                                            id="<?= $row['NOMBRE_CONSTANTE'] ?>"
                                                            onchange="changeselect(this,'<?= $constante ?>')" >
                                                        <?php foreach ($opciones as $opcion) {
                                                            $selected = '';
                                                            if (json_decode($row['VALOR']) != NULL && count(json_decode($row['VALOR'])) > 0) {
                                                                $valores = json_decode($row['VALOR']);
                                                                foreach ($valores as $v) {
                                                                    if ($opcion == $v) {
                                                                        $selected = 'selected';
                                                                    }
                                                                }
                                                            } ?>
                                                            <option value="<?= $opcion ?>" <?= $selected ?> ><?= $opcion ?> </option>
                                                        <?php } ?>
                                                    </select>

                                                <?php }

                                            } else {
                                                ?>
                                                <select class="form-control" name="<?= $row['NOMBRE_CONSTANTE'] ?>"
                                                        data-trigger="hover"
                                                        data-content="<?= $row['DESCRIPCION_AYUDA'] ?>"
                                                        id="<?= $row['NOMBRE_CONSTANTE'] ?>"
                                                        onchange="changeselect(this,'<?= $constante ?>')">
                                                    <option value="SELECCIONE" selected>SELECCIONE</option>
                                                    <?php foreach ($opciones as $opcion) { ?>
                                                        <option value="<?= $opcion ?>" <?= $row['VALOR'] == $opcion ? "selected" : '' ?> ><?= $opcion ?> </option>
                                                    <?php } ?>
                                                </select>


                                            <?php }
                                        }

                                    } ?></td>
                            </tr>
                        <?php }
                    }
                }
            }
            //si estamos en el modulo de borradores, o el modulo_ini=false (quiere decir que estamos trayendo todos los modulos)
            if ($modulo_Borrador == true || $modulos_ini == false) {
                ?>
                <tr>
                    <td><span>T&iacute;tulos Tratamiento</span>
                        <button type="button" class="btn blue"
                                style="float:right;margin-right: 10px; border-radius: 20px !important;"
                                title="Agregar Nuevo" onclick="crearTitulo()">
                            <i class="fa fa-plus"></i>
                        </button>
                    </td>
                    <td style="display : inline-flex;"><?php

                        if (count($titulos) > 0) { ?>
                            <select class="form-control" onchange="changeTituloConfig(this)" style="width: 100%"
                                    name="TITULO_TRATAMIENTO"
                                    id="TITULO_TRATAMIENTO">
                                <option value="SELECCIONE" selected>SELECCIONE</option>
                                <?php foreach ($titulos as $opcion) { ?>
                                    <option value="<?= $opcion['ID'] ?>"><?= $opcion['TITULO'] ?></option>
                                <?php } ?>
                            </select>
                        <?php } ?>
                        <button type="button" class="btn blue btntitulosconfig"
                                style="margin-left: 10px; border-radius: 20px !important; display: none;"
                                title="Editar" onclick="editarTitulo()">
                            <i class="fa fa-edit"></i>
                        </button>
                        <button type="button" class="btn red btntitulosconfig"
                                style="margin-left: 10px; border-radius: 20px !important; display: none;"
                                title="Editar" onclick="borrarTitulo()">
                            <i class="fa fa-trash"></i>
                        </button>
                    </td>
                </tr>
                <?php
            }
        }
        ?>
    </table>
</form>

<script>

    var languageseelct2 = {
        inputTooShort: function () {
            return 'Ingrese para buscar';
        },
        noResults: function () {
            return "Sin resultados";
        },
        searching: function () {
            return "Buscando...";
        },
        errorLoading: function () {
            return 'El resultado aún no se ha cargado.';
        },
        loadingMore: function () {
            return 'Cargar mas resultados...';
        },
    }

    $(document).ready(function () {
        declararSelect2(<?php  echo json_encode($selectsmultiples) ?>);

        $(".date-picker").datepicker({
            orientation: "left",
            language: 'es',
            format: 'dd-mm-yyyy'
        })
    });
</script>
