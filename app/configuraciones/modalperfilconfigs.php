<?php

use App\Helpers\PDOconfig;
use Aura\SqlQuery\QueryFactory;
use Philo\Blade\Blade;

require '../../vendor/autoload.php';
if (!isset($_SESSION)) {
    session_start();
}
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once '../../config.php';
$configHelper = new \App\Helpers\Configuraciones();
$configuraciones = $configHelper->getConfiguraciones(false, false, false);
$perfiles = array('ADMINISTRADOR', 'JEFE', 'GESTION', 'NORMAL');
?>
<style>
    .select2-dropdown--below {
        top: -2.8rem; /*your input height*/
    }
</style>

<form id="formulario_perfil_config">
    <table width="100%" class="table-bordered">
        <thead>
        <tr>
            <th>Funcionalidad</th>
            <th>Perfil</th>
            <th>Visible</th>
        </tr>
        </thead>
        <tbody>
        <?php if(count($configuraciones)) {
        foreach ($configuraciones as $row) {
        $config_perfil = $configHelper->getConfisPerfil("'" . $row['ID'] . "'", false);
        ?>
        <tr>
            <td  width="60%" data-trigger="hover"
                 data-content="<?= $row['DESCRIPCION_AYUDA'] ?>"><?= $row['NOMBRE_FUNCION'] ?></td>
            <td width="20%">
                <select class="form-control chosen-select selectperfiles" name="config_perfil[<?= $row['ID'] ?>][]" multiple
                        id="config_perfil<?= $row['ID'] ?>">
                    <?php foreach ($perfiles as $perfil) {
                        $selected = '';
                        foreach ($config_perfil as $configperfil) {
                            if ($configperfil['PERFIL'] == $perfil) {
                                $selected = 'selected';
                            }
                        }
                        ?>
                        <option value="<?= $perfil ?>" <?= $selected ?>><?= $perfil ?></option>

                    <?php } ?>
                </select>
            </td>

            <td width="20%">
                SI
                <input type="radio" name="radioconfig[<?= $row['ID'] ?>]" value="1" <?= $row['VISIBLE']==1?'checked':'' ?> >
                NO
                <input type="radio" name="radioconfig[<?= $row['ID'] ?>]" value="0"  <?= $row['VISIBLE']==0?'checked':'' ?>>

            </td>

        </tr>
        <?php }
        }

        ?>
        </tbody>
    </table>
</form>


