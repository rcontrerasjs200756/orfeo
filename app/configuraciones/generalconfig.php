<?php

use App\Helpers\PDOconfig;
use Aura\SqlQuery\QueryFactory;
use Philo\Blade\Blade;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require '../../vendor/autoload.php';
if (!isset($_SESSION["usuario_id"])) {
    session_start();
}
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

function validarGuardarConfig($borradHelper)
{

    $continuar = true;
    foreach ($_POST as $key => $value) {
        if ($key == "ANULACIONES_EXPEDIENTE") {
            if ($value == "") {
                $value = null;
            } else {
                $existe = $borradHelper->get_expedientes(false, $value);
                if (count($existe) < 1) {
                    $continuar = 'El expediente no existe o está cerrado';
                    break;
                }
            }
        }
    }

    return $continuar;
}

function guardarConfigs()
{

    require_once '../../config.php';
    $json = array();
    $configHelper = new \App\Helpers\Configuraciones();
    $borradHelper = new \App\Helpers\Borradores();
    $anulacion_automatica = 'NO';

    $validar = validarGuardarConfig($borradHelper);

    if ($validar === true) {

        foreach ($_POST as $key => $value) {

            $continuar = true;
            if ($value == "SELECCIONE") {
                $value = null;
            }
            if ($key == "CRON_FECHA_INICIAL") {
                if ($value == "") {
                    $value = null;
                } else {
                    $value = date('Y-m-d', strtotime($value)) . " 00:00";
                }
            }

            if ($key == "FECHA_RADICACION_REPORTE4") {
                if ($value == "") {
                    $value = null;
                } else {
                    $value = date('Y-m-d H:i', strtotime($value));
                }
            }

            if ($key == 'FUNCTION_ANULA_AUTO') {
                $anulacion_automatica = $value;
            }

            if ($key == 'NOMBRE_BOTON_VALIDAR') {
                /**
                 * cuando estoy guardandio NOMBRE_BOTON_VALIDAR, entonces seteo el valor
                 * en NOMBRE_BOTON_VALIDAR_CONJUGADO, y le coloco los valores respectivos
                 */
                $buscarConjugado=$configHelper->getConfiguraciones("'NOMBRE_BOTON_VALIDAR_CONJUGADO'");
                $decoded=json_decode($buscarConjugado[0]['OPCIONES']);
                $valueconjugado=json_encode($decoded->$value);
                $configHelper->updateValorConfig('NOMBRE_BOTON_VALIDAR_CONJUGADO', $valueconjugado, $buscarConjugado);
            }


            if ($key == 'ANULACIONES_EXPEDIENTE') {

                $ANULACIONES_EXPEDIENTE = $value;

                if (substr($ANULACIONES_EXPEDIENTE, 0, 4) != date('Y')) {
                    $continuar = false;
                }
            }

            if ($continuar) {

                $filaconfig = $configHelper->getConfiguraciones("'" . $key . "'", false, false);

                if (count($filaconfig) > 0) {
                    $_SESSION[$key] = $value;

                    //si es lista_desplegable_multiple, es un arreglo, por lo tanto lo guardo en session convertido a string
                    if ($filaconfig[0]['VALOR_TIPO'] == 'lista_desplegable_multiple') {
                        $_SESSION[$key] = json_encode($value);
                    }
                    $actualizar = $configHelper->updateValorConfig($key, $value, $filaconfig);
                }
            }
        }

        $json['configuraciones'] = $configHelper->getConfiguraciones(false, false, false);
    } else {
        $json['error'] = $validar;
    }
    echo json_encode($json);
}

function guardarPerfilConfigs()
{
    require_once '../../config.php';
    $json = array();
    $configHelper = new \App\Helpers\Configuraciones();
    $configHelper->deleteAllPerfilConfigs();
    foreach ($_POST['config_perfil'] as $key => $value) {
        foreach ($value as $val) {
            $insertar = $configHelper->insertPerfilConfigs($key, $val);
        }
    }
    foreach ($_POST['radioconfig'] as $key => $value) {
        $update = $configHelper->updateVisibleConfig($key, $value + 0);
    }


    $json['configuraciones'] = $configHelper->getConfiguraciones(false, false, false);
    echo json_encode($json);
}

function guardartitulos()
{

    require_once '../../config.php';
    $ruta_raiz = "../../";
    if (isset($db)) unset($db);
    include_once("$ruta_raiz/include/db/ConnectionHandler.php");
    $db = new ConnectionHandler("$ruta_raiz");
    $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
    $json = array();
    $configHelper = new \App\Helpers\Configuraciones($db);

    $json = array();
    if ($_POST['tituloConfigSelected'] > 10 || $_POST['tituloConfigSelected'] == 'SELECCIONE') {

        if ($_POST['abreviatura'] == "") {
            $_POST['abreviatura'] = "null";
        } else {
            $_POST['abreviatura'] = "'" . $_POST['abreviatura'] . "'";
        }

        if ($_POST['saveorupdateTitulo'] == "nuevo") {
            $retorno = $configHelper->insertTitulo($_POST['titulo'], $_POST['abreviatura'], $_POST['activo'],
                $_POST['orden']);
        } else {
            $retorno = $configHelper->updateTitulo($_POST['tituloConfigSelected'], $_POST['titulo'], $_POST['abreviatura'], $_POST['activo'],
                $_POST['orden']);
        }

        if (isset($retorno['error'])) {
            $json = $retorno;
        } else {
            $json['titulos'] = $configHelper->getTitulosTratamiento();
        }

    } else {
        $json['error'] = "No puede editar este título";
    }

    echo json_encode($json);
}

function guardartitulosmemos()
{
    require_once '../../config.php';
    $ruta_raiz = "../../";
    if (isset($db)) unset($db);
    include_once("$ruta_raiz/include/db/ConnectionHandler.php");
    $db = new ConnectionHandler("$ruta_raiz");
    $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
    $json = array();
    $configHelper = new \App\Helpers\Configuraciones($db);
    $retorno = $configHelper->insertTituloMemo($_POST['titulo']);

    if (isset($retorno['error'])) {
        $json = $retorno;
    } else {
        $buscarConfig = $configHelper->getConfiguraciones("'TITULO_MEMO'");
        $json['valor'] = json_decode($buscarConfig[0]['VALOR']);
        $json['opciones'] = json_decode($buscarConfig[0]['OPCIONES']);
    }


    echo json_encode($json);
}

//cambia la clave de validacion
function actualizaClaveModValidacion()
{
    require_once '../../config.php';
    $ruta_raiz = "../../";

    if (isset($db)) unset($db);
    include_once("$ruta_raiz/include/db/ConnectionHandler.php");
    $db = new ConnectionHandler("$ruta_raiz");
    $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
    $json = array();
    $configHelper = new \App\Helpers\Configuraciones($db);
    $borraHelper = new \App\Helpers\Borradores($db);

    $usuario_destino = $borraHelper->getUsuarioById($_SESSION['usuario_id']);
    $retorno = array();

    if (count($usuario_destino) > 0) {

        //si existe codigoemail, es porque se le habilitó el input de texto para escribir el código
        //y se le acaba de enviar un email y no se ha salido de la ventana de escribir contraseña
        if (isset($_POST['codigoemail'])) {

            if (!isset($_SESSION["codigo_passwvalidar_doc"])) {

                sendCodigoEmail($ruta_raiz);
                $json['error'] = 'Ha sido enviado un código a su email de verificación';
                echo json_encode($json);
                exit();
            }

            if ($_POST['codigoemail'] == '') {
                $json['error'] = 'Debe ingresar el código de verificación enviado a su email';
                echo json_encode($json);
                exit();
            }

            if ($_SESSION["codigo_passwvalidar_doc"] != '' &&
                $_POST['codigoemail'] !== $_SESSION["codigo_passwvalidar_doc"]) {
                $json['error'] = 'El código ingresado es distinto al enviado a su email';
                echo json_encode($json);
                exit();
            }

        } else {
            //o borraron el input, o simplemente no viene el input de el codigo
            sendCodigoEmail($ruta_raiz);
            if ($usuario_destino[0]['USUA_PASW_DOC'] == NULL
                || $usuario_destino[0]['USUA_PASW_DOC'] == '') {
                $json['error'] = 'NO TIENE PASSWORD';
                echo json_encode($json);
                exit();

            } else {
                $json['error'] = 'TIENE PASSWORD Y MODIFICA';
                echo json_encode($json);
                exit();
            }

        }


        if (!isset($_POST['passvalidacion'])) {
            $json['error'] = 'Debe ingresar una Contraseña';
            echo json_encode($json);
            exit();
        }
        $clave = $_POST['passvalidacion'];


        if ($usuario_destino[0]['USUA_PASW'] == SUBSTR(md5($clave), 1, 26)) {
            $json['error'] = 'La contraseña, no puede ser la misma de ingreso al aplicativo';
            echo json_encode($json);
            exit();
        }

        if (!isset($_POST['repeatvalidacion'])) {
            $json['error'] = 'Debe repetir la Contraseña';
            echo json_encode($json);
            exit();
        }

        if ($_POST['passvalidacion'] == '' || $_POST['passvalidacion'] == '') {
            $json['error'] = 'Debe ingresar una contraseña';
            echo json_encode($json);
            exit();
        }

        if (strlen($_POST['passvalidacion']) < 8) {
            $json['error'] = 'Debe contener al menos 8 caracteres';
            echo json_encode($json);
            exit();
        }

        if ($_POST['passvalidacion'] !== $_POST['passvalidacion']) {
            $json['error'] = 'Las contraseñas no coinciden';
            echo json_encode($json);
            exit();
        }


        $retorno = $configHelper->savePassValidac($_SESSION['usuario_id'], SUBSTR(md5($clave), 1, 26));
        $_SESSION["USUA_PASW_DOC"] = SUBSTR(md5($clave), 1, 26);
        $_SESSION["FECHA_PASW_DOC"] = date('YmdHis');
        //guardo el historial
        $configHelper->saveHistCambioClaveValidacDos();

    } else {
        $json['error'] = 'El usuario no se encuentra logueado en la aplicación';
        echo json_encode($json);
        exit();
    }
    echo json_encode($retorno);

}

function sendCodigoEmail($ruta_raiz)
{

    include_once $ruta_raiz . "conf/configPHPMailer.php";
    $email = $_SESSION["usua_email"];
    //$email = 'fernandoga22@gmail.com';
    // Instantiation and passing `true` enables exceptions
    $mail = new PHPMailer(true);
    $codigo = rand(100000, 999999);
    $codigo = "" . $codigo . "";
    $json = array();
    $conjugacionValidar=json_decode($_SESSION['NOMBRE_BOTON_VALIDAR_CONJUGADO'],true);
    try {
        //Server settings
        $mail->SMTPDebug = $debugPHPMailer;                                       // Enable verbose debug output
        $mail->isSMTP();                                            // Set mailer to use SMTP
        $mail->Host = $hostPHPMailer;  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                                   // Enable SMTP authentication
        $mail->Username = $usuarioEmailPQRS;                     // SMTP username
        $mail->Password = $passwordEmailPQRS;                               // SMTP password
        $mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
        $mail->Port = $portPHPMailer;                                    // TCP port to connect to

        //Recipients
        $mail->setFrom($admPHPMailer, $admPHPMailer);          // Name is optional
        $mail->addAddress($email, $email);     // Add a recipient


        // Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $subject = $_SESSION["ambiente"] != 'produccion' ? 'PRUEBA: Código de seguridad contraseña de '.strtolower($conjugacionValidar[10]).'' :
            'Código de seguridad contraseña de '.strtolower($conjugacionValidar[10]);
        $body = $_SESSION["ambiente"] != 'produccion' ? 'PRUEBA: ' : '';
        $mail->Subject = utf8_decode($subject);
        $mail->Body = utf8_decode("$body El código de seguridad a ingresar para la asignación de la contraseña de ".strtolower($conjugacionValidar[10]).", es el siguiente: <b>$codigo</b>");
        $mail->AltBody = utf8_decode("$body El código de seguridad a ingresar para la asignación de la contraseña de ".strtolower($conjugacionValidar[10]).", es el siguiente: $codigo");

        $enviar = $mail->send(); //quitar las barras de comentario
        if ($enviar == true) {
            $json['success'] = 'Correo enviado con éxito';
            $_SESSION["codigo_passwvalidar_doc"] = $codigo;
            $_SESSION["expiracion_passwvalidar_doc"] = date('YmdHis');
            $_SESSION["expiracion_passwvalidar_doc"] = date('YmdHis', strtotime($_SESSION["expiracion_passwvalidar_doc"] . '+' . $_SESSION["EXPIRACION_CODIGO_VALIDACION"] . ' minutes'));

        } else {
            $json['error'] = 'Ha ocurrido un error al enviar el correo con el código';
        }

    } catch (Exception $e) {
        $json['error'] = 'Ha ocurrido un error al enviar el correo con el código. ' . $mail->ErrorInfo;
    }

    return $json;
}

function validaPasswValidacion()
{
    require_once '../../config.php';
    $ruta_raiz = "../../";
    include_once $ruta_raiz . "/conf/configPHPMailer.php";
    if (isset($db)) unset($db);
    include_once("$ruta_raiz/include/db/ConnectionHandler.php");
    $db = new ConnectionHandler("$ruta_raiz");
    $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
    $json = array();
    $borraHelper = new \App\Helpers\Borradores($db);
    $password = $_POST['password'];
    $usuario_destino = $borraHelper->getUsuarioById($_SESSION['usuario_id']);
    $retorno = array();

    if (count($usuario_destino) > 0
        && SUBSTR(md5($password), 1, 26) == $usuario_destino[0]['USUA_PASW_DOC']) {
        $retorno['success'] = 'La contraseña coincide';
    } else {
        $retorno['success'] = 'Contraseña Incorrecta';
    }
    echo json_encode($retorno);
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "validaPasswValidacion") {

    validaPasswValidacion($_POST);
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "guardarConfigs") {

    guardarConfigs($_POST);
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "guardartitulos") {

    guardartitulos($_POST);
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "guardarPerfilConfigs") {

    guardarPerfilConfigs($_POST);
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "guardartitulosmemos") {

    guardartitulosmemos($_POST);
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "actualizaClaveModValidacion") {

    actualizaClaveModValidacion($_POST);
}

