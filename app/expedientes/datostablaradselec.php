<?php

use App\Helpers\PDOconfig;
use Aura\SqlQuery\QueryFactory;
use Philo\Blade\Blade;

require '../../vendor/autoload.php';

$array = array();

try {
    session_start();
    require_once '../../config.php';
    $views = $ABSOL_PATH . 'app/views';
    $cache = $ABSOL_PATH . 'app/cache';

    //Clearing the views cache
    \App\Helpers\MiscFunctions::clear_view_cache();

    $expedieteHelper = new \App\Helpers\Expedientes();

    $data = $_GET['data'];
    $radicado = $data['radicado'];
    $datajson = array();
    // Pagination Result
    $array = array();
    $start = 0;
    $limit = false;
    $draw = $_GET['draw'];

    if (!empty($draw)) {
        $start = $_GET['start'];
        $limit = $_GET['length'];
        if ($limit == '-1') {
            $limit = false;
        }
    }
    $search = $_GET['search'];
    $buscar = $search['value'];
    $total = $expedieteHelper->tablaradselec($radicado,
        false, false, $buscar);



    $cont = 0;
    $detailarray = array();

    $datos = $expedieteHelper->tablaradselec($radicado, $start, $limit, $buscar);
    $contadordetail = 0;
    if (count($datos) > 0) {
        foreach ($datos as $row) {

            $CONTRATISTA_json = array();
            $con = 0;

            $CONTRATISTA_json[$con] ="<span class='btn label label-sm label-info blue'>".$row["RADI_NUME_RADI"]."</span>";
            $con++;

            $CONTRATISTA_json[$con] ="<span class='btn label label-sm label-info blue'>".$row["RA_ASUN"]."</span>";
            $con++;

            $CONTRATISTA_json[$con] = date("d-m-Y H:i:s", strtotime($row["RADI_FECH_RADI"]));
            $con++;

            $CONTRATISTA_json[$con] =$row["DEPENDENCIA_ACTUAL"];
            $con++;

            $CONTRATISTA_json[$con] = $row["RADICADO"];

            /////
            $datajson[] = $CONTRATISTA_json;

        }


    }

    $array['data'] = $datajson;
    $array['draw'] = $draw;//esto debe venir por post
    $array['recordsTotal'] = count($total);
    $array['recordsFiltered'] = count($total);

} catch (Exception $e) {
    echo "Error: " . $e->getMessage();
    $response = "ERROR";
    $array['error'] = true;
}
echo json_encode($array);


?>

