<?php

use App\Helpers\PDOconfig;
use Aura\SqlQuery\QueryFactory;
use Philo\Blade\Blade;

require '../../vendor/autoload.php';

session_start();


function getInfExpediente($post)
{
    //trae la informacion del expediente enviado
    $json = array();
    try {
        require_once '../../config.php';

        $ruta_raiz = "../../";
        if (isset($db)) unset($db);
        include_once("$ruta_raiz/include/db/ConnectionHandlerNew.php");
        $db = new ConnectionHandler("$ruta_raiz");
        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        $expedHelper = new \App\Helpers\Expedientes($db);

        if (isset($_POST['expediente']) && $_POST['expediente'] != "") {

            $response = $expedHelper->getInfExpediente($_POST['expediente']);
            $json['infexpediente'] = $response;

        } else {
            $json['error'] = "Debe ingresar datos válidos";
        }

    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
        $response = "ERROR";
        $json['error'] = "Error ajax";
    }
    echo json_encode($json);
}

function tablaprinciexped($post)
{
    //datos de la tabla principal para los primeros 3 swiches
    try {
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        require_once '../../config.php';
        $views = $ABSOL_PATH . 'app/views';
        $cache = $ABSOL_PATH . 'app/cache';

        //Clearing the views cache
        \App\Helpers\MiscFunctions::clear_view_cache();

        $blade = new Blade($views, $cache);
        $datos = array();

        $session_id=trim(session_id());
        $session_name=session_name();
        echo $blade->view()->make('expedientes.partials.tablaprinciexpediente',
            compact('datos','session_id','session_name')
        )->render();

    } catch (\Exception $e) {
        return $e->getMessage();
    }
}

function tablaradselec(){
    //datos de la tabla de la ventana radicados seleccionados
    try {
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        require_once '../../config.php';
        $views = $ABSOL_PATH . 'app/views';
        $cache = $ABSOL_PATH . 'app/cache';

        //Clearing the views cache
        \App\Helpers\MiscFunctions::clear_view_cache();

        $blade = new Blade($views, $cache);
        $datos = array();

        $session_id=trim(session_id());
        $session_name=session_name();
        echo $blade->view()->make('expedientes.partials.tablaradselec',
            compact('datos','session_id','session_name')
        )->render();

    } catch (\Exception $e) {
        return $e->getMessage();
    }
}

function tablahistorial(){
    //datos de la tabla de la ventana Historial
    try {
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        require_once '../../config.php';
        $views = $ABSOL_PATH . 'app/views';
        $cache = $ABSOL_PATH . 'app/cache';

        //Clearing the views cache
        \App\Helpers\MiscFunctions::clear_view_cache();

        $blade = new Blade($views, $cache);
        $datos = array();

        $session_id=trim(session_id());
        $session_name=session_name();
        echo $blade->view()->make('expedientes.partials.tablahistorico',
            compact('datos','session_id','session_name')
        )->render();

    } catch (\Exception $e) {
        return $e->getMessage();
    }
}

function getTieneExpedientes(){

    $json = array();
    try {

        require_once '../../config.php';
        $ruta_raiz = "../../";
        if (isset($db)) unset($db);
        include_once("$ruta_raiz/include/db/ConnectionHandlerNew.php");

        $db = new ConnectionHandler("$ruta_raiz");
        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        $expedHelper = new \App\Helpers\Expedientes($db);
        $radicado=$_POST['radicado'];
        $tieneexpedientes=0;

        if (isset($_POST['radicado']) && $_POST['radicado'] != "") {

            $json['tieneexpedientes'] =tieneExpedientes($expedHelper,$_POST['radicado']);

        } else {
            $json['error'] = "Debe ingresar datos válidos";
        }

    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
        $response = "ERROR";
        $json['error'] = "Error ajax";
    }
    echo json_encode($json);
}

function getAnexosRad(){

    $json = array();
    try {


        require_once '../../config.php';
        $ruta_raiz = "../../";
        if (isset($db)) unset($db);
        include_once("$ruta_raiz/include/db/ConnectionHandlerNew.php");
        $db = new ConnectionHandler("$ruta_raiz");
        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        $expedHelper = new \App\Helpers\Expedientes($db);
        include_once "$ruta_raiz/tx/verLinkArchivo.php";
        $verLinkArchivo = new verLinkArchivo($db); //para validar si puede abrir los archivos de radicado

        $radicado=$_POST['radicado'];

        if (isset($_POST['radicado']) && $_POST['radicado'] != "") {
            $resulVali = $verLinkArchivo->valPermisoRadi( $radicado);
            $valImg = $resulVali['verImg'];

            $anexos = $expedHelper->getAnexosByRadicado($radicado);

            for($i=0; $i<count($anexos);$i++){
                $explode = explode("/", $anexos[$i]['ANEX_NOMB_ARCHIVO']); //esto va a asi sin la extennsion
                $anexos[$i]['ANEX_NOMB_ARCHIVO']=$explode[count($explode)-1];
                $anexos[$i]['TIENE_PERMISO']="NO";
                if($valImg == "SI") {
                    $anexos[$i]['TIENE_PERMISO']="SI";
                }
            }
            $json['anexos']=$anexos;

        } else {
            $json['error'] = "Debe ingresar datos válidos";
        }

    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
        $response = "ERROR";
        $json['error'] = "Error ajax";
    }
    echo json_encode($json);
}

function tieneExpedientes($expedHelper,$radicado){
    $tieneexpedientes=0;
    $cantExped=$expedHelper->cantExpedByRadic($radicado);
    if(isset($cantExped[0])){
        if($cantExped[0]['EXPEDIENTES']>0){
            $tieneexpedientes=$cantExped[0]['EXPEDIENTES'];
        }
    }

    return $tieneexpedientes;
}

function escluirRadExped(){

    //excluye un expediente de un radicado
    $json = array();
    try {

        require_once '../../config.php';
        $ruta_raiz = "../../";
        if (isset($db)) unset($db);
        include_once("$ruta_raiz/include/db/ConnectionHandlerNew.php");
        include_once "$ruta_raiz/include/tx/Historico.php";

        $db = new ConnectionHandler("$ruta_raiz");
        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        $borra = new \App\Helpers\Borradores($db);
        $expedHelper = new \App\Helpers\Expedientes($db);
        $reportePagoHelper  = new \App\Helpers\ReportesPagos($db);
        $radicado=$_POST['radicado'];
        $expediente=$_POST['expediente'];
        $tieneexpedientes=0;


        if (isset($_POST['radicado']) && $_POST['radicado'] != "" &&
            isset($_POST['expediente']) && $_POST['expediente'] != "") {

            $response = $expedHelper->escluirRadExped($radicado,$expediente);
            $json['infexpediente'] = $response;


            $tieneexpedientes=tieneExpedientes($expedHelper,$radicado);
            //hago el historial del expediente
            $radicadosarray=array();
            $Historico = new Historico( $db );
            $radicadosarray[0] = $radicado;
            $numExpediente = $expediente;
            $tipoTx = 52;
            $codusuario = $_SESSION["codusuario"];
            $observa = "Radicado Excluido del Expediente";
            $dependencia = $_SESSION["dependencia"];
            $Historico->insertarHistoricoExp( $numExpediente, $radicadosarray, $dependencia, $codusuario, $observa, $tipoTx, 0 );

            $json['success'] = "Excluido con éxito";
            $json['tieneexpedientes'] =$tieneexpedientes;

            $get = $borra->getExpedNotDeleted($radicado);
            $tipologia=false;
            $misexped = armarMisExpedientesEntrada($get, $borra, $tipologia);
            $json['misexped'] = $misexped;

        } else {
            $json['error'] = "Debe ingresar datos válidos";
        }

    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
        $response = "ERROR";
        $json['error'] = "Error ajax";
    }
    echo json_encode($json);
}

function armarMisExpedientesEntrada($get, $borra, $tipologia = false)
{ //arma el arreglo de los expedientes
    $expedientes = array();
    $json = array();
    if (count($get) > 0) {


        //recorro los expedientes a este borrador
        foreach ($get as $row) {


            $expedientes = array();
            //busco los datos del expediente
            $expedientes = $borra->get_expedientes(false, $row['SGD_EXP_NUMERO'], false,
                false, false, false,false,1000,false);
            $expedientes[0]['incluiExpe'] = "incluido";

            $tiplog = "";
            $tipologiacodigo = "";

            if ($tipologia != false) {
                $tipologiasearch = $borra->tipologiasByCodigo($tipologia);
                $tiplog = $tipologiasearch[0]['SGD_TPR_DESCRIP'];
                $tipologiacodigo = $tipologia;
            }
            $expedientes[0]['tipologia'] = $tiplog;
            $expedientes[0]['tipologiacodigo'] = $tipologiacodigo;

            $json[] = $expedientes[0];

        }
    }

    return $json;
}

function tienePermExcluir($post)
{

    //trae la informacion del expediente enviado
    $json = array();
    try {
        require_once '../../config.php';

        $ruta_raiz = "../../";
        if (isset($db)) unset($db);
        include_once("$ruta_raiz/include/db/ConnectionHandlerNew.php");
        $db = new ConnectionHandler("$ruta_raiz");
        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);

        $expedHelper = new \App\Helpers\Expedientes($db);
        $radicado=$_POST['radicado'];
        $expediente=$_POST['expediente'];

        if (isset($_POST['radicado']) && $_POST['radicado'] != "" &&
            isset($_POST['expediente']) && $_POST['expediente'] != "") {

            $response = $expedHelper->tienePermExcluir($radicado,$expediente);
            $json['success'] = $response;

        } else {
            $json['error'] = "Debe ingresar datos válidos";
        }

    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
        $response = "ERROR";
        $json['error'] = "Error ajax";
    }
    echo json_encode($json);
}


function getRadicadosXexped($post)
{

    $json = array();
    try {
        require_once '../../config.php';

        $ruta_raiz = "../../";
        if (isset($db)) unset($db);
        include_once("$ruta_raiz/include/db/ConnectionHandlerNew.php");
        $db = new ConnectionHandler("$ruta_raiz");
        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);

        $expedHelper = new \App\Helpers\Expedientes($db);
        $radicados=$_POST['radicados'];
        $expediente=$_POST['expediente'];

        if(count($radicados)>0){

            $condicion="";
            $cont=0;
            foreach($radicados as $row){
                $condicion.="'".$row['radicado']."'";
                $cont++;
                if(count($radicados)>$cont){
                    $condicion.=",";
                }
            }
            $response = $expedHelper->getRadicadosXexped($condicion,$expediente);

            $json['success'] = $response;

        }else{
            $json['error'] = "Debe ingresar al menos un radicado";
        }


    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
        $response = "ERROR";
        $json['error'] = "Error ajax";
    }
    echo json_encode($json);
}



function getSecuenciaExped(){
    $json = array();
    try {
        require_once '../../config.php';

        $ruta_raiz = "../../";
        if (isset($db)) unset($db);
        include_once("$ruta_raiz/include/db/ConnectionHandlerNew.php");
        $db = new ConnectionHandler("$ruta_raiz");
        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        $expedHelper = new \App\Helpers\Expedientes($db);

        require_once("$ruta_raiz/include/tx/Expediente.php");
        $objExpediente = new Expediente($db);

        if(isset($_POST['dependencia']) && $_POST['dependencia']!=''
        && isset($_POST['serie']) && $_POST['serie']!='' &&
            isset($_POST['subserie']) && $_POST['subserie']!='' &&
            isset($_POST['anio']) && $_POST['anio']!=''){

            $secuencia = $objExpediente->secExpediente($_POST['dependencia'],$_POST['serie'],$_POST['subserie'],$_POST['anio']);
            $json['secuencia'] =str_pad($secuencia, 5, "0",STR_PAD_LEFT);

            $json['responsables']=$expedHelper->getResponsables($_POST['dependencia']);
        }else {
            $json['error'] = "Debe ingresar al todos los datos";
        }



    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
        $response = "ERROR";
        $json['error'] = "Error ajax";
    }
    echo json_encode($json);
}


function getResponsables(){
    $json = array();
    try {

        require_once '../../config.php';

        $ruta_raiz = "../../";
        if (isset($db)) unset($db);
        include_once("$ruta_raiz/include/db/ConnectionHandlerNew.php");
        $db = new ConnectionHandler("$ruta_raiz");
        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        $expedHelper = new \App\Helpers\Expedientes($db);
       $responsables=$expedHelper->getResponsables($_GET['dependencia'],$_GET['search']);

       if(count($responsables)>0){
           $cont=0;
           foreach ($responsables as $row){
               $responsables[$cont]['id']=$row['ID'];
               $responsables[$cont]['text']=$row['USUA_NOMB'];
               $cont++;
           }
       }
        $json=$responsables;

    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
        $response = "ERROR";
        $json['error'] = "Error ajax";
    }
    echo json_encode($json);
}

function getValidaExisteTitulo($expedHelper,$titulo){
    /**
     * lo siguiente elimina mas de dos espacios sen blanco
     */
    $titulo= preg_replace('/( ){2,}/u',' ',$titulo);
    $existe=$expedHelper->getTituloExpediente(trim($titulo));
    return $existe;
}

function validaExisteTitulo(){
    try {

        require_once '../../config.php';
        $ruta_raiz = "../../";
        if (isset($db)) unset($db);
        include_once("$ruta_raiz/include/db/ConnectionHandlerNew.php");
        $db = new ConnectionHandler("$ruta_raiz");
        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        $expedHelper = new \App\Helpers\Expedientes($db);

        $existe=getValidaExisteTitulo($expedHelper,$_POST['titulo']);
        if(count($existe)>0){
            $existe=trim($existe[0]['SGD_EXP_NUMERO']);
        }
        $json['existe']=$existe;

    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
        $response = "ERROR";
        $json['error'] = "Error ajax";
    }
    echo json_encode($json);
}


function accionCrearExped(){

    try {

        require_once '../../config.php';

        $ruta_raiz = "../../";
        if (isset($db)) unset($db);
        include_once("$ruta_raiz/include/db/ConnectionHandlerNew.php");
        $db = new ConnectionHandler("$ruta_raiz");
        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        $expedHelper = new \App\Helpers\Expedientes($db);
        $existe=getValidaExisteTitulo($expedHelper,$_POST['titulo']);
        if(count($existe)>0){
            $json['error']="El Título ya existe! ".trim($existe[0]['SGD_EXP_NUMERO']);
            echo json_encode($json);
            exit();
        }

        $borraHelper = new \App\Helpers\Borradores($db);

        require_once("$ruta_raiz/include/tx/Expediente.php");
        $objExpediente = new Expediente($db);

        $expedientetemp=trim($_POST['expediente']);
        $existe=$objExpediente->existeExpediente($expedientetemp);

        $responsable=trim($_POST['responsable']);
        $buscarresponsable=$borraHelper->getUsuarioById($responsable);

        if(count($buscarresponsable)>0){
            if($existe==0){

                $arrParametro=array('1'=>trim($_POST['titulo']),'2'=>trim($_POST['descripcion']),'3'=>trim($_POST['otrosmedatatos']));
                //no existe el expediente
                $crear=$objExpediente->crearExpediente($expedientetemp,NULL,$_POST['dependencia'],$_SESSION['codusuario'],
                    $_SESSION['usua_doc'],$buscarresponsable[0]['USUA_DOC']
                    ,trim($_POST['serie']),trim($_POST['subserie']),null,null,null,$arrParametro);


                if($crear==0){
                    $json['error']="No se ha podido insertar el Expediente";
                }else{
                    $json['success']=$crear;


                    $menor_dos_anios=false;
                    $soloabiertos=false;

                    $expedientes = $borraHelper->get_expedientes(false,$crear,
                        false, false, false, false, $menor_dos_anios,1,$soloabiertos);
                    $json['expediente']=$expedientes;
                }

            }else{
                $json['error']='El expediente existe';
            }

        }else{
            $json['error']='El responsable no existe';
        }



    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
        $response = "ERROR";
        $json['error'] = "Error ajax";
    }
    echo json_encode($json);
}


if (!isset($_SESSION["usuario_id"])) {

    $json = array(
        'error' => 'expiro'
    );
    echo json_encode($json);
    exit;
}




if (isset($_POST['function_call']) and $_POST['function_call'] == "getInfExpediente") {

    getInfExpediente($_POST);
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "tablaprinciexped") {

    tablaprinciexped($_POST);
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "tablahistorial") {

    tablahistorial($_POST);
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "tablaradselec") {

    tablaradselec($_POST);
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "tienePermExcluir") {

    tienePermExcluir($_POST);
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "escluirRadExped") {

    escluirRadExped($_POST);
}


if (isset($_POST['function_call']) and $_POST['function_call'] == "getTieneExpedientes") {

    getTieneExpedientes($_POST);
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "getAnexosRad") {

    getAnexosRad($_POST);
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "getRadicadosXexped") {

    getRadicadosXexped($_POST);
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "getSecuenciaExped") {

    getSecuenciaExped($_POST);
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "validaExisteTitulo") {

    validaExisteTitulo($_POST);
}


if (isset($_GET['function_call']) and $_GET['function_call'] == "getResponsables") {

    getResponsables($_POST);
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "accionCrearExped") {

    accionCrearExped($_POST);
}



?>

