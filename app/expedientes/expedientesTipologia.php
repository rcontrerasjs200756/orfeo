<?php
session_start();

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Philo\Blade\Blade;

require '../../vendor/autoload.php';
require_once '../../config.php';

$ruta_raiz = "../../";
if (isset($db)) unset($db);
include_once("$ruta_raiz/include/db/ConnectionHandlerNew.php");
$db = new ConnectionHandler("$ruta_raiz");

ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
error_reporting(E_ALL);

$views = $ABSOL_PATH.'app/views';
$cache = $ABSOL_PATH.'app/cache';

//Clearing the views cache
\App\Helpers\MiscFunctions::clear_view_cache();

$blade          = new Blade($views, $cache);


if(!isset($_SESSION['usuario_id'])) { header('Location: '.$ruta_raiz.'login.'.php);}

echo $blade->view()->make('expedientes.consultaTipologia', compact( 'include_path'
))->render();



