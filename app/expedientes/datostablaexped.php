<?php

use App\Helpers\PDOconfig;
use Aura\SqlQuery\QueryFactory;
use Philo\Blade\Blade;

require '../../vendor/autoload.php';

$array = array();

try {
    ini_set('display_errors', 0);
    ini_set('display_startup_errors', 0);
    error_reporting(E_ALL);

    session_start();
    require_once '../../config.php';
    $views = $ABSOL_PATH . 'app/views';
    $cache = $ABSOL_PATH . 'app/cache';
    $ruta_raiz = "../../";
    if (isset($db)) unset($db);
    include_once("$ruta_raiz/include/db/ConnectionHandlerNew.php");
    $db = new ConnectionHandler("$ruta_raiz");

    //Clearing the views cache
    \App\Helpers\MiscFunctions::clear_view_cache();


    $expedieteHelper = new \App\Helpers\Expedientes();
    include_once "$ruta_raiz/tx/verLinkArchivo.php";
    $verLinkArchivo = new verLinkArchivo($db); //para validar si puede abrir los archivos de radicado

    $data = $_GET['data'];
    $expediente = $data['expediente'];
    $radicado=$data['radicado'];
    $datajson = array();
    // Pagination Result
    $array = array();
    $start = 0;
    $limit = false;
    $draw = $_GET['draw'];

    if (!empty($draw)) {
        $start = $_GET['start'];
        $limit = $_GET['length'];
        if ($limit == '-1') {
            $limit = false;
        }
    }
    $search = $_GET['search'];
    $buscar = $search['value'];
    $total = $expedieteHelper->tablaprinciexped($expediente,
        false, false, $buscar);


    $cont = 0;
    $detailarray = array();

    $datos = $expedieteHelper->tablaprinciexped($expediente, $start, $limit, $buscar);
    $contadordetail = 0;
    if (count($datos) > 0) {
        foreach ($datos as $row) {


            $CONTRATISTA_json = array();
            $con = 0;


            //ultimo td que contiene el boton
            $CONTRATISTA_json[$con] = '<a class="detail-icon" href="javascript:"><i class="glyphicon glyphicon-plus icon-plus"></i></a>';
            $con++;


            $resulVali = $verLinkArchivo->valPermisoRadi( $row["RADICADO"]);
            $valImg = $resulVali['verImg'];

            $CONTRATISTA_json[$con]="";
            if (strlen($row["RADI_PATH"]) > 0 && file_exists('../../bodega' . $row["RADI_PATH"])==true) {

                //con esto valido si tiene permiso de ir a ver el radicado
                if($valImg == "SI") {
                    $CONTRATISTA_json[$con] .= '<a href="' . $_SESSION['base_url'] . '/bodega' . $row["RADI_PATH"] . '" target="_blank">
                <span class="btn label label-sm label-info blue">
                ' . $row["RADICADO"] . '</span></a>';
                }else{
                    $CONTRATISTA_json[$con] .= '<a href="javascript:noPermiso()">
                    <span class="btn label label-sm label-info blue">' . $row["RADICADO"] . '</span></a>';
                }


            } else {
                $CONTRATISTA_json[$con] .= "<span class='btn label label-sm label-info blue'>" . $row["RADICADO"] . "</span>";
            }

            //esto se hace para marcar en la vista, el radicado actual seleccionado
            if($radicado==$row["RADICADO"]){
                $CONTRATISTA_json[$con] .= "<span style='color:red'>*</span>";
            }
            $con++;

            if($valImg == "SI") {
                $CONTRATISTA_json[$con] = "<a target='_blank'
            href='" . $_SESSION['base_url'] . "/verradicado.php?verrad=" . $row["RADICADO"] . "&" . session_name() . "=" . trim(session_id()) . "​&menu_ver_tmp=3' class=''> <span class='btn label label-sm label-info blue' >" . $row["ASUNTO"] . "</span></a>";

            }else{
                $CONTRATISTA_json[$con] = "<a 
            href='javascript:noPermiso()' class=''> <span class='btn label label-sm label-info blue' >" . $row["ASUNTO"] . "</span></a>";

            }
            $con++;

            if($valImg == "SI") {
                $CONTRATISTA_json[$con] = "<a target='_blank'
            href='" . $_SESSION['base_url'] . "/verradicado.php?verrad=" . $row["RADICADO"] . "&" . session_name() . "=" . trim(session_id()) . "​&menu_ver_tmp=3' class=''>" . date("d-m-Y H:i:s", strtotime($row["FECHA_RADICADO"])) . "</a>";
            }else{
                $CONTRATISTA_json[$con] = "<a 
            href='javascript:noPermiso()' class=''>" . date("d-m-Y H:i:s", strtotime($row["FECHA_RADICADO"])) . "</a>";

            }
            $con++;

            $CONTRATISTA_json[$con] = $row["DEPENDENCIA_ACTUAL"];
            $con++;

            $CONTRATISTA_json[$con] = $row["RADICADO"];


            /////
            $datajson[] = $CONTRATISTA_json;

        }


    }

    $array['data'] = $datajson;
    $array['draw'] = $draw;//esto debe venir por post
    $array['recordsTotal'] = count($total);
    $array['recordsFiltered'] = count($total);

} catch (Exception $e) {
    echo "Error: " . $e->getMessage();
    $response = "ERROR";
    $array['error'] = true;
}
echo json_encode($array);


?>

