<?php
/*
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
*/
use Philo\Blade\Blade;

require '../../vendor/autoload.php';
require_once '../../config.php';

$views = $ABSOL_PATH.'app/views';
$cache = $ABSOL_PATH.'app/cache';

//Clearing the views cache
\App\Helpers\MiscFunctions::clear_view_cache();

$delivered = \App\Helpers\MiscFunctions::getDelivered();

$blade = new Blade($views, $cache);
echo $blade->view()->make(
    'expedientes.sinExpRad',
    compact(
        'delivered',
        'include_path')
)->render();

?>