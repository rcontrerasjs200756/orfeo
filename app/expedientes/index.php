<?php
/**
 * el archivo que llama a este index.php, es el archivo llamado tohtml.inc.php ubicado dentro de la carpeta adodb
 */


use Philo\Blade\Blade;

require '../../vendor/autoload.php';
require_once '../../config.php';

$ruta_raiz = "../../";
if (isset($db)) unset($db);
include_once("$ruta_raiz/include/db/ConnectionHandlerNew.php");
$db = new ConnectionHandler("$ruta_raiz");

ini_set('display_errors', 0);
       ini_set('display_startup_errors', 0);
       error_reporting(E_ALL);

$views = $ABSOL_PATH.'app/views';
$cache = $ABSOL_PATH.'app/cache';

//Clearing the views cache
\App\Helpers\MiscFunctions::clear_view_cache();

$blade          = new Blade($views, $cache);
$expedHelper  = new \App\Helpers\Expedientes($db);
$reportePagoHelper  = new \App\Helpers\ReportesPagos($db);

$msg="";
session_start();

$tieneexpedientes=0;
$radicado=$_GET["radicado"];
$expedientes=array();
$inforadicado=array();
$validarexcluir=$_SESSION['VALIDAEXCLUYE_EXPED'];
$expedmasreciente="";
$expedientebyget="";//por si llega algun expediente para buscar la informacion al cargar la vista
if(isset($_GET["expediente"]) && $_GET["expediente"]!=""){
    $expedientebyget=$_GET["expediente"];
}
if(isset($_GET["radicado"]) && $_GET["radicado"]!=""){

    $cantExped=$expedHelper->cantExpedByRadic($_GET["radicado"]);
    $expedientes=$expedHelper->getExpedByRad($_GET["radicado"]);
    $inforadicado=$reportePagoHelper->get_rad_info($radicado);
    $tieneexpedientes=count($expedientes);
   if(count($expedientes)>0){
        $expedmasreciente=$expedientes[0]['EXPEDIENTE'];
    }

}else{
    $msg="Debe ingresar un radicado";
}

echo $blade->view()->make('expedientes.index', compact(
    'radicado','tieneexpedientes',
    'msg','include_path','expedientes','inforadicado','validarexcluir','expedmasreciente',
    'expedientebyget'

))->render();



?>