CREATE TABLE public.borrador_firmantes
(
    id_borrador_firmante serial NOT NULL,
    id_borrador integer NOT NULL,
    id_usuario_firmante integer,
    firmante_cargo character varying(50),
    firmante_principal integer,
    PRIMARY KEY (id_borrador_firmante),
FOREIGN KEY (id_borrador) REFERENCES borradores(id_borrador)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.borrador_firmantes
    OWNER to uorfeo;


ALTER TABLE public.borrador_firmantes
    ADD COLUMN fecha_registro timestamp without time zone;

COMMENT ON COLUMN public.borrador_firmantes.fecha_registro
    IS 'fecha en la que se registra la fila';


ALTER TABLE public.borradores
    ADD COLUMN tipo_borrador_dest character varying(50);

COMMENT ON COLUMN public.borradores.tipo_borrador_dest
    IS 'campo que indica una constante, al momento de crear el borrador, las opciones del boton Crear Borrador';

update borradores set tipo_borrador_dest='INTERNA_COMUNICACION' where tipo_radicado=3;
update borradores set tipo_borrador_dest='SALIDA_OFICIO' where tipo_radicado=1;



////
16/04/2019

ALTER TABLE public.seguridad DROP CONSTRAINT pk_seguridad_radi_nume_radi;

ALTER TABLE public.seguridad
    ADD COLUMN tipo_usu_permitido character varying;

COMMENT ON COLUMN public.seguridad.tipo_usu_permitido
    IS 'define si es un usuario de la tabla usuario o una dependencia, debe contener dos casos: USUARIO o DEPENDENCIA';

/////
23/04/2019

ALTER TABLE public.borradores
    ADD COLUMN folios_comunicacion integer;

ALTER TABLE public.borradores
    ADD COLUMN folios_anexos integer;

ALTER TABLE public.borradores
    ADD COLUMN descripcion_anexos character varying;


/////////
11/05/2019

ALTER TABLE public.borrador_estados
    ADD COLUMN nombre_transaccion character varying;

COMMENT ON COLUMN public.borrador_estados.nombre_transaccion
    IS 'nombre de la transaccion, si es una transaccion y no un estado';

ALTER TABLE public.borrador_estados
    ADD COLUMN sgd_ttr_codigo integer;

COMMENT ON COLUMN public.borrador_estados.sgd_ttr_codigo
    IS 'codigo que tendra al momento de insertar en hist_events';

ALTER TABLE public.borrador_estados
    ADD COLUMN tipo character varying;

COMMENT ON COLUMN public.borrador_estados.tipo
    IS 'e = estado, t = transaccion';

insert into borrador_estados (nombre_estado,nombre_transaccion,sgd_ttr_codigo,tipo)
values (null,'Enviado', 82,'t');

update borrador_estados set sgd_ttr_codigo=80,nombre_transaccion='Proyectado' where nombre_estado='Sin Aprobar';
update borrador_estados set sgd_ttr_codigo=84 where nombre_estado='Aprobado';
update borrador_estados set sgd_ttr_codigo=81 where nombre_estado='Revisado';
update borrador_estados set sgd_ttr_codigo=83 where nombre_estado='Devuelto';

UPDATE sgd_ttr_transaccion SET sgd_ttr_descrip='Borrador Proyectado' WHERE sgd_ttr_codigo=80;
INSERT INTO sgd_ttr_transaccion(sgd_ttr_codigo, sgd_ttr_descrip) VALUES (81, 'Borrador Revisado');
INSERT INTO sgd_ttr_transaccion(sgd_ttr_codigo, sgd_ttr_descrip) VALUES (82, 'Borrador Enviado');
INSERT INTO sgd_ttr_transaccion(sgd_ttr_codigo, sgd_ttr_descrip) VALUES (83, 'Borrador Devuelto');
INSERT INTO sgd_ttr_transaccion(sgd_ttr_codigo, sgd_ttr_descrip) VALUES (84, 'Borrador Aprobado');

Update hist_eventos set sgd_ttr_codigo=84
 Where sgd_ttr_codigo=80 And hist_obse like 'Borrador aprobado por%'

UPDATE borrador_historicos SET sgd_ttr_codigo=84 WHERE id_estado=2;
UPDATE borrador_historicos SET sgd_ttr_codigo=81 WHERE id_estado=3;
UPDATE borrador_historicos SET sgd_ttr_codigo=83 WHERE id_estado=4;
UPDATE borrador_historicos SET sgd_ttr_codigo=82 WHERE id_estado=5 or id_estado=0;

/////
31/05/2019
CREATE TABLE public.titulo_tratamiento
(
    id serial NOT NULL,
    titulo character varying,
    abreviatura character varying,
    activo integer,
    orden integer,
    PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.titulo_tratamiento
    OWNER to uorfeo;
COMMENT ON TABLE public.titulo_tratamiento
    IS 'tabla para guadar titulo, tratamiento o vocativo';

insert into titulo_tratamiento (titulo,abreviatura,activo,orden) values('Señor','Sr.',1,1);
insert into titulo_tratamiento (titulo,abreviatura,activo,orden) values('Señora','Sra.',1,2);
insert into titulo_tratamiento (titulo,abreviatura,activo,orden) values('Señores','Sres.',1,3);
insert into titulo_tratamiento (titulo,abreviatura,activo,orden) values('Doctor','Dr.',1,4);
insert into titulo_tratamiento (titulo,abreviatura,activo,orden) values('Doctora','Dra.',1,5);
insert into titulo_tratamiento (titulo,abreviatura,activo,orden) values('Ingeniero','Ing.',1,6);
insert into titulo_tratamiento (titulo,abreviatura,activo,orden) values('Ingeniera','Ing.',1,7);
insert into titulo_tratamiento (titulo,abreviatura,activo,orden) values('Arquitecto','Arq.',1,8);
insert into titulo_tratamiento (titulo,abreviatura,activo,orden) values('Arquitecta','Arq.',1,9);
insert into titulo_tratamiento (titulo,abreviatura,activo,orden) values('Licenciado','Lic.',1,10);
insert into titulo_tratamiento (titulo,abreviatura,activo,orden) values('Maestro','Mtro.',1,11);


CREATE TABLE public.configuraciones
(
    id serial NOT NULL,
    tipo character varying,
    modulo character varying,
    nombre_constante character varying,
    nombre_funcion text,
    valor_tipo character varying,
    opciones text,
    valor text,
    activo integer,
    descripcion_ayuda text,
    fecha_actualizacion timestamp without time zone,
    actualizado_por integer,
    valor_anterior text,
    PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.configuraciones
    OWNER to uorfeo;
COMMENT ON TABLE public.configuraciones
    IS 'tabla para las configuraciones';

insert into configuraciones (tipo,modulo,nombre_constante,nombre_funcion,valor_tipo,opciones,valor,visible,descripcion_ayuda,
fecha_actualizacion,actualizado_por,valor_anterior)
values ('f','BORRADORES','FUNCTION_BORRADORES','Habilitar funcionalidad de borradores?','lista_desplegable',
	   '{"0":"SI","1":"NO"}','NO',0,
	   'Si esta funcionalidad está deshabilitada, No se muestra el menú Borradores y las demás configuraciones de borradores no deberían mostrarse en configuración.',null,null,null),
	   ('p','BORRADORES','TITULO_MEMO','Titulo Oficios Internos','texto',null,'MEMORANDO',1,
	   'Titulo para encabezado oficios internos, eje: MEMORANDO, COMUNICACIÓN INTERNA',null,null,null),
	   ('p','BORRADORES','APROBAR_OBLIGATORIO_RADICAR','Borrador requiere aprobación para radicar','lista_desplegable',
	   '{"0":"SI","1":"NO"}','SI',1,
	   null,null,null,null),
	   ('p','BORRADORES','EXPEDIENTE_OBLIGATORIO_RADICAR','Expediente obligatorio al radicar','lista_desplegable',
	   '{"0":"SI","1":"NO"}','SI',1,
	   'Obligar a incluir borrador en expediente para poder radicar. (SI o NO)',null,null,null),
	   ('p','BORRADORES','EXPEDIENTE_ENVIAR','Sugerir la inclusión en expediente al enviar borrador','lista_desplegable',
	   '{"0":"SI","1":"NO"}','SI',1,
	   'Habilita o deshabilita la alerta para enviar borrador sin expediente.',null,null,null),
	   ('c','BORRADORES','VALIDAEXCLUYE_EXPED','Permitir excluir radicados desde la ventana expediente','lista_desplegable',
	   '{"0":"SI","1":"NO"}','SI',1,
	   'Le permite a un usuario excluir un radicado desde el modal de incluir en expediente. Si NO entonces sólo se puede excluir desde la pestaña expediente.',null,null,null),
	   ('p','BORRADORES','EDITOR_FUENTE_TIPO','Nombre de la fuente o tipo de letra del editor web.','lista_desplegable',
	   '{"0":"Arial","1":"Times New Roman"}','Arial',1,
	   'Nombre de la fuente o tipo de letra que usará por defecto el editor web.',null,null,null),
	   ('p','BORRADORES','EDITOR_FUENTE_TAMANIO','Tamaño de la fuente del editor web (10, 11, 12)','lista_desplegable',
	   '{"0":8,"1":10,"2":11,"3":12,"4":14}','12',1,
	   'Tamaño de la fuente que usará por defecto el editor web. Tamaños permitidos (8, 10, 11, 12, 14)',null,null,null),
	   ('p','BORRADORES','ASUNTO_MINLENGTH','Tamaño mínimo en el asunto','lista_desplegable',
	   '{"0":12,"1":25,"2":35,"3":45,"4":75}','35',1,
	   'Número de caracteres mínimos obligatorios, al escribir un asunto.',null,null,null),
	    ('p','BORRADORES','COMENTARIOS_MINLENGTH','Tamaño mínimo en los comentarios','lista_desplegable',
	   '{"0":5,"1":10,"2":20,"3":30}','10',1,
	   'Número de caracteres mínimos obligatorios, al escribir un comentario al enviar, devolver, revisar, no aprobar.',null,null,null),
	    ('c','BORRADORES','EDITOR_KEY','Llave o licencia del editor web Froala','texto',
	   null,null,1,
	   'Key Froala',null,null,null),
	   ('p','BORRADORES','GUARDAR_BORRADOR','Guardar borrador automáticamente cada # minutos','lista_desplegable',
	   '{"0":1,"1":3,"2":5,"3":"No guardar automaticamente"}','1',1,
	   'Número de minutos transcurridos para guardar automáticamente un borrador.',null,null,null),
	   ('p','BORRADORES','ELIMINAR_BORRADOR_AUTO','Eliminar borradores después de # días sin radicar','lista_desplegable',
	   '{"0":15,"1":30,"2":60,"3":90}','60',1,
	   'Después de # de días sin radicar un borrador, se eliminará. Se alertará 5 días antes.',null,null,null),
	   ('p','BORRADORES','EXPEDIENTE_CREAR','Método para solicitar creación de expedientes','lista_desplegable',
	   '{"0":"eMail","1":"Enlace soporte","2":"Formulario Orfeo"}','Formulario Orfeo',1,
	   'Método para solicitar creación de expedientes.',null,null,null),
	   ('p','BORRADORES','VALOR_EXPEDIENTE_CREAR',
	   'Valor para solicitar creación de expedientes','texto',
	   null,'eMail',1,
	   'Valor dependiendo de lo que seleccione en EXPEDIENTE_CREAR, para solicitar creación de expedientes.',null,null,null),
	   ('p','BORRADORES','MODIFICAR_AL_APROBAR','Permitir modificaciones a un borrador después de Aprobar?','lista_desplegable',
	   '{"0":"SI","1":"NO"}','SI',1,
	   'No permitir modificar el contenido de un documento (Destinatario, Asunto, editor web, plantilla, asociados, anexos). Puede agregar asociados, anexos, revisar, expediente, enviar, radicar.',null,null,null),
	   ('p','BORRADORES','LIMIT_BUSQ_AVANZ_EXPED','Límite de resultados en búsqueda de expedientes','lista_desplegable',
	   '{"0":100,"1":1000,"2":5000,"3":10000,"4":100000','5000',1,
	   'No permitir modificar el contenido de un documento (Destinatario, Asunto, editor web, plantilla, asociados, anexos). Puede agregar asociados, anexos, revisar, expediente, enviar, radicar.',null,null,null),
	   ('p','SOPORTE','MAIL_ADMIN_TECNICO','Correo electrónico administrador técnico','mail',
	   null,'idexan@gmail.com',1,
	   'Correo electrónico del ingeniero o superadministrador que hace mantenimiento a la plataforma Orfeo.',null,null,null),
	    ('p','SOPORTE','MAIL_ADMIN_FUNCIONAL','Correo electrónico administrador funcional','mail',
	   null,'idexan@gmail.com',1,
	   'Correo electrónico administrador funcional de la herramienta.',null,null,null),
	    ('p','SOPORTE','MAIL_SOPORTE_FUNCIONAL','Correo electrónico soporte funcional','mail',
	   null,'idexan@gmail.com',1,
	   'Correo electrónico para solicitudes de soporte funcional de la herramienta.',null,null,null),
	   ('f','ANULACION','FUNCTION_ANULA_AUTO','Habilitar funcionalidad de anulación automática','lista_desplegable',
	   '{"0":"SI","1":"NO"}','NO',0,
	   'Si esta funcionalidad está deshabilitada, las demás configuraciones de anulación no deberían aplicarse.',null,null,null),
	   ('c','ANULACION','ANULA_DIAS_TRAMITE','Número de días hábiles para anular automáticamente un radicado sin tramitar','lista_desplegable',
	   '{"0":30"1":60,"2":90}','90',0,
	   'Cuando se genera un radicado y no se tramita completamente, se anulará automáticamente, según los días hábiles seleccionados.  Se alertará 3 días antes.',null,null,null),
	   ('c','ANULACION','ANULA_DIAS_GESTION','Número de días hábiles para anular automáticamente un radicado que no se le ha realizado nada.','lista_desplegable',
	   '{"0":10"1":15,"2":25}','25',0,
	   'Cuando se genera un radicado y no se gestiona, no se le hace nada, se anulará automáticamente, según los días hábiles seleccionados. Se alertará 3 días antes.',null,null,null),
	   ('c','ANULACION','ANULACIONES_EXPEDIENTE','Número de expediente anual, en el cual se incluirán los radicados anulados automáticamente.','texto',
	   null,'2019110020000100004E',0,
	   'Cada año se debe indicar el número de expediente, en el cual se incluirán los radicados anulados automáticamente.',null,null,null),
	   ('c','ANULACION','CRON_FECHA_INICIAL','Fecha a partir de la cual se empezará a anular','fecha',
	   null,null,0,
	   'Fecha a partir de la cual se empezará a anular los expedientes',null,null,null),
	   ('c','ANULACION','ANULA_ACTO_ADMTVO','Mostrar activo administrativo de anulación automática','lista_desplegable',
	   '{"0":"SI","1":"NO"}','NO',1,
	   'Muestra en la parte inferior de la notificación de anulación automática, un enlace a un archivo llamado acto_administrativo_anulados.pdf
(Opción para hacer subir dicho archivo).',null,null,null);


CREATE TABLE public.perfil_has_configuracion
(
    id serial NOT NULL,
    configuracion_id integer,
    perfil character varying,
    fecha_creacion timestamp without time zone,
    usuario_registra integer,
    PRIMARY KEY (id),
    CONSTRAINT fk_configuracion_id FOREIGN KEY (configuracion_id)
        REFERENCES public.configuraciones (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.perfil_has_configuracion
    OWNER to uorfeo;
COMMENT ON TABLE public.perfil_has_configuracion
    IS 'Tabla que indica el perfil que tendra acceso a ciertas configuraciones especificas';


CREATE TABLE public.historial_configuracion
(
    id serial NOT NULL,
    usuario_id integer NOT NULL,
    fecha timestamp without time zone,
    configuracion_id integer NOT NULL,
    valor character varying,
    valor_anterior character varying,
    CONSTRAINT pk_historial_id PRIMARY KEY (id),
    CONSTRAINT fk_historial_configuracion_id FOREIGN KEY (configuracion_id)
        REFERENCES public.configuraciones (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.historial_configuracion
    OWNER to uorfeo;
COMMENT ON TABLE public.historial_configuracion
    IS 'muestra el historial de las configuraciones';


    //15/06/2019
    insert into borrador_estados (nombre_estado,nombre_transaccion,sgd_ttr_codigo,tipo)
values ('No Aprobado',null, 86,null);
insert into sgd_ttr_transaccion  (sgd_ttr_codigo,sgd_ttr_descrip)values (86,'No Aprobado');

//18/06/2019
ALTER TABLE public.configuraciones
    RENAME activo TO visible;


    //20/06/2019

    CREATE TABLE public.config_has_perfil
(
    id serial NOT NULL,
    config_id integer NOT NULL,
    perfil character varying,
    PRIMARY KEY (id),
    FOREIGN KEY (config_id)
        REFERENCES public.configuraciones (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.config_has_perfil
    OWNER to uorfeo;


    //11/07/2019

    update configuraciones set opciones='{"0":30,"1":60,"2":90}', valor=90 where nombre_constante='ANULA_DIAS_TRAMITE';
update configuraciones set opciones='{"0":10,"1":15,"2":25}' valor=25 where nombre_constante='ANULA_DIAS_GESTION';
update configuraciones set valor_tipo='lista_desplegable', opciones='{"0":"eMail","1":"Enlace soporte","2":"Formulario Orfeo"}',
valor='eMail' where nombre_constante='VALOR_EXPEDIENTE_CREAR';

//19/07/2019

insert into borrador_estados (nombre_estado,nombre_transaccion,sgd_ttr_codigo,tipo)
values ('Radicado',null, null,null);

ALTER TABLE borradores
    ADD COLUMN radi_nume numeric;

ALTER TABLE borrador_firmantes
    ADD COLUMN radi_nume numeric;

ALTER TABLE borrador_firmantes ALTER COLUMN id_borrador DROP NOT NULL;

CREATE TABLE public.document_metadata
(
    id_radicado_metadata serial NOT NULL,
    radi_nume_radi numeric,
    timestamp_m timestamp without time zone,
    anex_codigo numeric,
    radi_path_original character varying,
    radi_path_validado character varying,
    radi_path_fuente character varying,
    hash_doc_original character varying,
    hash_metadata character varying,
    clasification numeric,
    trd character varying,
    expediente character varying,
    expediente_titulo integer,
    transacciones text,
    codigoverificacion character varying,
    powered_by character varying,
    filetype character varying,
    filetype_ext character varying,
    mimetype character varying,
    type_version character varying,
    size character varying,
    pages numeric,
    PRIMARY KEY (id_radicado_metadata)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.document_metadata
    OWNER to uorfeo;
COMMENT ON TABLE public.document_metadata
    IS 'tabla que almacena el radicado que se aprueba';


CREATE TABLE public.document_authorized_by
(
    id_authorized serial NOT NULL,
    id_radicado_metadata integer NULL,
    usuario_id integer NOT NULL,
    timestamp_f timestamp without time zone,
    username character varying,
    autor character varying,
    usermail character varying,
    country character varying,
    city character varying,
    organization character varying,
    department character varying,
    nit_organization numeric,
    website character varying,
    so character varying,
    webbrowser character varying,
    ipaddress character varying,
    hash_pin character varying,
     geolocation character varying,
    PRIMARY KEY (id_authorized),
    CONSTRAINT fk_document_authorized_by_id FOREIGN KEY (id_radicado_metadata)
        REFERENCES public.document_metadata (id_radicado_metadata) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.document_authorized_by
    OWNER to uorfeo;
COMMENT ON TABLE public.document_authorized_by
    IS 'tabla que almacena los usuarios que aprueban un doc';


    //02/08/2019

insert into configuraciones (tipo,modulo,nombre_constante,nombre_funcion,valor_tipo,opciones,valor,visible,descripcion_ayuda,
fecha_actualizacion,actualizado_por,valor_anterior)
values('f','OP','FUNCTION_OP','Habilitar funcionalidad de OP','lista_desplegable',
	   '{"0":"SI","1":"NO"}','NO',0,
	   'Funcionalidad para gestión de Órdenes de Pago',null,null,null);

	   insert into configuraciones (tipo,modulo,nombre_constante,nombre_funcion,valor_tipo,opciones,valor,visible,descripcion_ayuda,
fecha_actualizacion,actualizado_por,valor_anterior)
values('f','RADICACION','FUNCTION_TEMAS_RAPIDOS','Habilitar funcionalidad de radicación de Temas Rápidos','lista_desplegable',
	   '{"0":"SI","1":"NO"}','NO',0,
	   'Habilitar funcionalidad de radicación de Temas Rápidos',null,null,null);

	    insert into configuraciones (tipo,modulo,nombre_constante,nombre_funcion,valor_tipo,opciones,valor,visible,descripcion_ayuda,
fecha_actualizacion,actualizado_por,valor_anterior)
values('f','VALIDAR','FUNCTION_VALIDAR_RADICADO','Habilitar funcionalidad de $validar radicados','lista_desplegable',
	   '{"0":"SI","1":"NO"}','NO',0,
	   'Habilitar funcionalidad de $validar radicados',null,null,null);

	   //29/08/2019
 insert into configuraciones (tipo,modulo,nombre_constante,nombre_funcion,valor_tipo,opciones,valor,visible,descripcion_ayuda,
fecha_actualizacion,actualizado_por,valor_anterior)
values('c','VALIDAR','NOMBRE_BOTON_VALIDAR','Nombre botón funcionalidad validar/aprobar radicados','lista_desplegable',
	   '{"0":"Validar","1":"Aprobar","2":"Aceptar"}','Validar',0,
	   'Nombre del botón',null,null,null);

	  ALTER TABLE document_metadata
    add COLUMN path_img_barra text;

     ALTER TABLE document_metadata
    add COLUMN status_document character varying(30);

    //06/09/2019
     insert into configuraciones (tipo,modulo,nombre_constante,nombre_funcion,valor_tipo,opciones,valor,visible,descripcion_ayuda,
fecha_actualizacion,actualizado_por,valor_anterior)
values('p','VALIDAR','METODO_VALIDACION','Método de $validación de documentos, con código de seguridad al correo electrónico?',
'lista_desplegable','{"0":"SI","1":"NO"}','SI',0,
	   'Al elegir SI, se enviará un código de seguridad al correo electrónica de la persona, para poder validar, el cual expira en la cantidad de minutos configurado.',null,null,null),
	   ('p','VALIDAR','EXPIRACION_CODIGO_VALIDACION','Número de minutos para que expire el código de seguridad para $validar documentos.','lista_desplegable',
	   '{"0":"1","1":"5","2":"15","3":"30","4":"45","5":"60","6":"90","7":"120","8":"240","9":"480","10":"600","11":"720","12":"1140"}','30',0,
	   'Tiempo en minutos de expiración del código',null,null,null),
	   ('p','VALIDAR','NOTA_VALIDAR','Nota importante! y advertencia al momento de $validar un documento','texto',
	   'La $validación del documento constituye el cierre del mismo y ya no se puede modificar','La $validación del documento constituye el cierre del mismo y ya no se puede modificar',0,
	   'Nota sobre la importancia y cuidado al $Validar un documento.',null,null,null),
	   ('p','VALIDAR','TIPOS_RADICADO_VALIDAR','Seleccionar los tipos de radicación que se pueden validar','lista_desplegable_multiple',
	   '','',0,
	   'No todos los tipos de radicación se pueden $validar, por ejemplo los de Entrada.',null,null,null);

	   //20/09/2019


ALTER TABLE anexos
	ADD COLUMN anex_regenerado int DEFAULT 0;

COMMENT ON COLUMN anexos.anex_regenerado
    IS 'Indica si el anexo ha sido re-generado, y se reinicia cada vez que modifica';

    //01/10/2019
    ALTER TABLE document_metadata
	ADD COLUMN hash_validado character varying;

//24/10/2019
update configuraciones set valor_tipo='lista_desplegable_multiple', opciones='{"0":"MEMORANDO","1":"COMUNICACIÓN INTERNA","2":"CIRCULAR","3":"Sin titulo"}'
where nombre_constante='TITULO_MEMO';

ALTER TABLE borradores
	ADD COLUMN tipo_comunicacion character varying;

//01/11/2019
	insert into borrador_estados (nombre_estado,nombre_transaccion,sgd_ttr_codigo,tipo)
values (null,'Borrador Comentado', 87,null);

//05/11/2019
insert into borrador_estados (nombre_estado,nombre_transaccion,sgd_ttr_codigo,tipo)
values ('Borrador Eliminado',null, 88,null);

/*//15/11/2019*/
ALTER TABLE borrador_firmantes
	ADD COLUMN tipo_validador character varying;

	/*18/11/2019*/
update configuraciones set  opciones='{"0":"Codigo eMail","1":"Clave única","2":"Sin código o clave"}'
where nombre_constante='METODO_VALIDACION';

ALTER TABLE usuario
	ADD COLUMN usua_pasw_doc character varying;

	ALTER TABLE usuario
	ADD COLUMN fecha_passw_doc timestamp without time zone;

/*22/11/2019*/

    insert into configuraciones (tipo,modulo,nombre_constante,nombre_funcion,valor_tipo,opciones,valor,visible,descripcion_ayuda,
fecha_actualizacion,actualizado_por,valor_anterior)
values('p','VALIDAR','LISTADO_FIRMANTES','Perfiles de usuarios validadores DE:',
'lista_desplegable','{"0":"Todos los usuarios","1":"Solo Jefes","2":"Jefes y Aprobadores","3":"Jefes, Aprobadores, Reasignadores, Supervisores y Públicos"}',
'Jefes, Aprobadores, Reasignadores, Supervisores y Públicos',0,
	   'Usuarios que aparecerán en el listado DE:(Firmantes) o validadores autorizados.',null,null,null);

	/*28/11/2019*/
	   ALTER TABLE document_authorized_by
ALTER COLUMN hash_pin TYPE text;

	/*18/11/2019*/
update configuraciones set  opciones='{"0":100,"1":1000,"2":5000,"3":10000,"4":100000}'
where nombre_constante='LIMIT_BUSQ_AVANZ_EXPED';


/*24/03/2020*/
insert into configuraciones (tipo,modulo,nombre_constante,nombre_funcion,valor_tipo,opciones,valor,visible,descripcion_ayuda,
fecha_actualizacion,actualizado_por,valor_anterior)
values('s','BORRADORES','TIPO_BORRADOR_SALIDA_OFICIO','Nombre del tipo de borrador Salida Oficio:','texto',
	   'Salida | Formato Oficio | Empresas - Ciudadanos','Salida | Formato Oficio | Empresas - Ciudadanos',0,
	   'Nombre del tipo de borrador Salida Oficio:',null,null,null);

	   insert into configuraciones (tipo,modulo,nombre_constante,nombre_funcion,valor_tipo,opciones,valor,visible,descripcion_ayuda,
fecha_actualizacion,actualizado_por,valor_anterior)
values('s','BORRADORES','TIPO_BORRADOR_INTERNA_COMUNICACION','Nombre del tipo de borrador Interna Comunicación:','texto',
	   'Interna | Formato Comunicación','Interna | Formato Comunicación',0,
	   'Nombre del tipo de borrador Interna Comunicación:',null,null,null);

	     insert into configuraciones (tipo,modulo,nombre_constante,nombre_funcion,valor_tipo,opciones,valor,visible,descripcion_ayuda,
fecha_actualizacion,actualizado_por,valor_anterior)
values('s','BORRADORES','TIPO_BORRADOR_INTERNA_CON_PLANTILLA','Nombre del tipo de borrador Interna Con Plantilla:','texto',
	   'Interna | Formato con plantilla','Interna | Formato con plantilla',0,
	   'TIPO_BORRADOR_INTERNA_CON_PLANTILLA',null,null,null);

	   insert into configuraciones (tipo,modulo,nombre_constante,nombre_funcion,valor_tipo,opciones,valor,visible,descripcion_ayuda,
fecha_actualizacion,actualizado_por,valor_anterior)
values('s','BORRADORES','TIPO_BORRADOR_INTERNA_SIN_PLANTILLA','TIPO_BORRADOR_INTERNA_SIN_PLANTILLA','texto',
	   'Interna | Sin plantilla','Interna | Sin plantilla',0,
	   'TIPO_BORRADOR_INTERNA_SIN_PLANTILLA',null,null,null);

	    insert into configuraciones (tipo,modulo,nombre_constante,nombre_funcion,valor_tipo,opciones,valor,visible,descripcion_ayuda,
fecha_actualizacion,actualizado_por,valor_anterior)
values('s','BORRADORES','TIPO_BORRADOR_REPORTE4','Nombre del tipo de borrador Entrada Reporte 4:','texto',
	   'Reporte 4','Reporte 4',0,
	   'Nombre del tipo de borrador Entrada Reporte 4:',null,null,null);

	   insert into configuraciones (tipo,modulo,nombre_constante,nombre_funcion,valor_tipo,opciones,valor,visible,descripcion_ayuda,
fecha_actualizacion,actualizado_por,valor_anterior)
values('s','BORRADORES','REPORTE4_TIPO','Tipo de radicación para los reportes tipo 4.','lista_desplegable',
	   '{"0":2,"1":3,"2":4}',4,0,
	   'Tipo de radicación para los reportes tipo 4',null,null,null);

	     insert into configuraciones (tipo,modulo,nombre_constante,nombre_funcion,valor_tipo,opciones,valor,visible,descripcion_ayuda,
fecha_actualizacion,actualizado_por,valor_anterior)
values('s','BORRADORES','REPORTE4_MODAL','Encabezado o titulo del modal para los reportes tipo 4. ','texto',
	   'RADICACIÓN REPORTE','RADICACIÓN REPORTE',0,
	   'Encabezado o titulo del modal para los reportes tipo 4.',null,null,null);

	    insert into configuraciones (tipo,modulo,nombre_constante,nombre_funcion,valor_tipo,opciones,valor,visible,descripcion_ayuda,
fecha_actualizacion,actualizado_por,valor_anterior)
values('s','BORRADORES','REPORTE4_ASUNTO','Escriba el asunto que tendrá por defecto el reporte. Las siguientes etiquetas o variables se remplazarán por el tipo de campo correspondiente.
Variable----Tipo
#AAAA      Año
#MM          Mes
#DD           Día
####          Numérico
@CCCC   Texto
@LISTA    Listado de opciones','texto',
	   'Reporte ## de ### . Tarea ### de #AAAA.','Reporte ## de ### . Tarea ### de #AAAA.',0,
	   'Escriba el asunto que tendrá por defecto el reporte. Las siguientes etiquetas o variables se remplazarán por el tipo de campo correspondiente.
Variable----Tipo
#AAAA      Año
#MM          Mes
#DD           Día
####          Numérico
@CCCC   Texto
@LISTA    Listado de opciones',null,null,null);

   insert into configuraciones (tipo,modulo,nombre_constante,nombre_funcion,valor_tipo,opciones,valor,visible,descripcion_ayuda,
fecha_actualizacion,actualizado_por,valor_anterior)
values('s','BORRADORES','FUNCTION_REPORTE4','Habilitar funcionalidad de reportes tipo 4.','lista_desplegable',
	   '{"0":"SI","1":"NO"}','NO',0,
	   'Habilitar funcionalidad de reportes tipo 4.',null,null,null);

	   /**
	   01/04/2020
	    */
	     insert into configuraciones (tipo,modulo,nombre_constante,nombre_funcion,valor_tipo,opciones,valor,
	     visible,descripcion_ayuda,fecha_actualizacion,actualizado_por,valor_anterior)
values('s','BORRADORES','FORMATO_REPORTE4','Ruta y nombre del fomato tipo 4 para descargar.','texto',
	   '/bodega/plantillas/formato4.docx','http://ruta.extena/',0,
	   'Indique el nombre del formato para el reporte 4, con su respectiva ruta. ',null,null,null);

	    insert into configuraciones (tipo,modulo,nombre_constante,nombre_funcion,valor_tipo,opciones,valor,visible,descripcion_ayuda,
fecha_actualizacion,actualizado_por,valor_anterior)
values('s','BORRADORES','REPORTE4_EXTENSIONES','Extensiones permitidas para Subir en este tipo de borrador.',
'lista_desplegable_multiple',
	   '{"0":"docx","1":"pdf","2":"odt"}','{"0":"docx","1":"pdf"}',0,
	   'Extensiones que podrá subir en este tipo de borrador reporte 4',null,null,null);



	      insert into configuraciones (tipo,modulo,nombre_constante,nombre_funcion,valor_tipo,opciones,valor,visible,descripcion_ayuda,
fecha_actualizacion,actualizado_por,valor_anterior)
values('s','SEGURIDAD','WS_APPSERVICE_URL','Url del servicio',
'texto',
	   '','',0,
	   'Url del servicio',null,null,null);

	   insert into configuraciones (tipo,modulo,nombre_constante,nombre_funcion,valor_tipo,opciones,valor,visible,descripcion_ayuda,
fecha_actualizacion,actualizado_por,valor_anterior)
values('s','VALIDAR','VALIDAR_EXTENSIONES','Extensiones permitidas para Validar radicados.',
'lista_desplegable_multiple',
	   '{"0":"docx","1":"pdf","2":"odt","3":"ods"}','{"0":"docx","1":"pdf"}',0,
	   'Extensiones permitidas para Validar radicados.',null,null,null);


	   /**
	   14/04/2020
	    */
	   ALTER TABLE document_metadata
    add COLUMN ra_asun text;

     ALTER TABLE document_metadata
    add COLUMN id_borrador integer NOT NULL;

ALTER TABLE document_metadata ALTER COLUMN expediente_titulo TYPE character varying;

 /**
	   30/04/2020
	    */
ALTER TABLE document_authorized_by
    ADD COLUMN url_app character varying(4000);

    /**
    05/05/2020
     */

     insert into configuraciones (tipo,modulo,nombre_constante,nombre_funcion,valor_tipo,opciones,valor,visible,descripcion_ayuda,
fecha_actualizacion,actualizado_por,valor_anterior)
values('s','VALIDAR','NOMBRE_BOTON_VALIDAR_CONJUGADO','Palabras conjugadas, según nombre del botón en funcionalidad de Validar',
'lista_desplegable',
	   '{"Validar":{"0":"Validar","1":"Validado","2":"Valida","3":"Validó","4":"Validando","5":"Validación",
	   "6":"Validadores","7":"Validadas","8":"Validar Virtualmente","9":"Validado Virtuálmente",
	   "10":"Validación Virtual","11":"Validador"},
	   "Aprobar":{"0":"Aprobar","1":"Aprobado","2":"Aprueba","3":"Aprobó","4":"Aprobando",
"5":"Aprobación","6":"Aprobadores","7":"Aprobadas","8":"Aprobar Virtualmente",
"9":"Aprobado Virtuálmente","10":"Aprobación Virtual","11":"Aprobador"},
	   "Aceptar":{"0":"Aceptar","1":"Aceptado","2":"Acepta","3":"Aceptó","4":"Aceptando",
"5":"Aceptación","6":"Aceptan","7":"Aceptadas","8":"Aceptar Virtualmente",
"9":"Aceptado Virtuálmente","10":"Aceptación Virtual","11":"Aceptador"} }',
'{"0":"Validar","1":"Validado","2":"Valida","3":"Validó","4":"Validando","5":"Validación",
	   "6":"Validadores","7":"Validadas","8":"Validar Virtualmente","9":"Validado Virtuálmente",
	   "10":"Validación Virtual","11":"Validador"}',0,
	   'Conjugación nombre botón validar/aprobar',null,null,null);

	   /**
    18/05/2020
     */
	   ALTER TABLE document_authorized_by
    ADD COLUMN navigator_appversion character varying(4000);

/**
05/06/2020
 */

 insert into configuraciones (tipo,modulo,nombre_constante,nombre_funcion,valor_tipo,opciones,valor,visible,descripcion_ayuda,
fecha_actualizacion,actualizado_por,valor_anterior)
values('s','SEGURIDAD','WS_APPSERVICE_URLS_ALLOWED','Urls permitidas para hacer solicitudes al servidor',
    'texto',
	   '{"0":"http://orfeo"}','{"0":"http://orfeo"}',0,
	   'Urls permitidas para hacer solicitudes al servidor',null,null,null);


	   insert into configuraciones (tipo,modulo,nombre_constante,nombre_funcion,valor_tipo,opciones,valor,visible,descripcion_ayuda,
fecha_actualizacion,actualizado_por,valor_anterior)
values('s','SEGURIDAD','WS_APPSERVICE_URLS_ALLOWED','Urls permitidas para hacer solicitudes al servidor',
    'texto',
	   '{"0":"http://orfeo"}','{"0":"http://orfeo"}',0,
	   'Urls permitidas para hacer solicitudes al servidor',null,null,null);

	   insert into configuraciones (tipo,modulo,nombre_constante,nombre_funcion,valor_tipo,opciones,valor,visible,descripcion_ayuda,
fecha_actualizacion,actualizado_por,valor_anterior)
values('s','SEGURIDAD','WS_APPSERVICE_HEADERS_ALLOWED','Parámetros permitidos para hacer solicitudes al servidor',
    'texto',
	   '{"0":"Content-Type","1":"Authorization","2":"Origin"}','{"0":"Content-Type","1":"Authorization","2":"Origin"}',0,
	   'Parámetros permitidos para hacer solicitudes al servidor',null,null,null);

	   	   insert into configuraciones (tipo,modulo,nombre_constante,nombre_funcion,valor_tipo,opciones,valor,visible,descripcion_ayuda,
fecha_actualizacion,actualizado_por,valor_anterior)
values('s','SEGURIDAD','WS_APPSERVICE_METHODS_ALLOWED','Métodos permitidos para hacer solicitudes al servidor',
    'texto',
	   '{"0":"POST","1":"GET","2":"PUT"}','{"0":"POST","1":"GET","2":"PUT"}',0,
	   'Métodos permitidos para hacer solicitudes al servidor',null,null,null);


/**
21/06/2020
 */

	   insert into configuraciones (tipo,modulo,nombre_constante,nombre_funcion,valor_tipo,opciones,valor,visible,descripcion_ayuda,
fecha_actualizacion,actualizado_por,valor_anterior)
values('s','BORRADORES','CARGO_EDITABLE','Definir si el cargo es editable.',
'lista_desplegable',
	   '{"0":"SI","1":"NO"}','NO',0,
	   'Definir si el cargo sera editable una vez se obtiene el valor.',null,null,null);

	   /**
	   02/06/2020
	    */
	    insert into configuraciones (tipo,modulo,nombre_constante,nombre_funcion,valor_tipo,opciones,valor,visible,descripcion_ayuda,
fecha_actualizacion,actualizado_por,valor_anterior)
values('s','SEGURIDAD','WS_APPSERVICE_TOKEN','Token de conexión Servicio Web Rest Aplicación Laravel',
    'texto',
	   '21497beba40b3b115589733b21f7a0c0b68dc7194f1c925c296a026dbfa761e3','21497beba40b3b115589733b21f7a0c0b68dc7194f1c925c296a026dbfa761e3',0,
	   'Token de conexión Servicio Web Rest Aplicación Laravel',null,null,null);

	   insert into configuraciones (tipo,modulo,nombre_constante,nombre_funcion,valor_tipo,opciones,valor,visible,descripcion_ayuda,
fecha_actualizacion,actualizado_por,valor_anterior)
values('v','VALIDAR','VALIDAR_DESDE_FECHA','Indicar la fecha a partir de la cual se puede empezar a validar documentos.',
    'texto',
	   '2019-09-10','2019-09-10',0,
	   'Indicar la fecha a partir de la cual se puede empezar a validar documentos',null,null,null);

	    /**
	   02/06/2020
	    */
	    insert into configuraciones (tipo,modulo,nombre_constante,nombre_funcion,valor_tipo,opciones,valor,visible,descripcion_ayuda,
fecha_actualizacion,actualizado_por,valor_anterior)
values('s','VALIDAR','DIAS_REABRIR_VALIDACION','Indique cuántos días puede estar abierta una validación para agregar validadores después de Cerrado. ','lista_desplegable',
	   '{"0":0,"1":1,"2":3,"3":5,"4":10}',3,0,
	   'Permite agregar validadores después del número de días indicado, después de su última validación. ',null,null,null);


	     /**
	   21/08/2020
	    */
	   insert into configuraciones (tipo,modulo,nombre_constante,nombre_funcion,valor_tipo,opciones,valor,visible,descripcion_ayuda,
fecha_actualizacion,actualizado_por,valor_anterior)
values('op','ORDENES DE PAGO','OP_FECHA_RADICACION','Fecha hasta que se va a poder seguir radicando borradores',
    'fecha',
	   '15-08-2020 15:00','15-08-2020 15:00',0,
	   'Fecha hasta que se va a poder seguir radicando borradores',null,null,null);


	   /**
	   04/09/2020
	    */

	   insert into configuraciones (tipo,modulo,nombre_constante,nombre_funcion,valor_tipo,opciones,valor,visible,descripcion_ayuda,
fecha_actualizacion,actualizado_por,valor_anterior)
values('s','RADICACION','USUARIOS_FINALIZAR_TODO',
'Usuarios que pueden finalizar radicados sin verificar si fueron validados o digitalizados. ','lista_desplegable_multiple',
	   '{"0":"MARIO.ORTIZ","1":"ANA.RUIZ"}','{"0":"MARIO.ORTIZ","1":"ANA.RUIZ"}',0,
	   'Usuarios que pueden finalizar radicados sin verificar si fueron validados o digitalizados. ',null,null,null);


 insert into configuraciones (tipo,modulo,nombre_constante,nombre_funcion,valor_tipo,opciones,valor,visible,descripcion_ayuda,
fecha_actualizacion,actualizado_por,valor_anterior)
values('s','BORRADORES','AGREGAR_PARA_EN_DE',
'Agregar automáticamente el usuario seleccionado en el PARA: hacia el DE:','lista_desplegable',
	   '{"0":"SI","1":"NO"}','SI',0,
	   'Agregar automáticamente el usuario seleccionado en el PARA: hacia el DE:',null,null,null);

	   insert into configuraciones (tipo,modulo,nombre_constante,nombre_funcion,valor_tipo,opciones,valor,visible,descripcion_ayuda,
fecha_actualizacion,actualizado_por,valor_anterior)
values('s','BORRADORES','TAMANO_ARCHIVO',
'Definir el tamano que tendran los archivos que se pueden subir','texto',
	   '3','SI',0,
	   'Definir el tamano que tendran los archivos que se pueden subir',null,null,null);

--fecha 30 octubre 2020
  ALTER TABLE public.usuario
    ADD COLUMN usua_codigo_apro numeric(20, 0);

  ALTER TABLE public.usuario
    ADD COLUMN fecha_exp_cod character varying(30);

