<?php

use App\Helpers\PDOconfig;
use Aura\SqlQuery\QueryFactory;
use Philo\Blade\Blade;

require '../../vendor/autoload.php';


function anularRadicados($get)
{
    $json = array();
    try {
        require_once '../../config.php';
    $ruta_raiz = "../../";
    if (isset($db)) unset($db);
    include_once("$ruta_raiz/include/db/ConnectionHandler.php");
    $db = new ConnectionHandler("$ruta_raiz");

        $radicadosHelper = new \App\Helpers\Radicados($db);
        $configsHelper = new \App\Helpers\Configuraciones($db);

        $configs_anular=$configsHelper->getConfiguraciones(false,false,"'ANULACION'");

//Busc si la funcionalidad de anular, esta habiliada o no, para mostrarla
        if(count($configs_anular)>0) {
            foreach($configs_anular as $row) {
                ${$row['NOMBRE_CONSTANTE']}=$row['VALOR'];
            }
        }

        $fechahoy = $CRON_FECHA_INICIAL; //fecha que se enviara al query

        $poranular = $radicadosHelper->radPorAnular($fechahoy,$ANULA_DIAS_TRAMITE);



        $expediente = $ANULACIONES_EXPEDIENTE;
        //COLOCAR LA FECHA INICIAR DEL QUERY 1

        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        $radicadosHelper->actNotifEstadoRadAnul($fechahoy);

        $fecha=date('Y-m-d H:i');

        echo "<br>";
        if($FUNCTION_ANULA_AUTO=="SI") {
            foreach ($poranular as $row) {
                echo "NUEVO";
                $fecharadmas30 = date("Y-m-d H:i", strtotime($row['FECHA_RADICADO'] . ' +'.$ANULA_DIAS_TRAMITE.'day'));
                //si la fecha de hoy es mayor que la fecha en la que fue el radicado +30 dias, debe anularse
                if ($fecha > $fecharadmas30) {
                    /*var_dump('paso');
                    var_dump($row);*/
                    $radicadosHelper->updateRadicAnular($row['RADICADO']);

                    $nextval = $radicadosHelper->selectNextVal();

                    echo "<br>";
                    //: SQL 3: Insertar radicados en expediente anual de anulados
                    $radicadosHelper->insertSgdAnulados($nextval[0]['NEXTVAL'], $row['RADICADO'], $row['TIPO_RADICADO'],
                        $row['DEPE_CODI'], $row['USUA_DOC_ACTUAL'], $row['USUA_CODI']);
                    echo "<br>";
                    $radicadosHelper->insertHistAnulacion($row['RADICADO'],
                        $row['DEPE_CODI'], $row['USUA_DOC_ACTUAL'], $row['USUA_CODI']);

                    echo "<br>";
                    //SQL 5
                    $radicadosHelper->updateExcluirRadExped($row['RADICADO']);

                    echo "<br>";
                    /////SQL 4
                    $radicadosHelper->excluirRadDeExped($expediente, $row['RADICADO'],
                        $row['DEPE_CODI'], $row['USUA_DOC_ACTUAL'], $row['USUA_CODI']);
                    echo "<br>";
                    $radicadosHelper->insertHistExped($expediente, $row['RADICADO'],
                        $row['DEPE_CODI'], $row['USUA_DOC_ACTUAL'], $row['USUA_CODI']);
                    echo "<br>";
                    $radicadosHelper->insertHistRad($expediente, $row['RADICADO'],
                        $row['DEPE_CODI'], $row['USUA_DOC_ACTUAL'], $row['USUA_CODI']);

                    echo "<br>";

                    //SQL 5
                    $radicadosHelper->insertHistExpedExclus($expediente, $row['RADICADO'],
                        $row['DEPE_CODI'], $row['USUA_DOC_ACTUAL'], $row['USUA_CODI']);
                    break;

                }
            }
        }


    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
        $response = "ERROR";
        $json['error'] = true;
    }


    echo json_encode($json);

}

if (isset($_GET['function_call']) and $_GET['function_call'] == "anularRadicados") {

    anularRadicados($_GET);
}


?>

