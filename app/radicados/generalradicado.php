<?php

use App\Helpers\PDOconfig;
use Aura\SqlQuery\QueryFactory;
use Philo\Blade\Blade;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\Settings;
use PhpOffice\PhpWord\SimpleType\TblWidth;
use PhpOffice\PhpWord\Style\Table;
use PhpOffice\PhpWord\TemplateProcessor;
use PhpOffice\PhpWord\Writer\Word2007\Element\Container;
use PhpOffice\Common\XMLWriter;

$ruta_raiz = "../../";
ini_set('log_errors', 1);


require '../../vendor/autoload.php';


if (!isset($_SESSION['usuario_id'])) {
    session_start();
}


function radExpedIncluidos($post)
{
    $json = array();
    try {
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        require_once '../../config.php';
        $ruta_raiz = "../../";
        if (isset($db)) unset($db);
        include_once("$ruta_raiz/include/db/ConnectionHandler.php");
        $db = new ConnectionHandler("$ruta_raiz");
        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        $borra = new \App\Helpers\Borradores($db);
        if ($_POST['radicado'] != "") {
            $get = $borra->getExpedNotDeleted($_POST['radicado']);
            $json['expedientes'] = $get;
        } else {
            $json['error'] = "Debe ingresar un radicado";
        }
        $json['fechaahora'] = date('YmdHis'); //retorno la hora de este momento


    } catch (Exception $e) {
        $json['error'] = "Error: " . $e->getMessage();
    }
    echo json_encode($json);

}

function fnbuscarFirmantes($borra, $radicado)
{


    //busco si el radicado, fue asociado a un borrador al momento de radicar(puede que si o no por los radicados viejos )
    $borrador_radicado = $borra->get_borradorByRadi($radicado);

    $buscarfirmantes = array();
    if (count($borrador_radicado) > 0) {
        $buscarfirmantes = $borra->getOnlyFirmByRad($borrador_radicado[0]['ID_BORRADOR']);
    }

    return $buscarfirmantes;

}

function buscarFirmantes()
{
    require_once '../../config.php';
    $ruta_raiz = "../../";
    if (isset($db)) unset($db);
    include_once("$ruta_raiz/include/db/ConnectionHandler.php");
    $db = new ConnectionHandler("$ruta_raiz");
    $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
    $borra = new \App\Helpers\Borradores($db);
    $radicado = $_GET['radicado'];
    $buscarfirmantes = array();
    $firmantes = $borra->getFirmByRadicado($radicado);
    if (count($firmantes) > 0) {
        $buscarfirmantes = $borra->barmarUsuariosComen($firmantes);
    }

    echo json_encode($buscarfirmantes);
}

function datausersPosibleAutoriza($radicado)
{
    $json = array();
    try {
        require_once '../../config.php';
        $ruta_raiz = "../../";
        if (isset($db)) unset($db);
        include_once("$ruta_raiz/include/db/ConnectionHandler.php");
        $db = new ConnectionHandler("$ruta_raiz");
        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        $helperRadi = new \App\Helpers\Radicados($db);
        $borra = new \App\Helpers\Borradores($db);

        if (!isset($_GET['search'])) {
            $_GET['search'] = NULL;
        }


        //busco si el radicado, fue asociado a un borrador al momento de radicar(puede que si o no por los radicados viejos )

        $firmantes = false;

        $buscarfirmantes = $borra->getOnlyFirmByRad($radicado);

        if (count($buscarfirmantes) > 0) {
            $firmantes = '';
            foreach ($buscarfirmantes as $row) {
                $firmantes .= $row['ID_USUARIO_FIRMANTE'] . ",";
            }
            $firmantes = substr($firmantes, 0, strlen($firmantes) - 1);
        }

        $usuarios = $helperRadi->getUsersWithPermAprob($_GET['search'],
            $firmantes,false,'TODOS');

        $usuarioeenviar = array();
        if (count($usuarios) > 0) {
            $usuarioeenviar = $borra->barmarUsuariosComen($usuarios);
        }


        return json_encode($usuarioeenviar);

    } catch (Exception $e) {
        $response = "Error: " . $e->getMessage();

        $json['error'] = $response;
        return json_encode($json);
    }
}

function dataUsersPosAutoriza()
{
    $json = array();
    try {
        require_once '../../config.php';
        $ruta_raiz = "../../";
        if (isset($db)) unset($db);
        include_once("$ruta_raiz/include/db/ConnectionHandler.php");
        $db = new ConnectionHandler("$ruta_raiz");
        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        $helperRadi = new \App\Helpers\Radicados($db);
        $borra = new \App\Helpers\Borradores($db);

        if (!isset($_GET['search'])) {
            $_GET['search'] = NULL;
        }


        $usuarios = $helperRadi->getUsersWithPermAprob($_GET['search'],
            false,false,'TODOS');

        $usuarioeenviar = array();
        if (count($usuarios) > 0) {
            $usuarioeenviar = $borra->barmarUsuariosComen($usuarios);
        }


        return json_encode($usuarioeenviar);

    } catch (Exception $e) {
        $response = "Error: " . $e->getMessage();

        $json['error'] = $response;
        return json_encode($json);
    }
}

function usersConfig()
{

    echo dataUsersPosAutoriza();

}


function usersPosibleAutoriza()
{
    $radicado = $_GET['radicado'];
    echo datausersPosibleAutoriza($radicado);

}

//dependiendo de la accion, guarda o borra los firmantes d eun radicado en la tabla borrador_firmantes
function ajaxRmOrAddFirmantes($post)
{
    $json = array();
    try {
        require_once '../../config.php';
        $ruta_raiz = "../../";
        if (isset($db)) unset($db);
        include_once("$ruta_raiz/include/db/ConnectionHandler.php");
        $db = new ConnectionHandler("$ruta_raiz");
        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        $helperRadi = new \App\Helpers\Radicados($db);
        $borra = new \App\Helpers\Borradores($db);

        $ANEXO_ID = $_POST['ANEXO_ID'];
        $infoAnexo = $helperRadi->buscarAnexoById($ANEXO_ID);
        $radicado = $infoAnexo[0]['ANEX_RADI_NUME'];
        if ($infoAnexo[0]['RADI_NUME_SALIDA'] != null && $infoAnexo[0]['RADI_NUME_SALIDA'] != '') {
            $radicado = $infoAnexo[0]['RADI_NUME_SALIDA'];
        }

        //busco si el radicado, fue asociado a un borrador al momento de radicar(puede que si o no por los radicados viejos )
        $borrador_radicado = $borra->get_borradorByRadi($radicado);

        $id_borrador = false;
        $firmantes = array();
        if (count($borrador_radicado) > 0) {
            $id_borrador = $borrador_radicado[0]['ID_BORRADOR'];
            $firmantes = $borra->getFirmByBorrador($id_borrador);
        }


        $fecha_transaccion = $db->conn->OffsetDate(0, $db->conn->sysTimeStamp);
        //busco los que estan en la bd actualmente

        //borro todos sus firmantes
        $borra->borrarFirmanteByRad($radicado);

        //si trae firmantes
        if (isset($_POST['firmantes'])) {
            foreach ($_POST['firmantes'] as $row) {

                $principal = 0;
                if (count($_POST['firmantes']) < 2) {
                    $principal = 1;
                }
                $usuario = $borra->getUsuarioById($row['ID']);

                //entra aqui cuando no existe $_POST['cargo_firmante'], quiere decir, que vienen mas de 1 firmante,
                //si viene 1 solo firmante, entonces $cargo_firmante es el input de texto.
                $cargo_firmante = $usuario[0]['USUA_CARGO'];

                $tipo_validador = isset($row['TIPO_VALIDADOR']) && $row['TIPO_VALIDADOR'] !== '' ? "'" . $row['TIPO_VALIDADOR'] . "'" : 'null';

                if (count($firmantes) > 0) {

                    $consiguio = FALSE;
                    $cont = 0;
                    //si entra aqui es porque ya tiene firmantes
                    foreach ($firmantes as $firmante) {
                        //pregunto si el id que viene del formulario es igual al que estoy recorriendo de la BD
                        if ($firmante['ID'] == $row['ID']) {
                            $consiguio = $cont;
                        }
                        $cont++;
                    }

                    if ($consiguio !== FALSE) {
                        $principal = $firmantes[$consiguio]['FIRMANTE_PRINCIPAL'];
                        if (count($_POST['firmantes']) < 2) {
                            $principal = 1;
                        }
                        //si entra aqui, guardo los valores, con los datos que tenia si es que tenia
                        $borra->guardarFirmantesRadicado($id_borrador, "'" . $firmantes[$consiguio]['FECHA_REGISTRO'] . "'", $row['ID'],
                            $cargo_firmante, $principal, $radicado, $tipo_validador);


                    } else {

                        //si entra aqui es porque es un nuevo firmante
                        $borra->guardarFirmantesRadicado($id_borrador, $fecha_transaccion, $row['ID'], $cargo_firmante,
                            $principal, $radicado, $tipo_validador);

                        $actualiaEst = $helperRadi->devolverEstValidaIcon($radicado);
                    }
                } else {


                    //si entra aqui es porqueno tiene ningun firmantes guardado
                    $borra->guardarFirmantesRadicado($id_borrador, $fecha_transaccion, $row['ID'], $cargo_firmante,
                        $principal, $radicado, $tipo_validador);

                }
            }
        }


        if (!isset($_GET['search'])) {
            $_GET['search'] = NULL;
        }

        $firmantes = false;
        $buscarfirmantes = $borra->getOnlyFirmByRad($radicado);
        //verifico si tiene firmante principal, y si no, se lo asigno al primero que encuentre
        $borra->tieneFirmPrincipal($buscarfirmantes, $id_borrador);
        //verifico si tiene Remitente, y si no, se lo asigno al primero que encuentre

        $borra->tieneRemitente($buscarfirmantes, $id_borrador);
        if (count($borrador_radicado) > 0) {

            if (count($buscarfirmantes) > 0) {
                $firmantes = '';
                foreach ($buscarfirmantes as $row) {
                    $firmantes .= $row['ID_USUARIO_FIRMANTE'] . ",";
                }
                $firmantes = substr($firmantes, 0, strlen($firmantes) - 1);
            }
        }

        $usuarios = $helperRadi->getUsersWithPermAprob($_GET['search'], $firmantes);
        $usuarioeenviar = array();
        if (count($usuarios) > 0) {
            $usuarioeenviar = $borra->barmarUsuariosComen($usuarios);
        }

        echo json_encode($usuarioeenviar);

    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
        $response = "ERROR";
        $json['error'] = true;
    }

}

function getFirmPrinc($get)
{
    $json = array();
    try {

        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        require_once '../../config.php';
        $ruta_raiz = "../../";
        if (isset($db)) unset($db);
        include_once("$ruta_raiz/include/db/ConnectionHandler.php");
        $db = new ConnectionHandler("$ruta_raiz");
        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        $helperRadi = new \App\Helpers\Radicados($db);
        $borra = new \App\Helpers\Borradores($db);
        $ANEXO_ID = $_GET['ANEXO_ID'];
        $infoAnexo = $helperRadi->buscarAnexoById($ANEXO_ID);

        $radicado = $infoAnexo[0]['ANEX_RADI_NUME'];

        if ($infoAnexo[0]['RADI_NUME_SALIDA'] != null && $infoAnexo[0]['RADI_NUME_SALIDA'] != '') {
            $radicado = $infoAnexo[0]['RADI_NUME_SALIDA'];
        }

        //busco si el radicado, fue asociado a un borrador al momento de radicar(puede que si o no por los radicados viejos )
        $borrador_radicado = $borra->get_borradorByRadi($radicado);


        $id_borrador = false;
        if (count($borrador_radicado) > 0) {
            $id_borrador = $borrador_radicado[0]['ID_BORRADOR'];
        }

        $ID_FIRMANTE_PRINCIPAL = $borra->getFirmantePrincipalIdbyBorr($id_borrador);

        echo json_encode($ID_FIRMANTE_PRINCIPAL);

    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
        $response = "ERROR";
        $json['error'] = true;
    }
}


function getUsersValidaron()
{
    require_once '../../config.php';
    $ruta_raiz = "../../";
    if (isset($db)) unset($db);
    include_once("$ruta_raiz/include/db/ConnectionHandler.php");
    $db = new ConnectionHandler("$ruta_raiz");
    $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
    $borra = new \App\Helpers\Borradores($db);
    $helperRadi = new \App\Helpers\Radicados($db);
    $radicado = $_POST['radicado'];

    $usuarios = array();
    $fuevalidado = $helperRadi->getDocumentMetadata($radicado);
    if (count($fuevalidado) > 0) {
        $usuarios = $helperRadi->getUsersValidaron($fuevalidado[0]['ID_RADICADO_METADATA']);
        if (count($usuarios) > 0) {
            $usuarios = $borra->barmarUsuariosComen($usuarios);
        }
    }

    $json = array();
    $json['document_metadata'] = $fuevalidado;
    $json['usuarios'] = $usuarios;

    echo json_encode($json);
}

function cleanSessionCorreo()
{
    $_SESSION['codigo_email_enviado'] = '';
    $_SESSION["codigo_aprobar_doc"] = '';
    $_SESSION["expiracion_aprobar_doc"] = '';


    $json = array();
    $json['success'] = 'Exito';
    echo json_encode($json);

}

function enviarcodigoemail()
{
    require_once '../../config.php';
    $ruta_raiz = "../../";
    if (isset($db)) unset($db);
    include_once("$ruta_raiz/include/db/ConnectionHandler.php");
    include_once $ruta_raiz . "/conf/configPHPMailer.php";
    $db = new ConnectionHandler("$ruta_raiz");
    $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
    $borra = new \App\Helpers\Borradores($db);
    $helperRadi = new \App\Helpers\Radicados($db);
    $email = $_SESSION["usua_email"];
    $radicados = json_decode($_POST['radicados']); //obtengo los radicados que quiero enviarles el codigo
    //$email = 'fernandoga22@gmail.com';
    // Instantiation and passing `true` enables exceptions
    $mail = new PHPMailer(true);
    $codigo = rand(100000, 999999);

    $_SESSION["codigo_aprobar_doc"] = $codigo;
    $_SESSION["expiracion_aprobar_doc"] = date('YmdHis');

    $_SESSION["expiracion_aprobar_doc"] = date('YmdHis', strtotime($_SESSION["expiracion_aprobar_doc"] . '+' . $_SESSION["EXPIRACION_CODIGO_VALIDACION"] . ' minutes'));

    $json = array();

    $conjugacionValidar = json_decode($_SESSION['NOMBRE_BOTON_VALIDAR_CONJUGADO'], true);
    try {
        //Server settings
        $updateCod = $helperRadi->updateCodigoAprobado($codigo,$_SESSION["expiracion_aprobar_doc"],$_SESSION["usuario_id"]);
        $mail->SMTPDebug = $debugPHPMailer;                                       // Enable verbose debug output
        $mail->isSMTP();                                            // Set mailer to use SMTP
        $mail->Host = $hostPHPMailer;  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                                   // Enable SMTP authentication
        $mail->Username = $usuarioEmailPQRS;                     // SMTP username
        $mail->Password = $passwordEmailPQRS;                               // SMTP password
        $mail->SMTPSecure = $tipoAutenticacion;                                  // Enable TLS encryption, `ssl` also accepted
        $mail->Port = $portPHPMailer;                                    // TCP port to connect to

        //Recipients
        $mail->setFrom($admPHPMailer, $admPHPMailer);          // Name is optional
        $mail->addAddress($email, $email);     // Add a recipient


        // Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $subject = $_SESSION["ambiente"] != 'produccion' ? 'PRUEBA: Validación Código de Seguridad' : 'Validación Código de Seguridad';
        $body = $_SESSION["ambiente"] != 'produccion' ? 'PRUEBA: ' : '';
        $mail->Subject = utf8_decode($subject);
        $mail->Body = utf8_decode("$body El código de seguridad para continuar con la " . $conjugacionValidar[5] . " es el siguiente: <b>$codigo</b>");
        $mail->AltBody = utf8_decode("$body El código de seguridad para continuar con la " . $conjugacionValidar[5] . " es el siguient: $codigo");

        $enviar = $mail->send(); //quitar las barras de comentario
        if ($enviar == true) {
            $json['success'] = 'Correo enviado con éxito';
            $json['expiracion_aprobar_doc'] = $_SESSION["expiracion_aprobar_doc"];
            if ($updateCod != false){
                $_SESSION['codigo_email_enviado'] = 'SI'; 
            }else{
                $json['error'] = 'Ha ocurrido un error al enviar el correo con el código';
            }
            $json['send'] = $_SESSION['codigo_email_enviado'];
            //el request no debe devolver el codigo porque puede ser capturado en el inspeccionador del navegador
            //$json['codigo_aprobar_doc'] = $_SESSION["codigo_aprobar_doc"];
        } else {
            $json['error'] = 'Ha ocurrido un error al enviar el correo con el código';
        }

    } catch (Exception $e) {
        $json['error'] = 'Ha ocurrido un error al enviar el correo con el código. ' . $e->getMessage();
    }

    //el request no debe devolver el codigo porque puede ser capturado en el inspeccionador del navegador
    //$json['codigo'] = $codigo;

    echo json_encode($json);
}


function accion_validar()
{
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    $ruta_raiz = "../../";
    $conjugacionValidar = json_decode($_SESSION['NOMBRE_BOTON_VALIDAR_CONJUGADO'], true);
    require_once '../../config.php';

    if (isset($db)) unset($db);
    include_once("$ruta_raiz/include/db/ConnectionHandlerNew.php");
    $db = new ConnectionHandler("$ruta_raiz");
    $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
    $borra = new \App\Helpers\Borradores($db);
    $helperRadi = new \App\Helpers\Radicados($db);
    $configHelper = new \App\Helpers\Configuraciones($db);

    $usuariologueado = $borra->getUsuarioById($_SESSION['usuario_id']);
    $codigo = $_POST['codigo'];
    $rad_padre = $_POST['radi_padre']; //puede ser el mismo que viene abajo en el arreglo

    $vRadi_nume_iden = 0;

    $ANEXO_ID = $_POST['ANEXO_ID'];
    $json = array();
    $type = "PDF417";
    $estatus = 'ABIERTO';
    $infoAnexo = $helperRadi->buscarAnexoById($ANEXO_ID);
    $radicado = $infoAnexo[0]['ANEX_RADI_NUME'];
    if ($infoAnexo[0]['RADI_NUME_SALIDA'] != null && $infoAnexo[0]['RADI_NUME_SALIDA'] != '') {
        $radicado = $infoAnexo[0]['RADI_NUME_SALIDA'];
    }


    $_GET['radicado'] = $radicado;
    $fuevalidado = $helperRadi->getDocumentMetadata($radicado);
    if (count($fuevalidado) > 0) {
        if ($fuevalidado[0]['STATUS_DOCUMENT'] != null) {
            $estatus = $fuevalidado[0]['STATUS_DOCUMENT'];
        }
    }

    //whether ip is from share internet
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip_address = $_SERVER['HTTP_CLIENT_IP'];
    } //whether ip is from proxy
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } //whether ip is from remote address
    else {
        $ip_address = $_SERVER['REMOTE_ADDR'];
    }

    if ($estatus != 'CERRADO') {

        $codigo = trim($codigo);


        /**
         * Solo se debe ingresar numeros, ya que si ingresa un string con letras o otra cosa que no sea numeros
         * va a arrojar el error.
         */
        if ($_SESSION["METODO_VALIDACION"] === 'Codigo eMail'
            &&
            isset($_SESSION["codigo_aprobar_doc"])
            && $_SESSION["codigo_aprobar_doc"] != ''
            &&
            (
                (
                    is_numeric($codigo)
                    &&
                    ($codigo+0) !== $_SESSION["codigo_aprobar_doc"]
                )
            ||
                (
                is_numeric($codigo)==false
                )

            )
        ) {
            $json['error'] = 'El código de seguridad ingresado no coincide. Verifique que tenga solo números.';

            echo json_encode($json);
            exit();
        }

        //pregunto si la hora y fecha actual, es mayor a la fecha de expiracion, entonces
        //retorno un msj diciendo0 que debe enviar nuevamente el codigo
        if ($_SESSION["METODO_VALIDACION"] == 'Codigo eMail' &&
            isset($_SESSION["codigo_aprobar_doc"]) && $_SESSION["codigo_aprobar_doc"] != '' &&
            $codigo === $_SESSION["codigo_aprobar_doc"]
            && date('YmdHis') > date('YmdHis', strtotime($_SESSION["expiracion_aprobar_doc"]))) {
            $json['error'] = 'El código ingresado, ya ha expirado, debe enviar nuevamente un código a su email';
            echo json_encode($json);
            exit();
        }


        if (($_SESSION["METODO_VALIDACION"] === 'Clave única' &&
            count($usuariologueado) > 0 &&
            SUBSTR(md5($codigo), 1, 26) !== $usuariologueado[0]['USUA_PASW_DOC'])) {
            $json['error'] = 'La clave ingresada es incorrecta';
            echo json_encode($json);
            exit();
        }

        $inforadicado = $borra->getRadicadoByNume_radi($radicado);

        if (count($inforadicado) > 0) {
            $ln = $_SESSION["digitosDependencia"];
            $path_anexo = "/" . substr(trim($infoAnexo[0]['ANEX_CODIGO']), 0, 4) . "/" . intval(substr(trim($infoAnexo[0]['ANEX_CODIGO']), 4, $ln)) . "/docs/" . trim($infoAnexo[0]['ANEX_NOMB_ARCHIVO']);
            $FilePath = $ruta_raiz . "bodega/" . substr(trim($infoAnexo[0]['ANEX_CODIGO']), 0, 4) . "/" . intval(substr(trim($infoAnexo[0]['ANEX_CODIGO']), 4, $ln)) . "/docs/" . trim($infoAnexo[0]['ANEX_NOMB_ARCHIVO']);


            //busco si tiene un borrador, para los radicados nuevos despues de esta funcionalidad.
            $borrador = $borra->get_borradorByRadi($radicado);

            $firmantesBor = $borra->getFirmByRadicado($radicado);


            $titulomemo = $borrador[0]['TIPO_COMUNICACION'];

            $archivo = pathinfo($path_anexo);
            $date = date('YmdHis') . '1';
            $ext = $archivo['extension'];
            $filename = $archivo['filename'];
            $name_pdf_original = $radicado . '_' . $date . '.pdf';
            $FilePathPdfOriginal_base = $archivo['dirname'] . '/' . $radicado . '_' . $date . '.pdf';

            //es el primer pdf creado sin el hash
            $FilePathPdfOriginal = '../../bodega' . $archivo['dirname'] . '/' . $radicado . '_' . $date . '.pdf';

            $mpdf = array();

            if (count($borrador) > 0) {

                try {

                    $otraInfoMetadata = $helperRadi->getTrdAndExpedMetadata($radicado);
                    $convertOtraInfoMetadata = $helperRadi->transformToJsonStringTrdAndExpedMetadata($otraInfoMetadata);

                    /**
                     * si es un arreglo, cualquiera de los 3 siguientes retorno, quiere decir que
                     * no había data, en $otraInfoMetadata, por lo tanto los guardo null
                     */

                    $trd_metadata = is_array($convertOtraInfoMetadata['trd']) ? 'null' : $convertOtraInfoMetadata['trd'];
                    $trd_expedientes = is_array($convertOtraInfoMetadata['expedientes']) ? 'null' : $convertOtraInfoMetadata['expedientes'];;
                    $trd_expediente_titulo = is_array($convertOtraInfoMetadata['expediente_titulo']) ? 'null' : $convertOtraInfoMetadata['expediente_titulo'];;
                    $trd_codigoverificacion =
                        count($otraInfoMetadata) > 0 ? $otraInfoMetadata[0]['RAD_COD_VERI'] : 123;

                    $db->conn->StartTrans();

                    //el historico lo busco aqui porque voy a mostrar en el documento del radicado, quien aprobo reviso  proyecto
                    $historico = $borra->getHistoricosByBorrador($borrador[0]['ID_BORRADOR'], "CASE 
                    borrador_historicos.id_estado
      WHEN '2' THEN 1
      WHEN '3' THEN 2
      WHEN '1' THEN 3 ELSE 5
   END");

                    $powered_by = 'mpdf';

                    if (count($fuevalidado) < 1) {

                        //no ha sido validado ni una vez, el radicado
                        $id_document_metadata = $helperRadi->insertdocument_metadata
                        (
                            $radicado,
                            $infoAnexo[0]['ANEX_CODIGO'],
                            'null',
                            'null',
                            $inforadicado[0]['SGD_SPUB_CODIGO'],
                            $trd_metadata,
                            $trd_expedientes,
                            $trd_expediente_titulo,
                            123,
                            'null', 1, $trd_codigoverificacion,
                            'mpdf', 'PDF', 'pdf',
                            'application/pdf', 'null',
                            'null', 'null', $path_anexo, 'null',
                            'ABIERTO',
                            $inforadicado[0]['RA_ASUN'],
                            $borrador[0]['ID_BORRADOR']
                        );
                        $id_document_metadata = $id_document_metadata->fields['ID_RADICADO_METADATA'];

                    } else {
                        $id_document_metadata = $fuevalidado[0]['ID_RADICADO_METADATA'];

                        $update_document_metadata = $helperRadi->updateDocumentMetadata(
                            $radicado,
                            $infoAnexo[0]['ANEX_CODIGO'],
                            $inforadicado[0]['SGD_SPUB_CODIGO'],
                            $trd_metadata,
                            $trd_expedientes,
                            $trd_expediente_titulo,
                            123,
                            $trd_codigoverificacion, 'mpdf', 'PDF',
                            'pdf', 'application/pdf',
                            'ABIERTO',
                            $inforadicado[0]['RA_ASUN'],
                            $borrador[0]['ID_BORRADOR'],
                            $id_document_metadata
                        );

                    }

                    $getCiudad = $helperRadi->getCiudadUsuario($_SESSION['usuario_id']);

                    $city = 'null';

                    if (count($getCiudad) > 0) {
                        if ($getCiudad[0]['MUNI_NOMB'] == $getCiudad[0]['DPTO_NOMB']) {
                            $city = $getCiudad[0]['MUNI_NOMB'] . ', ';
                        } else {
                            $city = $getCiudad[0]['MUNI_NOMB'] . ', ' . $getCiudad[0]['DPTO_NOMB'] . ', ';;
                        }
                        $city .= $getCiudad[0]['NOMBRE_PAIS'];
                    }

                    $url_app = $_POST['url_app'];

                    $id_document_authorized_by = $helperRadi->insertdocument_authorized_by($id_document_metadata,
                        $_SESSION['usuario_id'],
                        $_SESSION["usua_login"],
                        $_SESSION["usua_nomb"], $_SESSION["usua_email"], 'CO', $city,
                        $_SESSION["entidad_largo"], $_SESSION['dependencia'] . '-' . $_SESSION["depe_nomb"],
                        $_SESSION['nit_entidad'], $_SESSION['httpWebOficial'], $_POST['platform'],
                        $_POST['browser'],
                        $ip_address,  hash('sha256', $codigo) , $url_app, $_POST['navigator_appversion'], 'Geolocalización');


                    //////////////////////////////////////////////
                    $document_authorized_by = $helperRadi->getUsersValidaron($id_document_metadata);
                    if (count($document_authorized_by) > 0) {
                        $vRadi_nume_iden = 1;
                        if (count($document_authorized_by) == count($firmantesBor)) {
                            $vRadi_nume_iden = 2;
                        }
                    }

                    if ($borrador[0]['DOCUMENTO_FORMATO'] == "EditorWeb") {

                        ///////guardo la direccion, y busco los datos en las tablas, dependiendo del tipo de radicado
                        $datosDestinatario = $borra->datosDestinatario($borrador, $db);

                        //primero creo el pdf en base a los datos del editor,
                        $dataPdfOriginal = crearPdfWithHash($borra, $borrador, $radicado, $datosDestinatario, $titulomemo,
                            $historico, false, $FilePathPdfOriginal, false,
                            $inforadicado, array(), $configHelper);


                        //luego a ese pdf, le saco el hash, que pasa a ser el documento principal
                        $code = $dataPdfOriginal['hashfilemetadada'];
                        require_once($ruta_raiz . "app/resources/global/plugins/tcpdf_min/tcpdf_barcodes_2d.php");
                        $barcodeobj = new TCPDF2DBarcode($code, $type);
                        $pngbarra = '/bodega' . $archivo['dirname'] . '/hashbarra' . $radicado . '.png';
                        $rutahashbarra = $ruta_raiz . $pngbarra;
                        $barcodeobj->getBarcodePNG($w = 3, $h = 2, $color = array(0, 0, 0), $rutahashbarra);

                        $date = date('YmdHis') . '2';
                        $FilePathPdfOriginalValidado_base = $archivo['dirname'] . '/' . $radicado . '_' . $date . '.pdf';
                        $FilePathPdfOriginalValidado = '../../bodega' . $archivo['dirname'] . '/' . $radicado . '_' . $date . '.pdf';
                        $dataPdfValidado = crearPdfWithHash($borra, $borrador, $radicado, $datosDestinatario, $titulomemo,
                            $historico, $dataPdfOriginal['hashfilemetadada'], $FilePathPdfOriginalValidado, $rutahashbarra,
                            $inforadicado, $document_authorized_by, $configHelper);

                        $update_Radi_nume_iden = $helperRadi->updateRadi_nume_iden($radicado, $vRadi_nume_iden);
                        $terminarValidacion = terminarValidacion($helperRadi, $radicado, $borra, $rad_padre, $ruta_raiz,
                            $db, $inforadicado, $dataPdfValidado, $FilePathPdfOriginal_base, $FilePathPdfOriginalValidado_base, $code,
                            $pngbarra, 'ABIERTO', $infoAnexo);

                        if ($terminarValidacion == false) {
                            //ocurrio un error

                        } else {
                            $json['success'] = true;
                            $json['document_metadata'] = $terminarValidacion['fuevalidado'];
                            $json['usuarios'] = $terminarValidacion['usuarios'];
                        }

                    } else {

                        if (is_file($FilePath)) {

                            if ($ext == 'docx' || $ext == 'doc') {
                                $powered_by = 'phpdocx';
                                $funcionconvert = funcionconvert($db, $inforadicado,
                                    $radicado, $type, $borra, $historico, $document_authorized_by, $path_anexo,
                                    $configHelper, $borrador);

                                $dataPdfValidado = $funcionconvert['dataPdfValidado'];
                                $FilePathPdfOriginalValidado_base = $funcionconvert['FilePathPdfOriginalValidado_base'];
                                $code = $funcionconvert['code'];
                                $pngbarra = $funcionconvert['pngbarra'];

                                $terminarValidacion = terminarValidacion($helperRadi, $radicado, $borra,
                                    $rad_padre, $ruta_raiz,
                                    $db, $inforadicado, $dataPdfValidado, $FilePathPdfOriginal_base,
                                    $FilePathPdfOriginalValidado_base, $code,
                                    $pngbarra, 'ABIERTO', $infoAnexo);
                                if ($terminarValidacion == false) {

                                } else {
                                    $json['success'] = true;
                                    $json['document_metadata'] = $terminarValidacion['fuevalidado'];
                                    $json['usuarios'] = $terminarValidacion['usuarios'];
                                }
                            }

                            //$json['error'] = 'Hoy solo puede Validar documentos generados desde el Editor Web';
                            if ($ext == 'pdf' || $ext == 'PDF') {

                            }


                        } else {
                            $complete = $db->conn->CompleteTrans(false);
                            $json['error'] = 'No existe el archivo del radicado';
                        }
                    }


                    $db->conn->CompleteTrans();

                } catch (Exception $exception) {

                    if ($exception) {
                        error_log('' . "\n" . date('d-m-Y H:i:s') . '- Ha ocurrido un error al '.$conjugacionValidar[0].' ' . $exception->getMessage(),
                            3, $ruta_raiz . "bodega/tmp/accion_validar-errors.log");
                        $json['error'] = $exception->getMessage();
                    } else {

                        $json['error'] = 'Error al validar';
                        error_log('' . "\n" . date('d-m-Y H:i:s') . '- Ha ocurrido un error al '.$conjugacionValidar[0].' ' . $json['error'],
                            3, $ruta_raiz . "bodega/tmp/accion_validar-errors.log");
                    }

                    $complete = $db->conn->CompleteTrans(false);


                }


            } else {

                $json['error'] = 'Solo se pueden '.$conjugacionValidar[8].' 
                los documentos radicados desde el '.date('d-m-Y', strtolower($_SESSION['VALIDAR_DESDE_FECHA']));
            }


        } else {
            $json['error'] = 'No existe el radicado';
        }


    } else {
        $json['error'] = 'El proceso de '.$conjugacionValidar[5].' se encuentra en estado ' . $estatus;

        if ($estatus == 'BLOQUEADO') {
            $json['error'] = ' Las plantillas solo pueden ser validadas por una persona en este momento.';

        }
    }

    echo json_encode($json);
}

function serviceSaveHistDespValidar($post)
{
    $conjugacionValidar = json_decode($_SESSION['NOMBRE_BOTON_VALIDAR_CONJUGADO'], true);
    $ruta_raiz = "../../";
    error_log("Log accion_validar", 3, $ruta_raiz . "bodega/tmp/accion_validar-errors.log");
    require_once '../../config.php';

    if (isset($db)) unset($db);
    include_once("$ruta_raiz/include/db/ConnectionHandlerNew.php");
    $db = new ConnectionHandler("$ruta_raiz");
    $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);

    $db->conn->StartTrans();
    try {
        saveHistDespValidar($db, $_POST['radicado'], $_POST['infoAnexo'], $_POST['inforadicado']);
        $db->conn->CompleteTrans();

        $json['success'] = true;


    } catch (Exception $exception) {

        $db->conn->CompleteTrans(false);

        if ($exception) {
            $json['message'] = $exception->getMessage();
            $json['error'] = $exception->getMessage();
        } else {
            $json['error'] = 'Error al '.$conjugacionValidar[0];
        }
    }

    echo json_encode($json);

}

/**
 * guarda el historico y envia el email, despues de validar
 */
function saveHistDespValidar($db, $radicado, $infoAnexo, $inforadicado)
{

    $conjugacionValidar = json_decode($_SESSION['NOMBRE_BOTON_VALIDAR_CONJUGADO'], true);
    $ruta_raiz = '../../';
    require_once("$ruta_raiz/include/tx/Tx.php");
    $Tx = new Tx($db);
    $radicadoarray = Array();
    $radicadoarray[0] = $radicado;
    //este metodo es el del archivo Historico.php en include/tx/Historico.php
    $Tx->insertarHistorico($radicadoarray, $_SESSION['dependencia'], $_SESSION['codusuario'],
        $_SESSION['dependencia'], $_SESSION['codusuario'],
        "Documento ".$radicado." ". strtolower($conjugacionValidar[9]). ' ', 47,
        NULL);

    //aqui verifico si tiene radicado padre, si tiene, le inserto el hiostorial tambien
    if ($infoAnexo[0]['ANEX_RADI_NUME'] != null && $infoAnexo[0]['ANEX_RADI_NUME'] != ''
        && $infoAnexo[0]['ANEX_RADI_NUME'] !== $infoAnexo[0]['RADI_NUME_SALIDA']) {
        $radicadoarray = Array();
        $radicadoarray[0] = $infoAnexo[0]['ANEX_RADI_NUME'];
        $Tx->insertarHistorico($radicadoarray, $_SESSION['dependencia'], $_SESSION['codusuario'],
            $_SESSION['dependencia'], $_SESSION['codusuario'],
            "Radicado hijo: " . $infoAnexo[0]['RADI_NUME_SALIDA']. ' '.$conjugacionValidar[9], 47,
            NULL);
    }


    //mando a enviar el email de que ya fue validado el doc
    enviarEmailYaValido($ruta_raiz, $inforadicado, $radicado);
}

function terminarValidacion($helperRadi, $radicado, $borra, $rad_padre, $ruta_raiz, $db, $inforadicado,
                            $dataPdfValidado, $FilePathPdfOriginal_base, $FilePathPdfOriginalValidado_base, $code,
                            $pngbarra, $estatus_document = 'ABIERTO', $infoAnexo)
{

    $conjugacionValidar = json_decode($_SESSION['NOMBRE_BOTON_VALIDAR_CONJUGADO'], true);

    if (is_file($ruta_raiz . 'bodega' . $FilePathPdfOriginalValidado_base) == false) {
        throw new Exception('El documento '.strtolower($conjugacionValidar[1]).', no se pudo guardar. ' . $FilePathPdfOriginalValidado_base);

    }
    if (is_file($ruta_raiz . 'bodega' . $FilePathPdfOriginal_base) == false) {
        throw new Exception('El documento Original, no se pudo guardar. ' . $FilePathPdfOriginalValidado_base);
    }


    /**
     * con lo siguiente obtengo la version del pdf
     */

    $type_version = $helperRadi->pdfVersion($ruta_raiz . 'bodega' . $FilePathPdfOriginal_base);
    //////////////////////////////////////////////
    $pages = $dataPdfValidado['totalpaginas'];
    $query = "update document_metadata set radi_path_original='" . $FilePathPdfOriginal_base . "', 
                                radi_path_validado='" . $FilePathPdfOriginalValidado_base . "', hash_doc_original='$code', 
                                size='" . $dataPdfValidado['kb'] . "', timestamp_m=current_timestamp, path_img_barra='" . $pngbarra . "',
                                pages=$pages, type_version='" . $type_version . "'
                                 where radi_nume_radi=$radicado ";
    $guardar = $db->conn->execute($query);

    $textohash = $helperRadi->getMetaDataHash($radicado, $infoAnexo[0]['ANEX_CODIGO']);

    $hash_validado = hash_file('sha256', $ruta_raiz . 'bodega' . $FilePathPdfOriginalValidado_base) .
        hash_file('sha256', $ruta_raiz . 'bodega' . $FilePathPdfOriginal_base);

    //le actualizo el hash_metadata, luego de insertarlo
    $query = "update document_metadata set hash_metadata='" . hash('sha256', $textohash[0]['TEXTO']) . "', 
        hash_validado='$hash_validado'
                      where radi_nume_radi=$radicado ";
    $guardar = $db->conn->execute($query);

    /////////////////////////////////////////77




    $usuarios = array();
    $fuevalidado = $helperRadi->getDocumentMetadata($radicado);
    if (count($fuevalidado) > 0) {
        $usuarios = $helperRadi->getUsersValidaron($fuevalidado[0]['ID_RADICADO_METADATA']);
        if (count($usuarios) > 0) {
            $usuarios = $borra->barmarUsuariosComen($usuarios);
        }
    }

    $buscarfirmantes = $borra->getOnlyFirmByRad($radicado);
    $estatus = 3; //este es el anex_estado por defecto que va a tener el anexo

    $query = "update document_metadata set status_document='$estatus_document'
                      where radi_nume_radi=$radicado ";
    $guardar = $db->conn->execute($query);




    $radi_nume_iden=1;
    if ($buscarfirmantes != NULL &&
        count($buscarfirmantes) == count($usuarios)) {
        $query = "update document_metadata set status_document='CERRADO'
                      where radi_nume_radi=$radicado ";

        $guardar = $db->conn->execute($query);
        $estatus = 4; //si esta cerrado, lo coloco en anex_estado 4
        $radi_nume_iden=2;
    }

    //para actualizarle los datos al raricado origen
    $updateRadc = "update radicado set 
    radi_path='" . $FilePathPdfOriginalValidado_base . "', 
    radi_nume_iden=$radi_nume_iden
     where radi_nume_radi=$radicado ";
    $guardar = $db->conn->execute($updateRadc);

    //actualizo el dato sobre la tabla anexo
    $query = "update anexos set anex_estado=$estatus
                      where anex_radi_nume=$rad_padre and radi_nume_salida=$radicado ";
    $guardar = $db->conn->execute($query);

    saveHistDespValidar($db, $radicado, $infoAnexo, $inforadicado);

    return array('usuarios' => $usuarios, 'fuevalidado' => $fuevalidado);
}

//Solution to injet into templates without breaking things
function containerToXML($container)
{
    $xmlWriter = new XMLWriter(XMLWriter::STORAGE_MEMORY, './', Settings::hasCompatibility());
    $containerWriter = new Container($xmlWriter, $container);
    $containerWriter->write();
    return ($xmlWriter->getData());
}


function armarprimerpdf($inforadicado,
                        $radicado, $type, $borra, $historico, $document_authorized_by, $path_anexo,
                        $configHelper, $borrador, $PDFA)
{

    $ruta_raiz = "../../";
    if (!class_exists('mPDF')) {
        include($ruta_raiz . "mpdf/mpdf.php");
    }

    ////////////////////////////////GENERO UN DOCUMENTO PARA EL RADICADO//////////////////////////////////////
    // Create an instance of the class:
    $mpdf = new mPDF('utf-8', 'Letter');
    $mpdf->SetImportUse();
    $mpdf->PDFAauto = true;


}

function funcionconvert($db, $inforadicado,
                        $radicado, $type, $borra, $historico, $document_authorized_by, $path_anexo,
                        $configHelper, $borrador)
{

    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    $ruta_raiz = "../../";
    require_once $ruta_raiz . 'vendor/phpdocx/Classes/Phpdocx/Create/CreateDocx.inc';

    $archivo = pathinfo($path_anexo);
    $date = date('YmdHis') . '1';
    $ext = $archivo['extension'];
    $filename = $archivo['filename'];
    $FilePath = '../../bodega' . $path_anexo;
    $name_with_date = 'bodega' . $archivo['dirname'] . '/' . $radicado . '_' . $date;
    $dir_to_transform = '/bodega' . $archivo['dirname'];
    //$urlconfig = $configHelper.getConfiguraciones('LANDING_PAGE_URL'); 
    $codver = $inforadicado[0]['SGD_RAD_CODIGOVERIFICACION'];
    if (is_null($codver)){
        $codver = $inforadicado['sgd_rad_codigoverificacion'];
    } 
    if (!(is_null($codver)) and $codver != '' and !(is_null($_SESSION['LANDING_PAGE_URL']) ) and $_SESSION['LANDING_PAGE_URL'] != ''){
    $codver = 'Codigo de Verificación CV: '. $codver .' Comprobar desde: '. $_SESSION['LANDING_PAGE_URL']; 
    }else{
    $codver = '';
    }

    //es el primer pdf creado sin el hash
    $FilePathPdfOriginal = '../../bodega' . $archivo['dirname'] . '/' . $radicado . '_' . $date . '.pdf';


    //$comandxecute="libreoffice5.2 --headless --convert-to pdf --outdir /bodega/2019/430/docs/ /bodega/2019/430/docs/120194300049111_00001.docx 2>&1; echo $?";


    //primero creo el pdf en base a los datos del editor,
    $dataPdfOriginal = edicionDocxToPdf($db, $FilePath, $historico, false, false,
        $FilePathPdfOriginal, array(), $borra, $name_with_date, $configHelper, $borrador, $dir_to_transform, $radicado);

    $code = $dataPdfOriginal['hashfilemetadada'];

    require_once($ruta_raiz . "app/resources/global/plugins/tcpdf_min/tcpdf_barcodes_2d.php");
    $barcodeobj = new TCPDF2DBarcode($code, $type);
    $pngbarra = '/bodega' . $archivo['dirname'] . '/hashbarra' . $radicado . '.png';
    $rutahashbarra = $ruta_raiz . $pngbarra;
    $barcodeobj->getBarcodePNG($w = 3, $h = 1.5, $color = array(0, 0, 0), $rutahashbarra);


    //defino como se va a llamar el validado
    $date = date('YmdHis') . '2';
    $name_with_date = 'bodega' . $archivo['dirname'] . '/' . $radicado . '_' . $date;
    $FilePathPdfOriginalValidado_base = $archivo['dirname'] . '/' . $radicado . '_' . $date . '.pdf';
    //es el primer pdf creado sin el hash
    $FilePathPdf = '../../bodega' . $archivo['dirname'] . '/' . $radicado . '_' . $date . '.pdf';
    $dataPdfValidado = edicionDocxToPdf(
        $db,
        $FilePath,
        $historico,
        $rutahashbarra,
        $dataPdfOriginal['hashfilemetadada'],
        $FilePathPdf, $document_authorized_by, $borra, $name_with_date, $configHelper, $borrador, $dir_to_transform, $radicado, $codver);

    return array('dataPdfValidado' => $dataPdfValidado, 'FilePathPdfOriginalValidado_base' => $FilePathPdfOriginalValidado_base,
        'code' => $code, 'pngbarra' => $pngbarra);


}

function edicionDocxToPdf(
    $db,
    $FilePath,
    $historico,
    $rutahashbarra = false,
    $hashtext,
    $FilePathPdf,
    $document_authorized_by = array(),
    $helperBorra,
    $name_with_date,
    $configHelper,
    $borrador,
    $dir_to_transform, $radicado, $codver = '')
{

    /**
     * esta funcion empieza a armar la estructura para convertir el doc a pdf
     */
    $ruta_raiz = '../../';

    $docx = new Phpdocx\Create\CreateDocxFromTemplate($FilePath);

    $statistics = new Phpdocx\Transform\TransformDocAdvLibreOffice();
    $docx->enableCompatibilityMode();
    $docx->setLanguage('es-ES');
    $content = new Phpdocx\Elements\WordFragment($docx, 'document');
    $fuente_size = $_SESSION['EDITOR_FUENTE_TAMANIO'] != 'SELECCIONE' ? $_SESSION['EDITOR_FUENTE_TAMANIO'] : 11;
    $fuente_size_pie = $_SESSION['FUENTE_PIE_TAMANIO'] != 'SELECCIONE' ? $_SESSION['FUENTE_PIE_TAMANIO'] : 10;

    /**
     * arma los usuariios que validaron el doc
     */
    $armarUsersValidaron = armarUsersValidaron($content, $document_authorized_by, 'phpdocx', $helperBorra, $radicado);
    $content = $armarUsersValidaron['html'];

    /**
     * arma los revisó proyectó, aprobó etc
     */
    $content = $helperBorra->armarAprobRevisProy($content, $historico, $helperBorra, 'phpdocx',
        $document_authorized_by, $configHelper);

    /**
     * la siguiente expresion, arman los anexos folios y folios comunicacion
     */
    $content = $helperBorra->armarAnexosFolInfo($content, $name_library = 'phpdocx', $borrador, $helperBorra);

    $referenceNode = array(
        'type' => 'paragraph',
        'occurrence' => 1,
    );
    $docx->insertWordFragment($content, $referenceNode, 'after');


    if ($rutahashbarra != false) {

        $image = new Phpdocx\Elements\WordFragment($docx);
        $options = array(
            'src' => $rutahashbarra,
            'scaling' => 35,
            'spacingTop' => 0,
            'spacingBottom' => 0,
            'spacingLeft' => 20,
            'spacingRight' => 0,
            'textWrap' => 1,
            'float' => 'center',
            'width' => 500,
        );
        $image->addImage($options);
        $runs = array();
        $runs[] = $image;
        
        $paragraphOptions = array(
            'textAlign' => 'center',
        );
        $docx->addText('');
        $docx->addText($runs, $paragraphOptions);
        $htmlHash = $helperBorra->TildesHtml('<p  style="text-align:center;font-size:'.$fuente_size_pie.'pt;"><span style="text-align:center;font-size:14px">'.$hashtext.'</span><br /><span style="text-align:center;font-size:14px">'.$codver.'</span></p>');
        $docx->embedHTML($htmlHash);
        //$runs_t = array();
        //$runs_t[] = array(
        //    'text' => $hashtext,
        //);
        //$runs_cv = array();
        //$runs_cv[] = array(
        //    'text' => $codver,
        //);
        //$paragraphOptions = array(
        //    'textAlign' => 'center',
        //    'fontSize' => '9'
        //);
        
        //$docx->addText('');
        //$docx->addText($runs_cv, $paragraphOptions);

    }

    $crear = $docx->createDocx($ruta_raiz . "$name_with_date.docx");

    $ejecutar1 = exec('export HOME=/bodega/tmp && libreoffice --invisible --convert-to pdf ' . "/$name_with_date.docx" . ' --outdir ' . $dir_to_transform . '',
        $lineas_ejecutadas);


    if (count($lineas_ejecutadas) < 1) {
        throw new Exception('Ha ocurrido un error al validar el documento en 
        "lineas_ejecutadas": edicionDocxToPdf ' . $FilePathPdf);
    }


    if (is_file($FilePathPdf) == false) {
        throw new Exception('Ha ocurrido un error al validar el documento en: edicionDocxToPdf ' . $FilePathPdf);
    }


    /**
     * lo siguiente obtiene el numero de paginas del pdf
     */
    $ruta_raiz = "../../";
    if (!class_exists('mPDF')) {
        include($ruta_raiz . "mpdf/mpdf.php");
    }
    $mpdf = new mPDF();
    $mpdf->SetImportUse();
    $totalpaginas = $mpdf->SetSourceFile($FilePathPdf);
    if ($totalpaginas === false || $totalpaginas == NULL) {
        $totalpaginas = 0;
    }


    unlink($ruta_raiz . "$name_with_date.docx");

    $hashfilemetadada = hash_file('sha256', $FilePathPdf);

    $bytes = filesize($FilePathPdf);
    $kb = $helperBorra->formatSizeUnits($bytes);
    $kb = str_replace(",", "", $kb);

    return array('hashfilemetadada' => $hashfilemetadada, 'kb' => $kb, 'totalpaginas' => $totalpaginas);

}

function enviarEmailYaValido($ruta_raiz, $inforadicado, $radicado)
{
    $conjugacionValidar = json_decode($_SESSION['NOMBRE_BOTON_VALIDAR_CONJUGADO'], true);
    include_once $ruta_raiz . "/conf/configPHPMailer.php";
    $email = $_SESSION["usua_email"];
    $mail = new PHPMailer(true);
    try {
        //Server settings
        $mail->SMTPDebug = $debugPHPMailer;                                       // Enable verbose debug output
        $mail->isSMTP();                                            // Set mailer to use SMTP
        $mail->Host = $hostPHPMailer;  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                                   // Enable SMTP authentication
        $mail->Username = $usuarioEmailPQRS;                     // SMTP username
        $mail->Password = $passwordEmailPQRS;                               // SMTP password
        $mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
        $mail->Port = $portPHPMailer;                                    // TCP port to connect to

        //Recipients
        $mail->setFrom($admPHPMailer, $admPHPMailer);          // Name is optional
        $mail->addAddress($email, $email);     // Add a recipient


        // Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $textoAmbiente = $_SESSION["ambiente"] != 'produccion' ? 'PRUEBA:' : '';

        $mail->Subject = utf8_decode("$textoAmbiente Correo de confirmación");

        $asunto = $inforadicado[0]['RA_ASUN'];
        if (is_null($asunto)){
            $asunto = $inforadicado['ra_asun'];
        }

        $mail->Body = utf8_decode("$textoAmbiente Bien hecho! Ha ".strtolower($conjugacionValidar[9])." el documento, del radicado <b>$radicado</b>, Asunto: $asunto <br> <br>");
        $mail->AltBody = utf8_decode("$textoAmbiente Bien hecho! Ha ".strtolower($conjugacionValidar[9])." el documento: $radicado, asunto: $asunto");
        $enviar = $mail->send(); //quitar las barras de comentario

    } catch (Exception $e) {
        //$json['error'] = 'Ha ocurrido un error al enviar el correo con el código. '.$mail->ErrorInfo;
    }
}

function armarhashbarra($libreria = 'mpdf', $elemento, $rutahashbarra, $hashfile, $radi_info = null)
{
    $fuente = $_SESSION['EDITOR_FUENTE_TAMANIO'] != 'SELECCIONE' ? "'" . $_SESSION['EDITOR_FUENTE_TAMANIO'] . "'" : "'11'";
    $fuente_size_pie = $_SESSION['FUENTE_PIE_TAMANIO'] != 'SELECCIONE' ? $_SESSION['FUENTE_PIE_TAMANIO'] : 10;

    $html = '<br><table width="100%" style=""><tbody><tr>
<td style="width: 10%" ></td>
<td style="width: 80%"><div style="text-align:center!important;" >
<img  style="width: 600px;height: 40px;" src="' . $rutahashbarra . '"/>
</div>
</td>
<td style="width: 10%;"></td></tr></tbody></table>
<table width="100%" style=""><tbody><tr>
<td style="width: 10%;" ></td>
<td align="center" style="width: 80%;"><div style="width: 100%;text-align: center!important;  margin-bottom: -5px;" ><p style="text-align: center!important;font-size: '.$fuente_size_pie.' pt;">' . $hashfile;
if (!is_null($radi_info)){
    //$html .= '</p></div></td><td style="width: 30%;"></td></tr></tbody></table>';
    //$html .= '<table width="100%" style=""><tbody><tr><tr>
    //            <td style="width: 45%;" ></td>
    //            <td style="width: 10%;"><div style="width: 100%;text-align: center;  margin-bottom: -5px;" ><p style="text-align: center;font-size: 10px;">'. $radi_info ;
    //$html .= '</p></div></td><td style="width: 45%;"></td></tr></tbody></table>';
    $html .= '<br />'.$radi_info.'</p></div></td><td style="width: 10%;"></td></tr></tbody></table>';
}else{
$html .= '</p></div></td><td style="width: 10%;"></td></tr></tbody></table>';
}

    if ($libreria == 'mpdf') {
        $elemento->WriteHTML($html);
    } else if ($libreria == 'phpdocx') {
        $elemento->embedHTML($html);
    }


    return $elemento;
}

function crearPdfWithHash($helperBorra, $borrador, $radicado, $datosDestinatario, $titulomemo,
                          $historico, $hashtext, $FilePathPdf, $rutahashbarra = false, $inforadicado,
                          $document_authorized_by = array(), $configHelper)
{
    $imprimefirmantes = false;
    //$url = $configHelper.getConfiguraciones('LANDING_PAGE_URL'); 
    $radiInfo = $inforadicado[0]['SGD_RAD_CODIGOVERIFICACION']; 
    if (is_null($radiInfo)){
        $radiInfo = $inforadicado['sgd_rad_codigoverificacion'];
    }
    if (!(is_null($radiInfo)) && $radiInfo != '' && !(is_null($_SESSION['LANDING_PAGE_URL'])) && $_SESSION['LANDING_PAGE_URL'] != ''){
        $radiInfo = 'Codigo de Verificación CV: '. $radiInfo .' Comprobar desde: '. $_SESSION['LANDING_PAGE_URL'];
    }

    $firmantes = $helperBorra->getFirmByBorrador($borrador[0]['ID_BORRADOR']);
    $mpdf = $helperBorra->armarPdfRadicado('radicado', $radicado, $borrador, $datosDestinatario,
        $firmantes, $titulomemo, true, $imprimefirmantes, $inforadicado[0]['RADI_FECH_RADI']);

    $armarUsersValidaron = armarUsersValidaron($mpdf, $document_authorized_by, 'mpdf', $helperBorra, $radicado);
    $mpdf = $armarUsersValidaron['html'];


    $mpdf = $helperBorra->armarAprobRevisProy($mpdf, $historico, $helperBorra, 'mpdf',
        $document_authorized_by, $configHelper);

    /**
     * la siguiente expresion, arman los anexos folios y folios comunicacion
     */
    $mpdf = $helperBorra->armarAnexosFolInfo($mpdf, $name_library = 'mpdf', $borrador, $helperBorra);
    /************************************************
     */

    //si viene esta variable distinto de falso, entonces creo el hash y pego la imagen en el pdf
    if ($rutahashbarra != false) {
        $mpdf = armarhashbarra('mpdf', $mpdf, $rutahashbarra, $hashtext, $radiInfo);
    }

    $totalpaginas = count($mpdf->pages);
    $mpdf->Output($FilePathPdf);

    if (is_file($FilePathPdf) == false) {
        throw new Exception('El documento, no se pudo guardar: ' . $FilePathPdf);
    }
    $hashfilemetadada = hash_file('sha256', $FilePathPdf);

    $bytes = filesize($FilePathPdf);
    $kb = $helperBorra->formatSizeUnits($bytes);
    $kb = str_replace(",", "", $kb);

    return array('hashfilemetadada' => $hashfilemetadada, 'kb' => $kb, 'totalpaginas' => $totalpaginas);

}


//arma en el pdf o doc, los usuarios que validaron
//$name_library: le coloco por defecto mpdf
//$name_library: indica el nombre de la libreria que estoy usando, para luego, con la variable $libreria, lo agrego y lo retorno
function armarUsersValidaron($libreria, $document_authorized_by, $name_library = 'mpdf', $helperBorra, $radicado)
{
    $conjugacionValidar = json_decode($_SESSION['NOMBRE_BOTON_VALIDAR_CONJUGADO'], true);
    $fuente_size = $_SESSION['EDITOR_FUENTE_TAMANIO'] != 'SELECCIONE' ? $_SESSION['EDITOR_FUENTE_TAMANIO'] : 11;

    /**
     * hago la siguiente validacion, ya que en mpdf, si acepta la fuente con comillas simples adelante y detras,
     * pero con phpdocx que es la que se usa al momento de validar, no toma la fuente con esas comillas al inicio y al final
     */
    $comillaToFontFamly='';
    if($name_library=='mpdf'){
        $comillaToFontFamly="'";
    }

    $fuente = $_SESSION['EDITOR_FUENTE_TIPO'] != 'SELECCIONE' ? $comillaToFontFamly . $_SESSION['EDITOR_FUENTE_TIPO'] . $comillaToFontFamly : $comillaToFontFamly."Arial".$comillaToFontFamly;
    $html = '';


    if (count($document_authorized_by) > 0) {
        if ($name_library == 'mpdf') {
            $html .= "<br>";
        } else if ($name_library == 'phpdocx') {
        }

        //Si: La funcionalidad está habilitada FUNCTION_VALIDAR_RADICADO=SI
        // y si tiene almenos un tipo de radicación para validar: ej: TIPOS_RADICADO_VALIDAR  ["3"]
        if ($_SESSION["FUNCTION_VALIDAR_RADICADO"] == 'SI' &&
            $_SESSION['TIPOS_RADICADO_VALIDAR'] != '' && json_decode($_SESSION['TIPOS_RADICADO_VALIDAR']) != NULL) {
            $html .= $helperBorra->TildesHtml('<span style="font-weight: bold; font-family: ' . $fuente . ' !important; font-size:' . $fuente_size . 'pt">Documento ' . $radicado . ' '.strtolower($conjugacionValidar[9]).' por:</span>');

            if ($name_library == 'mpdf') {
                $html .= "<br><br>";
            } else if ($name_library == 'phpdocx') {
                $html .= "<br>";
            }

        }

        if ($name_library == 'mpdf') {
            $html.= '<table ><tbody>';
        } else if ($name_library == 'phpdocx') {
            $html.= '<table width="630px"><tbody>';
        }
        foreach ($document_authorized_by as $row) {
            $html.='<tr><td valign="top">';
            $html .= $helperBorra->TildesHtml('<span style=" font-size:' . $fuente_size . 'pt; font-family: ' . $fuente . '"><b>' . $row['USUA_NOMB'] . '</b>, ' . $row['USUA_CARGO'] . ', ' .
                $row['DEPE_NOMB'] . ', Fecha '.strtolower($conjugacionValidar[5]).': ' . date('d-m-Y H:i:s', strtotime($row['TIMESTAMP_F'])) . '<span>');
            $html.='</td></tr>';
        }
        $html.= '</tbody></table>';

    }
    if ($html != '') {
        if ($name_library == 'mpdf') {
            $libreria->WriteHTML($html);
        } else if ($name_library == 'phpdocx') {
            $libreria->embedHTML($html);
        }
    }

    return array('html' => $libreria);
}


//comprueba la validez de un documento
function comprobar_validez_doc()
{
    require_once '../../config.php';
    $ruta_raiz = "../../";
    if (isset($db)) unset($db);
    include_once("$ruta_raiz/include/db/ConnectionHandler.php");
    $db = new ConnectionHandler("$ruta_raiz");
    $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
    $borra = new \App\Helpers\Borradores($db);
    $helperRadi = new \App\Helpers\Radicados($db);
    $conjugacionValidar = json_decode($_SESSION['NOMBRE_BOTON_VALIDAR_CONJUGADO'], true);

    //whether ip is from share internet
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip_address = $_SERVER['HTTP_CLIENT_IP'];
    } //whether ip is from proxy
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } //whether ip is from remote address
    else {
        $ip_address = $_SERVER['REMOTE_ADDR'];
    }



    $ANEXO_ID = $_POST['ANEXO_ID'];
    $infoAnexo = $helperRadi->buscarAnexoById($ANEXO_ID);
    $radicado = $infoAnexo[0]['ANEX_RADI_NUME'];
    if ($infoAnexo[0]['RADI_NUME_SALIDA'] != null && $radicado = $infoAnexo[0]['RADI_NUME_SALIDA'] != '') {
        $radicado = $infoAnexo[0]['RADI_NUME_SALIDA'];
    }
    $json = array();
    $metadata = $helperRadi->getDocumentMetadata($radicado);
    if (count($metadata) > 0) {
        $inforadicado = $borra->getRadicadoByNume_radi($radicado);

        if(is_file($ruta_raiz . "bodega" . $inforadicado[0]['RADI_PATH']) == false){

            $json['error'] = 'El documento del radicado no existe';
            echo json_encode($json);
            exit();
        }

        if(is_file($ruta_raiz . "bodega" . $metadata[0]['RADI_PATH_ORIGINAL']) == false
        ){

            $json['error'] = 'El documento previamente validado, no existe';
            echo json_encode($json);
            exit();
        }



        $hash_radi_path = hash_file('sha256', $ruta_raiz . "bodega" . $inforadicado[0]['RADI_PATH']);
        $hash_doc_origin = hash_file('sha256', $ruta_raiz . "bodega" . $metadata[0]['RADI_PATH_ORIGINAL']);

        //var_dump($hash_doc_origin);
        //var_dump($metadata[0]['HASH_VALIDADO']);
        //$metadata[0]['HASH_DOC_ORIGINAL'] es el hash del documento sin el hash
        if ($hash_radi_path . $hash_doc_origin == $metadata[0]['HASH_VALIDADO']) {
            $json['success'] = 'Documento válido';
        } else {
            $json['error'] = 'Documento Modificado - Fallo de Integridad - Reportar a Gestión Documental y Soporte';
        }

        $textohash = $helperRadi->getMetaDataHash($radicado, $infoAnexo[0]['ANEX_CODIGO']);
        if (!isset($json['error'])) {
            if (hash('sha256', $textohash[0]['TEXTO']) == $metadata[0]['HASH_METADATA']) {
                $json['success'] = 'Documento válido ';
            } else {
                $json['error'] = 'Documento válido, pero metadatos modificados. Reportar a Gestión Documental y Soporte.';
            }
        }

        $usuarios = array();
        $fuevalidado = $helperRadi->getDocumentMetadata($radicado);
        $estatusValidation = 'ABIERTO';
        $comVal = 'INVALIDO';
        if (count($fuevalidado) > 0) {
            $estatusValidation = $fuevalidado[0]['STATUS_DOCUMENT'];
            $comVal = 'VALIDO';
            $usuarios = $helperRadi->getUsersValidaron($fuevalidado[0]['ID_RADICADO_METADATA']);
            if (count($usuarios) > 0) {
                $usuarios = $borra->barmarUsuariosComen($usuarios);
            }
        }

        $json['document_metadata'] = $fuevalidado;
        $json['usuarios'] = $usuarios;
        if ($estatusValidation == 'CERRADO'){
            $historicoValCom = $helperRadi->insertarHistoricoComp($radicado ,$comVal ,$ip_address,$fuevalidado[0]['FILETYPE_EXT'],$fuevalidado[0]['HASH_METADATA']);
            $historicoUltimo = array();
             $historicoUltimo['id'] =  $historicoValCom['response'];
            $historicoUltimo['document'] = $radicado;
            $historicoUltimo['date_verification'] = date("Y-m-d H:i:s");
            $historicoUltimo['result'] = $comVal;
            $json['ultimoHist'] = $historicoUltimo;

        }
    } else {
        $json['error'] = 'El documento no ha sido '.strtolower($conjugacionValidar[1]);
    }
    echo json_encode($json);
}

function verdocvalidado()
{
    require_once '../../config.php';
    $ruta_raiz = "../../";
    if (isset($db)) unset($db);
    include_once("$ruta_raiz/include/db/ConnectionHandler.php");
    $db = new ConnectionHandler("$ruta_raiz");
    $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
    $helperRadi = new \App\Helpers\Radicados($db);

    $ruta = '';
    $microtime = str_replace('.', '', microtime(true));
    $name = 'archivo' . $microtime . '.pdf';
    $radicado = $_GET['radicado'];
    $document_metadata = $helperRadi->getDocumentMetadata($radicado);
    if (count($document_metadata) > 0) {
        header('Content-Description: File Transfer');
        if (isset($_GET['tipo']) && $_GET['tipo'] == 'doc_original') {
            $ruta = '../../bodega' . $document_metadata[0]['RADI_PATH_ORIGINAL'];
            $name = 'documentooriginal' . $microtime . '.pdf';
        }

        if (isset($_GET['tipo']) && $_GET['tipo'] == 'doc_validado') {
            $ruta = '../../bodega' . $document_metadata[0]['RADI_PATH_VALIDADO'];
            $name = 'documentovalidado' . $microtime . '.pdf';
        }
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Type: application/pdf');
        header('Content-Disposition: attachment; filename=' . $name);

        //ob_clean();
        //flush();
        readfile($ruta);
        exit;
    }

}

function validaEstaEscaneado()
{


    require_once '../../config.php';
    $ruta_raiz = "../../";
    if (isset($db)) unset($db);
    include_once("$ruta_raiz/include/db/ConnectionHandler.php");
    $db = new ConnectionHandler("$ruta_raiz");
    $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
    $helperRadi = new \App\Helpers\Radicados($db);
    $helperBorra = new \App\Helpers\Borradores($db);
    $json = array();

    $radicados = $_POST['radicados'];
    $cont = 0;
    foreach ($radicados as $radicado) {
        $anexos = $helperRadi->getAnexosByRadicadoPadre($radicado);
        $fueescaneado = estaEscaneadoRad($radicado, $helperRadi);
        $radicados[$cont] = array();
        $radicados[$cont]['radicado'] = $radicado;
        $radicados[$cont]['hijos'] = array();
        $radicados[$cont]['fueescaneado'] = $fueescaneado;

        if ($anexos != null && count($anexos) > 0) {
            foreach ($anexos as $anexo) {

                $hijos = array();
                if (
                    $anexo['ANEX_SALIDA'] == 1
                    &&
                    $anexo['SGD_DOC_PADRE'] == 1
                    &&
                    ($anexo['ANEX_ESTADO'] == 2 || $anexo['ANEX_ESTADO'] == 3 || $anexo['ANEX_ESTADO'] == 4)
                    &&
                    $anexo['RADI_NUME_SALIDA'] != NULL && $anexo['RADI_NUME_SALIDA'] != $radicado
                ) {

                    $fueescaneado = estaEscaneadoRad($anexo['RADI_NUME_SALIDA'], $helperRadi);
                    $hijos['radi_hijo'] = $anexo['RADI_NUME_SALIDA'];
                    $hijos['fueescaneado'] = $fueescaneado;
                    $radicados[$cont]['hijos'][] = $hijos;
                }
            }
        }
        $cont++;
    }
    echo json_encode($radicados);

}


function estaEscaneadoRad($radicado, $helperRadi)
{
    $fueescaneado = 0;

    $escaneado = $helperRadi->radiescaneadouno($radicado);
    if ($escaneado[0]['COUNT'] > 0) {
        $fueescaneado = 1;
    }

    return $fueescaneado;
}

function clickEnBtnsValidar()
{
    require_once '../../config.php';
    $ruta_raiz = "../../";
    if (isset($db)) unset($db);
    include_once("$ruta_raiz/include/db/ConnectionHandler.php");
    $db = new ConnectionHandler("$ruta_raiz");
    $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
    $helperRadi = new \App\Helpers\Radicados($db);
    $helperBorra = new \App\Helpers\Borradores($db);
    $json = array();
    $anexo_id = $_POST['anexo_id'];
    $anexo = $helperRadi->buscarAnexoById($anexo_id);

    $fueescaneado = 0;
    $query_document_met = array();
    $borrador = array();
    $fueregenerado = 0;
    $radicado = '';
    $fecTransaccionDev = array();

    $pathinfoAnexo = false;
    $pathinfoRadPrincipal = false;
    if ($anexo != null && count($anexo) > 0 and $anexo[0]['RADI_NUME_SALIDA'] != NULL) {


        $radicado = $anexo[0]['RADI_NUME_SALIDA'];
        $infoRaidcado = $helperBorra->getRadicadoByNume_radi($radicado);
        $fueescaneado = estaEscaneadoRad($radicado, $helperRadi);
        $fecTransaccionDev = $helperBorra->getTransDevRad($radicado);
        //busco si tiene un borrador, para los radicados nuevos despues de esta funcionalidad.
        $borrador = $helperBorra->get_borradorByRadi($radicado);
        $query_document_met = $helperRadi->getDocumentMetadata($radicado);


        if ($anexo[0]['ANEX_REGENERADO'] != null && $anexo[0]['ANEX_REGENERADO'] != '') {
            $fueregenerado = $anexo[0]['ANEX_REGENERADO'] + 0;
        }

        /**
         * se guarda los path del anexo y del radicado, ya que si es modificar, se compara contra la del path principal.
         * Pero cuando la es accion Validar o re-generar, se compara el path contra el anexo
         */
        $pathinfoAnexo = pathinfo($anexo[0]['ANEX_NOMB_ARCHIVO']);
        $pathinfoRadPrincipal = pathinfo($infoRaidcado[0]['RADI_PATH']);

    }

    $json = array(
        'fueescaneado' => $fueescaneado,
        'query_document_met' => $query_document_met,
        'borrador' => $borrador,
        'fueregenerado' => $fueregenerado,
        'radicado' => $radicado,
        'anexo' => $anexo,
        'pathinfoAnexo' => $pathinfoAnexo,
        'pathinfoRadPrincipal' => $pathinfoRadPrincipal,
        'fecTransaccionDev' => $fecTransaccionDev[0]['FECHA_DEV']
    );
    echo json_encode($json);

}


function cerrarValidacion()
{

    require_once '../../config.php';
    $ruta_raiz = "../../";
    if (isset($db)) unset($db);
    include_once("$ruta_raiz/include/db/ConnectionHandler.php");
    $db = new ConnectionHandler("$ruta_raiz");
    $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
    $helperRadi = new \App\Helpers\Radicados($db);

    $ANEXO_ID = $_POST['ANEXO_ID'];
    $infoAnexo = $helperRadi->buscarAnexoById($ANEXO_ID);
    $radicado = $infoAnexo[0]['ANEX_RADI_NUME'];
    if ($infoAnexo[0]['RADI_NUME_SALIDA'] != null && $radicado = $infoAnexo[0]['RADI_NUME_SALIDA'] != '') {
        $radicado = $infoAnexo[0]['RADI_NUME_SALIDA'];
    }

    $query = "update document_metadata set status_document='CERRADO'
                      where radi_nume_radi=$radicado ";
    $guardar = $db->conn->execute($query);
    $estatus = 4; //cerrado

    //actualizo el dato sobre la tabla anexo
    $query = "update anexos set anex_estado=$estatus
                      where id=$ANEXO_ID ";
    $guardar = $db->conn->execute($query);

    $fuevalidado = $helperRadi->getDocumentMetadata($radicado);
    $json = array(
        'document_metadata' => $fuevalidado
    );
    echo json_encode($json);
}

function reAbrirValidacion()
{

    require_once '../../config.php';
    $ruta_raiz = "../../";
    if (isset($db)) unset($db);
    include_once("$ruta_raiz/include/db/ConnectionHandler.php");
    $db = new ConnectionHandler("$ruta_raiz");
    $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
    $helperRadi = new \App\Helpers\Radicados($db);

    $ANEXO_ID = $_POST['ANEXO_ID'];
    $infoAnexo = $helperRadi->buscarAnexoById($ANEXO_ID);
    $radicado = $infoAnexo[0]['ANEX_RADI_NUME'];
    if ($infoAnexo[0]['RADI_NUME_SALIDA'] != null && $radicado = $infoAnexo[0]['RADI_NUME_SALIDA'] != '') {
        $radicado = $infoAnexo[0]['RADI_NUME_SALIDA'];
    }

    $fuevalidado = $helperRadi->getDocumentMetadata($radicado);

    if ($fuevalidado[0]['STATUS_DOCUMENT'] == 'CERRADO'){

    $query = "update document_metadata set status_document='ABIERTO'
                      where radi_nume_radi=$radicado ";
    $guardar = $db->conn->execute($query);
    $estatus = 3; //abierto

    //actualizo el dato sobre la tabla anexo
    $query = "update anexos set anex_estado=$estatus
                      where id=$ANEXO_ID ";
    $guardar = $db->conn->execute($query);

    }

    $fuevalidado = $helperRadi->getDocumentMetadata($radicado);

    $json = array(
        'document_metadata' => $fuevalidado
    );
    echo json_encode($json);
}

function verPermisologia($json){
    require_once '../../config.php';
    $ruta_raiz = "../../";
    if (isset($db)) unset($db);
    include_once("$ruta_raiz/include/db/ConnectionHandler.php");
    $db = new ConnectionHandler("$ruta_raiz");
    $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);

    $numradi = null;
    if (isset($_GET['radicado'])){
        $numradi = $_GET['radicado'];
    }
    $numeExpe = null;
    if (isset($_GET['expediente'])){
        $numeExpe = $_GET['expediente'];
    }

    $isql = "select r.RADI_PATH, r.SGD_SPUB_CODIGO, u.CODI_NIVEL, u.USUA_NOMB,u.USUA_DOC,
         r.RADI_USU_ANTE, r.RADI_DEPE_ACTU
         from RADICADO r, USUARIO u
         where r.RADI_NUME_RADI='$numradi'
         and r.RADI_USUA_ACTU= u.USUA_CODI
         and r.RADI_DEPE_ACTU= u.DEPE_CODI";

    $rs = $db->conn->query($isql);
    $rutaRad = $rs->fields['RADI_PATH'];;
    $valorExp = '';
    if (is_null($numeExpe)){
        $consultaExpediente = "SELECT SGD_EXP_NUMERO  FROM SGD_EXP_EXPEDIENTE 
				     WHERE radi_nume_radi= $numradi AND sgd_exp_fech=(SELECT MAX(SGD_EXP_FECH) minFech  
				     from sgd_exp_expediente where radi_nume_radi= $numradi  and sgd_exp_estado<>2)  
				     and sgd_exp_estado<>2";
        $rsE = $db->conn->selectLimit($consultaExpediente, 10000)->getArray();
        $valorExp = $rsE[0]["SGD_EXP_NUMERO"];
    }else{
        $valorExp = $numeExpe;
    }


    $login = $_SESSION['krd'];
    $consulta1 = "SELECT upper(a.usua_nomb) as USUA_NOMB, a.id, upper(b.depe_nomb) AS DEPE_NOMB ,b.id as depe_id 
			   FROM usuario a,dependencia b 
			   WHERE  a.depe_codi=b.depe_codi 
			   AND  USUA_LOGIN='$login' ";
    $res1 = $db->conn->query($consulta1);
    $id_usuario = $res1->fields["ID"];

    $curl = curl_init();

    $paramsGetService = 'usuario_id='.$id_usuario;
    $paramsGetService .= '&entidad_valor='.$numradi;
    $paramsGetService .= '&tipo_entidad=RADICADO';
    if (!(is_null($numeExpe))){
        $paramsGetService .= '&entidad_valor_extra='.$numeExpe;
        $paramsGetService .= '&tipo_entidad_extra=EXPEDIENTE';
    }

    curl_setopt_array($curl, array(
        CURLOPT_URL => "http://34.228.216.86/back/integration/seguridad/entidad/consultar?".$paramsGetService,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "authorization: Bearer 21497beba40b3b115589733b21f7a0c0b68dc7194f1c925c296a026dbfa761e6",
            "cache-control: no-cache"
        ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
        $consultaServicio = "cURL Error #:" . $err;
    } else {
        $consultaServicio = json_decode($response) ;
    }



    $vecRadsD['verImg'] = $consultaServicio->acceso;
    $vecRadsD['verImgMsg'] = $consultaServicio->mensaje;
    $vecRadsD['pathImagen'] = $rutaRad;
    $vecRadsD['numExpe'] = $valorExp;
    $vecRadsD['segDep'] = (isset($consultaServicio->accesoConcededBy)) ? $consultaServicio->accesoConcededBy  : '';
    $vecRadsD['test'] = $consultaServicio->acceso;

    echo json_encode($vecRadsD);

}



if (isset($_POST['function_call']) and $_POST['function_call'] == "radExpedIncluidos") {

    radExpedIncluidos($_POST);
}

if (isset($_GET['function_call']) and $_GET['function_call'] == "usersPosibleAutoriza") {

    usersPosibleAutoriza($_GET);
}

if (isset($_GET['function_call']) and $_GET['function_call'] == "usersConfig") {

    usersConfig($_GET);
}

if (isset($_GET['function_call']) and $_GET['function_call'] == "buscarFirmantes") {

    buscarFirmantes($_GET);
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "ajaxRmOrAddFirmantes") {

    ajaxRmOrAddFirmantes($_POST);
}

if (isset($_GET['function_call']) and $_GET['function_call'] == "getFirmPrinc") {

    getFirmPrinc($_GET);
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "getUsersValidaron") {

    getUsersValidaron($_POST);
}
if (isset($_POST['function_call']) and $_POST['function_call'] == "enviarcodigoemail") {

    enviarcodigoemail($_POST);
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "accion_validar") {

    accion_validar($_POST);
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "comprobar_validez_doc") {

    comprobar_validez_doc($_POST);
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "clickEnBtnsValidar") {

    clickEnBtnsValidar($_POST);
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "cerrarValidacion") {

    cerrarValidacion($_POST);
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "reAbrirValidacion") {

    reAbrirValidacion($_POST);
}

if (isset($_GET['function_call']) and $_GET['function_call'] == "verdocvalidado") {

    verdocvalidado($_POST);
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "validaEstaEscaneado") {

    validaEstaEscaneado($_POST);
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "serviceSaveHistDespValidar") {

    serviceSaveHistDespValidar($_POST);
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "cleanSessionCorreo") {

    cleanSessionCorreo($_POST);
}

if (isset($_GET['function_call']) and $_GET['function_call'] == "verPermisologia") {

    verPermisologia($_GET);
}




