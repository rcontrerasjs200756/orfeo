<?php

use Philo\Blade\Blade;

require '../../vendor/autoload.php';
require_once '../../config.php';

$ruta_raiz = "../../";
if (isset($db)) unset($db);
include_once("$ruta_raiz/include/db/ConnectionHandlerNew.php");
$db = new ConnectionHandler("$ruta_raiz");

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
$views = $ABSOL_PATH.'app/views';
$cache = $ABSOL_PATH.'app/cache';

//Clearing the views cache
\App\Helpers\MiscFunctions::clear_view_cache();

$blade          = new Blade($views, $cache);
$radicadosHelper  = new \App\Helpers\Radicados($db);
$configsHelper = new \App\Helpers\Configuraciones($db);
$configs_anular=$configsHelper->getConfiguraciones(false,false,"'ANULACION'");

$msg="";
if(!isset($_SESSION))
{
    session_start();
}
$fechahoy = $_SESSION['CRON_FECHA_INICIAL'];

$ANULA_DIAS_TRAMITE=$configsHelper->getConfiguraciones("'ANULA_DIAS_TRAMITE'",false,false);
$poranular=$radicadosHelper->radPorAnular($fechahoy,$ANULA_DIAS_TRAMITE[0]['VALOR'],true);

//busco los radicados que no han sido notificados
$sinNotificar=$radicadosHelper->getRadicSinNotif();

if(count($sinNotificar)>0){
    $actualizo=$radicadosHelper->actNotifFechaRadAnul();
}

$notificHoy=$radicadosHelper->getRadicAnulNotifHoy();

echo $blade->view()->make('radicados.alertaanulados', compact(
    'poranular','notificHoy','fechahoy','configs_anular',
    'include_path',
    'msg'

))->render();



?>