<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require '../../vendor/autoload.php';
use Philo\Blade\Blade;

require_once '../../config.php';
$ruta_raiz = "../../";
if (isset($db)) unset($db);
include_once("$ruta_raiz/include/db/ConnectionHandlerNew.php");
$db = new ConnectionHandler("$ruta_raiz");
$db->conn->SetFetchMode(ADODB_FETCH_ASSOC);

$views = $ABSOL_PATH.'app/views';
$cache = $ABSOL_PATH.'app/cache';

//Clearing the views cache
\App\Helpers\MiscFunctions::clear_view_cache();

$blade = new Blade($views, $cache);
$dashboard_repo = new \App\Helpers\Dashboard($db);
$configHelper = new \App\Helpers\Configuraciones($db);
$habilitarBorradores=$configHelper->getConfiguraciones("'FUNCTION_BORRADORES'");

$pqr = $dashboard_repo->getPqr();
$without_file = $dashboard_repo->getWithoutFile();
$unclasified = $dashboard_repo->getUnclassified();
$unfinished = $dashboard_repo->getUnfinished();

$stats_data = $dashboard_repo->getStatsData();
$recent_radicated = $dashboard_repo->recentRadicated();
$graph_data = $dashboard_repo->assembleGraphData($stats_data);

echo $blade->view()->make(
    'dashboard.index',
    compact(
        'include_path',
        'unclasified',
        'without_file',
        'unfinished',
        'pqr',
        'stats_data',
        'recent_radicated',
        'graph_data',
        'habilitarBorradores'
    )
)->render();

?>