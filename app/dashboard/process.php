<?php

use App\Helpers\PDOconfig;
use Aura\SqlQuery\QueryFactory;
use Philo\Blade\Blade;
use App\Helpers\MiscFunctions;

require '../../vendor/autoload.php';

try {
    if($_POST['operation'] == "search_consolidated") {
        $from = \Carbon\Carbon::parse($_POST['from'])->format('Y-m-d H:i:s');
        $to = \Carbon\Carbon::parse($_POST['to'])->format('Y-m-d H:i:s');
        $response = searchConsolidated($from, $to);
    }
} catch(Exception $e) {
    echo "Error: ".$e->getMessage();
    $response= "ERROR";
}
echo $response;
exit;

function searchConsolidated($from, $to)
{
    require_once '../../config.php';
    $views = $ABSOL_PATH . 'app/views';
    $cache = $ABSOL_PATH . 'app/cache';

    //Clearing the views cache
    \App\Helpers\MiscFunctions::clear_view_cache();

    $blade = new Blade($views, $cache);
    $dashboard_repo = new \App\Helpers\Dashboard();
    $trd = $dashboard_repo->getTrdIndicator($from, $to);
    $canceled = $dashboard_repo->getCanceledIndicator($from, $to);
    $finished = $dashboard_repo->getFinishedIndicator($from, $to);
    $radicated = $dashboard_repo->getRadicatedIndicator($from, $to);
    $without_file = $dashboard_repo->getWithoutFileIndicator($from, $to);

    $trd_perc = 0;
    $finished_perc = 0;
    $canceled_perc = 0;
    $without_file_perc = 0;

    if ($radicated['TOTAL'] > 0) {
        $trd_perc = MiscFunctions::getPercentage($trd['TOTAL'], $radicated['TOTAL']);
        $finished_perc = MiscFunctions::getPercentage($finished['TOTAL'], $radicated['TOTAL']);
        $canceled_perc = MiscFunctions::getPercentage($canceled['TOTAL'], $radicated['TOTAL']);
        $without_file_perc = MiscFunctions::getPercentage($without_file['TOTAL'], $radicated['TOTAL']);
    }

    return $blade->view()->make(
        'dashboard.partials.consolidated',
        compact(
            'radicated',
            'finished',
            'finished_perc',
            'without_file',
            'without_file_perc',
            'trd',
            'trd_perc',
            'canceled',
            'canceled_perc'
        )
    )->render();
}

?>

