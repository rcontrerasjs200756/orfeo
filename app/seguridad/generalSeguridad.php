<?php

use App\Helpers\PDOconfig;
use Aura\SqlQuery\QueryFactory;
use Philo\Blade\Blade;

require '../../vendor/autoload.php';

session_start();

/*cada vez que se seleeciona o de desmarca una dependencia o un usuario*/
function updateSeguridad($post)
{

    ini_set('display_errors', 0);
    ini_set('display_startup_errors', 0);
    error_reporting(E_ALL);
    $json = array();
    try {
        require_once '../../config.php';

        $ruta_raiz = "../../";
        if (isset($db)) unset($db);
        include_once("$ruta_raiz/include/db/ConnectionHandlerNew.php");
        $db = new ConnectionHandler("$ruta_raiz");
        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        $seguridad = new \App\Helpers\Seguridad($db);

        if (isset($_POST['radicado']) &&isset($_POST['acccion'])
            && ($_POST['acccion']=="quitar" || $_POST['acccion']=="agregar")) {

            $depeOrUser=$_POST['tipo'];
            if($_POST['acccion']=="agregar"){

                $seguridad->addSeguridad($_POST['radicado'],$_POST['id'] ,$_SESSION['usuario_id'],$depeOrUser);
                $seguridad->updateSeguridadRad(1,$_POST['radicado']);

            }else{
                $seguridad->deleteSegById($_POST['radicado'],$_POST['id'],$depeOrUser);
                $buscarTodos=$seguridad->getSeguridadByRadi($_POST['radicado']);
            }

            $json['success']="Sus datos se han guardado con éxito";
        }else{
            $json['error'] = "Debe ingresar todos los datos";
        }

    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
        $response = "ERROR";
        $json['error'] = true;
    }
    echo json_encode($json);
}


/*actualiza solo el campo de seguridad en el radicado */
function updateOnlyRadSeg($post)
{

    ini_set('display_errors', 0);
    ini_set('display_startup_errors', 0);
    error_reporting(E_ALL);
    $json = array();
    try {
        require_once '../../config.php';
        $ruta_raiz = "../../";
        if (isset($db)) unset($db);
        include_once("$ruta_raiz/include/db/ConnectionHandlerNew.php");
        $db = new ConnectionHandler("$ruta_raiz");
        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        $seguridad = new \App\Helpers\Seguridad($db);
        $json['radicado']=array();

        include_once "$ruta_raiz/include/tx/Historico.php";

        $Historico = new Historico($db);
        $codiRegE=array();
        $codiRegE[0] = $_POST['radicado'];

        if (isset($_POST['radicado'])) {
            $observa = "Radicado Reservado";
            if($_POST['codigo']=="1"){
                $seguridad->updateSeguridadRad(1,$_POST['radicado']);
            }else{
                $observa = "Radicado Publico.";
                $seguridad->updateSeguridadRad(0,$_POST['radicado']);
            }
            $radiModi = $Historico->insertarHistorico($codiRegE, $_SESSION['dependencia'], $_SESSION["codusuario"], $_SESSION['dependencia'], $_SESSION["codusuario"], $observa, 54);
        }else{
            $json['error'] = "Debe ingresar todos los datos";
        }

    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
        $response = "ERROR";
        $json['error'] = true;
    }
    echo json_encode($json);

}

/*trae todos los datos de la taba seguridad, por radicado*/
function getAllSeguridad($post)
{

    ini_set('display_errors', 0);
    ini_set('display_startup_errors', 0);
    error_reporting(E_ALL);
    $json = array();
    try {
        require_once '../../config.php';
        $ruta_raiz = "../../";
        if (isset($db)) unset($db);
        include_once("$ruta_raiz/include/db/ConnectionHandlerNew.php");
        $db = new ConnectionHandler("$ruta_raiz");
        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        $seguridad = new \App\Helpers\Seguridad($db);
        $borrador = new \App\Helpers\Borradores($db);
        $json['radicado']=array();
        if (isset($_POST['radicado'])) {
            $buscarTodos=$seguridad->getSeguridadByRadi($_POST['radicado']);
            $json['success']=$buscarTodos;
            $json['radicado']=$borrador->getRadicadoByNume_radi($_POST['radicado']);
        }else{
            $json['error'] = "Debe ingresar todos los datos";
        }

    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
        $response = "ERROR";
        $json['error'] = true;
    }
    echo json_encode($json);

}


/*borra todo lo que hay en segurdad, de un radicado, cuando el usuario confirma que quiere eliminarlos y continuar en blanco*/
function borrarTodoSeguridad($post)
{

    ini_set('display_errors', 0);
    ini_set('display_startup_errors', 0);
    error_reporting(E_ALL);
    $json = array();
    try {
        require_once '../../config.php';
        $ruta_raiz = "../../";
        if (isset($db)) unset($db);
        include_once("$ruta_raiz/include/db/ConnectionHandlerNew.php");
        $db = new ConnectionHandler("$ruta_raiz");
        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        $seguridad = new \App\Helpers\Seguridad($db);
        $json['radicado']=array();
        if (isset($_POST['radicado'])) {
            $seguridad->updateSeguridadRad(1,$_POST['radicado']);
            $seguridad->deleteSegByRad($_POST['radicado']);
            $json['success'] = "Datos actualizados";
        }else{
            $json['error'] = "Debe ingresar todos los datos";
        }

    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
        $response = "ERROR";
        $json['error'] = true;
    }
    echo json_encode($json);

}




if (!isset($_SESSION["usuario_id"])) {

    $json = array(
        'error' => 'expiro'
    );
    echo json_encode($json);
    exit;
}




if (isset($_POST['function_call']) and $_POST['function_call'] == "updateSeguridad") {

    updateSeguridad($_POST);
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "getAllSeguridad") {

    getAllSeguridad($_POST);
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "updateOnlyRadSeg") {

    updateOnlyRadSeg($_POST);
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "borrarTodoSeguridad") {

    borrarTodoSeguridad($_POST);
}


