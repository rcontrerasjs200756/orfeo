<?php

use App\Helpers\PDOconfig;
use Aura\SqlQuery\QueryFactory;
use Philo\Blade\Blade;
require '../../vendor/autoload.php';

$json=array();
session_start();

ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
error_reporting(E_ALL);
try{
    require_once '../../config.php';
    $ruta_raiz = "../../";
    if (isset($db)) unset($db);
    include_once("$ruta_raiz/include/db/ConnectionHandlerNew.php");
    $db = new ConnectionHandler("$ruta_raiz");
    $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
    $borra = new \App\Helpers\Borradores($db);

    $seguridad     = new \App\Helpers\Seguridad($db);

    $quebuscar=$_GET['quebuscar'];
    $usuarioeenviar=array();
    $search=isset($_GET['search'])?$_GET['search']:'';
    if($quebuscar=="usuarios"){
        $usuarios  =  $seguridad->buscarUsuarios($search);

        if(isset($_GET['radicado']) && $_GET['radicado']!=''){
            /**
             *
             */
            $dest_especific=$helperBorra->getDestEspecificSalida($destinatario);
            $dest=array_merge($dest,$dest_especific);
        }

        $usuarioeenviar=$borra->barmarUsuariosComen($usuarios);
    }else{
        $usuarios  =  $seguridad->buscarDependencia($search);
        $usuarioeenviar=$borra->barmarUsuariosComen($usuarios);
    }
    echo json_encode($usuarioeenviar);
}catch(\Exception $e){
    return $e->getMessage();
}


?>

