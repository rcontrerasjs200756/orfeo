<?php
/*
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
*/
//Loading the needed elements
use App\Helpers\PDOconfig;
use Aura\SqlQuery\QueryFactory;
use Philo\Blade\Blade;
require '../../vendor/autoload.php';

try {
    if($_POST['operation'] == "process") {
        if($_POST['process_type'] == 1) {
            $response = process_form($_POST['number']);
        }else{
            $response = get_anexed($_POST['number']);
        }
    }
    if($_POST['operation'] == "a_process"){
        $response = process_a_data($_POST['data']);
    }
    if($_POST['operation'] == "b1_process"){
        $response = process_b1_data($_POST['data']);
    }
    if($_POST['operation'] == "b1_state_process"){
        $response = process_b1_state_data($_POST['data'], $_POST['obs']);
    }
    if($_POST['operation'] == "b2_process"){
        $response = process_b2_data($_POST['data']);
    }
    if($_POST['operation'] == "b2_show_selection"){
        $response = b2_show_selection($_POST['anex_code']);
    }
    if($_POST['operation'] == 'deliver_process'){
        $params = array();
        parse_str($_POST['obs'], $params);
        foreach($params['obs'] as $row){
            $obs[]= urldecode($row);
        }
        parse_str($_POST['op'], $params);
        foreach($params['op'] as $row){
            $op[]= $row;
        }
        parse_str($_POST['rad_number'], $params);
        foreach($params['rad_anex_code'] as $row){
            $rad_number[]= $row;
        }
        $response= save_delivered($rad_number, $op, $obs, $_POST['user']);
    }
    if($_POST['operation'] == 'receive_doc'){
        $params = array();
        parse_str($_POST['data'], $params);
        $receive= $params['receive'];
        $response= receive_docs($receive);
    }
    if($_POST['operation'] == 'signature_check_process'){
        $response= signature_check($_POST['user'], $_POST['pass']);
    }
    if($_POST['operation'] == 'update_doc_mng'){
        $response= update_doc_mng();
    }
    if($_POST['operation'] == 'update_ent_fis'){
        $response= update_ent_fis();
    }
    if($_POST['operation'] == 'cancel_receive'){
        $response= cancel_receive();
    }
}catch(Exception $e){
    echo "Error: ".$e->getMessage();
    $response= "ERROR";
}
echo $response;
exit;

function get_anexed($anexed){
    require_once '../../config.php';
    $views = $ABSOL_PATH.'app/views';
    $cache = $ABSOL_PATH.'app/cache';

    //Clearing the views cache
    \App\Helpers\MiscFunctions::clear_view_cache();

    $blade = new Blade($views, $cache);

    $radic_consult = new \App\Helpers\EntregaFisicos();
    $result = $radic_consult->get_anexed($anexed);
    $data= $radic_consult->get_radicated($anexed);
    $response["type"]= "anexed";
    $response["data"]= $blade->view()->make('entregaFisicos.partials.deliver', [
        'data'  => $data,
        'anex_count' => count($result),
        'radicado' => $anexed,
        'type' => 'anexed'
    ])->render();

    return json_encode($response);
}

function process_form($radicated)
{
    $radic_consult = new \App\Helpers\EntregaFisicos();
    $result = $radic_consult->process_number($radicated);
    return $result;
}

function process_a_data($data)
{
    try {
        require_once '../../config.php';
        $views = $ABSOL_PATH . 'app/views';
        $cache = $ABSOL_PATH . 'app/cache';

        //Clearing the views cache
        \App\Helpers\MiscFunctions::clear_view_cache();

        $blade = new Blade($views, $cache);

        $process = new \App\Helpers\EntregaFisicos();
        $result = $process->process_a_data(json_decode($data));

        $rad_info = json_decode($data);
        return $blade->view()->make('entregaFisicos.partials.deliver', [
            'data' => (array)$rad_info[0],
            'type' => 'a'
        ])->render();
    }catch(Exception $e){
        return "ERROR: ".$e->getMessage()." Line: ".$e->getLine()." File: ".$e->getFile();
    }
}

function process_b1_data($data)
{
    require_once '../../config.php';
    $views = $ABSOL_PATH.'app/views';
    $cache = $ABSOL_PATH.'app/cache';

    //Clearing the views cache
    \App\Helpers\MiscFunctions::clear_view_cache();

    $blade = new Blade($views, $cache);

    $rad_info= (array)json_decode($data);
    return $blade->view()->make('entregaFisicos.partials.deliver', [
        'data' => $rad_info,
        'type' => 'b1'
    ])->render();
}

function process_b1_state_data($data, $obs)
{
    require_once '../../config.php';
    $views = $ABSOL_PATH.'app/views';
    $cache = $ABSOL_PATH.'app/cache';

    //Clearing the views cache
    \App\Helpers\MiscFunctions::clear_view_cache();

    $blade = new Blade($views, $cache);

    $rad_info= (array)json_decode($data);
    return $blade->view()->make('entregaFisicos.partials.deliver', [
        'data' => $rad_info,
        'type' => 'b1_state',
        'obs' => $obs
    ])->render();
}

function process_b2_data($data)
{
    require_once '../../config.php';
    $views = $ABSOL_PATH.'app/views';
    $cache = $ABSOL_PATH.'app/cache';

    //Clearing the views cache
    \App\Helpers\MiscFunctions::clear_view_cache();

    $blade = new Blade($views, $cache);

    $rad_info= (array)json_decode($data);
    return $blade->view()->make('entregaFisicos.partials.b2_deliver', [
        'data' => $rad_info,
        'type' => 'b2'
    ])->render();
}

function b2_show_selection($anex_code)
{
    require_once '../../config.php';
    $views = $ABSOL_PATH.'app/views';
    $cache = $ABSOL_PATH.'app/cache';

    //Clearing the views cache
    \App\Helpers\MiscFunctions::clear_view_cache();
    $blade = new Blade($views, $cache);

    $process = new \App\Helpers\EntregaFisicos();
    $anex_data= $process->get_b2_anexed($anex_code);

    return $blade->view()->make('entregaFisicos.partials.deliver', [
        'data' => $anex_data,
        'type' => 'b2'
    ])->render();
}

function save_delivered($rad_number, $op, $obs, $user){
    $process = new \App\Helpers\EntregaFisicos();
    $anex_data= $process->save_delivered($rad_number, $op, $obs, $user);

    return $anex_data;
}

function signature_check($user, $pass){
    $process = new \App\Helpers\EntregaFisicos();
    $data= $process->signature_check($user, $pass);

    return $data;
}

function update_doc_mng(){
    require_once '../../config.php';
    $views = $ABSOL_PATH.'app/views';
    $cache = $ABSOL_PATH.'app/cache';

    //Clearing the views cache
    \App\Helpers\MiscFunctions::clear_view_cache();
    $blade = new Blade($views, $cache);

    $process = new \App\Helpers\EntregaFisicos();
    $data = $process->update_doc_mng();

    return $blade->view()->make('entregaFisicos.partials.doc_mng', [
        'data' => $data,
        'type' => 'b2'
    ])->render();
}

function update_ent_fis(){
    require_once '../../config.php';
    $views = $ABSOL_PATH.'app/views';
    $cache = $ABSOL_PATH.'app/cache';

    //Clearing the views cache
    \App\Helpers\MiscFunctions::clear_view_cache();
    $blade = new Blade($views, $cache);

    $delivered= \App\Helpers\MiscFunctions::getDelivered();

    return $blade->view()->make('entregaFisicos.partials.ent_fis', compact('delivered'))->render();
}

function receive_docs($data){
    try {
        $process = new \App\Helpers\EntregaFisicos();
        $result = $process->receive_docs($data);

        return $result;
    }catch (Exception $e){
        return "ERROR: ".$e->getMessage()." Line: ".$e->getLine()." File: ".$e->getFile();
    }
}
//Cancel the receive process
function cancel_receive(){
    $process    = new \App\Helpers\EntregaFisicos();
    $result     = $process->cancel_receive();

    return $result;
}
?>

