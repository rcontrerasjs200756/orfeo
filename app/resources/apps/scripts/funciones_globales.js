var body_vista_previa = "", bodypaddings_vista_previa = "", header_vista_previa = "",
    headerheight_vista_previa = "",
    height_vista_previa = "", modalheight_vista_previa = "", modal_vista_previa = "";

// Opera 8.0+
var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;

// Firefox 1.0+
var isFirefox = typeof InstallTrigger !== 'undefined';

// Safari 3.0+ "[object HTMLElementConstructor]"
var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) {
    return p.toString() === "[object SafariRemoteNotification]";
})(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification));

// Internet Explorer 6-11
var isIE = /*@cc_on!@*/false || !!document.documentMode;

// Edge 20+
var isEdge = !isIE && !!window.StyleMedia;

// Chrome 1 - 71
var isChrome = !!window.chrome && (!!window.chrome.webstore || !!window.chrome.runtime);

// Blink engine detection
var isBlink = (isChrome || isOpera) && !!window.CSS;

var navegadorActual = "";

var modalFirmantesDesde = 'BORRADOR';



var languageseelct2 = {
    inputTooShort: function () {
        return 'Ingrese para buscar';
    },
    noResults: function () {
        return "Sin resultados";
    },
    searching: function () {
        return "Buscando...";
    },
    errorLoading: function () {
        return 'El resultado aún no se ha cargado.';
    },
    loadingMore: function () {
        return 'Cargar mas resultados...';
    },
}

function definirNavegador() {
    if (isOpera == true) {
        navegadorActual = "isOpera"
    }
    if (isFirefox == true) {
        navegadorActual = "isFirefox"
    }
    if (isSafari == true) {
        navegadorActual = "isSafari"
    }
    if (isIE == true) {
        navegadorActual = "isIE"
    }
    if (isChrome == true) {
        navegadorActual = "isChrome"
    }
    if (isBlink == true) {
        navegadorActual = "isBlink"
    }
}
//esta funcion capitaliza la primera letra de un string
function capitalizarPrimeraLetra(str) {
    return str.charAt(0).toUpperCase() + str.slice(1).toLowerCase();
}
function retornarParteCadenaSplit(cadenaCompleta,separador,indice){
    var cadenaDividida = dividirCadena(cadenaCompleta,separador);
    return cadenaDividida[indice];
}

function dividirCadena(cadenaADividir,separador) {
    return cadenaADividir.split(separador);
}

//esta funcion determina eltamano del modal de vista previa
function retornoSizeVistaPrevia(id_modal_vista_previa, id_header_vista_previa, id_modal_body_vista_previa) {

    if (body_vista_previa == "") {
        modal_vista_previa = $('#' + id_modal_vista_previa)
        header_vista_previa = $("#" + id_header_vista_previa, modal_vista_previa);
        body_vista_previa = $("#" + id_modal_body_vista_previa, modal_vista_previa);
        modalheight_vista_previa = parseInt(modal_vista_previa.css("height"));
        headerheight_vista_previa = parseInt(header_vista_previa.css("height")) + parseInt(header_vista_previa.css("padding-top")) + parseInt(header_vista_previa.css("padding-bottom"));
        bodypaddings_vista_previa = parseInt(body_vista_previa.css("padding-top")) + parseInt(body_vista_previa.css("padding-bottom"));
        height_vista_previa = modalheight_vista_previa - headerheight_vista_previa - bodypaddings_vista_previa + 40;
    }
}

//determina la ruta del pdf que se va a mostrar en vista previa
//esto es para que muestre el pdf en eun modal
function showModalVistaPrevia(ruta, tipodoc="docx", id_modal_vista_previa, id_header_vista_previa, id_modal_body_vista_previa, embed) {

    retornoSizeVistaPrevia(id_modal_vista_previa, id_header_vista_previa, id_modal_body_vista_previa);

    $('#' + id_modal_vista_previa).css('height', height_vista_previa + "px");
    $('#' + id_modal_body_vista_previa).html('');
    $('#' + id_modal_body_vista_previa).html('<embed src="" id="' + embed + '" ' +
        '                           frameborder="0" style="width:100%; height:100%">');
    $('#' + id_modal_body_vista_previa).css('height', height_vista_previa + "px");
    $('#' + embed).attr('src', ruta);
    $('#' + id_modal_vista_previa).modal('show');
    $('#' + id_modal_vista_previa).css('z-index', '9999');

}

function muestraDocVistaPrevia(ruta, tipodoc){
    if (ruta != "" && (tipodoc == "pdf" || tipodoc == "PDF")) {


        showModalVistaPrevia(ruta, tipodoc, 'modal_vista_previa', 'header_vista_previa',
            'modal_body_vista_previa', 'embed');
        return true;

    }
}


function modalSeleccCargos(desde, modal, tbodycargos, claseeditables, id_borra) {

    if(Object.keys(firmantesselected).length<1){
        Swal.fire("Alerta", "Debe seleccionar al menos un usuario", "warning");
        return false;
    }

    modalFirmantesDesde = desde;

    if (modalFirmantesDesde == 'BORRADOR' && id_borra == "") {
        Swal.fire("Alerta", "Primero debe guardar el borrador", "warning");
        return false;
    } else {
        if (modalFirmantesDesde == 'BORRADOR' && codusuario != borradorActual.ID_USUARIO_ACTUAL) {
            return false;
        }
        var html = "";
        var cargo = "";
        var principal = "";
        //verifico si hay algun firmante en Remitente, porque si no, lo coloco por defecto en el select
        var encontroremitente = false;
        for (var i = 0; i < Object.keys(firmantesselected).length; i++) {
            if (firmantesselected[i]['TIPO_VALIDADOR'] != undefined &&
                firmantesselected[i]['TIPO_VALIDADOR'] == "Remitente") {
                encontroremitente = true;
            }
        }

        for (var i = 0; i < Object.keys(firmantesselected).length; i++) {
            html += "<tr>"
            html += "<td>" + firmantesselected[i]['text'] + "</td>"
            html += "<td>"
            cargo = "";
            principal = "";
            if (firmantesselected[i]['FIRMANTE_CARGO'] != undefined) {
                cargo = firmantesselected[i]['FIRMANTE_CARGO']
            } else {
                cargo = firmantesselected[i]['USUA_CARGO'];
            }
            //como no me importa el nombre, y al presionar el boton azul se envian name, pk, y el value, entonces us
            //el nombre para guardar el borrador
            var dataDisabled = "";

            console.log('cargo',cargo)
            if(
                configsHasValue['CARGO_EDITABLE'] == 'NO'
                &&
                (cargo!=null  && cargo.length > 3 )
            ){
                dataDisabled = "data-disabled='true' ";
            }

            html += "<a href='#'  class='" + claseeditables + "' " +
                " data-original-title='Ingrese un cargo' " +
                "  data-pk='" + firmantesselected[i]['ID'] + "' " +
                " data-name='" + id_borra + "'  data-value='" + cargo + "' " +
                "  data-firmante='" + firmantesselected[i]['ID'] + "' " +
                " data-url='" + base_url + "app/borradores/guardarCargosFirmantes.php' " +
                "data-title='Ingrese un cargo' "+
                dataDisabled + " ></a></td>"

            if (firmantesselected[i]['FIRMANTE_PRINCIPAL'] != undefined
                && firmantesselected[i]['FIRMANTE_PRINCIPAL'] == "1") {
                principal = "checked";
            }

            html += "<td><input type='radio' class='icheck checkmodalfirmantes' onclick='actFirmantePrinci(this," + id_borra + ")' data-firmante='" + firmantesselected[i]['ID'] + "' " +
                "data-borrador_id='" + id_borra + "'  " +
                " name='firmante_principal' " + principal + " /></td>";

            html += "<td><select class='form-control' id='tipo_validador_" + firmantesselected[i]['ID'] + "' " +
                " onchange='beforeUpdateColumFirmantes(this," + id_borra + ", " + firmantesselected[i]['ID'] + ")' >" +
                "<option value='' selected >Seleccione</option> ";
            html += "<option value='Remitente' ";

            if ((firmantesselected[i]['TIPO_VALIDADOR'] != undefined && firmantesselected[i]['TIPO_VALIDADOR'] == 'Remitente')
                || Object.keys(firmantesselected).length == 1
                || encontroremitente == false) {

                encontroremitente = true;
                html += "selected";
                //le actualizo al primero con Remitente, si entra en alguna de las condiciones de arriba.
                updateColumFirmantes('Remitente', id_borra, firmantesselected[i]['ID'])
            }
            html += " >Remitente</option>" +
                "<option value='Revisión' ";
            firmantesselected[i]['TIPO_VALIDADOR'] != undefined && firmantesselected[i]['TIPO_VALIDADOR'] == 'Revisión' ? html += "selected" : html += "";
            html += " >Revisión</option>" +
                "<option value='Aprobación' ";
            firmantesselected[i]['TIPO_VALIDADOR'] != undefined && firmantesselected[i]['TIPO_VALIDADOR'] == 'Aprobación' ? html += "selected" : html += "";
            html += " >Aprobación</option>" +
                "</select>" +
                "</td>"
            html += "</tr>"
        }

        $("#" + tbodycargos).html('');
        $("#" + tbodycargos).html(html)


        // editable
        /*recorro cada item del editable*/
        $('a.' + claseeditables).each(function () {

            $(this).editable({
                inputclass: "form-control input-medium",
                placeholder: 'Escriba un cargo',
                allowClear: true,
                emptytext: "Escriba un cargo",
                success: function (response, cargo_input) {
                    var obj = JSON.parse(response);

                    //actualizo el arreglo de firmantes, al arreglo principal
                    if (obj.success != undefined) {
                        for (var i = 0; i < Object.keys(firmantesselected).length; i++) {
                            if (firmantesselected[i]['id'] == obj.firmante_id) {

                                firmantesselected[i]['FIRMANTE_CARGO'] = cargo_input
                            }
                        }
                    }

                },

            });
        })

        $("#" + modal).modal({show: true, keyboard: false, backdrop: 'static'});
        $("#" + modal).css('z-index', '9999');
    }
}


//actualiza el firmante principal de un borrador
function actFirmantePrinci(esto, id_borra) {

    if ($('#tipo_validador_' + $(esto).attr('data-firmante')).val() != 'Remitente') {
        alertModal('Alerta!', 'El firmante principal, debe ser un Remitente', 'warning', 5000)
        $(esto).prop("checked", false);
        return false;
    }

    $.ajax({
        url: base_url + "app/borradores/generalborrador.php",
        method: "POST",
        data: {
            id_borrador: id_borra, firmante: $(esto).attr('data-firmante'),
            function_call: "actFirmantePrinci"
        },
        dataType: 'json',
        async: false,
        success: function (response) {
            //actualizo el arreglo de firmantes, al arreglo principal
            for (var i = 0; i < Object.keys(firmantesselected).length; i++) {
                if (firmantesselected[i]['id'] == $(esto).attr('data-firmante')) {
                    firmantesselected[i]['FIRMANTE_PRINCIPAL'] = "1"
                }
            }
            Swal.fire("Firmante principal", "Seleccionado correctamente.", "success");
        },
        error: function (response) {
            Swal.fire("Error", "Ha ocurrido un error al actualizar el firmante principal", "error");
        }
    });
}

//cuando se cierra sobre el modal de seleccionar firmantes
//moduloDesde es el modulo desde donde se esta mandando a cerrar el modal
function cerrarModalFirmantes(moduloDesde, modal) {

    var encontroprincipal = false;
    var encontroremitente = false;
    for (var i = 0; i < Object.keys(firmantesselected).length; i++) {
        if (firmantesselected[i]['FIRMANTE_PRINCIPAL'] != undefined &&
            firmantesselected[i]['FIRMANTE_PRINCIPAL'] == "1") {
            encontroprincipal = true;
        }

        if (firmantesselected[i]['TIPO_VALIDADOR'] != undefined &&
            firmantesselected[i]['TIPO_VALIDADOR'] == "Remitente") {
            encontroremitente = true;
        }
    }

    if (encontroprincipal == false) {
        Swal.fire("Alerta", "Debe seleccionar un firmante Principal", "warning");
        return false;
    } else if (encontroremitente == false) {
        Swal.fire("Alerta", "Debe seleccionar al menos un Remitente", "warning");
        return false;
    } else {
        $("#" + modal).modal('hide')
        if (moduloDesde == 'BORRADOR') {
            vaccion = 'guardar';
            crearBorrador('',vaccion);
        }

    }
}


function beforeUpdateColumFirmantes(esto, id_borrador, id_firmante) {
    updateColumFirmantes($(esto).val(), id_borrador, id_firmante)
}

//actualiza en la tabla borrador_firmantes la columa nueva, en la vista llamada Firmantes
function updateColumFirmantes(valor, id_borrador, id_firmante) {
    console.log('sdsd',firmantesselected)
    $.ajax({
        url: base_url + "app/borradores/generalborrador.php",
        method: "POST",
        data: {
            id_borrador: id_borrador, id_firmante: id_firmante, valor: valor,
            function_call: "actFirmantetipo_validador"
        },
        dataType: 'json',
        async: false,
        success: function (response) {

            //actualizo el arreglo de firmantes, al arreglo principal
            for (var i = 0; i < Object.keys(firmantesselected).length; i++) {
                if (firmantesselected[i]['ID'] == id_firmante) {
                    firmantesselected[i]['TIPO_VALIDADOR'] = '';
                    firmantesselected[i]['TIPO_VALIDADOR'] = valor
                }
            }

        },
        error: function (response) {
            Swal.fire("Error", "Ha ocurrido un error al actualizar los datos", "error");
        }
    });
}


function resetearVariables() {
    console.log('roger');

    if (inputexpediente == "") {
        console.log('no val');
        inputexpediente = "";
        inputtipologia = "";
    }else if (inputexpediente.val() == "") {
        console.log('val');
        inputexpediente.val('');
        inputtipologia.val('');
        $('#divinputtipologia').hide();
    }

    buscartodo = false;
    seleccexped = false; //para saber si ya hizo click al select de los expedientes
    selecttipologia = false;//para saber si ya hizo click al select de las tipologias
    tipologiaselected = "";
    expedienteselected = "";
    expedientestabla = new Array();
    yaincluyouno = false; //para saber si ya incluyo uno, y habilitar la caja de tiplogias
    tablalistacexpedientes = null;
}

//cuando se presiona el botoncerrar de busqueda avanzada
function cerrarBusquedaAvanz() {
    setTimeout(function () {
        //$("#divinputtipologia").css('display','block');//muestro el input de tipologia
        $("#divbuttonbusquedaAv").css('display', 'block');//muestro el enlace de busqueda avanzada
        $("#span_coincidencia_exped").html($("#inputexpediente").val())
        $(".divBusquedaAvanzada").css('display', 'none');//el div completo lo oculto
        $("#divinputexpediente").css('display', 'block')

        // arreglo=0=> incluidos, 1=> cerrados, 2=>abiertos
        //valido si hay al menos un expddiente incluido, muestro el div de las tipoogias
        var incluidosExcluidos = cuantosEnExped();
        if (incluidosExcluidos[0] > 0) {
            $("#divinputtipologia").css('display', 'block');
        }

    }, 0)

}

/*retorna cuantos hay incluidos, abiertos y cerrados, en la tabla principal*/
function cuantosEnExped() {

    var incluidos = 0;
    var cerrados = 0;
    var abiertos = 0;
    var retorno = new Array();
    expedientesincluidos = new Array();
    var contExpedincluidos = 0;
    expedientesincluidosComa = "";
    for (var i = 0; i < Object.keys(expedientestabla).length; i++) {

        //si el elemento i trae el indice incluiExpe es porque ya esta incluido
        if (expedientestabla[i]['incluiExpe'] != undefined && expedientestabla[i]['incluiExpe'] == "incluido") {
            incluidos++;
            expedientesincluidos[contExpedincluidos] = expedientestabla[i].NUMERO_EXPEDIENTE

            expedientesincluidosComa += "'" + expedientestabla[i].NUMERO_EXPEDIENTE + "'";
            contExpedincluidos++;
            expedientesincluidosComa += ",";

        } else {
            //si entra aqui es porque no esta incluido, y puede estar cerrado o abierto para incluir
            if (expedientestabla[i]['ESTADO'] == "Abierto") {
                abiertos++;
            }
            if (expedientestabla[i]['ESTADO'] == "Cerrado") {
                cerrados++;
            }
        }

    }
    retorno[0] = incluidos;
    retorno[1] = cerrados;
    retorno[2] = abiertos;
    $("#expedincluidcoma").val(expedientesincluidosComa);
    return retorno;
}

function DeleteExpedientestabla(){
    tablalistacexpedientes.clear().draw();
    tablalistacexpedientes.destroy();

    //tablalistacexpedientes = null;
}



function cerrarModalExpediente() {
    //cierra el modal de expediente, pero antes valida si ya se selecciono una tipologia  si ya incluyo un expediente
    if (tipologiaselected == "" && yaincluyouno == true) {

        var encontroincluido = false;
        for (var i = 0; i < Object.keys(expedientestabla).length; i++) {
            if (expedientestabla[i]['incluiExpe'] == "incluido") {
                encontroincluido = true;
            }
        }

        if (encontroincluido == true) {
            Swal.fire("Alerta", "Debe seleccionar una tipología", "warning");
        } else {
            $("#modal_expediente").modal('hide');
        }

    } else {
        $("#modal_expediente").modal('hide');
    }
}

function mostrabusquedaAvanz(html) {
    $("#divApendBusqAvan").html(html)
    $("#divApendBusqAvan").css('display', 'block');
    $("#divbuttonbusquedaAv").css('display', 'block')
    $("#span_coincidencia_exped").html($("#inputexpediente").val())
}

function definicionTablas(tablaDefinir){
   
    return tablaDefinir.DataTable({
        autoWidth: true,
        "order": [],
        "searching": false,
        "lengthChange": false,
        "columnDefs": [{
            "targets": 'no-sort',
            "orderable": false,
            "searchable": false,
            "targets": [0, 1]
        }],
        "language": {
            "emptyTable": "No se encontraron registros",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ resultados",
            "infoEmpty": "",
            "infoFiltered": "(filtrado de _MAX_ total resultados)",
            "infoPostFix": "",
            "thousands": ",",
            "lengthMenu": "Mostrar _MENU_ resultados",
            "loadingRecords": "Cargando...",
            "processing": "Buscando...",
            "search": "Buscar en los resultados:",
            "zeroRecords": false,
            "paginate": {
                "first": "<<",
                "last": ">>",
                "next": "<",
                "previous": ">"
            },
            "aria": {
                "sortAscending": ": activar ordenar columnas ascendente",
                "sortDescending": ": activar ordenar columnas descendente"
            }
        }
    });
}

function definirTablaAnexos(tabla){
    tablaAnexosTipos = null;
    tablaAnexosTipos = definicionTablas(tabla);

}

function definirTablaHistorico(tabla){
    tablaHistoricoC = null;
    tablaHistoricoC = definicionTablas(tabla);

}

function definirTablaTipologia(){
    tablalistaTipologias = null;
    tablalistaTipologias = $('#tablatipologias').DataTable({
        autoWidth: true,
        "order": [],
        "columnDefs": [{
            "targets": 'no-sort',
            "orderable": false,
        }],
        "language": {
            "emptyTable": "No se encontraron registros",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ resultados",
            "infoEmpty": "Mostrando 0 a 0 de 0 resultados",
            "infoFiltered": "(filtrado de _MAX_ total resultados)",
            "infoPostFix": "",
            "thousands": ",",
            "lengthMenu": "Mostrar _MENU_ resultados",
            "loadingRecords": "Cargando...",
            "processing": "Buscando...",
            "search": "Buscar en los resultados:",
            "zeroRecords": false,
            "paginate": {
                "first": "<<",
                "last": ">>",
                "next": "<",
                "previous": ">"
            },
            "aria": {
                "sortAscending": ": activar ordenar columnas ascendente",
                "sortDescending": ": activar ordenar columnas descendente"
            }
        }
    });
}


function definirTablaExpediente(columnaOne = 'Archivar') {

    $("#open_table_expediente").html('');

    $("#open_table_expediente").append('   <table class="table table-striped table-bordered table-hover ' +
        'table-checkable order-column dataTable no-footer" id="tablaexpedientes" > <thead class="bg-blue font-white bold"> ' +
        '<tr role="row"> <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" ' +
        'aria-label=" Id : activate to sort column ascending" style="width: 20%;text-align:center"> '+columnaOne+' </th>' +
        '<th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1"' +
        ' aria-label=" Asunto : activate to sort column ascending" style="width: 40%;text-align:center"> Expediente </th> ' +
        '<th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1"' +
        ' aria-label=" Estado : activate to sort column ascending" style="width: 40%;text-align:center">' +
        'Dependencia y TRD </th> </tr> </thead> <tbody id="tbodyexpediente"></tbody> </table>');

    tablalistacexpedientes = null;
    console.log('creando datatable');
    tablalistacexpedientes = $('#tablaexpedientes').DataTable({
        autoWidth: false,
        "columns": [
            {"width": "20%"},
            {"width": "40%"},
            {"width": "40%"},
        ],
        "order": [],
        "columnDefs": [{
            "targets": 'no-sort',
            "orderable": false,
        }],
        "language": {
            "emptyTable": "No se encontraron registros",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ resultados",
            "infoEmpty": "Mostrando 0 a 0 de 0 resultados",
            "infoFiltered": "(filtrado de _MAX_ total resultados)",
            "infoPostFix": "",
            "thousands": ",",
            "lengthMenu": "Mostrar _MENU_ resultados",
            "loadingRecords": "Cargando...",
            "processing": "Buscando...",
            "search": "Buscar en los resultados:",
            "zeroRecords": false,
            "paginate": {
                "first": "<<",
                "last": ">>",
                "next": "<",
                "previous": ">"
            },
            "aria": {
                "sortAscending": ": activar ordenar columnas ascendente",
                "sortDescending": ": activar ordenar columnas descendente"
            }
        }
    });

    $('.dataTables_filter input')
        .attr('title', 'Busca abajo en los resultados de las anteriores búsquedas.')
}


function alertModal(title=false, message, type, cerrarantesde) {

    var titulo = '';
    if (type == 'error') {
        var loaderBg = '#ff6849';
        var icon = 'error';
        titulo = 'ERROR'
    }
    if (type == 'success') {
        titulo = 'FELICIDADES'
    }
    if (type == 'warning') {
        titulo = 'ATENCION'
    }
    if (type == 'info') {
        titulo = 'INFORMACION'
    }

    if (title != false) {
        titulo = title
    }

    var hideAfter = 4500;
    if (cerrarantesde != true) {
        hideAfter = cerrarantesde;
    }
    if (cerrarantesde == false) {
        hideAfter = false;
    }
    /* return  $.toast({
        heading: titulo,
        text: message,
        position: 'top-right',
        loaderBg: loaderBg,
        icon: icon,
        hideAfter: hideAfter,
        stack: 6

    });*/

    return toastr[type](message, titulo, {
        "closeButton": true,
        "debug": false,
        "positionClass": "toast-top-right",
        "onclick": null,
        "showDuration": "1000",
        "hideDuration": "1000",
        "timeOut": hideAfter,
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    })
}


function copyTextToClipboard(text,mensajeamostrar) {
    //var textArea = document.createElement("textarea");
    console.log(text);

    var htmlintext ="<input type='text' id='testr' value='"+text+"' >";
    $( "#modal_nuevoexpediente" ).append( htmlintext);
    console.log($( "#testr" ).val());
    $( "#testr" ).select();
    try {
        var successful = document.execCommand('copy');
        var msg = successful ? 'successful' : 'unsuccessful';
        console.log('Copying text command was ' + msg);
        alertModal('Éxito!', mensajeamostrar, 'success', 5000)

    } catch (err) {
        console.log('Oops, unable to copy');
    }
    $( "#testr" ).remove();


}


function getOS() {
    var userAgent = window.navigator.userAgent,
        platform = window.navigator.platform,
        macosPlatforms = ['Macintosh', 'MacIntel', 'MacPPC', 'Mac68K'],
        windowsPlatforms = ['Win32', 'Win64', 'Windows', 'WinCE'],
        iosPlatforms = ['iPhone', 'iPad', 'iPod'],
        os = null;

    if (macosPlatforms.indexOf(platform) !== -1) {
        os = 'Mac OS';
    } else if (iosPlatforms.indexOf(platform) !== -1) {
        os = 'iOS';
    } else if (windowsPlatforms.indexOf(platform) !== -1) {
        os = 'Windows';
    } else if (/Android/.test(userAgent)) {
        os = 'Android';
    } else if (!os && /Linux/.test(platform)) {
        os = 'Linux';
    }

    return os;
}

function getBrowser(){

        var ua= navigator.userAgent, tem,
            M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
        if(/trident/i.test(M[1])){
            tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
            return 'IE '+(tem[1] || '');
        }

        if(M[1]=== 'Chrome'){
            tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
            if(tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
        }

        M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
        if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
        return M.join(' ');

}


/**
 * Formatea una fecha a YYYY-MM-DD
 * @param date
 * @returns {string}
 */

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [year, month, day].join('-');
}

/**
 * Verifica si un usuario espeficifo, está en la variable de configuracion: USUARIOS_FINALIZAR_TODO
 */
function estoyEnUsuariosFinalizarTodo(configJson,usualogin){

    var estoy = false;
    for (var i = 0; i < Object.keys(configJson).length; i++) {
        if (configJson[i]==usua_login) {
            estoy = true;
        }
    }

    return estoy;

}

function Validate(oFile,_validFileExtensions) {

        var oInput = oFile;
        if (oInput.type == "file") {
            var sFileName = oInput.value;
            if (sFileName.length > 0) {
                var blnValid = false;
                for (var j = 0; j < _validFileExtensions.length; j++) {
                    var sCurExtension = _validFileExtensions[j];
                    if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                        blnValid = true;
                        break;
                    }
                }
            }
        }


    return blnValid;
}


definirNavegador()
