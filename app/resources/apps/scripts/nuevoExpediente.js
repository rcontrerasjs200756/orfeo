var BtnNewEx = '<label class=" col-md-2">' +
    '<button type="button" class="btn green" onclick="modalBusquedaAvanz()">Crear Nuevo Expediente </button>' +
    '</label>';

var textHtmlAvanzadoUno = '<span class="help-block" style="display: inline; " >No se encontraron expedientes que coincidan con</span> ' +
    '<span style="color:#00b300;display: inlitypne;" id="span_coincidencia_exped" >' + $("#inputexpediente").val() + '.</span>' +
    ' <span class="help-block" style="display: inline;">Puede usar la <a href="#" class="" style=""   ' +
    'onclick="modalBusquedaAvanz()" title="Clic para buscar por Dependencia, Serie, Subserie y Año.\n" >B&uacute;squeda Avanzada</a>' +
    ' o solicitar la creaci&oacute;n de un expediente ' +
    ' <a href="#" class="" style=""   ' +
    'onclick="modalBusquedaAvanz()">Nuevo</a></span>' + '<br>' + BtnNewEx;

var textHtmlAvanzadoDos = '<span class="help-block" style="display: inline;float:right"><a href="#" class="" style=""   ' +
    'onclick="modalBusquedaAvanz()" title="Clic para buscar por Dependencia, Serie, Subserie y Año.\n">B&uacute;squeda Avanzada</a>' +
    ' o solicitar la creaci&oacute;n de un expediente ' +
    ' <a href="#" class="" style=""   ' +
    'onclick="modalBusquedaAvanz()">Nuevo</a></span>';

var correosoporte = correosoporte
var accionExpediente = 'general';
var inputexpediente = "";
var inputtipologia = "";
var buscartodo = false;
var seleccexped = false; //para saber si ya hizo click al select de los expedientes
var selecttipologia = false;//para saber si ya hizo click al select de las tipologias
var tipologiaselected = "";
var expedienteselected = "";
var expedientestabla = new Array();
var yaincluyouno = false; //para saber si ya incluyo uno, y habilitar la caja de tiplogias
var tablalistacexpedientes = null;
var midependencia = dependencia_user_log
var dependenDestinatario = ""; //almacena la dependencia del usuario seleccionado en destinatario, si es INTERNO
var usua_nomb = usua_nomb;
var depe_nomb = depe_nomb;
var estoyencrearsinradicado = 'NOCREAREXPEDSINRAD';
var mostrarRadbutton = 'NO';


//variable que almacena el texto de la ultima opcion del desplegable, cuando se escribe sobre
//el input de buscar expedientes
var texMasResultExped = "Para mas resultados y buscar en todos los años, clic en [Ampliar Búsqueda]";

var proximoexpedtemp=''; //el proximo expediente a crear, temporalmente.

var nuevoExpediente=''; //cada vez que se crea un expediente nuevo, se almacena en esta variable,
// se resetea cada vez que se abre el modal de nuevo expediente

//esto busca los xpedientes
var users = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace,
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
        url: base_url + "app/borradores/getexpedientes.php?buscartodo=false&key=%QUERY",//"getexpedientes.php?buscartodo=&key=%QUERY",
        /* ajax : {
         beforeSend: function(jqXhr, settings){
         settings.data = $.param({function_call:"buscardatosexpediente",key:"%QUERY"})
         },
         type: "POST"

         },*/
        /* prepare: function (settings) {
         settings.type = "POST";
         settings.contentType = "application/json; charset=UTF-8";
         settings.data = JSON.stringify(query);
         return settings;
         },*/
        filter: function (x) {

            console.log('paso', x.length)
            //x.length son los esultados encontrados
            if (x.length < 1) {
                mostrabusquedaAvanz(textHtmlAvanzadoUno)

            } else {
                // $("#divbuttonbusquedaAv").css('display', 'none');
            }
            return $.map(x, function (item) {


                var opciones = {
                    NUMERO_EXPEDIENTE: item.NUMERO_EXPEDIENTE, DEPENDENCIA: item.DEPENDENCIA,
                    TITULO_NOMBRE: item.TITULO_NOMBRE, DESCRIPCION: item.DESCRIPCION,
                    TRD: item.TRD, ESTADO: item.ESTADO, ACCESO : item.ACCESO 
                }

                return opciones;

            });
        },
        replace: function () {
            //voy almacenando lo que se va escribiendo
            textKeyupInputExped = inputexpediente.typeahead('val')
            var q = base_url + "app/borradores/getexpedientes.php?dependencia=" + dependenDestinatario + "&buscartodo=" + getbuscartodo() + "&key=" + encodeURIComponent(inputexpediente.typeahead('val'));

            return q;

        },

        wildcard: "%QUERY"
    }

});

var tipologias = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace,
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {

        url: base_url + "app/borradores/gettipologia.php?function_call=tipolByExped&expedientes=",
        replace: function (query, tipeando) {
            //tipeando es lo que estoy escribiendo sobre el input

            var q = query; //la url quee sta arriba
            //asi es que le paso los expedientes incluidos
            if ($('#expedincluidcoma').val()) {
                q += encodeURIComponent($('#expedincluidcoma').val());
            }
            q += "&q=" + encodeURIComponent(tipeando);
            return q;
        },
        filter: function (x) {
            return $.map(x, function (item) {

                var opciones = {SGD_TPR_CODIGO: item.SGD_TPR_CODIGO, SGD_TPR_DESCRIP: item.SGD_TPR_DESCRIP}

                return opciones;

            });
        },

        wildcard: "%QUERY"
    }

});
tipologias.initialize();
users.initialize();

/*cuandose se le da click al botion de expediente*/
function modalexpediente(endondeestoy) {



    estoyencrearsinradicado = endondeestoy

    resetearVariables();
    cerrarBusquedaAvanz();
    buscartodo = false;
    $('#modal_expediente').modal({show: true, keyboard: false, backdrop: 'static'});

    $("#inputexpediente").val('');
    $("#tbodyexpediente").html('');
    $("#divApendBusqAvan").css('display', 'block');//el enace de busqueda avanzada
    $("#divbuttonbusquedaAv").css('display', 'block')
    $("#span_coincidencia_exped").html($("#inputexpediente").val())
    mostrabusquedaAvanz(textHtmlAvanzadoDos);
    yaincluyouno = false;

    var ourCustomDataSetSource = function (query, callback) {
        callback([{
            name: 'Para mas resultados y buscar en todos los años, clic en [Ampliar Búsqueda]'
        }]);
    };
    if (inputexpediente == "") {
        inputexpediente = $('#inputexpediente');
        inputexpediente.typeahead({
                hint: true,
                highlight: true,
                minLength: 3
            },
            {
                name: 'nombre',
                source: users.ttAdapter(),
                templates: {
                    header: '<div style="border-button: solid 1px black; width: 100%;"></div>'
                },
                display: function (data) {

                    return data.NUMERO_EXPEDIENTE + ' ' + data.TITULO_NOMBRE + ' ' + data.DEPENDENCIA + ' ' + data.TRD;
                },
                limit: 50
            },
            {
                name: 'fixed',
                displayKey: 'name',
                source: ourCustomDataSetSource,
                templates: {
                    header: '<div style="border-top: solid 1px gray; width: 100%;"></div>'
                }
            });

        /*
         *    return {NUMERO_EXPEDIENTE: item.NUMERO_EXPEDIENTE, DEPENDENCIA: item.DEPENDENCIA,
         TITULO_NOMBRE: item.TITULO_NOMBRE,DESCRIPCION:item.DESCRIPCION,
         TRD:item.TRD,ESTADO:item.ESTADO};
         */

        inputexpediente.bind('typeahead:change', function (ev) {

            var selected = false;
            var value = ev.target.value;
            $('.tt-suggestion.tt-selectable').each(function () {
                if (value === $(this).text()) {
                    selected = true;
                }
            });
            if (!selected) {
                //inputexpediente.typeahead('val', '');
            }
        });


        inputexpediente.bind('typeahead:selected', function (obj, datum, name) {
            //aqui pasa cuando se selecciona un item

            //alert(JSON.stringify(obj)); // object
            // outputs, e.g., {"type":"typeahead:selected","timeStamp":1371822938628,"jQuery19105037956037711017":true,"isTrigger":true,"namespace":"","namespace_re":null,"target":{"jQuery19105037956037711017":46},"delegateTarget":{"jQuery19105037956037711017":46},"currentTarget":

            //alert(JSON.stringify(name)); // contains dataset name
            // outputs, e.g., "my_dataset"

            console.log('datum', datum); // contains datum value, tokens and custom fields
            // outputs, e.g., {"redirect_url":"http://localhost/test/topic/test_topic","image_url":"http://localhost/test/upload/images/t_FWnYhhqd.jpg","description":"A test description","value":"A test value","tokens":["A","test","value"]}
            // in this case I created custom fields called 'redirect_url', 'image_url', 'description'

            if (datum.TITULO_NOMBRE != undefined) {
                seleccexped = true;

                expedienteselected = datum;
                validarYaestaIncluido(datum);
                inputexpediente.typeahead('val', '');
                mostrabusquedaAvanz(textHtmlAvanzadoDos);
                /*if(selecttipologia==true && seleccexped==true){

                 validarYaestaIncluido(datum);
                 inputexpediente.typeahead('val', '');
                 }*/
            } else {
                //si entra aqui, es porque selecciono la opcion de mas resultados para busqueda avanzada,

                $("#search-expediente").click();
                inputexpediente.typeahead('val', '');
            }
        });
    }
    $('#inputexpediente').css('background-color', '#dad8d9');


    definirTablaExpediente();

    var entablatemp = expedientestabla;
    expedientestabla = null;
    expedientestabla = new Array();
    definirinputtipologia();
    var retornoMisExpe = buscarMisExped();
    buscarExpedRadAso();

    if (validaSeleccionoTipologia(retornoMisExpe) == false) {
        alertaTipologia();
    }


    if (tipologiaselected != "") {
        var textotipologiaactual = inputtipologia.typeahead('val');
        $("#divinputtipologia").css('display', 'block');
        tipologiasinborde();
        definirinputtipologia();
        inputtipologia.typeahead('val', textotipologiaactual);
    }


}


function tipologiasinborde() { //pone el input de tipologia sin el borde ojo, ya que hay
    // qeu volverlo a definir para que tome el quita el borde rojo
    $("#divinputtipologia").html('');
    $("#divinputtipologia").html('<div class="col-md-12 search-page search-content-2"> ' +
        '<div class="search-bar bordered"> ' +
        '<div class="input-group" style="width: 100%;" id="divpreviotipologia"> ' +
        '<input type="text" style=" color: #5E6373 !important;font-size: 16px !important; width:100%;"' +
        ' class="form-control typeahead"' +
        'id="inputtipologia"placeholder="Elegir tipo documental"> </div> </div> </div>');

}


function alertaTipologia() {

    $("#divinputtipologia").css('display', 'block');
    setTimeout(function () {
        $("#inputtipologia").css('border', '1px solid red !important');
        $("#inputtipologia").css('box-shadow', '0 0 3px red !important');
        $("#inputtipologia").css('margin', '10px !important');
        $("#divpreviotipologia").pulsate({  color: "#ff0000",
            reach: 20,                              // how far the pulse goes in px
            speed: 1000,                            // how long one pulse takes in ms
            pause: 1000,                               // how long the pause between pulses is in ms
            glow: true,                             // if the glow should be shown too
            repeat: 3,                           // will repeat forever if true, if given a number will repeat for that many times
            onHover: false                          // if true only pulsate if user hovers over the element
        });
    }, 500);
}

function buscarExpedRadAso() {
    //si el borrador tiene radicados asociados, busca a ver si esos radicados estan en expedientes,
    //para mostrarlos como sugerencia en el modal de expedientes
    if (estoyencrearsinradicado == 'NOCREAREXPEDSINRAD') {
        $.ajax({
            url: base_url + "app/borradores/generalborrador.php",
            method: "POST",
            async: false,
            data: {
                borrador_id: borrador_id,
                function_call: "buscarExpedRadAso"
            },
            dataType: "json",
            success: function (response) {
                recibeExpedientes(response);
            }
        }).done(function () {

        }).fail(function (error) {
            Swal.fire("Error", "Ocurrió un error al validar si está incluido al borrador", "error");
        });
    }
}

//define el input que tiene las tpologias
function definirinputtipologia() {

    inputtipologia = $('#inputtipologia');


    inputtipologia.typeahead({
            hint: true,
            highlight: true,
            minLength: 0,
        },
        {
            name: 'nombre',
            source: tipologias.ttAdapter(),
            display: function (data) {
                return data.SGD_TPR_DESCRIP;
            },
            limit: 10000
        });

    inputtipologia.bind('typeahead:change', function (ev) {

        var selected = false;
        var value = ev.target.value;
        $('.tt-suggestion.tt-selectable').each(function () {
            if (value === $(this).text()) {
                selected = true;
            }
        });
        if (!selected) {
            // inputtipologia.typeahead('val', '');
        }
    });

    inputtipologia.bind('typeahead:selected', function (obj, datum, name) {

        inputtipologia.typeahead('val', datum.SGD_TPR_DESCRIP);
        tipologiaselected = datum.SGD_TPR_CODIGO;
        selecttipologia = true;
        if (selecttipologia == true) {
            incluircontipologia();
        }
    });


    $('#inputtipologia').css('background-color', '#dad8d9');

}

function validaSeleccionoTipologia(retornoMisExpe) {
    //esto lo hago para que verifique si no estan incluidos los expeientes que ya ha agregado
    var tienetipologia = true;
    for (var i = 0; i < retornoMisExpe.length; i++) {
        if (retornoMisExpe[i]['tipologia'] == "") {
            tienetipologia = false;
            yaincluyouno = true;

            break;
        }
    }

    return tienetipologia;

}


function cerrarModalExpediente() {
    $("#modal_expediente").modal('hide');
}



function createNewExp(evento) {

    if (usuaPermExpediente < 1) {
        Swal.fire({
            title: "No tiene permiso",
            text: "Usted no tiene permiso para crear expedientes",
            type: "warning",
            closeOnConfirm: true,
            closeOnCancel: true,
            confirmButtonText: "Aceptar",
        })

        evento.preventDefault();
        return false;
    }

    if ($("#selectdependencia").val() == "") {

        Swal.fire({
            title: "Dependencia Inválida",
            text: "Elegir Dependencia de la lista o solicitar su creación a soporte: " + correosoporte,
            type: "warning",
            closeOnConfirm: true,
            closeOnCancel: true,
            confirmButtonText: "Aceptar",
        })

        evento.preventDefault();
        return false;
    }
    if ($("#serieavanzada").val() == "") {

        Swal.fire({
            title: "Serie Inválida",
            text: "Elegir Serie de la lista o solicitar su creación a soporte: " + correosoporte,
            type: "warning",
            closeOnConfirm: true,
            closeOnCancel: true,
            confirmButtonText: "Aceptar",
        })

        evento.preventDefault();
        return false;
    }
    if ($("#subserieavanzada").val() == "") {

        Swal.fire({
            title: "Sub Serie Inválida",
            text: "Elegir Sub Serie de la lista o solicitar su creación a soporte: " + correosoporte,
            type: "warning",
            closeOnConfirm: true,
            closeOnCancel: true,
            confirmButtonText: "Aceptar",
        })

        evento.preventDefault();
        return false;
    }
    if ($("#selectanio").val() == "") {

        Swal.fire({
            title: "Año Inválido",
            text: "Elegir Año de la lista",
            type: "warning",
            closeOnConfirm: true,
            closeOnCancel: true,
            confirmButtonText: "Aceptar",
        })

        evento.preventDefault();
        return false;
    }
    modalNuevoexpediente();

}


function modalBusquedaAvanz() { //cuando se presiona el enlace de busqueda avanzada

    //abre la pantalla de busqueda avanzada de expedientes
    $("#selectdependencia").html('').change()
    $("#selectdependencia").html('').change()
    $("#serieavanzada").val('');
    $("#subserieavanzada").val('');

    var resultDropdown = $(".divBusquedaAvanzada");
    resultDropdown.css('display', 'block');

    $("#divinputtipologia").css('display', 'none');

    queactualizar = new Array();
    queactualizar[0] = "dependencia";
    queactualizar[1] = "serie";
    //queactualizar[2]="subserie";
    //queactualizar[3]="anio";
    if (accionExpediente == 'consulta'){
        definirselect2Avanzadas('consultaExpediente'); 
    }else{
        definirselect2Avanzadas(); //defino los select2
    }
    
    changeBusquedaAvanzada(midependencia, queactualizar); //jhago la busqueda

    $("#selectdependencia").val(midependencia);
    $('#selectdependencia').trigger('change.select2');

    $("#selectanio").val()
    $('#selectanio').trigger('change.select2'); 

}


function definirselect2Avanzadas(ddParent = 'modal_expediente') { //define los select2 de los selectd e busqueda avanzada

    
    if (ddParent == 'modal_expediente'){
        ddpvalue = $('#'+ddParent);
    }else{
        ddpvalue = $(document.body);
    }
    $("#selectdependencia").select2(
        {
            width: '70%',
            dropdownParent: ddpvalue,
            allowClear: true,

            placeholder: 'Buscar Dependencias',
            escapeMarkup: function (markup) {
                return markup;
            }, // let our custom formatter work

            language: {
                inputTooShort: function () {
                    return 'Ingrese una dependencia para buscar';
                },
                noResults: function () {
                    return "Sin resultados";
                }
            }
        }).on('select2:select', function (e) {

        queactualizar = new Array();
        queactualizar[0] = "serie";
        queactualizar[1] = "subserie";
        queactualizar[2] = "anio";

        changeBusquedaAvanzada($("#selectdependencia").val(), queactualizar);
    }).on("select2:unselect", function (e) {

        $('#serieavanzada').html('');
        $('#subserieavanzada').html('');
        $('#selectanio').html('');

    }).trigger('change');

    $("#selectanio").select2(
        {
            width: '70%',
            dropdownParent: ddpvalue,
            allowClear: true,

            placeholder: 'Buscar por Año',
            escapeMarkup: function (markup) {
                return markup;
            }, // let our custom formatter work

            language: {
                inputTooShort: function () {
                    return 'Ingrese un año para buscar';
                },
                noResults: function () {
                    return "Sin resultados";
                }
            }
        }).on('select2:select', function (e) {
        queactualizar = new Array();
        changeBusquedaAvanzada($("#selectdependencia").val(), queactualizar);
    });

    $("#serieavanzada").select2(
        {
            width: '70%',
            dropdownParent: ddpvalue,
            allowClear: true,

            placeholder: 'Buscar por serie',
            escapeMarkup: function (markup) {
                return markup;
            }, // let our custom formatter work

            language: {
                inputTooShort: function () {
                    return 'Ingrese una serie';
                },
                noResults: function () {
                    return "Sin resultados";
                }
            }
        }).on('select2:select', function (e) {
        queactualizar = new Array();
        queactualizar[0] = "subserie";
        queactualizar[1] = "anio";
        $('#subserieavanzada').html('');
        $('#selectanio').html('');
        changeBusquedaAvanzada($("#selectdependencia").val(), queactualizar);
    }).on("select2:unselect", function (e) {

        $('#subserieavanzada').html('');
        $('#selectanio').html('');
    }).trigger('change');

    $("#subserieavanzada").select2(
        {
            width: '70%',
            dropdownParent: ddpvalue,
            allowClear: true,

            placeholder: 'Buscar por sub serie',
            escapeMarkup: function (markup) {
                return markup;
            }, // let our custom formatter work

            language: {
                inputTooShort: function () {
                    return 'Ingrese una sub serie';
                },
                noResults: function () {
                    return "Sin resultados";
                }
            }
        }).on('select2:select', function (e) {
        queactualizar = new Array();
        queactualizar[0] = "anio";
        $('#selectanio').html('');
        changeBusquedaAvanzada($("#selectdependencia").val(), queactualizar);
    }).on("select2:unselect", function (e) {
        $('#selectanio').html('');
    }).trigger('change');

}


function changeBusquedaAvanzada(dependencia, queactualizar) {  //cuando se abre el modal de busqueda avanzada, o cuando se hace un change de esos select

    $.ajax({
        url: base_url + "app/borradores/generalborrador.php",
        method: "POST",
        data: {
            function_call: "busqAvaExpediente",
            dependencia: dependencia,
            serie: $("#serieavanzada").val(),
            subserie: $("#subserieavanzada").val(),
            anio: $("#selectanio").val(),
            estoyen:estoyencrearsinradicado //varible que dice en que modulo estoy
        },
        dataType: "json",
        async: false,
        success: function (response) {
            //$("#divinputtipologia").css('display','none');//oculto el input de tipologia
            $("#divbuttonbusquedaAv").css('display', 'none');//oculto el enlace de busqueda avanzada

            $("#divinputexpediente").css('display', 'none');

            for (var j = 0; j < queactualizar.length; j++) {


                if (queactualizar[j] == "dependencia") {

                    var html = "<option value=''></option>";
                    for (var i = 0; i < response.dependencias.length; i++) {
                        html += "<option value='" + response.dependencias[i]['DEPE_CODI'] + "'>" +
                            response.dependencias[i]['DEPENDENCIA'] + " - " + response.dependencias[i]['DEPE_CODI'] + "</option>"
                    }
                    $("#selectdependencia").html(html);
                }


                if (queactualizar[j] == "anio") {
                    html = "<option value=''></option>";
                    for (var i = 0; i < response.anios.length; i++) {
                        html += "<option value='" + response.anios[i]['AÑO'] + "'> " +
                            "" + response.anios[i]['AÑO'] + "</option>"
                    }
                    $("#selectanio").html(html);
                    //$("#selectanio").val(anioactual);
                }


                if (queactualizar[j] == "serie") {
                    html = "<option value=''></option>";
                    for (var i = 0; i < response.series.length; i++) {
                        html += "<option value='" + response.series[i]['SGD_SRD_CODIGO'] + "'> " +
                            "" + response.series[i]['SERIE'] + "</option>"
                    }
                    $("#serieavanzada").html(html);
                }

                if (queactualizar[j] == "subserie") {
                    html = "<option value=''></option>";
                    for (var i = 0; i < response.subseries.length; i++) {
                        html += "<option value='" + response.subseries[i]['SGD_SBRD_CODIGO'] + "'> " +
                            "" + response.subseries[i]['SUBSERIE'] + "</option>"
                    }
                    $("#subserieavanzada").html(html);
                }

                /* $('#selectdependencia').trigger('change.select2');
                 $('#selectanio').trigger('change.select2');
                 $('#serieavanzada').trigger('change.select2');
                 $('#subserieavanzada').trigger('change.select2');*/

            }
        }
    })
}

//busca la secuencua para el nuevo expediente
function buscarSecuencia(dependencia, serie, subserie, anio) {
    secuencia = false;
    var bubleEventfake = new Event('fake',{bubbles:true});
    var ajaxCallGetSecuencia = ExpedientesService.getSecuenciaExpediente(dependencia,serie,subserie,anio);
    ajaxCallGetSecuencia.success( function (response){
        if (typeof(response.isSuccess) != 'undefined' && response.isSuccess == true){
            secuencia = response.secuencia != undefined ? response.secuencia : false;
        }

        if (secuencia == false){
            Swal.fire("Error", "Ocurrió un error al generar un numero de expediente nuevo", "error");
            closeModalNuevoExpediente(bubleEventfake);
        }
    }).error( function (error){
        Swal.fire("Error", "Ocurrió un error al generar un numero de expediente nuevo", "error");
        closeModalNuevoExpediente(bubleEventfake);
    });
    /*$.ajax({
        url: base_url + "app/expedientes/generalexpediente.php",
        method: "POST",
        data: {
            dependencia: dependencia,
            serie: serie,
            subserie: subserie,
            anio: anio,
            function_call: "getSecuenciaExped"
        },
        async: false,
        dataType: "json",
        success: function (response) {
            secuencia = response.secuencia != undefined ? response.secuencia : false
        }
    }).done(function () {

    }).fail(function (error) {
        Swal.fire("Error", "Ocurrió un error al validar si está incluido al borrador", "error");
    });*/



}

function getbuscartodo() {
    return buscartodo;
}

function definirSelectResponsable(dependencia) {
    $("#selectresponsable").val('');

    $("#selectresponsable").select2(
        {
            width: "100%",
            dropdownParent: $("#modal_nuevoexpediente"),
            allowClear: true,
            data: {},
            ajax: {
                url: base_url + "app/expedientes/generalexpediente.php",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        search: params.term,
                        type: 'public',
                        function_call: 'getResponsables',
                        dependencia: dependencia
                    };
                },
                processResults: function (data, params) {
                    return {
                        results: data,
                    };
                },
                cache: true,
            },
            placeholder: 'Buscar Responsable',
            escapeMarkup: function (markup) {
                return markup;
            }, // let our custom formatter work

            language: {
                inputTooShort: function () {
                    return 'Ingrese un responsable a buscar';
                }
            }
        }).on('select2:selecting', function (e) {
    })
}

function resetearCrearNuevoExped(){
//reseteo esta variable
    nuevoExpediente='';

    //defino el boton de crear
    $('#divBtnAccionCrearExped').html('');
    $('#divBtnAccionCrearExped').html('<button type="button" class="btn btn-lg red" id="btnAccionCrearExped"\n' +
        '                                    style="border-radius:20px !important" onclick="validarCrearExpediente(event)">\n' +
        '                                Crear Expediente\n' +
        '                            </button>')


    $("#titulonuevoexped").val('')
    $("#descripNuevoExped").val('')
    $("#otrosMetadNuevoExped").val('')
    $('#inputtipologia').val('')
    $("#titulonuevoexped").prop('readOnly', false);
    $("#descripNuevoExped").prop('readOnly', false);
    $("#otrosMetadNuevoExped").prop('readOnly', false);
    $('#selectresponsable').prop("disabled", false);

    console.log($('#inputtipologia').val());
}

function modalNuevoexpediente() {

    resetearCrearNuevoExped();
    buscarSecuencia(cerosAlaIzquierda($("#selectdependencia").val(),3), cerosAlaIzquierda($("#serieavanzada").val(),3),cerosAlaIzquierda($("#subserieavanzada").val(),3), $("#selectanio").val())
    $("#modal_nuevoexpediente").modal('show');
    $("#modal_nuevoexpediente").css('z-index', '20000');
    //$("#modal_nuevoexpediente").css('z-index', '99999');

    mostrarExpedConColor('Se creará el expediente: ');

    //defino el select de responsables
    definirSelectResponsable($("#selectdependencia").val());


    $("#dependencianueva").html("<span >Dependencia:  </span><span style='color:#0d0d0d;font-weight: 900'>" +
        $("#selectdependencia option:selected").text() + '</span>');
    $("#serienueva").html("<span >Serie: </span><span style='color:#0d0d0d;font-weight: 900'>" + $("#serieavanzada option:selected").text() + ' </span>');
    $("#subserienueva").html("<span >Subserie: </span><span style='color:#0d0d0d;font-weight: 900'>" + $("#subserieavanzada option:selected").text() + ' </span>');
    $("#anionuevo").html("<span >Año:</span><span style='color:#0d0d0d;font-weight: 900'>  " + $("#selectanio option:selected").text() + ' </span>')


    //agregar valores x defecto al titulo
    var titleex = '';
    var extitle = '';
    titleex = $("#subserieavanzada option:selected").text();
    extitle= titleex.substr(0, titleex.indexOf('-'));
    $('#titulonuevoexped').val(extitle +'-'+$("#selectanio option:selected").text());

}

function mostrarExpedConColor(textoadelante){
    var expnumbtext = '';
    $('#span_nro_exped').attr('style','display: contents;');
    $('#span_nro_exped').html('')
    var htmlproximoexpedtemp=''
    htmlproximoexpedtemp = '<span >'+textoadelante+'</span><span style="color: #514AA5; font-size: 16pt">'+$("#selectanio").val() +'</span>'+ '<span style="color: #4B952B;font-size: 16pt">'+
        cerosAlaIzquierda($("#selectdependencia").val(),3) +'</span><span style="color: #AABD0B;font-size: 16pt">'+
        cerosAlaIzquierda($("#serieavanzada").val(),3) +'</span><span style="color: #514AA5;font-size: 16pt">'+
        cerosAlaIzquierda($("#subserieavanzada").val(),3)+'</span>';
    //var secuencia = buscarSecuencia(cerosAlaIzquierda($("#selectdependencia").val(),3), cerosAlaIzquierda($("#serieavanzada").val(),3),cerosAlaIzquierda($("#subserieavanzada").val(),3), $("#selectanio").val())
    proximoexpedtemp = $("#selectanio").val() + cerosAlaIzquierda($("#selectdependencia").val(),3) + cerosAlaIzquierda($("#serieavanzada").val(),3) + cerosAlaIzquierda($("#subserieavanzada").val(),3)
    console.log(secuencia);
    console.log(proximoexpedtemp);
    proximoexpedtemp=proximoexpedtemp+secuencia+'E'
    htmlproximoexpedtemp = htmlproximoexpedtemp + '<span style="color: #27C4A1; font-size: 16pt">'+secuencia+'</span>' +
        '<span style="color: #010605; font-size: 16pt">E</span>';
    expnumbtext = $("#selectanio").val() + cerosAlaIzquierda($("#selectdependencia").val(),3) +cerosAlaIzquierda($("#serieavanzada").val(),3) +cerosAlaIzquierda($("#subserieavanzada").val(),3)+secuencia+'E';

   var mensajeamostrar="'Expediente copiado'";
    var botoncoex = crearBtnCopyClip(expnumbtext,mensajeamostrar);

    $('#span_nro_exped').html(htmlproximoexpedtemp+botoncoex);
}

function validarYaestaEnTabla() {

    //valido si ya seleccione un expediente con una tipologia ,para no duplciarlos en la tabla mostrada
    var yaestaentabla = false;
    var tamano = Object.keys(expedientestabla).length;

    if (tamano > 0) {
        for (var i = 0; i < expedientestabla.length; i++) {
            if (expedientestabla[i].NUMERO_EXPEDIENTE == expedienteselected.NUMERO_EXPEDIENTE
            ) {
                yaestaentabla = true
            }
        }
    }
    return yaestaentabla;

}




function validarYaestaIncluido() {
    //valido si esta en la tabla
    var yaestaentabla = validarYaestaEnTabla()

    if (yaestaentabla == false) {

        if (estoyencrearsinradicado == 'NOCREAREXPEDSINRAD') {
            console.log(estoyencrearsinradicado);
            $.ajax({
                url: base_url + "app/borradores/generalborrador.php",
                method: "POST",
                data: {
                    borrador_id: borrador_id,
                    expediente: expedienteselected.NUMERO_EXPEDIENTE,
                    //idtipologia: tipologiaselected,
                    function_call: "estaincluidoexpediente"
                },
                dataType: "json",
                success: function (response) {

                    //si no esta en la tabla entonces lo muestro, indepddientemente de
                    // si ya fue incluido el expediente al borrador
                    if (response.success == "incluido") {
                        armartablaexpediente(expedienteselected, "incluido")

                    } else {
                        armartablaexpediente(expedienteselected, expedienteselected.ESTADO)
                    }

                    var tamano = Object.keys(expedientestabla).length;
                    //lo guardo en el arreglo
                    if (tamano < 1) {
                        expedientestabla[0] = {};
                        expedientestabla[0] = expedienteselected;
                        //expedientestabla[0].TIPOLOGIA=tipologiaselected;
                    } else {
                        expedientestabla[tamano] = {};
                        expedientestabla[tamano] = expedienteselected;
                        //expedientestabla[tamano].TIPOLOGIA=tipologiaselected;
                    }

                    updatetablaexpediente();

                }
            }).done(function () {

            }).fail(function (error) {
                Swal.fire("Error", "Ocurrió un error al validar si está incluido al borrador", "error");
            });

        } else {

            armartablaexpediente(expedienteselected, expedienteselected.ESTADO)

            var tamano = Object.keys(expedientestabla).length;
            //lo guardo en el arreglo
            if (tamano < 1) {
                expedientestabla[0] = {};
                expedientestabla[0] = expedienteselected;
                //expedientestabla[0].TIPOLOGIA=tipologiaselected;
            } else {
                expedientestabla[tamano] = {};
                expedientestabla[tamano] = expedienteselected;
                //expedientestabla[tamano].TIPOLOGIA=tipologiaselected;
            }

            updatetablaexpediente();
        }
    } else {
        Swal.fire("Alerta", "Ya seleccionó este expediente", "warning");
    }
}

function updatetablaexpediente() {
    expedienteselected = "";
    inputexpediente.typeahead('val', '');
    selecttipologia = false;
    seleccexped = false;

}

function setSwicht(valor){
    
    valorOriginal = true;
    if (valor == true ){
        valorOriginal = false;
    }
    console.log(valor);
    $("#swich_seguridad").bootstrapSwitch('state', valor);
    
    if (valor == false){
        $('#listadosAutorizaciones > div').show(); 
    }else{
        $('#listadosAutorizaciones > div').hide();
    }
    

}

function mostrarModalSeguridad(){
    $('#modal_seguridad_exp').modal('show');

    $(".bootstrap-switch-label").html('Restringir')
    //esto es para el swiche de seguridad
    $('.bootstrap-switch-label').addClass('popovers');
    popoverRestringidoSeg();
    $('.bootstrap-switch-label').popover({container: '#divprevioswiche'});
}

function popoverPublicoSeg() {
    //$(".divshiden").css('display', 'block');
    $(".bootstrap-switch-label").html('Público')
    $(".bootstrap-switch-label").attr('data-original-title', 'Quitar restricción')
    $(".bootstrap-switch-label").attr('data-content', 'Todos los usuarios tendrán acceso al expediente')
    $('.bootstrap-switch-label').attr('data-placement', 'top')
    $('.bootstrap-switch-label').attr('data-trigger', 'hover')
    $('.bootstrap-switch-label').attr('data-container', '#ivprevioswiche')
    $(".bootstrap-switch-handle-off").html('Reservado')

}

function popoverRestringidoSeg() {
    //$(".divshiden").css('display', 'none');
    $(".bootstrap-switch-label").html('Restringir')
    $(".bootstrap-switch-label").attr('data-original-title', 'Restringir acceso al expediente')
    $(".bootstrap-switch-label").attr('data-content', 'Solo tendrá acceso al expediente el usuario actual ' +
        'y usuarios o dependencias autorizadas!')
    $('.bootstrap-switch-label').attr('data-placement', 'top')
    $('.bootstrap-switch-label').attr('data-trigger', 'hover')
    $('.bootstrap-switch-label').attr('data-container', '#divprevioswiche')
    $(".bootstrap-switch-handle-off").html('Restringir')

}

function agregarOption(item, index) {
   //var $option = $("<option selected></option>").val(item.id).text(item.text);
  
   var newOption = new Option(item.text, item.id, true, true);
    // Append it to the select
     console.log(newOption);
     if (typeof (item.usua_nomb) != 'undefined'){
      $("#id_select_users option[value='"+item.id+"']").remove();   
      $('#id_select_users').append(newOption).trigger('change.select2');
     }else{
      $("#select_dependencias option[value='"+item.id+"']").remove(); 
      $('#select_dependencias').append(newOption).trigger('change.select2');
     }
    
}

function buscarEntidades(){
     var tipo = 'expediente';
     var valor = expedienteSelected;
     
     $("#select_dependencias").val(null);
     $('#select_dependencias').trigger('change.select2');
     $("#id_select_users").val(null);
     $("#id_select_users").trigger('change.select2'); 

                    var ajax = SeguridadService.consultar(tipo,valor);
                    ajax.success(function (response) {
                        if(response.isSuccess){
                            var selectedUserValues = new Array();
                            var selectedDepenValues = new Array();

                            var contUser = 0;
                            var contDepen = 0;

                            for (var i = 0; i < Object.keys(response.entidades).length; i++) {

                                if (response.entidades[i].tipo_entidad_autorizada == "USUARIO") {
                                    
                                    for (var j = 0; j < Object.keys(response.usuarios).length; j++){
                                        if (response.entidades[i].entidad_autorizada == response.usuarios[j].id){
                                            selectedUserValues[contUser] = response.usuarios[j];
                                            contUser++;
                                            break;
                                        }
                                    }

                                }else{
                                    for (var k = 0; k < Object.keys(response.dependencias).length; k++){
                                        if (response.entidades[i].entidad_autorizada == response.dependencias[k].id){
                                            selectedDepenValues[contDepen] = response.dependencias[k];
                                            contDepen++;
                                            break;
                                        }
                                    }
                                } 
                            }

                            
                            selectedUserValues.forEach(agregarOption);
                            console.log(selectedUserValues);
                            console.log(selectedDepenValues);
                            selectedDepenValues.forEach(agregarOption);
                            let idDsSel = selectedDepenValues.map( el => el.id );
                            //$("#id_select_users").select2('data', selectedUserValues);
                            //$("#id_select_users").trigger('change');
                            $("#select_dependencias").val(idDsSel);
                            $('#select_dependencias').trigger('change.select2');
                            let idsSel = selectedUserValues.map( el => el.id );
                            
                            $("#id_select_users").val(idsSel);
                            $("#id_select_users").trigger('change.select2'); 
                            //$('#id_select_users').trigger('change');
                            
                            
                            //$("#id_select_users").val('173');
                            //$('#id_select_users').trigger('change');
                        return true;
                        }
                    }).error(function (error) {
                        alertModal('Advertencia', 'Error consultando los usuarios', 'warning', '5000');
                        return false;
                    });
}

function updateAcceso(state){ 
    console.log($elementSelected);
    var pacceso = 'P';
    var iconoN;
    var colorN;
    var iconoO;
    var colorO;
    if (state){
        pacceso = 'P';
        iconoN = 'fa-unlock';
        colorN = 'font-green';
        iconoO = 'fa-lock';
        colorO = 'font-red';
    }else{
        pacceso = 'R';
        
        iconoN = 'fa-lock';
        colorN = 'font-red';
        iconoO = 'fa-unlock';
        colorO = 'font-green';
    }
       console.log('update acceso'); 
             var paramsAcceso = {
                        'numero_expediente': expedienteSelected,
                        'acceso': pacceso,
                        'usuario_id' : codusuario
                    }

                    var ajax = ExpedientesService.updateAcceso(paramsAcceso);
                    ajax.success(function (response) {
                        if(response.isSuccess){
                        alertModal('Actualizado!', 'La seguridad del acceso  al expediente fue actualizada', 'success', '5000');
                        
                        $($elementSelected).removeClass(iconoO);
                        $($elementSelected).removeClass(colorO);
                        $($elementSelected).addClass(iconoN);
                        $($elementSelected).addClass(colorN);
                        $($elementSelected).attr("onclick","modalSeguridad(event," + state + ",'" + expedienteSelected + "')"); 
                        if (pacceso == 'R'){
                            $('#listadosAutorizaciones > div').show();
                        }else{
                            $('#listadosAutorizaciones > div').hide();
                        }
                        return true;
                        }else{
                        alertModal('Advertencia', 'La seguridad del acceso al expediente NO se logro modificar', 'warning', '5000');
                        return false;
                        }
                    }).error(function (error) {
                        alertModal('Advertencia', 'La seguridad del acceso al expediente NO se logro modificar', 'warning', '5000');
                        return false;
                    });
}

function modalSeguridad(e,estatus,expediente){
    
    $elementSelected = e.target;
    $('#swich_seguridad').bootstrapSwitch('readonly', false);

    
    if (expedientePermS == '2' && usuarioArchS == '5'){
        segModeLectura = false; 
        expedienteSelected = expediente;
        mostrarModalSeguridad();
        setSwicht(estatus);
        var iconoCarpeta = '';
        if (estatus){
            $('#textoNivelBloqueo').text('publico. Sin restricción de acceso');
            iconoCarpeta = '<i style="padding-left: 0.5em;" class="fa fa-2x fa-folder-open-o font-blue mr-1">';
        }else{
             $('#textoNivelBloqueo').text('reservado. con restricción de acceso');
             iconoCarpeta = '<i style="padding-left: 0.5em;" class="fa fa-2x fa-folder font-blue mr-1">';
        }
        $('#icCarp').html('');
        $('#icCarp').html(iconoCarpeta);
        if (Object.keys(allUsers).length < 1) {
            traerUsers('usuarios')
        }

        if (Object.keys(allDepend).length < 1) {
            traerUsers('dependencias')
        }
        
        buscarEntidades();

    }else if((expedientePermS == '2' || expedientePermS == '1') || (usuarioArchS == '5'  || usuarioArchS == '4' || usuarioArchS == '3' || usuarioArchS == '2' || usuarioArchS == '1')){
        segModeLectura = true;
        expedienteSelected = expediente;

        mostrarModalSeguridad()
        setSwicht(estatus);
        var iconoCarpeta = '';
        if (estatus){
            $('#textoNivelBloqueo').text('publico. Sin restricción de acceso');
            iconoCarpeta = '<i style="padding-left: 0.5em;" class="fa fa-2x fa-folder-open-o font-blue mr-1">';
        }else{
            $('#textoNivelBloqueo').text('reservado. con restricción de acceso');
            iconoCarpeta = '<i style="padding-left: 0.5em;" class="fa fa-2x fa-folder font-blue mr-1">';
        }
        $('#icCarp').html('');
        $('#icCarp').html(iconoCarpeta);
        if (Object.keys(allUsers).length < 1) {
            traerUsers('usuarios')
        }

        if (Object.keys(allDepend).length < 1) {
            traerUsers('dependencias')
        }
        buscarEntidades();
        $('#id_select_users').select2({ disabled: true});
        $('#select_dependencias').select2({ disabled: true});
        
    }else if(estatus == true){

         Swal.fire("Alerta",'Expediente sin restricción de acceso', "warning");


        Swal.fire({
            title: 'Acceso',
            text: "Expediente sin restricción de acceso",
            type: 'success',
            showCancelButton: false,
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Ok',
            confirmButtonClass: 'btn btn-success',
            buttonsStyling: false
        }).then((result) => {
            //si existe result.value y es ==true, entonces es que confirmaste
            //si no, llega result.dismiss y puede ser "esc" de la tecla escape y "cancel"
            //con cancel, entra en la segunda condicion, con esc, no
            if (result.value) {
            } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
            ) {
            }
        });
    }else{

        Swal.fire("Alerta",configsHasValue['EXPEDIENTE_ALERT_ACCESO'], "warning");
    }

    if (segModeLectura == true){
        $('#swich_seguridad').bootstrapSwitch('readonly', true);
    }else{
        $('#swich_seguridad').bootstrapSwitch('readonly', false);
    }
}

function armartablaexpediente(datum, estado) {
    var newrow = {};
    var count = 1; 
    iconoSeguridad = 'lock';
    colorSeguridad = 'orange';
    tooltip = '';
    swichtValue = true;
    if (datum.ACCESO == 'P'){ 
        iconoSeguridad = 'unlock';
        colorSeguridad = 'green';
        tooltip = 'data-toggle="tooltip" data-placement="top" title="Expediente sin restricción de acceso"';
        swichtValue = true;
    }else{
        iconoSeguridad = 'lock';
        colorSeguridad = 'red';
        swichtValue = false;
    }
    var expediente = "'" + datum.NUMERO_EXPEDIENTE + "'";
    if (accionExpediente == 'consulta'){
        estoyencrearsinradicado = 'no';
    }
    if (estado == "Abierto") {

        var estatus = "'Abierto'";
        if (estoyencrearsinradicado == 'NOCREAREXPEDSINRAD') {
            newrow[0] = '<button type="button" id="" class="btn  " ' +
                'style="background-color: Transparent;background-repeat:no-repeat;border: none;cursor:pointer;overflow: hidden;' +
                'outline:none;" onclick="addBorraExped(event,' + estatus + ',' + expediente + ')">' +
                ' <i class="fa fa-2x fa-share font-green-jungle"></i>' +
                '<i class="fa fa-2x fa-folder-open-o font-blue"></i>' +
                '</button><br> ' +
                ' <a href="#" onclick="addBorraExped(event,' + estatus + ',' + expediente + ')" style="font-size:13pt !important;">' +
                '<span  class="font-blue font-lg bold">Incluir</span><br>Abierto</a>';
        } else {
            newrow[0] = ' <i class="fa fa-2x fa-share font-green-jungle mr-1"></i>' +
                '<i style="padding-left: 0.5em;" class="fa fa-2x fa-folder-open-o font-blue mr-1"></i><i style="padding-left: 0.5em;" onClick ="modalSeguridad(event,' + swichtValue + ',' + expediente + ')" '+tooltip+' class="fa fa-2x fa-'+iconoSeguridad+' font-'+colorSeguridad
                +'"></i>' +
                '<br> ' +
                '<span  class="font-blue font-lg bold">Abierto</span>';
        }
    } else if (estado == "Cerrado") {
        var estatus = "'Cerrado'";
        newrow[0] = '<button type="button" id="" class="btn btn-lg " ' +
            'style="background-color: Transparent;background-repeat:no-repeat;border: none;cursor:pointer;overflow: hidden;' +
            'outline:none;" ' +
            ' disabled>' +
            ' <i class="fa fa-2x fa-close "></i>' +
            '<i class="fa fa-2x fa-folder "></i>' +
            '</button><br> ' +
            '<span style="font-size:13pt !important;">Cerrado</span>';
    } else if (estado == "incluido") {
        var estatus = "'incluido'";

        newrow[0] = '<span style="font-weight: 900;font-size:14pt;color: #00802b" >Ya est&aacute; incluido</span>,' +
            '<br>' +
            '' +
            ' <i class="fa fa-2x fa-reply font-yellow-gold"></i>' +
            '<i class="fa fa-2x fa-folder-open-o font-grey-silver"></i>' +
            '<br>';

        if (configsHasValue['VALIDAEXCLUYE_EXPED'] == "SI") {
            newrow[0] += '<a href="#" onclick="delBorraExped(' + estatus + ',' + expediente + ')" ' +
                ' style="font-size:13pt !important;"><span  class="font-blue font-lg bold">Excluir</span></a><br>';
        }
    }

    var titulonombre=datum.TITULO_NOMBRE!=null?datum.TITULO_NOMBRE:'';
    var DESCRIPCION=datum.DESCRIPCION!=null?datum.DESCRIPCION:'';
    var TRD=datum.TRD!=null?datum.TRD:'';

    newrow[count] = ' <a href="' + base_url + 'expediente/detalles_exp.php?num_exp=' + datum.NUMERO_EXPEDIENTE + '&par=' + encodeURIComponent(titulonombre) + '" target="_blank"> <span class="font-blue bold">' + datum.NUMERO_EXPEDIENTE + '</span><br> ' +
        '<span class="font-grey-silver" style="color: #8f8a8d !important">' + titulonombre + '<br>' +
        '' + DESCRIPCION + '</span> </a> ';
    count++;

    newrow[count] = '<a href="' + base_url + 'expediente/detalles_exp.php?num_exp=' + datum.NUMERO_EXPEDIENTE + '&par=' + encodeURIComponent(titulonombre) + '" target="_blank"> <span class="font-blue bold">' + datum.DEPENDENCIA + '</span><br> ' +
        '<span class="font-grey-silver" style="color:#8f8a8d !important">' + TRD + '</span> </a> ';

    var arreglo = new Array();
    var tr = new Array();
    tr = Object.assign({}, newrow, arreglo);
    console.log(tablalistacexpedientes);
    var rowNode = tablalistacexpedientes.row.add(tr).draw().node();
}


function buscartodoTrue(evento) {
    evento.preventDefault();

    if (inputexpediente.typeahead('val') == texMasResultExped) {
        inputexpediente.typeahead('val', textKeyupInputExped)
    }
    buscartodo = true;
    Swal.fire("Espere", "Por favor Espere mientras buscamos los resultados", "warning");
    var url = base_url + "app/borradores/getexpedientes.php?buscartodo=true&key=" + encodeURIComponent(inputexpediente.typeahead('val'));
    $.ajax({
        url: url,
        method: "GET",
        dataType: "json",
        async: false,
        success: function (response) {
            if (response.length > 0) {
                DeleteExpedientestabla;
                expedientestabla.length  = 0;
                definirTablaExpediente('Accion');

                recibeExpedientes(response);
                mostrabusquedaAvanz(textHtmlAvanzadoDos);
            } else {
                var html = '<span class="help-block" style="display: inline;" >No se encontraron expedientes que coincidan con</span> ' +
                    '<span style="color:#00b300;display: inline;" id="span_coincidencia_exped" >' + $("#inputexpediente").val() + '.</span>' +
                    ' <span class="help-block" style="display: inline;">Puede usar la <a href="#" class="" style=""   ' +
                    'onclick="modalBusquedaAvanz()">B&uacute;squeda Avanzada</a>' +
                    ' o solicitar la creaci&oacute;n de un expediente ' +
                    ' <a href="#" class="" style=""   ' +
                    'onclick="modalBusquedaAvanz()">Nuevo</a></span>';

                $("#divApendBusqAvan").html(html)
                $("#divApendBusqAvan").css('display', 'block');
                $("#divbuttonbusquedaAv").css('display', 'block')
            }
            setTimeout(function () {
                swal.close();
            }, 1000)
        }
    }).done(function () {

    }).fail(function (error) {
        Swal.fire("Error", "Ocurrió un error al validar si está incluido al borrador", "error");
    });
    buscartodo = false;
    $("#divApendBusqAvan").css('display', 'block');
    $("#span_coincidencia_exped").html($("#inputexpediente").val())

}

function recibeExpedientes(response) {

    mostrarexpedientes(response)
    cuantosEnExped();
}

function mostrarexpedientes(response) {
    var yaestaentabla = "";
    var estado = "";
    var tamano = "";
    DeleteExpedientestabla;
    for (var i = 0; i < response.length; i++) {
        expedienteselected = response[i];
        yaestaentabla = validarYaestaEnTabla() //valido si ya lo agregue en la tabla


        if (expedienteselected['tipologia'] != undefined) {
            if (expedienteselected['tipologia'] != "") {
                inputtipologia.typeahead('val', expedienteselected['tipologia']);
                yaincluyouno = true;
                tipologiaselected = expedienteselected['tipologiacodigo'];

            }
        }

        console.log(yaestaentabla);
        if (yaestaentabla == false) {
            tamano = Object.keys(expedientestabla).length;
            //lo guardo en el arreglo de expedientes que he agregado a la tabla
            if (tamano < 1) {
                expedientestabla[0] = {};
                expedientestabla[0] = response[i];
                //expedientestabla[0].TIPOLOGIA=tipologiaselected;
            } else {
                expedientestabla[tamano] = {};
                expedientestabla[tamano] = response[i];
                //expedientestabla[tamano].TIPOLOGIA=tipologiaselected;
            }

            estado = "";
            if (response[i]['incluiExpe'] == "incluido") {
                estado = "incluido";

            } else {

                estado = response[i].ESTADO;
                if (response[i].ESTADO == null) {
                    estado = "Abierto"
                }
            }
            armartablaexpediente(response[i], estado)
        }
    }

}


function limpiarBusquedaAvanz() {

    //cuandos e presiona el boton limpiar  en el modal de expedientes avanzados
    queactualizar = new Array();
    queactualizar[0] = "dependencia";
    //queactualizar[1] = "serie";
    //queactualizar[2]="subserie";
    //queactualizar[3]="anio";

    $('#selectdependencia').html('');
    $('#serieavanzada').html('');
    $('#subserieavanzada').html('');
    $('#selectanio').html('');
    changeBusquedaAvanzada(midependencia, queactualizar); //hago la busqueda

    //defino la tabla nuevaente y vuelvo a cargar los datos de la tabla
    definirTablaExpediente();
    expedientestabla = new Array();
    buscarMisExped();
}


function buscarMisExped(armar=true) {
    //si el borrador tiene radicados asociados, busca a ver si esos radicados estan en expedientes,
    //para mostrarlos como sugerencia en el modal de expedientes

    var retornoMisExpe = "";
    if (estoyencrearsinradicado == 'NOCREAREXPEDSINRAD') {
        $.ajax({
            url: base_url + "app/borradores/generalborrador.php",
            method: "POST",
            async: false,
            data: {
                borrador_id: borrador_id,
                function_call: "buscarMisExped"
            },
            dataType: "json",
            success: function (response) {
                retornoMisExpe = response
                if (armar == true) {
                    recibeExpedientes(response);
                }

            }
        }).done(function () {

        }).fail(function (error) {
            Swal.fire("Error", "Ocurrió un error al validar si está incluido al borrador", "error");
        });
    }
    return retornoMisExpe;
}


function resultAvanzados() {

    if ($("#selectdependencia").val() != "") {

        App.blockUI({
            boxed: !0,
            message: 'Buscando expedientes… espere'
        });
        var id_borrador = estoyencrearsinradicado == 'NOCREAREXPEDSINRAD' ? borrador_id : 0;

        //el boton buscar de busqueda avanzada
        $.ajax({
            url: base_url + "app/borradores/generalborrador.php",
            method: "POST",
            data: {
                dependencia: $("#selectdependencia").val(),
                serie: $("#serieavanzada").val(),
                subserie: $("#subserieavanzada").val(),
                anio: $("#selectanio").val(),
                function_call: "resultAvanzados",
                borrador: id_borrador
            },
            dataType: "json",
            success: function (response) {
                if (response.length > 0) {
                    definirTablaExpediente();
                    expedientestabla = new Array();
                    buscarMisExped();
                    recibeExpedientes(response);
                } else {
                    Swal.fire({
                            title: "Alerta",
                            text: "No se encontraron expedientes que coincidan con la búsqueda, desea solicitar la " +
                                "creación de un expediente Nuevo",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonText: "Solicitar Nuevo",
                            cancelButtonText: "Cerrar",
                            closeOnConfirm: true,
                            closeOnCancel: true
                        }).then((result) => {
                        if (result.value) {
                        modalNuevoexpediente();
                    } else if (
                        result.dismiss === Swal.DismissReason.cancel
                    ) {

                    }
                });
                }
                App.unblockUI()
            }
        }).done(function () {

        }).fail(function (error) {
            Swal.fire("Error", "Ocurrió un error al validar si está incluido al borrador", "error");
            App.unblockUI()
        });
    } else {
        Swal.fire("Alerta", "Debe elegir como mínimo la Dependencia a buscar", "warning");
    }
}


function validaTituloNuevoExped(evento, esto) {
    evento.preventDefault();

    var texto = $(esto).val();
    if (texto.length > 13) {
        validaExisteTitulo(texto)
    }

}

function validaExisteTitulo(titulo) {
    var retornoMisExpe = "";

    $.ajax({
        url: base_url + "app/expedientes/generalexpediente.php",
        method: "POST",
        async: false,
        data: {
            titulo: titulo,
            function_call: "validaExisteTitulo"
        },
        dataType: "json",
        success: function (response) {

            if (response.existe != undefined && Object.keys(response.existe).length > 0) {
                //existe el titulo
                alertModal('Título ya existe!', response.existe[0].SGD_EXP_NUMERO, 'info', 8000)
            }

        }
    }).done(function () {

    }).fail(function (error) {
        Swal.fire("Error", "Ocurrió un error al validar si está incluido al borrador", "error");
    });
    return retornoMisExpe;
}

function validarCrearExpediente(evento) {

    if ($("#titulonuevoexped").val() == "") {

        Swal.fire({
            title: "Título",
            text: "Debe ingresar un título",
            type: "warning",
            closeOnConfirm: true,
            closeOnCancel: true,
            confirmButtonText: "Aceptar",
        })

        evento.preventDefault();
        return false;
    }

    if ($("#selectresponsable").val() == undefined || $("#selectresponsable").val() == "" ||
        $("#selectresponsable").val() == null) {

        Swal.fire({
            title: "Responsable",
            text: "Debe seleccionar un responsable",
            type: "warning",
            closeOnConfirm: true,
            closeOnCancel: true,
            confirmButtonText: "Aceptar",
        })

        evento.preventDefault();
        return false;
    }

    var titulo = $("#titulonuevoexped").val()
    if (titulo.length < 15) {

        Swal.fire({
            title: "Título",
            text: "El título debe contener al menos 15 caracteres",
            type: "warning",
            closeOnConfirm: true,
            closeOnCancel: true,
            confirmButtonText: "Aceptar",
        })

        evento.preventDefault();
        return false;
    }

    if (titulo.length > 1000) {

        Swal.fire({
            title: "Título",
            text: "El título debe contener menos de 1000 caracteres",
            type: "warning",
            closeOnConfirm: true,
            closeOnCancel: true,
            confirmButtonText: "Aceptar",
        })

        evento.preventDefault();
        return false;
    }

    var descripNuevoExped = $("#descripNuevoExped").val()
    if (descripNuevoExped.length > 4000) {

        Swal.fire({
            title: "Descripción",
            text: "La Descripción, debe contener menos de 4000 caracteres",
            type: "warning",
            closeOnConfirm: true,
            closeOnCancel: true,
            confirmButtonText: "Aceptar",
        })
        evento.preventDefault();
        return false;
    }

    var otrosMetadNuevoExped = $("#otrosMetadNuevoExped").val()
    if (otrosMetadNuevoExped.length > 4000) {

        Swal.fire({
            title: "Otros Metadatos",
            text: "El campo Otros Metadatos, debe contener menos de 4000 caracteres",
            type: "warning",
            closeOnConfirm: true,
            closeOnCancel: true,
            confirmButtonText: "Aceptar",
        })
        evento.preventDefault();
        return false;
    }

    accionCrearExped();
}

function accionCrearExped(){

    $.ajax({
        url: base_url + "app/expedientes/generalexpediente.php",
        method: "POST",
        async: false,
        data: {
            expediente: proximoexpedtemp,
            dependencia: cerosAlaIzquierda($('#selectdependencia').val(),3),
            serie: cerosAlaIzquierda($("#serieavanzada").val(),3),
            subserie: cerosAlaIzquierda($("#subserieavanzada").val(),3),
            anio: $("#selectanio").val(),
            responsable:$("#selectresponsable").val(),
            titulo: $('#titulonuevoexped').val(),
            descripcion:$('#descripNuevoExped').val(),
            otrosmedatatos:$('#otrosMetadNuevoExped').val(),
            function_call: "accionCrearExped"
        },
        dataType: "json",
        success: function (response) {

            if (response.error != undefined) {
                //existe el expediente
                alertModal('Error', response.error, 'warning', 8000)
            }else{
                $("#titulonuevoexped").prop('readOnly', true);
                $("#descripNuevoExped").prop('readOnly', true);
                $("#otrosMetadNuevoExped").prop('readOnly', true);
                $('#selectresponsable').prop("disabled", true);
                nuevoExpediente=response.success;
                $('#divBtnAccionCrearExped').html('')
                alertModal('Éxito', 'El expediente fué creado con éxito: '+response.success, 'success', 15000)
                //mostrarExpedConColor('Expediente Creado: ');

                $('#span_nro_exped').html('');

                var html="<span >Expediente Creado: </span><span style=\" font-size: 16pt\"> "+nuevoExpediente+"" +
                    "</span>";
                var mensajeamostrar="'Expediente copiado'";
                var botoncoex = crearBtnCopyClip(nuevoExpediente,mensajeamostrar);

                $('#span_nro_exped').html(html+botoncoex);
                $('#BtnCopyClip').show();

                seleccexped = true;
                expedienteselected = response.expediente[0];
                validarYaestaIncluido(response.expediente[0]);
                inputexpediente.typeahead('val', '');
                mostrabusquedaAvanz(textHtmlAvanzadoDos);
            }

        }
    }).done(function () {

    }).fail(function (error) {
        Swal.fire("Error", "Ocurrió un error al validar si está incluido al borrador", "error");
    });

}

function closeModalNuevoExpediente(evento){
    evento.preventDefault();

    if(nuevoExpediente==''){
        //si todavia no he creado un expediente
        Swal.fire({
                title: "No ha creado el expediente",
                text: "Desea cerrar?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "SI",
                cancelButtonText: "NO",
                closeOnConfirm: true,
                closeOnCancel: true
            }).then((result) => {
            if (result.value) {
            $("#modal_nuevoexpediente").modal('hide');
        } else if (
            result.dismiss === Swal.DismissReason.cancel
        ) {

        }
    });
    }else{
        $("#modal_nuevoexpediente").modal('hide');
    }

}

function crearBtnCopyClip(pvalue,pmsg){
var coexboton ='<span id="BtnCopyClip" style="display: none">'+'<button type="button" title="Copiar número" class="btn btn-lg white" ' +
    'onclick="copyTextToClipboard(\''+pvalue.trim()+'\', '+pmsg+')" style="float: right; border-radius:20px !important">'+
    '<i class="fa fa-clipboard">'+'</i>'+'</button>'+' </span>';
return coexboton;
}



function cerrarModalExpSinRad(evento){
    evento.preventDefault();


        $("#modal_expediente").modal('hide');


}

function cerosAlaIzquierda(number, width) {
    var numberOutput = Math.abs(number); /* Valor absoluto del número */
    var length = number.toString().length; /* Largo del número */
    var zero = "0"; /* String de cero */

    if (width <= length) {
        if (number < 0) {
            return ("-" + numberOutput.toString());
        } else {
            return numberOutput.toString();
        }
    } else {
        if (number < 0) {
            return ("-" + (zero.repeat(width - length)) + numberOutput.toString());
        } else {
            return ((zero.repeat(width - length)) + numberOutput.toString());
        }
    }
}
