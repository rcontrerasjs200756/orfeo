var ExpedientesService = {
    
    urlApiIntegration: WS_APPSERVICE_URL + '/integration/expedientes',
    urlApi : WS_APPSERVICE_URL + '/expedientes',
    
    updateAcceso: function (data) {
        return $.ajax({
            type: 'POST',
            data: data,
            dataType: 'json',
            async: false,
            url: this.urlApiIntegration+ '/updateAcceso',
            headers: {
                "Authorization": 'Bearer '+configsHasValue['WS_APPSERVICE_TOKEN']
            },
        });
    },

    crearExpediente: function (data) {
        return $.ajax({
            type: 'POST',
            data: data,
            dataType: 'json',
            async: false,
            url: this.urlApi+ '/accionCrearExped',
            headers: {
                "Authorization": 'Bearer '+configsHasValue['WS_APPSERVICE_TOKEN']
            },
        });
    },

    getSecuenciaExpediente: function (depe_codi,serie,subserie,anio) {
        return $.ajax({
            type: 'GET',
            dataType: 'json',
            async: false,
            url: this.urlApi+ '/getSecuenciaExped' + '?' + 'dependencia=' +depe_codi+'&serie='+serie+'&subserie='+subserie+'&anio='+anio,
            headers: {
                "Authorization": 'Bearer '+configsHasValue['WS_APPSERVICE_TOKEN']
            },
        });
    },
}