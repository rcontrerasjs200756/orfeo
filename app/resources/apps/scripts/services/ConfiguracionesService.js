var ConfiguracionesService = {

    urlApi: WS_APPSERVICE_URL + '/configuraciones',

    getAllConfiguraciones: function () {
        return $.ajax({
            type: 'GET',
            dataType: 'json',
            async: false,
            url: this.urlApi + '/getAllConfiguraciones',
            headers: {
                "Authorization": 'Bearer ' + WS_APPSERVICE_TOKEN
            },
        });
    },
    getDayFromDate: function (date) {

        var fecha = new Date(date);

        var base_hours = fecha.getHours();
        var base_minutes = fecha.getMinutes();
        var ampm = base_hours >= 12 ? 'pm' : 'am';
        var hours = base_hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        var minutes = base_minutes < 10 ? '0' + base_minutes : base_minutes;
        var strTime = hours + ':' + minutes + ' ' + ampm;
        var mes = (parseInt(fecha.getMonth()) + parseInt(1))

        return new Array( fecha.getDate(),  base_hours +":"+minutes);
    }

}