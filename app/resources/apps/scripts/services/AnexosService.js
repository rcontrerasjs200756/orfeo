var AnexosService = {

    urlApi: WS_APPSERVICE_URL + '/anexos',

    getAllAnexExtAccept: function () {
        return $.ajax({
            type: 'GET',
            dataType: 'json',
            async: false,
            url: this.urlApi + '/getAllAnexExtAccept',
            headers: {
                "Authorization": 'Bearer ' + WS_APPSERVICE_TOKEN
            },
        });
    }

}