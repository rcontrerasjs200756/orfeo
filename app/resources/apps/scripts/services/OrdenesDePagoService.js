var OrdenesDePagoService = {

    urlApi : WS_APPSERVICE_URL+'/ordenesdepago',

    reasignarRadicado: function (data) {

        return $.ajax({
            type: 'POST',
            data: data,
            dataType: 'json',
            async:false,
            url: this.urlApi+ '/reasignarRadicado',
            headers: {
                "Authorization": 'Bearer '+WS_APPSERVICE_TOKEN
            },
        });
    },
}