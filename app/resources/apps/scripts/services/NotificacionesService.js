var NotificacionesService = {
    
    urlApi: WS_APPSERVICE_URL + '/notificaciones',
    
    sendNotificacion: function (data) {
        return $.ajax({
            type: 'POST',
            data: data,
            dataType: 'json',
            async: false,
            url: this.urlApi+ '/sendNotificacion',
            headers: {
                "Authorization": 'Bearer '+configsHasValue['WS_APPSERVICE_TOKEN']
            },
        });
    },
    
}