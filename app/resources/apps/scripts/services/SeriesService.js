var SeriesService = {

    urlApi: WS_APPSERVICE_URL + '/series/seguridad',

    updateAcceso: function (data) {
        return $.ajax({
            type: 'POST',
            data: data,
            dataType: 'json',
            async: false,
            url: this.urlApi + '/modificar',
            headers: {
                "Authorization": 'Bearer ' + configsHasValue['WS_APPSERVICE_TOKEN']
            },
        });
    },

}