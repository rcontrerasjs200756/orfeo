var BorradorService = {

    urlApi : configsHasValue['WS_APPSERVICE_URL']+'/borrador',

    revisarBorrador: function (data) {
        return $.ajax({
            type: 'POST',
            data: data,
            dataType: 'json',
            url: this.urlApi+ '/revisarBorrador',
            headers: {
                "Authorization": 'Bearer '+configsHasValue['WS_APPSERVICE_TOKEN']
            },
        });
    },

    guardarEditor: function (data) {
        return $.ajax({
            type: 'POST',
            data: data,
            dataType: 'json',
            async: false,
            url: this.urlApi+ '/guardarEditor',
            headers: {
                "Authorization": 'Bearer '+configsHasValue['WS_APPSERVICE_TOKEN']
            },
        });
    },

}