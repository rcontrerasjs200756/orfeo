var RadicadoService = {

    urlApi : WS_APPSERVICE_URL+'/radicado',

    getRadicadoByNume: function (radicado,days = 0) {
        if (typeof(days) == 'UNDEFINED' || typeof(days) == 'undefined' || days == null || days == ''){
            days = 0;
        }
        console.log(days);
        return $.ajax({
            type: 'GET',
            dataType: 'json',
            async:false,
            url: this.urlApi+ '/getRadicadoByNume/'+radicado+'/'+days,
            headers: {
                "Authorization": 'Bearer '+WS_APPSERVICE_TOKEN
            },
        });
    },


    validaEstaEscaneado: function (data) {
        return $.ajax({
            type: 'POST',
            data: data,
            dataType: 'json',
            url: this.urlApi+ '/validaEstaEscaneado',
            headers: {
                "Authorization": 'Bearer '+WS_APPSERVICE_TOKEN
            },
        });
    },

    getFirmantesByRad: function (radicado) {
        return $.ajax({
            type: 'GET',
            dataType: 'json',
            url: this.urlApi+ '/getFirmantesByRad/'+radicado,
            headers: {
                "Authorization": 'Bearer '+WS_APPSERVICE_TOKEN
            },
        });
    },
    getRadPrincByAnexo: function (anexo_id) {
        return $.ajax({
            type: 'GET',
            dataType: 'json',
            url: this.urlApi+ '/getRadPrincByAnexo/'+anexo_id,
            headers: {
                "Authorization": 'Bearer '+WS_APPSERVICE_TOKEN
            },
        });
    },


    getJefesdropdown: function (user_id,dependencia,dependencia_padre,radicado) {
        return $.ajax({
            type: 'GET',
            dataType: 'json',
            url: this.urlApi+ '/getJefesdropdown/'+radicado+'/'+user_id+'/'+dependencia+'/'+dependencia_padre,
            headers: {
                "Authorization": 'Bearer '+WS_APPSERVICE_TOKEN
            },
        });
    },

    getUser: function (user_id) {
        return $.ajax({
            type: 'GET',
            dataType: 'json',
            url: this.urlApi+ '/getUser/'+user_id ,
            headers: {
                "Authorization": 'Bearer '+WS_APPSERVICE_TOKEN
            },
        });
    }
}