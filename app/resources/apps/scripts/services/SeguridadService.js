var SeguridadService = {
    
    urlApi: WS_APPSERVICE_URL + '/integration/seguridad',
    
    autorizacion: function (data) {
        return $.ajax({
            type: 'POST',
            data: data,
            dataType: 'json',
            async: false,
            url: this.urlApi+ '/autorizacion',
            headers: {
                "Authorization": 'Bearer '+configsHasValue['WS_APPSERVICE_TOKEN']
            },
        });
    },
    consultar: function ($tipo,$valor) {
        return $.ajax({
            type: 'GET',
            dataType: 'json',
            async: false,
            url: this.urlApi+ '/consultar' +'?tipo_entidad='+$tipo+'&entidad='+$valor,
            headers: {
                "Authorization": 'Bearer '+configsHasValue['WS_APPSERVICE_TOKEN']
            },
        });
    }


}


