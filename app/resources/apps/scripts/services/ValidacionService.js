var ValidacionService = {

    urlApi : WS_APPSERVICE_URL+'/validacion',

    getDocumentMetadataByRad: function (data) {

        return $.ajax({
            type: 'POST',
            data: data,
            dataType: 'json',
            async:false,
            url: this.urlApi+ '/getDocumentMetadataByRad',
            headers: {
                "Authorization": 'Bearer '+WS_APPSERVICE_TOKEN
            },
        });
    },
    replaceDocVar: function (data) {

        return $.ajax({
            type: 'POST',
            data: data,
            dataType: 'json',
            async:false,
            url: this.urlApi+ '/replaceDocVar',
            headers: {
                "Authorization": 'Bearer '+WS_APPSERVICE_TOKEN
            },
        });
    }
}