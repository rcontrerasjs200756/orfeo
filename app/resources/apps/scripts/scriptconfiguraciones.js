var tituloConfigSelected = "SELECCIONE";
var saveorupdateTitulo = "nuevo";

//variable en javscript que almacena todas las configuraciones actuales, traidas dese la BD
var configuraciones = [];

var titulos_tratamiento = new Array(); //variable que almacena todos los titulos de la tabla titulo_tratamiento

//variable en javscript que almacena los nombres de las constants como llave y el valor como el value.
var configsHasValue = [];

//variable array que almacena los tipos de extenciones aceptadas para los anexos de un radicado
var anexosTipos = [];

//almacena los modulos separados por coma que se quieren mostrar en el modal, si es solo uno,
// dejarlo sin coma al final, coloco DASHBOARD por defecto
var modulosMostrar = "DASHBOARD";

var guardarconfigs = true;

$(document).on('show.bs.modal', '.modal', function (event) {
    var zIndex = 1040 + (10 * $('.modal:visible').length);
    $(this).css('z-index', zIndex);
    setTimeout(function () {
        $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
    }, 0);
});

//funcion que abre el modal de configuraciones
function modalConfig() {

    $.ajax({
        url: base_url + "app/configuraciones/modalconfig.php",
        method: "POST",
        data: {function_call: "modalConfig", modulos: modulosMostrar},
        dataType: 'html',
    }).done(function (result) {

        $("#modal_body_show_configs").html('')
        $("#modal_body_show_configs").html(result)
        $("#modal_show_configs").modal('show')
        $('#formulario_config input').popover();
        $('#formulario_config select').popover();
    })
        .fail(function () {

        });
}

function changeselect(esto, constante) {


    if (constante == 'FUNCTION_ANULA_AUTO' && $(esto).val() == 'SI') {

        if ($('#ANULACIONES_EXPEDIENTE').val() != undefined && $('#ANULACIONES_EXPEDIENTE').val() != '') {
            var texto = $('#ANULACIONES_EXPEDIENTE').val()
            if (anio_actual != texto.substring(0, 4)) {
                Swal.fire("Alerta", "El año del expediente anual, no coincide con el año actual", "warning");
                guardarconfigs = false
                return false;
            }
        }
    }

    guardarconfigs = true;
}

// al presionar el boton de guardar configuraciones
function guardarConfigs() {

    $('#FUNCTION_ANULA_AUTO').change();

    if (guardarconfigs) {
        $.ajax({
            url: base_url + "app/configuraciones/generalconfig.php",
            method: "POST",
            data: $('#formulario_config').serialize() + "&function_call=guardarConfigs",
            dataType: 'json',
        }).done(function (result) {
            if (result.error == undefined) {
                alertModal('Felicidades', 'Las configuraciones se han guardado con éxito', 'success');
                configuraciones = result.configuraciones;
                armarConfigsHasValues();
                $("#modal_show_configs").modal('hide');
                setTimeout(function () {
                    //llamarModuloBorradores();
                }, 2500)
            } else {
                alertModal('Error', result.error, 'error');

            }

        })
            .fail(function () {
            });
    }
}

//guarda los datos del modal de perfil con configuraciones
function guardarPerfilConfigs() {
    $.ajax({
        url: base_url + "app/configuraciones/generalconfig.php",
        method: "POST",
        data: $('#formulario_perfil_config').serialize() + "&function_call=guardarPerfilConfigs",
        dataType: 'json',
    }).done(function (result) {
        alertModal('Felicidades', 'Las configuraciones se han guardado con éxito', 'success');
        $("#modal_perfil_configs").modal('hide');
    })
        .fail(function () {

        });
}


function changeTituloConfig(esto) {
    tituloConfigSelected = $(esto).val()
    if ($(esto).val() != "SELECCIONE") {
        $(".btntitulosconfig").css('display', 'block');
    } else {
        $(".btntitulosconfig").css('display', 'none');
    }
}

//muestra los datos del titulo en el modal de editar
function showDataTituloEnModal(id) {

    for (var i = 0; i < titulos_tratamiento.length; i++) {
        console.log('titulos_tratamiento', titulos_tratamiento)
        if (titulos_tratamiento[i].ID == id) {
            console.log('paso')
            $("#inputedit_titulo").val(titulos_tratamiento[i].TITULO)
            $("#input_abreviatura").val(titulos_tratamiento[i].ABREVIATURA)
            $("#input_titulo_orden").val(titulos_tratamiento[i].ORDEN)

            if (titulos_tratamiento[i].ACTIVO == "1") {
                $("#radio_titulo_activo_no").removeAttr('checked')
                $("#radio_titulo_activo_si").attr('checked', 'checked')
            } else {
                $("#radio_titulo_activo_si").removeAttr('checked')
                $("#radio_titulo_activo_no").attr('checked', 'checked')
            }

        }
    }
}

function editarTitulo() {

    if ($("#TITULO_TRATAMIENTO").val() != "SELECCIONE") {
        saveorupdateTitulo = "editar"
        $("#strong_modal_edit_titulo").html('')
        $("#strong_modal_edit_titulo").html('Editar Título')
        showDataTituloEnModal(tituloConfigSelected);

        $("#modal_editar_titulos").modal('show')
        setTimeout(function () {
            $("#inputedit_titulo").focus()
        }, 500)

    }
}

function crearTitulo() {
    saveorupdateTitulo = "nuevo"
    $("#strong_modal_edit_titulo").html('')
    $("#strong_modal_edit_titulo").html('Crear Título')
    $("#inputedit_titulo").val('')
    $("#input_abreviatura").val('')
    $("#radio_titulo_activo_si").attr('checked', 'checked')
    $("#input_titulo_orden").val(parseInt(titulos_tratamiento.length) + parseInt(1))
    $("#modal_editar_titulos").modal('show');
    $("#modal_editar_titulos").css('z-index', 99999);
    setTimeout(function () {
        $("#inputedit_titulo").focus()
    }, 500)
}

function crearTituloMemo() {
    $("#add_name_titulo_memo").val('')
    $("#modal_agregar_titulo_memo").modal('show');
    $("#modal_agregar_titulo_memo").css('z-index', 99999);
    setTimeout(function () {
        $("#add_name_titulo_memo").focus()
    }, 500)
}

function guardarTitulo() {

    if ($("#inputedit_titulo").val() == "") {
        alertModal('Guadar Título', 'Debe ingresar un título', 'warning')
        return false;
    }


    $.ajax({
        url: base_url + "app/configuraciones/generalconfig.php",
        method: "POST",
        data: {
            function_call: "guardartitulos", titulo: $("#inputedit_titulo").val(),
            abreviatura: $("#input_abreviatura").val(), orden: $("#input_titulo_orden").val(),
            activo: $("#modal_editar_titulos input[name=radio_titulo_activo]:checked").val(),
            saveorupdateTitulo: saveorupdateTitulo, tituloConfigSelected: tituloConfigSelected
        },
        dataType: 'json',
    }).done(function (result) {
        if (result.error != undefined) {
            alertModal('Guadar Título', result.error, 'error')
            return false;
        } else {
            tituloConfigSelected = "SELECCIONE";
            saveorupdateTitulo = "nuevo";
            $(".btntitulosconfig").css('display', 'none');
            alertModal('Guadar Título', 'El Título ha guardado con éxito', 'success')
            titulos_tratamiento = result.titulos
            $("#modal_editar_titulos").modal('hide')
            armarTitulosSelectConfig();
        }

    })
        .fail(function () {

        });
}

function guardarTituloMemo() {

    if ($("#add_name_titulo_memo").val() == "") {
        alertModal('Guadar Título', 'Debe ingresar un título', 'warning')
        return false;
    }
    $.ajax({
        url: base_url + "app/configuraciones/generalconfig.php",
        method: "POST",
        data: {
            function_call: "guardartitulosmemos", titulo: $("#add_name_titulo_memo").val()
        },
        dataType: 'json',
    }).done(function (result) {
        if (result.error != undefined) {
            alertModal('Guadar Título', result.error, 'error')
            return false;
        } else {

            $(".btntitulosconfig").css('display', 'none');
            alertModal('Guadar Título', 'El Título ha guardado con éxito', 'success')
            $("#modal_agregar_titulo_memo").modal('hide')
            armarTitulosMemorando(result);
        }

    })
        .fail(function () {

        });
}

function armarTitulosMemorando(data){

    var dataarmada = {
        id: Object.keys(data.opciones).length,
        text: data.opciones[(Object.keys(data.opciones).length)-1]
    };

    var newOption = new Option(dataarmada.text, dataarmada.id, false, false);
    $('#TITULO_MEMO').append(newOption).trigger('change');

}

function borrarTitulo() {
    Swal.fire({
            title: "Alerta",
            text: "Desea eliminar el título seleccionado?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Confirmar",
            cancelButtonText: "Cancelar",
            closeOnConfirm: true,
            closeOnCancel: true
        }).then((result) => {
        if (result.value) {
        accionEliminarTitulo()
    } else if (
        result.dismiss === Swal.DismissReason.cancel
    ) {

    }
});
}

//arma los titulos en el select de configuraciones
function armarTitulosSelectConfig() {
    $("#TITULO_TRATAMIENTO").html('')
    var html = ' <option value="SELECCIONE" selected>SELECCIONE</option>';
    for (var i = 0; i < titulos_tratamiento.length; i++) {
        html += '<option value="' + titulos_tratamiento[i]['ID'] + '" >' + titulos_tratamiento[i]['TITULO'] + '</option>';
    }
    $("#TITULO_TRATAMIENTO").append(html)
}

function declararSelect2(selectsmultiples){

    $("#TIPOS_RADICADO_VALIDAR").select2();

     for (var i = 0; i < Object.keys(selectsmultiples).length; i++) {

         $("#"+selectsmultiples[i]).select2(
             {
                 width: '100%',
                 dropdownParent: $("#modal_show_configs"),
                 allowClear: true,
                 multiple:true,
                 escapeMarkup: function (markup) {
                     return markup;
                 }, // let our custom formatter work

                 language: {
                     inputTooShort: function () {
                         return 'Ingrese un año para buscar';
                     },
                     noResults: function () {
                         return "Sin resultados";
                     }
                 }
             })
    }

}

function accionEliminarTitulo() {


}

//muestra el modal para poder seleccionar que perfiles pueden ver y editar las configuraciones
function modalPerfilConfig() {
    var languageseelct2 = {
        inputTooShort: function () {
            return 'Ingrese para buscar';
        },
        noResults: function () {
            return "Sin resultados";
        },
        searching: function () {
            return "Buscando...";
        },
        errorLoading: function () {
            return 'El resultado aún no se ha cargado.';
        },
        loadingMore: function () {
            return 'Cargar mas resultados...';
        },
    }
    $.ajax({
        url: base_url + "app/configuraciones/modalperfilconfigs.php",
        method: "POST",
        dataType: 'html',
        async: false
    }).done(function (result) {

        $("#modal_body_perfil_configs").html('')
        $("#modal_body_perfil_configs").html(result)
        $("#modal_perfil_configs").modal('show')
        $('#formulario_config input').popover();
        $('#formulario_config select').popover();
    })
        .fail(function () {

        });

    $("select").chosen({width: '100%', search_contains: true});


}

//funcion que arma la variable configsHasValue
function armarConfigsHasValues() {

    configsHasValue = [];

    for (var i = 0; i < Object.keys(configuraciones).length; i++) {
        configsHasValue[configuraciones[i].NOMBRE_CONSTANTE] = "";
        configsHasValue[configuraciones[i].NOMBRE_CONSTANTE] = configuraciones[i].VALOR
    }

}

//funcion que arma la variable configsHasValue
function armarConfigsHasValuesMayus(configSession) {

    configsHasValue = [];
    
    //configSession.foreach(recorrerConfig);
    //configSession = JSON.parse(configSession);
    console.log(typeof(configSession));

    for (var i = 0; i < Object.keys(configSession).length; i++) {
        
        configsHasValue[configSession[i].NOMBRE_CONSTANTE] = "";
        configsHasValue[configSession[i].NOMBRE_CONSTANTE] = configSession[i].VALOR
    }

}

function recorrerConfig(item,index) {

    //configsHasValue = [];
    console.log('config');
    console.log(item);
    console.log(index);

    //for (var i = 0; i < Object.keys(configSession).length; i++) {
        
        //configsHasValue[configSession[i].NOMBRE_CONSTANTE] = "";
        //configsHasValue[configSession[i].NOMBRE_CONSTANTE] = configSession[i].VALOR
    //}

} 


//funcion que arma la variable configsHasValue
function armarConfigsHasValuesMinuscula(configs) {

    configsHasValue = [];

    for (var i = 0; i < Object.keys(configs).length; i++) {
        configsHasValue[configs[i].nombre_constante] = "";
        configsHasValue[configs[i].nombre_constante] = configs[i].valor;
    }

}
