var ReasignarRadicado = {

    rutaToBuscarUsuariosComen: 'app/borradores/',
    radicadoReasignar: '',

    /**
     * Levanta el modal para reasignar el radicado
     */
    showModalReasignar: function (evento, radicado,userLogId,usr = 1,usrDnomb = 1) {
        evento.preventDefault();
        var $encontro = false;
        for (var i = 0; i < Object.keys(usersValidaron).length; i++) {
            if (usersValidaron[i].user.id == userLogId) {
                $encontro = true;
            }
        }
        if($encontro == false){
            Swal.fire({
                title: "Alerta",
                text: "No ha "+conjugacionValidar[9]+" el documento.  Desea reasignar?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "SI",
                cancelButtonText: "NO",
                closeOnConfirm: true,
                closeOnCancel: true
            }).then((result) => {
                if (result.value) {
                    if (usr != 1){
                        setTimeout(function () {
                            console.log('reasignar ts');
                            var dropdown = $('#enviara');
                            var option = new Option( usrDnomb, usr, true, true);
                            dropdown.append(option).trigger('change');
                        }, 50);
                    }
                ReasignarRadicado.radicadoReasignar = radicado;
                //levanta el modal de reasignar el radicado

                console.log(radicado);
                $('#comentarioenviar').maxlength({
                    alwaysShow: true,
                    threshold: 9,
                    appendToParent: true,
                    warningClass: "label label-success",
                    limitReachedClass: "label label-danger",
                    preText: 'Mínimo ' + configsHasValue['COMENTARIOS_MINLENGTH'] + ' caracteres. ',
                    validate: true
                });

                $("#comentarioenviar").val('');
                $("#comentarioenviar").html('');
                $("#enviara").val('');

                $("#enviara").select2(
                    {
                        width: "100%",
                        dropdownParent: $("#modal_reasignar"),
                        allowClear: true,
                        data: {},
                        ajax: {
                            url: base_url + ReasignarRadicado.rutaToBuscarUsuariosComen + "buscarUsuariosComen.php",
                            dataType: 'json',
                            delay: 250,
                            data: function (params) {
                                return {
                                    search: params.term,
                                    type: 'public',
                                };
                            },
                            processResults: function (data, params) {
                                return {
                                    results: data,

                                };
                            },
                            cache: true
                        },
                        placeholder: 'Buscar Usuarios',
                        escapeMarkup: function (markup) {
                            return markup;
                        }, // let our custom formatter work

                        language: {
                            inputTooShort: function () {
                                return 'Ingrese un nombre para buscar';
                            }
                        }
                    });

                $('#div_enviar_marcar_como_revisado').css('display', 'none');
                $('.icheckbox_flat-green').removeClass('checked');
                $('#check_enviar_marcar_como_revisado').prop('checked', false)


                $("#modal_reasignar").modal('show');
                $("#modal_reasignar").css('z-index', '9999');
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                $encontro = false;
            }
        });
        }
        if($encontro == false){
            console.log('hola modal');
            return false;
        }else {

            if (usr != 1){
                setTimeout(function () {
                    console.log('reasignar ts');
                    var dropdown = $('#enviara');
                    var option = new Option( usrDnomb, usr, true, true);
                    dropdown.append(option).trigger('change');
                }, 50);
            }

            ReasignarRadicado.radicadoReasignar = radicado;
            //levanta el modal de reasignar el radicado

            console.log(radicado);
            $('#comentarioenviar').maxlength({
                alwaysShow: true,
                threshold: 9,
                appendToParent: true,
                warningClass: "label label-success",
                limitReachedClass: "label label-danger",
                preText: 'Mínimo ' + configsHasValue['COMENTARIOS_MINLENGTH'] + ' caracteres. ',
                validate: true
            });

            $("#comentarioenviar").val('');
            $("#comentarioenviar").html('');
            $("#enviara").val('');

            $("#enviara").select2(
                {
                    width: "100%",
                    dropdownParent: $("#modal_reasignar"),
                    allowClear: true,
                    data: {},
                    ajax: {
                        url: base_url + ReasignarRadicado.rutaToBuscarUsuariosComen + "buscarUsuariosComen.php",
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                search: params.term,
                                type: 'public',
                            };
                        },
                        processResults: function (data, params) {
                            return {
                                results: data,

                            };
                        },
                        cache: true
                    },
                    placeholder: 'Buscar Usuarios',
                    escapeMarkup: function (markup) {
                        return markup;
                    }, // let our custom formatter work

                    language: {
                        inputTooShort: function () {
                            return 'Ingrese un nombre para buscar';
                        }
                    }
                });

            $('#div_enviar_marcar_como_revisado').css('display', 'none');
            $('.icheckbox_flat-green').removeClass('checked');
            $('#check_enviar_marcar_como_revisado').prop('checked', false)


            $("#modal_reasignar").modal('show');
            $("#modal_reasignar").css('z-index', '9999');
        }
    },

    /**
     * Cuando se le da click al modal, el botonreasignar
     * @param evento
     */
    clickModalReasignar: function (evento) {
        evento.preventDefault();
        var usuario_id = $("#enviara").val()
        var comentarioenviar = $("#comentarioenviar").val()
        var UsuaSameDep =[];
        var alertConfigD = true;
        if (usuario_id == null || usuario_id == 'null' || usuario_id == '' || usuario_id == undefined) {
            Swal.fire("Alerta", "Seleccione una persona", "warning");
            return false;
        }
        console.log('empieza el beta');
        if (radicados_selecteds_v[0].sgd_trad_codigo == 4){
                console.log('paso 1');
                    RadicadoService.getUser(usuario_id).success(function (response) {
                    if (response.error == undefined) {
                        //recorro los usuarios que estan en la configuracion
                        $.each(userConfigurados, function (ind,elem) {
                            //si el usuario es el mismo de la configuracion no deberia correr la alert
                            if (elem.id == response.user.id){
                                alertConfigD = false;
                            }
                            //si el usuario que se le va a reasignar tiene la misma dependencia de un usuario configurado lo guardo para crear el modal luego


                            if (elem.dependencia.id == response.user.dependencia.id && elem.dependencia.id != dependencia){
                                UsuaSameDep.push(elem);

                            }
                        });

                        console.log([alertConfigD,UsuaSameDep]);

                        if (alertConfigD == true && UsuaSameDep.length > 0){

                            var countA = UsuaSameDep.length;

                            var htmlAlertR = 'Los Reportes deberian enviarse a:<br>';

                            $.each(UsuaSameDep, function (ind,elem) {
                                //si hay usuarios los listo en el modal
                                htmlAlertR +=  elem.usua_nomb;
                                if (countA == ind){
                                    htmlAlertR += ' ó';
                                }else if(countA == (ind + 1)) {
                                    htmlAlertR += ' ';
                                }else{
                                    htmlAlertR += ',';
                                }
                                htmlAlertR += '<br>';
                            });

                            htmlAlertR += '¿Cambiar usuario?';

                            console.log(htmlAlertR);

                            //@formatter:off
                            Swal.fire({
                                title: "Alerta",
                                html: htmlAlertR,
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonText: "Si",
                                cancelButtonText: "No",
                                closeOnConfirm: true,
                                closeOnCancel: true
                            }).then((result) => {
                                if (result.value) {

                                    return false;

                            } else if (
                                result.dismiss === Swal.DismissReason.cancel
                            ) {
                                if (comentarioenviar.length >= configsHasValue['COMENTARIOS_MINLENGTH']) {
                                    ReasignarRadicado.ejecutarReasignar(usuario_id, comentarioenviar,doc_codusuario);
                                } else {
                                    Swal.fire("Alerta", "Debe ingresar un comentario de al menos " + configsHasValue['COMENTARIOS_MINLENGTH'] + " caracteres", "warning");
                                }
                            }
                        });

                        }else{

                            console.log('paso 3');

                            if (comentarioenviar.length >= configsHasValue['COMENTARIOS_MINLENGTH']) {
                                ReasignarRadicado.ejecutarReasignar(usuario_id, comentarioenviar,doc_codusuario);
                            } else {
                                Swal.fire("Alerta", "Debe ingresar un comentario de al menos " + configsHasValue['COMENTARIOS_MINLENGTH'] + " caracteres", "warning");
                            }

                        }


                    } else {
                        Swal.fire("Error", response.message, "error");
                    }
                }).error(function (response) {

                    Swal.fire("Error", response.message, "error");
                });

        }else{

            if (comentarioenviar.length >= configsHasValue['COMENTARIOS_MINLENGTH']) {
                ReasignarRadicado.ejecutarReasignar(usuario_id, comentarioenviar,doc_codusuario);
            } else {
                Swal.fire("Alerta", "Debe ingresar un comentario de al menos " + configsHasValue['COMENTARIOS_MINLENGTH'] + " caracteres", "warning");
            }
        }

    },

    ejecutarReasignarD: function (usuario_id, comentarioenviar, userLogguedId,radicado_num) {

        console.log(ReasignarRadicado.radicadoReasignar);
        var params = {
            userLogguedId: userLogguedId,
            usuario_id: usuario_id,
            comentarioenviar: comentarioenviar,
            radicado: radicado_num
        }
        $('#guardarEnviar').prop('disabled', true);
        OrdenesDePagoService.reasignarRadicado(params).success(function (response) {
            if (response.error == undefined) {
                alertModal('Reasignado', 'Radicado '+ReasignarRadicado.radicadoReasignar+' reasignado con éxito', 'success', 7000)
                doc_showmodalaprobardoc(radicado_num);
            } else {
                $('#guardarEnviar').prop('disabled', false);
                Swal.fire("Error", response.message, "error");
            }
        }).error(function (response) {
            $('#guardarEnviar').prop('disabled', false);
            Swal.fire("Error", response.message, "error");
        });



    },

    ejecutarReasignar: function (usuario_id, comentarioenviar, userLogguedId) {


            

        var params = {
            userLogguedId: userLogguedId,
            usuario_id: usuario_id,
            comentarioenviar: comentarioenviar,
            radicado: ReasignarRadicado.radicadoReasignar,
            fromMod: 'reasignar'
        }

        $('#guardarEnviar').prop('disabled', true);
        OrdenesDePagoService.reasignarRadicado(params).success(function (response) {
            if (response.error == undefined) {
                alertModal('Reasignado', 'Radicado '+ReasignarRadicado.radicadoReasignar+' reasignado con éxito', 'success', 7000)
                setTimeout(function () {
                    $(location).attr('href','./app/dashboard/index.php?'+session_name +'='+session_id);
                },3500);

                //$('#modal_reasignar').modal('hide');
                //$('#modal_aprobar_documento').modal('hide');


            } else {
                $('#guardarEnviar').prop('disabled', false);
                Swal.fire("Error", response.message, "error");
            }
        }).error(function (response) {
            $('#guardarEnviar').prop('disabled', false);
            Swal.fire("Error", response.message, "error");
        });




    },



}