var AsociarImagenes = {

    validaEstaEscaneado: function (formulario,radicado) {


        /**
         * primero busco si ya fue validado y los usuarios que ya lo validaron si es que es asi
         * @type {{radicado: *}}
         */
        var params = {
            radicado: radicado,
        }

        ValidacionService.getDocumentMetadataByRad(params).success(function (response) {
            if (response.error == undefined) {

                if (response.count > 0) {
                    /**
                     * si entra aqui, quiere decir que ya lo validaron
                     */
                    console.log(response);
                    var usuariosQueValidaron = response.objects.document_authorized_by; //usuarios que ya validaron el doc: document_authorized_by
                    Swal.fire("No Permitido", "El radicado "+radicado+" ya tiene "+Object.keys(usuariosQueValidaron).length+ ' '+conjugacionValidar[10] , "warning");
                        return false;
                }else{
                    /**
                     * entra aqui si no ha sido validado, busco si fue escaneado
                     * @type {any[]}
                     */
                    var radicadoarray=new Array();
                    radicadoarray[0]=radicado
                    var data = {
                        'radicados': radicadoarray
                    }
                    RadicadoService.validaEstaEscaneado(data).success(function (response) {

                        if(response.error===undefined){

                            if(response.objects[0].fueescaneado==1) {
                                //@formatter:off
                                    Swal.fire({
                                            title: "El radicado ya se encuentra escaneado/digitalizado",
                                            text: 'seguro desea continuar?',
                                            type: "warning",
                                            showCancelButton: true,
                                            confirmButtonText: "SI",
                                            cancelButtonText: "NO",
                                            closeOnConfirm: true,
                                            closeOnCancel: true
                                        }).then((result) => {
                                        if (result.value) {
                                       formulario.submit()
                                    } else if (
                                        result.dismiss === Swal.DismissReason.cancel
                                    ) {

                                    }
                                });
                                    //@formatter:on
                            }else{
                                formulario.submit()
                            }

                        }else{
                            Swal.fire("Error", "Ha ocurrido un error al verificar si está escaneado", "error");

                        }


                    }).error(function (error) {
                        Swal.fire("Error", error.message, "error");

                        return false;
                    });
                }


            }
        }).error(function (error) {
            Swal.fire("Error", "Ha ocurrido un error al buscar los firmantes", "error");
        });


    }

}