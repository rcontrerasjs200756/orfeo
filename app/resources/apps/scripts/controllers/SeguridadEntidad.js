var Seguridad = {
    entidad_tipo : '',
    entidad_valor : '',
    entidad_acceso : '',
    entidad_justificacion : '',
    entidad_resetear : false,
    showModal: function (modal){
        $('#modal_seguridad').modal('show');
        return true;
    },

    consultarSeguridad: function (tipo,valor){
        var ajax;

             ajax = SeguridadService.consultar(tipo,valor);


        Seguridad.entidad_valor = '';
        Seguridad.entidad_acceso = '';
        Seguridad.entidad_justificacion = '',
        ajax.success(function (response){
           console.log(JSON.stringify(response));
           Seguridad.entidad_valor = valor;
           Seguridad.entidad_acceso = response.acceso;
           //Seguridad.entidad_acceso = 'R';
           Seguridad.entidad_justificacion = response.acceso_justificacion;
           Seguridad.showModal();
           Seguridad.iniciarPopover(Seguridad.entidad_acceso);
           Seguridad.definirUsersDepends();
            /*if (Object.keys(allUsers).length < 1) {
                Seguridad.traerUsers('usuarios')
            }

            if (Object.keys(allDepend).length < 1) {
                Seguridad.traerUsers('dependencias')
            }*/
           Seguridad.definirSelectValores(response);
           if(Seguridad.entidad_acceso == 'R'){
               $('#swich_seguridad').bootstrapSwitch('state',false, true);
           }else{
               $('#swich_seguridad').bootstrapSwitch('state',true, true);
           }
        }).error(function (error) {

            alertModal('Advertencia', 'error consultando la '+Seguridad.entidad_tipo, 'error', '5000');

        });
    },
    justificacionAndSave: function (state){
        if (state){
            Seguridad.entidad_justificacion = '';
            Seguridad.entidad_acceso = 'P';
            var data = {

                'acceso' : Seguridad.entidad_acceso,
                'justificacion' : Seguridad.entidad_justificacion,
                'usuario_id' : usuario_id,
                'resetear' : Seguridad.entidad_resetear
            }

            if (Seguridad.entidad_tipo == 'serie'){
                data.serie = Seguridad.entidad_valor;
                ajaxChangeAcceso = SeriesService.updateAcceso(data);
            }else if (Seguridad.entidad_tipo == 'subserie'){
                data.subserie = Seguridad.entidad_valor;
                ajaxChangeAcceso = SubSeriesService.updateAcceso(data);
            }


            ajaxChangeAcceso.success( function (response){
                Seguridad.cambiarIconos('P',Seguridad.entidad_valor);
                Seguridad.popoverPublico(Seguridad.entidad_tipo);
                if (Seguridad.entidad_resetear == false){
                    $("#select_dependencias").val(null);
                    $('#select_dependencias').trigger('change.select2');
                    $("#id_select_users").val(null);
                    $('#id_select_users').trigger('change.select2');
                }
                alertModal('Actualizado', 'el acceso ahora es PUBLICO ', 'success', '5000');
            }).error( function (error){
                Seguridad.entidad_acceso = 'R';
                Seguridad.popoverRestringido(Seguridad.entidad_tipo);
                $('#swich_seguridad').bootstrapSwitch('state', !state, true);
                return false;
                alertModal('Advertencia', 'error actualizando la serie '+Seguridad.entidad_valor, 'error', '5000');
            });

        }else{

            Seguridad.entidad_acceso = 'R';
            Swal.fire({
                title: 'Justificacion de Restriccion',
                input: 'text',
                inputPlaceholder: 'introduce tu justificacion',
                inputValue: Seguridad.entidad_justificacion,
                showCancelButton: true,
                inputValidator: (value) => {
                    if (!value) {
                        return 'Debes Justificar la restriccion'
                    }
                }
            }).then((result) => {
                if (result.value) {
                    /**
                     * Si entra aqui, es porque confirma que quiere restringir
                     */
                    Seguridad.popoverRestringido(Seguridad.entidad_tipo);
                    Seguridad.entidad_justificacion = result.value;
                    var data = {
                        'acceso' : Seguridad.entidad_acceso,
                        'justificacion' : Seguridad.entidad_justificacion,
                        'usuario_id' : usuario_id,
                        'resetear' : Seguridad.entidad_resetear
                    }
                    var ajaxChangeAcceso;
                    if (Seguridad.entidad_tipo == 'serie'){
                        data.serie = Seguridad.entidad_valor;
                        ajaxChangeAcceso = SeriesService.updateAcceso(data);
                    }
                    if (Seguridad.entidad_tipo == 'subserie'){
                        data.subserie = Seguridad.entidad_valor;
                        ajaxChangeAcceso = SubSeriesService.updateAcceso(data);
                    }
                    console.log('veo data');
                    console.log(data);
                    ajaxChangeAcceso.success( function (response){
                        Seguridad.cambiarIconos('R',Seguridad.entidad_valor);
                        if(Seguridad.entidad_resetear == false){
                            $("#select_dependencias").val(null);
                            $('#select_dependencias').trigger('change.select2');
                            $("#id_select_users").val(null);
                            $('#id_select_users').trigger('change.select2');
                        }
                        alertModal('Actualizado', 'el acceso ahora es RESERVADO', 'success', '5000');
                    }).error( function (error){
                        Seguridad.entidad_acceso = 'P';
                        Seguridad.popoverPublico(Seguridad.entidad_tipo);
                        $('#swich_seguridad').bootstrapSwitch('state', !state, true);
                        return false;
                        alertModal('Advertencia', 'error actualizando la serie '+Seguridad.entidad_valor, 'error', '5000');
                    });
                }else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    /**
                     * Si entra aqui, es porque cancela
                     */
                    Seguridad.entidad_acceso = 'P';
                    Seguridad.popoverPublico(Seguridad.entidad_tipo);
                    $('#swich_seguridad').bootstrapSwitch('state', !state, true);
                    return false;
                }
            });
        }
    }
    ,

    swichChange: function (state){


        Swal.fire({
            title: 'Listas de acceso:',
            icon: 'warning',
            text: "Mantener las listas de control de acceso?",
            showCancelButton: true,
            showConfirmButton: true,
            confirmButtonText: `Mantener`,
            cancelButtonText: `Reiniciar`
        }).then((result) => {
        if (result.value) {
            Seguridad.entidad_resetear = true;
            Seguridad.justificacionAndSave(state);
        }else if (
            /* Read more about handling dismissals below */
            result.dismiss === Swal.DismissReason.cancel
        ) {
            Seguridad.entidad_resetear = false;
            Seguridad.justificacionAndSave(state);
        }
    });

        /*if(state){
            Seguridad.entidad_acceso = 'P';
            Seguridad.popoverRestringido(Seguridad.entidad_tipo);
            $('#swich_seguridad').bootstrapSwitch('state', !state, true);
            return false;
        }else{
            Seguridad.entidad_acceso = 'R';
            Seguridad.popoverPublico(Seguridad.entidad_tipo);
            $('#swich_seguridad').bootstrapSwitch('state', !state, true);
            return false;
        }*/





    },

    popoverRestringido: function (entidadT) {
        $(".divshiden").css('display', 'block');
        $(".bootstrap-switch-label").html('Pública')
        $(".bootstrap-switch-label").attr('data-original-title', 'Quitar restricción')
        $(".bootstrap-switch-label").attr('data-content', 'Todos los usuarios tendrán acceso a la '+entidadT)
        $(".bootstrap-switch-handle-off").html('Reservada')
        $("#labelentidadestado").html('')
        $("#labelentidadestado").html('<span id="icBloq"><i style="padding-left: 0.5em;padding-right: 0.5em;" class="fa fa-2x fa-lock font-red"></i></span>'+capitalizarPrimeraLetra(entidadT)+' Reservada, con restricción de acceso')
        $("#btnLoadModalSegur").html('')
        $("#btnLoadModalSegur").html("<img src='imagenes/candado.jpg' alt='' border=0 width='25' height='25' >Reservada");
    },

    popoverPublico: function (entidadT) {
        $(".divshiden").css('display', 'none');
        $(".bootstrap-switch-label").html('Restringir')
        $(".bootstrap-switch-label").attr('data-original-title', 'Restringir acceso a la '+entidadT)
        $(".bootstrap-switch-label").attr('data-content', 'Solo tendrá acceso a la '+entidadT+' el usuario actual ' +
            'y usuarios o dependencias autorizadas!')
        $(".bootstrap-switch-handle-off").html('Restringir')

        $("#labelentidadestado").html('')
        $("#labelentidadestado").html('<span id="icBloq"><i style="padding-left: 0.5em;padding-right: 0.5em;" class="fa fa-2x fa-unlock font-green"></i></span>'+capitalizarPrimeraLetra(entidadT) +' Publica, sin restricción de acceso')
        $("#btnLoadModalSegur").html('')
        $("#btnLoadModalSegur").html("<img src='imagenes/candado.jpg' alt='' border=0 width='25' height='25' >Pública");
    },

    iniciarPopover: function (state){
        if (state == 'P'){
            Seguridad.popoverPublico(Seguridad.entidad_tipo);
        }else{
            Seguridad.popoverRestringido(Seguridad.entidad_tipo);
        }
        $('.bootstrap-switch-label').attr('data-placement', 'top')
        $('.bootstrap-switch-label').attr('data-trigger', 'hover')
        $('.bootstrap-switch-label').attr('data-container', 'body')
        $('.bootstrap-switch-label').popover();
    },

    cambiarIconos: function (state,id){
        var textoSeg;
        if (state == 'P'){
            textoSeg = ' Publica ';
        }else{
            textoSeg = ' Reservada ';
        }
        console.log($('#spanID'+id));
        console.log(textoSeg);
        $('#spanID'+id).text(textoSeg);
    },

    definirUsersDepends: function (){
        $("#id_select_users").select2(
            {
                width: '100%',
                multiple: true,
                dropdownParent: $(document.body),
                allowClear: true,
                data: allUsers,
                ajax: {
                    url: configsHasValue['WS_APPSERVICE_URL'] + '/seguridadRad/usersToAuthorize',
                    dataType: 'json',
                    delay: 250,
                    headers: {
                        "Authorization": 'Bearer '+configsHasValue['WS_APPSERVICE_TOKEN']
                    },
                    data: function (params) {
                        return {
                            search: params.term,
                            type: 'public',
                            quebuscar: 'usuarios'
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: data.objects,
                        };
                    },
                    cache: true,
                    error: function(xhr, ajaxOptions, thrownError){
                        console.log(xhr);
                        if(typeof(xhr.responseJson) != 'undefined'){

                            if (typeof(xhr.responseJson.message) != 'undefined'){
                                Swal.fire("Error", xhr.responseJSON.message, "error");
                            }
                        }

                        //else{
                        //    Swal.fire("Error", "Ha ocurrido un un error al buscar los datos", "error");
                        //}
                    }
                },
                placeholder: 'Buscar Usuarios',
                escapeMarkup: function (markup) {
                    return markup;
                }, // let our custom formatter work
                language: languageseelct2
            }).on('select2:selecting', function (e) {
            //cada vez que selecciono una opcion
            Seguridad.guardarUserSeg(e.params.args.data)

        }).on("select2:unselecting", function (e) {

            //cada vez que desmarco una opcion
            Seguridad.quitarUserSeg(e.params.args.data,'USUARIO')

        }).trigger('change');


        $("#select_dependencias").select2(
            {
                width: '100%',
                multiple: true,
                dropdownParent: $(document.body),
                allowClear: true,
                data: allDepend,
                ajax: {
                    url: configsHasValue['WS_APPSERVICE_URL'] + '/seguridadRad/usersToAuthorize',
                    dataType: 'json',
                    delay: 250,
                    headers: {
                        "Authorization": 'Bearer '+configsHasValue['WS_APPSERVICE_TOKEN']
                    },
                    data: function (params) {
                        return {
                            search: params.term,
                            type: 'public',
                            quebuscar: 'dependencias'
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: data.objects,
                        };
                    },
                    cache: true,
                    error: function(xhr, ajaxOptions, thrownError){
                        if(typeof(xhr.responseJson) != 'undefined'){

                            if (typeof(xhr.responseJson.message) != 'undefined'){
                                Swal.fire("Error", xhr.responseJSON.message, "error");
                            }
                        }
                    }
                },
                placeholder: 'Buscar Dependencias',
                escapeMarkup: function (markup) {
                    return markup;
                }, // let our custom formatter work
                language: languageseelct2
            }).on('select2:selecting', function (e) {
            //cada vez que selecciono una opcion
            Seguridad.guardarUserSeg(e.params.args.data)

        }).on("select2:unselecting", function (e) {

            //cada vez que desmarco una opcion
            Seguridad.quitarUserSeg(e.params.args.data,'DEPENDENCIA');

        }).trigger('change');
    },

    updateSeguridad: function (accion, tipo, seleccion) {
        var transaccion = 'AUTORIZAR';
        if (accion == 'quitar'){
            transaccion = 'DESAUTORIZAR';
        }else{
            transaccion = 'AUTORIZAR';
        }
        var v_entidad_tipo = Seguridad.entidad_tipo;
        var paramsAcceso = {

            'tipo_entidad_autorizada': tipo,
            'tipo_entidad' : Seguridad.entidad_tipo,
            'entidad_autorizada' : seleccion.id,
            'usuario_id' : usuario_id,
            'transaccion' : transaccion
        }

        paramsAcceso[v_entidad_tipo] = Seguridad.entidad_valor;

        var ajax = SeguridadService.autorizacion(paramsAcceso);
        ajax.success(function (response) {
            if(response.isSuccess){

                alertModal('Actualizado!', capitalizarPrimeraLetra(response.tipo_entidad_aut) +' '+ response.accion_entidad +': '+retornarParteCadenaSplit(seleccion.text,'(',0), 'success', '5000');

                return true;
            }else{
                alertModal('Advertencia', 'La seguridad del acceso al expediente NO se logro modificar', 'warning', '5000');
                return false;
            }
        }).error(function (error) {
            alertModal('Advertencia', 'La seguridad del acceso al expediente NO se logro modificar', 'warning', '5000');
            return false;
        });

    },

    //cada vez que se desmarca a un firmante, vuelvo a rellner el arreglo se los selecteds
    quitarUserSeg: function(data,tipo) {

        var temp = new Array();
        if (tipo == 'USUARIO') {
            temp = onlyUsersSelected
            onlyUsersSelected = new Array();
        } else {
            temp = onlyDependSelected
            onlyDependSelected = new Array();
        }

        var cont = 0;
        for (var i = 0; i < Object.keys(temp).length; i++) {
            if (temp[i].id == data.id) {
            } else {
                if (data.usua_nomb != undefined) {
                    onlyUsersSelected[cont] = temp[i]
                } else {
                    onlyDependSelected[cont] = temp[i]
                }
                cont++;
            }
        }
        Seguridad.updateSeguridad('quitar', tipo, data)

    },

    guardarUserSeg: function(data) {
        var temp = new Array();
        var tipo = 'USUARIO'
        console.log('data', data)
        if (data.usua_nomb != undefined) {
            temp = onlyUsersSelected
        } else {
            temp = onlyDependSelected
            tipo = 'DEPENDENCIA'
        }
        var yaesta = Seguridad.yaEstaUserOrDepend(data, temp)
        console.log('yaesta', yaesta);
        console.log(tipo);
        if (yaesta == false) {
            if (data.usua_nomb != undefined) {
                onlyUsersSelected[Object.keys(onlyUsersSelected).length] = data;
            } else {
                onlyDependSelected[Object.keys(onlyDependSelected).length] = data;
            }
            Seguridad.updateSeguridad('agregar', tipo, data)
        }

    },

    yaEstaUserOrDepend: function(seleccion, temp) {
        var encontro = false;
        for (var i = 0; i < Object.keys(temp).length; i++) {
            if (temp[i].id == seleccion.id) {
                encontro = true;
            }
        }
        return encontro;
    },

    traerUsers: function(quebuscar) {

        //?type=public&radicado="+radicado+"&quebuscar="+quebuscar
        $.ajax({
            url: configsHasValue['WS_APPSERVICE_URL'] + '/seguridadRad/usersToAuthorize',
            headers: {
                "Authorization": 'Bearer '+configsHasValue['WS_APPSERVICE_TOKEN']
            },
            type: 'GET',
            dataType: 'json',

            async: false,
            data: {
                type: 'public',
                quebuscar: quebuscar

            },
            success: function (response) {
                if (quebuscar == "usuarios") {
                    allUsers = response.objects;
                } else {
                    allDepend = response.objects;
                }

            },
            error: function (xhr, ajaxOptions, thrownError) {

                if(xhr.responseJSON.message){
                    Swal.fire("Error", xhr.responseJSON.message, "error");
                }else{
                    Swal.fire("Error", "Ha ocurrido un error al buscar los datos", "error");
                }
            }
        });
    },

    definirSelectValores: function (response) {
        var selectedUserValues = new Array();
        var selectedDepenValues = new Array();

        var contUser = 0;
        var contDepen = 0;

        for (var i = 0; i < Object.keys(response.entidades).length; i++) {

            if (response.entidades[i].tipo_entidad_autorizada == "USUARIO") {

                for (var j = 0; j < Object.keys(response.usuarios).length; j++){
                    if (response.entidades[i].entidad_autorizada == response.usuarios[j].id){
                        selectedUserValues[contUser] = response.usuarios[j];
                        contUser++;
                        break;
                    }
                }

            }else{
                for (var k = 0; k < Object.keys(response.dependencias).length; k++){
                    if (response.entidades[i].entidad_autorizada == response.dependencias[k].id){
                        selectedDepenValues[contDepen] = response.dependencias[k];
                        contDepen++;
                        break;
                    }
                }
            }
        }


        selectedUserValues.forEach(Seguridad.agregarOption);
        console.log(selectedUserValues);
        console.log(selectedDepenValues);
        selectedDepenValues.forEach(Seguridad.agregarOption);
        let idDsSel = selectedDepenValues.map( el => el.id );
        //$("#id_select_users").select2('data', selectedUserValues);
        //$("#id_select_users").trigger('change');
        $("#select_dependencias").val(idDsSel);
        $('#select_dependencias').trigger('change.select2');
        let idsSel = selectedUserValues.map( el => el.id );

        $("#id_select_users").val(idsSel);
        $("#id_select_users").trigger('change.select2');
        //$('#id_select_users').trigger('change');


        //$("#id_select_users").val('173');
        //$('#id_select_users').trigger('change');
    },

    agregarOption: function(item, index) {
        //var $option = $("<option selected></option>").val(item.id).text(item.text);

        var newOption = new Option(item.text, item.id, true, true);
        // Append it to the select
        console.log(newOption);
        if (typeof (item.usua_nomb) != 'undefined'){
            $("#id_select_users option[value='"+item.id+"']").remove();
            $('#id_select_users').append(newOption).trigger('change.select2');
        }else{
            $("#select_dependencias option[value='"+item.id+"']").remove();
            $('#select_dependencias').append(newOption).trigger('change.select2');
        }

    }
}

