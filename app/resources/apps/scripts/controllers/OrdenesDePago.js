var OrdenesDePago = {

    rutaToBuscarUsuariosComen: 'app/borradores/',
    radicadoReasignar: '',

    /**
     * Levanta el modal para reasignar el radicado
     */
    showModalReasignar: function (evento, radicado) {
        evento.preventDefault();
        OrdenesDePago.radicadoReasignar = radicado;
        //levanta el modal de reasignar el radicado

        console.log(radicado);
        $('#comentarioenviar').maxlength({
            alwaysShow: true,
            threshold: 9,
            appendToParent: true,
            warningClass: "label label-success",
            limitReachedClass: "label label-danger",
            preText: 'Mínimo ' + configsHasValue['COMENTARIOS_MINLENGTH'] + ' caracteres. ',
            validate: true
        });

        $("#comentarioenviar").val('');
        $("#comentarioenviar").html('');
        $("#enviara").val('');

        $("#enviara").select2(
            {
                width: "100%",
                dropdownParent: $("#modal_reasignar"),
                allowClear: true,
                data: {},
                ajax: {
                    url: base_url + OrdenesDePago.rutaToBuscarUsuariosComen + "buscarUsuariosComen.php",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            search: params.term,
                            type: 'public',
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: data,

                        };
                    },
                    cache: true
                },
                placeholder: 'Buscar Usuarios',
                escapeMarkup: function (markup) {
                    return markup;
                }, // let our custom formatter work

                language: {
                    inputTooShort: function () {
                        return 'Ingrese un nombre para buscar';
                    }
                }
            });

        $('#div_enviar_marcar_como_revisado').css('display', 'none');
        $('.icheckbox_flat-green').removeClass('checked');
        $('#check_enviar_marcar_como_revisado').prop('checked', false)


        $("#modal_reasignar").modal('show');
        $("#modal_reasignar").css('z-index', '9999');


    },

    /**
     * Cuando se le da click al modal, el botonreasignar
     * @param evento
     */
    clickModalReasignar: function (evento) {
        evento.preventDefault();
        var usuario_id = $("#enviara").val()
        var comentarioenviar = $("#comentarioenviar").val()

        if (usuario_id == null || usuario_id == 'null' || usuario_id == '' || usuario_id == undefined) {
            Swal.fire("Alerta", "Seleccione una persona", "warning");
            return false;
        }

        if (comentarioenviar.length >= configsHasValue['COMENTARIOS_MINLENGTH']) {
            OrdenesDePago.ejecutarReasignar(usuario_id, comentarioenviar);
        } else {
            Swal.fire("Alerta", "Debe ingresar un comentario de al menos " + configsHasValue['COMENTARIOS_MINLENGTH'] + " caracteres", "warning");
        }

    },

    ejecutarReasignar: function (usuario_id, comentarioenviar) {


        var params = {
            userLogguedId: userLogguedId,
            usuario_id: usuario_id,
            comentarioenviar: comentarioenviar,
            radicado: OrdenesDePago.radicadoReasignar
        }
        $('#guardarEnviar').prop('disabled', true);
        OrdenesDePagoService.reasignarRadicado(params).success(function (response) {
            if (response.error == undefined) {
                alertModal('Reasignado', 'Radicado '+OrdenesDePago.radicadoReasignar+' reasignado con éxito', 'success', 7000)
                $('#modal_reasignar').modal('hide');
                OrdenesDePago.refrescarTablaOp();
            } else {
                $('#guardarEnviar').prop('disabled', false);
                Swal.fire("Error", response.message, "error");
            }
        }).error(function (response) {
            $('#guardarEnviar').prop('disabled', false);
            Swal.fire("Error", response.message, "error");
        });

    },

    refrescarTablaOp: function () {

        if (swicheselected == "Contratista" || swicheselected == "Financiera" || swicheselected == "Supervisor") {
            if ($("#datefromop").val() != "" && $("#datetoop").val() != "") {
                tablaprincipalOP();
            }
        } else {
            $('#beforetableopcontainer').removeClass('hide');
            tablaAdmin();
        }
    },

}