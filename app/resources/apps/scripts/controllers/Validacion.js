var Validacion = {


    /**
     * verifica si ya un usuario validó el radicadio, recibe los datos de document_authorized_by
     * @param user_id
     * @param document_authorized_by
     * @returns {boolean}
     */
    checkUserYaValido: function (user_id, document_authorized_by) {

        var yaValido = false;
        if (document_authorized_by != null && Object.keys(document_authorized_by).length > 0) {
            for (var i = 0; i < Object.keys(document_authorized_by).length; i++) {
                if (document_authorized_by[i].id_authorized == user_id) {
                    yaValido = document_authorized_by[i]
                }
            }
        }

        return yaValido;
    },

    /**
     * verifica si este tipo de radicado, esta permitido para validar
     * @param radicado
     * @returns {boolean}
     */

    checkPermiteValidarRadicado: function (radicado) {
        var permitetiporad = false;

        var tiporadperm = JSON.parse(configsHasValue['TIPOS_RADICADO_VALIDAR'])

        for (var i = 0; i < Object.keys(tiporadperm).length; i++) {

            if (tiporadperm[i] == radicado.substr(radicado.length - 1, radicado.length) ) {
                permitetiporad = true;
                break;
            }
        }

        return permitetiporad;

    }
}

