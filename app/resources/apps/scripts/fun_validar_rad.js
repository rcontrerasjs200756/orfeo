var Upload = {
    ancho: 0,
    hasta: 1,
    speed: 200,
    id_move: '',
    init: function (name, id_file) {

        Upload.ancho = 0;
        Upload.hasta = 1;
        Upload.speed = 200;
        Upload.id_move = '';
        this.events();

    },
    refresh: function () {

        Upload.ancho = 0;
        Upload.hasta = 1;
        Upload.speed = 200;
        document.getElementById("myBar").style.width = 1;

    },
    events: function () {
        $("#myBar").css({"background": '#26a69a'})
    },
    move: function () {
        Upload.id_move = setInterval(this.frame, Upload.speed);
    },
    frame: function () {

        var elem = document.getElementById("myBar");
        if (Upload.ancho >= Upload.hasta) {
            Upload.refresh();
            clearInterval(Upload.id_move);
            Upload.id_move = '';
        } else {
            Upload.ancho++;
            elem.style.width = Upload.ancho + '%';
            document.getElementById("label").innerHTML = (Upload.ancho * 1) + '%';
        }
    },
    set_values_bar: function (width, to) {
        Upload.ancho = width
        Upload.hasta = to
    },
    frame: function () {

        var elem = document.getElementById("myBar");
        if (Upload.ancho >= Upload.hasta) {
            Upload.refresh();
            clearInterval(Upload.id_move);
            Upload.id_move = '';
        } else {
            Upload.ancho++;
            elem.style.width = Upload.ancho + '%';
            document.getElementById("label").innerHTML = (Upload.ancho * 1) + '%';
        }
    },
    upload: function () {

        Upload.hasta = 100;
        this.move();
        $("#upload_two").css({"display": "none"});
        $("#open_name").html('')
        $('#providers-list .row').empty();

        $("#upload").css("display", "none");
        $("#myProgress").css("display", "block");
        $("#processing").css("display", "block");

        var retorno = false
        $.ajax({
            url: base_url+"app/radicados/generalradicado.php",
            type: 'POST',
            dataType: 'json',
            async: false,
            data: {
                radicado: radicados_selecteds_v[0]['radicado'],
                function_call: "radExpedIncluidos"
            },
            success: function (response) {
                if (response.expedientes != undefined) {
                    fechaahora = response.fechaahora;
                    Upload.speed = 10;
                    Upload.set_values_bar(60, 100);
                    Upload.refresh();


                } else {
                    Swal.fire("Error", response.error, "error");
                }

            },
            error: function (response) {
                Swal.fire("Alerta", "Ha ocurrido un error al buscar los datos", "error");
            }
        });

        return retorno;

    }


}

/*var ladda={
    laddabtn:'',
    interval:'',
    init: function () {
        ladda.laddabtn='';
        ladda.interval='';
        clearInterval(ladda.interval);
    },
    btnvalidar: function () {
        clearInterval(ladda.interval);
        ladda.laddabtn=Ladda.create(document.querySelector( '.mt-ladda-btn' ));
        ladda.laddabtn.start();
    },
    barraprogress: function (){
        clearInterval(ladda.interval);
        //laddabtnvalidar=Ladda.bind(".mt-ladda-btn", {timeout: 5000});
        ladda.laddabtn=   Ladda.bind(".mt-ladda-btn.mt-progress-demo ", {
            callback: function (t) {
                var n = 0;
                ladda.interval= setInterval(function () {
                        n = Math.min(n + .1 * Math.random(), 1),
                            t.setProgress(n),
                        1 === n && (t.stop(), clearInterval(ladda.interval))
                    }, 200)
            }
        })
    }
};*/

var progressbutton = function () {
}


var languageseelct2 = {
    inputTooShort: function () {
        return 'Ingrese para buscar';
    },
    noResults: function () {
        return "Sin resultados";
    },
    searching: function () {
        return "Buscando...";
    },
    errorLoading: function () {
        return 'El resultado aún no se ha cargado.';
    },
    loadingMore: function () {
        return 'Cargar mas resultados...';
    },
}


function doc_expedincluidos() {
    var retorno = false;
    $.ajax({
        url: base_url+"app/radicados/generalradicado.php",
        type: 'POST',
        dataType: 'json',
        async: false,
        data: {
            radicado: radicados_selecteds_v[0]['radicado'],
            function_call: "radExpedIncluidos"
        },
        success: function (response) {
            if (response.expedientes != undefined) {
                retorno = response.expedientes;
            } else {
                if(response.error!=undefined){
                    Swal.fire("Error",response.error, "error");
                }else{
                    Swal.fire("Error","Ha ocurrido un error al verificar si está en expedientes", "error");
                }

            }

        },
        error: function (response) {
            Swal.fire("Alerta", "Ha ocurrido un error al buscar los datos", "error");
        }
    });

    return retorno;
}

//busca los usuarios en el servidor
function doc_searchusersPosibleAutoriza() {
    $.ajax({
        url: base_url+"app/radicados/generalradicado.php?function_call=usersPosibleAutoriza&type=public&radicado=" + radicados_selecteds_v[0]['radicado'],
        type: 'GET',
        dataType: 'json',
        async: false,
        success: function (response) {
            doc_allUsersSinAutorizar = response;
        },
        error: function (response) {
            Swal.fire("Error", "Ha ocurrido un error al buscar los usuarios", "error");
        }
    });
}

//funcion que colca selecteds los frmantes
function doc_selectedsFirmSelect2() {
    var selectedValues = new Array();
    for (var i = 0; i < Object.keys(firmantesselected).length; i++) {
        selectedValues[i] = firmantesselected[i].ID
    }

    $("#select_usuarios_aprueban").val(selectedValues)
    $('#select_usuarios_aprueban').trigger('change.select2');

}

//busca los usuarios firmantes en la tabla borradores_firmantes
function doc_buscarFirmantes() {
    $.ajax({
        url: base_url+"app/radicados/generalradicado.php?function_call=buscarFirmantes&radicado=" + radicados_selecteds_v[0]['radicado'],
        type: 'GET',
        dataType: 'json',
        async: false,
        success: function (response) {
            firmantesselected = response;
        },
        error: function (response) {
            Swal.fire("Error", "Ha ocurrido un error al buscar los firmantes", "error");
        }
    });
}

//verifica si un firmante esta en el arreglo firmantesselected
function doc_yaEstaAddFirmante(firmante) {
    var encontro = false;

    for (var i = 0; i < Object.keys(firmantesselected).length; i++) {
        if (firmantesselected[i].ID == firmante.id) {
            encontro = true;
        }
    }
    return encontro;
}

//cuando se selecciona un firmante de select2
function doc_guardarfirmantes(firmante) {

    if (firmante.NOMBRE != undefined) {
        alertModal('Nuevo ' + conjugacionValidar[11], firmante.NOMBRE + ' Seleccionado', 'success', 10000);
    }
    var yaesta = doc_yaEstaAddFirmante(firmante)
    if (yaesta == false) {
        firmantesselected[Object.keys(firmantesselected).length] = firmante;
    }
    doc_ajaxRmOrAddFirmantes()
}

//actualiza el estatus de un radicado en document_metadata
function cerrarValidacion() {
    $.ajax({
        url: base_url+"app/radicados/generalradicado.php",
        type: 'POST',
        dataType: 'json',
        data: {
            ANEXO_ID: anexos_selecteds_v[0]['ANEXO_ID'],
            function_call: "cerrarValidacion",
        },
        async: false,
        success: function (response) {
            getUsersValidaron();
        },
        error: function (response) {
            Swal.fire("Error", "Ha ocurrido un error al buscar los firmantes", "error");
        }
    });
}

//actualiza el estatus de un radicado en document_metadata
function reAbrirValidacion() {
    $.ajax({
        url: base_url+"app/radicados/generalradicado.php",
        type: 'POST',
        dataType: 'json',
        data: {
            ANEXO_ID: anexos_selecteds_v[0]['ANEXO_ID'],
            function_call: "reAbrirValidacion",
        },
        async: false,
        success: function (response) {
            getUsersValidaron();
        },
        error: function (response) {
            Swal.fire("Error", "Ha ocurrido un error al buscar los firmantes", "error");
        }
    });
}

//cada vez que se desmarca a un firmante, vuelvo a rellner el arreglo se los selecteds
function doc_quitarfirmante(firmante) {

    var yavalido = existeUsersValidaron(firmante.id);
    if (yavalido == true) {
        doc_selectedsFirmSelect2();
        alertModal(conjugacionValidar[11], firmante.NOMBRE + ', no se puede eliminar, ya ' + conjugacionValidar[3].toLowerCase() + ' el documento', 'warning', 10000);
        return false;
    }


    if (Object.keys(usersValidaron).length > 0 &&
        Object.keys(usersValidaron).length == (Object.keys(firmantesselected).length) - 1) {
        var retorno = true;
        //@formatter:off
        Swal.fire({
            title: "Alerta",
            text: 'Está seguro de borrar el último '+conjugacionValidar[11]+'?  (La '+conjugacionValidar[10]+' se CERRARÁ)',
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "SI",
            cancelButtonText: "NO",
            closeOnConfirm: true,
            closeOnCancel: true
        }).then((result) => {
            if (result.value) {
                cerrarValidacion();
                desmarcarFirmante(firmante);
                showOptsEstaEnFirmantes();
                showpersonporvalidar();
                validarDeshabAuthAp();
                doc_selectedsFirmSelect2();
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                retorno = false
            }
        });
        //@formatter:on
    } else {


        Swal.fire({
            title: "Alerta",
            text: '¿Está seguro de quitar el '+conjugacionValidar[11]+' '+firmante.NOMBRE+'  ?',
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "SI",
            cancelButtonText: "NO",
            closeOnConfirm: true,
            closeOnCancel: true,
            timer: 10000
        }).then((result) => {
            if (result.value) {
                //cerrarValidacion();
                desmarcarFirmante(firmante);
                showOptsEstaEnFirmantes();
                showpersonporvalidar();
                validarDeshabAuthAp();
                doc_selectedsFirmSelect2();
            } else if(result.dismiss === Swal.DismissReason.cancel) {
                retorno = false
            }
        });

    }

}

function desmarcarFirmante(firmante) {
    var tempfirmantes = firmantesselected
    firmantesselected = new Array();
    var cont = 0;
    var firmtipovalidador = false;
    var firmPrincipal = false;
    for (var i = 0; i < tempfirmantes.length; i++) {

        if (tempfirmantes[i].ID == firmante.id) {
        } else {
            firmantesselected[cont] = tempfirmantes[i]
            cont++;
        }
        if (tempfirmantes[i].TIPO_VALIDADOR == 'Remitente' && tempfirmantes[i].ID != firmante.id) {
            firmtipovalidador = true;
        }
        if (firmante.FIRMANTE_PRINCIPAL == 1) {
            firmPrincipal = true;
        }
    }

    if (firmante.NOMBRE != undefined) {
        alertModal('' + conjugacionValidar[11] + ' eliminado', firmante.NOMBRE, 'success', 10000);
    }

    var accion = 'REM';
    if (firmtipovalidador == false || firmPrincipal == true) {
        accion = 'REMMOD';
    }
    doc_ajaxRmOrAddFirmantes(accion);

}

//hace el ajax de quitar o agregar nuevos firmantes
function doc_ajaxRmOrAddFirmantes(accion = 'ADD') {
    $.ajax({
        url: base_url+"app/radicados/generalradicado.php",
        type: 'POST',
        dataType: 'json',
        data: {
            ANEXO_ID: anexos_selecteds_v[0]['ANEXO_ID'],
            function_call: "ajaxRmOrAddFirmantes",
            firmantes: firmantesselected
        },
        async: false,
        success: function (response) {

            getIdFirmante(accion);
        },
        error: function (response) {
            Swal.fire("Error", "Ha ocurrido un error al agregar o quitar firmantes", "error");
        }
    });
}

function getIdFirmante(accion) {
    $.ajax({
        url: base_url+"app/radicados/generalradicado.php?function_call=getFirmPrinc&type=public&ANEXO_ID=" + anexos_selecteds_v[0]['ANEXO_ID'],
        type: 'GET',
        dataType: 'json',
        async: false,
        success: function (response) {

            if (response[0] != undefined) {
                for (var i = 0; i < Object.keys(firmantesselected).length; i++) {
                    firmantesselected[i].FIRMANTE_PRINCIPAL = 0;
                    if (firmantesselected[i].ID_USUARIO_FIRMANTE == response[0].ID_USUARIO_FIRMANTE) {
                        firmantesselected[i].FIRMANTE_PRINCIPAL = response[0].FIRMANTE_PRINCIPAL;
                    }
                }
                if (accion == 'REMMOD') {
                    modalSeleccCargos('VALIDACION', 'modCargosFirmValid', 'tbodyCargFirmValid', 'aEditablesValid', response[0].ID_BORRADOR);
                }
            }

        },
        error: function (response) {
            console.log(JSON.stringify(response));
            Swal.fire("Error", "Ha ocurrido un error al obtener firmantes remitentes", "error");
        }
    });
}

//define los usuarios que van a poder aprobar/validar el doc
function doc_usersPosibleAutoriza() {

    doc_searchusersPosibleAutoriza();


    var txt = "'VALIDACION'";
    var modal = "'modCargosFirmValid'";
    var tbodycargosfirmantes = "'tbodyCargFirmValid'";
    var claseeditables = "'aEditablesValid'";
    var html = '<div class="row"  id="cabecera_modal_valida" ></div>';
    html += '<div class="row" id="row_autorizados"> ' +
        '<div class="col-md-12"  > ' +
        '<div class="form-group">' +
        '<label class=" col-md-2 ">' + conjugacionValidar[6] + ':</label>' +
        '<div class="col-md-10" > ' +
        '<div class="input-icon right">' +
        ' <select class="js-data-example-ajax" name="select_usuarios_aprueban[]" multiple ' +
        ' id="select_usuarios_aprueban" ngtr="hola" style="width: 100%">' +
        '</select><br> ' +
        '</div> ' +
        '</div> ' +
        '</div> ' +
        '</div>' +
        '<div class="col-md-4"  ></div><div class="col-md-4"  >' +
        '<a href="javascript:;" style="align-content: center"' +
        ' onclick="modalSeleccCargos(' + txt + ',' + modal + ',' + tbodycargosfirmantes + ',' + claseeditables + ',' + borrador_id + ')" ' +
        'class="btn btn-xs default">Seleccionar firmantes y cargos' +
        '<i class="fa fa-user"></i>' +
        '</a>' +
        '</div><div class="col-md-4"  ></div>' +
        '</div>' +
        '<div class="row" id="row_tabla_doc"></div>' +
        '<div class="row" id="row_btn_validar" style="text-align:center"></div>' +
        '<div class="row" id="row_doc_validado_bien" style="text-align:center"></div>' +
        '<div class="row" id="row_nro_user_sin_vali" style="text-align:center"></div>';

    $("#div_apend_aprob_doc").html(html)

    //select de usuarios autorizados para aprobar
    $("#select_usuarios_aprueban").select2(
        {
            width: "100%",
            multiple: true,
            dropdownParent: $("#modal_aprobar_documento"),
            allowClear: true,
            data: doc_allUsersSinAutorizar,
            ajax: {
                url: base_url+"app/radicados/generalradicado.php?function_call=usersPosibleAutoriza&radicado=" + radicados_selecteds_v[0]['radicado'],
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        search: params.term,
                        type: 'public'
                    };
                },
                processResults: function (data, params) {
                    return {
                        results: data,
                    };
                },
                cache: true
            },
            placeholder: 'Clic aquí para agregar usuarios',
            escapeMarkup: function (markup) {
                return markup;
            }, // let our custom formatter work
            language: languageseelct2
        }).on('select2:selecting', function (e) {
        //cada vez que selecciono una opcion
        doc_guardarfirmantes(e.params.args.data)
        showOptsEstaEnFirmantes();
        showpersonporvalidar();
        reAbrirValidacion();


    }).on("select2:unselecting", function (e) {
        //cada vez que desmarco una opcion

        doc_quitarfirmante(e.params.args.data)
        showOptsEstaEnFirmantes();
        showpersonporvalidar();
        validarDeshabAuthAp();
        doc_selectedsFirmSelect2();

    }).on("select2:unselect", function (e) {
        //despues de que ya haya sido desmaracado una opcion
        doc_selectedsFirmSelect2();

    }).trigger('change');

    doc_buscarFirmantes();
    doc_selectedsFirmSelect2();

}

//busca los usuarios que validaron el doc
function getUsersValidaron() {

    var params = {
        radicado: radicados_selecteds_v[0]['radicado'],
        function_call: "getUsersValidaron"
    }

    ValidacionService.getDocumentMetadataByRad(params).success(function (response) {
        if (response.error == undefined) {
            document_metadata = response.objects; //info de la tabla document_metadata
            usersValidaron = new Array();
            lastUserValidarion = new Array();
            if (response.count > 0) {
                usersValidaron = document_metadata.document_authorized_by; //usuarios que ya validaron el doc: document_authorized_by
                lastUserValidarion =getLastValidation(document_metadata.document_authorized_by);
                
            }
        }
    }).error(function (error) {
        Swal.fire("Error", "Ha ocurrido un error al buscar los firmantes", "error");
    });
}

/**
 * con esto valido que me retorne la iultima validacion
 * @param documentauthorizedby
 * @returns {*}
 */
function getLastValidation(documentauthorizedby){
    var arreglo=documentauthorizedby[0];
    for (var i = 0; i < Object.keys(documentauthorizedby).length; i++) {
        if ( documentauthorizedby[i].timestamp_f > arreglo.timestamp_f ){
            arreglo = usersValidaron[i]
        }
    }

    return arreglo;

}

//valida si existe un usuario en el arreglo de usuarios que ya validaron el doc
function existeUsersValidaron(usu) {
    var encontro = false;
    for (var i = 0; i < Object.keys(usersValidaron).length; i++) {
        if (usersValidaron[i].user.id == usu) {
            encontro = true;
        }
    }
    return encontro;

}

function armartabladocprincipal() {

    $("#row_tabla_doc").html('');
    var html = '';
    html += ' <div class="col-md-12"  >' +
        ' <table class="table-bordered" width="80%">' +
        '<tr>' +
        '<td><label >' + conjugacionValidar[1] + ' por:</label></td>' +
        '<td id=td_validado_por></td>' +
        '</tr> ' +
        '<tr>' +
        '<td><label >Dependencia:</label></td>' +
        '<td><label  id="lbl_tbl_dependencia"></label></td>' +
        '</tr> ' +
        '<tr>' +
        '<td><label >Correo Electrónico:</label></td>' +
        '<td><label  id="lbl_tbl_correo"></label></td>' +
        '</tr> ' +
        '<tr>' +
        '<td><label >Fecha ' + conjugacionValidar[5].toLowerCase() + ':</label></td>' +
        '<td><label  id="lbl_tbl_fecha_aprob"></label></td>' +
        '</tr> ' +
        '<tr>' +
        '<td><label >Dirección IP:</label></td>' +
        '<td><label  id="lbl_tbl_ip"></label></td>' +
        '</tr> ' +
        '<tr>' +
        '<td><label >Ubicación:</label></td>' +
        '<td><label  id="lbl_tbl_ubic"></label></td>' +
        '</tr> ' +
        '</table> ' +
        '</div>';

    $("#row_tabla_doc").html(html)

    definirdoc_validado_por();

}

//define el select2 de validaro Por
function definirdoc_validado_por() {
    $("#td_validado_por").html('')

    if (Object.keys(usersValidaron).length > 0) {

        var html = '';
        html += ' <select name="doc_validado_por" ' +
            ' id="doc_validado_por" style="width: 100%" ' +
            'onchange="changeselecvalidadopor(this.options[this.selectedIndex].value);row_doc_validado_bien(this.options[this.selectedIndex].value);" ' +
            ' placeholder="Buscar Usuarios">';

        if (Object.keys(usersValidaron).length > 0) {
            for (var i = 0; i < Object.keys(usersValidaron).length; i++) {
                html += '<option value="' + usersValidaron[i].user.id + '" >' + unionUserNameWithDepe(usersValidaron[i].user) + '</option>'
            }
        }
        html += '</select> ';
        $("#td_validado_por").html(html)

    } else {
        $("#td_validado_por").html('<p style="color: #00a9cc">El documento no ha sido ' + conjugacionValidar[1] + ' por ninguna persona</p>')
    }
    showOptsEstaEnFirmantes()
}

function showSpinner() {
    ///your are missing this part
    // Bind normal buttons
    Ladda.bind('div:not(.progress-demo) button', {timeout: 2000});
    // Bind progress buttons and simulate loading progress
    Ladda.bind('.progress-demo button', {
        callback: function (instance) {
            var progress = 0;
            var interval = setInterval(function () {
                progress = Math.min(progress + Math.random() * 0.1, 1);
                instance.setProgress(progress);

                if (progress === 1) {
                    instance.stop();
                    clearInterval(interval);
                }
            }, 200);
        }


    })
}

//muestra cosas, solo si el usuario esta en firmantes, y dependiendo de si ya validó o no, muestras lo que viene
function showOptsEstaEnFirmantes() {

    var objeto = {
        id: doc_codusuario
    }
    //valido si el usuario actual esta entre los usuarios firmantes
    var estoyenfirmantes = doc_yaEstaAddFirmante(objeto);
    if (estoyenfirmantes == true) {
        var estoyenvalidados = existeUsersValidaron(doc_codusuario);
        var html = '';

        if (estoyenvalidados == false) {
            //si no he validado el doc

            html = '' +
                '<div class="col-md-12" style="align-content: center" > ';

            /**
             * Esta variable indica si muestra el boton para validar en el modal
             * PD: en OP (ordenes de pago), no se habilita
             */

            if(showBtnValidarModal==true &&
                ((METODO_VALIDACION == 'Sin código o clave')
                    ||
                    (
                        METODO_VALIDACION == 'Codigo eMail' &&
                        codigo_email_enviado == '' &&
                        (expiracion_aprobar_doc == '' || (expiracion_aprobar_doc != '' && fechaahora > expiracion_aprobar_doc))

                    )
                )
            ) {

                console.log('mostrar email enviado 1');
                html+='<button type="button" title="' + conjugacionValidar[0] + ' documento" id="btn_validar_modal"' +
                    ' style="border-top-right-radius:0px !important; border-top-left-radius:70px !important; ' +
                    'border-bottom-left-radius:70px !important;" ' +
                    'onclick="doc_btnvalidardoc()" class="btn btn-lg blue demo-loading-btn btn-primary demo-loading-btn mt-ladda-btn ladda-button mt-progress-demo firstValidar" data-style="expand-left"> ' +
                    '<span class="ladda-label">' + conjugacionValidar[0] + '</span> <span class="ladda-spinner"></span></button></div>';
            }

            html+= '<div class="col-md-12" style="align-content: center" id="show_codigo_autor" ></div> ';


            if (
                (METODO_VALIDACION == 'Sin código o clave')
                ||
                (
                    METODO_VALIDACION == 'Codigo eMail' &&
                    codigo_email_enviado == '' &&
                    (expiracion_aprobar_doc == '' || (expiracion_aprobar_doc != '' && fechaahora > expiracion_aprobar_doc))
                )
            ) {
                console.log('mostrar email enviado 2');
                $('#row_btn_validar').html('');
                $('#row_btn_validar').html(html)
            } else {
                console.log('mostrar email enviado n4ev6');
                $('#row_btn_validar').html('');
                $('#row_btn_validar').html(html);

                if (METODO_VALIDACION == 'Clave única') {
                    showInputPassword()
                } else {


                    //alertModal('Código ' + codigo_aprobar_doc + ' Vigente', 'Hasta las ' + horaminuto_expira + ' del ' + solofecha_expira + ' o hasta cerrar sesión', 'success', '15000');
                    //showInputCodigo(codigo_aprobar_doc);
                    // no se muestra codigo ya que el usuario debe ir al correo e introducirlo , no debe ser automatico que aparezca
                    if(  METODO_VALIDACION == 'Codigo eMail' &&
                        codigo_email_enviado == 'SI' &&
                        ( (expiracion_aprobar_doc != '' && fechaahora < expiracion_aprobar_doc))
                    ){
                        //si entra aqui, es porque esta habilitado el enviar el codigo email, y se tiene un codigo y fecha de expiracion
                        //en sesion, y la fecha de expiración es válida.
                        alertModal('Código Vigente ', 'Hasta las ' + horaminuto_expira + ' del ' + solofecha_expira + ' o hasta cerrar sesión', 'success', '15000');
                        showInputCodigo();
                    }else if (  METODO_VALIDACION == 'Codigo eMail' &&
                        codigo_email_enviado == 'SI' &&
                        ( (expiracion_aprobar_doc != '' && fechaahora > expiracion_aprobar_doc))
                    ){
                        html = '' + '<div class="col-md-12" style="align-content: center" > ';
                        html+='<button type="button" title="' + conjugacionValidar[0] + ' documento" id="btn_validar_modal"' +
                    ' style="border-top-right-radius:0px !important; border-top-left-radius:70px !important; ' +
                    'border-bottom-left-radius:70px !important;" ' +
                    'onclick="doc_btnvalidardoc()" class="btn btn-lg blue demo-loading-btn btn-primary demo-loading-btn mt-ladda-btn ladda-button mt-progress-demo firstValidar" data-style="expand-left"> ' +
                    '<span class="ladda-label">' + conjugacionValidar[0] + '</span> <span class="ladda-spinner"></span></button></div>';
                        html+= '<div class="col-md-12" style="align-content: center" id="show_codigo_autor" ></div> ';
                        $('#row_btn_validar').html('');
                        $('#row_btn_validar').html(html);
                    }

                    /* var UIButtons=function(){
                         var t=function(){
                             $(".demo-loading-btn").click(function(){
                                 var t=$(this);t.button("loading"),
                                     setTimeout(function(){t.button("reset")},3e3)
                             })
                         };
                         return{init:function(){t()}}
                     }();
                     jQuery(document).ready(function(){UIButtons.init()}); */
                }


            }

//mando a mostrar los botones d ever original o descargar el archivo y el hash
            if (usersValidaron[0] != undefined) {
                changeselecvalidadopor(usersValidaron[0].user.id)
            }


        } else {
            $("#doc_validado_por").val(doc_codusuario)
            $('#doc_validado_por').trigger('change.select2');
            //si ya valide
            html = '<div class="col-md-12" style="align-content: center" id="show_codigo_autor" ></div> ';
            changeselecvalidadopor(doc_codusuario)
            $('#row_btn_validar').html('');
            $('#row_btn_validar').html(html)
        }

    } else {
        //quito todos los botones
        $('#row_btn_validar').html('');
        if (usersValidaron[0] != undefined) {
            changeselecvalidadopor(usersValidaron[0].user.id)
        }
    }
    row_doc_validado_bien();
}

//cada vez que cambia el select de validado Por:
function changeselecvalidadopor(id) {
    //muestro los datos en la tabla
    $('#lbl_tbl_dependencia').html('');
    $('#lbl_tbl_correo').html('');
    $('#lbl_tbl_fecha_aprob').html('');
    $('#lbl_tbl_ip').html('');
    $('#lbl_tbl_ubic').html('');
    for (var i = 0; i < Object.keys(usersValidaron).length; i++) {
        if (usersValidaron[i].user.id == id) {
            $('#lbl_tbl_dependencia').html(usersValidaron[i].department);
            $('#lbl_tbl_correo').html(usersValidaron[i].usermail);
            $('#lbl_tbl_fecha_aprob').html(usersValidaron[i].timestamp_f);
            $('#lbl_tbl_ip').html(usersValidaron[i].ipaddress);
            $('#lbl_tbl_ubic').html(usersValidaron[i].city);

            if ($('#trsvalidados') == undefined) {
                var html = '<tr class="trsvalidados" >' +
                    '<td><label >Hash:</label></td>' +
                    '<td><label id="lbl_tbl_hash"></label></td>' +
                    '</tr> ' +
                    '<tr  class="trsvalidados">' +
                    '<td><label >Tipo de archivo:</label></td>' +
                    '<td><label id="lbl_tbl_tipoarchivo"></label></td>' +
                    '</tr> ' +
                    '<tr class="trsvalidados">' +
                    '<td><label >Propiedades:</label></td>' +
                    '<td><label id="lbl_tbl_propiedades"></label></td>' +
                    '</tr> ';
                $('#lbl_tbl_ubic').after(html)
            }


            $('#lbl_tbl_hash').html(document_metadata.hash_doc_original);
            $('#lbl_tbl_tipoarchivo').html(document_metadata.filetype_ext);
            $('#lbl_tbl_propiedades').html(document_metadata.mimetype + ' | Tamaño: ' + document_metadata.size + ' KB | ' + document_metadata.pages + ' páginas.');

        }
    }

}

function confirmEnviaEemail() {
    setTimeout(function () {
        //@formatter:off
        Swal.fire({
            title: "Alerta",
            text: 'Para '+conjugacionValidar[0].toLowerCase()+', se enviará un código de autorización a su correo electrónico',
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Enviar",
            cancelButtonText: "Cancelar",
            closeOnConfirm: true,
            closeOnCancel: true
        }).then((result) => {
            if (result.value) {
                enviarcorreodoc();
            } else if (
                result.dismiss === Swal.DismissReason.cancel
            ) {
                $('#btn_validar_modal').prop('disabled', false)
            }
        });
        //@formatter:on
    }, 500)
}


//cuando se le da click al boton de Validar en el modal de validar Doc
function doc_btnvalidardoc() {

    $('#btn_validar_modal').prop('disabled', true);

    if (datos_primera_validacion.fueescaneado > 0) {
        Swal.fire("No permitido", "El documento principal ya fué entregado para escanear", "error");
        $('#btn_validar_modal').prop('disabled', false);
        return false;
    }

    if (Object.keys(usersValidaron).length >= (Object.keys(firmantesselected).length - 1)) {
        //@formatter:off
        Swal.fire({
            title: "Advertencia",
            text: 'Esta es la última '+conjugacionValidar[5]+' y se cierra el documento. Desea continuar?',
            type: "warning",
            showCancelButton: true,
            confirmButtonText: conjugacionValidar[0],
            cancelButtonText: " Agregar usuarios "+conjugacionValidar[6],
            closeOnConfirm: true,
            closeOnCancel: true
        }).then((result) => {
            if (result.value) {
                if (METODO_VALIDACION == 'Codigo eMail') {
                    confirmEnviaEemail();
                } else if(METODO_VALIDACION=='Clave única') {
                    showInputPassword();
                }else{
                    // si entra aqui, es porque METODO_VALIDACION=NO por lo que
                    // no se envia el codigo de verificacion al email
                    validarcodigo_email();
                }

            } else if (
                result.dismiss === Swal.DismissReason.cancel
            ) {

                $('#btn_validar_modal').prop('disabled', false)
            }
            $('#btn_validar_modal').prop('disabled', false)
        });
        //@formatter:on
    } else {
        if (METODO_VALIDACION == 'Codigo eMail') {
            confirmEnviaEemail();
        } else if (METODO_VALIDACION == 'Clave única') {
            showInputPassword();

        } else {
            // si entra aqui, es porque METODO_VALIDACION=NO por lo que
            // no se envia el codigo de verificacion al email
            validarcodigo_email();
        }
    }
}

//manda a mostrar el input de texto donde se ingresa el codigo que fue enviado al email
//valueinput por defecto lo colo en vacio, ya que puedo mandarlo a setear desde aqui
function showInputCodigo(valueinput='') {


    $('#btn_validar_modal.firstValidar').hide();


    var html = '<br><div class="col-md-4" >' +
        '<label>Ingrese Código de Autorización</label></div> ' +
        '<div class="col-md-3" ><input type="text" value="' + valueinput + '" ' +
        'id="input_cod_autoriz" class="form-control" ></div> ' +
        '<div class="col-md-2" >' +
        '<button type="button" title="Enviar" id="btn_enviar_codigo_email"' +
        ' style="border-top-right-radius:0px !important; border-top-left-radius:20px !important; float: left; ' +
        'border-bottom-left-radius:20px !important;" ' +
        'onclick="validarcodigo_email()" class="btn btn-lg blue"> ' +
        ''+conjugacionValidar[0]+' <i class="fa fa-edit"></i> </button>';

    if (
        (METODO_VALIDACION == 'Sin código o clave')
        ||
        (
            METODO_VALIDACION == 'Codigo eMail' &&
            codigo_email_enviado == 'SI' &&
            (expiracion_aprobar_doc == '' || (expiracion_aprobar_doc != '' && fechaahora > expiracion_aprobar_doc))
        )
    ) {
        console.log('mostrar email enviado 3');
    } else {

        if (METODO_VALIDACION == 'Clave única') {

        } else {

            html += '<br><button type="button" title="' + conjugacionValidar[0] + '" id="btn_validar_modal"' +
                ' style="border-top-right-radius:0px !important; border-top-left-radius:20px !important;float: left; ' +
                'border-bottom-left-radius:20px !important;" ' +
                'onclick="doc_btnvalidardoc()" class="btn btn-lg gray" > ' +
                'Enviar nuevo código<i class="fa fa-envelope"></i></button></div>';
        }
    }

    html+='</div>';

    $('#show_codigo_autor').html('');
    $('#show_codigo_autor').html(html)

}

function showInputPassword(valueinput='') {

    $('#btn_validar_modal').remove();
    var html = '<br><div class="col-md-4" >' +
        '<label>Ingrese la Contraseña para ' + conjugacionValidar[0] + '</label></div> ' +
        '<div class="col-md-3" ><input type="password" autocomplete="false" value="' + valueinput + '" ' +
        'id="input_passw_valida" class="form-control" ></div> ' +
        '<div class="col-md-2" ><button type="button" title="Enviar" id="btn_passw_valida"' +
        ' style="border-top-right-radius:0px !important; border-top-left-radius:20px !important; ' +
        'border-bottom-left-radius:20px !important;" ' +
        'onclick="validarpassw(event)" class="btn btn-lg blue"> ' +
        '' + conjugacionValidar[0] + ' <i class="fa fa-edit"></i> </button></div>';

    $('#show_codigo_autor').html('');
    $('#show_codigo_autor').html(html)

}

//confirma que quiere enviar el codigo a su correo
function enviarcorreodoc() {
    var numeros = [];
    numeros[0] = [];
    numeros[0].id = 12
    $.ajax({
        url: base_url+"app/radicados/generalradicado.php",
        type: 'POST',
        dataType: 'json',
        async: false,
        data: {
            function_call: "enviarcodigoemail"
        },
        success: function (response) {
            if (response.error == undefined && response.send == 'SI') {
                expiracion_aprobar_doc = response.expiracion_aprobar_doc;
                codigo_email_enviado =  response.send;// indicador de si el codigo email fue enviado y almacenado en bd garantia de mostrar opciones de validacion
                //codigo_aprobar_doc=response.codigo_aprobar_doc
                showInputCodigo();
                alertModal('Código generado', 'Se ha enviado el código al correo ' + usua_email, 'success', '15000');
                alertModal('Por favor espere', 'El correo puede tardar en llegar', 'success', '15000');
                Upload.upload();
            } else {
                Swal.fire("Error", response.error, "error");
            }
            $('#btn_validar_modal').prop('disabled', false)
        },
        error: function (response) {
            Swal.fire("Error", "Ha ocurrido un error por favor contactar con soporte o cerrar e intentar nuevamente",
                "error");
            $('#btn_validar_modal').prop('disabled', false)
        }
    });
}

//confirma que quiere enviar el codigo a su correo
function limpiarSessionCorreo() {

    $.ajax({
        url: base_url+"app/radicados/generalradicado.php",
        type: 'POST',
        dataType: 'json',
        async: false,
        data: {
            function_call: "cleanSessionCorreo"
        },
        success: function (response) {
            if (response.error == undefined) {
                console.log('session limpia');
            } else {
                Swal.fire("Error", response.error, "error");
            }

        },
        error: function (response) {
            Swal.fire("Error", "Ha ocurrido un error por favor contactar con soporte o cerrar e intentar nuevamente",
                "error");

        }
    });
}


// valida si deshabilita el select de usuarios a aprobar
function validarDeshabAuthAp() {
    var $fechaLimite;
    $fechaLimite = lastUserValidarion.timestamp_f;
    $.each(radicados_selecteds_v[0]['hist_eventos'], function (ind, elem) {
        if (elem.sgd_ttr_codigo == 28){
            $fechaLimite = elem.hist_fech;
            return false;
        }
    });
    if (Object.keys(usersValidaron).length > 0
        &&
        (Object.keys(usersValidaron).length >= Object.keys(firmantesselected).length || document_metadata.status_document == 'CERRADO')
    ) {

        /**
         * Entra aqui, cuando document_metadata, esta en estatus CERRADO,
         * entonces coloco por defecto que siempre se deshabilita el select de firmantes.
         * Sin embargo:
         * - Solo permitir agregar validadores, si el radicado tiene menos de CIERTA CANTIDAD DE DIAS (varia con la configuracion)de haber sido radicado.
         -Si el radicado se encuentra en el Archivo Virtual (Dependencia 999) entonces No
         permitir agregar validadores.
         */
        doc_selectedsFirmSelect2();

        $('#select_usuarios_aprueban').prop('disabled', true);


        /**
         * si configsHasValue['DIAS_REABRIR_VALIDACION']!='0' entonces no permitir agregar validadores
         * */


        if(configsHasValue['DIAS_REABRIR_VALIDACION']!='0' && configsHasValue['DIAS_REABRIR_VALIDACION']!='') {

            if (radicados_selecteds_v[0].hoymenos15 < formatDate($fechaLimite)) {
                $('#select_usuarios_aprueban').prop('disabled', false);
            } else {
                alertModal(conjugacionValidar[5], 'El radicado se encuentra CERRADO y por la fecha no puede agregar más ' + conjugacionValidar[6], 'info', 10000);

            }
        }


        if (radiPrincipal.radi_depe_actu == '999') {
            //alertModal(conjugacionValidar[5],'El radicado se encuentra en Archivo Virtual(999)', 'warning', 10000);

            doc_selectedsFirmSelect2();
            $('#select_usuarios_aprueban').prop('disabled', true);
        }

    }

    if(disableSelectValidadores==true){
        $('#select_usuarios_aprueban').prop('disabled', true);
    }
}



//cuando el metodo de validacion sea clave unica, entra aqui para  validar la contraseña
function validarpassw(e) {
    e.preventDefault();
    $('#btn_passw_valida').prop('disabled', true);

    if ($('#input_passw_valida').val() == '') {
        Swal.fire("Alerta", "Debe ingresar la contraseña", "warning");
        $('#btn_passw_valida').prop('disabled', false);
        return false;
    }

    if (tieneClaveValidacion == 'NO') {
        Swal.fire("Alerta", "Debe crear una contraseña para " + conjugacionValidar[10] + ", será redireccionado", "warning");
        $('#btn_passw_valida').prop('disabled', false);
        setTimeout(function () {
            window.location.href = base_url + "contraxx.php?ventanaselected=passw_validacion&" + session_name + "=" + session_id

        }, 4000)
        return false;
    }

    if (fechaahora > FECHA_PASW_DOC_MASEISMESES) {
        Swal.fire("Alerta", "Debe cambiar la contraseña", "warning");
        $('#btn_passw_valida').prop('disabled', false);
        return false;
    }

    validarcodigo_email();

}

//valida si el codigo ingresado es el que se le envio al correo
function validarcodigo_email() {

    $('#btn_enviar_codigo_email').prop('disabled', true);
    var codigo = '';
    if (METODO_VALIDACION == 'Codigo eMail') {
        codigo = $('#input_cod_autoriz').val();
        if ($('#input_cod_autoriz').val() == '') {
            $('#btn_passw_valida').prop('disabled', true);
            Swal.fire("Alerta", "Debe ingresar el código de verificación", "error");
            $('#btn_enviar_codigo_email').prop('disabled', false);
            return false;
        }
    }
    if (METODO_VALIDACION == 'Clave única') {
        codigo = $('#input_passw_valida').val();
    }


    Upload.hasta = 100;
    Upload.move();
    $("#upload_two").css({"display": "none"});
    $("#open_name").html('')
    $('#providers-list .row').empty();

    $("#upload").css("display", "none");
    $("#myProgress").css("display", "block");
    $("#processing").css("display", "block");

    setTimeout(function () {

        var appVersion = navigator.appVersion;


        if (datos_primera_validacion.pathinfoAnexo.extension == 'pdf' ||
            datos_primera_validacion.pathinfoAnexo.extension == 'PDF') {

            $.ajax({
                url: configsHasValue['WS_APPSERVICE_URL'] + '/action_validate',
                type: 'POST',
                dataType: 'json',
                async: false,
                headers: {
                    "Authorization": 'Bearer ' + configsHasValue['WS_APPSERVICE_TOKEN']
                },
                data: {
                    navigator_appversion: appVersion,
                    codigo: codigo,
                    radicado: radicados_selecteds_v[0]['radicado'],
                    ANEXO_ID: anexos_selecteds_v[0]['ANEXO_ID'],
                    radi_padre: radi_padre,
                    platform: navigator.platform,
                    expiracion_aprobar_doc: expiracion_aprobar_doc,
                    codigo_aprobar_doc: codigo_aprobar_doc,
                    usuario_id: doc_codusuario,
                    digitosDependencia: digitosDependencia,
                    usua_login: usua_login,
                    usua_nomb: usua_nomb,
                    usua_email: usua_email,
                    dependencia: dependencia,
                    depe_nomb: depe_nomb,
                    browser: getBrowser(),
                    entidad_largo: entidad_largo,
                    nit_entidad: nit_entidad,
                    httpWebOficial: httpWebOficial,
                    url_app: url_app

                },
                success: function (response) {
                    if (response.error == undefined) {

                        if (saveHistDespValidar(response.objects)) {
                            //limpiarSessionCorreo();
                            getUsersValidaron(); 

                            alertModal('Bien hecho !', 'Documento ' + conjugacionValidar[1] + '', 'success', '10000');
                            row_doc_validado_bien(doc_codusuario);

                            showpersonporvalidar();
                            armartabladocprincipal();
                            mostrarhash();
                            validarDeshabAuthAp();
                            console.log('tracking error log 1');
                        }

                    } else {
                        Swal.fire("Advertencia", response.message, "warning");
                    }

                    $('#btn_validar_modal').prop('disabled', false);
                    $('#btn_enviar_codigo_email').prop('disabled', false);
                    $('#btn_passw_valida').prop('disabled', false);
                },
                error: function (xhr, ajaxOptions, thrownError) {

                    if (xhr.responseJSON.message != undefined) {

                        Swal.fire("Error", xhr.responseJSON.message, "error");
                    } else {
                        Swal.fire("Error", "Ha ocurrido un error al " + conjugacionValidar[0] + "," +
                            " cierre el aplicativo e intente nuevamente", "error");
                    }
                    $('#btn_validar_modal').prop('disabled', false);
                    $('#btn_enviar_codigo_email').prop('disabled', false);
                    $('#btn_passw_valida').prop('disabled', false);
                }
            });

        } else {
            let sustituciones = {};
            let date = new Date();
            let ultimaV = false;
            if (Object.keys(usersValidaron).length >= (Object.keys(firmantesselected).length - 1)) {
                ultimaV = true;
            }
            let day = date.getDate()
            let month = date.getMonth() + 1
            let year = date.getFullYear()
            let valor = '';
            let mesEscrito = '';
            switch (month) {
                    case 1:
                      mesEscrito = "Enero";
                      break;
                    case 2:
                      mesEscrito = "Febrero";
                      break;
                    case 2:
                       mesEscrito = "Marzo";
                      break;
                    case 4:
                      mesEscrito = "Abril";
                      break;
                    case 5:
                      mesEscrito = "Mayo";
                      break;
                    case 6:
                      mesEscrito = "Junio";
                      break;
                    case 7:
                      mesEscrito = "Julio";
                      break;
                    case 8:
                      mesEscrito = "Agosto";
                      break;
                    case 9:
                      mesEscrito = "Septiembre";
                      break;
                    case 10:
                      mesEscrito = "Octubre";
                      break;
                    case 11:
                      mesEscrito = "Noviembre";
                      break;
                    case 12:
                      mesEscrito = "Diciembre"; 
                  }
            if(month < 10){
                valor =  day+'-0'+month+'-'+year;
            }else{
                valor =  day+'-'+month+'-'+year;
            }
            sustituciones['DATE_VAL'] = valor + '\n'; 
            sustituciones['DD_VAL'] = day.toString() ;
            sustituciones['MM_VAL'] = mesEscrito;
            sustituciones['YYYY_VAL'] = year + '\n';
            console.log(sustituciones);
            let data = {
                'anexo_id': anexos_selecteds_v[0]['ANEXO_ID'],
                'sustituciones' : JSON.stringify(sustituciones),
                'ultima' : ultimaV
            }
            console.log(data);
            ValidacionService.replaceDocVar(data).success( function (response){
                console.log(response);
                if (response.isSuccess == true){
                    $.ajax({
                        url: base_url+"app/radicados/generalradicado.php",
                        type: 'POST',
                        dataType: 'json',
                        async: false,
                        data: {
                            navigator_appversion: appVersion,
                            function_call: "accion_validar",
                            codigo: codigo,
                            radicado: radicados_selecteds_v[0]['radicado'],
                            ANEXO_ID: anexos_selecteds_v[0]['ANEXO_ID'],
                            radi_padre: radi_padre,
                            platform: navigator.platform,
                            browser: getBrowser(),
                            url_app: url_app

                        },
                        success: function (response) {
                            if (response.error == undefined) {
                                //limpiarSessionCorreo();
                                getUsersValidaron();

                                alertModal('Bien hecho !', 'Documento ' + conjugacionValidar[9] + '', 'success', '10000');
                                row_doc_validado_bien(doc_codusuario);

                                showpersonporvalidar();
                                armartabladocprincipal();
                                mostrarhash();
                                validarDeshabAuthAp();

                            } else {
                                Swal.fire("Error", response.error, "warning");
                            }

                            $('#btn_validar_modal').prop('disabled', false);
                            $('#btn_enviar_codigo_email').prop('disabled', false);
                            $('#btn_passw_valida').prop('disabled', false);
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            if (xhr.responseJSON.message) {
                                Swal.fire("Error", xhr.responseJSON.message, "error");
                            } else {
                                Swal.fire("Error", "Ha ocurrido un error al " + conjugacionValidar[0] + "," +
                                    " cierre el aplicativo e intente nuevamente", "error");
                            }
                            $('#btn_validar_modal').prop('disabled', false);
                            $('#btn_enviar_codigo_email').prop('disabled', false);
                            $('#btn_passw_valida').prop('disabled', false);
                        }
                    });
                }else{
                    Swal.fire("Error",response.message,'error');
                }
            }).error(function (error){
                Swal.fire("Error",error.message,'error');
            });




        }
        Upload.speed = 10;
        Upload.set_values_bar(95, 100);
    }, 3000)
console.log('tracking error log 2');  
}

/**
 * despues de validar el pdf, hay que mandar a guardar el historial y enviar el correo electronico de que fue validado
 */
function saveHistDespValidar(data) {
    console.log(data);
    var retorno = false;
    $.ajax({
        url: base_url+"app/radicados/generalradicado.php",
        type: 'POST',
        dataType: 'json',
        async: false,
        data: {
            function_call: "serviceSaveHistDespValidar",
            radicado: data.radicado,
            infoAnexo: data.infoAnexo,
            inforadicado: data.inforadicado,
        },
        success: function (response) {
            if (response.error == undefined) {
                retorno = true;
            } else {
                Swal.fire("Error", response.message, "error");
            }

        },
        error: function (response) {

        }
    });

    return retorno;

}

//muestra el texto de las personas que faltan por validar/ solo cuando ya haya al menos una persona que haya validado
function showpersonporvalidar() {
    getUsersValidaron();
    if (Object.keys(usersValidaron).length > 0 &&
        (parseInt(Object.keys(firmantesselected).length) - parseInt(Object.keys(usersValidaron).length)) > 0) {
        $('#row_nro_user_sin_vali').html('');
        $('#row_nro_user_sin_vali').html('Faltan ' + (parseInt(Object.keys(firmantesselected).length) - parseInt(Object.keys(usersValidaron).length)) + ' personas por ' + conjugacionValidar[0]);
    }
}

//el boton rojo de comorobar validez
function comprobarvalidez(e) {
    e.preventDefault();
    $('#btn_comprobarvalidez').prop('disabled', true);
    Upload.hasta = 100;
    Upload.move();
    $("#upload_two").css({"display": "none"});
    $("#open_name").html('')
    $('#providers-list .row').empty();

    $("#upload").css("display", "none");
    $("#myProgress").css("display", "block");
    $("#processing").css("display", "block");
    ultimaComH = [];
    setTimeout(function () {
        $.ajax({
            url: base_url+"app/radicados/generalradicado.php",
            type: 'POST',
            dataType: 'json',
            async: false,
            data: {
                function_call: "comprobar_validez_doc",
                ANEXO_ID: anexos_selecteds_v[0]['ANEXO_ID']
            },
            success: function (response) {
                Upload.speed = 10;
                Upload.set_values_bar(95, 100);
                if (response.error == undefined) {
                    alertModal('Comprobación exitosa !', response.success, 'success', '10000');
                    getUsersValidaron();
                    row_doc_validado_bien(doc_codusuario);
                    showpersonporvalidar();
                    armartabladocprincipal();
                    ultimaComH = response.ultimoHist;
                    rearmarBotonCom();
                    rearmarDeHistorico();
                } else {
                    Swal.fire("Documento inválido", response.error, "error");
                }

            },
            error: function (response) {
                Upload.speed = 10;
                Upload.set_values_bar(95, 100);
                Swal.fire("Error", "Ha ocurrido un error por favor contactar con soporte", "error");
            }
        });

        $('#btn_comprobarvalidez').prop('disabled', false);
    }, 3000)

}

//funcion que manda a descargar el pdf
function descargarpdf(tipo) {
    window.location.href = "./app/radicados/generalradicado.php?function_call=verdocvalidado&tipo=" + tipo + "&radicado=" + document_metadata.radi_nume_radi
}

//manda a descargar el doc (pdf) que se requiera
function ver_doc(tipo) {

    if (tipo == 'doc_original') {

        if(document_metadata.exist_radi_path_original==false || document_metadata.exist_radi_path_original==null ){
            alertModal('Alerta', 'El documento no existe', 'warning', '10000');
            return false;
        }
    }

    if (tipo == 'doc_validado') {
        if(document_metadata.exist_radi_path_validado==false || document_metadata.exist_radi_path_validado==null ){
            alertModal('Alerta', 'El documento no existe', 'warning', '10000')
            return false;
        }
    }

    if (navegadorActual == "isFirefox") {
        descargarpdf(tipo)
    } else {

        var radi_path_original = document_metadata.radi_path_original != undefined ? document_metadata.radi_path_original : document_metadata.radi_path_original;
        var radi_path_validado = document_metadata.radi_path_validado != undefined ? document_metadata.radi_path_validado : document_metadata.radi_path_validado;
        var ruta = ''
        if (tipo == 'doc_original') {
            ruta = base_url + 'bodega' + radi_path_original;
        }

        if (tipo == 'doc_validado') {
            ruta = base_url + 'bodega' + radi_path_validado;
        }
        $('#btn_descargar_pdf').attr("onclick", "descargarpdf('" + tipo + "')")
        showModalVistaPrevia(ruta, 'pdf', 'modal_vista_previa_validar', 'header_vista_previa_validar',
            'modal_body_vista_previa_validar', 'embed_validar');
    }
    return true;
}


function armarDesHistoricoCom() {

boton  = '';
sizeHistorico = parseInt(Object.keys(historicoComprobacion).length);
if (sizeHistorico  > 0) {
boton = '<button type="button" title="Historico de comprobacion" id="btn_historyVal"\n' +
    '     style="margin-right: 1em;border-bottom-left-radius:0px !important;border-top-left-radius:0px !important;"' +
    '      onclick="" data-toggle="dropdown" aria-expanded="false" class="btn btn-lg dropdown-toggle red">\n' +
    '      <i class="fa fa-angle-down" style="height: 1.3em !important;"></i>' +
    '       </button>';

boton = boton + '<ul class="dropdown-menu" role="menu" id="dropdown_histo_comp">';
boton = boton + '<li id="ultimo_comhis"><a href="#"  onclick="return false;" data-historico="'+historicoComprobacion[sizeHistorico-1].result+'" >Ultima Comprobación: '+(historicoComprobacion[sizeHistorico-1].date_verification)+'<br> por '+historicoComprobacion[sizeHistorico-1].usuario.usua_nomb+' [ '+historicoComprobacion[sizeHistorico-1].result+' ]</a></li>';
    boton = boton + '<li><a href="#" onclick="showModalHistorialComp();" data-historico="'+historicoComprobacion[sizeHistorico-1].result+'" >Ver Historial Completo de Comprobaciones</a></li></ul>';
}
return boton;
}
function rearmarBotonCom() {
    var   sizeHistorico = parseInt(Object.keys(historicoComprobacion).length);
    if (sizeHistorico < 1) {
var btnCom = '<button type="button" title="Comprobar Validez" id="btn_comprobarvalidez"';
btnCom = btnCom + 'style="border-top-right-radius:0px !important;border-bottom-right-radius:0px !important;border-top-left-radius:20px !important;';
btnCom = btnCom + 'border-bottom-left-radius:20px !important;"';
btnCom = btnCom + ' onclick="comprobarvalidez(event)" class="btn btn-lg red">';
btnCom = btnCom + '<i class="fa fa-file"></i> Comprobar Validez</button>';
btnCom = btnCom + '<button type="button" title="Historico de comprobacion" id="btn_historyVal"';
btnCom = btnCom + 'style="margin-right: 1em;border-bottom-left-radius:0px !important;border-top-left-radius:0px !important;"';
btnCom = btnCom + 'onclick="" data-toggle="dropdown" aria-expanded="false" class="btn btn-lg dropdown-toggle red">';
btnCom = btnCom + '<i class="fa fa-angle-down" style="height: 1.3em !important;"></i>';
btnCom = btnCom + '</button>';
btnCom = btnCom + '<ul class="dropdown-menu" role="menu" id="dropdown_histo_comp">';
btnCom = btnCom + '<li id="ultimo_comhis"></li>';
btnCom = btnCom + '<li><a href="#" onclick="showModalHistorialComp();" data-historico="'+ultimaComH['result']+'" >Ver Historial Completo de Comprobaciones</a></li></ul>';
        $('#botoncompVal').html('');
        $('#botoncompVal').html(btnCom);
    }

}

function returnMargin(){
 var sizeHistorico = parseInt(Object.keys(historicoComprobacion).length) + parseInt(Object.keys(ultimaComH).length);
    var  margin = '';
    if (sizeHistorico < 1) {
        margin = 'margin-right: 1em;';
    }else{
        margin = '';
    }
    return margin;
}

function rearmarDeHistorico(){
  if (parseInt(Object.keys(ultimaComH).length) > 0){
      $('#ultimo_comhis').html('');
      $('#ultimo_comhis').html('<a href="#"  onclick="return false;" data-historico="'+ultimaComH['result']+'" >Ultima Comprobación: '+(ultimaComH['date_verification']) +'<br> por '+usua_nomb+' [ '+ultimaComH['result']+' ]</a></li>');

  }
}

    function showModalHistorialComp(){
    tablaHistoricoADefinir = $('#tablahistorico');
    if (tablaHistoricoC != null){
        tablaHistoricoC.destroy();
    }
    $('#modal_historico_comprobacion').modal('show');
    definirTablaHistorico(tablaHistoricoADefinir);
    htmlTabla = '';
    for (let i = 0;  i < Object.keys(historicoComprobacion).length; i++) {
        htmlTabla = htmlTabla + '<tr><td>'+ historicoComprobacion[i].date_verification +'</td><td>El resultado de comprobación del documento fue: '+  historicoComprobacion[i].result +'</td><td>'+ historicoComprobacion[i].usuario.usua_nomb + '</td></tr>';

    }
    var sizeHistorico = parseInt(Object.keys(ultimaComH).length);
    if (sizeHistorico > 0){
        htmlTabla = htmlTabla + '<tr><td>'+ ultimaComH['date_verification']  +'</td><td>El resultado de comprobación del documento fue: '+  ultimaComH['result'] +'</td><td>'+  usua_nomb + '</td></tr>';
    }

    $('#tbodyhistorico').html('');
    $('#tbodyhistorico').html(htmlTabla);

}

//muestra el div donde aparecen los botones de ver documento validado y ver original
function row_doc_validado_bien() {

    if (usersValidaron[0] != undefined) {


        var verdoc = "'doc_validado'"
        var html = '<table width="100%"><tr><td></td>' +
            '<td align="right"> <button type="button" title="Ver documento ' + conjugacionValidar[1] + '" ' +
            '                                style="margin-right: 1em;border-top-right-radius:0px !important;\n' +
            '                                                    border-top-left-radius:20px !important;\n' +
            '                                                       border-bottom-left-radius:20px !important;"\n' +
            '                                onclick="ver_doc(' + verdoc + ')" class="btn btn-lg blue">\n' +
            '                          <i class="fa fa-file"></i> Ver documento ' + conjugacionValidar[1] + '\n' +
            '\n' +
            '                        </button></td></tr>';

        verdoc = "'doc_original'"
        html += '<tr><td></td><td align="right"><a href="#" style="margin-right: 1em;" title="Ver original" ' +
            '                                 onclick="ver_doc(' + verdoc + ')">' +
            '                          Ver original' +
            '                        </a><br></td></tr>' +
            '<tr><td></td><td align="right"><div id="botoncompVal" class="btn-group"><button type="button" title="Comprobar Validez" id="btn_comprobarvalidez"\n' +
            '                                style="'+returnMargin()+'border-top-right-radius:0px !important;border-bottom-right-radius:0px !important;border-top-left-radius:20px !important;\n' +
            '                                                       border-bottom-left-radius:20px !important;"\n' +
            '                                onclick="comprobarvalidez(event)" class="btn btn-lg red">\n' +
            '                          <i class="fa fa-file"></i> Comprobar Validez</button>' + armarDesHistoricoCom()
            '                         </div></td></tr>';


        $('#row_doc_validado_bien').html('');
        $('#row_doc_validado_bien').html(html);
        mostrarhash();
    }
}

//muestra la imagen del hash y el texto
function mostrarhash() {
    if ($('#row_img_barra').length < 1) {

        $('#row_nro_user_sin_vali').after('<div class="row" id="row_img_barra" style="text-align:center"></div>');
    }
    $('#row_img_barra').html('<img height="7%" width="50%" src="' + base_url + document_metadata.path_img_barra + '">');

    if ($('#row_hash_barra').length < 1) {
        $('#row_img_barra').after('<div class="row" id="row_hash_barra" style="text-align:center"></div>');
    }
    $('#row_hash_barra').html(document_metadata.hash_doc_original);
}


//hace el proceso de mostrar los datos que estan en la tabla
function mostrartabla() {
    getUsersValidaron();
    armartabladocprincipal();

}

function doc_reset_vars() {
    doc_allUsersSinAutorizar = new Array();
    firmantesselected = new Array();
    firmantesselected = new Array();
    document_metadata = new Array(); //info de la tabla document_metadata
    usersValidaron = new Array();  //usuarios que ya validaron el doc: document_authorized_by
    lastUserValidarion = new Array();
    $('#cabecera_modal_valida').html('')
}

//cuando presionan sobre uno de los usuarios el dropdown del boton Enviar en el modal raiz de borrador
function clickdropdownreasignar(esto, usr,userLog) {

    //le digo que va a buscar en el select a este usuario adicional
    usr_especific_select_enviar = usr;
    ReasignarRadicado.showModalReasignar(event, radicados_selecteds_v[0]['radicado'] ,userLog,usr,$(esto).attr('data-nombre_con_dependencia') );

    /**
     * Lo siguiente, agrega al select del modal de enviar, el usuario seleccionado
     */
    //setTimeout(function () {
    //  var dropdown = $('#enviara');
    //var option = new Option($(esto).attr('data-nombre_con_dependencia'), usr, true, true);
    //dropdown.append(option).trigger('change');
    //}, 50)

}



//levanta el modal de aprobar documentos
function doc_showmodalaprobardoc(radicado) {
    doc_reset_vars();
    var botonReasignar;
    $('#btn_validar_modal').text(conjugacionValidar[0]);
    $('#div_apend_aprob_doc').html('');
    doc_usersPosibleAutoriza();
    var btModasunto = '';
    var yavalide = false;
    for (let i = 0; i < usersValidaron.length; i++) {
        if (usersValidaron[i].usuario_id == doc_codusuario) {
            yavalide = true;
            break;
        }
    } 
    if (yavalide == false && isAsociado == 'SI') {
        alertModal('Advertencia','Para '+conjugacionValidar[0]+' el documento, ingrese al mismo','warning','10000');
    }

    if (dependencia  ==  radicados_selecteds_v[0]['radi_depe_actu'] && usua_codi == radicados_selecteds_v[0]['radi_usua_actu'] && isAsociado == 'NO') {

        if (radicados_selecteds_v[0]['sgd_trad_codigo'] != 2){
            btModasunto = '<span style="display: initial;" class="fa-item"><a href="#" onclick="modalmodAsunto();"> <i class="fa fa-pencil"></i> </a></span>';
        }else if (vusua_admin_sistema > 0){
            btModasunto = '<span style="display: initial;" class="fa-item"><a href="#" onclick="modalmodAsunto();"> <i class="fa fa-pencil"></i> </a></span>';
        }

        RadicadoService.getJefesdropdown(doc_codusuario, dependencia, dependencia_padre, radicados_selecteds_v[0]['radicado']).success(function (response) {
            if (response.error == undefined) {

                botonReasignar = '<div class="col-md-3" id="div_btn_reasignar"><div class="btn-group" style="float: right;"><button type="button" title="Reasignar Radicado" id="div_enviar"' +
                    'style="border-top-right-radius:0px !important;' +
                    'border-top-left-radius:20px !important;' +
                    'border-bottom-left-radius:20px !important;"' +
                    'onclick="ReasignarRadicado.showModalReasignar(event,' + radicados_selecteds_v[0]['radicado'] +',' +doc_codusuario+')" class="btn btn-sm blue">' +
                    '<i class="fa fa-arrow-right"></i>' +
                    'Reasignar' +
                    '</button>' +
                    '<button type="button"' +
                    'class="btn blue btn-sm dropdown-toggle depende_div_enviar"' +
                    'style="margin-right: 10px;' +
                    'padding-bottom: 0.7em;' +
                    'padding-top: 0.502em;' +
                    'border-top-right-radius:20px !important;' +
                    'border-bottom-right-radius:20px !important;" data-toggle="dropdown"' +
                    'aria-expanded="false">' +
                    '<i class="fa fa-angle-down"></i>' +
                    '</button>' +
                    '<ul class="dropdown-menu" role="menu" id="dropdown_enviar_a">';

                if(response.objects.length > 0){
                    $.each(response.objects, function (ind, elem) {
                        botonReasignar = botonReasignar + '<li>' +
                            '<a href="#" data-nombre_con_dependencia="'+elem.usua_nomb+'-'+elem.dependencia.depe_codi+'-'+elem.dependencia.depe_nomb+'"' +
                            'onclick="clickdropdownreasignar(this,'+elem.id+','+doc_codusuario+')">Reasignar a '+elem.usua_nomb+'</a></li>';
                    });
                    botonReasignar = botonReasignar + '<li>' +
                        '<a href="#" data-nombre_con_dependencia=""' +
                        'onclick="ReasignarRadicado.showModalReasignar(event,' + radicados_selecteds_v[0]['radicado'] + ','+doc_codusuario +')">Reasignar a otros</a></li>';
                }

                botonReasignar = botonReasignar +'</ul></div></div>';





                showpersonporvalidar();
                $("#modal_aprobar_documento").modal('show');
                $('#cabecera_modal_valida').html('<div class="col-md-9"  > <div class="form-group"><strong style="color: #2a2a2a; font-size: 12px">' + radicados_selecteds_v[0]['radicado'] + ' ' + getAsuntoWithBreakLine(radicado) + '</strong> '+btModasunto+'</div></div>' + botonReasignar)
                mostrartabla();

                if (Object.keys(firmantesselected).length < 1) {
                    alertModal('Alerta', 'No se ha seleccionado la persona(s) autorizada(s) para ' + conjugacionValidar[0].toLowerCase() + ' el documento', 'warning', '10000')
                }
                validarDeshabAuthAp();
            } else {
                Swal.fire("Error", "Ha ocurrido un error al buscar la información de los usuarios, comunicarse con soporte", "error");
                return false;
            }
        }).error(function (error) {
            Swal.fire("Error", "Ha ocurrido un error al buscar la información de los usuarios", "error");

            return false;
        });


    } else {

        if  ((dependencia  ==  radiPrincipal.radi_depe_actu && usua_codi == radiPrincipal.radi_usua_actu) && radiPrincipal.sgd_trad_codigo != 2) {
            btModasunto = '<span style="display: initial;" class="fa-item"><a href="#" onclick="modalmodAsunto();"> <i class="fa fa-pencil"></i> </a></span>';

        }else if (vusua_admin_sistema > 0){
            btModasunto = '<span style="display: initial;" class="fa-item"><a href="#" onclick="modalmodAsunto();"> <i class="fa fa-pencil"></i> </a></span>';
        }


        showpersonporvalidar();
        $("#modal_aprobar_documento").modal('show');
        $('#cabecera_modal_valida').html('<div class="col-md-8"  > <div class="form-group"><strong style="color: #2a2a2a; font-size: 12px">' + radicados_selecteds_v[0]['radicado'] + ' ' + getAsuntoWithBreakLine(radicado) + '</strong>'+btModasunto+'</div></div>' )
        mostrartabla();

        if (Object.keys(firmantesselected).length < 1) {
            alertModal('Alerta', 'No se ha seleccionado la persona(s) autorizada(s) para ' + conjugacionValidar[0].toLowerCase() + ' el documento', 'warning', '10000')
        }
        validarDeshabAuthAp();
    }
}



//cuando hace click al boton de la tabla de documentos en la raiz
function doc_clickvalidarraiz(radicado, ANEXO_ID) {
    RadicadoService.getRadicadoByNume(radicado,configsHasValue['DIAS_REABRIR_VALIDACION']).success(function (response) {
        if (response.error == undefined) {

            radicados_selecteds_v[0] = response.objects; //info de la tabla document_metadata
            radicados_selecteds_v[0]['radicado'] = radicado;
            radicados_selecteds_v[0]['hoymenos15'] = response.hoymenos15;
            radicados_selecteds_v[0]['fecharadicadoFormated'] = response.fecharadicadoFormated;
            userConfigurados = response.user_config;
            historicoComprobacion = response.objects.document_validity_history;
        } else {
            Swal.fire("Error", "Ha ocurrido un error al buscar la información del radicado, comunicarse con soporte", "error");
            return false;
        }
    }).error(function (error) {
        Swal.fire("Error", "Ha ocurrido un error al buscar la información del radicado", "error");

        return false;
    });

    anexos_selecteds_v[0] = {};
    anexos_selecteds_v[0]['ANEXO_ID'] = ANEXO_ID;


    var estainclido = doc_expedincluidos()
    console.log(Object.keys(estainclido).length);
    if (estainclido != false){
        console.log(Object.keys(estainclido).length);
        if(Object.keys(estainclido).length > 0) {
            //esta incluido en expediente
            isAsociado = 'NO';
            if (validaEnBotones('validar', ANEXO_ID) == true) {
                RadicadoService.getRadPrincByAnexo(anexos_selecteds_v[0]['ANEXO_ID']).success(function (response) {
                    if (response.error == undefined) {

                        radiPrincipal  = response.objects;
                        isAsociado = response.isAsociado;
                        doc_showmodalaprobardoc(radicado);

                        if (isAsociado == 'SI'){
                            $('#row_btn_validar').remove();
                        }
                    } else {
                        isAsociado = 'NO';
                        Swal.fire("Error", response.error, "error");
                        return false;
                    }
                }).error(function (error) {
                    isAsociado = 'NO';
                    Swal.fire("Error", response.error, "error");

                    return false;
                });

            }
        }else{

            var html = 'Radicado sin expediente, ' +
                'es necesario incluirlo antes de ' + conjugacionValidar[0].toLowerCase();
            //@formatter:off
            Swal.fire({
                title: "Alerta",
                text: html,
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Ir a Expediente",
                cancelButtonText: "Cerrar",
                closeOnConfirm: true,
                closeOnCancel: true
            }).then((result) => {
                if (result.value) {
                    window.location.href = "app/borradores/radicardesdeentrada.php?radicado=" + radicados_selecteds_v[0]['radicado'] + ","
                } else if (
                    result.dismiss === Swal.DismissReason.cancel
                ) {

                }
            });
            //@formatter:on
        }
    }else{

        var html = 'Radicado sin expediente, ' +
            'es necesario incluirlo antes de ' + conjugacionValidar[0].toLowerCase();
        //@formatter:off
        Swal.fire({
            title: "Alerta",
            text: html,
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Ir a Expediente",
            cancelButtonText: "Cerrar",
            closeOnConfirm: true,
            closeOnCancel: true
        }).then((result) => {
            if (result.value) {
                window.location.href = "app/borradores/radicardesdeentrada.php?radicado=" + radicados_selecteds_v[0]['radicado'] + ","
            } else if (
                result.dismiss === Swal.DismissReason.cancel
            ) {

            }
        });
        //@formatter:on
    }
}

function extNoPermit() {

    var extensionespermitidas = JSON.parse(configsHasValue['VALIDAR_EXTENSIONES'])
    var html = ''
    for (var i = 0; i < Object.keys(extensionespermitidas).length; i++) {
        html += extensionespermitidas[i].toUpperCase() + ', '
    }
    if (html != '') {
        html = html.substr(0, html.length - 2)
    }

    Swal.fire({
        title: "No permitido",
        text: 'Solo se puede ' + conjugacionValidar[8].toLowerCase() + ' documentos ' + html,
        type: "warning",
        closeOnConfirm: true,
        closeOnCancel: true
    });
}

/**
 * Funcion que indica cuales son los tipos de radicados permitidos para validar
 */
function tipoRadNoPermit() {
    var tiporadperm = JSON.parse(configsHasValue['TIPOS_RADICADO_VALIDAR'])
    var html = ''
    var texto = ''
    for (var i = 0; i < Object.keys(tiporadperm).length; i++) {
        texto = ''
        if (tiporadperm[i] == '1') {
            texto = 'Salida'
        }

        if (tiporadperm[i] == '3') {
            texto = 'Internos'
        }

        if (tiporadperm[i] == '4') {
            texto = 'Reportes'
        }
        html += texto.toUpperCase() + ', '
    }
    if (html != '') {
        html = html.substr(0, html.length - 2)
    }


    Swal.fire({
        title: "No permitido",
        text: 'Solo se puede ' + conjugacionValidar[8].toLowerCase() + ' ' + html,
        type: "warning",
        closeOnConfirm: true,
        closeOnCancel: true
    });
}

function cerrarmodalvalidar() {
    $('#modal_aprobar_documento').modal('hide');
}

function validaEnBotones(boton, anexo_id=0, anexo=false, tpradic=false, aplinteg=false, radicado=false, isAnexo=false) {
    var continuar = true;
    $.ajax({
        url:  base_url+"app/radicados/generalradicado.php",
        type: 'POST',
        dataType: 'json',
        async: false,
        data: {
            anexo_id: anexo_id,
            function_call: "clickEnBtnsValidar"
        },
        success: function (response) {
            datos_primera_validacion = response;

            /**
             * se guarda los path del anexo y del radicado, ya que si es modificar, se compara contra la del path principal.
             * Pero cuando la es accion Validar o re-generar, se compara el path contra el anexo
             */

            if (response.borrador[0] != undefined) {
                borrador_id = response.borrador[0]['ID_BORRADOR']
                //almaceno los datos en una variable


                if (boton == 're-generar') {

                    if (response.fueescaneado > 0) {
                        Swal.fire("No permitido", "El documento principal ya fue entregado para escanear", "warning",10000);
                        continuar = false;
                        return false;
                    }

                    if (continuar == true &&
                        Object.keys(response.query_document_met).length > 0 && response.query_document_met[0] != undefined
                        && response.query_document_met[0]['STATUS_DOCUMENT'] == 'CERRADO') {
                        Swal.fire("No permitido", "El radicado ya ha sido " + conjugacionValidar[1].toLowerCase() + " por todos los usuarios", "error");
                        continuar = false;
                        return false;
                    }

                    if (continuar == true && Object.keys(response.borrador).length > 0 &&
                        response.borrador[0]['DOCUMENTO_FORMATO'] == 'EditorWeb') {
                        Swal.fire("No permitido", "El documento principal es PDF  (No se debe re-generar)", "error");
                        continuar = false;
                        return false;
                    }

                    /**
                     * cuando el fue Salida, y viene de editorweb, el path en la tabla radiacdo, es un pdf,
                     * pero el path en anexo, es un docx, por lo tanto, como aqui se compara contra el path del anexo,
                     * no deberia poder re-generar, ya que el documento principal. no viene de una plantilla, sino del editorweb
                     */
                    if (continuar == true && Object.keys(response.borrador).length > 0 &&
                        (response.pathinfoAnexo.extension == 'pdf' || response.pathinfoAnexo.extension == 'PDF')
                        && response.borrador[0]['DOCUMENTO_FORMATO'] != 'EditorWeb') {
                        Swal.fire("No permitido", "El documento principal es PDF  (No se debe re-generar)", "error");
                        continuar = false;
                        return false;
                    }

                }

                if (boton == 'modificar') {


                    if ( response.fecTransaccionDev  == null &&
                        continuar == true &&
                        Object.keys(response.query_document_met).length > 0 && response.query_document_met[0] != undefined
                        && response.query_document_met[0]['STATUS_DOCUMENT'] == 'CERRADO') {

                        alertModal('Atención', 'Para modificar, debe agregar primero un '+conjugacionValidar[11].toLowerCase(), 'warning', 10000);
                        continuar = false;
                        return false;

                    }else if(response.fecTransaccionDev  == null &&
                        continuar == true &&
                        Object.keys(response.query_document_met).length > 0 && response.query_document_met[0] != undefined
                        && response.query_document_met[0]['STATUS_DOCUMENT'] == 'ABIERTO') {
                        continuar = true;
                        return true;
                    }else{

                        if (response.fueescaneado > 0) {
                            alertModal('Atención', 'El documento principal ya fue entregado para digitalizar.', 'warning', 10000);
                            continuar = true;
                        }

                        if (continuar == true &&
                            Object.keys(response.query_document_met).length > 0 && response.query_document_met[0] != undefined
                            && response.query_document_met[0]['STATUS_DOCUMENT'] == 'CERRADO') {
                            Swal.fire("No permitido", "El radicado ya ha sido " + conjugacionValidar[0].toLowerCase() + " por todos los usuarios", "error");
                            continuar = false;
                            return false;
                        }
                    }


                    if (continuar == true &&
                        Object.keys(response.borrador).length > 0 &&
                        (response.pathinfoRadPrincipal.extension == 'pdf' || response.pathinfoRadPrincipal.extension == 'PDF')) {
                        //@formatter:off
                        Swal.fire({
                            title: "Alerta",
                            text: 'El documento principal es un PDF, no deberia modificarlo. Está seguro?',
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonText: "SI",
                            cancelButtonText: "NO",
                            closeOnConfirm: true,
                            closeOnCancel: true
                        }).then((result) => {
                            if (result.value) {
                                todoOkModificar(anexo, tpradic, aplinteg, anexo_id,radicado,isAnexo);
                            } else if (
                                result.dismiss === Swal.DismissReason.cancel
                            ) {

                            }
                        });
                        //@formatter:on
                        continuar = false;
                        return false;
                    }

                    if (response.borrador[0]['DOCUMENTO_FORMATO'] == 'EditorWeb') {
                        alertModal('Advertencia', 'si modifica, se reemplazará el documento principal', 'warning', 10000);
                    }

                }

                if (boton == 'validar') {

                    /* if (continuar == true && Object.keys(response.borrador).length > 0 &&
                         (response.pathinfo.extension == 'pdf' || response.pathinfo.extension == 'PDF')) {
                         Swal.fire("No permitido", "El documento principal es PDF  (No se debe re-generar)", "error");
                         continuar = false;
                         return false;
                     }*/

                    if (continuar == true && response.fueregenerado == 0 &&
                        response.pathinfoAnexo.extension != 'pdf' && response.pathinfoAnexo.extension != 'PDF') {
                        Swal.fire("Sin Re-Generar", "Primero debe re-generar el documento", "error");
                        continuar = false;
                        return false;
                    }
                }
            }

            var pathcomparar = '';
            if (boton == 're-generar' || boton == 'modificar') {
                pathcomparar = response.pathinfoRadPrincipal.extension;

            } else {
                pathcomparar = response.pathinfoAnexo.extension;
            }
            if (response.fueregenerado == 0
                &&
                pathcomparar != 'pdf' && pathcomparar != 'PDF'
                &&
                (
                    (
                        Object.keys(response.borrador).length > 0 &&
                        response.borrador[0]['DOCUMENTO_FORMATO'] == 'Plantilla'
                    )
                    ||
                    (
                        Object.keys(response.borrador).length < 1
                    )
                )
            ) {
                alertModal('Sin Re-generar', 'Clic en regenerar al radicado ' + response.radicado, 'warning', 10000);
            }


        }
    }).done(function () {
        $(".guardar").prop('disabled', false);
    }).fail(function (error) {
        $(".guardar").prop('disabled', false);
        console.log(error);
    });

    return continuar;
}

/**
 * esta funcion, retorna el asunto filtrandolo por un radicado especifico, de la variable "asuntosRadicados",
 * que fue creada en lista_anexos.php, ya que los anexos, pueden venir con saltos de linea
 */
function getAsuntoWithBreakLine(radicado) {

    var asunto = '';

    $.each(asuntosRadicados, function (key, value) {

        if (key == radicado) {
            asunto = value
        }
    });

    return asunto

}


/**
 * trae, el nombre del usuario y la dependencia dentro del parentesis
 * @param user
 * @returns {string}
 */
function unionUserNameWithDepe(user) {
    return user.usua_nomb + ' (' + user.dependencia.depe_nomb + ')'
}

function modAsunto(){
    $('#btnModAsunto').prop("disabled", true);
    var Asunto;
    if (estatusValEscRad == 'validado' || estatusValEscRad == 'escaneado'){
        Asunto = $('#asuntoAdd').val();
    }else{
        Asunto = $('#AsuntoActual').val();
    }
    $.ajax({
        url: configsHasValue['WS_APPSERVICE_URL'] +'/radicado/saveAsunto',
        headers: {
            "Authorization": 'Bearer '+configsHasValue['WS_APPSERVICE_TOKEN']
        },
        type: 'POST',
        dataType: 'json',
        async: false,
        data: {
            radicado_id: radicados_selecteds_v[0]['radicado'],
            asunto: Asunto,
            user_id: doc_codusuario
        },
        success: function (response) {
            alertModal('Bien hecho !', response.data, 'success', '10000');
            if(pestana_document!=undefined && pestana_document!=false){
                setTimeout(function () {
                    window.location.href = pestana_document;
                    $('#btnModAsunto').prop("disabled", false);
                }, 3000)
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {

            if(xhr.responseJSON.message){
                Swal.fire("Error", xhr.responseJSON.message, "error");
                $('#btnModAsunto').prop("disabled", false);
            }else{
                Swal.fire("Error", "Ha ocurrido un error al modificar el asunto", "error");
                $('#btnModAsunto').prop("disabled", false);
            }
        }
    });
}

function traerRadicado(){
    var radicado = radicados_selecteds_v[0]['radicado'];
    console.log(radicado);
    var Readonly = '';
    var radicadoarray = new Array();
    radicadoarray[0] = radicado;

    $.ajax({
        url: configsHasValue['WS_APPSERVICE_URL'] +'/radicado/validaEstaEscaneado',
        headers: {
            "Authorization": 'Bearer '+configsHasValue['WS_APPSERVICE_TOKEN']
        },
        type: 'POST',
        dataType: 'json',
        async: false,
        data: {
            radicados: radicadoarray
        },
        success: function (response) {
            if (typeof(response.objects[0].radicado.radi_nume_iden) != 'Undefined' && (response.objects[0].radicado.radi_nume_iden == 1 || response.objects[0].radicado.radi_nume_iden == 2) ){

                estatusValEscRad = 'validado';
                Readonly = 'readonly';
            }else if(typeof(response.objects[0].fueescaneado) != 'Undefined' && (response.objects[0].fueescaneado != 0)){
                estatusValEscRad = 'escaneado';
                Readonly = 'readonly';
            }else{
                estatusValEscRad = 'none';
            }

            $('#modal_modAsunto').modal('show');
            if (estatusValEscRad == 'validado'){

                $('#btnAdvDanger').html('<strong>Advertencia!</strong> El Radicado ya ha sido '+conjugacionValidar[9]+'. Solo puede agregarle información al asunto.<br><br>El asunto solo se debería modificar cuando el documento se encuentra en BORRADOR.');
                $('#btnAdvDanger').show();
                showAsuntoInp(response.objects[0].radicado.ra_asun,Readonly);
                $('#addAsunto').show();
                $('#asuntoAdd').show();
                $('#btnModAsunto').show();
                combinarAsuntos();
            }else if(estatusValEscRad == 'escaneado'){
                $('#btnAdvDanger').html('<strong>Advertencia!</strong> El Radicado ya ha sido digitaliado. Solo puede agregarle información al asunto.<br><br>El asunto solo se debería modificar cuando el documento se encuentra en BORRADOR.');
                $('#btnAdvDanger').show();
                showAsuntoInp(response.objects[0].radicado.ra_asun,Readonly);
                $('#addAsunto').show();
                $('#asuntoAdd').show();
                $('#btnModAsunto').show();
                combinarAsuntos();
            }else{
                $('#btnAdvWar').click(function () {
                    showAsuntoInp(response.objects[0].radicado.ra_asun,'');
                });
                $('#btnAdvWar').show();

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {

            if(xhr.responseJSON.message){
                Swal.fire("Error", xhr.responseJSON.message, "error");
            }else{
                Swal.fire("Error", "Ha ocurrido un error al buscar los datos", "error");
            }
        }
    });
}

function showAsuntoInp(texto,attread) {

    $("#AsuntoActual").html(texto);
    if (attread != ''){
        $("#AsuntoActual").attr('readonly',attread);
    }
    $('#AsuntoActualLab').show();
    $('#AsuntoActual').show();
    $('#btnModAsunto').show();
}

function combinarAsuntos() {
    $('#nuevoAs').html($('#AsuntoActual').val());
    $('#asuntoAdd').keyup(function () {
        $('#nuevoAs').html($('#AsuntoActual').val() + ' ' +$('#asuntoAdd').val());
    });

    $('#newAsunto').show();
    $('#nuevoAs').show();
}

function modalmodAsunto(){
    traerRadicado();

}

function cerrarModalmodAsunto() {
    $("#modal_modAsunto").modal('hide');
}


$(document).ready(function () {

    jQuery('#modal_aprobar_documento').on('hidden.bs.modal', function (e) {
        if(pestana_document!=undefined && pestana_document!=false){
            setTimeout(function () {
                window.location.href = pestana_document;
            }, 500)
        }

    });

    if (
        radicados_sin_regenerar != undefined
        &&
        radicados_sin_regenerar != null
        &&
        radicados_sin_regenerar != 'null'
        &&
        Object.keys(radicados_sin_regenerar).length > 0
    ) {

        for (var i = 0; i < Object.keys(radicados_sin_regenerar).length; i++) {
            alertModal('Sin Re-generar', 'Clic en regenerar al radicado ' + radicados_sin_regenerar[i], 'warning', 10000);
        }
    }

});
