<?php

use Philo\Blade\Blade;

require '../../vendor/autoload.php';
require_once '../../config.php';

$ruta_raiz = "../../";




if (isset($db)) unset($db);
include_once("$ruta_raiz/include/db/ConnectionHandlerNew.php");
$db = new ConnectionHandler("$ruta_raiz");

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$views = $ABSOL_PATH . 'app/views';
$cache = $ABSOL_PATH . 'app/cache';

//Clearing the views cache
\App\Helpers\MiscFunctions::clear_view_cache();

$blade = new Blade($views, $cache);
$helperBorrador = new \App\Helpers\Borradores($db);
$helperRadicado = new \App\Helpers\Radicados($db);

require_once("$ruta_raiz/include/tx/Radicacion.php");
require_once("$ruta_raiz/include/tx/Tx.php");

$msg = "";
if(!isset($_SESSION['usuario_id'])) { header('Location: '.$ruta_raiz.'login.'.php);}


function convertBytes($value)
{
    if (is_numeric($value)) {
        return $value;
    } else {
        $value_length = strlen($value);
        $qty = substr($value, 0, $value_length - 1);
        $unit = strtolower(substr($value, $value_length - 1));
        switch ($unit) {
            case 'k':
                $qty *= 1024;
                break;
            case 'm':
                $qty *= 1048576;
                break;
            case 'g':
                $qty *= 1073741824;
                break;
        }
        return $qty;
    }
}

$maxFileSize = convertBytes(ini_get('upload_max_filesize'));
$maxFileSizeStr = ini_get('upload_max_filesize');

if (isset($_POST["dest"]) && $_POST["dest"] != "") {

//entra si estoy guardando el borrador

    if (!empty($_FILES) and $_FILES['userfile']['size'] != '0') {


        if ($_FILES['userfile']['size'] > $maxFileSize) {
            $errorsizefile = "El tamaño de algunos archivos excede el limite permitido: " . $maxFileSizeStr;
        } else {

            $ruta = "../../bodega";
            if (!is_dir("../../bodega")) {

            } else {
                if (!is_dir($ruta . "/" . date("Y"))) {

                    mkdir("$ruta", 0777);
                }
                $ruta .= "/" . date("Y");
                $dependencia = $_SESSION['dependencia'];
                if (!is_dir($ruta . "/" . $dependencia)) {

                    mkdir("$ruta", 0777);
                }
                $ruta .= "/" . $dependencia;
                if (!is_dir($ruta . "/docs")) {

                    $ruta .= "/docs";
                    mkdir("$ruta", 0777);
                }
                $ruta .= "/docs/";
                $userfile1_Temp = $_FILES['userfile']['tmp_name'];
                /* $name=date("YmdHis");

                 $random=rand(0, 9);
                 $name.=$random.".".$ext[count($ext)-1];*/
                $ext = explode(".", $_FILES['userfile']['name']);
                $random = rand(0, 9);

                $name = "p_" . $_POST['borrador_id'] . "_" . $_SESSION['usua_login'] . "_" . date('YmdHis') . $random . "." . $ext[count($ext) - 1];
                $name = strtolower($name);
                $bien2 = move_uploaded_file($userfile1_Temp, $ruta . $name);


                if ($bien2 == true) {
                    $_POST["documento_ruta"] = $ruta . $name;
                }
            }
        }

    }


    $retorno = $helperBorrador->save_all($_POST);

    $id_borrador = $retorno->fields['ID_BORRADOR'];
    $fecha_transaccion = $db->conn->OffsetDate(0, $db->conn->sysTimeStamp);
    $id_usuario_origen = $_SESSION["usuario_id"];
    $depe_codi_origen = $_SESSION['dependencia'];
    //busco la dependencia del usuario destino
    //guardo el historico y el comentario

    $msg = "Borrador Creado con Exito.";
    if ($_POST['borrador_id'] != "") {
        $msg = "Borrador " . $_POST['borrador_id'] . " Actualizado con Exito.";
    }


    //vengo de la vista de radicados.
    if ($_POST['radicadoactual'] != "") {

        if ($_POST['borrador_id'] == "") {
            $_POST['borrador_id'] = $retorno->fields['ID_BORRADOR'];
        }

        //esto es por si el radicado origen esta asociado a algun expediente, este borrador tambien.
        $expedientes = $helperBorrador->radiEstaEnExpediente($_POST['radicadoactual']);
        if (count($expedientes) > 0) {

            foreach ($expedientes as $item) {
                $expedientes = $helperBorrador->getExpedBorra($_POST['borrador_id'], $item['SGD_EXP_NUMERO']);

                if (!isset($expedientes[0])) {
                    $guardar = $helperBorrador->addExpeBorrador($_POST['borrador_id'], $item['SGD_EXP_NUMERO']);
                }
            }
        }


        //busco si ya fue asociado este radicado a este borrador
        $retorno = $helperBorrador->radicadoAsocByBorr($_POST['borrador_id'], $_POST['radicadoactual']);

        if (!isset($retorno[0])) {
            //si no existe ningun registro, lo guardo

            //busco el ultimo orden
            $response = $helperBorrador->getLastOrdenIndex($_POST['borrador_id']);
            $orden = 1;
            if ($response[0]['MAXIMO'] != "") {
                $orden = $response[0]['MAXIMO'] + 1;
            }

            $radicadocompleto = $helperBorrador->getRadicadoByNume_radi($_POST['radicadoactual']);

            $helperBorrador->insertBorrador_radicados_asociados($_POST['borrador_id'], $radicadocompleto[0]['RADI_NUME_RADI'], 1,
                $orden);


            //inserto un historial para el radicado origen
            $radicadoOrigen = $helperBorrador->getRadicadoByNume_radi($_POST['radicadoactual']);
            $noRad = $radicadoOrigen[0]['RADI_NUME_RADI'];
            $radicadoarray = Array();
            $radicadoarray[0] = $noRad;
            $Tx = new Tx($db);
            $a = $Tx->insertarHistorico($radicadoarray, $_SESSION['dependencia'], $_SESSION['codusuario'],
                $_SESSION['dependencia'], $_SESSION['codusuario'],
                "Se generó el borrador [".$_POST['borrador_id']."]: ",$_POST["asunto"], 80);

        }

    }

    $helperBorrador->guardarhistorico($id_borrador, $fecha_transaccion, "Ha sido creado un nuevo borrador", $id_usuario_origen,
        $depe_codi_origen, 0, 0, 1,80);


}



$radicadonumero = "";
$radicadoasunto='';
$radicado_has_exped = 0;
$usarioremitente = array();
$radicadocompleto=array();

/**
 * datos del borrador del radicado padre, si existe un radicado padre
 */
$borradorRadPadre=array();

//vengo desdela vista de radicados, pestana documentos
if ((isset($_GET['radicado']) && $_GET['radicado'] != "") ||
    (isset($_POST['radicadoactual']) && $_POST['radicadoactual'] != "")) {

    $radicadonumero = isset($_GET['radicado']) && $_GET['radicado'] != "" ? $_GET['radicado'] : $_POST['radicadoactual'];

    $borradorRadPadre= $helperBorrador->get_borradorByRadi($radicadonumero);

    $radicadocompleto = $helperBorrador->getRadicadoByNume_radi($radicadonumero);
    $radicadoasunto=$radicadocompleto[0]['RA_ASUN'];

    $msg = "";

    //esto es para saber si estoy desde radicar desde entrada, el radicado tiene expedientes.
    $expedientes = $helperBorrador->getExpedNotDeleted($_GET['radicado']);
    $radicado_has_exped = count($expedientes);


    //lo siguiente es para saber el remitente del radicado, para colocarlo por defecto en destinatario
    //al momento de crear el borradorgetexpedientes
    $remitente = $helperBorrador->getUserDelRadic($radicadonumero);


    if (isset($remitente[0]) && $remitente[0]['TABLA'] == "sgd_oem_oempresas") {
        $usarioremitente = $helperBorrador->getEmpresaForDest($remitente[0]['CODIGO'],$remitente[0]['SGD_DIR_DRECCIONES_ID']);
    } else if (isset($remitente[0]) && $remitente[0]['TABLA'] == "sgd_ciu_ciudadano") {
        $usarioremitente = $helperBorrador->getCiudadanoForDest($remitente[0]['CODIGO']);
    } else {
        //la tabla es: usuario
        $usarioremitente = $helperBorrador->getUserForDest($remitente[0]['CODIGO']);
    }

    ////////////////////////////////////////

} else {

}


//var_dump(array_search(array('ID_ESTADO'=>2, 'ID_USUARIO_ORIGEN'=>173), $arreglo[0]));

$rutaimagenes = "/" . $REPO_PATH . "/app/img/";

/*
$cantidad = $helperBorrador->borradoresAmiCargo();
foreach ($cantidad as $row) {
    $historico = array();
    $historico = $helperBorrador->getHistoricoBorrador($row['ID_BORRADOR']);

    if (count($historico) < 1) {

        $arrreglo = array(
            'borrador_id' => $row['ID_BORRADOR']
        );
        $borrar = $helperBorrador->borrarBorrador($arrreglo);
    }
}*/

$configHelper = new \App\Helpers\Configuraciones($db);
$configuraciones=$configHelper->getConfiguraciones(false,false,"'BORRADORES','SEGURIDAD','OP'");

$titulos=$configHelper->getTitulosTratamiento();

$jefes_dropdown_enviar=$helperBorrador->getDropdownEnviarA($_SESSION['dependencia']);

$borradorEstados=$helperBorrador->getBorradorEstados();

$ruta = "../../bodega";
$permisoEnBodega='SI';
if(!is_writable($ruta)){
    $permisoEnBodega='NO';
}




/*
 * esto por ahora no se va a hacer
$radValidados=$helperRadicado->getMetadataValidados();

if(count($radValidados)>0){
  foreach ($radValidados as $radvalidao){
      $buscarHash=$helperRadicado->getMetaDataHash($radvalidao['RADI_NUME_RADI']);
      var_dump($buscarHash[0]['TEXTO']);

  }
}
*/


echo $blade->view()->make('borradores.index', compact(
    'radicadonumero','radicadoasunto', 'rutaimagenes', 'maxFileSize', 'maxFileSizeStr',
    'include_path', 'radicado_has_exped', 'usarioremitente','configuraciones','titulos','jefes_dropdown_enviar',
    'msg','radicadocompleto','permisoEnBodega','borradorRadPadre','borradorEstados'

))->render();


?>