<?php

use App\Helpers\PDOconfig;
use Aura\SqlQuery\QueryFactory;
use Philo\Blade\Blade;

require '../../vendor/autoload.php';

$json = array();
session_start();




function terminarPdfRadicado($mpdf, $nameDocumRadi, $ruta)
{

    $totalpaginas = $mpdf->page;
    //genero el pdf del radicado, que es el que se va a guardar en radi_pacth
    $mpdf->Output($ruta . $nameDocumRadi);

    return array('totalpaginas' => $totalpaginas);

}

//trae un mes en espaol
function mesespanol($mes)
{
    if ($mes == "January") {
        $mes = "Enero";
    }
    if ($mes == "February") {
        $mes = "Febrero";
    }
    if ($mes == "March") {
        $mes = "Marzo";
    }
    if ($mes == "April") {
        $mes = "Abril";
    }
    if ($mes == "May") {
        $mes = "Mayo";
    }
    if ($mes == "June") {
        $mes = "Junio";
    }
    if ($mes == "July") {
        $mes = "Julio";
    }
    if ($mes == "August") {
        $mes = "Agosto";
    }
    if ($mes == "September") {
        $mes = "Septiembre";
    }
    if ($mes == "October") {
        $mes = "Octubre";
    }
    if ($mes == "November") {
        $mes = "Noviembre";
    }
    if ($mes == "December") {
        $mes = "Diciembre";
    }

    return $mes;
}


try {

    $json = get_borrador();

} catch (Exception $e) {
    echo "Error: " . $e->getMessage();
    $response = "ERROR";
    $json['error'] = true;
}
echo json_encode($json);


function get_borrador()
{
    try {

         ini_set('display_errors', 1);
         ini_set('display_startup_errors', 1);
         error_reporting(E_ALL);

        $ruta_raiz = "../../";
        $ruta = "../../bodega/tmp/";
        include("$ruta_raiz/config.php");

        if (isset($db)) unset($db);
        include_once("$ruta_raiz/include/db/ConnectionHandler.php");
        $db = new ConnectionHandler("$ruta_raiz");
        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        $borra = new \App\Helpers\Borradores($db);
        $configHelper = new \App\Helpers\Configuraciones($db);

        if (isset($_GET['borrador_id']) && $_GET['borrador_id'] != "") {

            $dest = $borra->get_borrador($_GET['borrador_id']);

            $titulomemo ='';
            if($dest[0]['TIPO_COMUNICACION']!=null && $dest[0]['TIPO_COMUNICACION']!=''){
                $titulomemo= $dest[0]['TIPO_COMUNICACION'];
            }

            if ($dest[0]['DOCUMENTO_FORMATO'] == "EditorWeb") {


                //el historico lo busco aqui porque voy a mostrar en el documento del radicado, quien aprobo reviso  proyecto
                $query = "SELECT  borrador_historicos.*, usuario.usua_login,usuario.usua_codi, usuario.usua_cargo
                FROM borrador_historicos  
                join borradores on (borradores.id_borrador=borrador_historicos.id_borrador)
               left join usuario on (usuario.id=borrador_historicos.id_usuario_origen)
                where borrador_historicos.id_borrador=" . $_GET['borrador_id'] . " and comentario_borrador!=''
                ORDER BY borrador_historicos.fecha_transaccion ASC";


                $historico = $db->conn->selectLimit($query)->getArray();

                ///////guardo la direccion, y busco los datos en las tablas, dependiendo del tipo de radicado
                $datosDestinatario = $borra->datosDestinatario($dest, $db);

                //creo la imagen con el codigo de barra
                $borrador = $dest;
                $firmantes = $borra->getFirmByBorrador($_GET['borrador_id']);
                $imprimefirmantes=''; //si declaro vacio, quiere decir que va a hacer las validaciones correspondientes

                if ($_GET['exportar'] == "pdf"){
                $mpdf = array();
                $mpdf = $borra->armarPdfRadicado('borrador',$borrador[0]['ID_BORRADOR'],$borrador, $datosDestinatario,$firmantes, $titulomemo,
                    false,$imprimefirmantes,false);

                $mpdf=$borra->armarAprobRevisProy($mpdf, $historico, $borra, 'mpdf',
                array(), $configHelper);



                /**
                 * las 2 siguientes expresiones, arman los anexos folios y folios comunicacion
                 */

                $mpdf = $borra->armarAnexosFolInfo($mpdf, $name_library = 'mpdf', $borrador,$borra);
                /************************************************
                */
                }elseif ($_GET['exportar'] == "docx" || $_GET['exportar'] == "doc"){
                    $section = array();
                    $phpWord = new \PhpOffice\PhpWord\PhpWord();
                    \PhpOffice\PhpWord\Settings::setZipClass(\PhpOffice\PhpWord\Settings::PCLZIP);
                    $phpWord->setDefaultParagraphStyle(
                        array(
                            //'alignment' => \PhpOffice\PhpWord\SimpleType\Jc::LEFT,
                            'spaceAfter' => \PhpOffice\PhpWord\Shared\Converter::pointToTwip(3),
                            'spacing' => 10,
                            'lineHeight' => 1,
                            'align' => 'both',
                            'space' => 1
                        )
                    );
                    $phpWord->setDefaultFontSize($_SESSION['EDITOR_FUENTE_TAMANIO'] != 'SELECCIONE' ? $_SESSION['EDITOR_FUENTE_TAMANIO'] : 10);
                    $phpWord->setDefaultFontName($_SESSION['EDITOR_FUENTE_TIPO'] != 'SELECCIONE' ? $_SESSION['EDITOR_FUENTE_TIPO'] : 'Arial');


                    $section = $borra->armarDocxRadicado('borrador',$borrador[0]['ID_BORRADOR'], $borrador, $datosDestinatario,
                        $phpWord, $firmantes, $titulomemo, $imprimefirmantes );

                    $section = $borra->armarAprobRevisProy($section, $historico, $borra, 'phpword',
                        array(), $configHelper);

                    $section = $borra->armarAnexosFolInfo($section, $name_library = 'phpword', $borrador, $borra);
                }

                $nameDocumRadi = "p_" . $_GET['borrador_id'] . "_" . str_replace(".", "_", $_SESSION['usua_login']) . "_" . date('YmdHis') ;
                $name="Borrador";

                if ($_GET['exportar'] == "docx" || $_GET['exportar'] == "doc") {
                    $name = $name. date('s').'.'.$_GET['exportar'];
                    $guardarPdf = $borra->terminarDocxRadicado('borrador',$section,$borrador[0]['ID_BORRADOR'], $ruta,$name, $phpWord,false);
                    $ruta .= $name;

                    $filedatatype = $_GET['exportar'];
                } elseif ($_GET['exportar'] == "pdf") {
                    $name = $name. date('s').'.pdf';
                    $guardarPdf = terminarPdfRadicado($mpdf, $name, $ruta);
                    $ruta .= $name;
                    $filedatatype = "pdf";
                }

            } else {
                $daa = explode("/", $dest[0]['DOCUMENTO_RUTA']);
                $name = $daa[count($daa) - 1];
                $ruta = $dest[0]['DOCUMENTO_RUTA'];
                $anexo_tipo = pathinfo($dest[0]['DOCUMENTO_RUTA']);
                $anexo_tipo = $anexo_tipo['extension'];
                $filedatatype = $anexo_tipo;
            }

            ///////
            if (isset($_GET['exportar']) && ($_GET['exportar'] != "pdf" && $_GET['exportar'] != "PDF")) {
                // header('Content-Description: File Transfer');
                switch ($filedatatype) {
                    case 'odt':
                        header('Content-Type: application/vnd.oasis.opendocument.text');
                        break;
                    case 'docx':
                        header('Content-Type: application/msword');
                        break;
                    case 'doc':
                        header('Content-Type: application/msword');
                        break;
                    case 'tif':
                        header('Content-Type: image/TIFF');
                        break;
                    case 'pdf':
                        header('Content-Type: application/pdf');
                        break;
                    case 'xls':
                        header('Content-Type: application/vnd.ms-excel');
                        break;
                    case 'csv':
                        header('Content-Type: application/vnd.ms-excel');
                        break;
                    case 'ods':
                        header('Content-Type: application/vnd.ms-excel');
                        break;
                    case 'html':
                        header('Content-Type: text/html');
                        break;
                    default :
                        header('Content-Type: application/octet-stream');
                        break;
                }
                header('Content-Disposition: inline; filename=' . $name);

                //ob_clean();
                //flush();
                readfile($ruta);
                exit;
            } else {
                $json['success'] = $ruta;
            }


        } else {
            $json['error'] = "Debe ingresar un borrador";
        }
        return $json;

    } catch (\Exception $e) {
        return $e->getMessage();
    }

}

?>

