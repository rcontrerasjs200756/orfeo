<?php

use App\Helpers\PDOconfig;
use Aura\SqlQuery\QueryFactory;
use Philo\Blade\Blade;
require '../../vendor/autoload.php';

$json=array();
session_start();
ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
error_reporting(E_ALL);

//archivo que guarda los cargos para los firmantes, en borradores

try {

    if(!isset($_SESSION["usuario_id"])){

        $json=array(
            'error'=>'expiro'
        );
        echo json_encode($json);
        exit;
    }

    require_once '../../config.php';

    $ruta_raiz = "../../";
    if (isset($db)) unset($db);
    include_once("$ruta_raiz/include/db/ConnectionHandlerNew.php");
    $db = new ConnectionHandler("$ruta_raiz");
    $borra     = new \App\Helpers\Borradores($db);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    $fecha_transaccion = $db->conn->OffsetDate(0, $db->conn->sysTimeStamp);
    if(isset($_POST['pk']) && $_POST['pk']!="" ) {

        $buscar=$borra->getFirmByBorAndUser($_POST['name'],$_POST['pk']);

        if(count($buscar)>0){
            $borra->updateFirmante($_POST['name'], $_POST['pk'], $_POST['value']);
        }else{
            $borra->guardarFirmantes($_POST['name'], $fecha_transaccion, $_POST['pk'], $_POST['value'],
                0,'');
        }
        $json['success']=true;
        $json['firmante_id']=$_POST['pk'];
    }else{
        $json['error']="Debe ingresar todos los datos";
    }

}catch(Exception $e){
    echo "Error: ".$e->getMessage();
    $response= "ERROR";
    $json['error']=true;
}
echo json_encode($json);



?>

