<?php

use App\Helpers\PDOconfig;
use Aura\SqlQuery\QueryFactory;
use Philo\Blade\Blade;
require '../../vendor/autoload.php';

$json=array();
session_start();
ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
error_reporting(E_ALL);


function convertBytes( $value ) {
    if ( is_numeric( $value ) ) {
        return $value;
    } else {
        $value_length = strlen($value);
        $qty = substr( $value, 0, $value_length - 1 );
        $unit = strtolower( substr( $value, $value_length - 1 ) );
        switch ( $unit ) {
            case 'k':
                $qty *= 1024;
                break;
            case 'm':
                $qty *= 1048576;
                break;
            case 'g':
                $qty *= 1073741824;
                break;
        }
        return $qty;
    }
}


try {

    if(!isset($_SESSION["usuario_id"])){

        $json=array(
            'error'=>'expiro'
        );
        echo json_encode($json);
        exit;
    }

    require '../../vendor/autoload.php';
    require_once '../../config.php';

    $ruta_raiz = "../../";
    if (isset($db)) unset($db);
    include_once("$ruta_raiz/include/db/ConnectionHandlerNew.php");
    $db = new ConnectionHandler("$ruta_raiz");
    $borra     = new \App\Helpers\Borradores($db);
    $maxFileSizeStr=ini_get('upload_max_filesize');


    $root = $_SERVER['HTTP_ORIGIN'];
    $base = explode("/",$BASE_PATH);
    $base=$root."/".$base[count($base)-1];
    if($_POST['id_borrador_anexo'] != "") {

        if (!empty($_FILES) and count($_FILES['files'])>0) {

            $ruta="../../bodega";
            if(!is_dir("../../bodega"))
            {
                $json['error']="Error";
            }
            else
            {
                $rutaPrincipal="/bodega";
                $ruta.="/borradores";

                if(!is_dir($ruta)){

                    mkdir("$ruta",0777);
                }
                $ruta.="/";

               /* if(!is_dir($ruta."/".date("Y"))){

                    mkdir("$ruta",0777);
                }
                $ruta.="/".date("Y");
                $rutaPrincipal.="/".date("Y");
                $dependencia=$_SESSION['dependencia'];
                if(!is_dir($ruta."/".$dependencia)){

                    mkdir("$ruta",0777);
                }
                $ruta.="/".$dependencia;
                $rutaPrincipal.="/".$dependencia;
                if(!is_dir($ruta."/docs")){

                    $ruta.="/docs";
                    mkdir("$ruta",0777);
                }
                $ruta.="/docs/";*/
                $rutaPrincipal.="../docs/";

                $bien2=false;
                $contador=0;
                $maxFileSize = convertBytes(ini_get('upload_max_filesize'));

                $cantidadFiles=count($_FILES);


                $filejson = new stdClass;

                $conterror=0;
                for ($i=0; $i<count($_FILES['files']['name']); $i++) {

                    $fileArray=array();


                    if($_FILES['files']['size'][$i]>$maxFileSize){
                        $json['error']="El tamaño de algunos archivos excede el limite permitido: ".$maxFileSizeStr;
                        $json['conterror'][$conterror]=$_FILES['files']['name'][$i];
                        $conterror++;
                        continue;
                    }

                    $userfile1_Temp = $_FILES['files']['tmp_name'][$i];
                    $anexo_tipo = pathinfo($_FILES['files']['name'][$i]);
                    $anexo_tipo = $anexo_tipo['extension'];

                    $ext= strtolower($anexo_tipo);

                    $codusuario = $_SESSION["usuario_id"];

                    /**
                     * a la fecha, le concateno los microsegundos
                     */
                    $fechaahora=date('YmdHis').str_replace('.','',microtime(true));
                    $name = "a_" . $_POST['id_borrador_anexo'] . "_" . str_replace(".", "_", $_SESSION['usua_login']) . "_" . $fechaahora  . "." . $ext;

                    $nombreOriginal=$_FILES['files']['name'][$i];


                    $infofile=pathinfo($nombreOriginal);

                    $nombreOriginal = preg_replace('/[^A-Za-z0-9ñÑñÑÁÉÍÓÚáéíóú\s-_]/iu', ' ', $infofile['filename']);
                    $nombreOriginal = strtolower($nombreOriginal).'.'.$infofile['extension'];

                    $bien2 = move_uploaded_file($userfile1_Temp, $ruta . $name);

                    //si es una imagen le hago un thumbnail
                    if($ext=="jpg" || $ext=="jpeg" || $ext=="png" || $ext=="gif" ) {
                        //thumbnail
                        $file = $ruta . $name;
                        $width = 80;

// Ponemos el . antes del nombre del archivo porque estamos considerando que la ruta está a partir del archivo thumb.php
                        $file_info = getimagesize($file);
// Obtenemos la relación de aspecto
                        $ratio = $file_info[0] / $file_info[1];

// Calculamos las nuevas dimensiones
                        $newwidth = $width;
                        $newheight = round($newwidth / $ratio);

// Sacamos la extensión del archivo

                        $nombrethumnail="";
                        if ($ext == "jpeg") $ext = "jpg";

// Dependiendo de la extensión llamamos a distintas funciones
                        switch ($ext) {
                            case "jpg":
                                $img = imagecreatefromjpeg($file);
                                break;
                            case "png":
                                $img = imagecreatefrompng($file);
                            case "gif":
                                $img = imagecreatefromgif($file);
                                break;
                        }


// Creamos la miniatura
                        $thumb = imagecreatetruecolor($newwidth, $newheight);
// La redimensionamos
                        imagecopyresampled($thumb, $img, 0, 0, 0, 0, $newwidth, $newheight, $file_info[0], $file_info[1]);
// La mostramos como jpg

                        $rutathumnail = $ruta . $name . ".80x80." . $ext;

                        $nombrethumnail =  $name . ".80x80." . $ext;
                        imagejpeg($thumb, $rutathumnail, 80);
                    }

                    if ($bien2==true) {
                        $codusuario = $_SESSION["usuario_id"];
                        $dependencia = $_SESSION['dependencia'];
                        $response = $borra->getLastAnexBorra($codusuario, $_POST['id_borrador_anexo'], $dependencia);
                        $cont = 2;
                        if (count($response) > 0) {
                            $cont = $response[0]['ANEXO_ORDEN'] + 1;
                        }

                        //GUARDO LOS DATOS EN LA TABLA
                       $anexo_id= $borra->saveAnexoBorrador($_POST['id_borrador_anexo'], $ruta . $name, $nombreOriginal,
                            $cont, $codusuario, $dependencia);


                        $fileArray=array(
                            'url'  => $base."/bodega/borradores/".$name,
                            'thumbnailUrl' => $base."/bodega/borradores/".$nombrethumnail,
                            'name' => $nombreOriginal,
                            'type' => $_FILES['files']['type'][$i],
                            'size' => $_FILES['files']['size'][$i],
                            'delete_url'   => $base."/bodega/borradores/".$name,
                            'delete_type' => 'DELETE', // method for destroy action
                            'anexo_id'=>$anexo_id
                        );


                        $filejson->files[] = $fileArray;

                    }

                }

                if($bien2==true && !isset($json['conterror'])){

                    $json=$filejson;

                }else{
                    $json['error']="Ha ocurrido un error al guardar el anexo";
                }

                //$json['anexos']=$borra->getAnexosBorrador($_POST['id_borrador_anexo']);

            }

        }else{
            $json['error']="Debe ingresar un archivo";
        }

    }else{
        $json['error']="El tamaño de algunos archivos excede el limite permitido: ".$maxFileSizeStr;
    }

}catch(Exception $e){
    echo "Error: ".$e->getMessage();
    $response= "ERROR";
    $json['error']=true;
}
echo json_encode($json);



?>

