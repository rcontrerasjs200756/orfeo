<?php

use App\Helpers\PDOconfig;
use Aura\SqlQuery\QueryFactory;
use Philo\Blade\Blade;
require '../../vendor/autoload.php';

$json=array();
session_start();

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//este archivo busca los radicados para asociarlos
try{
    require_once '../../config.php';

    $borra     = new \App\Helpers\Borradores();

    $radicados  =  $borra->buscarRadicadoHelper(trim(str_replace("'","",$_GET['search'])));
    $radicadoenviar=array();
    $cont=0;

    foreach ($radicados as $radicado){
        $radicadoenviar[$cont]['id']=$radicado['RADI_NUME_RADI'];
        $radicadoenviar[$cont]['text']=$radicado['RADI_NUME_RADI'].", ".$radicado['RA_ASUN'].", ".date('Y-m-d H:i',strtotime($radicado['RADI_FECH_RADI']));
        $radicadoenviar[$cont]['numeradi']=$radicado['RADI_NUME_RADI'];
        $radicadoenviar[$cont]['radi_depe_actu']=$radicado['RADI_DEPE_ACTU'];

        $cont++;
    }
    // var_dump($usuarios);
    echo json_encode($radicadoenviar);
}catch(\Exception $e){
    return $e->getMessage();
}


?>

