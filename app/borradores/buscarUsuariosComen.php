<?php

use App\Helpers\PDOconfig;
use Aura\SqlQuery\QueryFactory;
use Philo\Blade\Blade;

require '../../vendor/autoload.php';

$json = array();
session_start();

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
try {
    require_once '../../config.php';
    $ruta_raiz = "../../";
    if (isset($db)) unset($db);
    include_once("$ruta_raiz/include/db/ConnectionHandlerNew.php");
    $db = new ConnectionHandler("$ruta_raiz");
    $firmantes = false;
    $borra = new \App\Helpers\Borradores($db);
    $radicadoHelper =  new \App\Helpers\Radicados($db);


    /**
     * para el caso de $_SESSION['REPORTE4'], se quier eque tambien aparezca el usuario logueado
     */
    $buscar_otros_users=false;
    if(isset($_GET['optSelectedNewBorrador']) && $_GET['optSelectedNewBorrador']==$_SESSION['REPORTE4']):
        $buscar_otros_users=$_SESSION['usuario_id'];
    endif;

    if (isset($_GET['traerfirmantes']) && $_GET['traerfirmantes'] == 'SI') {

        if (isset($_GET['borrador']) && $_GET['borrador'] != '') {
            $borrador = $_GET['borrador'];

            $buscarfirmantes = $borra->getOnlyFirmnBorrador($borrador);

            if (count($buscarfirmantes) > 0) {
                $firmantes = '';
                foreach ($buscarfirmantes as $row) {

                    /**
                     * hago la siguiente validacion, porque si existe optSelectedNewBorrador y == REPORTE4,
                     * y el usuario que esta logueado esta entre los firmantes, entonces no debo repetirlo.
                     */
                    if(
                        $buscar_otros_users!==false &&
                        $buscar_otros_users!=$row['ID_USUARIO_FIRMANTE']
                    ){
                        $firmantes .= $row['ID_USUARIO_FIRMANTE'] . ",";
                    }

                }
                $firmantes = substr($firmantes, 0, strlen($firmantes) - 1);
            }
        }
    }

    $usuarioeenviar = array();

    $buscar='';
    if(isset($_GET['search'])){
        $buscar=$_GET['search'];
    }

    /**
     * si por GET, me llega un arreglo de usuarios que tambien debo buscar, entonces los recorro y los armo
     */
    if (isset($_GET['buscar_otros_users']) && count($_GET['buscar_otros_users'])>0) {
        if($buscar_otros_users!=false):
            $buscar_otros_users.=',';
        endif;

        foreach ($_GET['buscar_otros_users'] as $otheruser){
            $buscar_otros_users.=$otheruser . ",";
        }
        /**
         * lo siguiente lo hago para quitarle la ultima coma(,) al string
         */
        $buscar_otros_users = substr($buscar_otros_users, 0, strlen($buscar_otros_users) - 1);

    }

    $usuarios = $radicadoHelper->getUsersWithPermAprob($buscar, $firmantes,$buscar_otros_users);



    if($buscar_otros_users!==false){
        $usersadicionales = $borra->userAdicionalSelectEnviar($buscar_otros_users);
        $usuarios = array_merge($usuarios, $usersadicionales);
    }

    $usuarioeenviar = $borra->barmarUsuariosComen($usuarios);

    echo json_encode($usuarioeenviar);
} catch (\Exception $e) {
    return $e->getMessage();
}


?>

