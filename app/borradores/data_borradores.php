<?php

use App\Helpers\PDOconfig;
use Aura\SqlQuery\QueryFactory;
use Philo\Blade\Blade;

require '../../vendor/autoload.php';

$array = array();

try {
    session_start();
    require_once '../../config.php';
    $views = $ABSOL_PATH . 'app/views';
    $cache = $ABSOL_PATH . 'app/cache';

    $borradoresC = new \App\Helpers\Borradores();

    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    $data = $_GET['data'];
    $datajson = array();
    // Pagination Result
    $array = array();
    $start = 0;
    $limit = false;
    $draw = $_GET['draw'];

    if (!empty($draw)) {
        $start = $_GET['start'];
        $limit = $_GET['length'];
        if ($limit == '-1') {
            $limit = false;
        }
    }
    $search = $_GET['search'];


    $buscar = $search['value'];

    $ordenar = $_GET['order'];
    $order = false;
    $order_dir = 'desc';
    if (!empty($ordenar)) {
        $order_dir = $ordenar[0]['dir'];
        if ($ordenar[0]['column'] == 0) {
            $order = 'b.id_borrador';
        }
        if ($ordenar[0]['column'] == 1) {
            $order = 'b.asunto';
        }
        if ($ordenar[0]['column'] == 2) {
            $order = 'borrador_estados.nombre_estado';
        }
        if ($ordenar[0]['column'] == 3) {
            $order = 'b.destinatario';
        }
        if ($ordenar[0]['column'] == 4) {
            $order = 'usuario.usua_login';
        }

    }
    $estado=$borradoresC->getBorradorEstadoByNombre('Borrador Eliminado');
    $estadoeliminado='';
    if(count($estado)>0){
        $estadoeliminado=$estado[0]['ID_BORRADOR_ESTADO'];
    }

    $estadoComentario = $borradoresC->getBorradorEstadoByCodigo(87);
    $estadocomentado='';
    if(count($estadoComentario)>0){
        $estadocomentado=$estadoComentario[0]['ID_BORRADOR_ESTADO'];
    }
//vengo desdela vista de radicados, pestana documentos
    if (isset($data['radicadoactual']) && $data['radicadoactual'] != "") {
        $radicadonumero = isset($_GET['radicado']) && $_GET['radicado'] != "" ? $_GET['radicado'] : $data['radicadoactual'];
        $radicadocompleto = $borradoresC->getRadicadoByNume_radi($radicadonumero);

        $total = $borradoresC->get_all($buscar,false, false,false,false,
            $radicadocompleto[0]['RADI_NUME_RADI'],$estadoeliminado,$estadocomentado);
        $borradores = $borradoresC->get_all($buscar,$limit, $start,$order,$order_dir,
            $radicadocompleto[0]['RADI_NUME_RADI'],$estadoeliminado,$estadocomentado);
    } else {


        $total = $borradoresC->get_all($buscar,false, false,false,false,false,
            $estadoeliminado,$estadocomentado);
        $borradores = $borradoresC->get_all($buscar,$limit, $start,$order,$order_dir,false,
            $estadoeliminado,$estadocomentado);
    }

    $estadoComentario=$borradoresC->getBorradorEstadoByCodigo(87);

    $cont = 0;
    foreach ($borradores as $key => $row) {

        $historial = $borradoresC->get_comentarios($row["ID_BORRADOR"]);

        $row['RADICADO_ORIGEN']=false;
        $radOrigen=$borradoresC->getRadicadoOrigen($row["ID_BORRADOR"]);

        if(count($radOrigen)>0){
            $row['RADICADO_ORIGEN']=$radOrigen;
        }

        $consigioAprobado = false;
        $consigioRevisado = false;
        $cont_comentarios=0;
        $otroUserComento=false;
        foreach ($historial as $row2) {

            if ($row2["ID_ESTADO_BORRADOR"] == 2) {

                $consigioAprobado = true;
            }

            if ($row2["ID_ESTADO_BORRADOR"] == 3) {

                $consigioRevisado = true;
            }

            //aqui pregunto si tiene comentarios el borrador.
            if(isset($estadoComentario[0]['ID_BORRADOR_ESTADO'])
                && $row2["ID_ESTADO_BORRADOR"]==$estadoComentario[0]['ID_BORRADOR_ESTADO']){
                $cont_comentarios++;
                //aqui pregunto si un usuario distinto al mio, realizó un comentario
                if($row2["ID_USUARIO_ORIGEN"]!= $_SESSION["usuario_id"]) {
                    $otroUserComento=true;
                }
            }
        }


        $CONTRATISTA_json = array();
        $con = 0;

        $CONTRATISTA_json[$con] = '<p>'.$row["ID_BORRADOR"];
        if($row["ID_USUARIO_ACTUAL"] == $_SESSION["usuario_id"]) {
            $CONTRATISTA_json[$con] .=  '<i class="fa fa-male" data-toggle="tooltip"
                                                 title="Yo soy el usuario actual"></i>';
        }
        if($cont_comentarios>0) {
            $CONTRATISTA_json[$con] .=  '<i class="fa fa-commenting" data-toggle="tooltip"  ';
            if($otroUserComento){  $CONTRATISTA_json[$con] .=  ' style="color: green"';  }
            $CONTRATISTA_json[$con] .=  ' title="Tiene Comentario"></i>';
        }


        $CONTRATISTA_json[$con] .=  '</p>
                                        <p>';



        if(
            $row["TIPO_BORRADOR_DEST"] == $_SESSION['INTERNA_SIN_PLANTILLA']
            &&
            $row["DOCUMENTO_RUTA"] != "" ) {
            $CONTRATISTA_json[$con] .=  '<i class="fa fa-file-pdf-o" data-toggle="tooltip" 
title="Pdf"></i>';
        }


        if($row["TIPO_BORRADOR_DEST"] == $_SESSION['INTERNA_CON_PLANTILLA'] &&
            $row["DOCUMENTO_RUTA"] != "" && $row["DOCUMENTO_FORMATO"] == "Plantilla") {
            $CONTRATISTA_json[$con] .=  '<i class="fa fa-file-excel-o" 
data-toggle="tooltip" title="Word"></i>';
        }

        if(($row["TIPO_BORRADOR_DEST"] == $_SESSION['INTERNA_COMUNICACION'] ||
                $row["TIPO_BORRADOR_DEST"] == $_SESSION['SALIDA_OFICIO']) &&
            $row["DOCUMENTO_FORMATO"] == "Plantilla") {
            $CONTRATISTA_json[$con] .=  '<i class="fa fa-file-word-o" data-toggle="tooltip" 
                                            title="Word"></i>';
        }

        if(
            (
                $row["TIPO_BORRADOR_DEST"] == $_SESSION['INTERNA_COMUNICACION']
                ||
                $row["TIPO_BORRADOR_DEST"] == $_SESSION['SALIDA_OFICIO']
            )
            &&
            $row["DOCUMENTO_FORMATO"] == "EditorWeb") {
            $CONTRATISTA_json[$con] .=  ' <i class="fa fa-edit"></i>';
        }

        if($row["RADICADO_ORIGEN"] != false) {
            $CONTRATISTA_json[$con] .=  '  &nbsp;<span class="glyphicon glyphicon-paperclip"
                                                        style="font-size: 14px;color: black !important; "
                                                        title="Radicado asociado como respuesta o alcance"> </span> ';

        }
        $CONTRATISTA_json[$con] .=  '</p>';
        $con++;

        $tipo_borrador_dest="'".$row["TIPO_BORRADOR_DEST"]."'";
        $CONTRATISTA_json[$con] = '<a href="#" onclick="getBorrador('.$row["ID_BORRADOR"].','.$tipo_borrador_dest.')">
                                            '.$row["ASUNTO"].'</a>';
        $con++;



        if ($consigioAprobado == true && $consigioRevisado == true) {
            $row['NOMBRE_ESTADO'] = "Revisado y Aprobado";
        } elseif ($borradores[$cont]['ID_ESTADO'] == 3 && $consigioAprobado == true) {
            //si el estado actual es revisado, pero ya habia sido aprobado, pasa a Aprobado
            $row['NOMBRE_ESTADO'] = "Aprobado";
        }

        $clase = "yellow";

        if ($row["NOMBRE_ESTADO"] == "Aprobado") {
            $clase = "blue";
        }

        if ($row["NOMBRE_ESTADO"] == "Revisado y Aprobado") {
            $clase = "blue";
        }

        if ($row["NOMBRE_ESTADO"] == "Devuelto" || $row["NOMBRE_ESTADO"] == "No Aprobado") {
            $clase = "red";
        }

        if ($row["NOMBRE_ESTADO"] == "Revisado") {
            $clase = "green";
        }

        $daa = explode("/", $row["DESTINATARIO"]);

        $tipo_borrador_dest="'".$row["TIPO_BORRADOR_DEST"]."'";
        $CONTRATISTA_json[$con] = '<a href="#" class="">
                                                <span class="botonestabla btn label label-sm label-info '.$clase.'"
                                                      onclick="getBorrador('.$row["ID_BORRADOR"].','.$tipo_borrador_dest.')">
                                                    '.$row["NOMBRE_ESTADO"].' </span></a>';
        $con++;

        $CONTRATISTA_json[$con] = isset($daa[1])?$daa[1]:$row["DESTINATARIO"];
        $con++;

        $CONTRATISTA_json[$con] = $row["USUA_LOGIN"];
        $con++;
        $CONTRATISTA_json['DT_RowId'] = $row["ID_BORRADOR"];

        $datajson[] = $CONTRATISTA_json;
    }

    $array['data'] = $datajson;
    $array['draw'] = $draw;//esto debe venir por post
    $array['recordsTotal'] =count($total);
    $array['recordsFiltered'] = count($total);

} catch (Exception $e) {
    echo "Error: " . $e->getMessage();
    $response = "ERROR";
    $array['error'] = true;
}
echo json_encode($array);


?>

