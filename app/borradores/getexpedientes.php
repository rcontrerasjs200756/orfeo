<?php

use App\Helpers\PDOconfig;
use Aura\SqlQuery\QueryFactory;
use Philo\Blade\Blade;
require '../../vendor/autoload.php';

$json = array();
if(!isset($_SESSION))
{
    session_start();
}
try {


    if ($_GET['key'] != "") {
        $response = get_expedientes($_GET);
        $json = $response;

    }

} catch (Exception $e) {
    echo "Error: " . $e->getMessage();
    $response = "ERROR";
    $json['error'] = true;
}
echo json_encode($json);


function get_expedientes($vall)
{
    try {

        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);

        require_once '../../config.php';
        $ruta_raiz = "../../";
        if (isset($db)) unset($db);
        include_once("$ruta_raiz/include/db/ConnectionHandlerNew.php");
        $db = new ConnectionHandler("$ruta_raiz");
        $configsHelper = new \App\Helpers\Configuraciones($db);
        $limite=$configsHelper->getConfiguraciones("'LIMIT_BUSQ_AVANZ_EXPED'");
        $limit=$limite[0]['VALOR']+0;

        $borra = new \App\Helpers\Borradores($db);
        $dependencia=false;
        $buscar=trim(str_replace("'","",$vall['key']));

        if(isset($vall['dependencia']) && $vall['dependencia']!=""){
            $dependencia="'".$vall['dependencia']."'";
            $dependencia.=",'".$_SESSION["depecodi"]."'";
            $dependencia.=",'".$_SESSION["depe_codi_padre"]."'";
        }

        //Si el usuario busca un número de expediente completo, mostrarlo en búsqueda rápida sin importar a cuál dependencia pertenece
        // Propiedades de un número de expediente: Siempre termina en E mayúscula y empieza por 2*, es de ancho fijo de 19 caracteres:  201943001500200001E
        if(strlen($buscar)==19 && substr($buscar, 0,1)==2 &&
            (substr($buscar, strlen($buscar)-1,strlen($buscar))=='E' ||
                substr($buscar, strlen($buscar)-1,strlen($buscar))=='e' )
        ){
            $dependencia=false;
        }


        $menor_dos_anios=true;
        $soloabiertos=true;
        if(isset($vall['buscartodo']) && $vall['buscartodo']=="true"){

            $menor_dos_anios=false;
            $soloabiertos=false;
        }

        $expedientes = $borra->get_expedientes(trim(str_replace("'","",$vall['key'])),false,
            $dependencia, false, false, false, $menor_dos_anios,$limit,$soloabiertos);

        $json=array();
        $json=$expedientes;

        return $json;
    } catch (\Exception $e) {
        return $e->getMessage();
    }

}

?>

