<?php

use App\Helpers\PDOconfig;
use Aura\SqlQuery\QueryFactory;
use Philo\Blade\Blade;

require '../../vendor/autoload.php';
if (!isset($_SESSION)) {
    session_start();
}
try {

    if ($_GET['operation'] == "get_all") {
        $response = get_all();
        echo $response;
        exit;
    }
    if ($_GET['operation'] == "get_dest") {
        $response = get_dest($_GET);
        echo json_encode($response);
        exit;
    }

} catch (Exception $e) {
    echo "Error: " . $e->getMessage();
    $response = "ERROR";
}


function get_dest($post)
{

    try {
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        require_once '../../config.php';
        $helperBorra = new \App\Helpers\Borradores();

        $destinatario=false;

        if(isset($_GET['user_to_search']) && $_GET['user_to_search']!=''){
            $destinatario=$_GET['user_to_search'];
        }

        if(isset($_GET['borrador_id']) && $_GET['borrador_id']!=''){
            $borrador=$helperBorra->get_borrador($_GET['borrador_id']);
            $destinatario=$borrador[0]['ID_DESTINATARIO'];
        }
        $textoabuscar = isset($_GET['search'])?$_GET['search']:'';
        $opcionselected = $post['optSelectedNewBorrador'];
        $customB = $post['customB'];

        /**
         * aqui busco los destinatarios. OJO que abajo pregunto si ya habia un destinatario,
         * si es asi, lo busco, y lo agrego a $dest
         */
        $dest = $helperBorra->get_dest(trim($textoabuscar), $opcionselected,$destinatario , $customB);

        if($destinatario!=false && $opcionselected == $_SESSION['SALIDA_OFICIO']){
            /**
             * entra aqui, si estoy editanto un borrador, entonces busco el destinatario que ya tenia guardado
             * y lo agrego al arreglo de arriba "$dest"
             */
            $dest_especific=$helperBorra->getDestEspecificSalida($destinatario);
            $dest=array_merge($dest,$dest_especific);
        }
        $res = "";
        $cont = 0;
        $usuarioeenviar = array();
        foreach ($dest as $de) {


            $res = $de["TIPO_RADICADO"] . " " . $de["NOMBRE"] . "<br>" . $de["IDENTIFICACION"] . " " . $de["DIRECCION"];
            if (isset($de["LOGIN"])) {
                $res .= " /" . $de["LOGIN"];
            }
            $res .= " " .$de["EMAIL"];

            if($opcionselected!=$_SESSION['SALIDA_OFICIO']){
                $res=$de["NOMBRE"].' ('.$de["USUA_CARGO"]. ' - '.$de["DEPE_NOMB"].')';
            }

            $usuarioeenviar[$cont] = $de;
            $usuarioeenviar[$cont]['id'] = $de["ID"].'-'.$de["TIPO_DESTINATARIO"];
            $usuarioeenviar[$cont]['text'] = $res;
            $cont++;

        }
        return $usuarioeenviar;
    } catch (\Exception $e) {
        return $e->getMessage();
    }

}

function get_all()
{
    try {
        require_once '../../config.php';
        $views = $ABSOL_PATH . 'app/views';
        $cache = $ABSOL_PATH . 'app/cache';

        //Clearing the views cache
        \App\Helpers\MiscFunctions::clear_view_cache();

        $blade = new Blade($views, $cache);

        $borr = new \App\Helpers\Borradores();
        $borradores = $borr->get_all();

        return $blade->view()->make('borradores.partials.all_table',
            compact('borradores')
        )->render();

    } catch (\Exception $e) {
        return $e->getMessage();
    }
}

?>

