<?php

use App\Helpers\PDOconfig;
use Aura\SqlQuery\QueryFactory;
use Philo\Blade\Blade;
require '../../vendor/autoload.php';

$json = array();


try {

    if ($_GET['function_call'] != "") {

        if($_GET['function_call']=="todas_tipologias"){
            $response = todas_tipologias($_GET['key']);
        }

        if($_GET['function_call']=="tipolByExpedAndRad"){

            $response = tipolByExpedAndRad();
        }

        if($_GET['function_call']=="tipolByExped"){

            $response = tipolByExped();
        }


        $json = $response;

    }

} catch (Exception $e) {
    echo "Error: " . $e->getMessage();
    $response = "ERROR";
    $json['error'] = true;
}
echo json_encode($json);


function todas_tipologias($vall)
{
    try {

        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);

        require_once '../../config.php';

        $borra = new \App\Helpers\Borradores();

       // var_dump($_GET['term']);
        $expedientes = $borra->get_tipologias(trim(str_replace("'","",$vall)));

        $json=array();
        $json=$expedientes;
        return $json;
    } catch (\Exception $e) {
        return $e->getMessage();
    }

}

function tipolByExped(){

    try {

        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);

        require_once '../../config.php';

        $ruta_raiz = "../../";
        if (isset($db)) unset($db);
        include_once("$ruta_raiz/include/db/ConnectionHandlerNew.php");
        $db = new ConnectionHandler("$ruta_raiz");
        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);

        $expedHelper = new \App\Helpers\Expedientes($db);
        $expedientes=$_GET['expedientes'];

        if($expedientes!="" && $expedientes!=null) {

            $expedientes = explode(',', $expedientes);
//aqui pueden llegar varios expedientes separados por ","
//por lo que le hago un explode y elimino el ultimo indice, ya que el ultimo siempre viene una coma sola
            if (count($expedientes) > 0) {
                unset($expedientes[count($expedientes) - 1]);
            }
            $vall = "";
            if (isset($_GET['q'])) {
                $vall = $_GET['q'];
            }

            $condicion = "";
            $cont = 0;
            foreach ($expedientes as $row) {

                $condicion .= $row;
                $cont++;
                if (count($expedientes) > $cont) {
                    $condicion .= ",";
                }
            }


            $tipologias = $expedHelper->tipolByExped($condicion, trim(str_replace("'","",$vall)));

            for ($i = 0; $i < count($tipologias); $i++) {
                $tipologias[$i]['id'] = $tipologias[$i]['SGD_TPR_CODIGO'];
                $tipologias[$i]['text'] = $tipologias[$i]['SGD_TPR_DESCRIP'];
            }
        }else{
            $tipologias['error']="Debe incluir expedientes";
        }

        $json=array();
        $json=$tipologias;
        return $json;
    } catch (\Exception $e) {
        return $e->getMessage();
    }
}

function tipolByExpedAndRad()
{
    try {

        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);

        require_once '../../config.php';

        $ruta_raiz = "../../";
        if (isset($db)) unset($db);
        include_once("$ruta_raiz/include/db/ConnectionHandlerNew.php");
        $db = new ConnectionHandler("$ruta_raiz");
        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);

        $expedHelper = new \App\Helpers\Expedientes($db);
        $expedientes=$_GET['expedientes'];
        $radicados=$_GET['radicados'];

        if($expedientes!="" && $expedientes!=null) {

            $expedientes = explode(',', $expedientes);
//aqui pueden llegar varios expedientes separados por ","
//por lo que le hago un explode y elimino el ultimo indice, ya que el ultimo siempre viene una coma sola
            if (count($expedientes) > 0) {
                unset($expedientes[count($expedientes) - 1]);
            }
            $vall = "";
            if (isset($_GET['q'])) {
                $vall = $_GET['q'];
            }



            $radicados = explode(',', $radicados);
//aqui pueden llegar varios expedientes separados por ","
//por lo que le hago un explode y elimino el ultimo indice, ya que el ultimo siempre viene una coma sola
            if (count($radicados) > 0) {
                unset($radicados[count($radicados) - 1]);
            }


            $condicion = "";
            $cont = 0;
            foreach ($expedientes as $row) {

                $condicion .= $row;
                $cont++;
                if (count($expedientes) > $cont) {
                    $condicion .= ",";
                }
            }
            $cont = 0;
            $radicadosIn="";
            foreach ($radicados as $row) {

                $radicadosIn .= "'".$row;
                $cont++;
                if (count($radicados) > $cont) {
                    $radicadosIn .= "',";
                }else{
                    $radicadosIn .= "'";
                }
            }


            $tipologias = $expedHelper->tipolByExpedAndRad($condicion,$radicadosIn, trim(str_replace("'","",$vall)));

            for ($i = 0; $i < count($tipologias); $i++) {
                $tipologias[$i]['id'] = $tipologias[$i]['SGD_TPR_CODIGO'];
                $tipologias[$i]['text'] = $tipologias[$i]['SGD_TPR_DESCRIP'];
            }
        }else{
            $tipologias['error']="Debe incluir expedientes";
        }

        $json=array();
        $json=$tipologias;
        return $json;
    } catch (\Exception $e) {
        return $e->getMessage();
    }

}

?>

