<?php

use App\Helpers\PDOconfig;
use Aura\SqlQuery\QueryFactory;
use Philo\Blade\Blade;

require '../../vendor/autoload.php';

$json = array();
session_start();

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
try {


    if (!isset($_SESSION["usuario_id"])) {

        $json = array(
            'error' => 'expiro'
        );
        echo json_encode($json);
        exit;
    }

    if ($_POST['borrador_id'] != "") {
        $response = get_borrador($_POST['borrador_id']);
        $json['success'] = $response;

    }

} catch (Exception $e) {
    echo "Error: " . $e->getMessage();
    $response = "ERROR";
    $json['error'] = true;
}
echo json_encode($json);


function get_borrador($vall)
{
    try {


        require_once '../../config.php';

        $borra = new \App\Helpers\Borradores();

        $dest = $borra->get_borrador($vall);
        $radicados_aso = $borra->get_radicadosAsociados($vall);
        $comentarios = $borra->get_comentarios($vall);

        $anexos = $borra->getAnexosBorrador($vall);
        $cont = 0;
        foreach ($anexos as $row) {
            $anexo_tipo = pathinfo($row['ANEXO_RUTA']);
            $anexo_tipo = $anexo_tipo['extension'];
            $anexos[$cont]['EXTENSION'] = "";
            $anexos[$cont]['EXTENSION'] = $anexo_tipo;
            $cont++;
        }
        $expediente = $borra->getExpedBorrador($vall);
        $dependUsuActual = $borra->getDepenByDepeCodi($dest[0]['DEPENDENCIA_USU_ACTUAL']);


        $json['nombre_actual'] = "";
        $json['cargo_actual'] = "";
        $json['nombre_anterior'] = "";
        $json['dependUsuActu'] = $dependUsuActual[0]['DEPE_NOMB'];
        $json['dependUsuAnterior'] = "";
        if ($dest[0]['ID_USUARIO_ACTUAL'] != null && $dest[0]['ID_USUARIO_ACTUAL'] != "" &&
            $dest[0]['ID_USUARIO_ACTUAL'] != "0"
        ) {
            $usuario_actual = $borra->getUsuarioById($dest[0]['ID_USUARIO_ACTUAL']);

            if (count($usuario_actual) > 0) {
                $json['nombre_actual'] = $usuario_actual[0]['USUA_NOMB'];
                $json['cargo_actual'] = $usuario_actual[0]['USUA_CARGO'];
            }
        }

        if ($dest[0]['ID_USUARIO_ANTERIOR'] != null && $dest[0]['ID_USUARIO_ANTERIOR'] != "" &&
            $dest[0]['ID_USUARIO_ANTERIOR'] != "0"
        ) {
            $dependUsuAnterior = $borra->getDepenByDepeCodi($dest[0]['DEPENDENCIA_USU_ANTERIOR']);
            $json['dependUsuAnterior'] = $dependUsuAnterior[0]['DEPE_NOMB'];
            $usuario_actual = $borra->getUsuarioById($dest[0]['ID_USUARIO_ANTERIOR']);
            if (count($usuario_actual) > 0) {
                $json['nombre_anterior'] = $usuario_actual[0]['USUA_NOMB'];
            }
        }
        $dest[0]['EXTENSION_RUTA_FILE'] = "";
        $dest[0]['EXISTE_RUTA_FILE'] = false;

        if ($dest[0]['DOCUMENTO_RUTA'] != "" && $dest[0]['DOCUMENTO_RUTA'] != null) {
            $anexo_tipo = pathinfo($dest[0]['DOCUMENTO_RUTA']);
            $anexo_tipo = $anexo_tipo['extension'];
            $dest[0]['EXTENSION_RUTA_FILE'] = $anexo_tipo;
            $dest[0]['EXISTE_RUTA_FILE'] = is_file($dest[0]['DOCUMENTO_RUTA']);
        }

        //si el usuario es interno, guardo en el arreglo, la dependencia del destinatario
        $dest[0]['dependenDestinatario'] = "";
        if (
            $dest[0]['TIPO_BORRADOR_DEST'] == $_SESSION['INTERNA_COMUNICACION']
            ||
            $dest[0]['TIPO_BORRADOR_DEST'] == $_SESSION['INTERNA_CON_PLANTILLA']
            ||
            $dest[0]['TIPO_BORRADOR_DEST'] == $_SESSION['INTERNA_SIN_PLANTILLA']
            || $dest[0]['TIPO_BORRADOR_DEST'] == $_SESSION['REPORTE4']
        ) {

            $destinatario = $borra->getUsuarioById($dest[0]['ID_DESTINATARIO']);
            if (isset($destinatario[0])) {
                $dest[0]['dependenDestinatario'] = $destinatario[0]['DEPE_CODI'];
            }
        }

        $dest[0]['historico']=array();
        $historico = $borra->getHistoricoBorrador($vall);
        if($historico!=false && $historico!=null && count($historico)>0){
            $dest[0]['historico'] =$historico;
        }


        //$dest[0]['DOCUMENTO_WEB_CUERPO'] = str_replace('&quot;', '"', $dest[0]['DOCUMENTO_WEB_CUERPO']);
        //$dest[0]['DOCUMENTO_WEB_CUERPO'] = str_replace('&#39;', "'", $dest[0]['DOCUMENTO_WEB_CUERPO']);
        //$dest[0]['DOCUMENTO_WEB_CUERPO']= stripslashes($dest[0]['DOCUMENTO_WEB_CUERPO']);

        $json['borrador'] = $dest;
        $json['radasociados'] = $radicados_aso;
        $json['comentarios'] = $comentarios;
        $json['anexos'] = $anexos;
        $json['expediente'] = $expediente;
        $json['firmantes'] = array();
        $firmantes = $borra->getFirmByBorrador($vall);
        if (count($firmantes) > 0) {
            $json['firmantes'] = $borra->barmarUsuariosComen($firmantes);
        }


        return $json;
    } catch (\Exception $e) {
        return $e->getMessage();
    }

}

?>

