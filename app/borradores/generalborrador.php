<?php

use App\Helpers\PDOconfig;
use Aura\SqlQuery\QueryFactory;
use Philo\Blade\Blade;

require '../../vendor/autoload.php';
session_start();
if (!isset($_SESSION['usuario_id'])) {
    $response = "ERROR";
    $json['error'] = 'expiro';
    echo json_encode($json);
    exit();
}

ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
error_reporting(E_ALL);


function updatecada60($post)
{
    $json = array();
    try {
        require_once '../../config.php';
        $ruta_raiz = "../../";
        if (isset($db)) unset($db);
        include_once("$ruta_raiz/include/db/ConnectionHandler.php");
        $db = new ConnectionHandler("$ruta_raiz");
        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        $borra = new \App\Helpers\Borradores($db);
        $fecha_transaccion = $db->conn->OffsetDate(0, $db->conn->sysTimeStamp);

        if ($_POST['borrador_id'] != "") {

            $response = $borra->updatecada60helper($post);
            $json['success'] = $response;

        } else {

            $id = $borra->save_all($post);
            $dest = $borra->get_borrador($id->fields['ID_BORRADOR']);
            $_POST['borrador_id'] = $id->fields['ID_BORRADOR'];
            $json['success']['id'] = $id->fields['ID_BORRADOR'];
            $dest[0]['DOCUMENTO_WEB_CUERPO'] = str_replace('^^', '"', $dest[0]['DOCUMENTO_WEB_CUERPO']);
            $dest[0]['DOCUMENTO_WEB_CUERPO'] = str_replace('++', "'", $dest[0]['DOCUMENTO_WEB_CUERPO']);
            //$dest[0]['DOCUMENTO_WEB_CUERPO'] = stripslashes($dest[0]['DOCUMENTO_WEB_CUERPO']);
            $json['borradoractual'] = $dest[0];

        }
        //busco los que estan en la bd actualmente
        $firmantes = $borra->getFirmByBorrador($_POST['borrador_id']);
        //borro todos sus firmantes
        $borra->borrarFirmanteByBorrador($_POST['borrador_id']);

        globalGuardarFirmantes($_POST, $borra, $_POST['borrador_id'], $firmantes, $fecha_transaccion);


        if ($_POST['radicadoactual'] != "") {

            //busco si ya fue asociado este radicado a este borrador
            $retorno = $borra->radicadoAsocByBorr($_POST['borrador_id'], $_POST['radicadoactual']);

            if (!isset($retorno[0])) {
                //si no existe ningun registro, lo guardo

                //busco el ultimo orden
                $response = $this->getLastOrdenIndex($_POST['borrador_id']);
                $orden = 1;
                if ($response[0]['MAXIMO'] != "") {
                    $orden = $response[0]['MAXIMO'] + 1;
                }

                $borra->insertBorrador_radicados_asociados($_POST['borrador_id'], $_POST['radicadoactual'], 0, $orden);
            }
        }

    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
        $response = "ERROR";
        $json['error'] = true;
    }


    echo json_encode($json);

}

function globalGuardarFirmantes($post, $borradoresC, $id_borrador, $firmantes, $fecha_transaccion)
{

    /**
     * la variable $firmantes, son los firmantes que tiene el borrador antes de guardar estos nuevos si es que vienen nuevos
     */
    if (isset($_POST['id_usuario_firmante'])) {
        /**
         * recorro los firmantes que vienen por post
         */
        foreach ($post['id_usuario_firmante'] as $row) {

            $cargo_firmante = '';

            //aqui pregunto si viene solo 1 firmante
            if (count($post['id_usuario_firmante']) < 2 && count($post['id_usuario_firmante']) > 0 &&
                isset($post['cargo_firmante'])) {
                $cargo_firmante = $_POST['cargo_firmante'];
                /**
                 * si viene un solo firmante, el campo del cargo a tener en cuenta, es el campo $_POST['cargo_firmante']
                 * ya que es el input id="cargo_firmante" de texto que esta en el modal
                 */
            }

            $tipo_validador = '';
            $principal = 0;
            if (count($_POST['id_usuario_firmante']) < 2) {
                $principal = 1;
                $tipo_validador = 'Remitente';
            }
            $usuario = $borradoresC->getUsuarioById($row);

            if ($cargo_firmante == "") {
                //entra aqui cuando no existe $_POST['cargo_firmante'], quiere decir, que vienen mas de 1 firmante,
                //si viene 1 solo firmante, entonces $cargo_firmante es el input de texto.
                $cargo_firmante = $usuario[0]['USUA_CARGO'];
            }
            if (count($firmantes) > 0) {

                $consiguio = FALSE;
                $cont = 0;
                //si entra aqui es porque ya tiene firmantes
                foreach ($firmantes as $firmante) {
                    //pregunto si el id que viene del formulario es igual al que estoy recorriendo de la BD
                    if ($firmante['ID'] == $row) {
                        $consiguio = $cont;
                    }
                    $cont++;
                }

                /**
                 * si entra en el siguiente if, es porque ya existia el firmante
                 */
                if ($consiguio !== FALSE) {

                    $principal = $firmantes[$consiguio]['FIRMANTE_PRINCIPAL'];
                    $tipo_validador = $firmantes[$consiguio]['TIPO_VALIDADOR'];

                    if (count($_POST['id_usuario_firmante']) < 2) {
                        $principal = 1;
                        $tipo_validador = 'Remitente';
                    }else{
                        /**
                         * como hay mas de un firmante si entra aqui, entonces el cargo a guardar es del arreglo,
                         * si no, entra en el if de arriba, porque solo hay 1 firmante y toma lo que ya tenia $cargo_firmante
                         */
                        $cargo_firmante= $firmantes[$consiguio]['FIRMANTE_CARGO'];
                    }


                    //si entra aqui, guardo los valores, con los datos que tenia si es que tenia
                    $borradoresC->guardarFirmantes($id_borrador, "'" . $firmantes[$consiguio]['FECHA_REGISTRO'] . "'", $row,
                        $cargo_firmante, $principal, $tipo_validador, $firmantes[$consiguio]['RADI_NUME']);
                } else {

                    //si entra aqui es porque es un nuevo firmante
                    $borradoresC->guardarFirmantes($id_borrador, $fecha_transaccion, $row, $cargo_firmante,
                        $principal, $tipo_validador, null);
                }

            } else {

                /**
                 * Si es un nuevo borrador ($_POST['borrador_id'==''), y el borrador == $_SESSION['REPORTE4']
                 * y el destinatario es igual al firmante que viene por post, entonces lo pongo por defecto como Aprobación
                 */
                if (
                    $_POST['borrador_id'] == ""
                    &&
                    $_POST['tipo_borrador_dest'] == $_SESSION['REPORTE4']
                    &&
                    $_POST['id_destinatario']==$row
                ) {
                    $tipo_validador = 'Aprobación';
                }

                if ((isset($_POST['ID_FIRST_SELECTED'])) && $_POST['ID_FIRST_SELECTED'] != '' &&
                    ($_POST['ID_FIRST_SELECTED'] == $row)) {
                    $principal = 1;
                    $tipo_validador = 'Remitente';

                }
                //si entra aqui es porqueno tiene ningun firmantes guardado
                $borradoresC->guardarFirmantes($id_borrador, $fecha_transaccion, $row, $cargo_firmante,
                    $principal, $tipo_validador, null);
            }
        }
    }
}

function aprobarborrador($post)
{
    $json = array();
    try {
        require_once '../../config.php';

        $ruta_raiz = "../../";
        if (isset($db)) unset($db);
        include_once("$ruta_raiz/include/db/ConnectionHandler.php");
        $db = new ConnectionHandler("$ruta_raiz");
        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        $borra = new \App\Helpers\Borradores($db);

        if ($_POST['borrador_id'] != "") {
            $response = $borra->aprobarborradorHelper($post);
            $id_borrador = $_POST['borrador_id'];
            $fecha_transaccion = $db->conn->OffsetDate(0, $db->conn->sysTimeStamp);
            $id_usuario_origen = $_SESSION["usuario_id"];
            $depe_codi_origen = $_SESSION['dependencia'];

            $codusuario = $_SESSION["usuario_id"];
            $dependencia = $_SESSION["dependencia"];
            $usuariodatos = $borra->getUsuarioById($codusuario);
            //guardo el historico y el comentario
            $borra->guardarhistorico($_POST['borrador_id'], $fecha_transaccion,
                trim($_POST['comentario_aprobar']), $id_usuario_origen,
                $depe_codi_origen, 0, 0, 2, 84);
            $response = true;
            $json['success'] = $response;
            $comentarios = $borra->get_comentarios($id_borrador);
            $json['comentarios'] = $comentarios;
        }

    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
        $response = "ERROR";
        $json['error'] = true;
    }
    echo json_encode($json);

}

function revisarBorrador($post)
{
    $json = array();
    try {
        require_once '../../config.php';
        $ruta_raiz = "../../";
        if (isset($db)) unset($db);
        include_once("$ruta_raiz/include/db/ConnectionHandler.php");
        $db = new ConnectionHandler("$ruta_raiz");
        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);

        $borra = new \App\Helpers\Borradores($db);
        $json['yaReviso'] = false;
        if ($_POST['borrador_id'] != "") {
            $histRev = $borra->getHistRevBorrador($_POST['borrador_id'],$_SESSION["usuario_id"],$_SESSION['dependencia']);
            if($histRev!=false && $histRev!=null && count($histRev)>0) {
                //$response = $histRev;
                //$json['response'] = $response;
                $json['yaReviso'] = true;
            }

            //else{

                $response = $borra->revisarBorradorHelper($post);

                $id_borrador = $_POST['borrador_id'];

                $fecha_transaccion = $db->conn->OffsetDate(0, $db->conn->sysTimeStamp);

                $id_usuario_origen = $_SESSION["usuario_id"];
                $depe_codi_origen = $_SESSION['dependencia'];
                $comentario = $_POST['comentario'];

                //guardo el historico y el comentario
                $response = $borra->guardarhistorico($id_borrador, $fecha_transaccion, $comentario, $id_usuario_origen,
                    $depe_codi_origen, 0, 0, 3, 81);
                $json['success'] = $response;
                $comentarios = $borra->get_comentarios($id_borrador);
                $json['comentarios'] = $comentarios;

                $json['historico']=array();
                $historico = $borra->getHistoricoBorrador($id_borrador);
                if($historico!=false && $historico!=null && count($historico)>0){
                    $json['historico'] =$historico;
                }

            //}

        }

    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
        $response = "ERROR";
        $json['error'] = true;
    }
    echo json_encode($json);

}

//verifica el estatus de un usuario
function getUsuaEstado()
{


    $json = array();
    try {
        require_once '../../config.php';

        $ruta_raiz = "../../";
        if (isset($db)) unset($db);
        include_once("$ruta_raiz/include/db/ConnectionHandlerNew.php");
        $db = new ConnectionHandler("$ruta_raiz");
        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        $borra = new \App\Helpers\Borradores($db);

        $usuario_destino = $borra->getUsuarioById($_POST['usuario']);
        if ($usuario_destino[0]['USUA_ESTA'] == 1) {
            $json['success'] = 1;
        } else {
            $dependencia = $borra->getDepenByDepeCodi($usuario_destino[0]['DEPE_CODI']);
            $json['USUA_NOMB'] = $usuario_destino[0]['USUA_NOMB'];
            $json['DEPENDENCIA'] = $dependencia[0]['DEPE_NOMB'];
            $json['error'] = "El usuario se encuentra inactivo, por lo que no puede devolverle el borrador";
        }

    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
        $response = "ERROR";
        $json['error'] = true;
    }
    echo json_encode($json);
}

function devolverBorrador($post)
{
    $json = array();
    try {
        require_once '../../config.php';

        $ruta_raiz = "../../";
        if (isset($db)) unset($db);
        include_once("$ruta_raiz/include/db/ConnectionHandler.php");
        $db = new ConnectionHandler("$ruta_raiz");
        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        $borra = new \App\Helpers\Borradores($db);

        if ($_POST['borrador_id'] != "") {

            $usuario_destino = $borra->getUsuarioById($_POST['anterior']);
            if ($usuario_destino[0]['USUA_ESTA'] == 1) {
                $response = $borra->devolverBorrador($post);
                $id_borrador = $_POST['borrador_id'];
                $fecha_transaccion = $db->conn->OffsetDate(0, $db->conn->sysTimeStamp);
                $id_usuario_origen = $_SESSION["usuario_id"];
                $depe_codi_origen = $_SESSION['dependencia'];

                //guardo el historico y el comentario

                $borra->guardarhistorico($id_borrador, $fecha_transaccion, $_POST['comentario'], $id_usuario_origen,
                    $depe_codi_origen, $post['anterior'], $post['dependencia_actual'], 4, 83);
                $json['success'] = $response;
            } else {
                $dependencia = $borra->getDepenByDepeCodi($usuario_destino[0]['DEPE_CODI']);
                $json['USUA_NOMB'] = $usuario_destino[0]['USUA_NOMB'];
                $json['DEPENDENCIA'] = $dependencia[0]['DEPE_NOMB'];

                $json['error'] = "El usuario se encuentra inactivo, por lo que no puede devolverle el borrador";
            }

        }

    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
        $response = "ERROR";
        $json['error'] = true;
    }
    echo json_encode($json);
}

function noaprobarBorrador($post)
{
    $json = array();
    try {
        require_once '../../config.php';

        $ruta_raiz = "../../";
        if (isset($db)) unset($db);
        include_once("$ruta_raiz/include/db/ConnectionHandler.php");
        $db = new ConnectionHandler("$ruta_raiz");
        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        $borra = new \App\Helpers\Borradores($db);

        if ($_POST['borrador_id'] != "") {

            $usuario_destino = $borra->getUsuarioById($_POST['anterior']);
            if ($usuario_destino[0]['USUA_ESTA'] == 1) {
                $response = $borra->noaprobarBorrador($post);
                $id_borrador = $_POST['borrador_id'];
                $fecha_transaccion = $db->conn->OffsetDate(0, $db->conn->sysTimeStamp);
                $id_usuario_origen = $_SESSION["usuario_id"];
                $depe_codi_origen = $_SESSION['dependencia'];

                //guardo el historico y el comentario

                $borra->guardarhistorico($id_borrador, $fecha_transaccion, $_POST['comentario'], $id_usuario_origen,
                    $depe_codi_origen, $post['anterior'], $post['dependencia_actual'], 6, 86);
                $json['success'] = $response;
            } else {
                $dependencia = $borra->getDepenByDepeCodi($usuario_destino[0]['DEPE_CODI']);
                $json['USUA_NOMB'] = $usuario_destino[0]['USUA_NOMB'];
                $json['DEPENDENCIA'] = $dependencia[0]['DEPE_NOMB'];

                $json['error'] = "El usuario se encuentra inactivo, por lo que no puede devolverle el borrador";
            }

        }

    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
        $response = "ERROR";
        $json['error'] = true;
    }
    echo json_encode($json);
}

function guardarComentBorrador()
{
    $json = array();
    try {
        require_once '../../config.php';

        $ruta_raiz = "../../";
        if (isset($db)) unset($db);
        include_once("$ruta_raiz/include/db/ConnectionHandler.php");
        $db = new ConnectionHandler("$ruta_raiz");
        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        $borra = new \App\Helpers\Borradores($db);

        if ($_POST['borrador_id'] != "") {

            $estadoComentario = $borra->getBorradorEstadoByCodigo(87);
            if (count($estadoComentario) > 0) {
                $id_borrador = $_POST['borrador_id'];
                $fecha_transaccion = $db->conn->OffsetDate(0, $db->conn->sysTimeStamp);
                $id_usuario_origen = $_SESSION["usuario_id"];
                $depe_codi_origen = $_SESSION['dependencia'];

                //guardo el historico y el comentario

                $borra->guardarhistorico($id_borrador, $fecha_transaccion, $_POST['comentario'], $id_usuario_origen,
                    $depe_codi_origen, 0, 0, $estadoComentario[0]['ID_BORRADOR_ESTADO'], 87);

                $json['success'] = true;
                $comentarios = $borra->get_comentarios($id_borrador);
                $json['comentarios'] = $comentarios;
            } else {
                $json['error'] = 'El estado "Comentario", no existe';
            }

        }
    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
        $response = "ERROR";
        $json['error'] = true;
    }
    echo json_encode($json);
}


function guardarEnviar($post)
{
    $json = array();
    try {
        require_once '../../config.php';

        $ruta_raiz = "../../";
        if (isset($db)) unset($db);
        include_once("$ruta_raiz/include/db/ConnectionHandler.php");
        $db = new ConnectionHandler("$ruta_raiz");
        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        $borra = new \App\Helpers\Borradores($db);

        if ($_POST['borrador_id'] != "") {

            //el usuario actual, pasa a ser el usuario anterior, y el usuauro destino, pasa a ser el usuairo actual
            $post['actual'] = $_SESSION["usuario_id"];
            $post['dependencia_actual'] = $_SESSION['dependencia'];

            //AQUI LLEGA ES EL ID UNICO DEL USUARIO, ES DECIR, EL CAMPO SERIAL.

            //busco la dependencia del usuario destino
            $usuario_destino = $borra->getUsuarioById($_POST['usuario_destino']);

            $post['anterior'] = $usuario_destino[0]['ID'];
            $depe_codi_destino = $usuario_destino[0]['DEPE_CODI'];
            $post['dependencia_anterior'] = $depe_codi_destino;
            //actualizo el usuario origen

            $response = $borra->updateUsuActual($post);


            $id_borrador = $_POST['borrador_id'];
            $fecha_transaccion = $db->conn->OffsetDate(0, $db->conn->sysTimeStamp);
            $comentario_borrador = 'Enviado a ' . $usuario_destino[0]['USUA_NOMB'] . ': ' . $_POST['comentarioenviar'];
            $id_usuario_origen = $_SESSION["usuario_id"];
            $depe_codi_origen = $_SESSION['dependencia'];
            $id_usuario_destino = $usuario_destino[0]['ID'];


            //guardo el historico y el comentario
            $response = $borra->guardarhistorico($id_borrador, $fecha_transaccion, $comentario_borrador, $id_usuario_origen,
                $depe_codi_origen, $id_usuario_destino, $depe_codi_destino, 5, 82);

            $json['success'] = $response;

        }

    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
        $response = "ERROR";
        $json['error'] = true;
    }
    echo json_encode($json);
}


function solocomentarios($post)
{
    $json = array();
    try {
        require_once '../../config.php';

        $borra = new \App\Helpers\Borradores();

        if ($_POST['borrador_id'] != "") {

            $id_borrador = $_POST['borrador_id'];

            $comentarios = $borra->get_comentariosLike($id_borrador, $_POST['buscar']);
            $json['comentarios'] = $comentarios;

        }

    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
        $response = "ERROR";
        $json['error'] = true;
    }
    echo json_encode($json);
}


function guardarAsociado($post)
{
    $json = array();
    try {
        require_once '../../config.php';

        $borra = new \App\Helpers\Borradores();

        if ($_POST['borrador_id'] != "") {

            $id_borrador = $_POST['borrador_id'];
            $radicados = $_POST['radicados'];


            //guardo el historico y el comentario
            $response = $borra->borradorRadAsoc($id_borrador, $radicados);
            //si response es un arreglo con datos, quiere decir que algunos radicados seleccionados, ya fueron asociados a este
            //borrador
            if (count($response) > 1) {
                $json['alerta'] = $response;
            }
            $json['success'] = true;
            $radicados_aso = $borra->get_radicadosAsociados($id_borrador);


            $json['radasociados'] = $radicados_aso;

        }

    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
        $response = "ERROR";
        $json['error'] = true;
    }
    echo json_encode($json);
}

function borrarBorrador($post)
{
    $json = array();
    try {
        require_once '../../config.php';
        require '../../vendor/autoload.php';
        $ruta_raiz = "../../";
        if (isset($db)) unset($db);
        include_once("$ruta_raiz/include/db/ConnectionHandlerNew.php");
        $db = new ConnectionHandler("$ruta_raiz");

        ini_set('display_errors', 0);
        ini_set('display_startup_errors', 0);
        error_reporting(E_ALL);
        require_once("$ruta_raiz/include/tx/Historico.php");
        $borraHelper = new \App\Helpers\Borradores($db);
        $infoborra = $borraHelper->get_borrador($_POST['borrador_id']);
        if ($_POST['borrador_id'] != "") {


            /*$query = "delete from borrador_anexos where id_borrador='" . $_POST['borrador_id'] . "' ";
            $db->conn->execute($query);
            $query = "delete from borrador_historicos where id_borrador='" . $_POST['borrador_id'] . "' ";
            $db->conn->execute($query);
            $query = "delete from borrador_radicados_asociados where id_borrador='" . $_POST['borrador_id'] . "' ";
            $db->conn->execute($query);
            $query = "delete from borrador_expedientes where id_borrador='" . $_POST['borrador_id'] . "' ";
            $response = $db->conn->execute($query);

            $query = "delete from borrador_firmantes where id_borrador='" . $_POST['borrador_id'] . "' ";
            $response = $db->conn->execute($query);

            $query = "delete from borradores where id_borrador='" . $post['borrador_id'] . "' ";
            $response = $db->conn->execute($query);

            $response = $borra->borrarBorrador($post);*/

            $estado = $borraHelper->getBorradorEstadoByNombre('Borrador Eliminado');


            if (count($estado) > 0) {

                if ($_POST['radicado'] != "") {

                    $radicadoarray = Array();
                    $radicadoarray[0] = $_POST['radicado'];
                    $Tx = new Historico($db);
                    $texto = "El borrador [" . $_POST['borrador_id'] . "]: " . $infoborra[0]['ASUNTO'] . ", ha sido eliminado, No se radicó. ". $_POST['justificacion'];
                    $a = $Tx->insertarHistorico($radicadoarray, $_SESSION['dependencia'], $_SESSION['codusuario'],
                        $_SESSION['dependencia'], $_SESSION['codusuario'], substr($texto, 0, 4000), 88);
                }

                $query = "update borradores set id_estado=" . $estado[0]['ID_BORRADOR_ESTADO'] . " where id_borrador='" . $_POST['borrador_id'] . "' ";
                $response = $db->conn->execute($query);
                $json['success'] = $response;

                $id_usuario_origen = $_SESSION["usuario_id"];
                $depe_codi_origen = $_SESSION['dependencia'];
                $texto = "El borrador [" . $_POST['borrador_id'] . "]: " . $infoborra[0]['ASUNTO'] . ", ha sido eliminado, No se radicó. ". $_POST['justificacion'];
                $fecha_transaccion = $db->conn->OffsetDate(0, $db->conn->sysTimeStamp);

                $borraHelper->guardarhistorico($_POST['borrador_id'], $fecha_transaccion, $texto,
                    $id_usuario_origen, $depe_codi_origen, 0, 0, $estado[0]['ID_BORRADOR_ESTADO'] );


            } else {
                $json['error'] = 'No se ha podido eliminar el borrador';
            }

        }else{
            $json['error'] = 'Debemos obtener un borrador';
        }

    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
        $response = "ERROR";
        $json['error'] = true;
    }
    echo json_encode($json);

}


function buscarradicado($post)
{
    try {
        require_once '../../config.php';

        $borra = new \App\Helpers\Borradores();

        $coincide = $post['coincide'];
        $radicados = $borra->buscarRadicadoHelper($coincide);

        echo json_encode($radicados);
    } catch (\Exception $e) {
        return $e->getMessage();
    }

}

function removeanexo($post)
{
    try {
        require_once '../../config.php';

        $borra = new \App\Helpers\Borradores();

        $borrar_anexo = $borra->borrar_anexo($post['borrador_id'], $post['id_borrador_anexo']);
        $json['anexos'] = $borra->getAnexosBorrador($_POST['borrador_id']);
        echo json_encode($json);
    } catch (\Exception $e) {
        return $e->getMessage();
    }

}

//function que selecciona un raadicado asociadao como radicado origen
function seleccionarOrigen($post)
{
    try {
        require_once '../../config.php';

        $borra = new \App\Helpers\Borradores();

        $inforad = $borra->getRadicadoByNume_radi($post['radicado_asociado']);


        if (count($inforad) > 0) {
            if ($inforad[0]['RADI_DEPE_ACTU'] == '999' && $post['confirmar_999'] == 'false') {
                $json['error'] = 'El radicado ' . $post['radicado_asociado'] . ' está Finalizado, está seguro de seleccionarlo como Padre? Para modificaciones, debe solicitar devolución del radicado padre a su bandeja.';
            } else {
                $actualiza = $borra->seleccionarOrigen($post['borrador_id'], $post['radicado_asociado']);
                $json['radiaso'] = $borra->get_radicadosAsociados($post['borrador_id']);
            }

        } else {
            $json['error'] = 'El radicado no existe';
        }

        echo json_encode($json);
    } catch (\Exception $e) {
        return $e->getMessage();
    }

}

function removeRadiAsoc($post)
{
    try {
        require_once '../../config.php';

        $borra = new \App\Helpers\Borradores();

        $borrar_radiaso = $borra->removeRadiAsoc($post['borrador_id'], $post['radicado_asociado']);
        $json['radiaso'] = $borra->get_radicadosAsociados($post['borrador_id']);
        echo json_encode($json);
    } catch (\Exception $e) {
        return $e->getMessage();
    }

}


function formatSizeUnits($bytes)
{
    /*if ($bytes >= 1073741824)
    {
        $bytes = number_format($bytes / 1073741824, 2) . ' GB';
    }
    elseif ($bytes >= 1048576)
    {
        $bytes = number_format($bytes / 1048576, 2) . ' MB';
    }*/
    if ($bytes >= 1024) {
        $bytes = number_format($bytes / 1024, 2);
    } elseif ($bytes > 1) {
        $bytes = $bytes;
    } elseif ($bytes == 1) {
        $bytes = $bytes;
    } else {
        $bytes = '0';
    }

    return $bytes;
}

function radicar($post)
{
    try {
        require_once '../../config.php';

        $ruta_raiz = "../../";
        include("$ruta_raiz/config.php");
        if (isset($db)) unset($db);
        include_once("$ruta_raiz/include/db/ConnectionHandler.php");
        $db = new ConnectionHandler("$ruta_raiz");
        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        $ruta_raiz = "../../";


        require_once("$ruta_raiz/include/tx/Radicacion.php");
        require_once("$ruta_raiz/include/tx/Tx.php");
        require_once($ruta_raiz . "class_control/anexo.php");
        include($ruta_raiz . "fpdf/html2pdf.php");

        $helperBorra = new \App\Helpers\Borradores($db);
        $configHelper = new \App\Helpers\Configuraciones($db);
        $ruta = "../../bodega";
        if (!is_writable($ruta)) {
            $retorno['error'] = 'Problemas para subir archivos a la bodega. Solicitar soporte URGENTE.';
            echo json_encode($retorno);
            exit();
        }


        $anexosG = $helperBorra->updateDatosAnexosFolios($_POST['borrador_id'], $_POST['folios_comunicacion'], $_POST['folios_anexos'], $_POST['desc_anexos']);


        //busco los datos del borrador
        $query = "SELECT  *
                FROM borradores  
                join borrador_estados on (borrador_estados.id_borrador_estado=borradores.id_estado)
                where id_borrador=" . $_POST['borrador_id'] . "
                ORDER BY id_borrador ASC";

        $borrador = $db->conn->selectLimit($query, 1)->getArray();
        $borradorDocRuta = $borrador[0]['DOCUMENTO_RUTA'];

        if (!(is_null($borradorDocRuta))){
            $nombre_fichero = str_replace("../../bodega", $ruta, $borradorDocRuta);

            if (file_exists($nombre_fichero)) {
                $nombre_fichero = '';
            } else {
                $retorno['error'] = 'La plantilla no se pudo encontrar.';
                echo json_encode($retorno);
                return false;
            }
        }
        $rad = new Radicacion($db);

        //////////////////radicados asociados///////////////////////////
        $query = "SELECT  *
                FROM borrador_radicados_asociados where id_borrador='" . $_POST['borrador_id'] . "' 
                order by radicado_origen desc, id_borrador_radicado_asociado desc";


        $radicadosasociados = $db->conn->selectLimit($query, 10000)->getArray();

        $numeRadiAscOrigen = "null";
        foreach ($radicadosasociados as $radaso) {
            if ($radaso['RADICADO_ORIGEN'] == 1) {
                $numeRadiAscOrigen = $radaso['RADICADO_ASOCIADO'];
            }
        }

        $inforadPadre = ""; //variable que almacena la info delr adicado padre, si es que esoy radicando desde documentos.
        if ($numeRadiAscOrigen != "null") {
            $inforadPadre = $rad->getDatosRad($numeRadiAscOrigen);
            //aprovecho para buscar la info del padre
        }

        $tipo_radicado = $borrador[0]['TIPO_RADICADO'];
        $tipo_destinatario = $borrador[0]['TIPO_DESTINATARIO'];


        $rutaRadipacht = "";
        if (!is_dir($ruta . "/" . date("Y"))) {

            mkdir("$ruta", 0777);
        }

        $anio = date("Y");
        if ($inforadPadre != "") {
            $anio = substr($inforadPadre['radi_nume_radi'], 0, 4);
        }

        $ruta .= "/" . $anio;

        $rutaRadipacht .= "/" . $anio;

        $dependencia = $_SESSION['dependencia'];
        if (!is_dir($ruta . "/" . $dependencia)) {

            mkdir("$ruta", 0777);
        }

        if ($inforadPadre != "") {
            $dependencia = $inforadPadre['depe_codi'];
        }
        $ruta .= "/" . $dependencia;
        $rutaRadipacht .= "/" . $dependencia;

        if (!is_dir($ruta . "/docs")) {

            mkdir("$ruta", 0777);
        }

        $ruta .= "/docs/";
        $rutaRadipacht .= "/docs/";


        /********************empiezo con los expedientes*************************/
        $query = "SELECT  *
                FROM borrador_expedientes where id_borrador='" . $_POST['borrador_id'] . "' order by id_borrador_expedientes asc";

        $expedientes = $db->conn->selectLimit($query, 10000)->getArray();


        //empiezo a crear el radicado
        $rad->tipRad = $tipo_radicado;
        $rad->raAsun = $borrador[0]['ASUNTO']; // Asunto del radicado
        $rad->radiPath = "''";    // Ruta del radicado
        $rad->nofolios = $_POST['folios_comunicacion'];//numeros de paginas que tiene el pdf
        $rad->noanexos = $_POST['folios_anexos'];
        $rad->descAnex = $_POST['desc_anexos'];    // Descripcion del anexo
        $rad->radiTipoDeri = "";
        $rad->radiDepeRadi = $_SESSION['dependencia'];
        $rad->usuaDoc = $_SESSION["codusuario"];
        $rad->radiUsuaActu = $numeRadiAscOrigen != "null" ? 1 : $_SESSION['codusuario'];
        $rad->radiDepeActu = $numeRadiAscOrigen != "null" ? 999 : $_SESSION['dependencia'];
        $rad->depe_codi = $_SESSION['dependencia'];
        $rad->usuaCodi = $_SESSION["codusuario"];
        $rad->usNivel = $_SESSION["nivelus"];

        //notese que del swiche viene "false" cuando esta retringido, por lo tanto se coloca 1 si es false, si es "true" es publico
        $rad->nivelRad = $_POST['seguridad_radicado'] == "false" ? 1 : 0;
        $rad->carpCodi = $tipo_radicado;        // $tipo de radicado;
        $rad->radiCuentai = "Borrador Id [" . $_POST['borrador_id'] . "]";
        $rad->eespCodi = "null";    //$documento_us3;
        $rad->mrecCodi = 1;        //"dd/mm/aaaa"
        $rad->radiFechOfic = "";//$fecha_gen_doc_YMD;
        $rad->radiNumeDeri = $numeRadiAscOrigen;    //trim($radicadopadre);
        $rad->radiPais = 170;        //$tmp_mun->get_pais_codi();
        $rad->trteCodi = $tipo_destinatario;        // tipo de destinatario;
        $rad->tdocCodi = $expedientes[0]['SGD_TPR_CODIGO'] != "" ? $expedientes[0]['SGD_TPR_CODIGO'] : 0;        // Tipo documental del radicado
        $rad->tdidCodi = 0;        // $tip_doc;
        $rad->sgd_apli_codi = 0;        // Por defecto aplicaciones integradas Cero
        //aqui guardo el radicado
        $query_sec = "Select depe_rad_tp" . $tipo_radicado . " As SECR_TP From dependencia Where depe_codi=" . $_SESSION['dependencia'];
        $rs = $db->query($query_sec);
        $depe_secr_tp = $rs->fields['SECR_TP'];
        $noRad = $rad->newRadicado($tipo_radicado, $depe_secr_tp);
        $noRad = trim($noRad);

        //el historico lo busco aqui porque voy a mostrar en el documento del radicado, quien aprobo reviso  proyecto
        $historico = $helperBorra->getHistBorraJoinUser($_POST['borrador_id']);

        $infoRadicado = $rad->getDatosRad($noRad);

        if ($numeRadiAscOrigen != "null") {
            //para actualizarle los datos al raricado origen
            $query = "update radicado set radi_nume_deri=$noRad, radi_tipo_deri=0
                      where radi_nume_radi=$numeRadiAscOrigen and (radi_nume_deri is null or  radi_nume_deri=0)";
            $guardar = $db->conn->execute($query);
        }

        $firmantes = $helperBorra->getFirmByBorrador($_POST['borrador_id']);

        $titulomemo = '';
        if ($borrador[0]['TIPO_COMUNICACION'] != null && $borrador[0]['TIPO_COMUNICACION'] != '') {
            $titulomemo = $borrador[0]['TIPO_COMUNICACION'];
        }


        //genero el documento del radicado
        $totalpaginas = 0;
        $generarDoc = generardocRadicado($borrador, $noRad, $db, $ruta, $rutaRadipacht, $historico,
            $helperBorra, $firmantes, $titulomemo, $infoRadicado, $configHelper);
        $documento_ruta = $generarDoc['documento_ruta'];
        $primerAnexoNombre = $generarDoc['primerAnexoNombre'];
        $kb = $generarDoc['kb'];
        $kb = str_replace(",", "", $kb);
        if ($_POST['folios_comunicacion'] != "" && $_POST['folios_comunicacion'] >= 0) {
            $totalpaginas = $_POST['folios_comunicacion'];
        }

        $arrayRuta = explode("bodega",$documento_ruta);
        if (sizeof($arrayRuta) > 1){
            $rutaSinBodega = $arrayRuta[1];
        }else{
            $rutaSinBodega = $documento_ruta;
        }
        $query = "update radicado set radi_path=";
        if ($documento_ruta != 'null') {
            if ($borrador[0]['TIPO_BORRADOR_DEST'] == 'INTERNA_SIN_PLANTILLA' && is_null($borrador[0]['DOCUMENTO_RUTA']) ){
                $query .= 'null';
            }else{
                $query .= "'" . $rutaSinBodega . "'";
            }
        } else {
            $query .= $documento_ruta;
        }
        //para actualizarle la ruta del radicado
        $query .= ", 
        radi_nume_folio=" . $totalpaginas . " where radi_nume_radi=" . $noRad . " ";

        $guardar = $db->conn->execute($query);


        //empiezo con las direcciones
        if ($noRad != "" && $noRad != false) {

            ///////guardo la direccion, y busco los datos en las tablas, dependiendo del tipo de radicado
            if ($borrador[0]['TIPO_DESTINATARIO'] == 2) {
                //Externo | Empresa sgd_oem_oempresas

                $query = "SELECT  *
                FROM sgd_oem_oempresas  
                where sgd_oem_codigo=" . $borrador[0]['ID_DESTINATARIO'] . " ";

                $usuario = $db->conn->selectLimit($query, 1)->getArray();

                $rad->oemCodigo = $usuario[0]['SGD_OEM_CODIGO'];
                $rad->grbNombresUs = "'" . $usuario[0]['SGD_OEM_OEMPRESA'] . "'";
                $rad->dirNombre = "'" . $borrador[0]['NOMBRE_PERSONA'] . "'";
                $rad->direccion = "'" . $usuario[0]['SGD_OEM_DIRECCION'] . "'";
                $rad->dirTelefono = "'" . $usuario[0]['SGD_OEM_TELEFONO'] . "'";
                $rad->dirTelcelular = $usuario[0]['SGD_OEM_TELCELULAR'];
                $rad->dirMail = "'" . $usuario[0]['SGD_OEM_EMAIL'] . "'";
                $rad->ccDocumento = "'" . $usuario[0]['SGD_OEM_NIT'] . "'";
                //$rad->ciuCodigo = $usuario[0]['SGD_OEM_CODIGO'];
                $rad->espCodigo = NULL;

                $query = "Select nextval('sec_dir_direcciones') ";
                $nextval = $db->conn->selectLimit($query, 1)->getArray();

                $rad->dirCodigo = $nextval[0]['NEXTVAL'];
                $rad->muniCodi = $usuario[0]['MUNI_CODI'];
                $rad->dpto_tmp1 = $usuario[0]['DPTO_CODI'];

                $rad->idPais = $usuario[0]['ID_PAIS'];
                $rad->idCont = $usuario[0]['ID_CONT'];


            } elseif ($borrador[0]['TIPO_DESTINATARIO'] == 4) {
                //Interno | Funcionario usuario

                $query = "SELECT  * 
                FROM usuario  
                where id=" . $borrador[0]['ID_DESTINATARIO'] . " ";

                $usuario = $db->conn->selectLimit($query, 1)->getArray();

                $query = "Select d.depe_nomb dir From usuario u
                join dependencia d on(d.depe_codi=u.depe_codi)
                Where u.depe_codi=d.depe_codi And
                u.usua_doc='" . $usuario[0]['USUA_DOC'] . "' ";

                $direc = $db->conn->selectLimit($query, 1)->getArray();

                $rad->grbNombresUs = "'" . $usuario[0]['USUA_NOMB'] . "'";
                $rad->dirNombre = "'" . $borrador[0]['NOMBRE_PERSONA'] . "'";
                $rad->direccion = "'" . $direc[0]['DIR'] . "'";
                $rad->dirTelefono = "'" . $usuario[0]['USUA_TELEFONO1'] . " " . $usuario[0]['USUA_EXT'] . "'";
                $rad->dirTelcelular = $usuario[0]['USUA_CELULAR'];
                $rad->dirMail = "'" . $usuario[0]['USUA_EMAIL'] . " " . $usuario[0]['USUA_EMAIL1'] . "'";
                $rad->ccDocumento = "'" . $usuario[0]['USUA_DOC'] . "'";
                $rad->ciuCodigo = $usuario[0]['SGD_CIU_CODIGO'];
                $rad->espCodigo = NULL;
                $rad->funCodigo = $usuario[0]['USUA_DOC'];

                $query = "Select nextval('sec_dir_direcciones') ";
                $nextval = $db->conn->selectLimit($query, 1)->getArray();

                $rad->dirCodigo = $nextval[0]['nextval'];
                $rad->muniCodi = 1;
                $rad->dpto_tmp1 = 11;

                $rad->idPais = $usuario[0]['ID_PAIS'];
                $rad->idCont = $usuario[0]['ID_CONT'];
                $rad->dirPersonaCargo = $usuario[0]['USUA_CARGO'];

            } else {
                //Externo | Ciudadano sgd_ciu_ciudadano 1 1

                $query = "SELECT  *
                FROM sgd_ciu_ciudadano  
                where sgd_ciu_codigo=" . $borrador[0]['ID_DESTINATARIO'] . " ";

                $usuario = $db->conn->selectLimit($query, 1)->getArray();

                $query = "Select nextval('sec_dir_direcciones') ";
                $direcc = $db->conn->selectLimit($query, 1)->getArray();

                $rad->grbNombresUs = "'" . $usuario[0]['SGD_CIU_NOMBRE'] . " " . $usuario[0]['SGD_CIU_APELL1'] . " " .
                    $usuario[0]['SGD_CIU_APELL2'] . "'";
                $rad->dirNombre = "'" . $borrador[0]['NOMBRE_PERSONA'] . "'";
                $rad->direccion = "'" . $usuario[0]['SGD_CIU_DIRECCION'] . "'";
                $rad->dirTelefono = "'" . $usuario[0]['SGD_CIU_TELEFONO'] . "'";
                $rad->dirTelcelular = $usuario[0]['SGD_CIU_TELCELULAR'];
                $rad->dirMail = "'" . $usuario[0]['SGD_CIU_EMAIL'] . "'";
                $rad->ccDocumento = "'" . $usuario[0]['SGD_CIU_CEDULA'] . "'";
                $rad->ciuCodigo = $usuario[0]['SGD_CIU_CODIGO'];
                $rad->espCodigo = NULL;

                $query = "Select nextval('sec_dir_direcciones') ";
                $nextval = $db->conn->selectLimit($query, 1)->getArray();

                $rad->dirCodigo = $nextval[0]['NEXTVAL'];
                $rad->muniCodi = $usuario[0]['MUNI_CODI'];
                $rad->dpto_tmp1 = $usuario[0]['DPTO_CODI'];

                $rad->idPais = $usuario[0]['ID_PAIS'];
                $rad->idCont = $usuario[0]['ID_CONT'];
            }


            $rad->trdCodigo = $tipo_destinatario;
            $rad->radiNumeRadi = $noRad;
            $rad->cargo = $borrador[0]['CARGO_PERSONA'];
            ////////guardo la direccion
            $respuestaInsert = $rad->insertDireccion($noRad, 1, 0);


            ///////////////////////////////EMPIEZO A GUARDAR LOS HISTORICOS//////////////////////////////////////////

            $radicadoarray = Array();
            $radicadoarray[0] = $noRad;
            $cont = 0;
            $Tx = new Tx($db);
            foreach ($historico as $histor) {

                $usuario_id_destino = $histor['ID_USUARIO_DESTINO'];
                $usua_codi_destino = 0;

                if ($usuario_id_destino != "0") {
                    $query = "SELECT  *
                FROM usuario where id='" . $usuario_id_destino . "' ";

                    $usuario_destino = $db->conn->selectLimit($query, 1)->getArray();
                    $usua_codi_destino = $usuario_destino[0]['USUA_CODI'];
                }

                $a = $Tx->insertarHistorico($radicadoarray, $histor['DEPE_CODI_ORIGEN'], $histor['USUA_CODI'],
                    $histor['DEPE_CODI_DESTINO'], $usua_codi_destino, substr($histor['COMENTARIO_BORRADOR'], 0, 4000), $histor['SGD_TTR_CODIGO'],
                    $histor['FECHA_TRANSACCION']);
                
                $cont++;

                //este es el movimiento de radicacion
                if ($cont == count($historico)) {

                    $tipoBorradorDest = $helperBorra->getTipoBorradorDest($borrador[0]['TIPO_BORRADOR_DEST']);

                    $a = $Tx->insertarHistorico($radicadoarray, $_SESSION['dependencia'], $_SESSION['codusuario'],
                        $_SESSION['dependencia'], $_SESSION['codusuario'],
                        "Radicado Desde borrador [".$borrador[0]['ID_BORRADOR']."], como " . $tipoBorradorDest . ' a partir de ' . $borrador[0]['DOCUMENTO_FORMATO'], 2,
                        NULL);
                }

            }


            ///////empiezo con la tabla borrador

            /**
             * si no tiene radicado origen, y
             * - es plantilla, o
             * - DOCUMENTO_FORMATO== EditorWeb && TIPO_BORRADOR_DEST!=INTERNA_SIN_PLANTILLA olos borradores INTERNA_SIN_PLANTILLA,
             *              se guardan en TIPO_BORRADOR_DEST="SinPlantilla", y solo se sabe si tiene alguna plantilla, si
             *              DOCUMENTO_RUTA !=null)
             */

            if ($numeRadiAscOrigen == "null" &&
                (
                    $borrador[0]['DOCUMENTO_FORMATO'] == "Plantilla"
                    ||
                    (
                        $borrador[0]['DOCUMENTO_FORMATO'] == "EditorWeb"
                        &&
                        $borrador[0]['TIPO_BORRADOR_DEST'] != $_SESSION['INTERNA_SIN_PLANTILLA']
                    )
                    ||
                    (
                        $borrador[0]['TIPO_BORRADOR_DEST'] == $_SESSION['INTERNA_SIN_PLANTILLA']
                        &&
                        $borrador[0]['DOCUMENTO_RUTA'] !=NULL
                    )
                )
            ) {

                $anexo = new Anexo($db);

                $anexo->anex_radi_nume = $noRad;
                $anexo->sgd_trad_codigo = $tipo_radicado;
                $anexo->anex_desc = $borrador[0]['ASUNTO'];

                $anexo->anex_numero = 1;
                $anexo->anex_salida = 1;
                $anexo->radi_nume_salida = $noRad;
                $anexo->anex_estado = 2;
                $anexo->anex_solo_lect = "'N'";

                //BUSCO EL TIPO DE ANEXO
                $query = "SELECT  *
                      FROM anexos_tipo where anex_tipo_ext='pdf' ";
                $query_tipo_anexo = $db->conn->selectLimit($query, 1)->getArray();
                $tipo_anexo = 18;
                if (count($query_tipo_anexo) > 0) {
                    $tipo_anexo = $query_tipo_anexo[0]['ANEX_TIPO_CODI'];
                }
                //$anexo->anex_tipo = $tipo_anexo;

                $anexo->anex_codigo = $noRad . "00001";
                $anexo->anex_nomb_archivo = $primerAnexoNombre; //viene de la creacion del documento del radicado, debe ser un docx
                $anexo->anex_radi_fech = $infoRadicado['fechaRadicacion'];
                $anexo->anex_creador = "'" . $_SESSION['usua_login'] . "'";
                $anexo->sgd_rem_destino = 1;
                $anexo->sgd_dir_tipo = 1;
                $anexo->anex_tamano = $kb; //pendiente
                $anexo->anex_borrado = "'N'";
                $anexo->anex_depe_creador = $_SESSION['dependencia'];
                $anexo->sgd_tpr_codigo = 0;
                $anexo->anex_origen = 3;
                $anexo->anex_ubic = "'BORRADOR'";
                $anexo->usua_doc = $_SESSION['usua_doc'];
                $anexo->anex_regenerado = 0;
                $auxnumero = $anexo->obtenerMaximoNumeroAnexo($noRad);

                if ($borrador[0]['DOCUMENTO_FORMATO'] == "EditorWeb") {
                    if ($borrador[0]['TIPO_BORRADOR_DEST'] != $_SESSION['INTERNA_SIN_PLANTILLA']) {
                        $anexo->anex_regenerado = 1;
                    }
                }

                $anexoCodigo = $anexo->anexarFilaRadicado($auxnumero);

            }
            ///////////////////////////////////////////////////////////

            foreach ($radicadosasociados as $borra_anex) {
                $encontroOrigen = false;
                $anex_estado = 4;
                $texto = "Se radicó el borrador [" . $borrador[0]['ID_BORRADOR'] . "] con el número " . $borra_anex['RADICADO_ASOCIADO'] . ": " . $borrador[0]['ASUNTO'] . ". Asociado a este radicado.";
                $texto = substr($texto, 0, 4000);
                $anexo = new Anexo($db);
                $anexo->anex_regenerado = 0;

                if ($borra_anex['RADICADO_ORIGEN']) {
                    //si tiene un radicado origen
                    $encontroOrigen = true;
                    $numeRadiAscOrigen = $borra_anex['RADICADO_ASOCIADO'];
                    $borra_anex['RADICADO_ASOCIADO'] = $noRad;
                    $anex_estado = 2;
                    $texto = "Radicado generado como borrador de " . $numeRadiAscOrigen;

                    if ($borrador[0]['DOCUMENTO_FORMATO'] == "EditorWeb") {
                        if ($borrador[0]['TIPO_BORRADOR_DEST'] == $_SESSION['INTERNA_SIN_PLANTILLA']) {
                        } else {
                            $anexo->anex_regenerado = 1;
                        }
                    }
                }

                $query = "Select radi_path, ra_asun, radi_fech_radi, sgd_trad_codigo,radi_nume_radi
                          From radicado Where radi_nume_radi='" . $borra_anex['RADICADO_ASOCIADO'] . "' ";


                $trad_codigo = $db->conn->selectLimit($query, 1)->getArray();
                $primercaracter = substr($trad_codigo[0]['RADI_PATH'], 0, 1);
                if ($primercaracter != "/") {
                    $trad_codigo[0]['RADI_PATH'] = "/" . $trad_codigo[0]['RADI_PATH'];
                }

                $radicado_asociado_path = "/../../.." . $trad_codigo[0]['RADI_PATH'];
                $radicado_asociado_asunto = $trad_codigo[0]['RA_ASUN'];
                $radicado_asociado_tipo = $trad_codigo[0]['SGD_TRAD_CODIGO'];
                $radicado_asociado_fecha = $trad_codigo[0]['RADI_FECH_RADI'];


                if ($encontroOrigen == true) {
                    $radicado_asociado_path = $primerAnexoNombre;
                    $anexo->sgd_doc_padre = 1;
                }

                $anex_desc = $radicado_asociado_asunto;

                if ($encontroOrigen == false) {
                    $anex_desc = $anex_desc . " (" . date('Y-m-d H:i', strtotime($radicado_asociado_fecha)) . ")";
                }

                $anexo->anex_radi_nume = $numeRadiAscOrigen != "null" ? $numeRadiAscOrigen : $noRad;
                $anexo->sgd_trad_codigo = $radicado_asociado_tipo;
                $anexo->anex_desc = $anex_desc;
                $auxnumero = $anexo->obtenerMaximoNumeroAnexo($borra_anex['RADICADO_ASOCIADO']);
                $anexo->anex_salida = 1;
                $anexo->radi_nume_salida = $borra_anex['RADICADO_ASOCIADO'];
                $anexo->anex_estado = $anex_estado;
                $anexo->anex_solo_lect = "'S'";
                $anexo->anex_nomb_archivo = $radicado_asociado_path;
                $anexo->anex_tipo = 7;
                $anexo->anex_radi_fech = $radicado_asociado_fecha;
                $anexo->anex_creador = "'" . $_SESSION['usua_login'] . "'";
                $anexo->sgd_rem_destino = 1;
                $anexo->sgd_dir_tipo = 1;
                $anexo->anex_tamano = NULL;
                $anexo->anex_borrado = "'N'";
                $anexo->anex_depe_creador = $_SESSION['dependencia'];
                $anexo->sgd_tpr_codigo = 0;
                $anexo->anex_origen = 3;
                $anexo->anex_ubic = "'BORRADOR'";
                $anexo->usua_doc = $_SESSION['usua_doc'];
                $anexo->anex_fech_anex = $radicado_asociado_fecha;
                $anexo->anex_regenerado = 1; 

                $auxnumero = $anexo->obtenerMaximoNumeroAnexo($borra_anex['RADICADO_ASOCIADO']);

                //verifica que ya este radicado no tenga un anexo para registrado para este radicado asociado
                if (count($helperBorra->getAnexByRadByAsod($numeRadiAscOrigen != "null" ? $numeRadiAscOrigen : $noRad, $borra_anex['RADICADO_ASOCIADO'])) < 1) {
                    $anexoCodigo = $anexo->anexarFilaRadicado($auxnumero);
                }

                $radicadoarrayAsociad = Array();
                $radicadoarrayAsociad[0] = $borra_anex['RADICADO_ASOCIADO'];
                $a = $Tx->insertarHistorico($radicadoarrayAsociad, $_SESSION['dependencia'], $_SESSION['codusuario'],
                    $_SESSION['dependencia'], $_SESSION['codusuario'], $texto, 85,
                    NULL);

            }

            if ($numeRadiAscOrigen != "null") {
                //si tiene un radicado origen, le hago el historial
                //inserto un historial para el radicado origen

                $numeroradi = $numeRadiAscOrigen;
                $radicadoarrayOrig = Array();
                $radicadoarrayOrig[0] = $numeroradi;
                $texto = "Se radicó el borrador [" . $borrador[0]['ID_BORRADOR'] . "] con el número " . $noRad . ": " . $borrador[0]['ASUNTO'];
                $texto = substr($texto, 0, 4000);
                $a = $Tx->insertarHistorico($radicadoarrayOrig, $_SESSION['dependencia'], $_SESSION['codusuario'],
                    $_SESSION['dependencia'], $_SESSION['codusuario'], $texto, 85,
                    NULL);
            }


            //////////////EMPIEZO CON LA TABLA BORRADOR_ANEXOS/////////////
            $query = "SELECT  *
                FROM borrador_anexos where id_borrador='" . $_POST['borrador_id'] . "' order by id_borrador_anexo ASC";

            $borrador_anexos = $db->conn->selectLimit($query, 10000)->getArray();


            foreach ($borrador_anexos as $borra_anex) {

                $pathinfo = pathinfo($borra_anex['ANEXO_NOMBRE']);

                $anexo_tipo = pathinfo($borra_anex['ANEXO_RUTA']);
                $anexo_tipo = $anexo_tipo['extension'];

                $bytes = filesize($borra_anex['ANEXO_RUTA']);
                $kb = formatSizeUnits($bytes);
                $kb = str_replace(",", "", $kb);

                $anexo = new Anexo($db);
                $anexo->anexoExtension = $anexo_tipo;
                $anexo->anex_radi_nume = $numeRadiAscOrigen != "null" ? $numeRadiAscOrigen : $noRad;
                $anexo->sgd_trad_codigo = NULL;
                $anexo->anex_desc = $pathinfo['filename'];
                $anexo->anex_numero = "";//$borra_anex['ANEXO_ORDEN'];
                $anexo->anex_salida = 0;
                $anexo->radi_nume_salida = "";
                $anexo->anex_estado = 0;
                $anexo->anex_solo_lect = "'N'";
                $anexo->anex_codigo = "";//son automaticos
                $anexo->anex_radi_fech = $infoRadicado['fechaRadicacion'];
                $anexo->anex_creador = "'" . $_SESSION['usua_login'] . "'";
                $anexo->sgd_rem_destino = 1;
                $anexo->sgd_dir_tipo = 1;
                $anexo->anex_tamano = $kb; //pendiente
                $anexo->anex_borrado = "'N'";
                $anexo->anex_depe_creador = $_SESSION['dependencia'];
                $anexo->sgd_tpr_codigo = 0;
                $anexo->anex_origen = 3;
                $anexo->anex_ubic = "'Desde Borrador'";
                $anexo->usua_doc = $_SESSION['usua_doc'];
                $anexo->anex_fech_anex = $borra_anex['ANEXO_FECHA'];
                $auxnumero = $anexo->obtenerMaximoNumeroAnexo($numeRadiAscOrigen != "null" ? $numeRadiAscOrigen : $noRad);

                $anexoCodigo = $anexo->anexarFilaRadicado($auxnumero);

                $name = trim($numeRadiAscOrigen != "null" ? $numeRadiAscOrigen : $noRad) . '_' . trim(str_pad($auxnumero + 1, 5, "0",
                        STR_PAD_LEFT)) . "." . $pathinfo['extension'];


                //si se esta radicando desde documentos, entonces les cambio la ruta d elos anexos, a la ruta del radicado padre
                if ($numeRadiAscOrigen != "null") {
                    $year = substr($numeRadiAscOrigen, 0, 4);
                    $ruta = "../../bodega/" . $year . "/" . $inforadPadre['depe_codi'] . "/docs/";
                }

                if ($_SESSION['borradorremove'] == 1) {
                    rename($borra_anex['ANEXO_RUTA'], $ruta . $name); //copio el anexo a esta ruta para que se puedan abrir desde la aplicacion
                } else {
                    copy($borra_anex['ANEXO_RUTA'], $ruta . $name);
                }
            }


            foreach ($expedientes as $expediente) {

                $numeroexpediente = $expediente["SGD_EXP_NUMERO"];
                $codi = $_SESSION["codusuario"];
                $dependencia = $_SESSION['dependencia'];
                $usua_doc = $_SESSION["usua_doc"];

                $fechaahora = date("Y-m-d H:i:s");
                $borrador_id = $_POST['borrador_id'];
                $query = "Insert Into sgd_exp_expediente
                            (sgd_exp_numero, radi_nume_radi, sgd_exp_fech, depe_codi, usua_codi, usua_doc, sgd_exp_estado, sgd_exp_asunto) 
                            VALUES ('$numeroexpediente', '$noRad', TO_TIMESTAMP('$fechaahora', 'YYYY/MM/DD HH24:MI:SS'), '$dependencia', '$codi', 
                            '$usua_doc', 0, 'Insertado desde Borrador B$borrador_id' );
                          ";
                $db->conn->execute($query);

                $query_he = "Insert Into sgd_hfld_histflujodoc (sgd_exp_numero, sgd_fexp_codigo, sgd_hfld_fech, radi_nume_radi, usua_doc, usua_codi, depe_codi, sgd_ttr_codigo, sgd_hfld_observa)
                          VALUES ('$numeroexpediente', 0, TO_TIMESTAMP('$fechaahora', 'YYYY/MM/DD HH24:MI:SS'), '$noRad', '$usua_doc', '$codi', '$dependencia', '53', 'Incluido radicado en Expediente desde Borradores.' );
                          ";
                $db->conn->execute($query_he);

                $Tx->insertarHistorico($radicadoarray, $_SESSION['dependencia'], $_SESSION['codusuario'],
                    $_SESSION['dependencia'], $_SESSION['codusuario'],
                    "Radicado: " . $noRad . " incluido en Expediente: " . $expediente["SGD_EXP_NUMERO"], 53,
                    NULL);

            }


            /*$query = "delete }exos where id_borrador='" . $post['borrador_id'] . "' ";
            $db->conn->execute($query);
            $query = "delete from borrador_historicos where id_borrador='" . $post['borrador_id'] . "' ";
            $db->conn->execute($query);
            $query = "delete from borrador_radicados_asociados where id_borrador='" . $post['borrador_id'] . "' ";
            $db->conn->execute($query);
            $query = "delete from borrador_expedientes where id_borrador='" . $post['borrador_id'] . "' ";
            $response = $db->conn->execute($query);

           /* $query = "delete from borrador_firmantes where id_borrador='" . $post['borrador_id'] . "' ";
            $response = $db->conn->execute($query);*/


            $query = "update borrador_firmantes set  radi_nume=$noRad where id_borrador='" . $post['borrador_id'] . "' ";
            $response = $db->conn->execute($query);
            $query = "update borradores set radi_nume=$noRad where id_borrador='" . $post['borrador_id'] . "' ";


            $response = $db->conn->execute($query);

            if ($_SESSION['borradorremove'] == 1) {
                $query = "update borradores set id_estado=7 where id_borrador='" . $post['borrador_id'] . "' ";
                $response = $db->conn->execute($query);

            }


            $retorno['success'] = $numeRadiAscOrigen != "null" ? $numeRadiAscOrigen : $noRad;


        } else {
            $retorno['error'] = 'No se pudo radicar este borrador';
        }

        echo json_encode($retorno);
    } catch (\Exception $e) {
        return $e->getMessage();
    }

}

//genera el documento del radicado
function generardocRadicado($borrador, $noRad, $db, $ruta, $rutaRadipacht,
                            $historico, $helperBorra, $firmantes, $titulomemo, $infoRadicado, $configHelper)
{

    $crear_doc=false;

    //si no tiene documento guardado, le genero un documento en pdf
    if ($borrador[0]['DOCUMENTO_FORMATO'] == "EditorWeb") {

        //si es EditorWeb y es INTERNA_SIN_PLANTILLA, no le creo ningun archivo.
        if ($borrador[0]['TIPO_BORRADOR_DEST'] == $_SESSION['INTERNA_SIN_PLANTILLA']) {
            $primerAnexoNombre = 'null';
            $documento_ruta = 'null';
        } else {

            //$borrador[0]['DOCUMENTO_WEB_CUERPO'] = str_replace('^^', '"', $borrador[0]['DOCUMENTO_WEB_CUERPO']);
            //$borrador[0]['DOCUMENTO_WEB_CUERPO'] = str_replace('++', "'", $borrador[0]['DOCUMENTO_WEB_CUERPO']);
            //$borrador[0]['DOCUMENTO_WEB_CUERPO'] = stripslashes($borrador[0]['DOCUMENTO_WEB_CUERPO']);

            ///////guardo la direccion, y busco los datos en las tablas, dependiendo del tipo de radicado
            $datosDestinatario = $helperBorra->datosDestinatario($borrador, $db);

            //creo la imagen con el codigo de barra
            crearImgBarra($noRad);

            $imprimefirmantes = ''; //si declaro vacio, quiere decir que va a hacer las validaciones correspondientes
            $mpdf = array();
            $firmantes = $helperBorra->getFirmByBorrador($_POST['borrador_id']);//al radicar, no se muestran los firmantes en el pdf

            $mpdf = $helperBorra->armarPdfRadicado('radicado', $noRad, $borrador, $datosDestinatario,
                $firmantes, $titulomemo, false, $imprimefirmantes, $infoRadicado['fechaRadicacion']);


            if($crear_doc===true){
                $section = array();
                $phpWord = new \PhpOffice\PhpWord\PhpWord();
                \PhpOffice\PhpWord\Settings::setZipClass(\PhpOffice\PhpWord\Settings::PCLZIP);
                $phpWord->setDefaultParagraphStyle(
                    array(
                        //'alignment' => \PhpOffice\PhpWord\SimpleType\Jc::LEFT,
                        'spaceAfter' => \PhpOffice\PhpWord\Shared\Converter::pointToTwip(3),
                        'spacing' => 10,
                        'lineHeight' => 1,
                        'align' => 'both',
                        'space' => 1
                    )
                );
                $phpWord->setDefaultFontSize($_SESSION['EDITOR_FUENTE_TAMANIO'] != 'SELECCIONE' ? $_SESSION['EDITOR_FUENTE_TAMANIO'] : 10);
                $phpWord->setDefaultFontName($_SESSION['EDITOR_FUENTE_TIPO'] != 'SELECCIONE' ? $_SESSION['EDITOR_FUENTE_TIPO'] : 'Arial');


                $section = $helperBorra->armarDocxRadicado('radicado', $noRad, $borrador, $datosDestinatario,
                    $phpWord, $firmantes, $titulomemo, $imprimefirmantes);
            }


            /**
             * armo los revisó, proyectó, aprobó etc
             */
            $mpdf = $helperBorra->armarAprobRevisProy($mpdf, $historico, $helperBorra, 'mpdf',
                array(), $configHelper);

            if($crear_doc===true) {
                $section = $helperBorra->armarAprobRevisProy($section, $historico, $helperBorra, 'phpword',
                    array(), $configHelper);
            }


            /**
             * las 2 siguientes expresiones, arman los anexos folios y folios comunicacion
             */
            if($crear_doc===true) {
                $section = $helperBorra->armarAnexosFolInfo($section, $name_library = 'phpword', $borrador, $helperBorra);
            }
            $mpdf = $helperBorra->armarAnexosFolInfo($mpdf, $name_library = 'mpdf', $borrador,$helperBorra);
            /************************************************
             */

            $random = rand(0, 9);
            $nameDocumRadi = "p_" . $_POST['borrador_id'] . "_" . str_replace(".", "_", $_SESSION['usua_login']) . "_" . date('YmdHis') . $random;

            $rutaPdfRadi = $ruta . $nameDocumRadi . ".pdf";
            $guardarPdf = terminarPdfRadicado($mpdf, $nameDocumRadi, $ruta);
            $totalpaginas = $guardarPdf['totalpaginas'];

            if($crear_doc===true) {
                $rutaDocRadi = $helperBorra->terminarDocxRadicado('radicado',
                    $section, $noRad, $ruta, $nameDocumRadi, $phpWord,
                    $infoRadicado['fechaRadicacion']);
            }


            /////////////////////////////////////////////////////
            /*el documentod el radicado si va en pdf, pero el anexo 0001 es el que va en docx*/
            //$name = $noRad . "_00001" . ".pdf";
            $name = $noRad . "_00001" . ".docx";

            if($crear_doc===true) {
                //como arriba ya genere el .doc, aqui lo convierto a 001 para mandarlo al anexo
                rename($rutaDocRadi, $ruta . $name); //copio el anexo a esta ruta para que se puedan abrir desde la aplicacion
            }
            $primerAnexoNombre = $name;
            $bytes = filesize($rutaPdfRadi);
            $kb = formatSizeUnits($bytes);
            $kb = str_replace(",", "", $kb);
            //envio la ruta del pdf ya que es el documento principal del radicado
            $documento_ruta = $rutaRadipacht . $nameDocumRadi . ".pdf";
        }
    } else {
        //si ya tiene un documento guardado,

        $anexo_tipo = pathinfo($borrador[0]['DOCUMENTO_RUTA']);
        $anexo_tipo = $anexo_tipo['extension'];
        $ext = $anexo_tipo;
        $name = $noRad . "_00001" . "." . $ext;
        $primerAnexoNombre = $name;

        $documento_ruta = $rutaRadipacht . $name;
        rename($borrador[0]['DOCUMENTO_RUTA'], $ruta . $name); //muevo el anexo a esta ruta para que se puedan abrir desde la aplicacion
        $bytes = filesize($ruta . $name);
        $kb = formatSizeUnits($bytes);
        $kb = str_replace(",", "", $kb);
    }

    return array(
        'kb' => $kb,
        'documento_ruta' => $documento_ruta,
        'primerAnexoNombre' => $primerAnexoNombre
    );

}

function terminarPdfRadicado($mpdf, $nameDocumRadi, $ruta)
{

    $rutaPdfRadi = $ruta . $nameDocumRadi . ".pdf";
    $totalpaginas = $mpdf->page;
    //genero el pdf del radicado, que es el que se va a guardar en radi_pacth
    $mpdf->Output($rutaPdfRadi);

    return array('totalpaginas' => $totalpaginas);

}


function crearImgBarra($noRad)
{
    $noRad = '*' . $noRad . '*';
    $ruta_raiz = "../../";
    // Establecer el tipo de contenido
// Crear la imagen
    $im = imagecreatetruecolor(350, 30);

// Crear algunos colores
    $blanco = imagecolorallocate($im, 255, 255, 255);
    //$gris = imagecolorallocate($im, 128, 128, 128);
    $negro = imagecolorallocate($im, 0, 0, 0);
    imagefilledrectangle($im, 0, 0, 399, 29, $blanco);

// El texto a dibujar
    $texto = $noRad;
// Reemplace la ruta por la de su propia fuente
    $fuente = '../../includes/ttf';
    $fuente = '../../include/fuentes/code3of9_regular.ttf';
// Añadir algo de sombra al texto
    // imagettftext($im, 30, 0, 11, 21, $gris, $fuente, $texto);

// Añadir el texto
    imagettftext($im, 30, 0, 10, 20, $negro, $fuente, $texto);

// Usar imagepng() resultará en un texto más claro comparado con imagejpeg()

    imagepng($im, $ruta_raiz . "bodega/imagenbarra.png");

    imagedestroy($im);
    imagedestroy($ruta_raiz . "bodega/imagenbarra.png");
}


/*esta funcion, cierra todas las etiquetas html no cerradas*/
function closetags($html)
{
    preg_match_all('#<([a-z]+)(?: .*)?(?<![/|/ ])>#iU', $html, $result);
    $openedtags = $result[1];
    var_dump($result);
    preg_match_all('#</([a-z]+)>#iU', $html, $result);
    $closedtags = $result[1];
    var_dump($result);
    $len_opened = count($openedtags);


    if (count($closedtags) == $len_opened) {
        return $html;
    }
    var_dump("paso");
    $openedtags = array_reverse($openedtags);
    for ($i = 0; $i < $len_opened; $i++) {
        if (!in_array($openedtags[$i], $closedtags)) {
            $html .= '</' . $openedtags[$i] . '>';
        } else {
            unset($closedtags[array_search($openedtags[$i], $closedtags)]);
        }
    }
    return $html;
}

function getOpennedTags($string, $removeInclompleteTagEndTagIfExists = true)
{

    $tags = array();
    $tagOpened = false;
    $tagName = '';
    $tagNameLogged = false;
    $closingTag = false;

    foreach (str_split($string) as $c) {
        if ($tagOpened && $c == '>') {
            $tagOpened = false;
            if ($closingTag) {
                array_pop($tags);
                $closingTag = false;
                $tagName = '';
            }
            if ($tagName) {
                array_push($tags, $tagName);
            }
        }
        if ($tagOpened && $c == ' ') {
            $tagNameLogged = true;
        }
        if ($tagOpened && $c == '/') {
            if ($tagName) {
                //orphan tag
                $tagOpened = false;
                $tagName = '';
            } else {
                //closingTag
                $closingTag = true;
            }
        }
        if ($tagOpened && !$tagNameLogged) {
            $tagName .= $c;
        }
        if (!$tagOpened && $c == '<') {
            $tagNameLogged = false;
            $tagName = '';
            $tagOpened = true;
            $closingTag = false;
        }
    }

    if ($removeInclompleteTagEndTagIfExists && $tagOpened) {
        // an tag has been cut for exemaple ' blabh blah <a href="sdfoefzofk' so closing the tag will not help...
        // let's remove this ugly piece of tag
        $pos = strrpos($string, '<');
        $string = substr($string, 0, $pos);
    }

    return $tags;
}

function cantidadBorradoresUsu()
{
    require_once '../../config.php';
    $json = array();
    $borra = new \App\Helpers\Borradores();
    $cantidad = $borra->cantidadBorradoresUsu();

    $json['amicargo'] = $cantidad;
    $_SESSION["borradores"] = $cantidad;
    echo json_encode($json);
}

//trae un mes en espaol
function mesespanol($mes)
{
    if ($mes == "January") {
        $mes = "Enero";
    }
    if ($mes == "February") {
        $mes = "Febrero";
    }
    if ($mes == "March") {
        $mes = "Marzo";
    }
    if ($mes == "April") {
        $mes = "Abril";
    }
    if ($mes == "May") {
        $mes = "Mayo";
    }
    if ($mes == "June") {
        $mes = "Junio";
    }
    if ($mes == "July") {
        $mes = "Julio";
    }
    if ($mes == "August") {
        $mes = "Agosto";
    }
    if ($mes == "September") {
        $mes = "Septiembre";
    }
    if ($mes == "October") {
        $mes = "Octubre";
    }
    if ($mes == "November") {
        $mes = "Noviembre";
    }
    if ($mes == "December") {
        $mes = "Diciembre";
    }

    return $mes;
}

function addexpedienteborraEntrada()
{

    require_once '../../config.php';

    ini_set('display_errors', 0);
    ini_set('display_startup_errors', 0);
    error_reporting(E_ALL);
    $ruta_raiz = "../../";
    include("$ruta_raiz/config.php");
    if (isset($db)) unset($db);
    include_once("$ruta_raiz/include/db/ConnectionHandler.php");
    $db = new ConnectionHandler("$ruta_raiz");
    $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
    $ruta_raiz = "../../";

    require_once("$ruta_raiz/include/tx/Tx.php");
    require_once($ruta_raiz . "class_control/anexo.php");
    include($ruta_raiz . "fpdf/html2pdf.php");

    $borra = new \App\Helpers\Borradores($db);

    $noRad = $_POST["radicado"];
    //$inforadicado=$borra->getRadicadoByNume_radi($noRad);

    $tipologia = false;
    if (isset($_POST['idtipologia']) && $_POST['idtipologia'] != "") {
        $tipologia = $_POST['idtipologia'];
    }

    //actualizo la tipologia
    if ($tipologia != false) {
        $query = "update radicado set tdoc_codi=$tipologia where radi_nume_radi=$noRad  ";
        $db->conn->execute($query);
    }

    $numeroexpediente = $_POST["expediente"];
    $codi = $_SESSION["codusuario"];
    $dependencia = $_SESSION['dependencia'];
    $usua_doc = $_SESSION["usua_doc"];

    $fechaahora = date("Y-m-d H:i:s");

    $yaestaentabla = $borra->getExpedBorradorEntrada($noRad, $numeroexpediente);
    if (!isset($yaestaentabla[0])) {
        $query = "Insert Into sgd_exp_expediente
                            (sgd_exp_numero, radi_nume_radi, sgd_exp_fech, depe_codi, usua_codi, usua_doc, sgd_exp_estado, sgd_exp_asunto) 
                            VALUES ('$numeroexpediente', '$noRad', TO_TIMESTAMP('$fechaahora', 'YYYY/MM/DD HH24:MI:SS'), '$dependencia', '$codi', 
                            '$usua_doc', 0, 'Insertado desde Expediente' );
                          ";
        $db->conn->execute($query);
    } else {
        $query = "update sgd_exp_expediente set depe_codi='$dependencia', usua_codi='$codi', usua_doc='$usua_doc',sgd_exp_estado=0  
where sgd_exp_numero='$numeroexpediente'  and  radi_nume_radi='$noRad' ";
        $db->conn->execute($query);
    }

    $query_he = "Insert Into sgd_hfld_histflujodoc (sgd_exp_numero, sgd_fexp_codigo, sgd_hfld_fech, radi_nume_radi, usua_doc, usua_codi, depe_codi, sgd_ttr_codigo, sgd_hfld_observa) 
                          VALUES ('$numeroexpediente', 0, TO_TIMESTAMP('$fechaahora', 'YYYY/MM/DD HH24:MI:SS'), '$noRad', '$usua_doc', '$codi', '$dependencia', '53', 'Incluido radicado en Expediente desde Borradores.' );
                          ";
    $db->conn->execute($query_he);

    $radicadoarray = Array();
    $radicadoarray[0] = $noRad;
    $Tx = new Tx($db);
    $Tx->insertarHistorico($radicadoarray, $_SESSION['dependencia'], $_SESSION['codusuario'],
        $_SESSION['dependencia'], $_SESSION['codusuario'],
        "Radicado: " . $noRad . " incluido en Expediente: " . $numeroexpediente, 53,
        NULL);

    //busco los que no estan borrados, es decir, sgd_exp_estado!=2
    $get = $borra->getExpedNotDeleted($noRad);
    $json = array();
    $json['success'] = "Incluido en expediente | Ahora elija el Tipo Documental";

    $misexped = armarMisExpedientesEntrada($get, $borra, $tipologia);
    //busco mis expedientes
    $json['misexped'] = $misexped;

    echo json_encode($json);
}


function addexpedienteborra()
{

    require_once '../../config.php';

    $borra = new \App\Helpers\Borradores();

    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);


    $get = $borra->getExpedBorra($_POST['borrador_id'], $_POST['expediente'], false);

    $add = false;
    $json = array();

    $tipologia = false;
    if (isset($_POST['idtipologia']) && $_POST['idtipologia'] != "") {
        $tipologia = $_POST['idtipologia'];
    }

    if (!isset($get[0]['ID_BORRADOR_EXPEDIENTES'])) {

        $add = $borra->addExpeBorrador($_POST['borrador_id'], $_POST['expediente'], $tipologia);

        $get = $borra->getExpedBorrador($_POST['borrador_id']);
        $json['success'] = "Incluido en expediente | Ahora elija el Tipo Documental";
        $misexped = armarMisExpedientes($get, $borra);
        //busco mis expedientes
        $json['misexped'] = $misexped;
    } else {
        $json['yaestabaincluido'] =true;
        $json['success'] = "Ya el expediente fué incluido al borrador";
    }


    echo json_encode($json);
}

function excluirexpedienteborra()
{

    require_once '../../config.php';

    $borra = new \App\Helpers\Borradores();

    $get = $borra->getExpedBorra($_POST['borrador_id'], $_POST['expediente'], false);

    $add = false;
    $json = array();
    if (isset($get[0]['ID_BORRADOR_EXPEDIENTES'])) {

        $add = $borra->excluirExpediente($_POST['borrador_id'], $_POST['expediente']);
        $json['success'] = "Excluido con éxito";

        $get = $borra->getExpedBorrador($_POST['borrador_id']);
        $misexped = armarMisExpedientes($get, $borra);
        //busco mis expedientes
        $json['misexped'] = $misexped;

    } else {
        $json['error'] = "El expediente no ha sido incluido a este borrador";
    }

    echo json_encode($json);

}


function getExpedBorrador()
{

    require_once '../../config.php';

    $borra = new \App\Helpers\Borradores();

    $get = $borra->getExpedBorrador($_POST['borrador_id']);
    echo json_encode($get);
}

function estaincluidoexpediente()
{

    //valida si ya esta incluido el borrador a un expediente
    require_once '../../config.php';


    $borra = new \App\Helpers\Borradores();
    $tipologia = false;
    if (isset($_POST['idtipologia'])) {
        $tipologia = $_POST['idtipologia'];
    }

    $get = $borra->getExpedBorra($_POST['borrador_id'], $_POST['expediente'], $tipologia);

    $json = array();
    if (!isset($get[0]['ID_BORRADOR_EXPEDIENTES'])) {
        $json['success'] = "no incluido";
    } else {
        $json['success'] = "incluido";
    }

    echo json_encode($json);
}


function estaincluidoexpedienteEntrada()
{

    //valida si ya esta incluido el borrador a un expediente
    require_once '../../config.php';

    $borra = new \App\Helpers\Borradores();
    $get = $borra->getExpedBorradorEntrada($_POST['radicado'], $_POST['expediente']);

    $json = array();
    if (isset($get[0]['RADI_NUME_RADI']) && $get[0]['SGD_EXP_ESTADO'] != 2) {
        $json['success'] = "incluido";

    } else {
        $json['success'] = "no incluido";
    }

    echo json_encode($json);
}


function incluircontipologia()
{

    //valida si ya esta incluido el borrador a un expediente
    require_once '../../config.php';

    $borra = new \App\Helpers\Borradores();

    $expedientes = $borra->getExpedBorrador($_POST['borrador_id']);
    $idtipologia = $_POST['idtipologia'];
    $error = false;
    $json = array();
    foreach ($expedientes as $row) {

        $update = $borra->updExpeTipBorrador($_POST['borrador_id'], $row['SGD_EXP_NUMERO'], $idtipologia);

        if ($update == false) {
            $error = true;
        }

    }

    if ($error == false) {
        $get = $borra->getExpedBorrador($_POST['borrador_id']);
        $json['success'] = "Expedientes Actualizados";


    } else {
        $json['error'] = "Ocurrio un error al actualizar el expediente";
    }

    echo json_encode($json);
}

function incluircontipologiaEntrada()
{

    require_once '../../config.php';


    $ruta_raiz = "../../";
    if (isset($db)) unset($db);
    include_once("$ruta_raiz/include/db/ConnectionHandler.php");
    $db = new ConnectionHandler("$ruta_raiz");
    $borra = new \App\Helpers\Borradores($db);

    $radicado = $_POST['radicado'];
    $tipologia = $_POST['idtipologia'];
    $error = true;
    $json = array();

    //actualizo la tipologia
    if ($tipologia == false || $tipologia == null || $tipologia == "null" || $tipologia == "NULL") {
        $tipologia = 0;
    }
    $query = "update radicado set tdoc_codi=$tipologia where radi_nume_radi=$radicado  ";


    $db->conn->execute($query);
    $error = false;


    if ($error == false) {

        $json['success'] = "Tipologías Actualizadas";

    } else {
        $json['error'] = "Ocurrio un error al actualizar las Tipologías";
    }

    echo json_encode($json);
}


//actualiza las tipologias a los radicados que vienen en arrelgo
function updateTipoloTodosRad()
{

    require_once '../../config.php';
    $ruta_raiz = "../../";
    if (isset($db)) unset($db);
    include_once("$ruta_raiz/include/db/ConnectionHandlerNew.php");
    $db = new ConnectionHandler("$ruta_raiz");
    $borra = new \App\Helpers\Borradores($db);

    $radicados = $_POST['radicados'];
    $tipologia = $_POST['idtipologia'];
    $error = true;
    $json = array();


    //actualizo la tipologia
    if (count($radicados) > 0) {

        foreach ($radicados as $row) {

            //actualizo la tipologia
            if ($tipologia == false || $tipologia == null || $tipologia == "null" || $tipologia == "NULL") {
                $tipologia = 0;
            }
            $rad = $row['radicado'];
            $query = "update radicado set tdoc_codi=$tipologia where radi_nume_radi=$rad ";

            $db->conn->execute($query);
            $error = false;

        }
    }
    if ($error == false) {
        $json['success'] = "Tipologías Actualizadas";
    } else {
        $json['error'] = "Ocurrió un error al actualizar las Tipologías";
    }

    echo json_encode($json);
}

function busqAvaExpediente()
{


    //trae los datos para la busqueda avanzada en el modal de expediente
    require_once '../../config.php';

    $borra = new \App\Helpers\Borradores();

    $dependencias = $borra->getDependExped();
    $anios = array();
    if ($_POST['dependencia'] != "" && $_POST['serie'] != "" && $_POST['subserie'] != "") {
        $anios = $borra->getAnioExpedDepen($_POST['dependencia'], $_POST['serie'], $_POST['subserie']);
    }
    $series = $borra->getSerieExped($_POST['dependencia']);

    $subseries = array();
    if ($_POST['dependencia'] != "" && $_POST['serie'] != "") {
        $subseries = $borra->getSubSerieExped($_POST['dependencia'], $_POST['serie']);
    }


    $json = array();
    $json['dependencias'] = $dependencias;

    $moduloEstoy = $_POST['estoyen']; //CREAREXPEDSINRAD

    if ($moduloEstoy == 'CREAREXPEDSINRAD') {
        //si solicito estos valores dese el modulo de crear expedientes sin radicado

        //verifico si en el arreglo de los años, estan el año anterior al actual y el año actual,
        //si no es asi, los agrego al arreglo de años (se agregaran al final)
        if (!in_array(array('AÑO' => (date('Y') - 1)), $anios)) {
            $anios = array_merge($anios, array(0 => array('AÑO' => (date('Y') - 1))));
        }
        if (!in_array(array('AÑO' => date('Y')), $anios)) {
            $anios = array_merge($anios, array(0 => array('AÑO' => (date('Y')))));
        }

        for ($i = 0; $i < count($anios); $i++) {
            for ($j = 0; $j < count($anios) - 1; $j++) {
                //luego los recorro

                //si el año del sql es menor al año que estoy pasando
                if ($anios[$j]['AÑO'] < $anios[$j + 1]['AÑO']) {
                    $tempmasuno = $anios[$j]['AÑO'];
                    $anios[$j]['AÑO'] = $anios[$j + 1]['AÑO'];
                    $anios[$j + 1]['AÑO'] = $tempmasuno;
                }

            }
        }
    }

    $json['anios'] = $anios;
    $json['series'] = $series;
    $json['subseries'] = $subseries;

    //$anioshtml = "";
    /*for ($anio = date('Y'); $anio >= (date('Y') - 20); $anio--) {
        $anioshtml .= "<option value='" . $anio . "'> " . $anio . "</option>";
    }
    $json['anios'] = $anioshtml;*/
    echo json_encode($json);
}

function resultAvanzados()
{

    require_once '../../config.php';
    $ruta_raiz = "../../";
    if (isset($db)) unset($db);
    include_once("$ruta_raiz/include/db/ConnectionHandlerNew.php");
    $db = new ConnectionHandler("$ruta_raiz");
    $configsHelper = new \App\Helpers\Configuraciones($db);
    $limite = $configsHelper->getConfiguraciones("'LIMIT_BUSQ_AVANZ_EXPED'");
    $limit = $limite[0]['VALOR'] + 0;
    $limit = 10000;//en busqueda avanzada dejo quemado el limit a 10000

    $borra = new \App\Helpers\Borradores($db);


    $expedientes = $borra->get_expedientes(false, false, $_POST['dependencia'] != "" ? "'" . $_POST['dependencia'] . "'" : false,
        $_POST['serie'] != "" ? $_POST['serie'] : false, $_POST['subserie'] != "" ? $_POST['subserie'] : false,
        $_POST['anio'] != "" ? $_POST['anio'] : false, false, 10000, false);


    $borrador_id = $_POST['borrador'];

    for ($i = 0; $i < count($expedientes); $i++) {
        $get = $borra->getExpedBorra($borrador_id, $expedientes[$i]['NUMERO_EXPEDIENTE'], false);

        if (isset($get[0])) {
            $expedientes[$i]['incluiExpe'] = "incluido";
        } else {
            $expedientes[$i]['incluiExpe'] = "no incluido";
        }
    }

    $json = array();
    $json = $expedientes;

    echo json_encode($json);
}


function resultAvanzadosEntrada()
{

    require_once '../../config.php';
    $ruta_raiz = "../../";
    if (isset($db)) unset($db);
    include_once("$ruta_raiz/include/db/ConnectionHandlerNew.php");
    $db = new ConnectionHandler("$ruta_raiz");
    $configsHelper = new \App\Helpers\Configuraciones($db);
    $limite = $configsHelper->getConfiguraciones("'LIMIT_BUSQ_AVANZ_EXPED'");
    $limit = $limite[0]['VALOR'] + 0;
    $limit = 10000;//en busqueda avanzada dejo quemado el limit a 10000
    $borra = new \App\Helpers\Borradores($db);

    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

    $expedientes = $borra->get_expedientes(false, false, $_POST['dependencia'] != "" ? "'" . $_POST['dependencia'] . "'" : false,
        $_POST['serie'] != "" ? $_POST['serie'] : false, $_POST['subserie'] != "" ? $_POST['subserie'] : false,
        $_POST['anio'] != "" ? $_POST['anio'] : false, false, $limit, false);


    $radicado = $_POST['radicado'];

    for ($i = 0; $i < count($expedientes); $i++) {
        $get = $borra->getExpedBorradorEntrada($radicado, $expedientes[$i]['NUMERO_EXPEDIENTE']);
        if (isset($get[0]) && $get[0]['SGD_EXP_ESTADO'] != 2) {
            $expedientes[$i]['incluiExpe'] = "incluido";
        } else {
            $expedientes[$i]['incluiExpe'] = "no incluido";
        }

    }

    $json = array();
    $json = $expedientes;

    echo json_encode($json);
}


function buscarExpedRadAso()
{

    require_once '../../config.php';
    $ruta_raiz = "../../";
    if (isset($db)) unset($db);
    include_once("$ruta_raiz/include/db/ConnectionHandlerNew.php");
    $db = new ConnectionHandler("$ruta_raiz");
    $configsHelper = new \App\Helpers\Configuraciones($db);
    $limite = $configsHelper->getConfiguraciones("'LIMIT_BUSQ_AVANZ_EXPED'");
    $limit = $limite[0]['VALOR'] + 0;
    $borra = new \App\Helpers\Borradores($db);

    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

    $borrador_id = $_POST['borrador_id'];
    $radicados_aso = $borra->get_radicadosAsociados($borrador_id);

    $json = array();
    $expedientes = array();
    if (count($radicados_aso) > 0) {

        //recorro los radicados asociados a este borrador
        foreach ($radicados_aso as $radAso) {

            //busco si este radicado asociado esta en algun expeidente
            $estaenExpe = $borra->radiEstaEnExpediente($radAso['RADI_NUME_RADI']);
            foreach ($estaenExpe as $esta) {

                //busco los datos del expediente
                $expedientes = $borra->get_expedientes(false, $esta['SGD_EXP_NUMERO'], false,
                    false, false, false, false, $limit, false);

                if (count($expedientes) > 0) {
                    //valido si el borradoractual ya esta incluido en este expediente
                    $get = $borra->getExpedBorra($borrador_id, $expedientes[0]['NUMERO_EXPEDIENTE'], false);

                    if (isset($get[0])) {
                        $expedientes[0]['incluiExpe'] = "incluido";
                    } else {
                        $expedientes[0]['incluiExpe'] = "no incluido";
                    }

                    $json[] = $expedientes[0];
                }

            }


        }
    }

    echo json_encode($json);

}

function actFirmantePrinci()
{
    require_once '../../config.php';
    $borra = new \App\Helpers\Borradores();
    $get = $borra->updateFirmPrincip($_POST['id_borrador'], $_POST['firmante']);
    $json['success'] = true;
    echo json_encode($json);
}


function actFirmantetipo_validador()
{
    require_once '../../config.php';
    $borra = new \App\Helpers\Borradores();

    $get = $borra->updateFirmtipo_validador($_POST['id_borrador'], $_POST['id_firmante'], $_POST['valor']);
    $buscarfirmantes = array();
    $firmantes = $borra->getFirmByBorrador($_POST['id_borrador']);
    if (count($firmantes) > 0) {
        $buscarfirmantes = $borra->barmarUsuariosComen($firmantes);
    }

    echo json_encode($buscarfirmantes);
}


//trae los anexos de un borrador
function getAnexos()
{

    try {
        require_once '../../config.php';

        $borra = new \App\Helpers\Borradores();

        $anexos = $borra->getAnexosBorrador($_POST['borrador_id']);

        $cont = 0;
        foreach ($anexos as $row) {
            $anexo_tipo = pathinfo($row['ANEXO_RUTA']);
            $anexo_tipo = $anexo_tipo['extension'];
            $anexos[$cont]['EXTENSION'] = "";
            $anexos[$cont]['EXTENSION'] = $anexo_tipo;
            $cont++;
        }

        $json['anexos'] = $anexos;
        echo json_encode($json);
    } catch (\Exception $e) {
        return $e->getMessage();
    }
}

function traeExtension()
{
    $json = array();
    $anexo_tipo = pathinfo($_POST['nombre']);
    $anexo_tipo = $anexo_tipo['extension'];
    $json['extension'] = $anexo_tipo;
    echo json_encode($json);
}

function guardarBorrador()
{
    try {

        require '../../vendor/autoload.php';
        require_once '../../config.php';

        $ruta_raiz = "../../";
        if (isset($db)) unset($db);
        include_once("$ruta_raiz/include/db/ConnectionHandler.php");
        $db = new ConnectionHandler("$ruta_raiz");

        ini_set('display_errors', 0);
        ini_set('display_startup_errors', 0);
        error_reporting(E_ALL);

        $borradoresC = new \App\Helpers\Borradores($db);

        $documentoentrar = '';    
        require_once("$ruta_raiz/include/tx/Radicacion.php");
        require_once("$ruta_raiz/include/tx/Historico.php");

        $msg = "";
        $json = array();
        /////////////
        if (isset($_POST["dest"]) && $_POST["dest"] != "") {


            $maxFileSizeStr = ini_get('upload_max_filesize');
            $nombre_archivo = "";

            if(isset($_POST['borrarDocumentoRuta']) && $_POST['borrarDocumentoRuta']=="true"){
                $_POST['documento_ruta']='';
            }
            $documentoentrar = 'no entre'; 
            if (!empty($_FILES) and $_FILES['userfile']['size'] != '0') {
                $documentoentrar = 'entre';
                $ext = pathinfo($_FILES["userfile"]["name"])['extension'];

                $maxFileSize = convertBytes(ini_get('upload_max_filesize'));

                /*if ($ext != "docx") {
                    $json['error'] = "El tipo de archivo debe ser .docx";
                    echo json_encode($json);
                    exit;
                }*/

                if ($_FILES['userfile']['size'] > $maxFileSize) {
                    $errorsizefile = "El tamaño del archivo excede el limite permitido: " . $maxFileSizeStr;
                    $json['error'] = $errorsizefile;
                    echo json_encode($json);
                    exit;
                } else {

                    $ruta = "../../bodega";
                    if (!is_dir("../../bodega")) {

                    } else {
                        if (!is_dir($ruta . "/" . date("Y"))) {
                            $oldmask = umask(0);
                            mkdir("$ruta", 0777);
                            umask($oldmask);
                        }
                        $ruta .= "/" . date("Y");
                        $dependencia = $_SESSION['dependencia'];
                        if (!is_dir($ruta . "/" . $dependencia)) {
                            $oldmask = umask(0);
                            mkdir("$ruta", 0777);
                            umask($oldmask);
                        }
                        $ruta .= "/" . $dependencia;
                        if (!is_dir($ruta . "/docs")) {

                            $ruta .= "/docs";
                            $oldmask = umask(0);
                            mkdir("$ruta", 0777);
                            umask($oldmask);
                        }
                        $ruta .= "/docs/";
                        $userfile1_Temp = $_FILES['userfile']['tmp_name'];

                        $nombre_archivo = "_" . str_replace(".", "_", $_SESSION['usua_login']) . "_" . date('YmdHis') . date('s') . "." . $ext;

                    }
                }

            }

            $retorno = $borradoresC->save_all($_POST);
            $id_borrador = $retorno;
            $json['entrar'] = $documentoentrar;

            if ($nombre_archivo != "") {

                //si entra aqui es porque trajo un archivo y ya guardo un nombre de archhito temporal
                $name = "p_" . $id_borrador . $nombre_archivo;
                
                $bien2 = move_uploaded_file($userfile1_Temp, $ruta . $name); 
                
                $json['docuemntovalores'] = $bien2;
                if ($bien2 == true) { 
                    $_POST["documento_ruta"] = $ruta . $name;
                    $query = " update borradores set documento_ruta='" . $_POST["documento_ruta"] . "' where id_borrador=" . $id_borrador . " ";
                    //executing

                    $response = $db->conn->execute($query);
                    $json['documento'] = $name;
                }
            }

            $fecha_transaccion = $db->conn->OffsetDate(0, $db->conn->sysTimeStamp);
            $id_usuario_origen = $_SESSION["usuario_id"];
            $depe_codi_origen = $_SESSION['dependencia'];
            //busco la dependencia del usuario destino
            //guardo el historico y el comentario
            if ($_POST['borrador_id'] == "") {
                $borradoresC->guardarhistorico($id_borrador, $fecha_transaccion, "Ha sido creado un nuevo borrador", $id_usuario_origen,
                    $depe_codi_origen, 0, 0, 1, 80);
            }

            //busco los que estan en la bd actualmente
            $firmantes = $borradoresC->getFirmByBorrador($id_borrador);
            //borro todos sus firmantes
            $borradoresC->borrarFirmanteByBorrador($id_borrador);

            //si trae firmantes, guardo los firmantes
            globalGuardarFirmantes($_POST, $borradoresC, $id_borrador, $firmantes, $fecha_transaccion);

            $buscarfirmantes = $borradoresC->getOnlyFirmnBorrador($id_borrador);

            //verifico si tiene firmante principal, y si no, se lo asigno al primero que encuentre
            $borradoresC->tieneFirmPrincipal($buscarfirmantes, $id_borrador);

            $msg = "Borrador Creado con Exito.";
            if ($_POST['borrador_id'] != "") {
                $msg = "Borrador " . $_POST['borrador_id'] . " Actualizado con Exito.";
            }

            //vengo de la vista de radicados.
            if ($_POST['radicadoactual'] != "") {

                if ($_POST['borrador_id'] == "") {
                    $_POST['borrador_id'] = $id_borrador;
                }

                //esto es por si el radicado origen esta asociado a algun expediente, este borrador tambien.
                $expedientes = $borradoresC->radiEstaEnExpediente($_POST['radicadoactual']);
                if (count($expedientes) > 0) {

                    foreach ($expedientes as $item) {
                        $expedientes = $borradoresC->getExpedBorra($_POST['borrador_id'], $item['SGD_EXP_NUMERO']);

                        if (!isset($expedientes[0])) {
                            $guardar = $borradoresC->addExpeBorrador($_POST['borrador_id'], $item['SGD_EXP_NUMERO'], false);
                        }
                    }
                }


                //busco si ya fue asociado este radicado a este borrador
                $retorno = $borradoresC->radicadoAsocByBorr($_POST['borrador_id'], $_POST['radicadoactual']);


                if (!isset($retorno[0])) {
                    //si no existe ningun registro, lo guardo

                    //busco el ultimo orden
                    $response = $borradoresC->getLastOrdenIndex($_POST['borrador_id']);
                    $orden = 1;
                    if ($response[0]['MAXIMO'] != "") {
                        $orden = $response[0]['MAXIMO'] + 1;
                    }

                    $radicadocompleto = $borradoresC->getRadicadoByNume_radi($_POST['radicadoactual']);

                    $borradoresC->insertBorrador_radicados_asociados($_POST['borrador_id'], $radicadocompleto[0]['RADI_NUME_RADI'], 1,
                        $orden);


                    //inserto un historial para el radicado origen
                    $radicadoOrigen = $borradoresC->getRadicadoByNume_radi($_POST['radicadoactual']);
                    $noRad = $radicadoOrigen[0]['RADI_NUME_RADI'];
                    $radicadoarray = Array();
                    $radicadoarray[0] = $noRad;
                    $texto = "Se generó el nuevo borrador [" . $_POST['borrador_id'] . "]: " . $_POST['asunto'] . ". Como respuesta o alcance a este radicado.";
                    $Tx = new Historico($db);
                    $a = $Tx->insertarHistorico($radicadoarray, $_SESSION['dependencia'], $_SESSION['codusuario'],
                        $_SESSION['dependencia'], $_SESSION['codusuario'], substr($texto, 0, 4000), 80);

                }

            }


            if (!isset($json['error'])) {
                $json['success'] = $msg;
            }

            $json['borrador_id'] = $id_borrador;

        } else {
            $json['error'] = "Debe seleccionar un destinatario";
        }

        echo json_encode($json);
    } catch (\Exception $e) {
        return $e->getMessage();
    }

}


function buscarMisExpedEntrada()
{


    //el radicado desde entrada
    require_once '../../config.php';
    $ruta_raiz = "../../";
    if (isset($db)) unset($db);
    include_once("$ruta_raiz/include/db/ConnectionHandler.php");
    $db = new ConnectionHandler("$ruta_raiz");
    $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
    $borra = new \App\Helpers\Borradores($db);

    $json = array();

    $radicado = $_POST['radicado'];
    $getExpedNotDeleted = $borra->getExpedNotDeleted($radicado);
    $inforadicado = $borra->getRadicadoByNume_radi($radicado);

    $tipologia = false;
    if (isset($inforadicado[0]['TDOC_CODI']) && $inforadicado[0]['TDOC_CODI'] != "") {
        $tipologia = $inforadicado[0]['TDOC_CODI'];
    }

    $json = armarMisExpedientesEntrada($getExpedNotDeleted, $borra, $tipologia);

    echo json_encode($json);

}


function buscarMisExped()
{

    require_once '../../config.php';

    $borra = new \App\Helpers\Borradores();

    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

    $borrador_id = $_POST['borrador_id'];
    $get = $borra->getExpedBorrador($borrador_id);
    $json = array();
    $json = armarMisExpedientes($get, $borra);

    echo json_encode($json);

}

function armarMisExpedientes($get, $borra)
{ //arma el arreglo de los expedientes
    $expedientes = array();
    $json = array();
    if (count($get) > 0) {

        //recorro los expedientes a este borrador
        foreach ($get as $row) {

            $expedientes = array();
            //busco los datos del expediente
            $expedientes = $borra->get_expedientes(false, $row['SGD_EXP_NUMERO'], false,
                false, false, false, false, 1000, false);
            $expedientes[0]['incluiExpe'] = "incluido";
            $expedientes[0]['DESCRIPCION'] = substr($expedientes[0]['DESCRIPCION'], 0, 50);

            $tiplog = "";
            $tipologiacodigo = "";
            if ($row['SGD_TPR_CODIGO'] != null && $row['SGD_TPR_CODIGO'] != "") {
                $tipologia = $borra->tipologiasByCodigo($row['SGD_TPR_CODIGO']);
                $tiplog = $tipologia[0]['SGD_TPR_DESCRIP'];
                $tipologiacodigo = $row['SGD_TPR_CODIGO'];
            }
            $expedientes[0]['tipologia'] = $tiplog;
            $expedientes[0]['tipologiacodigo'] = $tipologiacodigo;

            $json[] = $expedientes[0];

        }
    }

    return $json;
}


function armarMisExpedientesEntrada($get, $borra, $tipologia = false)
{ //arma el arreglo de los expedientes
    $expedientes = array();
    $json = array();
    if (count($get) > 0) {


        //recorro los expedientes a este borrador
        foreach ($get as $row) {


            $expedientes = array();
            //busco los datos del expediente
            $expedientes = $borra->get_expedientes(false, $row['SGD_EXP_NUMERO'], false,
                false, false, false, false, 1000, false);
            $expedientes[0]['incluiExpe'] = "incluido";

            $tiplog = "";
            $tipologiacodigo = "";

            if ($tipologia != false) {
                $tipologiasearch = $borra->tipologiasByCodigo($tipologia);
                $tiplog = $tipologiasearch[0]['SGD_TPR_DESCRIP'];
                $tipologiacodigo = $tipologia;
            }
            $expedientes[0]['tipologia'] = $tiplog;
            $expedientes[0]['tipologiacodigo'] = $tipologiacodigo;

            $json[] = $expedientes[0];

        }
    }

    return $json;
}


function guardarEditor()
{

    $json = array();
    try {

        require_once '../../config.php';
        $borra = new \App\Helpers\Borradores();

        $borra->updateEditor($_POST);

        echo json_encode(true);
    } catch (\Exception $e) {
        return $e->getMessage();
    }

}


function convertBytes($value)
{
    if (is_numeric($value)) {
        return $value;
    } else {
        $value_length = strlen($value);
        $qty = substr($value, 0, $value_length - 1);
        $unit = strtolower(substr($value, $value_length - 1));
        switch ($unit) {
            case 'k':
                $qty *= 1024;
                break;
            case 'm':
                $qty *= 1048576;
                break;
            case 'g':
                $qty *= 1073741824;
                break;
        }
        return $qty;
    }
}

function actualizar_path_versionamiento($post){
    $json = array();
    try {
        require_once '../../config.php';
        $ruta_raiz = "../../";
        if (isset($db)) unset($db);
        include_once("$ruta_raiz/include/db/ConnectionHandler.php");
        $db = new ConnectionHandler("$ruta_raiz");
        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        $borra = new \App\Helpers\Borradores($db);
        $borra->updPath($_POST["borrador_id"],$_POST["path_nuevo"]);

        $json["success"] = true;
        $json["status"] = 200;
        $json["mensaje"] = "Ruta de documento actualizada";
        echo json_encode($json);
    }catch (\Exception $e) {
        $json["success"] = false;
        $json["status"] = 400;
        $json["mensaje"] = $e->getMessage();
        echo json_encode($json);
    }
}

function consultar_tipos_anexos($get)
{
$json = array();
    try {
        require_once '../../config.php';
        $ruta_raiz = "../../";
        if (isset($db)) unset($db);
        include_once("$ruta_raiz/include/db/ConnectionHandler.php");
        $db = new ConnectionHandler("$ruta_raiz");
        $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
        $anexosDesdeBorra = new \App\Helpers\Borradores($db);
        $tiposAnexos = $anexosDesdeBorra->getAnexosTipos();
        $json["success"] = true;
        $json["status"] = 200;
        $json["anexos_tipos"] = $tiposAnexos;
        echo json_encode($json);
    }catch (\Exception $e) {
        $json["success"] = false;
        $json["status"] = 400;
        $json["mensaje"] = $e->getMessage();
        echo json_encode($json);
    }
}

function get_dropdown_users(){
    $json = array();
    try {

    require_once '../../config.php';
    $ruta_raiz = "../../";
    if (isset($db)) unset($db);
    include_once("$ruta_raiz/include/db/ConnectionHandler.php");
    $db = new ConnectionHandler("$ruta_raiz");
    $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);

    $borrador = new \App\Helpers\Borradores($db);
    $users = $borrador->getDropdownEnviarByBorr($_GET['dependencia'],$_GET['borrador']);
        $json["success"] = true;
        $json["status"] = 200;
        $json["users"] = $users;
        echo json_encode($json);
    }catch (\Exception $e) {
        $usersVacio = array();
        $json["success"] = false;
        $json["status"] = 400;
        $json["users"] = $usersVacio;
        $json["mensaje"] = $e->getMessage();
        echo json_encode($json);
    }
}



if (!isset($_SESSION["usuario_id"])) {

    $json = array(
        'error' => 'expiro'
    );
    echo json_encode($json);
    exit;
}


if (isset($_POST['function_call']) and $_POST['function_call'] == "updatecada60") {

    updatecada60($_POST);
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "aprobarborrador") {

    aprobarborrador($_POST);
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "borrarBorrador") {

    borrarBorrador($_POST);
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "devolverBorrador") {

    devolverBorrador($_POST);
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "revisarBorrador") {

    revisarBorrador($_POST);
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "guardarEnviar") {

    guardarEnviar($_POST);
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "buscarradicado") {

    buscarradicado($_POST);
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "guardarAsociado") {

    guardarAsociado($_POST);
}


if (isset($_POST['function_call']) and $_POST['function_call'] == "removeanexo") {

    removeanexo($_POST);
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "removeRadiAsoc") {

    removeRadiAsoc($_POST);
}


if (isset($_POST['function_call']) and $_POST['function_call'] == "solocomentarios") {

    solocomentarios($_POST);
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "radicar") {

    radicar($_POST);
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "addexpedienteborra") {

    addexpedienteborra();
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "addexpedienteborraEntrada") {

    addexpedienteborraEntrada();
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "excluirexpedienteborra") {

    excluirexpedienteborra();
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "cantidadBorradoresUsu") {

    cantidadBorradoresUsu($_POST);
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "getExpedBorrador") {

    getExpedBorrador();
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "incluircontipologia") {

    incluircontipologia();
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "incluircontipologiaEntrada") {

    incluircontipologiaEntrada();
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "estaincluidoexpediente") {

    estaincluidoexpediente();
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "busqAvaExpediente") {

    busqAvaExpediente();
}


if (isset($_POST['function_call']) and $_POST['function_call'] == "resultAvanzados") {

    resultAvanzados();
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "resultAvanzadosEntrada") {

    resultAvanzadosEntrada();
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "buscarExpedRadAso") {

    buscarExpedRadAso();
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "getAnexos") {

    getAnexos();
}


if (isset($_POST['function_call']) and $_POST['function_call'] == "guardarBorrador") {

    guardarBorrador();
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "guardarEditor") {

    guardarEditor();
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "buscarMisExped") {

    buscarMisExped();
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "buscarMisExpedEntrada") {

    buscarMisExpedEntrada();
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "estaincluidoexpedienteEntrada") {

    estaincluidoexpedienteEntrada();
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "actFirmantePrinci") {

    actFirmantePrinci();
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "updateTipoloTodosRad") {

    updateTipoloTodosRad();
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "traeExtension") {

    traeExtension();
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "seleccionarOrigen") {

    seleccionarOrigen($_POST);
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "getUsuaEstado") {

    getUsuaEstado($_POST);
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "modalConfig") {

    modalConfig($_POST);
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "noaprobarBorrador") {

    noaprobarBorrador($_POST);
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "guardarComentBorrador") {

    guardarComentBorrador($_POST);
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "actFirmantetipo_validador") {

    actFirmantetipo_validador($_POST);
}

if (isset($_POST['function_call']) and $_POST['function_call'] == "actualizar_path_versionamiento") {

    actualizar_path_versionamiento($_POST);
}

if (isset($_GET['function_call']) and $_GET['function_call'] == "consultar_tipos_anexos") {

    consultar_tipos_anexos($_GET);
}

if (isset($_GET['function_call']) and $_GET['function_call'] == "get_dropdown_users") {

    get_dropdown_users($_GET);
}


?>

