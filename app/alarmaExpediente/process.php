<?php

use App\Helpers\PDOconfig;
use Aura\SqlQuery\QueryFactory;
use Philo\Blade\Blade;

require '../../vendor/autoload.php';

try {
    if ($_POST['operation'] == "search-exp") {
        $included = filter_var($_POST['included'], FILTER_VALIDATE_BOOLEAN);
        $excluded = filter_var($_POST['excluded'], FILTER_VALIDATE_BOOLEAN);
        $response = getSearchResults($_POST['query'], $included, $excluded);
    }
} catch (Exception $e) {
    echo "Error: ".$e->getMessage();
    $response = "ERROR";
}
echo $response;
exit;

function getSearchResults($query, $included, $excluded)
{
    try{
        require_once '../../config.php';
        $views = $ABSOL_PATH . 'app/views';
        $cache = $ABSOL_PATH . 'app/cache';

        //Clearing the views cache
        \App\Helpers\MiscFunctions::clear_view_cache();

        $blade = new Blade($views, $cache);

        $pazysalvo_repo = new \App\Helpers\AlarmaExpediente();
        $data =  $pazysalvo_repo->getRecords($query, $included, $excluded);

        return $blade->view()->make(
            'alarmaExpediente.partials.search_exp_table',
            compact('data')
        )->render();
    } catch(\Exception $e) {
        return $e->getMessage();
    }
}
?>

