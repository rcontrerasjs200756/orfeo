<?php

use Philo\Blade\Blade;

require '../../vendor/autoload.php';
require_once '../../config.php';

$views = $ABSOL_PATH.'app/views';
$cache = $ABSOL_PATH.'app/cache';

//Clearing the views cache
\App\Helpers\MiscFunctions::clear_view_cache();

$blade = new Blade($views, $cache);
$records = new \App\Helpers\AlarmaExpediente();
$data = $records->getReportData();

echo $blade->view()->make(
    'alarmaExpediente.index',
    compact(
        'data',
        'include_path'
    )
)->render();

?>