<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <title>.::Sistema de Gestión Documental::.</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <META http-equiv="Pragma" content="no-cache">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="Preview page of Metronic Admin Theme #1 for blank page layout" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="{{$include_path}}global/plugins/font-awesome/css/fonts.googleapis.css" rel="stylesheet" type="text/css" />
    <link href="{{$include_path}}global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="{{$include_path}}global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />


    <link href="{{$include_path}}global/plugins/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="{{$include_path}}global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN THEME GLOBAL STYLES
    el siguiente archivo css, les quita la propiedad de borderradios a los input de form control, por eso lo comente.
    <link href="{{$include_path}}global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
-->
    <link href="{{$include_path}}global/css/components-rounded.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="{{$include_path}}global/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="{{$include_path}}layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
   <link href="{{$include_path}}layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />


    <link href="{{$include_path}}global/plugins/sweetalert2-9.10.2/package/dist/sweetalert2.min.css" rel="stylesheet" type="text/css" />
    <link href="{{$include_path}}global/css/chosen.min.css" rel="stylesheet" type="text/css" />
     <link href="{{$include_path}}global/css/select2.min.css" rel="stylesheet" />
    <link href="{{$include_path}}global/css/select2-bootstrap.min.css"  rel="stylesheet"/>
    <link href="{{$include_path}}global/css/editor.css"  rel="stylesheet"/>

    <!--  datatable -->
    <link href="{{$include_path}}global/css/datatable/datatables.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="{{$include_path}}global/css/datatable/datatables.bootstrap.css" rel="stylesheet" type="text/css" />

    <link href="{{$include_path}}global/plugins/chosen_v1.8.7/chosen.css" rel="stylesheet" type="text/css" />



    <link rel="stylesheet" href="{{$include_path}}global/plugins/blueimp/css/blueimp-gallery.min.css">


    <link href="{{$include_path}}global/plugins/jquery-upload/css/style.css"   type="text/css"    />
    <link href="{{$include_path}}global/plugins/jquery-upload/css/jquery.fileupload.css"   type="text/css"    />
    <link href="{{$include_path}}global/plugins/jquery-upload/css/jquery.fileupload-ui.css"   type="text/css"    />

    <link href="{{$include_path}}global/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css" />
    <link href="{{$include_path}}global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
    <!--  datatable -->



    <link href="{{$include_path}}pages/css/search.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{$include_path}}pages/css/invoice.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{$include_path}}global/plugins/typeahead/typeahead.css" rel="stylesheet" type="text/css"/>

    <link href="{{$include_path}}global/plugins/x-editable/bootstrap-editable.css"
          rel="stylesheet"/>
    <link href="{{$include_path}}global/plugins/typeahead/typeahead.js-bootstrap.css" rel="stylesheet" type="text/css"/>

    <link href="{{$include_path}}global/plugins/icheck/flat/_all.css" rel="stylesheet" type="text/css"/>


    <!-- END THEME LAYOUT STYLES -->
    <!-- BEGIN PAGE STYLES -->
    @yield('page-css')
    <!-- END PAGE STYLES -->
    <link rel="shortcut icon" href="favicon.ico" />
    <script type="text/javascript">
        var base_url = '<?php echo $_SESSION['base_url']; ?>';
        var anio_actual='<?= date('Y') ?>'
        var userLogguedId = '<?php echo $_SESSION["usuario_id"] ?>'
    </script>

    <script type="text/javascript">
        var WS_APPSERVICE_URL = '<?php echo $_SESSION['WS_APPSERVICE_URL']; ?>';
        var WS_APPSERVICE_TOKEN = '<?php echo $_SESSION['WS_APPSERVICE_TOKEN']; ?>';
        var conjugacionValidar = '<?php  echo $_SESSION['NOMBRE_BOTON_VALIDAR_CONJUGADO'] ?>';

        var doc_codusuario = '<?php echo $_SESSION["usuario_id"]?>';
        var usua_email = '<?php echo $_SESSION["usua_email"]?>';
        var codigo_aprobar_doc = '<?php echo $_SESSION["codigo_aprobar_doc"]?>'; //codigo que se tiene en session para el envio de email
        var expiracion_aprobar_doc = '<?php echo $_SESSION["expiracion_aprobar_doc"]?>'; //fecha de expiracion que se tiene en session para el envio de email
        var horaminuto_expira = '<?= $_SESSION["expiracion_aprobar_doc"] != '' ? date('H:m', strtotime($_SESSION["expiracion_aprobar_doc"])) : '' ?>'
        var solofecha_expira = '<?= $_SESSION["expiracion_aprobar_doc"] != '' ? date('d-m-Y', strtotime($_SESSION["expiracion_aprobar_doc"])) : '' ?>'
        var codigo_email_enviado = '<?= $_SESSION['codigo_email_enviado'] ?>';// indicador de si el codigo email fue enviado y almacenado en bd garantia de mostrar opciones de validacion
        var fechaahora = '<?= date('YmdHis') ?>';
        var METODO_VALIDACION = '<?php echo $_SESSION["METODO_VALIDACION"]?>';
        var NOMBRE_BOTON_VALIDAR = '<?= $_SESSION["NOMBRE_BOTON_VALIDAR"]?>';
        var radicados_sin_regenerar=null;
        var dependencia_user_log = '<?php echo $_SESSION['dependencia'] ?>';
        var usuaPermExpediente='<?= $_SESSION["usuaPermExpediente"]?>';
        
        var configuracionSession= '<?= json_encode($_SESSION["CONFIG"],JSON_FORCE_OBJECT) ?>';
    </script>

</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed page-sidebar-closed-hide-logo page-content-white">
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content" style="margin-left: 0 !important;">
                <!-- BEGIN PAGE HEADER-->
                <!-- BEGIN PAGE TITLE
                <h1 class="page-title">
                    @yield('page-title')
                </h1>
                -->
                <!-- END PAGE TITLE-->
                <!-- END PAGE HEADER-->
                @yield('content')
            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
<!--[if lt IE 9]>
<script src="{{$include_path}}global/plugins/respond.min.js"></script>
<script src="{{$include_path}}global/plugins/excanvas.min.js"></script>
<script src="{{$include_path}}global/plugins/ie8.fix.min.js"></script>
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="{{$include_path}}global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="{{$include_path}}global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="{{$include_path}}global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="{{$include_path}}global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="{{$include_path}}global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="{{$include_path}}global/plugins/bootstrap-switch/js/bootstrap-switch.js" type="text/javascript"></script>


<!-- END CORE PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="{{$include_path}}global/scripts/app.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->

        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="{{$include_path}}pages/scripts/components-bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->

<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="{{$include_path}}layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
<script src="{{$include_path}}layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
<script src="{{$include_path}}layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
<script src="{{$include_path}}layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
        <script src="{{$include_path}}global/plugins/sweetalert2-9.10.2/package/dist/sweetalert2.js" type="text/javascript"></script>
        <!--<script src="{{$include_path}}global/plugins/chosen.jquery.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>-->
        <script src="{{$include_path}}global/plugins/select2.full.min.js" type="text/javascript"></script>
        <script src="{{$include_path}}global/plugins/editor.js" type="text/javascript"></script>

        <script src="{{$include_path}}global/plugins/tinymce/tinymce.min.js" type="text/javascript"></script>

        <script src="{{$include_path}}global/plugins/html-docx.js" type="text/javascript"></script>
        <script src="{{$include_path}}global/plugins/file-saver/FileSaver.js" type="text/javascript"></script>
        <script src="{{$include_path}}global/plugins/bootstrap-table/bootstrap-table.js"  type="text/javascript"></script>
        <script src="{{$include_path}}pages/scripts/toastr.min.js"  type="text/javascript"></script>
        <script src="{{$include_path}}pages/scripts/ui-toastr.min.js"  type="text/javascript"></script>
        <script src="{{$include_path}}global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"  ></script>
        <script src="{{$include_path}}global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.es.min.js"  ></script>
        <script src="{{$include_path}}global/plugins/jquery.blockui.min.js"  ></script>
        <script src="{{$include_path}}global/plugins/chosen_v1.8.7/chosen.jquery.js" ></script>

        <script src="{{$include_path}}pages/scripts/bootstrap-maxlength.min.js" type="text/javascript"></script>
        <script src="{{$include_path}}pages/scripts/components-bootstrap-maxlength.js" type="text/javascript"></script>

        <script src="{{$include_path}}global/plugins/x-editable/bootstrap-editable.min.js"></script>
        <script src="{{$include_path}}global/plugins/typeahead/typeaheadjs.js"></script>

        <script src="{{$include_path}}global/plugins/typeahead/handlebars.min.js" type="text/javascript"></script>
        <script src="{{$include_path}}global/plugins/typeahead/typeahead.bundle.min.js" type="text/javascript"></script>



        <script src="{{$include_path}}apps/scripts/scriptconfiguraciones.js?v=<?= date('YmdHis') ?>"  ></script>

        <script src="{{$include_path}}apps/scripts/funciones_globales.js?v=<?= date('YmdHis') ?>"
                type="text/javascript"></script>


        <script src="{{$include_path}}apps/scripts/services/ConfiguracionesService.js?v=<?= date('Ymdis') ?>"
                type="text/javascript"></script>

        <script src="{{$include_path}}apps/scripts/services/ValidacionService.js?v=<?= date('Ymdis') ?>"
                type="text/javascript"></script>

        <script src="{{$include_path}}apps/scripts/services/RadicadoService.js?v=<?= date('Ymdis') ?>"
                type="text/javascript"></script>

        <script src="{{$include_path}}apps/scripts/services/OrdenesDePagoService.js?v=<?= date('Ymdis') ?>"
                type="text/javascript"></script>

        <script src="{{$include_path}}apps/scripts/services/AnexosService.js?v=<?= date('Ymdis') ?>"
                type="text/javascript"></script>

        <script src="{{$include_path}}apps/scripts/services/NotificacionesService.js?v=<?= date('Ymdis') ?>"
                type="text/javascript"></script>
                
        <script src="{{$include_path}}apps/scripts/services/ExpedienteService.js?v=<?= date('Ymdis') ?>"
                type="text/javascript"></script>  

        <script src="{{$include_path}}apps/scripts/services/SeguridadService.js?v=<?= date('Ymdis') ?>"
                type="text/javascript"></script> 
        <!-- Controllers -->


        <script src="{{$include_path}}apps/scripts/controllers/Configuraciones.js?v=<?= date('Ymdis') ?>"
                type="text/javascript"></script>

        <script src="{{$include_path}}apps/scripts/controllers/OrdenesDePago.js?v=<?= date('Ymdis') ?>"
                type="text/javascript"></script>

        <script src="{{$include_path}}apps/scripts/fun_validar_rad.js?v=<?= date('YmdHis') ?>"
                type="text/javascript"></script>

        <script src="{{$include_path}}apps/scripts/controllers/Validacion.js?v=<?= date('Ymdis') ?>"
                type="text/javascript"></script>

        <script src="{{$include_path}}apps/scripts/nuevoExpediente.js?v=<?= date('Ymdis') ?>"
                type="text/javascript"></script>


        <script>
            $(document).ready(function () {

                console.log('session vacia');
                console.log(userLogguedId);
                
                armarConfigsHasValuesMayus(JSON.parse(configuracionSession));

                /**
                 * busco las configuraciones se comento ya que consumia muchos recursos
                 
                ConfiguracionesService.getAllConfiguraciones().success(function (response) {
                    if (response.error == undefined) {
                        configsHasValue= response.objects;

                        
                        armarConfigsHasValuesMinuscula(configsHasValue); 
                        armarConfigsHasValuesMayus(configuracionSession);
                         

                        console.log(configsHasValue['TAB_OP_VISIBLE']);
                    } else {
                        Swal.fire("Error", response.message, "error");
                        continuar = false;
                        return false;
                    }
                }).error(function (error) {

                    Swal.fire("Error", error.message, "error");
                    return false;
                });
               */

                //console.log(configsHasValue);

                AnexosService.getAllAnexExtAccept().success(function (response) {
                    if (response.error == undefined) {

                        anexosTipos = Array.from(response.objects,function(x){ return x.anex_tipo_ext});
                    } else {
                        Swal.fire("Error", response.message, "error");
                        continuar = false;
                        return false;
                    }
                }).error(function (error) {

                    Swal.fire("Error", error.message, "error");
                    return false;
                });

            })
        </script>



<!-- END THEME LAYOUT SCRIPTS -->
<!-- BEGIN PAGE SCRIPTS -->
@yield('page-js')
<!-- END PAGE SCRIPTS -->
@yield('page-modal')
        <div id="modal_show_configs" class="modal fade" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header bg-blue-chambray bg-font-yellow text-center" id="header_vista_previa">

                        <button type="button" title="Cerrar" aria-hidden="true" class="btn btn-lg " data-dismiss="modal"
                                style="float: right;background-color: #2C3E50!important; border-radius:20px !important"
                        >
                            <i class="fa fa-remove"></i></button>
                        <h4 class="modal-title"><strong>Configuraciones</strong></h4>
                    </div>
                    <div class="modal-body" id="modal_body_show_configs">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-lg blue"
                                style="border-radius:20px !important" onclick="guardarConfigs()">
                            Guardar
                        </button>
                        <button type="button" class="btn btn-lg" data-dismiss="modal"
                                style="border-radius:20px !important">
                            Cerrar
                        </button>
                    </div>
                </div>
            </div>
        </div>
</body>


</html>