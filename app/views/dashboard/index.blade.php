@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
    <button  title="Configuraciones"
             style="text-align:right;float:right !important;border-radius:20px !important; background-color:transparent"
       onclick="modalConfig()">
        <i class="fa fa-wrench" aria-hidden="true"></i>&nbsp</button>
        </div>
    </div>
    <br>
    <div class="row">
        <?php
        if($_SESSION["FUNCTION_BORRADORES"]=="SI"){
        ?>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12"  title="Borradores a cargo, pendientes por gestionar.">
                <a class="dashboard-stat dashboard-stat-v2 green rounded-4"
                   href="<?=$_SESSION['base_url'] ?>app/borradores/index.php">
                    <div class="visual">
                        <i class="fa fa-pencil-square-o"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="{{$_SESSION["borradores"]}}">0</span>
                        </div>
                        <div class="desc"> Borradores </div>
                    </div>
                </a>
            </div>
        <?php }else{ ?>


        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 green rounded-4" href="../../cuerpoTablero.php?{{session_name()."=".session_id()}}&radicaded={{base64_encode(serialize($unclasified))}}">
                <div class="visual">
                    <i class="fa fa-clone"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{(isset($unclasified))? count($unclasified) : '0'}}">0</span>
                    </div>
                    <div class="desc"> Sin Clasificar </div>
                </div>
            </a>
        </div>

            <?php } ?>

        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12"  title="Radicados sin archivar en su respectivo expediente.">
            <a class="dashboard-stat dashboard-stat-v2 purple rounded-4" href="../../cuerpoTablero.php?{{session_name()."=".session_id()}}&radicaded={{base64_encode(serialize($without_file))}}">
                <div class="visual">
                    <i class="fa fa-folder-open"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{(isset($without_file))? count($without_file) : '0'}}">0</span>
                    </div>
                    <div class="desc"> Sin Expediente </div>
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" title="Total de radicados en sus bandejas, pendientes por finalizar.">
            <a class="dashboard-stat dashboard-stat-v2 blue rounded-4" href="../../cuerpoTablero.php?{{session_name()."=".session_id()}}&radicaded={{base64_encode(serialize($unfinished))}}">
                <div class="visual">
                    <i class="fa fa-archive"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{(isset($unfinished))? count($unfinished) : '0'}}">0</span>
                    </div>
                    <div class="desc"> Sin Finalizar / Total </div>
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" title="Total de radicados en su bandeja, relacionados con PQRS.">
            <a class="dashboard-stat dashboard-stat-v2 red rounded-4" href="../../cuerpoTablero.php?{{session_name()."=".session_id()}}&radicaded={{base64_encode(serialize($pqr))}}">
                <div class="visual">
                    <i class="fa fa-exclamation-circle"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{(isset($pqr))? count($pqr) : '0'}}"></span>
                    </div>
                    <div class="desc"> PQR </div>
                </div>
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 margin-bottom-10">
            <a id="consolidated_show" class="pull-right">Consolidados</a>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="portlet light bordered" style="min-height: 436px; max-height: 436px">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-share font-red-sunglo hide"></i>
                        <span class="caption-subject font-dark bold uppercase">Radicados por días</span>
                        <span class="caption-helper">desde su radicación</span>
                    </div>
                    <div class="actions">
                        <!--
                        <div class="btn-group">
                            <a href="" class="btn dark btn-outline btn-circle btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> Filter Range
                                <span class="fa fa-angle-down"> </span>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li>
                                    <a href="javascript:;"> Q1 2014
                                        <span class="label label-sm label-default"> past </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:;"> Q2 2014
                                        <span class="label label-sm label-default"> past </span>
                                    </a>
                                </li>
                                <li class="active">
                                    <a href="javascript:;"> Q3 2014
                                        <span class="label label-sm label-success"> current </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:;"> Q4 2014
                                        <span class="label label-sm label-warning"> upcoming </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        -->
                    </div>
                </div>
                <div class="portlet-body">
                    <div id="site_activities_loading">
                        <img src="{{$include_path}}global/img/loading.gif" alt="loading" /> </div>
                    <div id="site_activities_content" class="display-none">
                        <div id="site_activities" style="height: 320px;"> </div>
                    </div>
                    <!--
                    <div style="margin: 20px 0 10px 30px">
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-6 text-stat">
                                <span class="label label-sm label-success"> Revenue: </span>
                                <h3>$13,234</h3>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-6 text-stat">
                                <span class="label label-sm label-info"> Tax: </span>
                                <h3>$134,900</h3>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-6 text-stat">
                                <span class="label label-sm label-danger"> Shipment: </span>
                                <h3>$1,134</h3>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-6 text-stat">
                                <span class="label label-sm label-warning"> Orders: </span>
                                <h3>235090</h3>
                            </div>
                        </div>
                    </div>
                    -->
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="portlet light bordered" style="min-height: 436px; max-height: 436px">
                <div class="portlet-title tabbable-line">
                    <div class="caption">
                        <i class="icon-globe font-dark hide"></i>
                        <span class="caption-subject font-dark bold uppercase">NOTIFICACIONES RECIENTES</span>
                    </div>
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#tab_1_1" class="active" data-toggle="tab"> Radicados </a>
                        </li>
                        <!--
                        <li>
                            <a href="#tab_1_2" data-toggle="tab"> Historial </a>
                        </li>
                        -->
                    </ul>
                </div>
                <div class="portlet-body">
                    <!--BEGIN TABS-->
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1_1">
                            <div class="scroller" style="height: 339px;" data-always-visible="1" data-rail-visible="0">
                                <ul class="feeds">
                                    @foreach ($recent_radicated as $row)
                                        <li>
                                            <div class="col1">
                                                <div class="cont" data-toggle="tooltip" data-title="{{\App\Helpers\MiscFunctions::get_radicated_status($row['TRANSACCION_CODIGO'])}}" data-placement="bottom">
                                                    <div class="cont-col1">
                                                        <div class="label label-sm label-success bg-{{\App\Helpers\MiscFunctions::get_radicated_notif_color($row['TRANSACCION_CODIGO'])}} rounded-4">
                                                            <i class="fa fa-{{\App\Helpers\MiscFunctions::get_radicated_notif_icon($row['TRANSACCION_CODIGO'])}}"></i>
                                                        </div>
                                                    </div>
                                                    <div class="cont-col2">
                                                        <div class="desc font-sm font-dark">
                                                            <a href="../../verradicado.php?{{session_name()."=".session_id()}}&verrad={{$row["RADICADO"]}}&menu_ver_tmp=1"
                                                              >{{$row['ASUNTO']}}</a>
                                                            <span class="label label-sm label-default">
                                                                @if(strlen($row["PATH"]) > 0 && file_exists($_SESSION['base_url'].'bodega'.$row["PATH"]))
                                                                <a onclick="funlinkArchivo('{{$row['RADICADO']}}}','.');" style="font-size: 12px; color:#000000; font-weight: bold; text-decoration: none"> {{$row['RADICADO']}} </a>
                                                                @else
                                                                    <span style="font-size: 12px; color:#000000; font-weight: bold; text-decoration: none">{{$row['RADICADO']}}</span>
                                                                @endif
                                                            </span> {{$row['COMENTARIO']}}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col2">
                                                <div class="date"> {{\App\Helpers\MiscFunctions::get_radicated_notif_date($row['FECHA'])}}</div>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_1_2">
                            <div class="scroller" style="height: 290px;" data-always-visible="1" data-rail-visible1="1">
                                <ul class="feeds">
                                    <li>
                                        <a href="javascript:;">
                                            <div class="col1">
                                                <div class="cont">
                                                    <div class="cont-col1">
                                                        <div class="label label-sm label-success">
                                                            <i class="fa fa-bell-o"></i>
                                                        </div>
                                                    </div>
                                                    <div class="cont-col2">
                                                        <div class="desc"> New user registered </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col2">
                                                <div class="date"> Just now </div>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--END TABS-->
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-modal')
    <div id="consolidated_modal" class="modal fade" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-blue bg-font-yellow text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><strong>Indicadores Consolidados</strong></h4>
                </div>
                <div class="modal-body">
                    <div class="row" id="date_selection">
                        <div class="col-md-8 col-md-offset-3 col-sm-8 col-sm-offset-3 text-center">
                            <div class="input-group input-large" style="padding-left: 13%;">
                                <input type="text" class="form-control date-picker" name="from" id="from" readonly data-date-format="dd-mm-yyyy">
                                <span class="input-group-addon"> a </span>
                                <input type="text" class="form-control date-picker" name="to" id="to" readonly data-date-format="dd-mm-yyyy">
                            </div>
                            <span class="help-block font-red hide" id="date-error">Introduzca un rango válido</span>
                            <button class="btn btn-sm bg-blue-sharp font-white margin-top-10" id="search-consolidated">Ir</button>
                        </div>
                    </div>
                    <div class="row" id="consolidated_indicators"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="rad_img_modal" class="modal fade" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                </div>
                <div class="modal-body">
                    <img src="" class="img-responsive" width="100%">
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-css')
    <style>
        .date-picker
        {
            z-index:1151 !important;
        }
        .consolidated-chart canvas
        {
            width: 100% !important;
        }
        #consolidated_modal .modal-dialog
        {
            width: 80%;
        }
        #search-consolidated
        {
            margin-right: 200px;
        }
    </style>
    <link href="{{$include_path}}global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
@endsection

@section('page-js')
    <script src="{{$include_path}}global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.es.min.js" type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/moment.min.js" type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
    <script>
        $(document).ready(function(){
            $('#consolidated_modal').on('hidden.bs.modal', function (e) {
                $('.date-picker').datepicker('destroy');
                var date_from= moment().subtract(1, 'months').startOf('month').format('DD-M-YYYY');
                var date_to= moment().subtract(1, 'months').endOf('month').format('DD-M-YYYY');
                $('#date_selection').removeClass('hide');
                $('#from').val(date_from);
                $('#to').val(date_to);
                $("#consolidated_indicators").html('');
                $("#consolidated_indicators").addClass('hide');
            });
        });

        $("[data-toggle='tooltip']").tooltip();
        function showChartTooltip(x, y, xValue, yValue) {
            $('<div id="tooltip" class="chart-tooltip">' + yValue + '<\/div>').css({
                position: 'absolute',
                display: 'none',
                top: y - 40,
                left: x - 40,
                border: '0px solid #ccc',
                padding: '2px 6px',
                'background-color': '#fff'
            }).appendTo("body").fadeIn(200);
        }
        if ($('#site_activities').size() != 0) {
            //site activities
            var previousPoint2 = null;
            $('#site_activities_loading').hide();
            $('#site_activities_content').show();

            var data1 = {{json_encode($graph_data)}};

            var plot_statistics = $.plot($("#site_activities"),

                    [{
                        data: data1,
                        lines: {
                            fill: 0.2,
                            lineWidth: 0,
                        },
                        color: ['#BAD9F5']
                    }, {
                        data: data1,
                        points: {
                            show: true,
                            fill: true,
                            radius: 4,
                            fillColor: "#9ACAE6",
                            lineWidth: 2
                        },
                        color: '#9ACAE6',
                        shadowSize: 1
                    }, {
                        data: data1,
                        lines: {
                            show: true,
                            fill: false,
                            lineWidth: 3
                        },
                        color: '#9ACAE6',
                        shadowSize: 0
                    }],

                    {

                        xaxis: {
                            tickLength: 0,
                            tickDecimals: 0,
                            mode: "categories",
                            min: 0,
                            font: {
                                lineHeight: 18,
                                style: "normal",
                                variant: "small-caps",
                                color: "#6F7B8A"
                            }
                        },
                        yaxis: {
                            ticks: 5,
                            tickDecimals: 0,
                            tickColor: "#eee",
                            font: {
                                lineHeight: 14,
                                style: "normal",
                                variant: "small-caps",
                                color: "#6F7B8A"
                            }
                        },
                        grid: {
                            hoverable: true,
                            clickable: true,
                            tickColor: "#eee",
                            borderColor: "#eee",
                            borderWidth: 1
                        }
                    });

            $("#site_activities").bind("plothover", function(event, pos, item) {
                $("#x").text(pos.x.toFixed(2));
                $("#y").text(pos.y.toFixed(2));
                if (item) {
                    if (previousPoint2 != item.dataIndex) {
                        previousPoint2 = item.dataIndex;
                        $("#tooltip").remove();
                        var x = item.datapoint[0].toFixed(2),
                                y = item.datapoint[1].toFixed(2);
                        showChartTooltip(item.pageX, item.pageY, item.datapoint[0], item.datapoint[1] + ' radicados');
                    }
                }
            });

            $('#site_activities').bind("mouseleave", function() {
                $("#tooltip").remove();
            });
        }
        $(document).on('click', '#consolidated_show', function(e){
            e.preventDefault();
            var date_from= moment().subtract(1, 'months').startOf('month').format('DD-M-YYYY');
            var date_to= moment().subtract(1, 'months').endOf('month').format('DD-M-YYYY');

            $('.date-picker').datepicker({
                rtl: App.isRTL(),
                orientation: "bottom left",
                autoclose: true,
                startView: 'year',
                language:"es"
            });
            $('#from').val(date_from);
            $('#to').val(date_to);
            $('#consolidated_modal').modal('show');
        });

        $(document).on('click', '#search-consolidated', function(e){
            e.preventDefault();
            var from= $('#from').val();
            var to= $('#to').val();
            $.ajax({
                url: "process.php",
                method: "POST",
                data: {from:from, to:to, operation:"search_consolidated"},
                dataType: "html",
                success: function(response) {
                    if(response != "ERROR") {
                        $('#consolidated_indicators').html('');
                        $('#consolidated_indicators').html(response);
                        $('#date_selection').addClass('hide');
                        $('#consolidated_indicators').removeClass('hide');
                    }
                }
            }).done(function(){
                $('.consolidated-chart').easyPieChart();
            }).fail(function(error){
                console.log(error);
            });

        });
        $(document).on('click', '.open-img', function(e){
            e.preventDefault();
            console.log($(this).attr('href'));
            var img_url = $(this).attr('href');
            $('#rad_img_modal .modal-body img').attr('src', img_url);
            $('#rad_img_modal').modal('show');
        });
    </script>
@endsection