<div class="col-md-2 col-md-offset-1 text-center margin-top-40">
    <div class="easy-pie-chart">
        <span class="bold font-lg font-blue">{{$radicated['TOTAL']}}</span>
        <a class="font-sm font-dark margin-top-15" href="javascript:;">Radicados</a>
    </div>
</div>
<div class="margin-bottom-10 visible-sm"> </div>
<div class="col-md-2 text-center">
    <div class="easy-pie-chart">
        <div class="consolidated-chart number visits bold" data-bar-color="blue" data-percent="{{number_format($finished_perc,1)}}">
            <div style="height: 14px">
                {{number_format($finished_perc,1)}} %
            </div>
            <div class="font-xs" style="height: 10px">
                ({{($finished['TOTAL'] > 0)?$finished['TOTAL']:0}})
            </div>
        </div>
        <br>
        <a class="font-sm font-dark margin-top-10" href="javascript:;"> Finalizados</a>
    </div>
</div>
<div class="margin-bottom-10 visible-sm"></div>
<div class="col-md-2 text-center">
    <div class="easy-pie-chart">
        <div class="consolidated-chart number visits bold" data-bar-color="yellow" data-percent="{{number_format($without_file_perc,1)}}">
            <div style="height: 14px">
                {{number_format($without_file_perc,1)}} %
            </div>
            <div class="font-xs" style="height: 10px">
                ({{($without_file['TOTAL'] > 0)?$without_file['TOTAL']:0}})
            </div>
        </div>
        <br>
        <a class="font-sm font-dark margin-top-10" href="javascript:;"> Sin Expediente</a>
    </div>
</div>
<div class="margin-bottom-10 visible-sm"> </div>
<div class="col-md-2 text-center">
    <div class="easy-pie-chart">
        <div class="consolidated-chart number visits bold" data-bar-color="green" data-percent="{{number_format($trd_perc,1)}}">
            <div style="height: 14px">
                {{number_format($trd_perc,1)}} %
            </div>
            <div class="font-xs" style="height: 10px">
                ({{($trd['TOTAL'] > 0)?$trd['TOTAL']:0}})
            </div>
        </div>
        <br>
        <a class="font-sm font-dark margin-top-10" href="javascript:;"> Sin Clasificar TRD</a>
    </div>
</div>
<div class="margin-bottom-10 visible-sm"> </div>
<div class="col-md-2 text-center">
    <div class="easy-pie-chart">
        <div class="consolidated-chart number visits bold" data-bar-color="red" data-percent="{{number_format($canceled_perc,1)}}">
            <div style="height: 14px">
                {{number_format($canceled_perc,1)}} %
            </div>
            <div class="font-xs" style="height: 10px">
                ({{($canceled['TOTAL'] > 0)?$canceled['TOTAL']:0}})
            </div>
        </div>
        <br>
        <a class="font-sm font-dark margin-top-10" href="javascript:;"> Anulados</a>
    </div>
</div>