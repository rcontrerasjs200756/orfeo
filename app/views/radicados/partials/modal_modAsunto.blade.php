<div id="modal_modAsunto" class="modal fade" tabindex="-1" aria-hidden="true" style="display: none;">
    <div class="modal-dialog " style="width:65%;">
        <div class="modal-content">
            <div class="modal-header bg-blue-chambray bg-font-yellow text-center">
                <button type="button" title="Cerrar"
                        class="btn btn-lg " onclick="cerrarModalmodAsunto()"
                        style="float: right;background-color: #2C3E50!important; border-radius:20px !important">
                    <i class="fa fa-remove"></i></button>
                <h4 class="modal-title"><strong id="titulomodalnuevoexped">Modificar Asunto de Radicado</strong></h4>

            </div>
            <div class="modal-body" id="radicated_info">

                <div id="btnAdvWar" style="display:none;" class="alert alert-warning alert-dismissable">

                    <strong>Advertencia!</strong>
                    <br> El asunto solo se debería modificar cuando el documento se encuentra en BORRADOR.
                    <br> Los cambios se guardarán en el historial del radicado. ¿Está seguro?
                    <div class="row">
                        <div class="col-md-12 ">
                            <span class="col-md-3"><button type="button"  class="btn btn-danger">Si, Continuar</button></span><span class="col-md-3"><button onclick="cerrarModalmodAsunto()" type="button" class="btn btn-info">Cancelar</button></span><span class="col-md-6"></span>
                        </div>
                    </div>
                </div>

                <div id="btnAdvDanger" class="alert alert-danger" style="display: none;">

                </div>

                <form action="" method="POST" class="form-horizonal" id="">
                    <div class="form-body modal-body todo-task-modal-body">

                        <div class="row" style="margin-top:40px">
                            <div class="col-md-12 ">
                                <div class="form-group">



                                    <label class=" col-md-12" style="display: none;" id="AsuntoActualLab">AsuntoActual :</label>
                                    <div class="col-md-12">
                                        <div class="input-icon right" style="position: relative;">
                                            <textarea  required="required" name="asuntoActual" id="AsuntoActual" class="form-control" maxlength="999" style="display: none;border: solid grey 1px; height:90px" placeholder=""></textarea>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div><br>

                                    <label style="display: none;" class=" col-md-12" id="addAsunto">Agregar al asunto:</label>
                                    <div class="col-md-12">
                                        <div class="input-icon right" style="position: relative;">
                                            <textarea required="required" name="asuntoAdd" id="asuntoAdd" class="form-control" maxlength="999" style="display: none;border: solid grey 1px; height:90px" placeholder=""></textarea>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div><br>

                                    <label style="display: none;" class=" col-md-12" id="newAsunto">Nuevo Asunto:</label>
                                    <div class="col-md-12">
                                        <div id="nuevoAs" style="display: none;" class="alert alert-info">

                                        </div>
                                    </div> <br>


                                </div>
                            </div>

                        </div>
                    </div>


                </form>
            </div>
            <div class="modal-footer">
                <div class="form-actions">
                    <div class="row">

                        <div class="col-md-6">

                        </div>
                        <div class="col-md-4" id="divBtnAccionCrearExped">
                            <button  type="button" class="btn btn-lg blue" id="btnModAsunto"
                                     style="display:none;border-radius:20px !important" onclick="modAsunto(event)">
                                Guardar Cambios
                            </button>
                        </div>
                        <div class="col-md-2">
                            <button type="button" class="btn btn-lg" onclick="cerrarModalmodAsunto()"
                                    style="border-radius:20px !important">
                                Cerrar
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>