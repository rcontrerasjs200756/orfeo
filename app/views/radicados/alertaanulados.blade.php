@extends('layouts.app')

@section('content')

    <link href="{{$include_path}}pages/css/search.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{$include_path}}pages/css/invoice.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{$include_path}}global/plugins/typeahead/typeahead.css" rel="stylesheet" type="text/css"/>

    <style>
        table.dataTable thead tr th {
            word-wrap: break-word;
            word-break: break-all;
        }

        .botonestabla {
            border-radius: 5px 5px 5px 5px !important;
        }


        .page-content-wrapper .page-content {
            padding: 1px 0px 0px !important;
        }


    </style>


    <div class="row" style="border-top: 0px !important">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet  bordered">


                <div class="portlet-body">

                    <div class="row">
                        <div class="col-md-12">
                            <button  title="Configuraciones"
                                     style="text-align:right;float:right !important;border-radius:20px !important; background-color:transparent"
                                     onclick="modalConfig()">
                                <i class="fa fa-wrench" aria-hidden="true"></i>&nbsp</button>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-6" id="divproximosanulados">
                            <?php
                            if(count($configs_anular)>0) {
                                foreach($configs_anular as $row) {
                                    ${$row['NOMBRE_CONSTANTE']}=$row['VALOR'];
                                }
                            }

                            if(count($poranular) > 0) { ?>

                            <div class="panel panel-warning">
                                <!-- Default panel contents -->
                                <div class="panel-heading">
                                    <h3 class="panel-title">Advertencia!</h3>
                                </div>
                                <div class="panel-body">
                                    <p style="color: red">Los siguientes radicados ser&aacute;n anulados. </p>
                                    <span style="float: right; color:gray"></span>
                                </div>

                            <?php

                           //con esta si aparece a los 3 dias
                            //$fechahoy=date('Y-m-d H:i:s');
                            $contPorAnular = 0;

                            foreach($poranular as $row){

                            $fecharadmas30 = date("Y-m-d H:i", strtotime($row['FECHA_RADICADO'] . ' +'.$ANULA_DIAS_TRAMITE.'day'));
                            //si la fecha de hoy es mayor que la fecha en la que fue el radicado +30 dias, debe anularse

                            $fecha1 = new DateTime($fecharadmas30);
                            $fecha2 = new DateTime($fechahoy);
                            $diferencia = $fecha1->diff($fecha2);

                            if(($diferencia->days > 0 ) || ($diferencia->days == 0 && $diferencia->h > 0)){

                            $contPorAnular++;
                            $color = "badge-danger";
                            $numero = 1;
                            if ($diferencia->days == 3) {
                                $numero = 3;
                                $color = "badge-info";
                            }
                            if ($diferencia->days == 2) {
                                $color = "badge-warning";
                                $numero = 2;
                            }
                            if ($diferencia->days == 1) {
                                $numero = 1;
                                $color = "badge-danger";
                            }  ?>
                            <!-- List group -->
                                <ul class="list-group">
                                    <li class="list-group-item"> <?= $row['RADICADO']; ?> <?= $row['ASUNTO']; ?>
                                        <span style="font-size: 09pt !important" class="badge <?= $color ?>">en <?= $diferencia->days; ?> d&iacute;as</span>
                                    </li>
                                </ul>
                                <?php
                                }

                                }
                            if($ANULA_ACTO_ADMTVO=="SI"){
                            ?>
                                <ul class="list-group">
                                    <li class="list-group-item"><a
                                                href="../../../bodega/plantillas/acto_administrativo_anulados.pdf">Acto
                                            Administrativo</a>

                                    </li>
                                </ul>
                            </div>
                            <?php }
                            } ?>
                        </div>

                        <div class="col-md-6">

                            <?php

                            if(count($notificHoy) > 0) { ?>

                            <div class="panel panel-warning">
                                <!-- Default panel contents -->
                                <div class="panel-heading">
                                    <h3 class="panel-title">Advertencia!</h3>
                                </div>
                                <div class="panel-body">
                                    <p>Los siguientes radicados fueron anulados. </p>
                                </div>
                            <?php

                            foreach($notificHoy as $row){


                            ?>
                            <!-- List group -->
                                <ul class="list-group">
                                    <li class="list-group-item"> <?= $row['RADICADO']; ?> <?= $row['ASUNTO']; ?>
                                        <span class="badge badge-info">
                                            <?= date("d-m-Y H:i:s", strtotime($row['FECHA_ANULADO'])) ?> </span>
                                    </li>
                                </ul>


                            <?php  }
                            }
                            ?>

                        </div>
                    </div>


                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
@endsection

@section('page-js')

    <script src="{{$include_path}}global/plugins/bootstrap-table/bootstrap-table.min.js"
            type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/bootstrap-table/locale/bootstrap-table-es-SP.js"
            type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"
            type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.es.js"
            type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/moment.min.js" type="text/javascript"></script>

    <!--  datatable -->
    <script src="{{$include_path}}global/plugins/datatable/datatable.js" type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/datatable/datatables.bootstrap.js" type="text/javascript"></script>
    <!--  datatable -->
    <script src="{{$include_path}}global/plugins/typeahead/handlebars.min.js" type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/typeahead/typeahead.bundle.min.js" type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js"
            type="text/javascript"></script>

    <script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.ui.widget.js"></script>
    <!-- The Templates plugin is included to render the upload/download listings -->
    <script src="{{$include_path}}global/plugins/blueimp/js/tmpl.min.js"></script>
    <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
    <script src="{{$include_path}}global/plugins/blueimp/js/load-image.all.min.js"></script>
    <!-- The Canvas to Blob plugin is included for image resizing functionality -->
    <script src="{{$include_path}}global/plugins/blueimp/js/canvas-to-blob.min.js"></script>
    <!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
    <script src="{{$include_path}}global/plugins/bootstrap/js/bootstrap.js"></script>
    <!-- blueimp Gallery script -->
    <script src="{{$include_path}}global/plugins/blueimp/js/jquery.blueimp-gallery.min.js"></script>

    <script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.iframe-transport.js"
            type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.fileupload.js" type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.fileupload-process.js"
            type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.fileupload-image.js"
            type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.fileupload-audio.js"
            type="text/javascript"></script>

    <script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.fileupload-video.js"
            type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.fileupload-validate.js"
            type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.fileupload-ui.js"
            type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/jquery-upload/js/form-fileupload.min.js"
            type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/froala/js/froala_editor.min.js" type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/froala/js/froala_editor.pkgd.js" type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/froala/js/languages/es.js" type="text/javascript"></script>




    <!--  <link rel="stylesheet" type="text/css" href="https://editor-latest.s3.amazonaws.com/css/froala_editor.pkgd.min.css">



      <script type="text/javascript" src="https://editor-latest.s3.amazonaws.com/js/froala_editor.pkgd.min.js"></script>



      <link rel="stylesheet" type="text/css" href="https://editor-latest.s3.amazonaws.com/css/froala_style.min.css">-->

    <script type="text/javascript">
        var poranular = "<?= $contPorAnular ?>";
        //viene de app.blade
         modulosMostrar="ANULACION";
        $(document).ready(function () {

            if (poranular < 1) {
               // $("#divproximosanulados").remove();
            }

        })

    </script>
@endsection