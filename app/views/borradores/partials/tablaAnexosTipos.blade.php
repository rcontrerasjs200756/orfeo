<div id="modal_ejemplo_anexos_tipos" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header bg-blue-chambray bg-font-yellow text-center">
                <button type="button" class="btn btn-lg" style="float: right;background-color: #2C3E50!important; border-radius:20px !important"  data-dismiss="modal" aria-hidden="true"><i class="fa fa-remove"></i></button></button>
                <h4 class="modal-title"><strong>Modal Ejemplo Anexos Tipos</strong></h4>
            </div>
            <div class="modal-body" id="ejemplo_info">
                        <div class="col-md-12">
                            <!-- BEGIN SAMPLE TABLE PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-social-dribbble font-green"></i>
                                        <span class="caption-subject font-green bold uppercase">Ejemplo tipos de anexos</span>
                                    </div>

                                </div>
                                <div class="portlet-body">
                                    <div class="table-scrollable" id="open_table_tipos_anexos"
                                         style="position: relative;">
                                        <table class="table table-striped table-bordered table-hover
           table-checkable order-column dataTable no-footer" id="tablaejemplotipoanexos" >
                                            <thead>
                                            <tr role="row">
                                                <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1"
                                                    colspan="1"
                                                    aria-label=" Asunto : activate to sort column ascending"
                                                    style="width: 50%;text-align:center"> Extension </th>
                                                <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1"
                                                    colspan="1"
                                                    aria-label=" Asunto : activate to sort column ascending"
                                                    style="width: 50%;text-align:center"> Descripcion </th>
                                            </tr>
                                            </thead>
                                            <tbody id="tbodyanexostipos"></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- END SAMPLE TABLE PORTLET-->
                        </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-lg" data-dismiss="modal"
                        style="border-radius:20px !important">
                    Cancelar
                </button>
            </div>
        </div>
    </div>
</div>