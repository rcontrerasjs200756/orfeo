<div id="modal_versionamiento_exe" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header bg-blue-chambray bg-font-yellow text-center">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><strong>Modal Versionamiento</strong></h4>
            </div>
            <div class="modal-body" id="path_version_info">
                <form action="" method="POST" class="form-horizonal" id="">
                    <div class="form-body modal-body todo-task-modal-body">
                        <div class="form-actions">
                            <div class="row"></div>
                        </div>
                        <div class="row" style="margin-top:40px">

                            <div class="col-md-12 " style="margin-top:10px">
                                <div class="form-group has-error">
                                    <label class=" col-md-3">Path de nueva version:</label>
                                    <div class="col-md-9">
                                            <input type="text" name="ruta" id="ruta_version" class="form-control">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-lg green"
                        id="" onclick="guardarVersion()"
                        style="border-radius:20px !important">
                    <i class="fa fa-check-square-o"></i>
                    Guardar
                </button>
                <button type="button" class="btn btn-lg" data-dismiss="modal"
                        style="border-radius:20px !important">
                    Cancelar
                </button>
            </div>
        </div>
    </div>
</div>