

<table id="all_table"
       data-toggle="table"
       data-striped="true"
       data-pagination="true"
       data-page-size="10"
       data-search="true"
       data-locale="es-SP" >
    <thead>
    <tr>
        <th data-width="15%" width="15%" data-align="center">Id</th>
        <th data-width="40%"  width="40%" data-align="center">Asunto</th>
        <th data-width="15%"   width="15%" data-align="center">Estado</th>
        <th data-width="15%"  width="15%" data-align="center">Destinatario</th>
        <th data-width="15%"  width="15%" data-align="center">Usuario Anterior</th>
    </tr>
    </thead>
    <tbody>
    @foreach($borradores as $key => $row)

        <tr>
            <td>
                {{$row["ID_BORRADOR"]}}
            </td>
            <td>
                <a href="#" onclick="getBorrador({{$row["ID_BORRADOR"]}})" >
                    {{substr($row["ASUNTO"],0,100)}}...</a>
            </td>

            <?php

            $clase="yellow";
            if($row["NOMBRE_ESTADO"]=="Aprobado"){
                $clase="blue";
            }

            if($row["NOMBRE_ESTADO"]=="Devuelto"){
                $clase="red";
            }

            if($row["NOMBRE_ESTADO"]=="Revisado"){
                $clase="green";
            }

            ?>
            <td><span class="btn {{$clase}} btn-lg" style="border-radius:10px"
                      onclick="getBorrador({{$row["ID_BORRADOR"]}})">{{$row["NOMBRE_ESTADO"]}}</span></td>
            <td>{{$row["DESTINATARIO"]}}</td>
            <td>{{$row["USUA_LOGIN"]}}</td>

        </tr>
    @endforeach
    </tbody>
</table>