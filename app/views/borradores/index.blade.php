@extends('layouts.app')

@section('content')

    <?php
    $EDITOR_FUENTE_TAMANIO = 12;
    $EDITOR_FUENTE_TIPO = "Arial";
    function preg_grep_keys_values($pattern, $input, $flags = 0) {
        return array_merge(
          array_intersect_key($input, array_flip(preg_grep($pattern, array_keys($input), $flags))),
          preg_grep($pattern, $input, $flags)
        );
    }
    
    
    
    $configarray = array();
    
    foreach ($configuraciones as $row) {

        if ($row['NOMBRE_CONSTANTE'] == "EDITOR_FUENTE_TAMANIO") {
            $EDITOR_FUENTE_TAMANIO = $row['VALOR'];
        }
        if ($row['NOMBRE_CONSTANTE'] == "EDITOR_FUENTE_TIPO") {
            $EDITOR_FUENTE_TIPO = $row['VALOR'];
        }
        
        $configarray[$row['NOMBRE_CONSTANTE']] = $row['VALOR'];
    }
    $r_fr_r = preg_grep_keys_values('~FUNCTION_RADICADO_~i', $configarray);
    $r_tb_r = preg_grep_keys_values('~TIPO_BORRADORRADI_~i', $configarray);
    $r_tr_r = preg_grep_keys_values('~TIPO_RADICADO_~i', $configarray);
    $r_mr_r = preg_grep_keys_values('~MODAL_RADICADO_~i', $configarray);
    $r_for_r = preg_grep_keys_values('~FORMATO_RADICADO_~i', $configarray);
    $r_tir_r = preg_grep_keys_values('~TIPO_IDENTIFICADOR_RADICADO_~i', $configarray);
    $r_tbier_r = preg_grep_keys_values('~INTERNA_EXTENSIONES_RADICADO_~i', $configarray);
    $r_ar_r = preg_grep_keys_values('~ASUNTO_RADICADO_~i', $configarray);
    $rfunction_radicado_r = array_values($r_fr_r);
    $rtipo_borradorradi_r = array_values($r_tb_r);
    $rtipo_radicado_r = array_values($r_tr_r);
    $rmodal_radicado_r = array_values($r_mr_r);
    $rformato_radicado_r = array_values($r_for_r);
    $rtipo_identificador_r = array_values($r_tir_r);
    $rextensiones_radicado_r = array_values($r_tbier_r);
    $rasunto_radicado_r = array_values($r_ar_r);
    ?>

    <style>
        table.dataTable thead tr th {
            word-wrap: break-word;
            word-break: break-all;
        }

        .botonestabla {
            border-radius: 5px 5px 5px 5px !important;
        }

        .tt-input {
            font-size: 50px;
        }

        /*lo siguiente es la fuente predeterminada para el editor froala: Arial 12*/
        .fr-view {
            font-family: <?= $EDITOR_FUENTE_TIPO?>                 !important;
            font-size: <?= $EDITOR_FUENTE_TAMANIO?>pt !important;
            text-align: justify !important;
            line-height: 1 !important;
        }

        .popover, .tooltip {
            z-index: 9999999 !important;
        }

        /* lo siguiente es para el listado de expedientes, al buscar un expediente, un bordeado */
        .tt-suggestion {
            border-top: solid 1px rgb(218, 216, 217);

        }

    </style>


    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">


                <div class="portlet-body">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">

                                <div class="actions">

                                    <div class="btn-group">
                                        <a class="btn green btn-lg" href="javascript:;" data-toggle="dropdown"
                                           style="border-radius: 7px !important ">
                                            <i class="fa fa-plus"></i>
                                            <span class="hidden-xs" id="spanlistaexped">Nuevo Borrador</span>
                                            <i class="fa fa-angle-down"></i>
                                        </a>
                                        <ul class="dropdown-menu pull-left" id="ulexpedientes">
                                            <li title="Salida | Formato Oficio | Empresas - Ciudadanos">
                                                <a href="javascript:;"
                                                   data-texto="Salida | Formato Oficio | Empresas - Ciudadanos"
                                                   onclick="btnNuevoBorrador('<?= $_SESSION['SALIDA_OFICIO']?>')">
                                                    <?= $_SESSION['TIPO_BORRADOR_SALIDA_OFICIO']?>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;"
                                                   data-texto="Interna | Formato Comunicaci&oacute;n"
                                                   onclick="btnNuevoBorrador('<?= $_SESSION['INTERNA_COMUNICACION']?>')">
                                                    <?= $_SESSION['TIPO_BORRADOR_INTERNA_COMUNICACION']?>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;" data-texto="Interna | Formato pdf o Impreso"
                                                   onclick="btnNuevoBorrador('<?= $_SESSION['INTERNA_CON_PLANTILLA']?>')">
                                                    <?= $_SESSION['TIPO_BORRADOR_INTERNA_CON_PLANTILLA']?>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;" data-texto="Interna | Formato sin planilla"
                                                   onclick="btnNuevoBorrador('<?= $_SESSION['INTERNA_SIN_PLANTILLA']?>')">
                                                    <?= $_SESSION['TIPO_BORRADOR_INTERNA_SIN_PLANTILLA']?></a>
                                            </li>
                                            
                                            <?php
                                            $customBorrador = array();
                                            foreach ($rfunction_radicado_r as $key => $value) {
                                                $customBorrador[$key] = array("tipo_radicado" => json_decode($rtipo_radicado_r[$key]),
                                                                              "tipo_identificador" => $rtipo_identificador_r[$key],
                                                                              "tipo_borradorradi" => $rtipo_borradorradi_r[$key],
                                                                              "modal_radicado" => $rmodal_radicado_r[$key],
                                                                              "function_radicado" => $rfunction_radicado_r[$key],
                                                                              "formato_radicado" => $rformato_radicado_r[$key],
                                                                              "extensiones_radicado" => json_decode($rextensiones_radicado_r[$key]),
                                                                              "asunto_radicado" => $rasunto_radicado_r[$key]);
                                                
 
                                                if ($value == 'SI'){
                                                ?>
                                                <li><a href="javascript:;"  data-texto="<?= $rtipo_borradorradi_r[$key] ?>"
                                                onclick="btnNuevoBorrador('<?= $rtipo_identificador_r[$key] ?>',<?= htmlspecialchars(json_encode('{"tipo_radicado":'.$rtipo_radicado_r[$key].',"tipo_identificador":"'.
                                                                              $rtipo_identificador_r[$key].'","tipo_borradorradi":"'.
                                                                              $rtipo_borradorradi_r[$key].'","modal_radicado":"'.
                                                                              $rmodal_radicado_r[$key].'","function_radicado":"'.
                                                                              $rfunction_radicado_r[$key].'","formato_radicado":"'.
                                                                              $rformato_radicado_r[$key].'","extensiones_radicado":'.
                                                                              $rextensiones_radicado_r[$key].',"asunto_radicado":"'.
                                                                              $rasunto_radicado_r[$key].'"}')); ?>)">
                                                
                                                <?= $rtipo_borradorradi_r[$key]; ?> 
                                                </a></li>
                                                <?php
                                                }
                                                
                                                
                                            }
                                            
                                            $_SESSION['TIPOS_BORRADOR_CUSTOM'] = $customBorrador;

                                            ?>
                                            
                                            <?php if($_SESSION['FUNCTION_REPORTE4'] == 'SI'){ ?>
                                            <li class="divider"></li>
                                            <li>
                                                <a href="javascript:;" data-texto="REPORTE4"
                                                   onclick="btnNuevoBorrador('<?= $_SESSION['REPORTE4']?>')"><?= $_SESSION['TIPO_BORRADOR_REPORTE4']?></a>
                                            </li>
                                            <?php } ?>

                                        </ul>
                                    </div>
                                    <span class="botonestabla label label-sm blue" style="color:#778899 !important">
                                Proyectar nuevo documento</span>
                                </div>

                            </div>

                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-12">
                                        <button title="Configuraciones"
                                                style="text-align:right;float:right !important;border-radius:20px !important; background-color:transparent"
                                                onclick="modalConfig()">
                                            <i class="fa fa-wrench" aria-hidden="true"></i>&nbsp
                                        </button>
                                    </div>
                                </div>
                                @if($msg!="")
                                    <div class="col-lg-12" style="color:green">
                                        {{$msg}}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>


                    <div id="sample_1_wrapper" class="dataTables_wrapper no-footer">

                        <table class="table table-striped table-bordered table-hover
                                 table-checkable order-column dataTable no-footer" id="tabla">
                            <thead>
                            <tr role="row">

                                <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1"
                                    aria-label=" Id : activate to sort column ascending" style="width: 15%;"> Id
                                </th>

                                <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1"
                                    aria-label=" Asunto : activate to sort column ascending" style="width: 40%;"> Asunto
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1"
                                    aria-label=" Estado : activate to sort column ascending" style="width: 15%"> Estado
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1"
                                    aria-label=" Destinatario : activate to sort column ascending" style="width: 15%">
                                    Destinatario
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1"
                                    aria-label=" Usuario Anterior : activate to sort column ascending"
                                    style="width:15%">
                                    Usuario Anterior
                                </th>

                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>


                    </div>


                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

@endsection

@section('page-modal')
    <div id="all_modal" class="modal fade" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog " style="width:63%;">
            <div class="modal-content">

                <div class="modal-header bg-blue-chambray bg-font-yellow text-center">

                    <button type="button" title="Cerrar" aria-hidden="true"
                            class="btn btn-lg "
                            style="float: right;background-color: #2C3E50!important; border-radius:20px !important"
                            onclick="cerrarmodborrador()">
                        <i class="fa fa-remove"></i></button>

                    <h4 class="modal-title"><strong id="titulomodal">NUEVO BORRADOR</strong></h4>
                </div>
                <div class="modal-body" id="radicated_info">
                    <form action="" method="POST" class="form-horizonal" id="search-form"
                          enctype="multipart/form-data">
                        <input type="hidden" id="tipo_borrador_dest" name="tipo_borrador_dest">

                        <div class="form-body modal-body todo-task-modal-body">
                            <div class="form-actions">

                                <div class="row" id="divrowbtns">
                                    <div class="col-md-12">
                                        <div class="btn-group" style="float: right;">


                                            <button type="button" title="Enviar borrador" id="div_enviar"
                                                    style="border-top-right-radius:0px !important;
                                                    border-top-left-radius:20px !important;
                                                       border-bottom-left-radius:20px !important;"
                                                    onclick="enviarBorrador()" class="btn btn-lg blue">
                                                <i class="fa fa-arrow-right"></i>
                                                Enviar
                                            </button>
                                            <button type="button"
                                                    class="btn blue btn-lg dropdown-toggle depende_div_enviar"
                                                    style="margin-right: 10px;
                                                    float:right;
                                                    border-top-right-radius:20px !important;
border-bottom-right-radius:20px !important;" data-toggle="dropdown"
                                                    aria-expanded="false">
                                                <i class="fa fa-angle-down"></i>
                                            </button>

                                            <ul class="dropdown-menu" role="menu" id="dropdown_enviar_a">
                                                <?php
                                                if(count($jefes_dropdown_enviar) > 0){
                                                foreach ($jefes_dropdown_enviar as $row){
                                                ?>
                                                <li>
                                                    <a href="#" data-nombre_con_dependencia="<?= $row['NOMBRE'] ?>"
                                                       onclick="clickdropdownenviar(this,<?= $row['ID'] ?>)">Enviar a
                                                        (<?= $row['USUA_NOMB'] ?>) </a>
                                                </li>
                                                <?php  }
                                                }
                                                ?>
                                            </ul>
                                        </div>


                                        <button type="button" id="div_no_aprobar" class="btn btn-lg red-pink"
                                                title="Rechazado para corregir"
                                                onclick="btn_no_aprobar()"
                                                style="border-radius:20px !important;margin-right: 10px;float:right">
                                            <i class="glyphicon glyphicon-remove"></i>
                                            No Aprobar
                                        </button>

                                        <button type="button" id="buttom_aprobar"
                                                onclick="botonaprobar(event)"
                                                class="btn btn-lg green"
                                                style="border-radius:20px;margin-right: 10px;float:right">
                                            <i class="fa fa-check-square-o"></i>
                                            Aprobar
                                        </button>

                                        <button type="button" class="btn btn-lg" id="div_radicar"
                                                style="border-radius:20px !important; margin-right:10px;
                                                background-color: orange;float:right" onclick="formRadicar()">
                                            <i class="fa fa-barcode"></i>
                                            Radicar
                                        </button>

                                        <button type="button" id="div_expediente" class="btn btn-lg yellow  "
                                                style="border-radius:20px !important;margin-right: 10px;float:right"
                                                title="Incluir en expediente"
                                                onclick="modalExpedienteBorrad('NOCREAREXPEDSINRAD')">
                                            <i class="fa fa-folder-open-o" data-toggle="tooltip"
                                            ></i>
                                        </button>

                                        <button type="button" class="btn btn-lg blue-dark guardar" id="primerguardar"
                                                onclick="crearBorrador(event)"
                                                style="border-radius:20px !important; margin-right: 10px;float:left">
                                            <i class="fa fa-save"></i>
                                            Guardar
                                        </button>
                                    </div>
                                </div>

                                <div class="row" id="rowtitulomemo">
                                </div>

                            </div>
                            <input required="required" type="hidden" name="borrador_id" value=""
                                   id="borrador_id" class="form-control" style="border: solid grey 1px" placeholder="">
                            <input required="required" type="hidden" name="radicadoactual" value=""
                                   id="radicadoactual" class="form-control" style="border: solid grey 1px">

                            <input type="hidden" name="editoroplantilla" value="EditorWeb"
                                   id="editoroplantilla" class="form-control" style="border: solid grey 1px">

                            <div class="row" id="div_advertenciacabecera">

                            </div>

                            <div class="row" id="div_margin_top_princ">
                                <div class="col-md-12 ">
                                    <div class="form-group">
                                        <label class=" col-md-2" id="lbldestinatario">Destinatario: </label>
                                        <div class="col-md-10">
                                            <div class="input-group input-icon right"
                                                 style="margin-bottom: 20px !important;"
                                                 id="div_primerafila_dinamica">

                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 " style="margin-top:10px" id="divasunto">
                                    <div class="form-group">
                                        <label class=" col-md-2">Asunto: </label>
                                        <div class="col-md-10">
                                            <div class="input-icon right">
                                                <textarea required="required" name="asunto" id="asunto"
                                                          rows="3" maxlength="999"
                                                          class="form-control campos_valida_keyup"
                                                          title="Escriba un asunto amplio"
                                                          placeholder=""></textarea>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 ">
                                    <div class="form-group">
                                        <label class=" col-md-2"> </label>
                                        <div class="col-md-10">

                                        </div>
                                    </div>


                                    <div class="col-md-12 " style="margin-top:10px" id="div_edit">
                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <!--  <div class="col-lg-12 " id="editor" >

                                                  </div>-->

                                                <div class="col-lg-12 nopadding">
                                                    <textarea id="froalaeditor" name="edit"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 " style="margin-top:10px" id="div_folios_raiz">
                                        <div class="form-group">


                                            <label class=" col-md-2"
                                                   style="width:19.66667%; font-size:10pt;color:#bdbdbd !important ">Folios
                                                Comunicaci&oacute;n:</label>
                                            <div class="col-md-1">

                                                <input type="number" name="folios_comunicacion_raiz"
                                                       style="padding: 2px 0px;font-size:12pt;color:#bdbdbd !important "
                                                       min="0" value="1" autocomplete="off"
                                                       id="folios_comunicacion_raiz" class="form-control"
                                                       placeholder="">
                                            </div>


                                            <label class=" col-md-2"
                                                   style="width:13.66667%;font-size:10pt ;color:#bdbdbd !important ">Folios
                                                Anexos:</label>
                                            <div class="col-md-1">

                                                <input type="number" name="folios_anexos_raiz" min="0" value="0"
                                                       autocomplete="off"
                                                       id="folios_anexos_raiz" class="form-control"
                                                       style="padding: 2px 0px;font-size:12pt;color:#bdbdbd !important  "
                                                       placeholder="">
                                            </div>

                                            <label class=" col-md-2"
                                                   style="width:15.66667%; font-size:10pt;color:#bdbdbd !important ">Descripci&oacute;n
                                                de anexos:</label>
                                            <div class="col-md-4">
                                                <input required="required" style="padding: 2px 0px; font-size:12pt; "
                                                       type="text"
                                                       name="desc_anexos_raiz" value=""
                                                       id="desc_anexos_raiz"
                                                       class="form-control"
                                                       placeholder="Anexos como planos, libros, CD...">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12" style="display:none" id="div_loader">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <div class="progress progress-striped active">
                                                    <div class="progress-bar progress-bar-success" role="progressbar"
                                                         aria-valuenow="0" aria-valuemin="0"
                                                         aria-valuemax="100" style="width: 0%" id="loader">
                                                        <span class="sr-only"> 0% Completado </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12" style="display:none" id="div_cargardoc">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <div class="input-icon right" id="div_previo_cargardoc">
                                                    <input type="file"
                                                           name="userfile" value=""
                                                           accept=".docx,application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                                                           id="cargardoc"
                                                           class="form-control" placeholder="Seleccione el archivo">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 " style="margin-top:10px">
                                        <div class="form-group has-error" id="div_subirplantilla">

                                        </div>
                                    </div>

                                    <!--<div class="col-md-12 " style="margin-top:10px">
                                        <div class="form-group has-error">
                                            <div class="col-md-12">
                                                <div class="input-icon right ">
                                                    <button class="btn green-haze" type="button"
                                                            onclick="showModalEjemplo()" id="botonejemplo"
                                                            style="float: left;border-radius:20px !important"
                                                    ><i class="fa fa-table" style="font-size: 10px;"></i></button>
                                                    <h4>EJEMPLOS ANEXOS TIPOS
                                                    </h4>
                                                    <br>
                                                </div>
                                            </div>
                                        </div>
                                    </div> -->

                                    <div class="col-md-12 " style="margin-top:10px">
                                        <div class="form-group has-error">
                                            <div class="col-md-12">
                                                <div class="input-icon right assoc">
                                                    <button class="btn green-haze" type="button"
                                                            onclick="modalradicado()" id="botonaddradicado"
                                                            style="float: left;border-radius:20px !important"
                                                    ><i class="fa fa-plus" style="font-size: 10px;"></i></button>
                                                    <h4>RADICADOS ASOCIADOS
                                                    </h4>
                                                    <br>

                                                    <div id="append_radiaso"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 " style="margin-top:10px">
                                        <div class="form-group has-error">
                                            <div class="col-md-12">
                                                <div class="input-icon right assoc">
                                                    <button class="btn green-haze" type="button" id="botonaddanexo"
                                                            onclick="modalanadiranexo()"
                                                            style="float: left;border-radius:20px !important"
                                                    ><i class="fa fa-plus" style="font-size: 10px;"></i></button>
                                                    <h4>ANEXOS</h4>
                                                    <br>
                                                    <div id="append_anexos"></div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-12 " style="margin-top:10px">
                                        <div class="form-group has-error">
                                            <div class="col-md-12">
                                                <div class="input-icon">


                                                    <div class="portlet light " style="padding:0px">
                                                        <div class="portlet-title">
                                                            <div class="caption">
                                                                <i class="icon-bubble font-hide hide"></i>
                                                                <span class="caption-subject font-hide bold uppercase">HISTÓRICO</span>
                                                            </div>
                                                            <button class="btn btn-sm "
                                                                    onclick="modalAddComentBorrad(event)"><i
                                                                        class="fa fa-commenting"></i>Agregar Comentario
                                                            </button>
                                                            <div class="actions">
                                                                <div class="portlet-input input-inline">
                                                                    <div class="input-icon right">
                                                                        <i class="icon-magnifier"></i>
                                                                        <input type="text"
                                                                               class="form-control input-circle"
                                                                               placeholder="buscar..."
                                                                               id="buscarcomentarios"
                                                                               style="border: solid grey 1px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="portlet-body" id="chats">
                                                            <div class="slimScrollDiv"
                                                                 style="position: relative; overflow: hidden; width: auto; height: auto;">
                                                                <div class="scroller"
                                                                     style="height: auto; overflow: hidden; width: auto;"
                                                                     data-always-visible="1" data-rail-visible1="1"
                                                                     data-initialized="1">
                                                                    <ul class="chats" id="ulchat">

                                                                    </ul>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 " style="margin-top:10px; text-align:center">
                                        <div class="form-group has-error">
                                            <div class="col-md-12">
                                                <div class="input-icon right assoc">
                                                    <a href="#"
                                                       style="color:#93B5F9; text-transform: none; display:none">M&aacute;s
                                                        comentarios....</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>

                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-5 col-md-4">
                                        <button type="button" class="btn btn-lg blue-dark guardar"
                                                id="segundoguardar" style="border-radius:20px !important"
                                                onclick="crearBorrador(event)"
                                        >
                                            <i class="fa fa-save"></i>
                                            Guardar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" onclick="cerrarmodborrador()">Cerrar</button>
                </div>
            </div>
        </div>
    </div>


    <div id="modal_expediente" class="modal fade" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-header bg-blue-chambray bg-font-yellow text-center">
                    <button type="button" title="Cerrar" onclick="cerrarModalExpediente()"
                            class="btn btn-lg "
                            style="float: right;background-color: #2C3E50!important; border-radius:20px !important">
                        <i class="fa fa-remove"></i></button>
                    <h4 class="modal-title"><strong>Buscar Expedientes</strong></h4>
                </div>


                <div class="modal-body" id="radicated_info">

                    <form action="" method="POST" class="form-horizonal" id="">

                        <div class="form-body modal-body todo-task-modal-body">

                            <div class="form-actions">
                                <div class="row"></div>
                            </div>

                            <div class="row" style="margin-top:40px">
                                <div class="col-md-12 ">


                                    <input type="hidden"
                                           id="expedincluidcoma">


                                    <div>
                                        <div>
                                            <div id="buttonRadExp" class="row" style="display: none;">
                                                <div class="col-md-9"></div>
                                                <div class="col-md-3">
                                                    <button type="button" class="btn btn-lg" id="div_radicar"
                                                                              style="border-radius:20px !important; margin-right:10px;
                                                background-color: orange;float:right" onclick="formRadicar()">
                                                        <i class="fa fa-barcode"></i>
                                                        Radicar
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 search-page search-content-2"
                                                     id="divinputexpediente"
                                                     style="height:100px !important;min-height:100px !important">
                                                    <div class="input-group search-bar bordered">
                                                        <input type="text"
                                                               style="color: #5E6373 !important;font-size: 16px !important"
                                                               id="inputexpediente" class="form-control typeahead"
                                                               title="Busca en los últimos 2 años, en el titulo, descripción y número. Clic en [Ampliar Búsqueda] para más resultados"
                                                               placeholder="Buscar por número de expediente o titulo en los 2 últimos años">
                                                        <span class="input-group-btn"
                                                              title="Clic para buscar en todos los campos del expediente y años.">
                        <button class="btn blue uppercase bold" type="button" id="search-expediente"
                                onclick="buscartodoTrue(event)">Ampliar Búsqueda</button>
                    </span>
                                                    </div>

                                                </div>

                                                <div class="col-md-12" id="divnohayexpedientes">
                                                    <div class="input-group ">

                                                    </div>

                                                </div>

                                                <div class="row" id="divbuttonbusquedaAv">
                                                    <div class="col-md-12" style="text-align: left">
                                                        <div class=" col-md-12 input-group has-warning"
                                                             style="text-align: left;display: inline-block;"
                                                             id="divApendBusqAvan">


                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="divBusquedaAvanzada" style="display:none">

                                                    <div class="form-group">
                                                        <label class=" col-md-2">Dependencia: </label>
                                                        <div class="col-md-10">
                                                            <div class="input-group input-icon right"
                                                                 style="margin-bottom: 20px !important;">


                                                                <select name="selectdependencia" class="form-control"
                                                                        id="selectdependencia">

                                                                </select>

                                                            </div>

                                                        </div>
                                                        <label class=" col-md-2">Serie: </label>
                                                        <div class="col-md-10">
                                                            <div class="input-group input-icon right"
                                                                 style="margin-bottom: 20px !important;">

                                                                <select name="serieavanzada" class="form-control"
                                                                        id="serieavanzada">

                                                                </select>

                                                            </div>

                                                        </div>
                                                        <label class=" col-md-2">Subserie: </label>
                                                        <div class="col-md-10">
                                                            <div class="input-group input-icon right"
                                                                 style="margin-bottom: 20px !important;">

                                                                <select name="subserieavanzada" class="form-control"
                                                                        id="subserieavanzada">

                                                                </select>

                                                            </div>

                                                        </div>
                                                        <label class=" col-md-2">Año: </label>
                                                        <div class="col-md-10">
                                                            <div class="input-group input-icon right"
                                                                 style="margin-bottom: 20px !important;">


                                                                <select name="selectanio" class="form-control"
                                                                        id="selectanio">

                                                                </select>

                                                            </div>

                                                        </div>
                                                        <label class=" col-md-2"><input type="hidden"></label>
                                                        <label class=" col-md-2">
                                                            <button type="button" class="btn green"
                                                                    onclick="resultAvanzados()">Buscar
                                                            </button>
                                                        </label>
                                                        <label class=" col-md-2">
                                                            <button type="button" class="btn default"
                                                                    onclick="limpiarBusquedaAvanz()">Limpiar
                                                            </button>
                                                        </label>
                                                        <label class=" col-md-2">
                                                            <button type="button" class="btn red"
                                                                    onclick="createNewExp(event)">Crear Nuevo Expediente
                                                            </button>
                                                        </label>

                                                        <label class=" col-md-3"
                                                               style="float:center;text-align: center ">
                                                            <button type="button" class="btn default"
                                                                    onclick="cerrarBusquedaAvanz()">Cerrar
                                                            </button>
                                                        </label>


                                                    </div>
                                                </div>


                                                <div style="display:none" id="divinputtipologia">
                                                    <div class="col-md-12 search-page search-content-2">
                                                        <div class="search-bar bordered">
                                                            <div class="input-group" style="width: 100%;"
                                                                 id="divpreviotipologia">
                                                                <input type="text"
                                                                       style="color: #5E6373 !important;font-weight: 900 !important;font-size: 16px !important; width:100%;
                                                            border: 1px solid red !important;box-shadow:0 0 3px red !important;margin:10px !important"
                                                                       class="form-control typeahead"
                                                                       id="inputtipologia"
                                                                       placeholder="Elegir tipo documental">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>


                                        </div>
                                    </div>

                                    <div class="table-responsive  " id="open_table_expediente"
                                         style="position: relative;">
                                        <table class="table table-striped table-bordered table-hover
                                 table-checkable order-column dataTable no-footer" id="tablaexpedientes">
                                            <thead class="bg-blue font-white bold">
                                            <tr role="row">

                                                <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1"
                                                    colspan="1"
                                                    aria-label=" Id : activate to sort column ascending"
                                                    style="width: 20%;text-align:center"> Archivar
                                                </th>

                                                <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1"
                                                    colspan="1"
                                                    aria-label=" Asunto : activate to sort column ascending"
                                                    style="width: 40%;text-align:center"> Expediente
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1"
                                                    colspan="1"
                                                    aria-label=" Estado : activate to sort column ascending"
                                                    style="width: 40%;text-align:center">
                                                    Dependencia y TRD
                                                </th>

                                            </tr>
                                            </thead>
                                            <tbody id="tbodyexpediente"></tbody>
                                        </table>
                                    </div>

                                </div>

                            </div>
                        </div>


                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-lg" data-dismiss="modal"
                            style="border-radius:20px !important">
                        Cerrar
                    </button>
                </div>
            </div>
        </div>
    </div>


    @include('expedientes.partials.modalnuevoexpediente')


    <div id="modal_enviar" class="modal fade" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-header bg-blue-chambray bg-font-yellow text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><strong>ENVIAR</strong></h4>
                </div>
                <div class="modal-body" id="radicated_info">
                    <form action="" method="POST" class="form-horizonal" id="">
                        <div class="form-body modal-body todo-task-modal-body">
                            <div class="form-actions">
                                <div class="row"></div>
                            </div>
                            <div class="row" style="margin-top:40px">
                                <div class="col-md-12 ">
                                    <div class="form-group has-error">
                                        <label class=" col-md-2">Enviar a: </label>
                                        <div class="col-md-10">
                                            <div class="input-icon right">

                                                <select class="js-data-example-ajax" id="enviara" name="">

                                                </select>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 " style="margin-top:10px">
                                    <div class="form-group has-error">
                                        <label class=" col-md-2">Comentario: </label>
                                        <div class="col-md-10">
                                            <div class="input-icon right">
                                                <textarea required="required" name="comentarioenviar"
                                                          id="comentarioenviar" maxlength="999" class="form-control"
                                                          style="border: solid grey 1px; height:90px"
                                                          placeholder=""></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 " style="margin-top:10px; display: none"
                                     id="div_enviar_marcar_como_revisado">
                                    <div class="form-group">
                                        <label class=" col-md-2">&nbsp;</label>
                                        <div class="col-md-10">
                                            <div class="input-icon right">
                                                <input type="checkbox" class="icheck"
                                                       data-checkbox="icheckbox_flat-green"
                                                       value="1" id="check_enviar_marcar_como_revisado">&nbsp;&nbsp;Marcar
                                                como revisado
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>


                    </form>
                </div>
                <div class="modal-footer">
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-4">
                                <button type="button" class="btn btn-lg blue"
                                        id="guardarEnviar" onclick="onclickguardarenv()"
                                        style="border-radius:20px !important">
                                    <i class="fa fa-arrow-right"></i>
                                    Enviar
                                </button>

                            </div>

                            <div class="col-md-4">
                                <button type="button" class="btn btn-lg red" data-dismiss="modal"
                                        style="border-radius:20px !important">
                                    Cancelar
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal_comentario_revisar" class="modal fade" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-header bg-blue-chambray bg-font-yellow text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><strong>Marcar documento como revisado</strong></h4>
                </div>
                <div class="modal-body" id="radicated_info">
                    <form action="" method="POST" class="form-horizonal" id="">
                        <div class="form-body modal-body todo-task-modal-body">
                            <div class="form-actions">
                                <div class="row"></div>
                            </div>
                            <div class="row" style="margin-top:40px">

                                <div class="col-md-12 " style="margin-top:10px">
                                    <div class="form-group has-error">
                                        <label class=" col-md-3">Comentario (opcional): </label>
                                        <div class="col-md-9">
                                            <div class="input-icon right">
                                                <textarea required="required" name="comentariorevisar"
                                                          id="comentariorevisar" class="form-control" maxlength="999"
                                                          style="border: solid grey 1px; height:90px"
                                                          placeholder=""></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-lg green"
                            id="" onclick="guardarRevisarBorrador()"
                            style="border-radius:20px !important">
                        <i class="fa fa-check-square-o"></i>
                        Revisado
                    </button>
                    <button type="button" class="btn btn-lg" data-dismiss="modal"
                            style="border-radius:20px !important">
                        Cancelar
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div id="modal_comentario_borrador" class="modal fade" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-header bg-blue-chambray bg-font-yellow text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><strong>Agregar comentario al borrador</strong></h4>
                </div>
                <div class="modal-body" id="radicated_info">
                    <form action="" method="POST" class="form-horizonal" id="">
                        <div class="form-body modal-body todo-task-modal-body">
                            <div class="form-actions">
                                <div class="row"></div>
                            </div>
                            <div class="row" style="margin-top:40px">

                                <div class="col-md-12 " style="margin-top:10px">
                                    <div class="form-group has-error">
                                        <label class=" col-md-3">Comentario:</label>
                                        <div class="col-md-9">
                                            <div class="input-icon right">
                                                <textarea required="required" name="comentarioaborrador"
                                                          id="comentarioaborrador" class="form-control" maxlength="999"
                                                          style="border: solid grey 1px; height:90px"
                                                          placeholder=""></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-lg blue"
                            id="" onclick="clickSaveComentBorrad(event)"
                            style="border-radius:20px !important">
                        <i class="fa fa-check-square-o"></i>
                        Comentar
                    </button>
                    <button type="button" class="btn btn-lg" data-dismiss="modal"
                            style="border-radius:20px !important">
                        Cancelar
                    </button>
                </div>
            </div>
        </div>
    </div>


    <div id="modal_comentario_aprobar" class="modal fade" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-header bg-blue-chambray bg-font-yellow text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><strong>Marcar documento como Aprobado</strong></h4>
                </div>
                <div class="modal-body" id="radicated_info">
                    <form action="" method="POST" class="form-horizonal" id="">
                        <div class="form-body modal-body todo-task-modal-body">
                            <div class="form-actions">
                                <div class="row"></div>
                            </div>
                            <div class="row" style="margin-top:40px">

                                <div class="col-md-12 " style="margin-top:10px">
                                    <div class="form-group has-error">
                                        <label class=" col-md-3">Comentario:</label>
                                        <div class="col-md-9">
                                            <div class="input-icon right">
                                                <textarea required="required" name="comentarioaprobar"
                                                          id="comentarioaprobar" class="form-control" maxlength="999"
                                                          style="border: solid grey 1px; height:90px"
                                                          placeholder=""></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-lg green"
                            id="" onclick="btnModalComentAprobar()"
                            style="border-radius:20px !important">
                        <i class="fa fa-check-square-o"></i>
                        Aprobar
                    </button>
                    <button type="button" class="btn btn-lg" data-dismiss="modal"
                            style="border-radius:20px !important">
                        Cancelar
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div id="modal_comentario_devolver" class="modal fade" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-header bg-blue-chambray bg-font-yellow text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><strong>DEVOLVER</strong></h4>
                </div>
                <div class="modal-body" id="radicated_info">
                    <form action="" method="POST" class="form-horizonal" id="">
                        <div class="form-body modal-body todo-task-modal-body">
                            <div class="form-actions">
                                <div class="row"></div>
                            </div>
                            <div class="row" style="margin-top:20px">

                                <div class="col-md-12 ">
                                    <div class="form-group has-error">
                                        <h4 id="labelnombredevolver"></h4>

                                    </div>
                                </div>

                                <div class="col-md-12 " style="margin-top:10px">
                                    <div class="form-group has-error">
                                        <label class=" col-md-2">Comentario: </label>
                                        <div class="col-md-10">
                                            <div class="input-icon right">
                                                <textarea required="required" name="comentariodevolver"
                                                          id="comentariodevolver" maxlength="999" class="form-control"
                                                          style="border: solid grey 1px; height:90px"
                                                          placeholder=""></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-12" style="align-content: center; text-align: center; float:center">

                                    <button type="button" id="" class="btn btn-lg yellow"
                                            onclick="guardardevolver()"
                                            style="border-radius:20px !important;margin-right: 10px;float:center">
                                        <i class="fa fa-undo"></i>
                                        Devolver
                                    </button>

                                    <button type="button" class="btn btn-lg blue"
                                            style="border-radius:20px !important;margin-right: 10px;float:center"
                                            title="Enviar borrador"
                                            onclick="enviarBorrador()">
                                        <i class="fa fa-arrow-right"></i>
                                        Enviar a otro usuario
                                    </button>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>


    <div id="modal_coment_no_aprobar" class="modal fade" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-header bg-blue-chambray bg-font-yellow text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><strong id="strong_title_no_aprobar">DEVOLVER PARA AJUSTAR</strong></h4>
                </div>
                <div class="modal-body" id="radicated_info">
                    <form action="" method="POST" class="form-horizonal" id="">
                        <div class="form-body modal-body todo-task-modal-body">
                            <div class="form-actions">
                                <div class="row"></div>
                            </div>
                            <div class="row" style="margin-top:20px">

                                <div class="col-md-12 ">
                                    <div class="form-group has-error">
                                        <h4 id="labelnombre_no_aprobar"></h4>

                                    </div>
                                </div>

                                <div class="col-md-12 " style="margin-top:10px">
                                    <div class="form-group has-error">
                                        <label class=" col-md-2">Comentario: </label>
                                        <div class="col-md-10">
                                            <div class="input-icon right">
                                                <textarea required="required" name="comentario_no_aprobar"
                                                          id="comentario_no_aprobar" maxlength="999"
                                                          class="form-control"
                                                          style="border: solid grey 1px; height:90px"
                                                          placeholder=""></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-12" style="align-content: center; text-align: center; float:center">

                                    <button type="button" id="" class="btn btn-lg yellow"
                                            onclick="guardarnoaprobar()"
                                            style="border-radius:20px !important;margin-right: 10px;float:center">
                                        <i class="fa fa-undo"></i>
                                        No Aprobar
                                    </button>

                                    <button type="button" class="btn btn-lg blue"
                                            style="border-radius:20px !important;margin-right: 10px;float:center"
                                            title="Enviar borrador"
                                            onclick="enviarBorrador()">
                                        <i class="fa fa-arrow-right"></i>
                                        Enviar a otro usuario
                                    </button>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <div id="modalcargosfirmantes" class="modal fade" aria-hidden="true">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-header bg-blue-chambray bg-font-yellow text-center">

                    <button type="button" title="Cerrar" aria-hidden="true"
                            class="btn btn-lg "
                            style="float: right;background-color: #2C3E50!important; border-radius:20px !important"
                            onclick="cerrarModalFirmantes('BORRADOR','modalcargosfirmantes');">
                        <i class="fa fa-remove"></i></button>

                    <h4 class="modal-title"><strong>Escriba el cargo y seleccione el firmante principal</strong></h4>
                </div>
                <div class="modal-body">

                    <br>

                    <table role="presentation" class="table table-bordered table-striped" border="1">
                        <thead>
                        <th>Nombre</th>
                        <th>Cargo</th>
                        <th>Remitente Principal</th>
                        <th>Firmantes</th>
                        </thead>
                        <tbody id="tbodycargosfirmantes"></tbody>
                    </table>

                </div>
                <div class="modal-footer">
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-6">
                                <button type="button" class="btn btn-lg red"
                                        style="border-radius:20px !important"
                                        onclick="cerrarModalFirmantes('BORRADOR','modalcargosfirmantes');">
                                    Guardar y cerrar
                                </button>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

   <?php /*@include('borradores.partials.modalVersionamiento')

    @include('borradores.partials.tablaAnexosTipos') */
   ?>

    <div id="modalanadiranexo" class="modal fade" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-header bg-blue-chambray bg-font-yellow text-center">
                    <button type="button" title="Cerrar" aria-hidden="true" class="btn btn-lg "
                            style="float: right;background-color: #2C3E50!important; border-radius:20px !important"
                            onclick="cerralModalAnexo()">
                        <i class="fa fa-remove"></i></button>
                    <h4 class="modal-title"><strong>AGREGAR ANEXO</strong></h4>
                </div>
                <div class="modal-body" id="radicated_info">
                    <form id="formanexo" action="https://jquery-file-upload.appspot.com/" method="POST"
                          enctype="multipart/form-data">
                        <!-- Redirect browsers with JavaScript disabled to the origin page -->
                        <noscript><input type="hidden" name="redirect"
                                         value="https://blueimp.github.io/jQuery-File-Upload/"></noscript>
                        <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
                        <div class="row fileupload-buttonbar">
                            <div class="col-lg-10">
                                <!-- The fileinput-button span is used to style the file input field as button -->


                                <!-- The fileinput-button span is used to style the file input field as button -->
                                <span class="btn green fileinput-button">
                                                <i class="fa fa-plus"></i>
                                                <span>Agregar archivos</span>
                                                <input type="file" name="files[]" multiple=""
                                                       style="position: absolute;
    top: 0;
    left: 0;
    margin: 0;
    opacity: 0;
    -ms-filter: 'alpha(opacity=0)';
    font-size: 5px;
    direction: ltr;
    cursor: pointer;
height: 30px;"> </span>

                                <input type="hidden" name="id_borrador_anexo" id="id_borrador_anexo">


                                <button type="submit" class="btn btn-primary start">
                                    <i class="glyphicon glyphicon-upload"></i>
                                    <span>Subir todos</span>
                                </button>
                                <button type="reset" class="btn btn-warning cancel">
                                    <i class="glyphicon glyphicon-ban-circle"></i>
                                    <span>Cancelar todo</span>
                                </button>
                                <button type="button" class="btn red delete btn_anexo_del_todos"
                                        style="display:none !important">
                                    <i class="fa fa-trash"></i>
                                    <span>Borrar todos</span>
                                </button>
                                <input type="checkbox" class="toggle btn_anexo_del_todos"
                                       style="display:none !important">
                                <!-- The global file processing state -->
                                <span class="fileupload-process"></span>
                            </div>
                            <!-- The global progress state -->
                            <div class="col-lg-5 fileupload-progress fade">
                                <!-- The global progress bar -->
                                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0"
                                     aria-valuemax="100">
                                    <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                                </div>
                                <!-- The extended global progress state -->
                                <div class="progress-extended">&nbsp;</div>
                            </div>
                        </div>
                        <!-- The table listing the files available for upload/download -->
                        <table role="presentation" class="table table-striped">
                            <tbody class="files"></tbody>
                        </table>
                    </form>

                    <br>

                    <!-- The blueimp Gallery widget -->
                    <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
                        <div class="slides"></div>
                        <h3 class="title"></h3>
                        <a class="prev">‹</a>
                        <a class="next">›</a>
                        <a class="close">×</a>
                        <a class="play-pause"></a>
                        <ol class="indicator"></ol>
                    </div>

                    <script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td>
            <span class="preview"></span>
        </td>
        <td>
            <p class="name">{%=file.name%}</p>
            <strong class="error text-danger"></strong>
        </td>
        <td>
            <p class="size">Procesando...</p>
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
        </td>
        <td>
            {% if (!i && !o.options.autoUpload) { %}
                <button class="btn btn-primary start" disabled>
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Subir</span>
                </button>
            {% } %}
            {% if (!i) { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Borrar</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}



























                    </script>
                    <!-- The template to display files available for download -->
                    <script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade" id="trbodyanexos{%=file.anexo_id%}">
        <td>
            <span class="preview">
                {% if (file.thumbnailUrl) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-id="{%=file.anexo_id%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                {% } %}
            </span>
        </td>
        <td>
            <p class="name">
                {% if (file.url) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-id="{%=file.anexo_id%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                {% } else { %}
                    <span>{%=file.name%}</span>
                {% } %}
            </p>
            {% if (file.error) { %}
                <div><span class="label label-danger">Error</span> {%=file.error%}</div>
            {% } %}
        </td>
        <td>
            <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </td>
        <td>
            {% if (file.delete_url) { %}
                <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.delete_url%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Borrar</span>
                </button>
                <input type="checkbox" name="delete" value="1" class="toggle">
            {% } else { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cerrar</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}



























                    </script>
                </div>
                <div class="modal-footer">


                    <div class="row">
                        <div class="col-md-12" style="text-align: left">
                            <p>* Tamaño máximo permitido por archivo: <strong id="valorTam"></strong>,
                                * Cantidad de archivos permitidos por
                                subida: <?php echo ini_get('max_file_uploads ') ?></p>
                            <div class="col-md-5">
                                <button type="button" class="btn dark btn-outline" onclick="cerralModalAnexo()">Cerrar
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="modal_radicado" class="modal fade" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-header bg-blue-chambray bg-font-yellow text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><strong>Asociar Radicados</strong></h4>
                </div>
                <div class="modal-body" id="radicated_info">
                    <form action="" method="POST" class="form-horizonal" id="">
                        <div class="form-body modal-body todo-task-modal-body">
                            <div class="form-actions">
                                <div class="row"></div>
                            </div>
                            <div class="row" style="margin-top:40px">
                                <div class="col-md-12 ">
                                    <div class="form-group has-error">
                                        <label class=" col-md-2">Buscar radicado a asociar: </label>
                                        <div class="col-md-10">
                                            <div class="input-icon right">

                                                <select class="js-data-example-ajax" id="selectradicado" multiple>

                                                </select>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="modal-footer">
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-4">
                                        <button type="button" class="btn btn-lg blue-dark"
                                                id="" onclick="guardarAsociado()"
                                                style="border-radius:20px !important">

                                            <i class="fa fa-save"></i> Asociar Radicados Seleccionados
                                        </button>
                                    </div>

                                    <div class="col-md-5">
                                        <button type="button" class="btn btn-lg red" data-dismiss="modal"
                                                style="border-radius:20px !important">
                                            Cancelar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>


    <div id="modal_before_radicar" class="modal  fade " tabindex="99999999" aria-hidden="true">
        <div class="modal-dialog modal-lg" style="width:63% !important">
            <div class="modal-content ">
                <div class="modal-header bg-blue-chambray bg-font-yellow text-center">
                    <button type="button" data-dismiss="modal" title="Cerrar" aria-hidden="true"
                            class="btn btn-lg "
                            style="float: right;background-color: #2C3E50!important; border-radius:20px !important">
                        <i class="fa fa-remove"></i></button>
                    <h4 class="modal-title" id="titlemodalbeforerad"><strong>Antes de Radicar</strong></h4>
                </div>

                <div class="modal-body" id="radicated_info">

                    <div class="row">
                        <div class="col-md-12 ">

                            <label class=" col-md-2">Folios Comunicaci&oacute;n:</label>
                            <div class="col-md-1">

                                <input type="number" name="folios_comunicacion" min="0" value="" autocomplete="off"
                                       id="folios_comunicacion" class="form-control" style="" placeholder="">
                            </div>

                            <label class=" col-md-1">Folios Anexos:</label>
                            <div class="col-md-2">

                                <input type="number"
                                       name="folios_anexos" min="0" value="" autocomplete="off"
                                       id="folios_anexos" class="form-control" style="" placeholder="">
                            </div>


                            <label class=" col-md-2">Descripci&oacute;n de anexos:</label>
                            <div class="col-md-3">
                                <input required="required" type="text" value=""
                                       id="desc_anexos"
                                       class="form-control"
                                       placeholder="Anexos como planos, libros, CD...">
                            </div>


                        </div>
                    </div>
                    <br>
                    <div class="row" id="search-filter-container" style="text-align: center">
                        <div class="col-md-12">
                            <div class="search-bar bordered">
                                <form action="#" class="form-horizontal">
                                    <div class="form-body">
                                        <div class="form-group" id="divprevioswiche">

                                            <input type="checkbox" id="swich_seguridad" class="make-switch "
                                                   data-on-text="P&uacute;blico" data-off-text="Restringir"
                                                   checked data-on-color="primary" data-off-color="danger"
                                            >

                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="form-actions">
                        <div class="row" style="text-align:center">
                            <div class="col-md-6">
                                <button type="button" class="btn btn-lg" id="btn_generarradicado"
                                        style="border-radius:20px !important; margin-right:10px;
                                                background-color: orange;float:right" onclick="generarRadicado(event)">
                                    <i class="fa fa-barcode"></i>
                                    Radicar
                                </button>

                            </div>

                            <div class="col-md-3">
                                <button type="button" class="btn btn-lg red" data-dismiss="modal"
                                        style="border-radius:20px !important">
                                    Cancelar
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <div id="modal_justificar_delete_borrador" class="modal fade" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-header bg-blue-chambray bg-font-yellow text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">
                        <strong id="strong_justificar_delete_borrador">
                            Escriba la justificación para eliminar el borrador</strong></h4>
                </div>
                <div class="modal-body" id="radicated_info">
                    <form action="" method="POST" class="form-horizonal" id="">
                        <div class="form-body modal-body todo-task-modal-body">
                            <div class="form-actions">
                                <div class="row"></div>
                            </div>
                            <div class="row" style="margin-top:40px">

                                <div class="col-md-12 ">
                                    <div class="form-group has-error">
                                        <label class=" col-md-12"
                                               id="label_justificar_delete_borrador">
                                            Escriba la justificación para eliminar el borrador, y si el trámite ya se
                                            realizó con un radicado, relacionarlo aquí.</label>
                                        <div class="col-md-12">
                                            <div class="input-icon right">
                                                <textarea required="required" name="textareadelborrador"
                                                          id="textareadelborrador" class="form-control" maxlength="999"
                                                          style="border: solid grey 1px; height:90px"
                                                          placeholder=""></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-lg green"
                            id="" onclick="borrarBorrador(event)"
                            style="border-radius:20px !important">
                        <i class="fa fa-check-square-o"></i>
                        Continuar
                    </button>
                    <button type="button" class="btn btn-lg" data-dismiss="modal"
                            style="border-radius:20px !important">
                        Cancelar
                    </button>
                </div>
            </div>
        </div>
    </div>


    <div id="modal_vista_previa" class="modal fade" tabindex="-1" aria-hidden="true" style="z-index: 9999999">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-blue-chambray bg-font-yellow text-center" id="header_vista_previa">

                    <button type="button" title="Cerrar" aria-hidden="true" class="btn btn-lg " data-dismiss="modal"
                            style="float: right;background-color: #2C3E50!important; border-radius:20px !important"
                    >
                        <i class="fa fa-remove"></i></button>
                    <h4 class="modal-title"><strong>Vista Previa</strong></h4>
                </div>
                <div class="modal-body" id="modal_body_vista_previa">

                    <embed src="" id="embed"
                           frameborder="0" style="width:100%; height:100%">
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-lg" data-dismiss="modal"
                            style="border-radius:20px !important">
                        Cerrar
                    </button>
                </div>
            </div>
        </div>
    </div>




    <div id="modal_editar_titulos" class="modal fade" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-blue-chambray bg-font-yellow text-center">

                    <button type="button" title="Cerrar" aria-hidden="true" class="btn btn-lg " data-dismiss="modal"
                            style="float: right;background-color: #2C3E50!important; border-radius:20px !important"
                    >
                        <i class="fa fa-remove"></i></button>
                    <h4 class="modal-title"><strong id="strong_modal_edit_titulo">Editar T&iacute;tulo</strong></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <form>
                            <div class="form-body">
                                <div class="form-group">
                                    <div class=" col-md-2">Nombre del T&iacute;tulo</div>
                                    <div class=" col-md-4">
                                        <input type="text" class="form-control" id="inputedit_titulo">
                                    </div>

                                    <div class=" col-md-2">Abreviatura</div>
                                    <div class=" col-md-4">
                                        <input type="text" class="form-control" id="input_abreviatura">
                                    </div>

                                    <div class=" col-md-2">Activo</div>
                                    <div class=" col-md-4" style="display : inline-flex;">
                                        SI&nbsp;
                                        <input type="radio" name="radio_titulo_activo" value="1"
                                               id="radio_titulo_activo_si"
                                               checked>
                                        &nbsp; &nbsp; &nbsp;
                                        NO&nbsp;
                                        <input type="radio" name="radio_titulo_activo" value="0"
                                               id="radio_titulo_activo_no">
                                    </div>
                                    <div class="form-group">
                                        <div class=" col-md-2">Orden</div>
                                        <div class=" col-md-4">
                                            <input type="number" min="1" class="form-control" id="input_titulo_orden">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-lg blue"
                            style="border-radius:20px !important" onclick="guardarTitulo()">
                        Guardar
                    </button>
                    <button type="button" class="btn btn-lg" data-dismiss="modal"
                            style="border-radius:20px !important">
                        Cerrar
                    </button>
                </div>
            </div>
        </div>
    </div>


    <div id="modal_agregar_titulo_memo" class="modal fade" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-blue-chambray bg-font-yellow text-center">

                    <button type="button" title="Cerrar" aria-hidden="true" class="btn btn-lg " data-dismiss="modal"
                            style="float: right;background-color: #2C3E50!important; border-radius:20px !important"
                    >
                        <i class="fa fa-remove"></i></button>
                    <h4 class="modal-title"><strong>Agregar Titulo Oficios Internos</strong></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <form>
                            <div class="form-body">
                                <div class="form-group">
                                    <div class=" col-md-2">Nombre del T&iacute;tulo</div>
                                    <div class=" col-md-4">
                                        <input type="text" class="form-control" id="add_name_titulo_memo">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-lg blue"
                            style="border-radius:20px !important" onclick="guardarTituloMemo()">
                        Guardar
                    </button>
                    <button type="button" class="btn btn-lg" data-dismiss="modal"
                            style="border-radius:20px !important">
                        Cerrar
                    </button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('page-css')
    <style>
        .tab-pane {
            min-height: 420px;
        }

        .tabbable-line > .nav-tabs > li.active {
            border-bottom: 4px solid #FF9900 !important;
        }

        .tabbable-line > .nav-tabs > li:hover {
            border-bottom: 4px solid rgba(255, 154, 0, 0.49) !important;
        }

         .modal-content {

            width: 1000px;
            margin: 0 auto 0 auto;
            margin-left: -150px;
        }

        #modal_vista_previa > .modal-dialog > .modal-content {
            width: 70em !important;
            margin: 0 auto 0 auto !important;
            margin-left: -2em !important;
        }

        .assoc {
            font-size: 14px;
            font-weight: 600;
            color: #9eacb4;
            margin-top: 25px;
        }

        .as {
            border: 1px solid #e0e6e9;
            background-color: #fff;
            color: #c1cbd1;
            text-decoration: none;
            padding: 0 .4em;
            font-size: 20px;
            margin: -.3em 0 0 .5em;
            float: none;
        }

        .divBusquedaAvanzada {
            background: #eee;
            padding: 3px 30px;
            max-height: 400px;
            overflow-y: scroll;
        }

        .divBusquedaAvanzada p {
            cursor: pointer;
        }

        .resultEnviar {
            background: #eee;
            padding: 3px 30px;
            max-height: 400px;
            overflow-y: scroll;
            margin-bottom: 20px;
        }

        .resultEnviar p {
            cursor: pointer;
        }

        .as:hover {
            background-color: #4db3a4;
            color: #fff;
            text-decoration: none
        }

        .as:focus {
            color: #c1cbd1;
            text-decoration: none
        }

        .fr-view {
            font-size: 12pt;
        }

        hr, p {
            margin: 7px !important;
        }

        .select2-results__option[aria-selected=true] {
            display: none !important;
        }

        .select2-results__option--selected {
            display: none !important;
        }

        .modal-open .tt-menu {
            z-index: 30055 !important;
        }

    </style>
    <link href="{{$include_path}}global/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="{{$include_path}}global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"
          rel="stylesheet" type="text/css"/>

    <link href="{{$include_path}}global/plugins/froala_editor_3.1.1/css/froala_editor.css" rel="stylesheet"
          type="text/css"/>
    <link href="{{$include_path}}global/plugins/froala_editor_3.1.1/css/froala_editor.pkgd.css" rel="stylesheet"
          type="text/css"/>
    <link href="{{$include_path}}global/plugins/froala_editor_3.1.1/css/froala_style.css" rel="stylesheet"
          type="text/css"/>

    <link href="{{$include_path}}global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet"
          type="text/css"/>
@endsection

@section('page-js')

    <script>
        App.blockUI({
            boxed: !0,
            message: 'Cargando Borradores...'
        })
    </script>
    <script src="{{$include_path}}global/plugins/bootstrap-table/bootstrap-table.min.js"
            type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/bootstrap-table/locale/bootstrap-table-es-SP.js"
            type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"
            type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.es.js"
            type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/moment.min.js" type="text/javascript"></script>

    <!--  datatable -->
    <script src="{{$include_path}}global/plugins/datatable/datatable.js" type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/datatable/datatables.bootstrap.js" type="text/javascript"></script>
    <!--  datatable -->

    <script src="{{$include_path}}global/plugins/typeahead/handlebars.min.js" type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/typeahead/typeahead.bundle.min.js" type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js"
            type="text/javascript"></script>


    <!-- The Templates plugin is included to render the upload/download listings -->
    <script src="{{$include_path}}global/plugins/blueimp/js/tmpl.min.js"></script>
    <script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.ui.widget.js"></script>
    <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
    <script src="{{$include_path}}global/plugins/blueimp/js/load-image.all.min.js"></script>
    <!-- The Canvas to Blob plugin is included for image resizing functionality -->
    <script src="{{$include_path}}global/plugins/blueimp/js/canvas-to-blob.min.js"></script>

    <!-- PROBANDO SIN ESTO Bootstrap JS is not required, but included for the responsive demo navigation
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>-->

    <!-- blueimp Gallery script -->
    <script src="{{$include_path}}global/plugins/blueimp/js/jquery.blueimp-gallery.min.js"></script>

    <script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.iframe-transport.js"
            type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.fileupload.js" type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.fileupload-ui.js"
            type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.fileupload-process.js"
            type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.fileupload-image.js"
            type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.fileupload-audio.js"
            type="text/javascript"></script>

    <script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.fileupload-video.js"
            type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.fileupload-validate.js"
            type="text/javascript"></script>

    <script src="{{$include_path}}global/plugins/jquery-upload/js/form-fileupload.min.js"
            type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/froala/js/froala_editor.min.js" type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/froala/js/froala_editor.pkgd.js" type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/froala/js/languages/es.js" type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/froala/js/plugins/font_size.min.js" type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/froala/js/plugins/line_height.min.js" type="text/javascript"></script>



    <script src="{{$include_path}}global/plugins/x-editable/bootstrap-editable.min.js"></script>
    <script src="{{$include_path}}global/plugins/typeahead/typeaheadjs.js"></script>

    <script src="{{$include_path}}global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
            type="text/javascript"></script>

    <script src="{{$include_path}}global/plugins/icheck/icheck.min.js"
            type="text/javascript"></script>


    <script src="{{$include_path}}apps/scripts/funciones_globales.js?v=<?= date('Ymdis') ?>"
            type="text/javascript"></script>

    <script src="{{$include_path}}apps/scripts/services/BorradorService.js?v=<?= date('Ymdis') ?>"
            type="text/javascript"></script>

    <script src="{{$include_path}}global/plugins/jquery.pulsate.min.js" type="text/javascript"></script>

    <script id="template-upload" type="text/x-tmpl"> {% for (var i=0, file; file=o.files[i]; i++) { %}
                            <tr class="template-upload fade">
                                <td>
                                    <span class="preview"></span>
                                </td>
                                <td>
                                    <p class="name">{%=file.name%}</p>
                                    <strong class="error label label-danger"></strong>
                                </td>
                                <td>
                                    <p class="size">Processing...</p>
                                    <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                                        <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                                    </div>
                                </td>
                                <td> {% if (!i && !o.options.autoUpload) { %}
                                    <button class="btn blue start" disabled>
                                        <i class="fa fa-upload"></i>
                                        <span>Subir</span>
                                    </button> {% } %} {% if (!i) { %}
                                    <button class="btn btn-warning cancel">
                                        <i class="fa fa-ban"></i>
                                        <span>Cancelar</span>
                                    </button> {% } %} </td>
                            </tr> {% } %}






    </script>
    <script>

        <?php   $fechah = date("dmy_h_m_s") . " " . time("h_m_s");
        $session_id = session_id();
        $phpsession = session_name() . "=" . trim(session_id());
        $ent = $_SESSION['entidad'];
        $dependencia = $_SESSION['dependencia'];
        $krd = $_SESSION["krd"];
        $encabezado = "$phpsession&krd=$krd&fechah=$fechah&primera=1&ent=$ent&depende=$dependencia";
        //$encabezado2="$phpsession&krd=$krd&fechah=$fechah&primera=1&ent=$ent&depende=$dependencia";
        $anioactual = date('Y');

        $tipos_de_radicados = is_array($_SESSION["tpNumRad"]) && count($_SESSION["tpNumRad"]) > 0 ? $_SESSION["tpNumRad"] : array();
        $permisos_radicar = is_array($_SESSION["tpPerRad"]) && count($_SESSION["tpPerRad"]) > 0 ? $_SESSION["tpPerRad"] : array();
        $nombre_tipos_radicado = is_array($_SESSION["tpDescRad"]) && count($_SESSION["tpDescRad"]) > 0 ? $_SESSION["tpDescRad"] : array();
        $DiaHoraMin = date("d H:i");
        ?>

        /***************************************************************
         * Variables que vienen de session
         **************************************************************/
        var permisoEnBodega = '<?php echo $permisoEnBodega ?>';
        var rutaimg = '<?php echo $rutaimagenes ?>';
        var usua_nomb = '<?php  echo $_SESSION["usua_nomb"] ?>';
        var depe_nomb = '<?php  echo $_SESSION["depe_nomb"] ?>';
        var anioactual = '<?php  echo $anioactual ?>';
        var phpsession = '<?php  echo $phpsession ?>'
        var dependencia_user_log = '<?php echo $dependencia ?>'
        var objetoeditor = new Array();
        var session_id = '<?php echo session_id()  ?>';
        var krd = '<?php echo $_SESSION["krd"] ?>';
        var codusuario = '<?php echo $_SESSION["usuario_id"] ?>'
        var usua_perm_aprobar = '<?php echo $_SESSION["usua_perm_aprobar"] ?>';
        var borradorremove = '<?php echo $_SESSION['borradorremove'] ?>' //para poder borrar borradores
        var CantBorradaMiCargo = '<?php echo $_SESSION["borradores"] ?>'; //cantidad de borradores que tiene acatualmente este usaurio
        var DiaHoraMin = '<?php echo $DiaHoraMin ?>';



        //constantes ara el boton azul de crear nuevo borrador
        var SALIDA_OFICIO = '<?php echo $_SESSION['SALIDA_OFICIO']?>'
        var INTERNA_COMUNICACION = '<?php echo $_SESSION['INTERNA_COMUNICACION']?>'
        var INTERNA_CON_PLANTILLA = '<?php echo $_SESSION['INTERNA_CON_PLANTILLA']?>'
        var INTERNA_SIN_PLANTILLA = '<?php echo $_SESSION['INTERNA_SIN_PLANTILLA']?>'
        var REPORTE4 = '<?php echo $_SESSION['REPORTE4']?>'
        var correosoporte = '<?= $_SESSION['soporte_funcional'] ?>'
        //permiso para crear expedientes
        var usuaPermExpediente = '<?= $_SESSION["usuaPermExpediente"]?>';
        var usua_doc = '<?= $_SESSION["usua_doc"]?>';
        var mostrarRadbutton = 'NO';
        var radicaInExp = false;






        /**************************************************************************************
         * *
         * Variables que tienen que ver con el reporte 4
         * *
         * **************************************************************************************/

        /**
         * usuarios para el select de destinatarios en el modal de borradores
         */
        var allUsersDestinatarios = new Array();

        /**
         * reporte4_modal almacena el texto que se va a mostrar en el modal, cuando sea este tipo
         */
        var reporte4_modal = '<?= $_SESSION["REPORTE4_MODAL"]?>';

        /**
         * reporte4_asunto almacena el texto que se va a mostrar en el modal, cuando sea este tipo
         */
        var reporte4_asunto = '<?= $_SESSION["REPORTE4_ASUNTO"]?>';

        /**
         * reporte4_tipo almacena el tipo de radicado con el que se va a guardar el borrador(borradores.tipo_radicad)
         */
        var reporte4_tipo = '<?= $_SESSION["REPORTE4_TIPO"]?>';

        /**
         * REPORTE4_EXTENSIONES: extensiones que permitirá el borrador tipo REPORTE4
         */
        var REPORTE4_EXTENSIONES = '<?= $_SESSION["REPORTE4_EXTENSIONES"]?>';
        REPORTE4_EXTENSIONES = JSON.parse(REPORTE4_EXTENSIONES)

        /**
         * FORMATO_REPORTE4: Url donde estará un formato(plantilla) para REPORTE4
         */
        var FORMATO_REPORTE4 = '<?= $_SESSION["FORMATO_REPORTE4"]?>';


        /***************************************************************************************
         * Finaliza Variables que tienen que ver con el reporte 4
         * ***************************************************************************************/

        /***************************************************************
         * FINAL DE VARIABLES QUE VIENEN DE SESSION
         **************************************************************/

        var comentario_aprobar = ''; //texto al momento de aprobar por el modal de escribir comentario
        var borrador_id = "";
        var borradorActual = new Array();
        var radicadoactual = '<?php echo $radicadonumero ?>'; //radicado que viene distinto de vacio si se esta llamando esta vista desde el radicado
        var radicadoasunto = '<?php echo json_encode($radicadoasunto);?>'; //asunto de radicado que viene distinto de vacio si se esta llamando esta vista desde el radicado

        var infRadicadoPadre = new Array(); //informacion del radicado padre si es que vengo desde radicado
        var remitenteRadicado = new Array(); //almacena el remitente del radicado, si es que vengo dese radicado/documentos

        var radicado_has_exped = '<?php echo $radicado_has_exped ?>'; //debe venir en 0 por defecto, pero
        // si estoy en radicar desde entrada, almacena cuantos expedientes tiene, para hacer la validacion al intentar
        //crear el borrador

        var CantBorradPuedoVer = ''; //total del contador del arreglo de borradores
        var nombre_usu_anterior = ""; //nombre del usuario anterior del borrador
        var depe_nomb_usu_anterior = ""; //nombre de la dependencia  del usuario anterior del borrador

        var BorradorRevisado = false; //Indicador de si el borrador actual esta revisado o no.
        var tbodytr = "";
        var phpVersion = '<?php echo phpversion() ?>';
        var misRadicadosAsociados = new Array();
        var editoroplantilla = ""; //para saber si estoy enviando editor o plantilla
        var maxFileSize = '<?php echo $maxFileSize ?>'; //el maximo ttamano permitido por archivo
        //var maxFileSizeStr = '<?php echo $maxFileSizeStr ?>'; //el maximo ttamano permitido por archivo
        var maxFileSizeStr = configsHasValue['TAMANO_ARCHIVO'];
        var documentRout = false; //var que señala si borrador tiene una ruta de plantilla almacenada
        var fileExistPlant = false; //var que señala si borrador tiene un archivo de plantilla almacenada
        var expedientesincluidos = new Array(); //almacena los expedientes incluidos
        var expedientesincluidosComa = ""; //almacena los expedientes incluidos, separados por coma
        var canceloEnvioAprob = false; //variable que define si se cancela o no el envio del borrador, al moment de aprobarse
        var optSelectedNewBorrador = ""; //opcion seleccionada al presionar sobre una de las opciones en nuevo borrador
        var firmantesselected = new Array(); //firmantes o firmante seleccionados
        var cargofirmante = ""; //almacena e valor del input cuando es un solo firmante
        var radPadre = ""; //radicado padre, se setea cuandos e recorren los radicads asociados

        var dropdown_enviar = new Array(); //almacena la lista que se muestra en el desplegable del dropdown en el botond e enviar.
        var usr_especific_select_enviar = "" //adicional a los usuarios que se muestran en el select de enviar,
        // busca uno especifico y lo agrega al select


        var HABILITARGUARDAR = true; //variable que dice si se puede ejecutar la accion otra vez de guardar

        var seguridad_radicado = "true"; //variable que al momento de radicar el borrador, indica si es publico el radicado o no
        //tdo esto mediante el swiche, por defecto esta en true, es decir, Publico

        var allUsersFirmantes = new Array(); //arreglo que almacena todos los usuarios a mostrar
        //en el selectde firmantes

        var timer = 9, // las dos siguientes variables es para al momento de enviar el borrador
            //cuando se aprueba
            isTimerStarted = false;

        //viene de app.blade
        var modulosMostrar = "BORRADORES";

        var swalAprobarDevolver = '';


        //almacena lo que se va escribiendo en el input de buscar expedientes
        var textKeyupInputExped = "";

        var continuarEscSwal = true; //para que cuando se presione escape con el swal alert,
        // no permita continuar ya que esc es confirm=false

        function validaExpiro(response) {
            if (response.error != undefined && response.error == "expiro") {
                window.location.href = "../../cerrar_session.php"
            }
        }


        var doc = "'docx'"
        var pdf = "'pdf'"
        var htmldiveditor = '';


        var consiguioaprobar = false; //para saber si al menos fue aprobado una vez, indepndientemente del usuario
        var almenosaprobado = false;//para saber si al menos fue aprobado una vez, indepndientemente del usuario
        var almenosrevisado = false;//para saber si al menos fue revisado una vez, indepndientemente del usuario
        var usuarioAproboBorrador = ""; //usuario que aprueba el borrador. por defecto esta vacio


        var permiso_para_radicar = new Array(); //variable que almacenará el valor que tenga en $_SESSION["tpNumRad"]
        var tiposDeRadicados = new Array(); // $_SESSION["tpPerRad"]
        var nombresTiposRadicados = new Array(); // $_SESSION["tpDescRad"]

        var anexosBorrador = new Array(); //indica los anexos que tiene el borrador

        /**
         * datos del borrador del radicado padre, si existe un radicado padre
         */
        var borradorRadPadre = new Array();


        /**
         * Todos los estados posibles del borrador
         */
        var borradorEstados = new Array();

        /**
         * Cada vez que se presiona sobre el boton de borrar(al lado de modificar plantilla), esto se setea en true,
         * para que en el request, se quite la ruta que habia.
         * @type {boolean}
         */
        var borrarDocumentoRuta = false;


        var tableAjaxIndexBorradores = new Array();
        var cargarIndexBorradores = new Array();
        
        
        /**
         * variables que se refieren a borradores customizados
         */
        
        var customBorradorSel = false;
        var datosCustomBorrador = {};
        var customBorradorSession = '<?php echo json_encode($_SESSION['TIPOS_BORRADOR_CUSTOM'],JSON_FORCE_OBJECT) ?>';

        var tablaAnexosTipos = null;
        var tablaAnexosTiposADefinir = null;
        var htmlTabla = null;
    </script>


    <script src="{{$include_path}}apps/scripts/nuevoExpediente.js?v=<?= date('Ymdis') ?>"
            type="text/javascript"></script>

    <script>
        var valTeclesr = false;
        //var tableTest = $('#tabla').DataTable();
        //setInterval( function () {
        //    tableTest.ajax.reload( null, false ); // user paging is not reset on reload
        //}, 2000 );

        function definirfroala() { //define el editor

            var textofroala = 'Escriba el documento desde el saludo, hasta el nombre de la persona que firma.';
            if (optSelectedNewBorrador == INTERNA_COMUNICACION) {
                textofroala = 'Escriba el documento a partir del saludo...';
            }

            $('#froalaeditor').froalaEditor({
                key: configsHasValue['EDITOR_KEY'],
                language: 'es',
                placeholderText: textofroala,
                //key:'Pg1bwwA-32=='
                enter: $.FroalaEditor.ENTER_BR,
                fontFamilyDefaultSelection: configsHasValue['EDITOR_FUENTE_TIPO'],

                fontSizeUnit: 'pt', //supuestamente para cambiar la unidad de  px o pt
                fontSizeSelection: true, //para mostrar un select con los tamanos
                fontSizeDefaultSelection: configsHasValue['EDITOR_FUENTE_TAMANIO'], //la opcion que va a mostrar por defecto */
                fontSize: ['8', '9', '10', '11', '12', '14', '16', '18', '24', '30', '36', '48', '60', '72', '96'],
                imageOutputSize: true,
                lineHeights: {
                    Default: '1',
                    '1': '1',
                    '1.15': '1.15',
                    '1.5': '1.5',
                    Double: '2'
                },
                toolbarButtons: ['fullscreen', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|', 'fontFamily', 'fontSize', 'color', 'inlineClass', 'inlineStyle',
                    'paragraphStyle', 'lineHeight', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', '-', 'insertLink', 'insertImage', 'insertVideo', 'embedly', 'insertFile', 'insertTable', '|', 'emoticons', 'fontAwesome', 'specialCharacters', 'insertHR', 'selectAll', 'clearFormatting', '|', 'print', 'getPDF', 'spellChecker', 'help', 'html', '|', 'undo', 'redo']
            })
            $('#froalaeditor').froalaEditor('align.apply', 'justify');

            setTimeout(function () {
                $('#froalaeditor').froalaEditor('html.set', '');
            }, 0)
        }


        function definirtinymce() { //define el editor

            if (tinymce.editors.length < 1) {

                tinymce.init({
                    selector: '#content',
                    plugins: [
                        "advlist autolink lists link image charmap print preview anchor",
                        "searchreplace visualblocks code fullscreen fullpage",
                        "insertdatetime media table contextmenu paste"
                    ],
                    toolbar: "insertfile undo redo | styleselect | bold italic | " +
                        "alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | " +
                        "link image",

                });

                var objetoeditor = $('#tinymce', parent.frames["leftFrame"].document);
                //console.log('objeto',objetoeditor)
            }
        }

        function accents_supr(data) {
            return !data ?
                '' :
                typeof data === 'string' ?
                    data
                        .replace(/\n/g, ' ')
                        .replace(/á/g, 'a')
                        .replace(/é/g, 'e')
                        .replace(/í/g, 'i')
                        .replace(/ó/g, 'o')
                        .replace(/ú/g, 'u')
                        .replace(/ê/g, 'e')
                        .replace(/î/g, 'i')
                        .replace(/ô/g, 'o')
                        .replace(/è/g, 'e')
                        .replace(/ï/g, 'i')
                        .replace(/ü/g, 'u')
                        .replace(/ç/g, 'c') :
                    data;
        };

        function menuMisBorradores() {

            if (parent.frames["leftFrame"] != undefined) {
                var obj = $('.a_menu_borrador', parent.frames["leftFrame"].document);
                var html = '';
                var title = CantBorradaMiCargo + " borradores a mi cargo";

                if (CantBorradPuedoVer == CantBorradaMiCargo) {
                    html += 'Borradores(' + CantBorradPuedoVer + ')';
                } else {
                    html += 'Borradores(' + CantBorradaMiCargo + '/' + CantBorradPuedoVer + ')';
                    title += ' de un total de ' + CantBorradPuedoVer + '';
                }
                obj.html(html + '<i class="fa fa-pencil-square-o"' +
                    ' aria-hidden="true"></i> ' +
                    '');

                obj.attr('title', title)

            }

        }

        $(document).on('keydown', function (event) {

            var key = window.event ? event.keyCode : event.which;
            continuarEscSwal = true;
            if (key == 27) {
                continuarEscSwal = false;
            }
        });

        $(document).ready(function () {

            $('#valorTam').html( ((maxFileSize/1024)/1024) + 'MB');



            setInterval(function () {
                valTeclesr = false;
            }, 10000);
            $('#asunto').on('keydown', function () {
                valTeclesr = true;
            });

            //lo siguiente es el swiche de seguridad que aparece en el modal
            //de antes de radicar, el popover es dinamico para la opcion que esta deshabilitada
            $('#swich_seguridad').on('switchChange.bootstrapSwitch', function (event, state) {

                seguridad_radicado = $('#swich_seguridad').bootstrapSwitch('state');
                if (seguridad_radicado == false) {
                    $(".bootstrap-switch-handle-off").html('Reservado')
                    $(".bootstrap-switch-label").html('Público')
                    $(".bootstrap-switch-label").attr('data-original-title', 'Quitar restricción')
                    $(".bootstrap-switch-label").attr('data-content', 'Todos los usuarios tendrán acceso al documento')

                } else {
                    $(".bootstrap-switch-label").html('Restringir')
                    $(".bootstrap-switch-label").attr('data-original-title', 'Restringir acceso al documento')
                    $(".bootstrap-switch-label").attr('data-content', 'Solo tendrá acceso al documento el usuario actual ' +
                        'y usuarios o dependencias autorizadas!')
                    $(".bootstrap-switch-handle-off").html('Restringir')

                }

                $('.bootstrap-switch-label').attr('data-placement', 'top')
                $('.bootstrap-switch-label').attr('data-trigger', 'hover')
                $('.bootstrap-switch-label').attr('data-container', 'body')
                $('.bootstrap-switch-label').popover();

            });
            // $('.bootstrap-switch-label') es la opcion que aparece en gris como deshabilitada
            $(".bootstrap-switch-label").html('Restringir')
            $(".bootstrap-switch-label").css('opacity', '0.5')




            var patt = new RegExp("(.|/)("+anexosTipos.join('|')+")$");
            console.log(patt);
            $('#formanexo').fileupload({
                url: 'guardaranexo.php',
                 acceptFileTypes:  patt,
                maxFileSize: parseInt(maxFileSize) + parseInt(0),
                messages: {
                    maxNumberOfFiles: 'Número máximo de archivos excedidos',
                    acceptFileTypes: 'Tipo de archivo no permitido',
                    maxFileSize: 'Tamaño de archivo No permitido',
                    minFileSize: 'El archivo es demasiado pequeño',
                    uploadedBytes: 'Los bytes cargados exceden el tamaño del archivo'
                }
            }).bind('fileuploaddestroy', function (e, data) {
                //cuando presiono el boton borrar
                //obtengo el id del anexo
                var fileid = data.context.find('a[download]').attr('data-id');
                //lo nando a borrar de la BD
                removeanexo(fileid)
                //remuevo el tr donde se muestra el anexo
                $("#trbodyanexos" + fileid).remove();

            });

            // Enable iframe cross-domain access via redirect option:
            $('#formanexo').fileupload(
                'option',
                'redirect',
                window.location.href.replace(
                    /\/[^\/]*$/,
                    '/cors/result.html?%s'
                )
            );

            $.ajax({
                url: "generalborrador.php",
                method: "POST",
                data: {function_call: "cantidadBorradoresUsu"},
                dataType: "json",
                success: function (response) {
                    CantBorradaMiCargo = response.amicargo;
                    //esto es para cargar los borradores que tiene el usuarioa actual
                    //busco dentro del otro frame que es donde esta el menu lateral. al elemento a que tiene la clase a_menu_borrador
                    menuMisBorradores()
                }
            }).done(function () {
            }).fail(function (error) {
                Swal.fire("Error", "Ha ocurrido un error", "error");
                return false;
            });

            //le mando esta variable para saber desde donde lo estoy llamando
            var data = {}
            data.radicadoactual = radicadoactual

            var url = "data_borradores.php"
            var table = $('#tabla').DataTable();
            table.destroy();
            var tableoptions = {

                "order": [[0, "desc"]],
                "destroy": true,
                "processing": true,
                "serverSide": true,
                "ajax": {
                    "url": url,
                    "data": function (d) {
                        d.data = data
                    },
                },
                rowId: 'DT_RowId',
                retrieve: false,
                "aLengthMenu": [[10, 30, 50, -1], [10, 30, 50, "Todos"]],
                "scrollX": true,
                "scrollCollapse": true,

                /********para poder mover con las flechas*********/
                keys: true,
                "searching": true,
                "ordering": true,
                "bPaginate": true,
                fixedHeader: {
                    header: true,
                    footer: true
                },
                paging: true,
                info: true,
                "sScrollX": "100%",
                autoWidth: true,
                'createdRow': function (row, data, dataIndex) {

                    $(row).attr('tabindex', dataIndex);
                    $(row).attr('data-id_borrador', data['id_borrador']);

                },
                "language": {
                    "emptyTable": "No se encontraron registros",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ resultados",
                    "infoEmpty": "Mostrando 0 a 0 de 0 resultados",
                    "infoFiltered": "(filtrado de _MAX_ total resultados)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ resultados",
                    "loadingRecords": "Cargando...",
                    "processing": "Buscando...",
                    "search": "Búsqueda:",
                    "zeroRecords": "No se encontraron resultados",
                    "paginate": {
                        "first": "<<",
                        "last": ">>",
                        "next": "<",
                        "previous": ">"
                    },
                    "aria": {
                        "sortAscending": ": activar ordenar columnas ascendente",
                        "sortDescending": ": activar ordenar columnas descendente"
                    }
                },
                /************************************/
                "fnInitComplete": function (settings, json) {

                    setTimeout(function () {
                        $('div.dataTables_filter input').focus();
                    }, 5);
                },

                "fnDrawCallback": function (datos) {

                    CantBorradPuedoVer = datos.json.recordsTotal;
                    menuMisBorradores();
                    //esto funciona cuando se hace el paginate, ya que con fnInitComplete solo lo hace una vez
                    /*setTimeout(function () {
                        $('div.dataTables_filter input').focus();
                    }, 5);*/

                    //cada vez que se cierra el modal de borradores, o se pagina, pasa por aqui,
                    //por lo tanto le pongo el focus a el ultimo borrador seleccionado
                    $("#" + borrador_id).focus();
                    $("#" + borrador_id).select();
                },

                "footerCallback": function (row, data, start, end, display) {

                }
            };

            tableAjaxIndexBorradores = $('#tabla').DataTable(tableoptions);

            /* lo siguiente es para cuando presione enter en el input de texto, haga el ajax,
            //actualmente no funciona, porque ya se hace el ajasx automatico
            $('#tabla_filter input[type=search]').keyup(function () {
                var table = $('#tabla').DataTable();
                table.search(
                    jQuery.fn.DataTable.ext.type.search.string(this.value)
                ).draw();
            });*/

            $(".dataTables_paginate").css('color', '#666 !important')

            //cuando se cierra el mo dal de borrador, se vuelve a llamar al index.php de borradores,
            //por lo tanto refresca toda la vista
            jQuery('#all_modal').on('hidden.bs.modal', function (e) {

                tableAjaxIndexBorradores.ajax.reload(null, false);

            });

            setInterval( function () {
                tableAjaxIndexBorradores.ajax.reload( null, false ); // user paging is not reset on reload
            }, 300000 );


            jQuery('#modalanadiranexo').on('hidden.bs.modal', function (e) {
                //cuando se cierra el modal de anexos
                getAnexos();
            });


            $('#chats').slimscroll({
                height: '300px'
            })
            $('#buscarcomentarios').on('keyup keypress', function (e) {
                var keyCode = e.keyCode || e.which;
                if (keyCode === 13) {
                    e.preventDefault();
                    return false;
                }
            });


            $('#buscarcomentarios').on("keyup input", function () {
                var inputVal = $(this).val();

                $.ajax({
                    url: "generalborrador.php",
                    method: "POST",
                    data: {borrador_id: borrador_id, buscar: inputVal, function_call: "solocomentarios"},
                    dataType: "json",
                    success: function (response) {

                        if (response.comentarios) {
                            mostrarcomentarios(response.comentarios);
                        }
                    }
                }).done(function () {
                }).fail(function (error) {
                    Swal.fire("Error", "Ha ocurrido un error", "error");
                    return false;
                });

            });


            // Set search input value on click of result item
            $(document).on("click", ".resultEnviar p", function () {
                $('#enviara').val($(this).text());
                $(this).parent(".resultEnviar").empty();
            });

            $('#persona, #cargo, #asunto, #edit').on('click', function () {
                document.getElementById('primerguardar').innerHTML = '<i class="fa fa-save"></i> Guardar';
                document.getElementById('segundoguardar').innerHTML = '<i class="fa fa-save"></i> Guardar';
                $("#primerguardar").val('Guardar');
                $("#segundoguardar").val('Guardar');
            });

            $('#tipopersona').on('change', function () {
                document.getElementById('primerguardar').innerHTML = '<i class="fa fa-save"></i> Guardar';
                document.getElementById('segundoguardar').innerHTML = '<i class="fa fa-save"></i> Guardar';
                $("#primerguardar").val('Guardar');
                $("#segundoguardar").val('Guardar');
            });

            lleganDatos(<?php  echo json_encode($usarioremitente) ?>,<?php  echo json_encode($configuraciones) ?>,
                <?php  echo json_encode($titulos) ?>,<?php  echo json_encode($jefes_dropdown_enviar) ?>,
                <?php  echo json_encode($radicadocompleto) ?>,<?php  echo json_encode($permisos_radicar) ?>,
                <?php  echo json_encode($borradorRadPadre) ?>, <?php  echo json_encode($borradorEstados) ?>,
                <?php  echo json_encode($tipos_de_radicados) ?>, <?php echo json_encode($nombre_tipos_radicado) ?>)


            setTimeout(function () {
                App.unblockUI()
            }, 1000)

            /**
             * llamo la funcion que dice que despues de un tiempo especifico, manda a refrescar
             * el index de borradores
             */
            refreshTimeoutIndexBorradores();

        });

        /**
         * Funcion que manda a resfrescar la tabla de borradores
         * cuando se deja de presionar una tecla y cuando se detiene el mouse, en tantos minutos o segundos
         * depende la configuracion (actualmente en 5 minutos)
         */
        function refreshTimeoutIndexBorradores(){
            //setup before functions
            var doneTypingInterval = 300;  //time in ms, 5 second for example

            //on keyup, start the countdown
            $('body').on('keyup', function () {
                clearTimeout(cargarIndexBorradores);
                cargarIndexBorradores = setInterval(fnCargarIndexBorradores, doneTypingInterval * 1000);
            });

            //on keydown, clear the countdown
            $('body').on('keydown', function () {
                clearTimeout(cargarIndexBorradores);
            });

            //on keydown, clear the countdown
            $('body').on('mousemove', function () {
                clearTimeout(cargarIndexBorradores);
                cargarIndexBorradores = setInterval(fnCargarIndexBorradores, doneTypingInterval * 1000);
            });
        }

        function fnCargarIndexBorradores(){
            tableAjaxIndexBorradores.ajax.reload(null, false)
        }

        function llamarModuloBorradores() {
            var url = "index.php";
            if (radicadoactual != "") {
                url = "index.php?radicado=" + radicadoactual;
            }
            window.clearInterval(fn60sec);
            window.location.href = url;
        }

        /*funcion que almacena variables que vienene desde el servidor, generalmente arreglos*/
        function lleganDatos(usuarioremitente, configs, titulos, jefes_dropdown_enviar, inforadicadopadre,
                             permisoParaRadicar, borrador_rad_padre, borradorestados, tipos_de_radicados,
                             nombres_tipo_radicados) {
            remitenteRadicado = usuarioremitente;
            configuraciones = configs;
            titulos_tratamiento = titulos;
            dropdown_enviar = jefes_dropdown_enviar;
            infRadicadoPadre = inforadicadopadre;
            permiso_para_radicar = permisoParaRadicar;
            borradorRadPadre = borrador_rad_padre;
            borradorEstados = borradorestados;
            tiposDeRadicados = tipos_de_radicados;
            nombresTiposRadicados = nombres_tipo_radicados
            //armarConfigsHasValues();
            /**
             * Aqui actualizo la url del appservice, al servicio de borradores
             */
            BorradorService.urlApi = configsHasValue['WS_APPSERVICE_URL'] + '/borrador'

        }


        function resetForm() {
            armarRowTbn();
            definirfroala();

            borradorActual = new Array();
            borrador_id = "";
            BorradorRevisado = false;
            usua_perm_aprobar = '<?php echo $_SESSION["usua_perm_aprobar"] ?>';

            borrarDocumentoRuta = false;
            anexosBorrador = new Array();

            $('#cargo_firmante').prop('readonly', false);
            $('#cargo').prop('readonly', false);
            $("#tipo_radicado").val('');
            $("#dest").removeAttr('readonly');
            $("#asunto").removeAttr('readonly');
            $('#folios_anexos_raiz').prop('readonly', false)
            $('#folios_anexos').prop('readonly', false)

            allUsersFirmantes = new Array();
            firmantesselected = new Array();
            dependenDestinatario = "";
            usuarioAproboBorrador = "";

            editoroplantilla = "EditorWeb";
            $("#editoroplantilla").val('EditorWeb');
            if (optSelectedNewBorrador == INTERNA_SIN_PLANTILLA) {
                editoroplantilla = "SinPlantilla";
                $("#editoroplantilla").val('SinPlantilla');
            }


            organizarMostrarDivEditor('link1');

            if (borradorActual.DOCUMENTO_FORMATO != undefined && borradorActual.DOCUMENTO_FORMATO != "") {
                $("#div_modificarborrar").css("display", "block");
            }


            setTimeout(function () {
                $("#radicadoactual").val(radicadoactual);
            }, 500)

            nombre_usu_anterior = "";
            depe_nomb_usu_anterior = "";
            // clearing inputs
            // $("input").val('');
            //$("textarea").html('');
            // $("textarea").val('');
            $("#all_modal input").val('');
            $("#all_modal textarea").val('');
            $("#all_modal textarea").html('');

            $("#folios_comunicacion_raiz").val('1');
            $("#folios_anexos_raiz").val('0');
            $("#desc_anexos_raiz").val('');
            $("#div_expediente").css("display", "none");
            $("#div_borrar").css("display", "none");
            $("#div_radicar").css("display", "none");
            $("#buttom_aprobar").css("display", "none");
            $('.depende_div_aprobar').css("display", "none");
            $("#div_no_aprobar").css("display", "none");
            $("#persona").val('');
            $("#cargo").val('');
            $("#div_enviar").css("display", "none");
            $(".depende_div_enviar").css("display", "none");
            $("#all_modal input").prop('disabled', false);
            $("#all_modal textarea").prop('disabled', false);
            $(".guardar").prop('disabled', false);
            $(".guardar").css("display", "block");
            $("#div_edit").css("display", "block");
            $("#botonaddanexo").css("display", "block");
            $("#botonaddradicado").css("display", "block");

            /*
             tinymce.get('content').setContent('');
             tinymce.activeEditor.getBody().setAttribute('contenteditable', true);*/
            $('#froalaeditor').froalaEditor('html.set', '');
            $('#froalaeditor').froalaEditor('edit.on');


            $("#titulomodal").html('');
            $("#titulomodal").html('NUEVO BORRADOR');
            $("#advertenciacabecera").html('');
            $("#div_advertenciacabecera").css('display', 'none');
            $("#div_margin_top_princ").css('margin-top', '30px');
            $("#append_radiaso").html('');
            $("#append_anexos").html('');
            $("#ulchat").html('');
            $("#buscarcomentarios").val('')

            expedientestabla = new Array();
            misRadicadosAsociados = new Array();

            var intervalo = 60;
            if (configsHasValue['GUARDAR_BORRADOR'] == "3") {
                intervalo = 180
            }
            if (configsHasValue['GUARDAR_BORRADOR'] == "5") {
                intervalo = 300
            }
            if (configsHasValue['GUARDAR_BORRADOR'] == "No guardar automaticamente") {
                intervalo = false
            }

            if (intervalo != false) {
                setInterval(fn60sec, intervalo * 1000);
            }


            $('#asunto').maxlength({
                alwaysShow: true,
                threshold: 9,
                appendToParent: true,
                warningClass: "label label-success",
                limitReachedClass: "label label-danger",
                preText: 'Mínimo ' + configsHasValue['ASUNTO_MINLENGTH'] + ' caracteres. ',
                validate: true
            });
            allUsersDestinatarios = new Array();
            $("#selectDest").val(null).trigger('change.select2');
            //oculto el editor web
            validarmostrarEditor();


        }

        function alertSaveBorradorAuto() {
            //@formatter:off
            Swal.fire({
                title: "Alerta",
                text: "No ha guardado el borrador. Desea guardar ahora?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "SI",
                cancelButtonText: "NO",
                closeOnConfirm: true,
                closeOnCancel: true
            }).then((result) => {
                if (result.value) {
                setTimeout(function () {
                    $("#primerguardar").click();
                }, 500)
            } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
            ) {
                $(".guardar").prop('disabled', false);
            }
        });
            //@formatter:on
        }

        function fn60sec() {

            $(".guardar").prop('disabled', true);

            if ($("#asunto").val() == "") {

                $(".guardar").prop('disabled', false);
                return false;
            }

            var asunto = $("#asunto").val();

            if ($('#asunto').is(':visible') && asunto.length < configsHasValue['ASUNTO_MINLENGTH']) {

                $(".guardar").prop('disabled', false);
                return false;
            }

            if ($('#asunto').is(':visible') && asunto.length > 999) {

                $(".guardar").prop('disabled', false);
                return false;
            }

            if (valTeclesr == true) {

                window.clearInterval(fn60sec);
                $(".guardar").prop('disabled', false);
                return false;
            }

            var editor = "";
            if ($('#div_edit').is(':visible')) //el div donde esta el editor froala
            {
                editor = $('#froalaeditor').froalaEditor('html.get');
            }


            if ($('#all_modal').is(':visible') && $("#dest").val() != "" && $("#asunto").val() != "") {

                if (borradorActual.ID_USUARIO_ACTUAL == undefined) {
                    //cuando no se ha creado el borrador

                    if (
                        (
                            asunto.length >= configsHasValue['ASUNTO_MINLENGTH']
                            &&
                            asunto.length <= 999
                        )
                        &&
                        (
                            $("#asunto").is(":focus")
                        )

                    ) {
                        /**
                         * si cumple una de las dos condiciones anteriores, entonces no arroja la alerta
                         */

                    } else {
                        alertSaveBorradorAuto();
                    }


                } else if (borradorActual.ID_USUARIO_ACTUAL == codusuario) {
//aquisolo entra cuando el borrador ya esta creado. y el usuario actual e sigual al nque esta logueado
                    var data = {
                        borrador_id: borrador_id,
                        dest: $('#dest').val(),
                        persona: $('#persona').val(),
                        cargo: $('#cargo').val(),
                        asunto: $('#asunto').val(),
                        edit: editor,
                        radicadoactual: $("#radicadoactual").val(),
                        id_destinatario: $("#id_destinatario").val(),
                        tipo_destinatario: $("#tipo_destinatario").val(),
                        tipo_radicado: $("#tipo_radicado").val(),
                        editoroplantilla: editoroplantilla,
                        tipopersona: $("#tipopersona").val(),
                        tipo_borrador_dest: $("#tipo_borrador_dest").val(),
                        folios_comunicacion_raiz: $("#folios_comunicacion_raiz").val(),
                        folios_anexos_raiz: $("#folios_anexos_raiz").val(),
                        desc_anexos_raiz: $("#desc_anexos_raiz").val(),
                        optSelectedNewBorrador: optSelectedNewBorrador,
                        function_call: "updatecada60"
                    }


                    if ($("#id_usuario_firmante").val()) {
                        data.id_usuario_firmante = $("#id_usuario_firmante").val();
                    }

                    if ($("#cargo_firmante").val()) {
                        data.cargo_firmante = $("#cargo_firmante").val();
                    }
                    $.ajax({
                        url: "generalborrador.php",
                        method: "POST",
                        data: data,
                        dataType: "json",
                        async: false,
                        success: function (response) {

                            validaExpiro(response);
                            if (response.success) {
                                $("#primerguardar").val('Guardado...');
                                $("#segundoguardar").val('Guardado...');
                                document.getElementById('primerguardar').innerHTML = 'Guardado...';
                                document.getElementById('segundoguardar').innerHTML = 'Guardado...';

                                setTimeout(function () {
                                    document.getElementById('primerguardar').innerHTML = '<i class="fa fa-save"></i> Guardar';
                                    document.getElementById('segundoguardar').innerHTML = '<i class="fa fa-save"></i> Guardar';
                                    $("#primerguardar").val('Guardar');
                                    $("#segundoguardar").val('Guardar');
                                }, 7000)

                                $("#div_expediente").css("display", "block");
                                if (response.success.id) {
                                    var borrador = response.borradoractual;
                                    borrador_id = response.success.id;
                                    borradorActual = borrador;
                                }
                                guardarEditor(borrador_id);

                            } else {

                                Swal.fire("Error", "Ha ocurrido un error", "error");
                                return false;
                            }
                            $(".guardar").prop('disabled', false);

                        }
                    }).done(function () {
                        $(".guardar").prop('disabled', false);
                    }).fail(function (error) {
                        $(".guardar").prop('disabled', false);
                        console.log(error);
                    });

                }
            } else {
                $(".guardar").prop('disabled', false);
            }
        }
    </script>
    <script>



        function convertImagesToBase64() {
            contentDocument = tinymce.get('content').getDoc();
            var regularImages = contentDocument.querySelectorAll("img");
            var canvas = document.createElement('canvas');
            var ctx = canvas.getContext('2d');
            var ctx = canvas.getContext('2d');
            [].forEach.call(regularImages, function (imgElement) {
                // preparing canvas for drawing
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                canvas.width = imgElement.width;
                canvas.height = imgElement.height;
                ctx.drawImage(imgElement, 0, 0);
                // by default toDataURL() produces png image, but you can also export to jpeg
                // checkout function's documentation for more details
                var dataURL = canvas.toDataURL();
                imgElement.setAttribute('src', dataURL);
            })
            canvas.remove();
        };

        //Helper function for calculation of progress
        function formatFileSize(bytes) {
            if (typeof bytes !== 'number') {
                return '';
            }

            if (bytes >= 1000000000) {
                return (bytes / 1000000000).toFixed(2) + ' GB';
            }

            if (bytes >= 1000000) {
                return (bytes / 1000000).toFixed(2) + ' MB';
            }
            return (bytes / 1000).toFixed(2) + ' KB';
        }


        function getFile(filePath) {
            var extension = "";
            $.ajax({
                url: "generalborrador.php",
                method: "POST",
                data: {
                    nombre: filePath,
                    function_call: "traeExtension"
                },
                async: false,
                dataType: "json",
                success: function (response) {
                    extension = response.extension

                }
            }).done(function () {
            }).fail(function (error) {
                console.log(error);
            });
            return extension
        }

        /**
         * Muestra el boton de seleccionar plantilla
         */
        function mostrarInputPlant() {


            $("#div_cargardoc").css("display", "block");
            $("#div_loader").css("display", "block");
            $("#div_edit").css("display", "none");


            $('#cargardoc').change(function (){
                var sizeByte = this.files[0].size;
                var siezekiloByte = parseInt(sizeByte / 1024);

                if(siezekiloByte > (configsHasValue['TAMANO_ARCHIVO'] * 1024)){
                    Swal.fire('Alerta','El tamaño supera el limite permitido',"warning");
                    $(this).val('');
                }
            });

            var elem = document.getElementById("loader");
            var width = 1;
            elem.style.width = width + '%';
            var extensionpermi = new Array();
            extensionpermi[0] = "docx"

            //si es interna sin plantilla, voy a cargar un pdf
            if (optSelectedNewBorrador == SALIDA_OFICIO) {
                $('#cargardoc').prop("accept", ".docx,application/vnd.openxmlformats-officedocument.wordprocessingml.document")
            }

            //si es interna sin plantilla, voy a cargar un pdf
            if (optSelectedNewBorrador == INTERNA_COMUNICACION) {
                $('#cargardoc').prop("accept", ".docx,application/vnd.openxmlformats-officedocument.wordprocessingml.document")
            }

            //si es interna sin plantilla, voy a cargar un pdf
            if (optSelectedNewBorrador == INTERNA_SIN_PLANTILLA) {

                $('#cargardoc').attr("accept", "application/pdf")
                extensionpermi = new Array();
                extensionpermi[0] = "pdf"
            }

            //si es con plantilla, permite estas extensiones
            if (optSelectedNewBorrador == INTERNA_CON_PLANTILLA) {
                $('#cargardoc').attr("accept", "application/vnd.oasis.opendocument.text,application/vnd.oasis.opendocument.spreadsheet,.docx,application/vnd.openxmlformats-officedocument.wordprocessingml.document")
                extensionpermi[1] = "ods"
                extensionpermi[2] = "odt"
            }

            if (optSelectedNewBorrador == SALIDA_OFICIO) {
                var extgrabadas = JSON.parse(configsHasValue['EXTENSIONES_RADICADO_1']);
                if (extgrabadas.length > 1){
                    $('#cargardoc').attr("accept", ".docx,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/pdf");
                }else{
                    if (extgrabadas[0] == 'doc' || extgrabadas[0] == 'docx'){
                        $('#cargardoc').attr("accept", ".docx,application/vnd.openxmlformats-officedocument.wordprocessingml.document");

                    }else {
                        $('#cargardoc').attr("accept", "application/pdf");
                    }
                }
                for (let i = 0; i < extgrabadas.length; i++) {
                    extensionpermi[i] = extgrabadas[i];
                }

            }
            
            //si es custom, permite estas extensiones
            if (customBorradorSel == true) {
                $('#cargardoc').attr("accept", "application/vnd.oasis.opendocument.text,application/vnd.oasis.opendocument.spreadsheet,.docx,application/vnd.openxmlformats-officedocument.wordprocessingml.document")
                extensionpermi[1] = datosCustomBorrador.extensiones_radicado[0];
            }


            //si es REPORTE4 permite seleccionar pdf y docx
            if (optSelectedNewBorrador == REPORTE4) {

                var string_acept = '';
                if (Object.keys(REPORTE4_EXTENSIONES).length > 0) {
                    for (var i = 0; i < Object.keys(REPORTE4_EXTENSIONES).length; i++) {
                        extensionpermi[i] = REPORTE4_EXTENSIONES[i];
                        string_acept += '.' + REPORTE4_EXTENSIONES[i] + ','
                    }
                }
                $('#cargardoc').attr("accept", string_acept)
                // $('#cargardoc').attr("accept", "application/pdf, .docx")
                //extensionpermi[1] = "pdf"
            }
            console.log('extensionpermi', extensionpermi)

            //defino el input type file
            $("#cargardoc").change(function (event) {
                event.preventDefault();

                if (permisoEnBodega == 'SI') {
                    var namefile = getFile($("#cargardoc").val());
                    extension = namefile.toLowerCase()
                    var pasa = false;
                    var strpermitidas = "";
                    $.each(extensionpermi, function (key, value) {

                        strpermitidas += "." + value + " "
                        if (extension == value) {
                            pasa = true;
                        }
                    });

                    if (pasa == false) {
                        Swal.fire("Alerta", "El tipo de archivo deben ser " + strpermitidas, "warning");
                        $("#cargardoc").val("");
                        return false;
                    }

                    move();
                    setTimeout(function () {
                        crearBorrador(event)
                    }, 1500)

                } else {
                    Swal.fire("Alerta", "Problemas para subir archivos a la bodega. Solicitar soporte URGENTE.", "error");
                }


            });

        }

        /**
         * Cuando presiono cualquier boton de cargar archivo principal
         */
        function confirmarcargadoc() {

            if ($("#id_destinatario").val() == "") {
                Swal.fire("Alerta", "Debe ingresar un destinatario y un asunto primero", "warning");
                return false;
            }

            /**
             * Si optSelectedNewBorrador, es cualquier ade los 3 siguientes, entonces no le muestra el editor,
             * es decir, no pasa a la siguiente validacion
             */
            if (
                optSelectedNewBorrador == INTERNA_CON_PLANTILLA
                ||
                optSelectedNewBorrador == INTERNA_SIN_PLANTILLA
                ||
                optSelectedNewBorrador == REPORTE4 || customBorradorSel == true 
            ) {
                mostrarDivDescargaPlant()
                return true;
            }

            /**
             * Si llega aqui,  es porque optSelectedNewBorrador es distinto de las 3 anteriores
             */
            if (
                (
                    borradorActual.ID_USUARIO_ACTUAL != undefined
                    &&
                    borradorActual.ID_USUARIO_ACTUAL == codusuario
                )
                ||
                (
                    borrador_id == ""
                )
            ) {
                //@formatter:off
                Swal.fire({
                    title: "Alerta",
                    text: "Si elige subir una plantilla como documento principal, no podrá usar el editor web para este borrador",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Confirmar",
                    cancelButtonText: "Cancelar",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }).then((result) => {
                    if (result.value) {

                        /**
                        * Si entra aqui, es porque confirma que quiere cargar una plantilla
                        */
                    mostrarDivDescargaPlant()
                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                        /**
                        * Si entra aqui, es porque cancela que quiere cargar una plantilla
                        */
                    validarmostrarEditor();
                }
            });
                //@formatter:on
            } else {

                Swal.fire("Error", "Esta opción no está permitida", "error");
                return false;

            }
        }

        function cerrarmodborrador() {

            /*if ((borrador_id == "" )|| (borrador_id != "" && $('.guardar').val('Guardar'))) {

             Swal.fire({
             title: "Alerta",
             text: "Si cierra ahora perderá los cambios, desea cerrar la ventana?",
             type: "warning",
             showCancelButton: true,
             confirmButtonText: "Cerrar",
             cancelButtonText: "Cancelar",
             closeOnConfirm: true,
             closeOnCancel: true
             },
             function (isConfirm) {
             if (isConfirm) {
             $('#all_modal').modal('hide');
             } else {

             }
             });
             } else {*/
            $('#all_modal').modal('hide');
            //}
            customBorradorSel = false;
            datosCustomBorrador = {};
        }

        function revisar(event) {

            event.preventDefault();
            $('#comentariorevisar').maxlength({
                alwaysShow: true,
                threshold: 9,
                appendToParent: true,
                warningClass: "label label-success",
                limitReachedClass: "label label-danger",
                preText: 'Mínimo ' + configsHasValue['COMENTARIOS_MINLENGTH'] + ' caracteres. ',
                validate: true
            });
            $("#comentariorevisar").val('');
            $("#comentariorevisar").html('');
            $("#modal_comentario_revisar").modal('show');
            $("#modal_coment_no_aprobar").css('z-index', '99999');
            setTimeout(function () {
                $("#comentariorevisar").focus();

            }, 500)

        }


        function guardarRevisarBorrador() {
            if ( BorradorRevisado == true){
                Swal.fire({
                    title: "Alerta",
                    text: "Ya tiene una revision. ¿Desea volver a revisar?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Si",
                    cancelButtonText: "No",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }).then((result) => {
                    if (result.value){
                        var comentario = $("#comentarioenviar")
                        var tamano = comentario.val()

                        if (comentario.val() != "" && tamano.length < configsHasValue['COMENTARIOS_MINLENGTH']) {
                            Swal.fire("Alerta", "Debe ingresar un comentario de al menos " + configsHasValue['COMENTARIOS_MINLENGTH'] + " caracteres", "warning");
                            return false;
                        }
                        $.ajax({
                            url: "generalborrador.php",
                            method: "POST",
                            data: {
                                borrador_id: borrador_id,
                                comentario: $("#comentariorevisar").val(),
                                function_call: "revisarBorrador"
                            },
                            dataType: "json",
                            success: function (response) {

                                //validaExpiro(response);
                                if (response.success) {
                                    borradorActual.historico = response.historico
                                    $("#modal_comentario_revisar").modal('hide');
                                    mostrarcomentarios(response.comentarios);
                                    mostrarDivRadicar(borradorActual, response.comentarios)
                                    $("#div_enviar").css("display", "block");
                                    $(".depende_div_enviar").css("display", "block");
                                    Swal.fire("Revisar!", "El borrador ha sido revisado", "success");
                                    BorradorRevisado = true;
                                } else {

                                    Swal.fire("Error", "Ha ocurrido un error al revisar el borrador", "error");
                                }
                            }
                        }).done(function () {
                        }).fail(function (error) {
                            console.log(error);
                        });
                    }else if (
                        /* Read more about handling dismissals below */
                        result.dismiss === Swal.DismissReason.cancel
                    ) {
                        /**
                         * Si entra aqui, es porque cancela
                         */
                        return false;
                    }
                });
            }else {
                var comentario = $("#comentarioenviar")
                var tamano = comentario.val()

                if (comentario.val() != "" && tamano.length < configsHasValue['COMENTARIOS_MINLENGTH']) {
                    Swal.fire("Alerta", "Debe ingresar un comentario de al menos " + configsHasValue['COMENTARIOS_MINLENGTH'] + " caracteres", "warning");
                    return false;
                }
                $.ajax({
                    url: "generalborrador.php",
                    method: "POST",
                    data: {
                        borrador_id: borrador_id,
                        comentario: $("#comentariorevisar").val(),
                        function_call: "revisarBorrador"
                    },
                    dataType: "json",
                    success: function (response) {

                        //validaExpiro(response);
                        if (response.success) {
                            borradorActual.historico = response.historico
                            $("#modal_comentario_revisar").modal('hide');
                            mostrarcomentarios(response.comentarios);
                            mostrarDivRadicar(borradorActual, response.comentarios)
                            $("#div_enviar").css("display", "block");
                            $(".depende_div_enviar").css("display", "block");
                            Swal.fire("Revisar!", "El borrador ha sido revisado", "success");
                            BorradorRevisado = true;
                        } else {

                            Swal.fire("Error", "Ha ocurrido un error al revisar el borrador", "error");
                        }
                    }
                }).done(function () {
                }).fail(function (error) {
                    console.log(error);
                });
            }
        }

        function aprobarBorrador() {
            if (borradorActual.ID_USUARIO_ACTUAL == codusuario) {
                $.ajax({
                    url: "generalborrador.php",
                    method: "POST",
                    data: {
                        borrador_id: borrador_id,
                        function_call: "aprobarborrador",
                        'comentario_aprobar': comentario_aprobar
                    },
                    async: false,
                    dataType: "json",
                    success: function (response) {

                        if (response.success) {
                            $("#div_no_aprobar").remove();
                            setTimeout(function () {
                                alertModal('Aprobado!', 'El borrador ha sido aprobado, ya se puede Radicar', 'success', 5000)
                            }, 1000)
                            $("#div_radicar").css("display", "block");
                            $("#div_enviar").css("display", "block");
                            $(".depende_div_enviar").css("display", "block");

                            $("#buttom_aprobar").html("<i class='fa fa-check'></i>Revisar");
                            $("#buttom_aprobar").attr("onclick", "revisar(event)");
                            $("#buttom_aprobar").css("display", "block");


                            //si aprueba, quito el boton que despliega las opciones
                            $('.depende_div_aprobar').remove();
                            $('#uldropdownaprobar').remove();
                            setTimeout(function () {
                                $("#buttom_aprobar").css("border-top-right-radius", "20px");
                                $("#buttom_aprobar").css("border-bottom-right-radius", "20px");
                            }, 50)

                            mostrarcomentarios(response.comentarios);
                            mostrarDivRadicar(borradorActual, response.comentarios)

                        } else {
                            setTimeout(function () {
                                Swal.fire("Error", "Ha ocurrido un error al actualizar el estado del borrador", "error");
                            }, 1000)

                        }
                    }
                }).done(function () {
                }).fail(function (error) {
                    console.log(error);
                });

            } else {
                Swal.fire("Error", "Usted no puede aprobar este borrador", "error");
            }
        }

        /*esta funcion se ejecuta cuando se va a aprobar y el borrador
         * ya tenia un usuario anterior, por lo que se muestra una alerta de que sera
         * enviado nuevamente al usuarioanterior*/
        function alertaseraenviado() {

            let timerInterval;
            //@formatter:off
            Swal.fire({
                title: 'Alerta',
                type:"warning",
                html: "El borrador será enviado a: " + nombre_usu_anterior + ", En <b>9</b> segundos...",
                timer: 9000,
                timerProgressBar: true,
                showCancelButton: true,
                confirmButtonText: "Enviar",
                cancelButtonText: "Cancelar",
                closeOnConfirm: true,
                closeOnCancel: true,
                onBeforeOpen: () => {
                Swal.showLoading()
                timerInterval = setInterval(() => {
                    const content = Swal.getContent()
                    if (content) {
                        const b = content.querySelector('b')
                        if (b) {
                            b.textContent = Math.floor((Swal.getTimerLeft()/1000) % 60);
                        }
                    }
                }, 1000)
        },onClose: () => {
                clearInterval(timerInterval)
            }
        }).then((result) => {

                /* Se cerró solo */
                if (result.dismiss === Swal.DismissReason.timer) {
                ejecutaEnviar(borradorActual.ID_USUARIO_ANTERIOR, 'Borrador reenviado  al usuario anterior, luego de Aprobar')
            }
        })
            //@formatter:on


        }


        /*
         * cuando se le da click al boton de aprobar en el modal
         * */
        function botonaprobar(event) {

            continuarEscSwal = true;
            comentario_aprobar = '';
            event.preventDefault();
            //@formatter:off
            Swal.fire({
                title: 'Alerta',
                text: "Qué desea hacer?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Aprobar',
                cancelButtonText: 'Revisar',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then((result) => {
                //si existe result.value y es ==true, entonces es que confirmaste
                //si no, llega result.dismiss y puede ser "esc" de la tecla escape y "cancel"
                //con cancel, entra en la segunda condicion, con esc, no
                if (result.value) {
                confirmSwaAprobar();
            } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
            ) {
                revisar(event);
            }
        })
            //@formatter:on

        }


        //cuando arriba, en el swa1 confirman que van a aprobar el borrador
        function confirmSwaAprobar() {
            aprobarBorrador();
            almenosaprobado = true;
            //si lo estoy aprobando, pero era de un usuario anterior,
            //se lo devuelvo, es decir, se lo envio otra vez
            if (Object.keys(borradorActual).length > 0 && borradorActual.ID_USUARIO_ANTERIOR != undefined &&
                borradorActual.ID_USUARIO_ANTERIOR != "" && borradorActual.ID_USUARIO_ANTERIOR != "0"
                && borradorActual.ID_USUARIO_ANTERIOR != 0) {
                canceloEnvioAprob = false;
                timer = 9
                isTimerStarted = false;
                setTimeout(function () {
                    /*estas variables as  seteo cada vez que voy a confirmar, si quiero enviar*/
                    alertaseraenviado();
                }, 2000)
            }
        }

        //cuando en el modal de aprobar para escribir un comentario, se le da click.
        function btnModalComentAprobar() {
            comentario_aprobar = $('#comentarioaprobar').val()
            confirmSwaAprobar();
            $('#modal_comentario_aprobar').modal('hide')
        }

        //cuando en el modal de guardar comentario al borrador, se le da click al boton de guardar.
        function clickSaveComentBorrad() {
            if ($('#comentarioaborrador').val() == '') {
                alertModal('Alerta!', 'Debe ingresar un comentario', 'warning', 5000)
                return false;
            }

            $.ajax({
                url: "generalborrador.php",
                method: "POST",
                data: {
                    borrador_id: borrador_id,
                    comentario: $('#comentarioaborrador').val(),
                    function_call: "guardarComentBorrador",
                },
                dataType: "json",
                async: false,
                success: function (response) {
                    validaExpiro(response);
                    if (response.success) {
                        $('#modal_comentario_borrador').modal('hide');
                        alertModal('Comentado!', 'El comentario ha sido guardado', 'success', '5000')
                        mostrarcomentarios(response.comentarios);
                    } else {
                        alertModal('Error!', response.error, 'error', '5000')
                    }
                }
            }).done(function () {
            }).fail(function (error) {
                Swal.fire("Error", "Ha ocurrido un error", "error");
            });

        }

        /*
         * funcion que se ejecuta cuando se le da click al confirmar el enviar el borrador
         */
        function onclickguardarenv() {

            var enviara = $("#enviara").val()
            var comentarioenviar = $("#comentarioenviar").val()
            ejecutaEnviar(enviara, comentarioenviar);
            

        }
        
        function notificarEnvio(){
            //aqui NOTIFICA
            var subparams = {'id':borrador_id,'id_usuario':codusuario};
             var params = {
                        'notifName': 'SEND_ERRASER',
                        'valuesArr': subparams
                    }
                    console.log(configsHasValue);
                    if (configsHasValue['FUNCTION_NOTIFICACIONES_TX'] == 'SI') {
                        var ajax = NotificacionesService.sendNotificacion(params);

                            ajax.success(function (response) {
                                if (response.isSuccess) {
                                    alertModal('Enviado!', 'La Notificacion ha sido enviada al correo', 'success', '5000');
                                } else {
                                    alertModal('Advertencia', 'La Notificacion NO ha sido enviada al correo', 'success', '5000');
                                }
                            }).error(function (error) {
                                alertModal('Advertencia', 'La Notificacion NO ha sido enviada al correo', 'success', '5000');
                            });
                    }
        }


        //esta funcion se llama cada vez que se quiera enviar un borrador a alguien
        function ejecutaEnviar(enviara, comentarioenviar) {
            fn60sec();
            var accionenviar = guardarenv(enviara, comentarioenviar)
            if (accionenviar['success'] != undefined) {

                if ($('#check_enviar_marcar_como_revisado') != undefined &&
                    $('#check_enviar_marcar_como_revisado').is(':checked')) {


                    var params = {
                        'borrador_id': borrador_id,
                        'comentario': '',
                        'usuario_id': codusuario,
                        'dependencia': dependencia_user_log
                    }

                    if (BorradorRevisado == true){

                        Swal.fire({
                            title: "Alerta",
                            text: "Ya tiene una revision. ¿Desea volver a revisar?",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonText: "Si",
                            cancelButtonText: "No",
                            closeOnConfirm: true,
                            closeOnCancel: true
                        }).then((result) => {
                            if (result.value) {

                                /**
                                 * Si entra aqui, es porque confirma que quiere revisar de nuevo
                                 */
                                var ajax = BorradorService.revisarBorrador(params);
                                ajax.success(function (response) {
                                    notificarEnvio();
                                }).error(function (error) {

                                });
                            } else if (
                                /* Read more about handling dismissals below */
                                result.dismiss === Swal.DismissReason.cancel
                            ) {
                                /**
                                 * Si entra aqui, es porque cancela
                                 */
                                 return false;
                            }
                        });

                    }

                    var ajax = BorradorService.revisarBorrador(params);
                    ajax.success(function (response) {
                        notificarEnvio();
                    }).error(function (error) {

                    });
                }else{
                    notificarEnvio();
                }


                alertModal('Enviado!', 'El borrador ha sido enviado', 'success', '5000')
                $('#modal_enviar').modal('hide');
                $('#all_modal').modal('hide');

                //redirecciono a la lista de borradores
                setTimeout(function () {
                    $('#all_modal').modal('hide');
                }, 3500);
            } else if (accionenviar['error'] != undefined) {
                Swal.fire("Error", "Ha ocurrido un error al enviar el borrador", "error");
            }
        }

        /*
         * funcion que se ejecuta luego de pasar por onclickguardarenv
         */

        function guardarenv(enviara, comentarioenviar) {

            var retorno = new Array();
            if (codusuario != enviara) {
                if (borradorActual.ID_USUARIO_ACTUAL == codusuario) {

                    if (enviara != "" && enviara != null) {

                        //el comentario debe ser obligatorio y de tantos caracteres minimo
                        if (comentarioenviar.length >= configsHasValue['COMENTARIOS_MINLENGTH']) {
                            $.ajax({
                                url: "generalborrador.php",
                                method: "POST",
                                data: {
                                    borrador_id: borrador_id,
                                    anterior: borradorActual.ID_USUARIO_ANTERIOR,
                                    actual: borradorActual.ID_USUARIO_ACTUAL,
                                    function_call: "guardarEnviar",
                                    usuario_destino: enviara,
                                    comentarioenviar: comentarioenviar,
                                    dependencia_anterior: borradorActual.DEPENDENCIA_USU_ANTERIOR,
                                    dependencia_actual: borradorActual.DEPENDENCIA_USU_ACTUAL,
                                },
                                dataType: "json",
                                async: false,
                                success: function (response) {
                                    validaExpiro(response);
                                    if (response.success) {
                                        retorno['success'] = "El borrador ha sido enviado"
                                    } else {
                                        retorno['error'] = "Ha ocurrido un error al enviar el borrador"
                                    }
                                }
                            }).done(function () {
                            }).fail(function (error) {
                                Swal.fire("Error", "Ha ocurrido un error", "error");
                            });
                        } else {
                            Swal.fire("Alerta", "Debe ingresar un comentario de al menos " + configsHasValue['COMENTARIOS_MINLENGTH'] + " caracteres", "warning");
                        }
                    } else {
                        Swal.fire("Alerta", "Debe seleccionar al menos un usuario", "warning");
                    }
                } else {
                    Swal.fire("Error", "Usted no es el usuario actual", "error");
                }
            } else {
                Swal.fire("Error", "No se puede enviar a usted mismo", "error");
            }
            return retorno;
        }

        //fncion que actuliza el radicado origen
        function seleccionarOrigen(radicado, confirmar_999=false) {
            $.ajax({
                url: "generalborrador.php",
                method: "POST",
                data: {
                    borrador_id: borrador_id,
                    radicado_asociado: radicado,
                    function_call: "seleccionarOrigen",
                    confirmar_999: confirmar_999
                },
                dataType: "json",
                success: function (response) {

                    if (response.radiaso) {
                        mostrarradicadoAso(response.radiaso);
                        Swal.fire("Felicidades", "El radicado " + radicado + " ha sido seleccionado como radicado padre", "success");
                    } else {
                        if (response.error == 'El radicado no existe') {
                            Swal.fire("Error", response.error, "error");
                        } else {
                            Swal.fire({
                                    title: "Alerta",
                                    text: response.error,
                                    type: "warning",
                                    showCancelButton: true,
                                    confirmButtonText: "SI",
                                    cancelButtonText: "NO",
                                    closeOnConfirm: true,
                                    closeOnCancel: true
                                },
                                function (isConfirm) {
                                    if (isConfirm) {
                                        seleccionarOrigen(radicado, true)
                                    } else {
                                        $("#radioselecOrigen" + radPadre).prop("checked", true);
                                    }
                                });
                        }

                    }
                }
            }).done(function () {
            }).fail(function (error) {
                Swal.fire("Error", "Ha ocurrido un error", "error");
            });
        }

        //para confirmar al seleccionar un radicado asociado como radicado padre
        function confirmSelectRadOrigen(radicado_asociado) {
            if (configsHasValue['ANEXO_HIJO_RADICADO_5'] == "NO" && $(tipo_radicado).val() == 5) {
                Swal.fire("Advertencia", "Este tipo de borrador solo puede asociarle radicados.", "warning");
                $("#radioselecOrigen" + radPadre).prop("checked", false);
                $("#radioselecOrigen" + radicado_asociado).prop("checked", false);
            }else {
                Swal.fire({
                    title: "Alerta",
                    text: "Desea seleccionar el radicado " + radicado_asociado + ", como radicado padre?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "SI",
                    cancelButtonText: "NO",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }).then((result) => {
                    if (result.value) {
                        seleccionarOrigen(radicado_asociado)
                    } else if (
                        /* Read more about handling dismissals below */
                        result.dismiss === Swal.DismissReason.cancel
                    ) {
                        $("#radioselecOrigen" + radPadre).prop("checked", false);
                        $("#radioselecOrigen" + radicado_asociado).prop("checked", false);

                    }
                })
            }
        }

        function mostrarradicadoAso(asociados) {
            $("#append_radiaso").html('');

            var html = "";
            var id = ""
            //var d = new Date(asociados[i]["RADI_FECH_RADI"]);
            var e = "";
            var selected = '';
            var radOrigen = 0
            for (var i = 0; i < asociados.length; i++) {
                radOrigen = 0
                selected = '';
                id = asociados[i]["RADI_NUME_RADI"];
                d = new Date();
                var d = new Date(asociados[i]["RADI_FECH_RADI"]);
                e = formatDate(d);

                html += '<div class="row"><div class="col-md-12">';

                //aqui muestro el radio button para saber cual va a ser rl radicado padre
                if (asociados[i]["RADICADO_ORIGEN"] == 1) {
                    radOrigen = 1;
                    selected = 'checked';
                    radPadre = asociados[i]["RADI_NUME_RADI"]; //seteo el radiado padre
                }

                if (Object.keys(asociados).length < 2 && radOrigen == 1) {
                    //cuando el unico radicado asocaido sea el padre, ntonces no aparece el radio button

                } else {
                    html += '<input id="radioselecOrigen' + asociados[i]["RADI_NUME_RADI"] + '" ' +
                        'title="Radicado padre del borrador actual" style="text-decoration: none !important;' +
                        'padding: 0 0.4em !important;margin: 01em 0 0 0.5em !important;' +
                        ' padding-left: 5px; float:left !important" type="radio" name="radio_rad_origen" ' +
                        'onclick="confirmSelectRadOrigen(' + asociados[i]["RADI_NUME_RADI"] + ')" ' + selected + ' /> ';
                }

                if (asociados[i]["RADICADO_ORIGEN"] == 1) {
                    html += '' +
                        '<span title="Radicado padre del borrador actual" style="text-decoration: none !important;color: black !important;' +
                        'padding: 0 0.4em !important;margin: 01em 0 0 0.5em !important;' +
                        ' padding-left: 5px; float:left !important" class="glyphicon glyphicon-paperclip"> </span>';
                }
                html += '';
                html += '';
                html += ' <h5><a href="../../verradicado.php?verrad=' + asociados[i]["RADI_NUME_RADI"] + '&PHPSESSID=' + session_id + '&depeBuscada=&filtroSelect=&tpAnulacion=&carpeta=&tipo_carp=0&adodb_next_page=1&busqRadicados=&nomcarpeta=Interna&agendado=&orderTipo=DESC&chkCarpeta=&orderNo=16" ' +
                    'target="_blank" >' +
                    asociados[i]["RADI_NUME_RADI"] + ', ' +
                    '' + asociados[i]["RA_ASUN"].substring(0, 70);

                if (asociados[i]["RA_ASUN"].length > 70) {

                    html += '...';
                }
                html += ' ' + e;
                html += '</a>';


                if (borradorActual.ID_USUARIO_ACTUAL == codusuario) {
                    /* html+='<button class="btn red-intense" type="button" onclick="removeradiaso(' + id + ')"' +
                     ' style="float: left;"' +
                     ' > <i class="fa fa-remove"></i></button>';*/

                    if (configsHasValue['MODIFICAR_AL_APROBAR'] == "NO" && usuarioAproboBorrador != "" && usuarioAproboBorrador != codusuario) {
                    } else {
                        html += ' <a href="#" class="cerrarmostrarR" style="border: 1px solid !important;border-color: #e0e6e9 !important;' +
                            'background-color: #fff !important;color: #c1cbd1 !important;text-decoration: none !important;' +
                            'padding: 0 0.4em !important;font-size: 15px !important;margin: -0.1em 0 0 0.5em !important;' +
                            ' padding-left: 5px; "  ' +
                            ' onclick="removeradiaso(' + id + ',' + radOrigen + ')">X    ' +
                            '</a>';
                    }


                }


                html += ' </h5> </div> </div>';
            }


            $("#append_radiaso").append(html);

            delayBotones();

        }

        function formatDate(date) {

            var hours = date.getHours();
            var minutes = date.getMinutes();
            var ampm = hours >= 12 ? 'pm' : 'am';
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            minutes = minutes < 10 ? '0' + minutes : minutes;
            var strTime = hours + ':' + minutes + ' ' + ampm;
            var mes = (parseInt(date.getMonth()) + parseInt(1))
            return date.getFullYear() + "-" + mes + "-" + date.getDate() + "  " + strTime;
        }


        function delayBotones() {
            $(".cerrarmostrarR").hover(function () {
                // $( this ).fadeOut( 500 );
                $(this).fadeIn(250);
                $(this).css('background-color', '#e7505a');
            });

            $(".cerrarmostrarR")
                .mouseout(function () {
                    $(this).css('background-color', '#fff');
                })
        }

        //muestra los comentarios traidos del servidor
        function mostrarcomentarios(comentarios) {
            $("#ulchat").html('');

            console.log('comentarios', comentarios)
            var html = "";
            var fecha = "";
            var comentario = '';
            for (var i = 0; i < Object.keys(comentarios).length; i++) {

                d = new Date();
                var d = new Date(comentarios[i]["FECHA_TRANSACCION"]);

                fecha = formatDate(d);

                html += ' <li class="in"> <div class="message"> <span class="arrow"> </span>' +
                    '<a href="#" class="name">' + comentarios[i]["USUA_NOMB"] + ' - ' + comentarios[i]["DEPE_NOMB"] + '</a>' +
                    '<span class="datetime"> ' + fecha + ' </span>' +
                    '<a href="#" class="name">';


                if (comentarios[i]["ID_BORRADOR_ESTADO"] == "0" || comentarios[i]["ID_BORRADOR_ESTADO"] == undefined) {
                    html += 'Enviado';
                } else {

                    if (comentarios[i]["NOMBRE_ESTADO"] == "Sin Aprobar") {
                        html += 'Proyectado';
                    } else {
                        if (comentarios[i]["NOMBRE_ESTADO"] == "" || comentarios[i]["NOMBRE_ESTADO"] == "null"
                            || comentarios[i]["NOMBRE_ESTADO"] == null) {
                            html += '' + comentarios[i]["NOMBRE_TRANSACCION"];
                        } else {
                            html += '' + comentarios[i]["NOMBRE_ESTADO"];
                        }

                    }

                }
                comentario = comentarios[i]["COMENTARIO_BORRADOR"] != null && comentarios[i]["COMENTARIO_BORRADOR"] != 'null' ? comentarios[i]["COMENTARIO_BORRADOR"] : '';
                html += '</a>' +
                    ' <span class="body">' + comentario + '</span>' +
                    ' </div> </li>';

                //84 = Aprobado
                if (comentarios[i]["SGD_TTR_CODIGO"] == "84") {
                    //si el usuario que lo aprobo, es distinto del que esta logueado.
                    //No permitir modificar el contenido de un documento (Destinatario, Asunto, editor web, plantilla, asociados, anexos)
                    if (comentarios[i]["ID_USUARIO_ORIGEN"] != codusuario) {

                    }
                }

            }
            $("#ulchat").append(html);


        }


        //muestra los anexos traidos del servidor
        function mostraranexos(anexos) {
            $("#append_anexos").html('');

            var html = "";
            var nombre = "";
            for (var i = 0; i < anexos.length; i++) {

                nombre = "'";
                nombre += anexos[i]["ANEXO_NOMBRE"] + "'";
                var cadena = new String(anexos[i]["ANEXO_NOMBRE"]);

                html += ' ' +
                    '<div class="row"><div class="col-md-12">';


                if (anexos[i]['EXTENSION'] == "pdf" || anexos[i]['EXTENSION'] == "PDF") {
                    var tipoextension = "'pdf'"
                    var href = "'" + anexos[i]["ANEXO_RUTA"] + "'"

                    //si el navegador actual es firefox, entonces no va a mostrar nada en el modal, sino que
                    //lo mando a descargar en una pestaña nueva del navegador
                    /*if (navegadorActual == "isFirefox") {
                        html += '<h5><a  href="' + anexos[i]["ANEXO_RUTA"] + '" target="_blank" ' +
                            'style=" text-transform: lowercase !important;">' + cadena + '</a>';
                    } else {

                    }*/
                    html += '<h5><a  href="#" onclick="exportar(' + href + ',' + tipoextension + ')" ' +
                        'style="">' + cadena + '</a>';
                } else {

                    html += '<h5><a  href="' + anexos[i]["ANEXO_RUTA"] + '" target="_blank" ' +
                        'style="">' + cadena + '</a>';
                }
                if (borradorActual.ID_USUARIO_ACTUAL == codusuario) {

                    if (configsHasValue['MODIFICAR_AL_APROBAR'] == "NO" && usuarioAproboBorrador != "" && usuarioAproboBorrador != codusuario) {
                    } else {
                        html += '<a href="#" class="cerrarmostrarR" style="border: 1px solid !important;border-color: #e0e6e9 !important;' +
                            'background-color: #fff !important;color: #c1cbd1 !important;text-decoration: none !important;' +
                            'padding: 0 0.4em !important;font-size: 15px !important;margin: -0.1em 0 0 0.5em !important;' +
                            ' padding-left: 5px; "  ' +
                            ' onclick="removeanexo(' + anexos[i]["ID_BORRADOR_ANEXO"] + ')">X' +
                            '</a>';
                    }

                }


                html += '</h5> </div></div>'

            }

            $("#append_anexos").append(html);
            delayBotones();
        }

        function accionremoveradiaso(radicado_asociado) {
            $.ajax({
                url: "generalborrador.php",
                method: "POST",
                data: {
                    borrador_id: borrador_id,
                    radicado_asociado: radicado_asociado,
                    function_call: "removeRadiAsoc",
                },
                dataType: "json",
                success: function (response) {
                    validaExpiro(response);
                    if (response.radiaso) {
                        for (var i = 0; i < misRadicadosAsociados.length; i++) {
                            if (misRadicadosAsociados[i]['RADI_NUME_RADI'] == radicado_asociado) {
                                misRadicadosAsociados.splice(i,1);
                            }
                        }
                        mostrarradicadoAso(response.radiaso);
                    } else {

                        Swal.fire("Error", "Ha ocurrido un error al eliminar el radicado Asociado", "error");
                    }
                }
            }).done(function () {
            }).fail(function (error) {
                Swal.fire("Error", "Ha ocurrido un error", "error");
            });
        }

        //hace una validacion antes de eliminar el radiacdo asociado
        function removeradiaso(radicado_asociado, radOrigen) {


            if (borradorActual.ID_USUARIO_ACTUAL == codusuario) {
                if (radOrigen == 1) {
                    //@formatter:off
                    Swal.fire({
                        title: "Alerta",
                        text: "Está seguro de eliminar el radicado como padre? El nuevo radicado no quedará asociado a este.",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonText: "SI",
                        cancelButtonText: "NO",
                        closeOnConfirm: true,
                        closeOnCancel: true
                    }).then((result) => {
                        if (result.value) {
                        accionremoveradiaso(radicado_asociado)
                    } else if (
                        /* Read more about handling dismissals below */
                        result.dismiss === Swal.DismissReason.cancel
                    ) {

                    }
                });
                    //@formatter:on
                } else {
                    accionremoveradiaso(radicado_asociado)
                }
            } else {
                Swal.fire("Error", "No tiene permisos para eliminar este radicado asociado", "error");
            }

        }

        //busca los anexos , se usa cuando se cierra el modal de anexos
        function getAnexos() {

            if (borradorActual.ID_USUARIO_ACTUAL == codusuario) {
                $.ajax({
                    url: "generalborrador.php",
                    method: "POST",
                    data: {
                        borrador_id: borrador_id,
                        function_call: "getAnexos",
                    },
                    dataType: "json",
                    success: function (response) {
                        validaExpiro(response);
                        if (response.anexos) {
                            anexosBorrador = response.anexos;
                            mostraranexos(response.anexos);

                            if (optSelectedNewBorrador == REPORTE4) {
                                putFoliosAnexosFromCountAnexos(true);
                            }else{
                                putFoliosAnexosFromCountAnexos(false);
                            }

                        } else {

                            Swal.fire("Error", "Ha ocurrido un error al buscar los anexos", "error");
                        }
                    }
                }).done(function () {
                }).fail(function (error) {
                    Swal.fire("Error", "Ha ocurrido un error", "error");
                });
            } else {
                Swal.fire("Error", "No tiene permisos para eliminar este anexo", "error");
            }
        }

        //elimina un anexo
        function removeanexo(id) {

            if (borradorActual.ID_USUARIO_ACTUAL == codusuario) {
                $.ajax({
                    url: "generalborrador.php",
                    method: "POST",
                    data: {
                        borrador_id: borrador_id,
                        id_borrador_anexo: id,
                        function_call: "removeanexo",
                    },
                    dataType: "json",
                    success: function (response) {
                        validaExpiro(response);
                        if (response.anexos) {
                            anexosBorrador = response.anexos
                            if (optSelectedNewBorrador == REPORTE4) {
                                putFoliosAnexosFromCountAnexos(true);
                            }else{
                                putFoliosAnexosFromCountAnexos(false);
                            }
                            mostraranexos(response.anexos);
                        } else {

                            Swal.fire("Error", "Ha ocurrido un error al eliminar el anexo", "error");
                        }
                    }
                }).done(function () {
                }).fail(function (error) {
                    Swal.fire("Error", "Ha ocurrido un error", "error");
                });
            } else {
                Swal.fire("Error", "No tiene permisos para eliminar este anexo", "error");
            }
        }

        function guardarAsociado() {

            if ($("#selectradicado").val() != "" && $("#selectradicado").val() != null) {

                $.ajax({
                    url: "generalborrador.php",
                    method: "POST",
                    data: {
                        borrador_id: borrador_id,
                        function_call: "guardarAsociado",
                        radicados: $("#selectradicado").val()
                    },
                    dataType: "json",
                    success: function (response) {
                        validaExpiro(response);
                        if (response.success) {

                            var msj = "Los radicados han sido asociados";
                            var tipo = "success";
                            if (response.alerta) {
                                tipo = "warning";
                                msj = "Los siguientes radicados, ya habian sido asociados a este borrador: ";
                                for (var i = 0; i < response.alerta.length; i++) {
                                    msj += response.alerta[i];

                                    if ((parseInt(i) + parseInt(1)) != response.alerta.length) {
                                        msj += ","
                                        ;
                                    }
                                }

                            }

                            Swal.fire("Asociados", msj, tipo);

                            if (response.radasociados) {
                                var asociados = response.radasociados
                                misRadicadosAsociados = asociados
                                mostrarradicadoAso(asociados);
                            }
                            $("#modal_radicado").modal('hide');
                        } else {

                            Swal.fire("Error", "Ha ocurrido un error al asociar el radicado", "error");
                        }
                    }
                }).done(function () {
                }).fail(function (error) {
                    Swal.fire("Error", "Ha ocurrido un error", "error");
                });
            } else {
                Swal.fire("Alerta", "Debe seleccionar al menos un radicado", "error");
            }
        }

        //cuando se confirma que se va a devolver
        function guardardevolver() {
            if (borradorActual.ID_USUARIO_ACTUAL == codusuario) {
                if ($("#comentariodevolver").prop('disabled') == true ||
                    ($("#comentariodevolver").prop('disabled') == false &&
                        $("#comentariodevolver").val().length >= configsHasValue['COMENTARIOS_MINLENGTH'])) {


                    $.ajax({
                        url: "generalborrador.php",
                        method: "POST",
                        data: {
                            borrador_id: borrador_id,
                            anterior: borradorActual.ID_USUARIO_ANTERIOR,
                            dependencia_anterior: borradorActual.DEPENDENCIA_USU_ANTERIOR,
                            actual: borradorActual.ID_USUARIO_ACTUAL,
                            dependencia_actual: borradorActual.DEPENDENCIA_USU_ACTUAL,
                            comentario: $("#comentariodevolver").val(),
                            function_call: "devolverBorrador"
                        },
                        dataType: "json",
                        success: function (response) {
                            validaExpiro(response);
                            if (response.success) {

                                Swal.fire("Devuelto!", "El borrador fué devuelto al usuario a " + nombre_usu_anterior, "success");
                                $('#modal_comentario_devolver').modal('hide');

                                setTimeout(function () {
                                    $('#all_modal').modal('hide');
                                }, 2000);
                            } else {
                                usuaInacDevolver(response);
                            }
                        }
                    }).done(function () {
                    }).fail(function (error) {
                        Swal.fire("Error", "Ha ocurrido un error", "error");
                    });
                } else {
                    Swal.fire("Alerta", "Debe ingresar un comentario de al menos " + configsHasValue['COMENTARIOS_MINLENGTH'] + " caracteres", "warning");
                }
            } else {
                Swal.fire("Error", "No puede devolver este borrador, ya que usted no es el usuario actual", "error");
            }

        }

        //cuando presionan el boton de confirmar en el modal de No Aprobar
        function guardarnoaprobar() {
            if (borradorActual.ID_USUARIO_ACTUAL == codusuario) {
                if ($("#comentario_no_aprobar").prop('disabled') == true ||
                    ($("#comentario_no_aprobar").prop('disabled') == false &&
                        $("#comentario_no_aprobar").val().length >= configsHasValue['COMENTARIOS_MINLENGTH'])) {


                    $.ajax({
                        url: "generalborrador.php",
                        method: "POST",
                        data: {
                            borrador_id: borrador_id,
                            anterior: borradorActual.ID_USUARIO_ANTERIOR,
                            dependencia_anterior: borradorActual.DEPENDENCIA_USU_ANTERIOR,
                            actual: borradorActual.ID_USUARIO_ACTUAL,
                            dependencia_actual: borradorActual.DEPENDENCIA_USU_ACTUAL,
                            comentario: $("#comentario_no_aprobar").val(),
                            function_call: "noaprobarBorrador"
                        },
                        dataType: "json",
                        success: function (response) {
                            validaExpiro(response);
                            if (response.success) {

                                Swal.fire("No Aprobado!", "El borrador no fué aprobado y será devuelto al usuario a " + nombre_usu_anterior, "success");
                                $('#modal_coment_no_aprobar').modal('hide');
                                setTimeout(function () {
                                    $('#all_modal').modal('hide');
                                }, 4000);
                            } else {
                                usuaInacNoAprobar(response);
                            }
                        }
                    }).done(function () {
                    }).fail(function (error) {
                        Swal.fire("Error", "Ha ocurrido un error", "error");
                    });
                } else {
                    Swal.fire("Alerta", "Debe ingresar un comentario de al menos " + configsHasValue['COMENTARIOS_MINLENGTH'] + " caracteres", "warning");
                }
            } else {
                Swal.fire("Error", "No puede devolver este borrador, ya que usted no es el usuario actual", "error");
            }

        }

        //funcion que deshabilita la caja de texto y muestra una aleeta
        //al intentar devolver a  un usuario que esta inactivo
        function usuaInacDevolver(response) {
            $('#comentariodevolver').prop('disabled', true);
            Swal.fire("Alerta", response.error, "warning");
            $("#labelnombredevolver").html('');
            var html = '<label style="color: #778899 !important;">El usuario: &nbsp;</label>' +
                '<label style="color: #3598dc">' + response.USUA_NOMB + '</label> <label style="color: #778899 !important;">de&nbsp;</label>' +
                '<label style="color: #3598dc">' + response.DEPENDENCIA + '</label><br>' +
                '<label style="color: #778899 !important;">se encuentra: &nbsp;</label>' +
                '<label style="color: red">INACTIVO &nbsp;</label>' +
                '<label style="color: #778899 !important;">No es posible devolver el borrador.</label>'
            $("#labelnombredevolver").html(html);

        }

        //funcion que deshabilita la caja de texto y muestra una aleeta
        //al intentar devolver a  un usuario que esta inactivo
        function usuaInacNoAprobar(response) {
            $('#comentario_no_aprobar').prop('disabled', true);
            Swal.fire("Alerta", response.error, "warning");
            $("#labelnombre_no_aprobar").html('');
            var html = '<label style="color: #778899 !important;">El usuario: &nbsp;</label>' +
                '<label style="color: #3598dc">' + response.USUA_NOMB + '</label> <label style="color: #778899 !important;">de&nbsp;</label>' +
                '<label style="color: #3598dc">' + response.DEPENDENCIA + '</label><br>' +
                '<label style="color: #778899 !important;">se encuentra: &nbsp;</label>' +
                '<label style="color: red">INACTIVO &nbsp;</label>' +
                '<label style="color: #778899 !important;">No es posible devolver el borrador.</label>'
            $("#labelnombre_no_aprobar").html(html);

        }

        //cuando presionan el boton de No Aprobar
        function btn_no_aprobar() {

            var response = validaUsuaEstado();
            if (response.success) {
                modalNoAprobarBorrador()
            } else {
                modalNoAprobarBorrador();
                usuaInacNoAprobar(response)
            }
        }

        //obtiene el estatus de un usuario, lretorna un error si esta inactivo, y success con= si esta activo,
        //se aplica actualmente cuando se va a enviar un borrador
        function validaUsuaEstado() {
            var retorno = false;
            $.ajax({
                url: "generalborrador.php",
                method: "POST",
                data: {
                    usuario: borradorActual.ID_USUARIO_ANTERIOR,
                    function_call: "getUsuaEstado"
                },
                dataType: "json",
                async: false,
                success: function (response) {
                    retorno = response;
                    validaExpiro(response);
                }
            }).done(function () {
            }).fail(function (error) {
                Swal.fire("Error", "Ha ocurrido un error", "error");
            });
            return retorno;
        }

        //cuando presionan el botn de No Aprobar
        function modalNoAprobarBorrador() {
            $('#comentario_no_aprobar').prop('disabled', false);
            $("#comentario_no_aprobar").val('');
            $("#comentario_no_aprobarr").html('');

            $("#labelnombre_no_aprobar").html('');
            $("#labelnombre_no_aprobar").html('<label style="color: #778899 !important;">El borrador será devuelto a: &nbsp;</label>' +
                '<label style="color: #3598dc">' + nombre_usu_anterior + '</label> <label style="color: #778899 !important;">de &nbsp;</label>' +
                '<label style="color: #3598dc">' + depe_nomb_usu_anterior + '</label>');

            //redirecciono a la lista de borradores
            setTimeout(function () {
                $("#comentario_no_aprobar").focus();
            }, 500);

            $('#comentario_no_aprobar').maxlength({
                alwaysShow: true,
                threshold: 9,
                appendToParent: true,
                warningClass: "label label-success",
                limitReachedClass: "label label-danger",
                preText: 'Mínimo ' + configsHasValue['COMENTARIOS_MINLENGTH'] + ' caracteres. ',
                validate: true
            });
            $("#modal_coment_no_aprobar").modal('show');
            $("#modal_coment_no_aprobar").css('z-index', '9999');
        }

        function btn_devolver_dropdown() {
            var response = validaUsuaEstado();
            if (response.success) {
                devolverBorrador()
            } else {
                devolverBorrador();
                usuaInacDevolver(response)
            }
        }

        function devolverBorrador() {
            $('#comentariodevolver').prop('disabled', false);
            $("#comentariodevolver").val('');
            $("#comentariodevolver").html('');

            $("#labelnombredevolver").html('');
            $("#labelnombredevolver").html('<label style="color: #778899 !important;">El borrador será devuelto a: &nbsp;</label>' +
                '<label style="color: #3598dc">' + nombre_usu_anterior + '</label> <label style="color: #778899 !important;">de &nbsp;</label>' +
                '<label style="color: #3598dc">' + depe_nomb_usu_anterior + '</label>');

            //redirecciono a la lista de borradores
            setTimeout(function () {
                $("#comentariodevolver").focus();
            }, 500);

            $('#comentariodevolver').maxlength({
                alwaysShow: true,
                threshold: 9,
                appendToParent: true,
                warningClass: "label label-success",
                limitReachedClass: "label label-danger",
                preText: 'Mínimo ' + configsHasValue['COMENTARIOS_MINLENGTH'] + ' caracteres. ',
                validate: true
            });
            $("#modal_comentario_devolver").modal('show');
            $("#modal_comentario_devolver").css('z-index', '9999');
        }

        //cuando se va a cerrar el modal de anexos
        function cerralModalAnexo() {

            //busco los botones que tienen la clase start dentro del form de anexos
            //hay que empezar a contar despues  de 1, ya que el boton  Subir Todos tiene esta clase
            var cant_anexos = $('#formanexo button[class*=start]').length

            if (cant_anexos > 1) {
                //@formatter:off
                Swal.fire({
                    title: "Alerta",
                    text: "No se han subido los anexos. Desea cerrar esta ventana?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "SI",
                    cancelButtonText: "NO",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }).then((result) => {
                    if (result.value) {
                    $("#modalanadiranexo").modal('hide')
                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                ) {

                }
            });
                //@formatter:on
            } else {
                $("#modalanadiranexo").modal('hide')
            }

        }

        //cuando se presiona sobre el boton de aexos
        function modalanadiranexo() {

            if (borrador_id != "") {
                //permisoEnBodegapermiso para escribir en bodega
                if (permisoEnBodega == 'SI') {
                    $(".files").html('');
                    $("#id_borrador_anexo").val(borrador_id);
                    $("#cargaanexo").val('');
                    $("#modalanadiranexo").modal({show: true, keyboard: false, backdrop: 'static'});
                    $("#modalanadiranexo").css('z-index', '9999');
                } else {
                    Swal.fire("Alerta", "Problemas para subir archivos a la bodega. Solicitar soporte URGENTE.", "error");
                }


            } else {
                Swal.fire("Alerta", "Primero guardar el borrador, para anexar archivos", "error");
            }
        }

        function modalEnviar() {
            //levanta el modal de enviar el borrador

            $('#comentarioenviar').maxlength({
                alwaysShow: true,
                threshold: 9,
                appendToParent: true,
                warningClass: "label label-success",
                limitReachedClass: "label label-danger",
                preText: 'Mínimo ' + configsHasValue['COMENTARIOS_MINLENGTH'] + ' caracteres. ',
                validate: true
            });

            $("#comentarioenviar").val('');
            $("#comentarioenviar").html('');
            $("#enviara").val('');

            $("#enviara").select2(
                {
                    width: "100%",
                    dropdownParent: $("#modal_enviar"),
                    allowClear: true,
                    data: {},
                    ajax: {
                        url: "buscarUsuariosComen.php",
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                search: params.term,
                                type: 'public',
                            };
                        },
                        processResults: function (data, params) {
                            return {
                                results: data,

                            };
                        },
                        cache: true
                    },
                    placeholder: 'Buscar Usuarios',
                    escapeMarkup: function (markup) {
                        return markup;
                    }, // let our custom formatter work

                    language: {
                        inputTooShort: function () {
                            return 'Ingrese un nombre para buscar';
                        }
                    }
                });

            $('#div_enviar_marcar_como_revisado').css('display', 'none');
            $('.icheckbox_flat-green').removeClass('checked');
            $('#check_enviar_marcar_como_revisado').prop('checked', false)
            /**
             *verifico si el usuario actual, ya revisó el borrador; Si no es así, entonces le muestro un check para
             * marcarlo como revisado, y aprovechar de enviarlo
             */

            if ((checkUsuBorradEstatus(borradorActual.ID_USUARIO_ACTUAL, getIdEstadoFromName('Revisado')) === false)
                &&
                (checkUsuBorradEstatus(borradorActual.ID_USUARIO_ACTUAL, getIdEstadoFromName('Aprobado')) === false)) {

                $('#div_enviar_marcar_como_revisado').css('display', 'block');

            }

            $("#modal_enviar").modal('show');
            $("#modal_enviar").css('z-index', '9999');
        }


        function enviarBorrador() {


            $("#modal_comentario_devolver").modal('hide')
            $("#modal_coment_no_aprobar").modal('hide')
            //caqui entra cuandos e presiona el boto enviar
            //pero si el borrador no tiene expdientes, se arroja una alerta para ir a expedientes o continuar
            //y enviar el borrador
            var retornoMisExpe = buscarMisExped(false)
            if (configsHasValue['EXPEDIENTE_ENVIAR'] == "SI" &&
                retornoMisExpe.length < 1) {
                //@formatter:off
                Swal.fire({
                    title: "Alerta",
                    text: "No ha sido definido el expediente en el cual se incluirá el radicado",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Enviar sin Expediente",
                    cancelButtonText: "Ir a Expedientes",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }).then((result) => {
                    if (result.value) {
                    modalEnviar();
                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    mostrarRadbutton = 'SI';
                    modalExpedienteBorrad('NOCREAREXPEDSINRAD');
                }
            });
                //@formatter:on
            } else {
                modalEnviar();
            }

        }

        //abre el modal de Asociar Radicados
        function modalradicado() {

            if (borrador_id != "") {
                $("#selectradicado").val('');

                $("#selectradicado").select2(
                    {
                        width: "100%",
                        dropdownParent: $("#modal_radicado"),
                        minimumInputLength: 1,
                        allowClear: true,
                        data: {},
                        ajax: {
                            url: "buscarRadicado.php",
                            dataType: 'json',
                            delay: 250,
                            data: function (params) {
                                return {
                                    search: params.term,
                                    type: 'public'
                                };
                            },
                            processResults: function (data, params) {
                                return {
                                    results: data,
                                };
                            },
                            cache: true,
                        },
                        placeholder: 'Buscar Radicado',
                        escapeMarkup: function (markup) {
                            return markup;
                        }, // let our custom formatter work

                        language: {
                            inputTooShort: function () {
                                return 'Ingrese número de radicado o asunto a buscar';
                            }
                        }
                    }).on('select2:selecting', function (e) {
                    //ese id es el numero de radicado
                    //cuando selecciono una opcion

                    var yaFueAsociado = validaYaEstaAsociado(e.params.args.data.id);

                    if (yaFueAsociado == true) {
                        e.preventDefault();
                        Swal.fire("Alerta", "Este radicado ya fué asociado a este borrador", "warning");
                        return false;
                    }

                })


                $("#modal_radicado").modal('show');
                $("#modal_radicado").css('z-index', '9999');
            } else {
                Swal.fire("Error", "Primero guardar el borrador, para asociar radicados", "error");
            }
        }

        //valida si un radicado fue asociado a un borraodr
        function validaYaEstaAsociado(radi_nume_radi) {
            var encontro = false;

            for (var i = 0; i < misRadicadosAsociados.length; i++) {

                if (misRadicadosAsociados[i]['RADI_NUME_RADI'] == radi_nume_radi) {
                    encontro = true;
                }

            }

            return encontro; 
        }


        function move() {
            var elem = document.getElementById("loader");
            var width = 1;
            var id = setInterval(frame, 5);

            function frame() {
                if (width >= 100) {
                    clearInterval(id);
                } else {
                    width++;
                    elem.style.width = width + '%';
                }
            }
        }

        function modalJustificDelBorrador(event) {
            event.preventDefault();

            $('#textareadelborrador').maxlength({
                alwaysShow: true,
                threshold: 9,
                appendToParent: true,
                warningClass: "label label-success",
                limitReachedClass: "label label-danger",
                preText: 'Mínimo 20 caracteres.',
                validate: true
            });

            $('#label_justificar_delete_borrador').html('');
            $('#label_justificar_delete_borrador').html('Escriba la justificación para eliminar el borrador [' + borrador_id + '], y si el trámite ya se realizó con un radicado, relacionarlo aquí.');
            $('#textareadelborrador').val('');
            $('#modal_justificar_delete_borrador').modal('show')

        }


        function confirmBorrarBorra() {

            if (borradorActual.ID_USUARIO_ACTUAL == codusuario) {
                $.ajax({
                    url: "generalborrador.php",
                    method: "POST",
                    data: {
                        borrador_id: borrador_id,
                        radicado: radicadoactual,
                        justificacion: $('#textareadelborrador').val(),
                        function_call: "borrarBorrador"
                    },
                    dataType: "json",
                    success: function (response) {
                        validaExpiro(response);
                        if (response.success) {

                            Swal.fire("Eliminado!", "El borrador fue eliminado", "success");
                            $('#all_modal').modal('hide');

                            //redirecciono a la lista de borradores
                            setTimeout(function () {
                                var redirigir = "index.php"

                                if (radicadoactual != "") {
                                    redirigir += "?radicado=" + radicadoactual
                                }
                                window.location.href = redirigir
                            }, 500);
                        } else {

                            Swal.fire("Error", data.error, "error");
                        }
                    }
                }).done(function () {
                }).fail(function (error) {
                    Swal.fire("Error", "Ha ocurrido un error", "error");
                });
            } else {
                Swal.fire("Error", "Usted no es el usuario actual, por lo que no puede eliminar el borrador", "error");
            }
        }

        function borrarBorrador(event) {
            var caracteres = $('#textareadelborrador').val();

            if (caracteres.length < 20) {
                Swal.fire("Ojo", "Debe contener mínimo 20 caracteres", "warning");
                return false
            }

            event.preventDefault();
            //@formatter:off
            Swal.fire({
                title: "Alerta",
                text: "Seguro que desea eliminar el borrador?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Si",
                cancelButtonText: "No",
                closeOnConfirm: true,
                closeOnCancel: true
            }).then((result) => {
                if (result.value) {
                confirmBorrarBorra();
            } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
            ) {

            }
        });
            //@formatter:on

        }


        //cuando el doy click a una seleccion de tipologia, actualizo a los expedientes
        //seleccionados
        function incluircontipologia() {


            $.ajax({
                url: "generalborrador.php",
                method: "POST",
                data: {
                    borrador_id: borrador_id,
                    idtipologia: tipologiaselected,
                    function_call: "incluircontipologia"
                },
                dataType: "json",
                success: function (response) {

                    if (response.success) {

                        alertModal('Tipo Documental','Actualizado en expediente. Puede continuar', 'success', '10000')
                        if (mostrarRadbutton == 'SI'){
                            showRadButton();
                        }

                        var textotipologiaactual = inputtipologia.typeahead('val');
                        tipologiasinborde();
                        definirinputtipologia();
                        inputtipologia.typeahead('val', textotipologiaactual);

                    } else {
                        Swal.fire("Error", response.error, "error");
                    }

                }
            }).done(function () {

            }).fail(function (error) {
                Swal.fire("Error", "Ocurrió un error al validar si está incluido al borrador", "error");
            });

        }

        function showRadButton(){
            $('#buttonRadExp').show();
        }


        function confirmExcluirExpd(estatus, expediente, idtipologia) {
//@formatter:off
            Swal.fire({
                title: "Alerta",
                text: "Seguro que quiere excluir el borrador de este expediente?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Si, excluir",
                cancelButtonText: "Cancelar",
                closeOnConfirm: true,
                closeOnCancel: true
            }).then((result) => {
                if (result.value) {
                delBorraExped(expediente, idtipologia);
            } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
            ) {

            }
        });
            //@formatter:on

        }


        function cerrarModalExpediente() {
            //cierra el modal de expediente, pero antes valida si ya se selecciono una tipologia  si ya incluyo un expediente
            if (tipologiaselected == "" && yaincluyouno == true) {

                var encontroincluido = false;
                for (var i = 0; i < Object.keys(expedientestabla).length; i++) {
                    if (expedientestabla[i]['incluiExpe'] == "incluido") {
                        encontroincluido = true;
                    }
                }

                if (encontroincluido == true) {
                    Swal.fire("Alerta", "Debe seleccionar una tipología", "warning");
                } else {
                    $("#modal_expediente").modal('hide');
                }

            } else {
                $("#modal_expediente").modal('hide');
            }
        }

        function delBorraExped(estatus, expediente) { //excluye un borrador de un expediente


            $.ajax({
                url: "generalborrador.php",
                method: "POST",
                data: {
                    borrador_id: borrador_id, expediente: expediente,
                    function_call: "excluirexpedienteborra"
                },
                dataType: "json",
                success: function (response) {
                    validaExpiro(response);
                    if (response.success) {
                        Swal.fire(response.success, "", "success");

                        for (var i = 0; i < Object.keys(expedientestabla).length; i++) {
                            if (expedientestabla[i]['NUMERO_EXPEDIENTE'] == expediente) {
                                expedientestabla[i]['incluiExpe'] = "no incluido"
                            }
                        }

                        //defino la tabla nuevaente y vuelvo a cargar los datos de la tabla
                        definirTablaExpediente();
                        var entablatemp = expedientestabla;
                        expedientestabla = null;
                        expedientestabla = new Array();
                        recibeExpedientes(response.misexped);
                        recibeExpedientes(entablatemp);
                        buscarExpedRadAso();

                    } else {
                        Swal.fire("Error", response.error, "error");
                    }

                }
            }).done(function () {

            }).fail(function (error) {
                Swal.fire("Error", "Ocurrio un error al incluir el expediente al borrador", "error");
            });


        }

        function addBorraExped(evento, estatus, expediente) {

            evento.preventDefault();
            if (estatus == "Cerrado") {
                Swal.fire("Espere", "Expediente Cerrado - Debe solicitar soporte para su apertura o la creación de " +
                    "un nuevo expediente", "warning");
            } else {

                $.ajax({
                    url: "generalborrador.php",
                    method: "POST",
                    data: {
                        borrador_id: borrador_id, expediente: expediente, idtipologia: tipologiaselected,
                        function_call: "addexpedienteborra"
                    },
                    dataType: "json",
                    success: function (response) {

                        validaExpiro(response);
                        if (response.success) {

                            if(response.yaestabaincluido!==undefined){
                                if (tipologiaselected != "") {
                                    alertModal('Incluido en expediente',response.success, 'success', '10000')
                                } else {
                                    /**
                                     * Entra aqui si no ha seleccionado la tipologia
                                     */
                                    alertModal('Incluido en expediente', 'Ahora elija el Tipo Documental', 'success', '15000')

                                }
                            }else{
                                alertModal('Incluido en expediente', response.success, 'success', '10000')

                            }

                            yaincluyouno = true;

                            $("#divinputtipologia").css('display', 'block');

                            //defino la tabla nuevaente y vuelvo a cargar los datos de la tabla
                            definirTablaExpediente();
                            var entablatemp = expedientestabla;
                            expedientestabla = new Array();
                            recibeExpedientes(response.misexped);
                            recibeExpedientes(entablatemp);
                            buscarExpedRadAso();

                            setTimeout(function () {
                                $("#inputtipologia").css('border', '1px solid red !important');
                                $("#inputtipologia").css('box-shadow', '0 0 3px red !important');
                                $("#inputtipologia").css('margin', '10px !important');
                                $("#divpreviotipologia").pulsate({  color: "#ff0000",
                                    reach: 20,                              // how far the pulse goes in px
                                    speed: 1000,                            // how long one pulse takes in ms
                                    pause: 1000,                               // how long the pause between pulses is in ms
                                    glow: true,                             // if the glow should be shown too
                                    repeat: 3,                           // will repeat forever if true, if given a number will repeat for that many times
                                    onHover: false                          // if true only pulsate if user hovers over the element
                                    });
                            }, 500);


                        } else {
                            Swal.fire("Error", response.error, "error");
                        }

                    }
                }).done(function () {

                }).fail(function (error) {
                    Swal.fire("Error", "Ocurrio un error al incluir el expediente al borrador", "error");
                });
            }

        }


        //modal que aparece antes de radicad
        function modalBeforeRadicar() {

            $('#modal_before_radicar').modal('show');
            $("#folios_comunicacion").val('1');
            $("#folios_anexos").val('0');
            $("#desc_anexos").val('');

            //si los camps e folios que estannen  la raiz del modal  del borrador,
            //estan distintoss de vacios, se los coloco en este modal
            if ($("#folios_comunicacion_raiz").val() != "") {
                $("#folios_comunicacion").val($("#folios_comunicacion_raiz").val());
            }
            if ($("#folios_anexos_raiz").val() != "") {
                $("#folios_anexos").val($("#folios_anexos_raiz").val());
            }

            $("#desc_anexos").val($("#desc_anexos_raiz").val());


            //esto es para el swiche de seguridad
            $('.bootstrap-switch-label').addClass('popovers');
            $('.bootstrap-switch-label').attr('data-original-title', 'Restringir acceso al documento')
            $('.bootstrap-switch-label').attr('data-content', 'Solo tendra acceso al documento el usuario actual ' +
                'y usuarios o dependencias autorizadas!')
            $('.bootstrap-switch-label').attr('data-placement', 'top')
            $('.bootstrap-switch-label').attr('data-trigger', 'hover')
            $('.bootstrap-switch-label').attr('data-container', 'body')
            $('.bootstrap-switch-label').popover()
        }

        function generarRadicado(e) {
            e.preventDefault();

            $('#btn_generarradicado').prop('disabled', true);


            Swal.fire("Espere", "Por favor espere", "warning");
            fn60sec();

            var editor = "";
            if ($('#div_edit').is(':visible')) //el div donde esta el editor froala
            {
                editor = $('#froalaeditor').froalaEditor('html.get');
            }

            if (optSelectedNewBorrador != INTERNA_SIN_PLANTILLA) {

                if ($('#div_edit').is(':visible')) //el div donde esta el editor froala
                {
                    if ($('#froalaeditor').froalaEditor('html.get') == "") {
                        swal.close();
                        Swal.fire("Alerta", "Debe ingresar contenido en el editor", "warning");
                        $('#btn_generarradicado').prop('disabled', false);
                    }
                }
            }


            $.ajax({
                url: "generalborrador.php",
                method: "POST",
                data: {
                    borrador_id: borrador_id, dest: $('#dest').val(), persona: $('#persona').val(),
                    cargo: $('#cargo').val(), asunto: $('#asunto').val(), edit: editor,
                    radicadoactual: $("#radicadoactual").val(), folios_comunicacion: $("#folios_comunicacion").val(),
                    folios_anexos: $("#folios_anexos").val(), desc_anexos: $("#desc_anexos").val(),
                    function_call: "radicar", 'seguridad_radicado': seguridad_radicado
                },
                dataType: "json",
                success: function (response) {
                    validaExpiro(response);
                    if (response.success) {
                        alertModal('Radicado', 'Borrador radicado con número ' + response.success, 'success', '5000')
                        Swal.fire({
                            title: "Radicar",
                            text: "El borrador será eliminado",
                            type: "success",
                            showCancelButton: false,
                            showConfirmButton: false
                        });
                        setTimeout(function () {

                            //si radicadoactual=="" qiere decir que estamos desde la ventana de borraores
                            //si no es asi, redirecciono a la funcion del parent, que lo que hace es llamar a la ventana doucmentos del radicado padre
                            if (radicadoactual == "") {
                                window.location.href = "../../verradicado.php?verrad=" + response.success + "&" + phpsession + "&menu_ver_tmp=2"
                            } else {
                                parent.redireccionaracarpeta();
                            }

                        }, 5000);
                    } else {
                        $('#btn_generarradicado').prop('disabled', false);
                        Swal.fire("Error", response.error, "error");
                    }

                }
            }).done(function () {

            }).fail(function (error) {
                $('#btn_generarradicado').prop('disabled', false);
                Swal.fire("Error", "No se pudo radicar este borrador", "error");
            });
        }

        //cuando presionan el boton de Radicar en la raiz del modal de borrador
        function formRadicar() {

            if (borradorActual.ID_USUARIO_ACTUAL == codusuario) {

                if (optSelectedNewBorrador == REPORTE4) {
                    if (DiaHoraMin
                        > ConfiguracionesService.getDayFromDate(configsHasValue['FECHA_RADICACION_REPORTE4']) ) {

                        var DiaHoras=  ConfiguracionesService.getDayFromDate(configsHasValue['FECHA_RADICACION_REPORTE4'])
                        Swal.fire("Fecha máxima para radicar:" , 'Día '+DiaHoras[0]+' a las '+DiaHoras[1], "warning");
                        return false;
                    }
                }

                //debe al menos revisarlo, si la configuracion siguiente esta en NO
                if (configsHasValue['APROBAR_OBLIGATORIO_RADICAR'] == "NO" &&
                    (almenosrevisado == false && almenosaprobado == false)) {
                    alertModal('Alerta', 'Es necesario Revisar o Aprobar el borrador para poder Radicar', 'warning', '5000')
                    return false;
                }

                //debe tener siempre al menos un firmante
                if (Object.keys(firmantesselected).length < 1) {
                    alertModal('Sin firmantes', 'Debe seleccionar al menos un firmante', 'warning', '5000')
                    return false;
                }

                var tipo_radicado = borradorActual.TIPO_RADICADO


                /**
                 * busco el nombre del tipo de radicado al cual pertenece el borrador
                 */
                var nombre_tipo_rad_borrador = '';
                $.each(tiposDeRadicados, function (key1, value1) {

                    if (value1 == tipo_radicado) {
                        nombre_tipo_rad_borrador = nombresTiposRadicados[key1]
                    }
                });

                /**
                 * valido que el usuario tenga permiso para radicar
                 * Recorro los tipos de radicados que estan disponibles en orfeo (tiposDeRadicados),
                 * luego en permiso_para_radicar, me viene el arreglo con el indice del tipo de radicado, con sus permisos.
                 * 0= no tiene permiso, 3= si tiene permiso.
                 */

                var consiguiopermiso = false;
                if (Object.keys(tiposDeRadicados).length > 0) {

                    for (var i = 0; i < Object.keys(tiposDeRadicados).length; i++) {

                        if (tiposDeRadicados[i] == tipo_radicado) {

                            $.each(permiso_para_radicar, function (key, value) {

                                if (key == tipo_radicado && value == '3') {
                                    consiguiopermiso = true;
                                }
                            });
                        }
                    }

                    if (consiguiopermiso == false) {
                        alertModal('Alerta', 'No tiene permiso para Radicar ' + nombre_tipo_rad_borrador, 'warning', '5000')
                        return false;
                    }
                } else {
                    alertModal('Alerta', 'No tiene permiso para Radicar ' + nombre_tipo_rad_borrador, 'warning', '5000')
                    return false;
                }


                if (tipo_radicado == "1") {
                    tipo_radicado = "Radicado Salida";
                } else {
                    tipo_radicado = "Radicado Interno";
                }

                $.ajax({
                    url: "generalborrador.php",
                    method: "POST",
                    data: {
                        borrador_id: borrador_id,
                        function_call: "getExpedBorrador"
                    },
                    dataType: "json",
                    async: false,
                    success: function (response) {
                        validaExpiro(response);
                        if (response) {

                            if (configsHasValue['EXPEDIENTE_OBLIGATORIO_RADICAR'] == "NO" ||
                                (configsHasValue['EXPEDIENTE_OBLIGATORIO_RADICAR'] == "SI" && response.length > 0)) {
                                //aqui valido si antes de radicar, ya todos los expedientes tienen una tipologia
                                var retornoMisExpe = buscarMisExped(false);
                                if (configsHasValue['EXPEDIENTE_OBLIGATORIO_RADICAR'] == "SI" && response.length > 0 &&
                                    validaSeleccionoTipologia(retornoMisExpe) == false) {
                                    modalExpedienteBorrad('NOCREAREXPEDSINRAD');
                                    Swal.fire("Elija el Tipo Documental para poder radicar", "", "warning");
                                } else {
                                    $("#titlemodalbeforerad").html("")
                                    $("#titlemodalbeforerad").html("<strong>Se generará un radicado tipo: " + tipo_radicado + "<strong>")


                                    //Al crear un borrador por cualquiera de las opciones, excepto por “Interna | sin plantilla”
                                    // validar al momento de RADICAR, que tenga
                                    // escrito algo en el editor o que haya subido una plantilla, de lo contrario No permitir radicar y alertar.
                                    //si es distinta de INTERNA_SIN_PLANTILLA, entones guardo los datos para tener los datos actualizados

                                    fn60sec();
                                    if (optSelectedNewBorrador == INTERNA_COMUNICACION ||
                                        optSelectedNewBorrador == SALIDA_OFICIO) {

                                        if (editoroplantilla == 'EditorWeb') {
                                            var editor = $('#froalaeditor').froalaEditor('html.get');
                                            if (editor == "") {
                                                Swal.fire("No ha escrito nada en el editor o subido una plantilla", "", "warning");
                                                return false;
                                            } else if (editor.length < 150) {
                                                Swal.fire("El contenido del Editor Web no corresponde a un documento", "", "warning");
                                                return false;
                                            }
                                        } else {
                                            var documentoruta = borradorActual.DOCUMENTO_RUTA;
                                            if (documentoruta == null || documentoruta == 'null' || documentoruta == '') {
                                                Swal.fire("No ha escrito nada en el editor o subido una plantilla", "", "warning");
                                                return false;
                                            } else if (fileExistPlant == false) {
                                                Swal.fire("Formato o plantilla del borrador no existe. Verificar.", "", "warning");
                                                return false;
                                            }
                                        }
                                    }

                                    if (optSelectedNewBorrador == REPORTE4) {

                                        if (editoroplantilla == 'EditorWeb') {
                                            Swal.fire("No ha subido una plantilla", "", "warning");
                                            return false;
                                        }
                                        var documentoruta = borradorActual.DOCUMENTO_RUTA;
                                        if (documentoruta == null || documentoruta == 'null' || documentoruta == '') {
                                            Swal.fire("No ha subido una plantilla", "", "warning");
                                            return false;
                                        } else if (fileExistPlant == false) {
                                            Swal.fire("Formato o plantilla del borrador no existe. Verificar.", "", "warning");
                                            return false;
                                        }

                                    }

                                    if (optSelectedNewBorrador == INTERNA_CON_PLANTILLA || customBorradorSel == true) {

                                        if (editoroplantilla == 'EditorWeb') {
                                            var editor = $('#froalaeditor').froalaEditor('html.get');
                                            if (editor == "") {
                                                Swal.fire("No ha subido una plantilla", "", "warning");
                                                return false;
                                            }
                                        } else {
                                            var documentoruta = borradorActual.DOCUMENTO_RUTA;
                                            if (documentoruta == null || documentoruta == 'null' || documentoruta == '') {
                                                Swal.fire("No ha subido una plantilla", "", "warning");
                                                return false;
                                            } else if (fileExistPlant == false) {
                                                Swal.fire("Formato o plantilla del borrador no existe. Verificar.", "", "warning");
                                                return false;
                                            }
                                        }
                                    }


                                    modalBeforeRadicar();
                                }

                            } else {
//@formatter:off
                                Swal.fire({
                                    title: "Alerta",
                                    text: "No ha sido definido el expediente en el cual se incluirá el radicado",
                                    type: "warning",
                                    showCancelButton: true,
                                    confirmButtonText: "Ir a Expedientes",
                                    cancelButtonText: "Cerrar",
                                    closeOnConfirm: true,
                                    closeOnCancel: true
                                }).then((result) => {
                                    if (result.value) {
                                    radicaInExp = true;
                                    mostrarRadbutton = 'SI';
                                    modalExpedienteBorrad('NOCREAREXPEDSINRAD');
                                } else if (
                                    /* Read more about handling dismissals below */
                                    result.dismiss === Swal.DismissReason.cancel
                                ) {

                                }
                            });
                                //@formatter:on

                            }

                        } else {
                            Swal.fire("Error", "Ocurrio un error al buscar los expedientes incluidos", "error");
                        }

                    }
                }).done(function () {

                }).fail(function (error) {
                    Swal.fire("Error", "Error al buscar los expedientes", "error");
                });


            } else {
                Swal.fire("Error", "No puede radicar este borrador", "error");
            }
        }

        function guardaranexo() {
            console.log('guardar anexo');
            var formData = new FormData($("#formanexo")[0]);
            var contentType = false;
            console.log($("#formanexo")[0]);
            $.ajax({
                url: "guardaranexo.php",
                type: 'POST',
                data: formData,
                dataType: 'json',
                cache: false,
                contentType: contentType,
                processData: false,
                success: function (response) {

                    validaExpiro(response);

                    console.log('response', response)
                    if (response.success && response.success == true) {
                        anexosBorrador = response.anexos
                        Swal.fire("Guardar", "El Anexo fué guardado exitosamente", "success");

                        $("#modalanadiranexo").modal('hide');
                    } else {
                        Swal.fire("Error", response.error, "error");

                    }

                    mostraranexos(response.anexos);

                },
                error: function (response) {

                    Swal.fire("Error", "Ha ocurrido un error", "error");
                }
            });
        }

        //cuando se presiona sobre el boton de exportar
        function exportar(ruta, tipodoc) {

            if (ruta == null || ruta == 'null') {
                return false;
            }

            if (borrador_id != "") {

                fn60sec();

                if (ruta != "" && (tipodoc == "pdf" || tipodoc == "PDF")) {

                    /* if (navegadorActual == "isFirefox") {
                        window.location.href = "exportar.php?borrador_id=" + borrador_id + "&navegador=" + navegadorActual + "&exportar=" + tipodoc
                    } else {
                        showModalVistaPrevia(ruta, tipodoc, 'modal_vista_previa', 'header_vista_previa',
                            'modal_body_vista_previa', 'embed');
                    }*/

                    showModalVistaPrevia(ruta, tipodoc, 'modal_vista_previa', 'header_vista_previa',
                        'modal_body_vista_previa', 'embed');
                    return true;

                }

                if ($('#div_edit').is(':visible')) {


                    if (tipodoc != "pdf" && tipodoc != "PDF") {
                        console.log([borrador_id,navegadorActual,tipodoc]);
                        window.location.href = "exportar.php?borrador_id=" + borrador_id + "&navegador=" + navegadorActual + "&exportar=" + tipodoc
                    } else {
                        mostrarVistaPrevia("exportar.php?borrador_id=" + borrador_id + "&navegador=" + navegadorActual + "&exportar=" + tipodoc, tipodoc);
                    }


                } else {
                    if (tipodoc != "pdf" && tipodoc != "PDF") {
                        console.log([borrador_id,navegadorActual,tipodoc]);
                        window.location.href = "exportar.php?borrador_id=" + borrador_id + "&navegador=" + navegadorActual + "&exportar=" + tipodoc

                    } else {
                        mostrarVistaPrevia("exportar.php?borrador_id=" + borrador_id + "&navegador=" + navegadorActual);
                    }


                }
            } else {
                Swal.fire("Mensaje", "Primero debe guardar el borrador", "warning");
            }
        }


        //esto se ejecuta cuando se tiene el editor y se quiere ver la vista previa
        function mostrarVistaPrevia(url, tipodoc='docx') {

            $.ajax({
                url: url,
                method: "GET",
                dataType: "json",
                async: false,
                success: function (response) {
                    if (response.success) {
                        showModalVistaPrevia(response.success, tipodoc, 'modal_vista_previa', 'header_vista_previa',
                            'modal_body_vista_previa', 'embed')
                    } else {

                        Swal.fire("Error", "Ha ocurrido un error", "error");
                    }
                }
            }).done(function () {
            }).fail(function (error) {
                Swal.fire("Error", "Ha ocurrido un error", "error");
            });


        }


        function ajaxGetBorrador(idborrador) {
            var retorno = false;
            var response = '';
            $.ajax({
                url: "getborrador.php",
                method: "POST",
                data: {borrador_id: idborrador},
                dataType: "json",
                async: false,
                success: function (data) {
                    response = data;
                    if (data.success) {
                        if( typeof(response.success.borrador[0].historico) != 'undefined'){
                            var historicoB = response.success.borrador[0].historico;
                            $.each(historicoB, function (ind, elem) {
                                if(elem.SGD_TTR_CODIGO == '81' && elem.ID_USUARIO_ORIGEN == codusuario && elem.DEPE_CODI_ORIGEN == dependencia_user_log) {
                                    BorradorRevisado = true;
                                }
                            });

                        }
                    } else {
                        Swal.fire("Error", "Ha ocurrido un error", "error");
                    }
                }
            }).done(function () {
            }).fail(function (error) {
                Swal.fire("Error", "Ha ocurrido un error", "error");
            });

            return new Array(retorno, response);
        }

        function resetVar(){
            $('#buttonRadExp').hide();
            mostrarRadbutton = 'NO';
        }

        function buscarUserEnviar(){
            //con esto llamo a la consulta
            $.ajax({
                url: "generalborrador.php?function_call=get_dropdown_users&dependencia="+dependencia_user_log+"&borrador="+borrador_id,
                type: 'GET',
                dataType: 'json',
                async: false,
                success: function (response) {
                    if (response.users.length > 0){
                        console.log(response.users);
                        addUsersToDropdownEnviar('',response.users);
                    }

                },error: function (response) {

                }

            });
        }

        function getBorrador(idborrador, tipo_borrador_dest=false) {
            var retorno = false;
            var test = JSON.parse(customBorradorSession);
            for (var i = 0; i < Object.keys(test).length; i++) {
                if (test[i].tipo_identificador == tipo_borrador_dest) {
                    customBorradorSel = true;
                    datosCustomBorrador = test[i]; 
                }

            }

            $("#" + idborrador).focus();
            $("#" + idborrador).select();

            if (tipo_borrador_dest != false) {
                optSelectedNewBorrador = tipo_borrador_dest

            }

            resetForm();
            resetVar();
            borrador_id = idborrador;
            buscarUserEnviar();
            //ejecuto el ajax que busca los datos
            retorno = ajaxGetBorrador(idborrador);
            console.log('pa ve');
            console.log(retorno);
            if (retorno[1] != undefined && retorno[1].success != undefined) {

                $('#rowtitulomemo').html('');
                if (retorno[1].success.borrador[0].TIPO_RADICADO == '5'){
                    customBorradorSel = true;
                } 
                mostrarDatosBorrador(retorno[1]);
                $('#all_modal').modal({show: true, keyboard: false, backdrop: 'static'});
                retorno = true;
            } else {
                retorno = false;
            }

            return retorno;

        }

        //cuando presiono el boton borrar en el cuadro gris donde se ve el documento ruta
        function btnBorraPlantilla() {

            borrarDocumentoRuta = true;
            editoroplantilla = "EditorWeb";
            $("#editoroplantilla").val('EditorWeb');

            if (optSelectedNewBorrador == INTERNA_SIN_PLANTILLA) {
                editoroplantilla = "SinPlantilla";
                $("#editoroplantilla").val('SinPlantilla');
            }
            $("#primerguardar").click();
        }

        function organizarMostrarDivEditor(link) {
            link = 'link1';
            var nombredoc = "";
            var rutadoc = "";

            if (borradorActual.DOCUMENTO_RUTA != undefined && borradorActual.DOCUMENTO_RUTA != '' &&
                borradorActual.DOCUMENTO_RUTA != null && borradorActual.DOCUMENTO_RUTA != 'NULL') {
                var documentoruta = borradorActual.DOCUMENTO_RUTA;
                //Splitting it with : as the separator
                var myarr = '';

                if (documentoruta != null && documentoruta != 'null') {
                    var myarr = documentoruta.split("/");
                    nombredoc = myarr[myarr.length - 1];
                } else {
                    nombredoc = '';
                }

                rutadoc = borradorActual.DOCUMENTO_RUTA;
                link = 'link2'
            }
            console.log('link', link)
            console.log('borradorActual', borradorActual)
            mostrarDivEditor(link, rutadoc, nombredoc);

            if (link == 'link2') {
                $("#div_edit").css("display", "none");
                $("#content").css("display", "none");
                $("#div_modificarborrar").css("display", "block");

                $("#div_cargardoc").css("display", "none");
                $("#div_loader").css("display", "none");
            } else {

                if (optSelectedNewBorrador == INTERNA_CON_PLANTILLA
                    ||
                    optSelectedNewBorrador == INTERNA_SIN_PLANTILLA
                    ||
                    optSelectedNewBorrador == REPORTE4 || customBorradorSel == true ) {
                    $("#div_edit").css("display", "none");
                    $("#content").css("display", "none");
                    $("#div_modificarborrar").css("display", "block");
                    $(".div_btn_doc_and_pdf").css("display", "none");


                } else {

                    $("#div_edit").css("display", "block");
                    $("#content").css("display", "block");
                    $("#div_modificarborrar").css("display", "none");
                }


            }


        }

        /**
         * abre en una ventana nueva, la url que le coloco para descargar plantilla en reporte4
         * @param url
         */
        function descFormatoReporte4(url) {
            var win = window.open(url, '_blank');
            win.focus();

        }
        function descFormatoPlantilla(url) {
            var win = window.open(url, '_blank');
            win.focus(); 
        }

        //muestra el html donde estan los div de exportar doc y pdf
        function mostrarDivEditor(armar, rutadoc="", nombrearchivo='') {
            htmldiveditor = "";
            $("#div_subirplantilla").html('')
            //si armar ==link1 quiere decir que no ha subido plantillas de documentos,
            //si es link2 es porque subio un archvio de plantilla o lo va a subir, y por lo tanto mostramos
            //el boton de descargarlos


            if (armar == "link1") {


                $("#div_cargardoc").css("display", "none");
                $("#div_loader").css("display", "none");
                $("#div_edit").css("display", "block");

                var titlesubirdoc = "Subir plantilla de Orfeo (No se usará el editor web y se después radicar, se debe Re-generar)";

                if (optSelectedNewBorrador == INTERNA_CON_PLANTILLA || customBorradorSel == true) {
                    titlesubirdoc = "Subir .DOCX .ODT ó .ODS. Después de radicar, debe Re-generar"
                }

                if (optSelectedNewBorrador == INTERNA_SIN_PLANTILLA) {
                    titlesubirdoc = "";
                }

                if (optSelectedNewBorrador == SALIDA_OFICIO) {
                    titlesubirdoc = "";
                }



                //
                var displaydoc = borradorremove == 0 ? 'block' : 'none' //cuando $borradorremove=0;  mostrar .DOCX en vista previa, de lo contrario No.

                var rutavacia = "''" //aqui siempre la ruta es vacia
                htmldiveditor += '<div class="col-md-12">' +
                    ' <div class="input-icon right " style="background:#E1E5EC; padding:20px; 5px;height: 70px;"> ' +
                    '<div  style="text-align:left; float:left" >';

                if (FORMATO_REPORTE4 != undefined && FORMATO_REPORTE4 != '' && FORMATO_REPORTE4 != null
                    && FORMATO_REPORTE4 != "NULL" && optSelectedNewBorrador == REPORTE4) {

                    var is_link = FORMATO_REPORTE4;
                    var url = base_url + FORMATO_REPORTE4;

                    if (is_link.substr(0, 5) == 'https' || is_link.substr(0, 4) == 'http' || is_link.substr(0, 3) == 'www') {
                        url = FORMATO_REPORTE4
                    }
                    url = "'" + url + "'"

                    htmldiveditor += '<a  class="#" onclick="descFormatoReporte4(' + url + ')" >Descargar Formato <img id="imgupload" style="min-height: 40px;max-height:50px;  min-width: 50px;max-width: 63px; display: ' + displaydoc + '" src="' + rutaimg + 'DOCXdown.png"></a> ';
                }
                
                if ( customBorradorSel == true) {
                    console.log('substr');
                    console.log(datosCustomBorrador);
                    var is_link = datosCustomBorrador.formato_radicado;
                    var url = base_url + datosCustomBorrador.formato_radicado;

                    if (is_link.substr(0, 5) == 'https' || is_link.substr(0, 4) == 'http' || is_link.substr(0, 3) == 'www') {
                        url = datosCustomBorrador.formato_radicado;
                    }
                    url = "'" + url + "'"

                    htmldiveditor += '<a  class="#" onclick="descFormatoPlantilla(' + url + ')" >Descargar Formato <img id="imgupload" style="min-height: 40px;max-height:50px;  min-width: 50px;max-width: 63px; display: ' + displaydoc + '" src="' + rutaimg + 'DOCXdown.png"></a> ';
                }

                htmldiveditor += '<span class="div_btn_doc_and_pdf" >Vista Previa &nbsp;&nbsp;</span>' +
                    '<a href="#" class="div_btn_doc_and_pdf" onclick="exportar(' + rutavacia + ',' + doc + ')"  title="Descargar documento en formato Word">' +
                    '<img id="imgupload" style="min-height: 40px;max-height:50px;  min-width: 50px;max-width: 63px; display: ' + displaydoc + '" src="' + rutaimg + 'DOCXdown.png">' +
                    '</a> ' +
                    '<a href="#" class="div_btn_doc_and_pdf" onclick="exportar(' + rutavacia + ',' + pdf + ')" title="Descargar documento en formato PDF">' +
                    '<img id="imgupload" style="padding-left:10px; min-height: 40px;max-height:50px; min-width: 70px;max-width: 70px; " src="' + rutaimg + 'PDF.png"> ' +
                    '</a> ' +
                    '</div>' +
                    '<div style="text-align:right; float:right">' +
                    '<span >';
                if (optSelectedNewBorrador == REPORTE4) {

                    var strin_acept = '';
                    if (Object.keys(REPORTE4_EXTENSIONES).length > 0) {
                        for (var i = 0; i < Object.keys(REPORTE4_EXTENSIONES).length; i++) {
                            if (REPORTE4_EXTENSIONES[i] == 'docx') {
                                strin_acept += ' Word,'
                            }
                            if (REPORTE4_EXTENSIONES[i] == 'pdf' || REPORTE4_EXTENSIONES[i] == 'PDF') {
                                strin_acept += ' PDF,'
                            }

                            if (REPORTE4_EXTENSIONES[i] == 'odt' || REPORTE4_EXTENSIONES[i] == 'ODT') {
                                strin_acept += ' ODT,'
                            }
                        }
                    }

                    if (strin_acept != '') {
                        htmldiveditor += 'Subir Formato (' + strin_acept.substr(0, strin_acept.length - 1) + ')';
                    }
                } else {

                    if (optSelectedNewBorrador == SALIDA_OFICIO) {
                        var ext = JSON.parse(configsHasValue['EXTENSIONES_RADICADO_1']);
                        console.log(ext);
                        htmldiveditor += 'Subir ';
                        for (let i = 0; i < ext.length; i++) {
                            if (i > 0){
                                htmldiveditor += ' ó ';
                            }
                            htmldiveditor += ext[i];
                        }

                    }else{
                        htmldiveditor += 'Subir Plantilla';
                    }

                }
                htmldiveditor += '</span> ' +
                    '<a href="#" onclick="confirmarcargadoc()" title="' + titlesubirdoc + '" ' + '>';


                if (optSelectedNewBorrador == INTERNA_CON_PLANTILLA || customBorradorSel == true) {

                    htmldiveditor += '<img id="imgupload" style="min-height: 40px;max-height:50px; min-width: 40px;max-width: 50px; " ';
                    htmldiveditor += 'src="' + rutaimg + 'PLANTILLAup.png"> ';
                } else if (optSelectedNewBorrador == REPORTE4) {

                    if (Object.keys(REPORTE4_EXTENSIONES).length > 0) {
                        for (var i = 0; i < Object.keys(REPORTE4_EXTENSIONES).length; i++) {
                            if (REPORTE4_EXTENSIONES[i] == 'docx') {
                                htmldiveditor += '<img id="imgupload" style="min-height: 40px;max-height:50px; min-width: 40px;max-width: 50px; " ';
                                htmldiveditor += 'src="' + rutaimg + 'DOCXup.png"> ';
                            }
                            if (REPORTE4_EXTENSIONES[i] == 'pdf' || REPORTE4_EXTENSIONES[i] == 'PDF') {
                                htmldiveditor += '<img id="imgupload" style="min-height: 40px;max-height:50px; min-width: 40px;max-width: 50px;' +
                                    ' " src="' + rutaimg + 'PDFup.png"> ';
                            }
                        }
                    }


                } else {
                    if (optSelectedNewBorrador == SALIDA_OFICIO) {
                        ext = JSON.parse(configsHasValue['EXTENSIONES_RADICADO_1']);
                        for (let i = 0; i < ext.length; i++) {
                            if(ext[i] == 'doc' || ext[i] == 'docx'){
                                htmldiveditor += ' <img id="imgupload" style="min-height: 40px;max-height:50px; min-width: 40px;max-width: 50px; " ';
                                htmldiveditor += 'src="' + rutaimg + 'DOCXup.png"> ';
                            }else{
                                htmldiveditor += ' <img id="imgupload" style="min-height: 40px;max-height:50px; min-width: 40px;max-width: 50px; " ';
                                htmldiveditor += 'src="' + rutaimg + 'PDF.png"> ';
                            }
                        }


                    }else{
                    htmldiveditor += '<img id="imgupload" style="min-height: 40px;max-height:50px; min-width: 40px;max-width: 50px; " ';
                    htmldiveditor += 'src="' + rutaimg + 'DOCXup.png"> ';
                    }
                }


                htmldiveditor += '</a> </div> </div> </div>';

                if (optSelectedNewBorrador == INTERNA_SIN_PLANTILLA) {

                    htmldiveditor = '<div class="col-md-12">' +
                        ' <div class="input-icon right " style="background:#E1E5EC; padding:20px; 5px;height: 70px;">' +
                        ' <div class="div_btn_doc_and_pdf"' +
                        ' style="text-align:left; float:left"  > ' +
                        'Vista Previa &nbsp;&nbsp;' +
                        '<a href="#" onclick="exportar(' + rutavacia + ',' + doc + ')"  title="Descargar documento en formato Word"><img id="imgupload" ' +
                        'style="min-height: 40px;max-height:50px;  min-width: 50px;max-width: 63px; display: display: ' + displaydoc + '" src="' + rutaimg + 'DOCXdown.png"></a> ' +
                        '<a href="#" onclick="exportar(' + rutavacia + ',' + pdf + ')" title="Descargar documento en formato PDF"><img id="imgupload"' +
                        'style="padding-left:10px; min-height: 40px;max-height:50px; min-width: 70px;max-width: 70px; "' +
                        'src="' + rutaimg + 'PDF.png"> </a> </div> <div style="text-align:right; float:right"><span >' +
                        '(Opcional) Subir documento Principal </span> <a href="#" onclick="confirmarcargadoc()" ' +
                        'title="' + titlesubirdoc + '" ' + '> ' +
                        '<img id="imgupload" style="min-height: 40px;max-height:50px; min-width: 40px;max-width: 50px; " src="' + rutaimg + 'PDFup.png"> ' +
                        '</a> </div> </div> </div>';
                }


            } else {

                //entra aqui cuando ya tiene pantilla o cuando se va a subir la plantilla

                var href = "#";
                if (rutadoc != "") {
                    href = rutadoc;
                }
                href = "'" + href + "'"
                var tipoextension = "'" + borradorActual.EXTENSION_RUTA_FILE + "'"
                htmldiveditor = '<div class="col-md-12"> ' +
                    '<div class="input-icon right" style="' +
                    'background:#E1E5EC;; padding:20px; 5px;height: 70px;"> ' +
                    ' <div style="text-align:left; float:left">';


                var imgtoshow = ""; //variable que muestra la imagen dependiendo de la extension del archivo si es que hay
                var textodespuesimg = "Plantilla"
                if (optSelectedNewBorrador == INTERNA_SIN_PLANTILLA) {
                    //sin plantilla solo muestra pdf por los momentos
                    textodespuesimg = "Documento Principal"
                    if (rutadoc != "") {
                        imgtoshow = "PDFdown.png";
                    } else {
                        imgtoshow = "PDFup.png";
                    }
                    htmldiveditor += armarImgCargarPlantilla(href, tipoextension, rutaimg, imgtoshow) + textodespuesimg + ' ';
                } else {
                    textodespuesimg = "Plantilla"
                    console.log('imagen que veo '+ tipoextension);
                    if (rutadoc != "" && rutadoc != null && rutadoc != "null") {
                        //si la ruta es distinta de vacio, entonces todos los conos son de down
                        //por defecto coloco docx
                        imgtoshow = "DOCXdown.png";
                        if (optSelectedNewBorrador == INTERNA_CON_PLANTILLA || customBorradorSel == true) {
                            //INTERNA_CON_PLANTILLA puede tener formatos docx, odt y ods
                            if (borradorActual.EXTENSION_RUTA_FILE == "odt" || borradorActual.EXTENSION_RUTA_FILE == "ODT") {
                                imgtoshow = "ODTdown.png";
                            }
                            if (borradorActual.EXTENSION_RUTA_FILE == "ods" || borradorActual.EXTENSION_RUTA_FILE == "ODS") {
                                imgtoshow = "ODSdown.png";
                            }
                        }

                        if (optSelectedNewBorrador == INTERNA_SIN_PLANTILLA) {
                            imgtoshow = "PDFdown.png";
                        }

                        if (optSelectedNewBorrador == REPORTE4) {
                            /**
                             * se valida con comillas simples, ya que arriba se le concatenó las comillas dobles
                             */
                            if (tipoextension == "'pdf'" || tipoextension == "'PDF'") {
                                imgtoshow = "PDFdown.png";
                            }
                        }
                        if (optSelectedNewBorrador == SALIDA_OFICIO) {
                            if (tipoextension == "'pdf'" || tipoextension == "'PDF'") {
                                textodespuesimg = "Documento Principal";
                                imgtoshow = "PDFdown.png";
                            }
                        }


                        htmldiveditor += armarImgCargarPlantilla(href, tipoextension, rutaimg, imgtoshow) + textodespuesimg + ' ';

                    } else {

                        imgtoshow = "DOCX.png";
                        if (optSelectedNewBorrador == INTERNA_SIN_PLANTILLA) {
                            imgtoshow = "PDF.png";
                            htmldiveditor += armarImgCargarPlantilla(href, tipoextension, rutaimg, imgtoshow);
                        }

                        if (optSelectedNewBorrador == INTERNA_CON_PLANTILLA || customBorradorSel == true) {
                            imgtoshow = "PLANTILLAup.png";
                            htmldiveditor += armarImgCargarPlantilla(href, tipoextension, rutaimg, imgtoshow);
                        }

                        if (optSelectedNewBorrador == SALIDA_OFICIO) {

                            htmldiveditor += armarImgCargarPlantilla(href, tipoextension, rutaimg, imgtoshow);
                        }


                        if (optSelectedNewBorrador == REPORTE4) {

                            if (Object.keys(REPORTE4_EXTENSIONES).length > 0) {
                                for (var i = 0; i < Object.keys(REPORTE4_EXTENSIONES).length; i++) {
                                    if (REPORTE4_EXTENSIONES[i] == 'docx') {
                                        htmldiveditor += armarImgCargarPlantilla(href, tipoextension, rutaimg, imgtoshow);
                                    }
                                    if (REPORTE4_EXTENSIONES[i] == 'pdf' || REPORTE4_EXTENSIONES[i] == 'PDF') {
                                        htmldiveditor += armarImgCargarPlantilla(href, tipoextension, rutaimg, 'PDF.png');
                                    }
                                }
                            }

                        }

                        htmldiveditor += textodespuesimg + ' ';
                    }

                }


                if (rutadoc != "") {
                    htmldiveditor += '<a href="#" onclick="exportar(' + href + ',' + tipoextension + ')" >' + nombrearchivo + '</a>'
                }
                htmldiveditor += '</div>';

                htmldiveditor += ' <div style="text-align:right; float:right;display:none" ' +
                    'id="div_modificarborrar">';

                var titleTobtnBorrar = 'Elimina la plantilla y restaura el editor web.';
                if (optSelectedNewBorrador == INTERNA_SIN_PLANTILLA) {
                    titleTobtnBorrar = 'Elimina la plantilla y restaura su estado inicial';
                }

                if (configsHasValue['MODIFICAR_AL_APROBAR'] == "NO" && usuarioAproboBorrador != "" && usuarioAproboBorrador != codusuario) {
                } else {
                    //esto era el ejemplo
                    /* htmldiveditor += '<a href="#" onclick="mostrarModalVersion()" title="generar nueva version">' +
                    'Versionar <i class="fa fa-arrow-up" data-toggle="tooltip" title="Versionar"></i></a> &nbsp;' + '<a href="#" onclick="mostrarInputPlant()" title="subir plantilla nueva o modificada">' +
                        'Modificar <i class="fa fa-arrow-up" data-toggle="tooltip" title="Modificar"></i></a> &nbsp;' +
                        '<a href="#" title="' + titleTobtnBorrar + '" onclick="btnBorraPlantilla()">' +
                        'Borrar <i class="fa fa-trash" data-toggle="tooltip" title="Borrar"></i></a>'; */

                    htmldiveditor += '<a href="#" onclick="mostrarInputPlant()" title="subir plantilla nueva o modificada">' +
                        'Modificar <i class="fa fa-arrow-up" data-toggle="tooltip" title="Modificar"></i></a> &nbsp;' +
                        '<a href="#" title="' + titleTobtnBorrar + '" onclick="btnBorraPlantilla()">' +
                        'Borrar <i class="fa fa-trash" data-toggle="tooltip" title="Borrar"></i></a>';
                }

                htmldiveditor += ' </div> </div> </div>';


            }

            $("#div_subirplantilla").html(htmldiveditor)
            return htmldiveditor
        }

        function armarImgCargarPlantilla(href, tipoextension, rutaimg, imgtoshow) {
            var html = ' <a href="#" onclick="exportar(' + href + ',' + tipoextension + ')" title="Descargar plantilla">' +
                '<img id="imgupload" style="min-height: 40px;max-height:50px; min-width: 50px;max-width: 63px; " ' +
                ' src="' + rutaimg + imgtoshow + '" > </a> ';

            return html;

        }

        /*function showModalEjemplo(){
            // identifico la tabla del modal
            tablaAnexosTiposADefinir = $('#tablaejemplotipoanexos');
            //si ya esta inicializada la destruyo ya que mas abajo la vuelvo a inicializar
            if (tablaAnexosTipos != null){
                tablaAnexosTipos.destroy();
            }
            //muestro el modal
            $('#modal_ejemplo_anexos_tipos').modal('show');
            //inicializo la tabla
            definirTablaAnexos(tablaAnexosTiposADefinir);
            //consulto y armo segun los resultados
            consultarTiposAnexos();
        }*/ 

        /*function  consultarTiposAnexos() {
            //con esto llamo a la consulta
            $.ajax({
                url: "generalborrador.php?function_call=consultar_tipos_anexos",
                type: 'GET',
                dataType: 'json',
                async: false,
                success: function (response) { 
                    htmlTabla = '';
                    var tiposAnexos = response.anexos_tipos;
                    //verifico que vino al menos 1 resultado
                    if (tiposAnexos.length > 0) {
                        //proceso cada uno de los resultados para armar todas las filas
                        $.each(tiposAnexos, function (index, elem) {
                            //la construccion de las filas la ejecuta esta funcion
                            addFilaTiposAnexosEjemplo(index, elem);
                        });
                        //agrego lo que arme a la tabla
                        $('#tbodyanexostipos').html('');
                        $('#tbodyanexostipos').html(htmlTabla);
                    }else{
                        //si no hay resultados inicializo la tabla de nuevo esta vez sin resultados de consulta ya que no ibtuve
                        tablaAnexosTipos.destroy();
                        definirTablaAnexos(tablaAnexosTiposADefinir);
                    }

                },error: function (response) {
                    //si la consulta da error inicializo la tabla vacia y disparo un alerta de error
                    tablaAnexosTipos.destroy();
                    definirTablaAnexos(tablaAnexosTiposADefinir);
                    //esta es la alerta
                    Swal.fire("Error", "Ha ocurrido un error", "error");
                }

            });
        }*/

        function addFilaTiposAnexosEjemplo(ind,item){
            if (item.ANEX_TIPO_EXT != '' && item.ANEX_TIPO_EXT != ' '){
            htmlTabla = htmlTabla + '<tr><td>'+ item.ANEX_TIPO_EXT +'</td><td>'+  item.ANEX_TIPO_DESC +'</td> </tr>';
            }
       }

        /*function guardarVersion(){

            var dataForm = { 'borrador_id' : borrador_id , 'path_nuevo' : $('#ruta_version').val() , 'function_call' : 'actualizar_path_versionamiento' };
            var contentTypeJ = false;
            $.ajax({
                url: "generalborrador.php",
                type: 'POST',
                data: dataForm,
                dataType: 'json',
                async: false,
                success: function (response) {

                    alertModal('Exito', 'Ruta de nueva version actualizada correctamente', 'success', '5000')

                },error: function (response) {

                    Swal.fire("Error", "Ha ocurrido un error", "error");
                }

            });
        }*/

        /*function mostrarModalVersion(){
            $('#modal_versionamiento_exe').modal({show: true, keyboard: false, backdrop: 'static'});
        }*/


        function armarRowTbn() {

        console.log();
            $('#div_previo_cargardoc').html('');
            $('#div_previo_cargardoc').html('<input type="file"\n' +
                '                                                           name="userfile" value=""\n' +
                '                                                           accept=".docx,application/vnd.openxmlformats-officedocument.wordprocessingml.document"\n' +
                '                                                           id="cargardoc"\n' +
                '                                                           class="form-control" placeholder="Seleccione el archivo">\n' +
                '                                                ');

            var htmltitulosmenos = '';
            for (var i = 0; i < Object.keys(configuraciones).length; i++) {
                if (configuraciones[i]['NOMBRE_CONSTANTE'] == 'TITULO_MEMO') {
                    var valores = JSON.parse(configuraciones[i]['VALOR'])
                    for (var i = 0; i < Object.keys(valores).length; i++) {
                        htmltitulosmenos += '<option value="' + valores[i] + '">' + valores[i] + '</option>';
                    }
                }
            }

            $('#divrowbtns').html('');
            var html = ' <div class="col-md-12"> <div class="btn-group" style="float: right;">' +
                ' ' +
                '<button type="button" title="Enviar borrador" id="div_enviar" ' +
                'style="border-top-right-radius:0px !important; border-top-left-radius:20px !important;border-bottom-left-radius:20px !important;" ' +
                'onclick="enviarBorrador()" class="btn btn-lg blue"> <i class="fa fa-arrow-right"></i>Enviar </button> ' +
                '<button type="button" ' +
                'class="btn blue btn-lg dropdown-toggle depende_div_enviar" ' +
                'style="margin-right: 10px; float:right; border-top-right-radius:20px !important;border-bottom-right-radius:20px !important;" data-toggle="dropdown"aria-expanded="false"> ' +
                '<i class="fa fa-angle-down"></i> </button> ' +
                '<ul class="dropdown-menu" role="menu" id="dropdown_enviar_a">';

            html += addUsersToDropdownEnviar('', new Array());

            html += '</ul> </div> <button type="button" class="btn btn-lg" id="div_radicar" ' +
            'style="border-radius:20px !important; margin-right:10px;background-color: orange;float:right" onclick="formRadicar()"> ' +
            '<i class="fa fa-barcode"></i>Radicar </button> ';

            html += ' <button type="button" id="div_no_aprobar" class="btn btn-lg red-pink" ' +
                'title="Rechazado para corregir" onclick="btn_no_aprobar()" style="border-radius:20px !important;margin-right: 10px;float:right">  ' +
                '<i class="glyphicon glyphicon-remove"></i>No Aprobar </button> ' +


                ' ' +
                '<div class="btn-group" style="float: right;">' +
                '<button type="button" title="Aprobar Borrador" id="buttom_aprobar" ' +
                'style="border-top-right-radius:0px; border-top-left-radius:20px !important;border-bottom-left-radius:20px !important;" ' +
                'onclick="botonaprobar(event)" class="btn btn-lg green"> <i class="fa fa-arrow-right"></i>Enviar </button> ' +
                '<button type="button" ' +
                'class="btn green btn-lg dropdown-toggle depende_div_aprobar" ' +
                'style="margin-right: 10px; float:right; border-top-right-radius:20px !important;border-bottom-right-radius:20px !important;" data-toggle="dropdown"aria-expanded="false"> ' +
                '<i class="fa fa-angle-down"></i> </button> <ul id="uldropdownaprobar" class="dropdown-menu" role="menu"">';

            var texto = "'aprobar'";
            html += '    <li><a href="#"   ' +
                'onclick="clickdropdownaprobar(event,' + texto + ')">Comentar y Aprobar</a></li>';
            texto = "'revisar'";
            html += '    <li><a href="#" ' +
                'onclick="clickdropdownaprobar(event,' + texto + ')">Comentar y Revisar</a></li>';

            var textofnmodalexped = "'NOCREAREXPEDSINRAD'";

            html += '</ul> </div>' +

                '<button type="button" id="div_expediente" ' +
                'class="btn btn-lg yellow  " style="border-radius:20px !important;margin-right: 10px;float:right" ' +
                'title="Incluir en expediente" onclick="modalExpedienteBorrad(' + textofnmodalexped + ')"> <i class="fa fa-folder-open-o" data-toggle="tooltip" ></i> ' +
                '</button> <button type="button" class="btn btn-lg blue-dark guardar" id="primerguardar" onclick="crearBorrador(event)"' +
                ' style="border-radius:20px !important; margin-right: 10px;float:left"> <i class="fa fa-save"></i>Guardar </button> ' +
                '</div>' +
                '' +
                '</div>';

            $('#divrowbtns').html(html);
            $('#rowtitulomemo').html('');

            if (optSelectedNewBorrador == INTERNA_COMUNICACION) {
                $('#rowtitulomemo').html(' <br><div class="col-md-12 "><div class="form-group"> ' +
                    '<label class="col-md-2 selecttipocomunicacion" >Tipo de comunicación</label>' +
                    '                                         <div class="col-md-3">   <select name="selecttipocomunicacion" class="form-control selecttipocomunicacion"\n' +
                    '                                                    id="selecttipocomunicacion">' +
                    '' + htmltitulosmenos +
                    '</select></div></div></div>');
            }

            $('#div_primerafila_dinamica').html('');
            html = '<select class="js-data-example-ajax" id="selectDest" name="selectDest">\n' +
                '\n' +
                '                                                </select>\n' +
                '\n' +
                '                                                <input type="hidden" name="dest" value="" id="dest">\n' +
                '\n' +
                '                                                <input type="hidden" name="tipo_radicado" value="" id="tipo_radicado">\n' +
                '\n' +
                '                                                <input type="hidden" name="id_destinatario" value=""\n' +
                '                                                       id="id_destinatario">\n' +
                '                                                <input type="hidden" name="tipo_destinatario" value=""\n' +
                '                                                       id="tipo_destinatario">\n' +
                '                                                <input type="hidden" value="guardarBorrador" name="function_call"\n' +
                '                                                       id="function_call">';
            $('#div_primerafila_dinamica').html(html);

        }

        function mostrarDatosBorrador(response) {

            var borrador = response.success.borrador[0];
            //muestro los radicados asociados
            borradorActual = borrador;
            console.log('getborrador', response)
            $("#tipo_radicado").val(borradorActual.TIPO_RADICADO);

            //defino el tipo de borrador
            optSelectedNewBorrador = borradorActual.TIPO_BORRADOR_DEST;


            //la dependencia del destinatario, si es que es interno el borrador
            if (borradorActual.dependenDestinatario != undefined &&
                borradorActual.dependenDestinatario != "") {
                dependenDestinatario = borradorActual.dependenDestinatario
            }

            $("#tipo_borrador_dest").val(optSelectedNewBorrador)

            $("#desc_anexos_raiz").val(borradorActual.DESCRIPCION_ANEXOS == "NULL" || borradorActual.DESCRIPCION_ANEXOS == "null" ? "" : borradorActual.DESCRIPCION_ANEXOS)
            $("#folios_comunicacion_raiz").val(borradorActual.FOLIOS_COMUNICACION)
            $("#folios_anexos_raiz").val(borradorActual.FOLIOS_ANEXOS)

            //guardo los firmantes si es que tuvo
            firmantesselected = response.success.firmantes
            traerUsersEnviar();

            /**
             * Con lo siguiente, agrego los firmantes a la lista del dropdown de enviar*
             */
            addUsersToDropdownEnviar('', firmantesselected);

            //defino la cabecera, como el destinatario el cargo etc
            definircabeceraborra();
            
            $(".divcabeceradinamicos").css('display', 'block');
            
            //los mando a poner selected si es que tuvo
            selectedsFirmSelect2();
            $("#selectDest").val(borradorActual.ID_DESTINATARIO + '-' + borradorActual.TIPO_DESTINATARIO)
            $('#selectDest').trigger('change.select2');


            misRadicadosAsociados = response.success.radasociados //guardo en local, mis radicados asociados
            mostrarradicadoAso(response.success.radasociados);
            mostrarcomentarios(response.success.comentarios);

            anexosBorrador = response.success.anexos;
            mostraranexos(response.success.anexos);

            nombre_usu_anterior = response.success.nombre_anterior;
            depe_nomb_usu_anterior = response.success.dependUsuAnterior


            var htmladvertencia = "";
            var expediente = response.success.expediente;

            //siempre lo muestro
            $("#div_expediente").css('display', 'block');

            $("#id_destinatario").val(borradorActual.ID_DESTINATARIO);
            $("#tipo_destinatario").val(borradorActual.TIPO_DESTINATARIO)
            $("#tipopersona").val(borradorActual.TIPOPERSONA);


            if (codusuario == borradorActual.ID_USUARIO_ACTUAL) {
                /**
                 * Si entra aqui, es porque el usuario actual, ES el dueño del borrador
                 */
                setTimeout(function () {
                    $("#froalaeditor").froalaEditor("edit.on");
                }, 0)

                $("#div_borrar").remove();
                var borrar = "";
                borrar += '' +
                    ' <button type="button" id="div_borrar"  onclick="modalJustificDelBorrador(event)" title="Eliminar borrador" ' +
                    'class="btn btn-lg red-mint" style="border-radius:20px !important; margin-left: 10px;' +
                    ' float: left !important;">' +
                    '<i class="fa fa-trash"></i></button>'

                $("#primerguardar").after(borrar);
                $("#div_margin_top_princ").css('margin-top', '30px');

                //si el borrador es del usuario actual y es distinto de SIN AProbar , muestro el boton de enviar

                $("#div_enviar").css("display", "block");
                $(".depende_div_enviar").css("display", "block");

                $("#div_expediente").css("display", "block");

                $("#all_modal input").prop('disabled', false);
                $("#all_modal textarea").prop('disabled', false);
                $("#btnselectcargos").prop('disabled', false);
                $("#id_usuario_firmante").prop('disabled', false);
                $("#selectDest").prop('disabled', false);
                $(".guardar").prop('disabled', false);

            } else {
                /**
                 * Si entra aqui, es porque el usuario actual, NO es el dueño del borrador
                 */
                console.log('busca aqui');
                console.log(response.success);
                $("#divrowbtns").html('')
                htmladvertencia += ' <div class="col-md-10 " style="float: left" id=""> <div class="note note-warning"> ' +
                    '<h4 class="block">Usuario Actual:</h4><span id="advertenciacabecera">';

                htmladvertencia += response.success.nombre_actual;
                htmladvertencia += ' - Cargo ' + response.success.cargo_actual + ' - Dependencia ' + response.success.dependUsuActu;
                htmladvertencia += '</span><br>' ;
                htmladvertencia += '<span>Solo el usuario actual puede modificar el borrador.</span></div> </div>';

                var textofnmodalexped = "'NOCREAREXPEDSINRAD'";

                htmladvertencia += '<div class="col-md-2 "> <button type="button" id="div_expediente" class="btn btn-lg yellow  "' +
                    ' style="border-radius:20px !important;margin-left: 10px;float:left" title="Incluir en expediente"' +
                    ' onclick="modalExpedienteBorrad(' + textofnmodalexped + ')"> <i class="fa fa-folder-open-o" data-toggle="tooltip" ></i> ' +
                    '</button></div>';


                //desactivo el editor de plantilla
                //tinymce.activeEditor.getBody().setAttribute('contenteditable', false);
                $('#froalaeditor').froalaEditor('edit.off');


                $("#buttom_aprobar").css("display", "none");
                $('.depende_div_aprobar').css("display", "none");
                $("#all_modal input").prop('disabled', true);
                $("#all_modal textarea").prop('disabled', true);
                $("#tipopersona").prop('disabled', true);
                $("#edit").css("display", "none");
                $(".Editor-container").css("display", "none");
                $(".guardar").css("display", "none");
                $("#botonaddanexo").css("display", "none");
                $("#botonaddradicado").css("display", "none");
                //$("#botonaddradicado").remove();
                //$("#botonaddanexo").remove();

                $("#div_radicar").css("display", "none");

                $("#all_modal input").prop('disabled', true);
                $("#all_modal textarea").prop('disabled', true);
                $("#btnselectcargos").prop('disabled', true);
                $("#id_usuario_firmante").prop('disabled', true);
                $("#selectDest").prop('disabled', true);
                $(".guardar").prop('disabled', true);

                /**
                 * Fin de condicion, si soy el usuario actual del borrador
                 */
            }


            $("#div_advertenciacabecera").html('');
            if (htmladvertencia != "") {
                setTimeout(function () {
                    $("#div_advertenciacabecera").html(htmladvertencia);
                    $("#div_advertenciacabecera").css("display", 'block');
                }, 500)

            }
            var cargoEdicion = 'NO';
            $("#borrador_id").val(borradorActual.ID_BORRADOR);
            $('#dest').val(borradorActual.DESTINATARIO)
            $('#asunto').val(borradorActual.ASUNTO);
            console.log(borradorActual.CARGO_PERSONA);
            console.log(response.success.cargo_actual);
            console.log('cargosss');
            if (borradorActual.CARGO_PERSONA == response.success.cargo_actual){
                $('#cargo').val(response.success.cargo_actual);
            }else{
                $('#cargo').val(borradorActual.CARGO_PERSONA);
                cargoEdicion = 'SI';
            }
            if (optSelectedNewBorrador == SALIDA_OFICIO) {
                if (borradorActual.TIPO_DESTINATARIO == "2" && customBorradorSel != true) {
                    $('#persona').val(borradorActual.NOMBRE_PERSONA);
                    $(".divpersonacargo").css('display', 'block')
                    $("#divpreviotipopersona").remove()
                } else {
                    $(".divpersonacargo").css('display', 'none')
                    $("#divtipopersona").before('<div class="col-md-2" id="divpreviotipopersona"></div>');
                }
            }
            var cargo = $('#cargo').val();
            if (optSelectedNewBorrador != SALIDA_OFICIO &&
                cargo != undefined && (configsHasValue['CARGO_EDITABLE'] == 'NO') && (cargo.length > 3) && (cargoEdicion == 'NO')) {
                $('#cargo').prop('readonly', true);
            } else {
                $('#cargo').prop('readonly', false);
            }


            editoroplantilla = "EditorWeb"; //defino por defecto editor
            $("#editoroplantilla").val('EditorWeb');
            if (optSelectedNewBorrador == INTERNA_SIN_PLANTILLA) {
                editoroplantilla = "SinPlantilla";
                $("#editoroplantilla").val('SinPlantilla');
            }

            setTimeout(function () {
                $('#froalaeditor').froalaEditor('html.set', borradorActual.DOCUMENTO_WEB_CUERPO); //lo seteo siempre

            }, 500)


            /**
             *Si el borrador, tiene un usuario anterior, y si soy el dueño actual del borrador, y  tengo permiso para aprobar *
             * agrego al usuario anterior
             */
            if (
                borradorActual.ID_USUARIO_ANTERIOR != ""
                &&
                borradorActual.ID_USUARIO_ANTERIOR != "0"
                &&
                borradorActual.ID_USUARIO_ACTUAL == codusuario
                &&
                usua_perm_aprobar == "1"
            ) {
                $("#div_no_aprobar").css("display", "block");
                add_anterior_dropdown_enviar_a();
            }


            mostrarDivRadicar(borradorActual, response.success.comentarios);

            $("#titulomodal").html('');
            var tituloEdicion = '';
            if (optSelectedNewBorrador == SALIDA_OFICIO) {
                tituloEdicion += "RADICACIÓN DE SALIDA"+ ' -';
            } else if (optSelectedNewBorrador == REPORTE4) {
                tituloEdicion += reporte4_modal+ ' -';
            } else if (customBorradorSel == true){
                tituloEdicion += datosCustomBorrador.modal_radicado+ ' -';
            }else {
                tituloEdicion += "RADICACIÓN INTERNA -";
            }
            tituloEdicion += ' EDITAR BORRADOR ' + borradorActual.ID_BORRADOR;
            $("#titulomodal").html(tituloEdicion);

            if (borradorActual.DOCUMENTO_FORMATO == "Plantilla") {
                editoroplantilla = "Plantilla";
                $("#editoroplantilla").val('Plantilla');

                organizarMostrarDivEditor('link2');
            }

            if (borradorActual.DOCUMENTO_FORMATO == "SinPlantilla") {
                editoroplantilla = "SinPlantilla";
                $("#editoroplantilla").val('SinPlantilla');

                organizarMostrarDivEditor('link2');
            }


            //si MODIFICAR_AL_APROBAR==NO y el usuario que aproboel borrador es distinto al logueado, entonces
            //No permitir modificar el contenido de un documento (Destinatario, Asunto, editor web, plantilla, asociados,
            // anexos). Puede agregar asociados, anexos, revisar, expediente, enviar, radicar.

            if (configsHasValue['MODIFICAR_AL_APROBAR'] == "NO" && usuarioAproboBorrador != "" &&
                usuarioAproboBorrador != codusuario) {
                $("#dest").attr('readonly', true)
                $("#asunto").prop('readonly', true)
                $('#froalaeditor').froalaEditor('edit.off');
            }

            if (codusuario != borradorActual.ID_USUARIO_ACTUAL) {
                $("#div_modificarborrar").remove();
            }

            if (optSelectedNewBorrador == REPORTE4) {
                putFoliosAnexosFromCountAnexos(true);
            }else{
                putFoliosAnexosFromCountAnexos(false);
            }


        }

        function validarmostrarEditor() {

            //si es este tipo de borrador  escondo por defecto el editor
            if (
                optSelectedNewBorrador == INTERNA_CON_PLANTILLA
                ||
                optSelectedNewBorrador == INTERNA_SIN_PLANTILLA
                ||
                optSelectedNewBorrador == REPORTE4 || customBorradorSel == true 
            ) {
                $("#div_edit").css("display", "none");
                $(".div_btn_doc_and_pdf").css("display", "none");

            } else {

                /**
                 * Esta funcion, es llamada desde resetform, que por defecto, tiene editoroplantilla = "EditorWeb",
                 * pero luego cuando llama a getborrador, se actualiza a Plantilla o SinPlantilla, si es el caso
                 */
                //para los demas, muestro el editor
                editoroplantilla = "EditorWeb";
                $("#editoroplantilla").val('EditorWeb');
                $("#div_cargardoc").css("display", "none");
                $("#div_loader").css("display", "none");
                $("#div_edit").css("display", "block");
                $("#edit").prop("disabled", false);
                if (borradorActual.DOCUMENTO_RUTA != undefined && borradorActual.DOCUMENTO_RUTA != '' &&
                    borradorActual.DOCUMENTO_RUTA != null && borradorActual.DOCUMENTO_RUTA != 'NULL') {
                    $("#div_modificarborrar").css("display", "block");
                }
            }


        }

        function mostrarDivRadicar(borradorActual, historial) {
            consiguioaprobar = false;
            almenosaprobado = false;
            for (var i = 0; i < historial.length; i++) {

                if (codusuario == borradorActual.ID_USUARIO_ACTUAL) {

                    //si consiguio al menos una transaccion en aprobado
                    if (historial[i]["NOMBRE_ESTADO"] == "Aprobado") {
                        $("#div_radicar").css("display", "block");
                        consiguioaprobar = true;
                    }
                    //si consiguo una transaccion en Revisado, y el borrador es INTERNA_SIN_PLANTILLA
                    if (historial[i]["NOMBRE_ESTADO"] == "Revisado" &&
                        optSelectedNewBorrador == INTERNA_SIN_PLANTILLA) {
                        $("#div_radicar").css("display", "block");
                    }
                    //si la siguiente configuracion es="NO" entonces siempre muestro el boton de radicar
                    if (configsHasValue['APROBAR_OBLIGATORIO_RADICAR'] == "NO") {
                        $("#div_radicar").css("display", "block");
                    }
                }

                if (historial[i]["NOMBRE_ESTADO"] == "Aprobado") {
                    almenosaprobado = true;
                    //guardo el usuario que aprobo el borrador.
                    usuarioAproboBorrador = historial[i]["ID_USUARIO_ORIGEN"];
                }

                if (historial[i]["NOMBRE_ESTADO"] == "Revisado") {
                    //guardo si al menos fue revisado una  vez
                    almenosrevisado = true;
                }

            }

            if (almenosaprobado == true) {
                $("#div_no_aprobar").css("display", "none");
                $("#li_devolver_a").remove();
            }

            if (((consiguioaprobar == false && usua_perm_aprobar == "1")) && codusuario == borradorActual.ID_USUARIO_ACTUAL) {
                $("#buttom_aprobar").html("<i class='fa fa-check-square-o'></i>Aprobar");
                $("#buttom_aprobar").attr("onclick", "botonaprobar(event)");
                $("#buttom_aprobar").css("display", "block");
                $('.depende_div_aprobar').css("display", "block");

            } else {

                $("#buttom_aprobar").html("<i class='fa fa-check'></i>Revisar");
                $("#buttom_aprobar").attr("onclick", "revisar(event)");
                $("#buttom_aprobar").css("display", "block");
                $('.depende_div_aprobar').remove();
                $('#uldropdownaprobar').remove();
                setTimeout(function () {
                    $("#buttom_aprobar").css("border-top-right-radius", "20px");
                    $("#buttom_aprobar").css("border-bottom-right-radius", "20px");
                }, 50)


            }

            if (codusuario != borradorActual.ID_USUARIO_ACTUAL) {
                $("#buttom_aprobar").css("display", "none");
                $('.depende_div_aprobar').css("display", "none");
            }

            if (configsHasValue['APROBAR_OBLIGATORIO_RADICAR'] == "NO" &&
                (almenosrevisado == false && almenosaprobado == false)) {
                $("#div_radicar").css("opacity", 0.65);

            } else {
                $("#div_radicar").css("opacity", 10);
            }

        }


    </script>


    <script>

        $(document).on('click', '#deactivate-user', function (e) {
            e.preventDefault();
            return false;
        });

        $(document).on('click', '.show-search', function () {
            inputexpediente.typeahead('val', '');
            $('.search-page.search-content-2').removeClass('hide');
            $('.invoice').html('').addClass('hide');
        });


        function guardarEditor(borrador_id) { //guarda los datos del editor
            if (optSelectedNewBorrador == INTERNA_SIN_PLANTILLA) {
                editoroplantilla = "SinPlantilla";
                $("#editoroplantilla").val('SinPlantilla');
            }

            var editor = "";
            if ($('#div_edit').is(':visible')) //el div donde esta el editor froala
            {
                editor = $('#froalaeditor').froalaEditor('html.get');
            }

            var params = {
                borrador_id: borrador_id,
                function_call: "guardarEditor",
                editor: editor,
                editoroplantilla: editoroplantilla,
                optSelectedNewBorrador: optSelectedNewBorrador
            }

            var ajax = BorradorService.guardarEditor(params);
            ajax.success(function (response) {
                documentRout = response.rutaExist;
                fileExistPlant = response.exists;
            }).error(function (error) {
                Swal.fire("Error", "Ha ocurrido un error al actualizar los datos del editor", "error");
            });

        }

        //cuando se presiona los botones guardar
        function crearBorrador(evento, accion) {

            if (borrador_id != '') {
                accion = 'guardar';
            } else {
                accion = 'crear';
            }

            $(".guardar").prop('disabled', true);

            if (HABILITARGUARDAR == false) {
                return false;
            }

            $("#function_call").val("guardarBorrador")
            if ($("#id_destinatario").val() == "") {

                Swal.fire({
                    title: "Destinatario Inválido",
                    text: "Elegir destinatario de la lista o solicitar su creación a soporte: " + correosoporte,
                    type: "warning",
                    closeOnConfirm: true,
                    closeOnCancel: true,
                    confirmButtonText: "Aceptar",
                })
                $(".guardar").prop('disabled', false);
                evento.preventDefault();
                return false;
            }

            if ($("#asunto").val() == "") {
                Swal.fire("Alerta", "Debe ingresar un asunto", "warning");
                $(".guardar").prop('disabled', false);
                evento.preventDefault();
                return false;
            }
            var asunto = $("#asunto").val();


            if ($('#asunto').is(':visible') && asunto.length < configsHasValue['ASUNTO_MINLENGTH']) {
                Swal.fire("Alerta", "El asunto debe contener al menos " + configsHasValue['ASUNTO_MINLENGTH'] + " caracteres", "warning");
                $(".guardar").prop('disabled', false);
                evento.preventDefault();
                return false;
            }

            if ($('#asunto').is(':visible') && asunto.length > 999) {
                Swal.fire("Alerta", "El asunto debe contener menos de 999 caracteres", "warning");
                $(".guardar").prop('disabled', false);
                evento.preventDefault();
                return false;
            }

            var asunto = $('#asunto').val();
            if (radicadoactual != '') {
                asunto = asunto.toString()
                asunto = asunto.split("...").length - 1
                if (asunto > 1) {
                    alertModal('Alerta', 'Complete el asunto, no puede contener ...', 'warning', '5000')
                    $(".guardar").prop('disabled', false);
                    return false;
                }

            }


            HABILITARGUARDAR = false;

            var formData = new FormData($("#search-form")[0]);
            var contentType = false;

            var primer_firmante = firmantesselected[0] != undefined ? firmantesselected[0]['ID'] : '';
            formData.append('ID_FIRST_SELECTED', primer_firmante);
            formData.append('optSelectedNewBorrador', optSelectedNewBorrador);
            formData.append('borrarDocumentoRuta', borrarDocumentoRuta)
            $.ajax({
                url: "generalborrador.php",
                type: 'POST',
                data: formData,
                dataType: 'json',
                cache: false,
                contentType: contentType,
                processData: false,
                async: false,
                success: function (response) {

                    if (response.success) {

                        alertModal('Guardar', response.success, 'success', '10000')

                        guardarEditor(response.borrador_id);

                        var retornogetBorrador = getBorrador(response.borrador_id)

                        /**
                         * Si existe response.documento, es porque se acaba de subir un archivo en el request anterior
                         */
                        if (response.documento) {
                            //si subio un documento, mando a mostrar el div de abajo de los exportar
                            organizarMostrarDivEditor('link2')

                        }

                        //verifica si al menos alguno de los firmantes, es Remitente.
                        if (retornogetBorrador === true) {
                            validaHayRemitente(accion);
                        }

                    } else {
                        Swal.fire("Error", response.error, "error");

                    }
                    $(".guardar").prop('disabled', false);
                    HABILITARGUARDAR = true;

                },
                error: function (response) {
                    $(".guardar").prop('disabled', false);
                    HABILITARGUARDAR = false;
                    Swal.fire("Error", "Ha ocurrido un error", "error");
                }
            });
        }

        //verifica si al menos alguno de los firmantes, es Remitente.
        function validaHayRemitente(accion = 'crear') {
            console.log(accion);
            var encontroremitente = false;

            for (var i = 0; i < Object.keys(firmantesselected).length; i++) {
                console.log('firmantes remitentes');
                console.log(firmantesselected[i]);
                if (firmantesselected[i]['TIPO_VALIDADOR'] !== undefined &&
                    firmantesselected[i]['TIPO_VALIDADOR'] === "Remitente") {
                    encontroremitente = true;
                }
            }

            // comentada esta condicion ya que ahora no se debe abrir al primer guardado
            //if (encontroremitente == false ||
            //    (
            //        (Object.keys(firmantesselected).length > 1) && (accion == 'crear'))) {
            if (encontroremitente == false) {
                var txt = "BORRADOR";
                var modal = "modalcargosfirmantes";
                var tbodycargosfirmantes = "tbodycargosfirmantes";
                var claseeditables = "aEditables";
                if (Object.keys(firmantesselected).length > 1) {
                    modalSeleccCargos(txt, modal, tbodycargosfirmantes, claseeditables, borrador_id)
                }
                alertModal('Alerta', 'Debe seleccionar un Remitente', 'warning', '5000')
            }
        }


        /*funcion que se ejecuta cuando se selecciona alguna de las opciones delsplegables del Nuevo Borrador*/
        function onClickNewBorrador() {
            resetForm();
            resetVar();
            definircabeceraborra();

            if ((radicadoactual == "") || (radicadoactual != "" && radicado_has_exped > 0)) {
                $('#all_modal').modal({show: true, keyboard: false, backdrop: 'static'});

                if (radicadoactual != "") {
                    $('#asunto').val('... Respuesta/Alcance ... radicado ' + radicadoactual + ' ' + radicadoasunto + ' ...')
                } else if (optSelectedNewBorrador == REPORTE4) {
                    $('#asunto').val(reporte4_asunto + ' ' + usua_nomb + ' CC ' + usua_doc)
                } else if (customBorradorSel == true){
                    $('#asunto').val(datosCustomBorrador.asunto_radicado);
                }

                /**
                 * si optSelectedNewBorrador==REPORTE4, coloco por defecto el usuario logueado
                 */
                if (optSelectedNewBorrador == REPORTE4) {

                    setSelectedUsersFirmantes(codusuario);
                    /**
                     * a continuacion, mando a buscar los datos del usuario logueado, en el arreglo de allUsersFirmantes
                     * para asignarselo a los firmantesselected
                     */
                    var verificaesta = userIsInFirmantes(codusuario);

                    if (verificaesta.datos != undefined) {
                        firmantesselected[0] = verificaesta.datos;
                    }

                    $("#desc_anexos_raiz").val('Anexos electrónicos');

                    putFoliosAnexosFromCountAnexos(true);


                    if(configsHasValue['AGREGAR_PARA_EN_DE'] == 'SI'){
                        //setSelectedUserDestinatario(codusuario);
                    }

                }else{
                    putFoliosAnexosFromCountAnexos(false);
                }



            } else {
                //@formatter:off
                Swal.fire({
                    title: "Radicado padre " + radicadoactual + " sin expediente",
                    text: "Para continuar, es requisito incluirlo en el expediente",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Incluir en expediente",
                    cancelButtonText: "Cerrar",
                    closeOnConfirm: true,
                    closeOnCancel: true
                    }).then((result) => {
                    if(result.value)
                {

                    setTimeout(function () {
                        //desdeborrador, le ice que lo estoy haciendo desde borrador,
                        //y para ser mas especifico, desde el info del raicado, es decir,
                        //desde el iframe, entonces le [paso esa variable
                        //para poder hcer el back cuando lo incluta en expediente
                        window.location.href = "../borradores/radicardesdeentrada.php?desdeborrador=1&radicado=" + radicadoactual + ","
                    }, 500)
                }else if (
                    result.dismiss === Swal.DismissReason.cancel
                ) {

                }
            });
//@formatter:on

            }


        }

        /*cuando se selecciona alguna de las opciones en el boton de nuevo Borrador*/
        function btnNuevoBorrador(option,customBorrador = {}) {
            optSelectedNewBorrador = option;
            console.log(customBorrador);
            var fechaBorradorRep = new Date();
            var anoBorradorRep = fechaBorradorRep.getFullYear();
            var mesBorradorRep = fechaBorradorRep.getMonth()+1;
            var diaBorradorRep = fechaBorradorRep.getDate();
            if (JSON.stringify(customBorrador)=='{}') {
            customBorradorSel = false;
            }else{
            customBorradorSel = true;
            datosCustomBorrador = JSON.parse(customBorrador);
            console.log(diaBorradorRep);
            datosCustomBorrador.asunto_radicado = datosCustomBorrador.asunto_radicado.replace("#AAAA",anoBorradorRep);
            datosCustomBorrador.asunto_radicado = datosCustomBorrador.asunto_radicado.replace("#MM",mesBorradorRep+'-');
            datosCustomBorrador.asunto_radicado = datosCustomBorrador.asunto_radicado.replace("#DD",diaBorradorRep+'-');
            }
            
             

            setTimeout(function () {
                $("#tipo_borrador_dest").val(optSelectedNewBorrador)
            }, 1)

            if (infRadicadoPadre[0] != undefined) {
                if (infRadicadoPadre[0].RADI_DEPE_ACTU == 999) {
                    alertModal('Alerta', 'Radicado padre ' + infRadicadoPadre[0].RADI_NUME_RADI + ' archivado, No puede crear borrador, debe solicitar devolución a su bandeja.', 'warning', '5000')
                    return false;
                }
            }

            onClickNewBorrador();
        }

        /**
         * trae los usuarios para el select de enviar y para el select de firmantes,
         * opcionalmente, recibe unos usuarios a buscar tambien, pero no es obligatorio
         */

        function traerUsersEnviar(usuarios_a_buscar = new Array()) {

            var urlusuariosfirm = "buscarUsuariosComen.php?type=public&traerfirmantes=SI&borrador=" + borrador_id + "&optSelectedNewBorrador=" + optSelectedNewBorrador;

            $.ajax({
                url: urlusuariosfirm,
                type: 'GET',
                dataType: 'json',
                async: false,
                data: {
                    buscar_otros_users: usuarios_a_buscar
                },
                success: function (response) {
                    allUsersFirmantes = response;
                },
                error: function (response) {
                    Swal.fire("Error", "Ha ocurrido un error al buscar los usuarios", "error");
                }
            });
        }


        //dependiendo del tipo de borrador, muestra o no Persupper(id)ona y cargs
        function definirPersona(tipo_borrador) {
            $("#divpreviotipopersona").remove()

            if (tipo_borrador == "Externo|Empresa") {


            $(".divpersonacargo").css('display', 'block');

            } else {
                setTimeout(function () {
                    $(".divpersonacargo").css('display', 'none');
                    $("#divtipopersona").before('<div class="col-md-2" id="divpreviotipopersona"></div>');
                }, 1)

            }

            $(".divcabeceradinamicos").css('display', 'block')
            $("#div_cargo_dest").css('display', 'block')
        }

        //functio   que define las cabeceras del borrador, donde se muestran cargo para , etc
        function definircabeceraborra() {
            var html = '';

            if (Object.keys(allUsersFirmantes).length < 1) {

                traerUsersEnviar()
            }

            html = '<div class="col-md-12 divcabeceradinamicos"  style="display:none" > <div class="form-group">  ' +
                '<label class=" col-md-2 divpersonacargo">Persona: </label> ' +
                '<div class="col-md-2" id="divtipopersona"> ' +
                '<div class="input-icon right">' +
                ' <select name="tipopersona" class="form-control" id="tipopersona">';


            if (Object.keys(titulos_tratamiento).length > 0) {
                for (var i = 0; i < Object.keys(titulos_tratamiento).length; i++) {
                    html += '<option value="' + titulos_tratamiento[i].TITULO + '">' + titulos_tratamiento[i].TITULO + '</option>';
                }
            }
            html += '</select> ' +
                '</div> </div> <div class="col-md-5 divpersonacargo"> ' +
                '<div class="input-icon right"> <input type="text" name="persona" value="" ' +
                'id="persona" class="form-control" autocomplete="off"  ' +
                'onkeyup="keyupAlfanumerico(event,this)" style="border: solid grey 1px" placeholder=""> </div> </div> ' +
                '<label class=" col-md-1 divpersonacargo " id="labelcargo">Cargo: </label> ' +
                '<div class="col-md-2 divpersonacargo"> <div class="input-icon right">' +
                ' <input type="text" name="cargo" value="" id="cargo" autocomplete="off" ' +
                'onkeyup="keyupAlfanumerico(event,this)" class="form-control" style="border: solid grey 1px" ' +
                'placeholder=""> </div> </div></div> </div>';

            if (optSelectedNewBorrador == SALIDA_OFICIO) {

                $("#lbldestinatario").html('');
                $("#lbldestinatario").html('Destinatario:')
            }
            if (optSelectedNewBorrador == INTERNA_COMUNICACION ||
                optSelectedNewBorrador == INTERNA_CON_PLANTILLA ||
                optSelectedNewBorrador == INTERNA_SIN_PLANTILLA ||
                optSelectedNewBorrador == REPORTE4 || customBorradorSel == true) {
                html = '<div class="col-md-12 divcabeceradinamicos" id="div_cargo_dest"  style="display:none" > <div class="form-group">' +
                    '<label class=" col-md-2 "></label>' +
                    '<label class=" col-md-1 " id="labelcargo">Cargo: </label>' +
                    '<div class="col-md-5"  > ' +
                    '' +
                    ' ' +
                    '<input type="text" name="cargo" value="" id="cargo" class="form-control" style="" ' +
                    'placeholder="">' +
                    '</div> </div> </div>';

                $("#lbldestinatario").html('');
                $("#lbldestinatario").html('PARA: <span style="font-size:10pt;color:#bdbdbd !important" >(Destinatario)</span>');

            }

            html += '<div class="col-md-12 divcabeceradinamicos"  > <div class="form-group">' +
                '<label class=" col-md-2 ">DE: <span style="font-size:10pt;color:#bdbdbd !important" >(Firmantes)</span></label>' +
                '<div class="col-md-10" > ' +
                '<div class="input-icon right">' +
                ' <select class="js-data-example-ajax" name="id_usuario_firmante[]" multiple ' +
                ' id="id_usuario_firmante" style="width: 100%">' +
                '</select><br> ' +
                '</div> </div> </div> </div>';

            html += '<div class="col-md-12 divcabeceradinamicos" id="div_cargo_firmante" style="display:none" > <div class="form-group">' +
                '<label class=" col-md-2 "></label>' +
                '<label class=" col-md-1 " id="textocargounfirmante">Cargo: </label>' +
                '<div class="col-md-5" id="divcargofirmantes"> ' +
                '' +
                '' +
                '<input type="text" name="cargo_firmante" value="" id="cargo_firmante" class="form-control" ' +
                'placeholder="">' +
                '</div> </div> </div>';


            $(".divcabeceradinamicos").remove()
            $("#divasunto").before(html)


            var user_to_search = '';

            if (borrador_id == '') {
                var user_to_search = '';
                if (Object.keys(remitenteRadicado).length > 0) {
                    user_to_search = remitenteRadicado[0]['ID'];
                }
            }

            traerUsersDestinatarios(user_to_search);

            /**
             * defino el select2 de destinatarios
             */
            definirSelectDest();

            /**
             * Luego de definir el select de destinatarios, desmarco toda opcion seleccionada, y luego verifico
             * si borrador_id=='', esto quiere decir que estoy creando un borrador nuevo, y tambien ptregunto si
             * borradorRadPadre > 0, para entonces si tiene datos, marcar el destinatario del radicado padre
             */
            $("#selectDest").val(null).trigger('change.select2');

            if (borrador_id == '' && Object.keys(borradorRadPadre).length > 0) {

                $("#selectDest").val(borradorRadPadre[0]['ID_DESTINATARIO'] + '-' + borradorRadPadre[0]['TIPO_DESTINATARIO']).trigger('change.select2');
            }
            //if (borrador_id == '' && Object.keys(infRadicadoPadre).length > 0) {

                //$("#selectDest").val($("#id_destinatario").val() + '-' + $("#tipo_destinatario").val()).trigger('change.select2');
            //}

            /**
             * defino el select2 de firmantes
             */
            definirSelectFirmantes()

            quitOponerCargFirman();


            //si es Externo|Ciudadano o Externo|Empresa es del tipo SALIDA
            $("#titulomodal").html('');
            var htmltitulomodal = "NUEVO BORRADOR";
            if (optSelectedNewBorrador == SALIDA_OFICIO) {
                htmltitulomodal += " PARA RADICACIÓN DE SALIDA"
            } else if (optSelectedNewBorrador == REPORTE4) {
                htmltitulomodal += " PARA " + reporte4_modal
            } else if (customBorradorSel == true){
                htmltitulomodal += " PARA " + datosCustomBorrador.modal_radicado;
            }else {
                htmltitulomodal += " PARA RADICACIÓN INTERNA"
            }
            $("#titulomodal").html(htmltitulomodal);

            //console.log('esto es lo que necesito evaluar');
            //console.log(remitenteRadicado[0]['ID'] + '-' + remitenteRadicado[0]['TIPO_RADICADO']);
            //SI ESTOY INTENTANDO CREAR UN NUEVO borrador desde radicados /documentos
            //entonces coloco el destinatario por defecto del radicado, como sugerencia
            if (borrador_id == "" && radicadoactual != "" && Object.keys(remitenteRadicado).length > 0) {

                if (
                    (optSelectedNewBorrador == SALIDA_OFICIO &&
                        (
                            remitenteRadicado[0]['TIPO_RADICADO'] == "Externo|Ciudadano"
                            ||
                            remitenteRadicado[0]['TIPO_RADICADO'] == "Externo|Empresa"
                        )
                    )
                    ||
                    (
                        (
                            optSelectedNewBorrador == INTERNA_COMUNICACION ||
                            optSelectedNewBorrador == INTERNA_CON_PLANTILLA ||
                            optSelectedNewBorrador == INTERNA_SIN_PLANTILLA ||
                            optSelectedNewBorrador == REPORTE4 || customBorradorSel == true
                        )
                        &&
                        (
                            remitenteRadicado[0]['TIPO_RADICADO'] == "Interno |"
                        )
                    )
                ) {

                    setearTipoRadicado(remitenteRadicado[0]['TIPO_RADICADO']);

                    $("#id_destinatario").val(remitenteRadicado[0]['ID'])
                    $("#tipo_destinatario").val(remitenteRadicado[0]['TIPO_DESTINATARIO'])
                    $('#dest').val(remitenteRadicado[0]['TIPO_RADICADO'] + " " + remitenteRadicado[0]['NOMBRE']);
                    $("#selectDest").val(remitenteRadicado[0]['ID'] + '-' + remitenteRadicado[0]['TIPO_DESTINATARIO']).trigger('change.select2');
                    definirPersona(remitenteRadicado[0]['TIPO_RADICADO']);

                    //es empresa
                    if (remitenteRadicado[0]['PERSONACARGO'] != undefined) {
                        $("#cargo").val(remitenteRadicado[0]['PERSONACARGO'])
                    }
                    //es empresa sgd_dir_nombre
                    if (remitenteRadicado[0]['SGD_DIR_NOMBRE'] != undefined) {
                        $("#persona").val(remitenteRadicado[0]['SGD_DIR_NOMBRE'])
                    }

                }


                $("#div_cargo_firmante").css('display', 'none')
            }


            $(".campos_valida_keyup").on('keyup', function (e) {
                var positionselecttion = e.target.selectionStart
                var string = $(this).val();
                //lo siguiente no permite dos espacios en blanco seguidos.
                /[^a-zA-ZáéíóúÁÉÍÓÚñÑ´\w\s]/gi
                string = string.replace(/^[a-zA-ZáéíóúÁÉÍÓÚ]*$ {2,}/g, ' ');
                string = string.trim();

                var cadena = string;
                cadena = cadena.split(" ");
                var retorno = "";
                for (var j = 0; j < cadena.length; j++) {
                    var str = cadena[j]
                    str.replace(/(.)\1*/g, function (m, $1) {
                        //$1 + m.length indica la letra y la cantidad de veces que aparece seguida,
                        // aun cuando sea: aaeaaa retorna a2, e1, a3
                        var character = $1
                        var relleno = character.repeat(3).concat(character);
                        //valido que solo cuando sea letras, no pueda ser mayor a 3 caracteres seguidos
                        if (m.length > 3 && Number.isInteger(parseInt(relleno) + parseInt(0)) == false) {
                            cadena[j] = cadena[j].replace(relleno, $1);
                            positionselecttion = positionselecttion - 3
                        }
                    });

                    retorno += cadena[j] + " ";
                }

                $(this).val(retorno);
                //posiciona el cursor donde estaba escribiendo
                $(this)[0].setSelectionRange(positionselecttion, positionselecttion);

            })

        }

        //no deja escribir comillas
        function keydowNoComilla(e, esto) {

            if (e.key == "'" || e.key == '"') {
                e.preventDefault();
                return false;
            }
            return true;
        }

        function keyupAlfanumerico(e, esto) {
            var inputValue = $(esto).val();
            inputValue = inputValue.replace(/[^a-zA-ZáéíóúÁÉÍÓÚñÑ´\w\s]/gi, '');

            $(esto).val(inputValue)
        }


        //funcion qeu define cual va a ser el texto que se va a mostrar en el boton de cargo cuando son mas de 1 firmante
        function textoBtnCargoFirm() {
            var texto = "Seleccionar firmantes y cargos";

            for (var i = 0; i < firmantesselected.length; i++) {
                if (firmantesselected[i].FIRMANTE_CARGO != undefined && firmantesselected[i].FIRMANTE_CARGO != "") {
                    //aqui entraria si el firmante tiene cargo
                } else {
                    //aqui entra si es indefinido el cargo o el cargo =="
                    texto = "Seleccionar firmantes y cargo";
                }
            }

            return texto;
        }


        /**
         * funcion que quita o pone el input de cargo de firmantes, o muestra el boton para seleccionar el principal etc
         */
        function quitOponerCargFirman() {
            var html = ''

            //si los firmantes son mayor que 1, muestro el boton para seleccionar los cargos
            var txt = "'BORRADOR'";
            var modal = "'modalcargosfirmantes'";
            var tbodycargosfirmantes = "'tbodycargosfirmantes'";
            var claseeditables = "'aEditables'";
            var borrad = "'" + borrador_id + "'";

            if (Object.keys(firmantesselected).length > 1) {

                html += '<div class="input-icon right">' +
                    '<a href="#"' +
                    ' onclick="modalSeleccCargos(' + txt + ',' + modal + ',' + tbodycargosfirmantes + ',' + claseeditables + ',' + borrad + ')" ' +
                    'id="btnselectcargos" class="btn default btn-block">' + textoBtnCargoFirm() + '</a>' +
                    '</div>';
                $('#textocargounfirmante').html('');
            } else {

                //almaceno el texto, si es que esta hablitado el input de texto
                if ($("#cargo_firmante").val() != undefined) {
                    cargofirmante = $("#cargo_firmante").val();
                } else {

                    if (Object.keys(firmantesselected).length > 0) {
                        if (firmantesselected[Object.keys(firmantesselected).length - 1].FIRMANTE_CARGO != undefined) {
                            cargofirmante = firmantesselected[Object.keys(firmantesselected).length - 1].FIRMANTE_CARGO
                        } else {
                            cargofirmante = firmantesselected[Object.keys(firmantesselected).length - 1].USUA_CARGO;
                        }
                    } else {
                        cargofirmante = '';
                    }
                }

                //si no, muestro otra vez el input de texto
                html += '<div class="input-icon right">' +
                    '' +
                    '<input type="text" name="cargo_firmante" value="' + cargofirmante + '" id="cargo_firmante"' +
                    ' class="form-control"' +
                    ' style="border: solid grey 1px" ' +
                    'placeholder="">' +
                    '</div>';
                $('#textocargounfirmante').html('Cargo: ');
            }

            $("#divcargofirmantes").html('');
            $("#divcargofirmantes").html(html);
            var cargo = $('#cargo_firmante').val();

            if (optSelectedNewBorrador != SALIDA_OFICIO &&
                cargo != undefined && (configsHasValue['CARGO_EDITABLE'] == 'NO') && (cargo.length > 3)) {

                $('#cargo_firmante').prop('readonly', true);
            } else {
                $('#cargo_firmante').prop('readonly', false);
            }
        }


        function modalExpedienteBorrad(endondeestoy) {
            //ejecuto el ajax que busca los datos
            var retorno = ajaxGetBorrador(borrador_id);
            if (retorno[1] != undefined && retorno[1].success != undefined) {

                if (retorno[1].success.borrador[0].ID_ESTADO == '7'
                    || retorno[1].success.borrador[0].ID_ESTADO == '11') {
                    alertModal('Alerta', 'El borrador ' + borrador_id + ', ya fue Radicado con el número ' +
                        retorno[1].success.borrador[0].RADI_NUME, 'warning', 30000)
                    cerrarmodborrador();
                    return false;
                } else {
                    modalexpediente(endondeestoy)
                }
            }
        }

        //verifica si un firmante esta en el arreglo firmantesselected
        function yaEstaAddFirmante(firmante) {
            var encontro = false;
            for (var i = 0; i < firmantesselected.length; i++) {
                if (firmantesselected[i].ID == firmante.ID) {
                    encontro = true;
                }
            }
            return encontro;
        }

        //cuando se selecciona un firmante de select2
        function guardarfirmantes(firmante) {

            $("#div_cargo_firmante").css('display', 'block')
            $("#cargo_firmante").val(firmante.USUA_CARGO)

            var cargo = $('#cargo_firmante').val();
            if (optSelectedNewBorrador != SALIDA_OFICIO &&
                cargo != undefined && (configsHasValue['CARGO_EDITABLE'] == 'NO') && (cargo.length > 3)) {
                $('#cargo_firmante').prop('readonly', true);
            } else {
                $('#cargo_firmante').prop('readonly', false);
            }

            var yaesta = yaEstaAddFirmante(firmante)
            if (yaesta == false) {
                firmantesselected[Object.keys(firmantesselected).length] = firmante;
            }
            quitOponerCargFirman();
        }

        //cada vez que se desmarca a un firmante, vuelvo a rellner el arreglo se los selecteds
        function quitarfirmante(firmante) {

            /**
             * lo siguiente valida que si es REPORTE4, no se pueda desmarcar el usuario que esta logueado
             */

            if (optSelectedNewBorrador == REPORTE4) {
                if (firmante.ID == codusuario) {
                    alertModal('Alerta', 'No puede eliminar este usuario', 'warning', 5000)
                    return false;
                }

                if (firmante.ID == $("#id_destinatario").val()) {
                    alertModal('Alerta', 'No puede eliminar este usuario', 'warning', 5000)
                    return false;
                }
            }

            /**
             * elimino el firmante del arreglo
             */
            delFirmanteFromArraySelected(firmante);

            if (Object.keys(firmantesselected).length > 0) {
                $("#div_cargo_firmante").css('display', 'block')
            } else {
                $("#div_cargo_firmante").css('display', 'none')
            }
            quitOponerCargFirman();
        }


        //funcion que colca selecteds los frmantes
        function selectedsFirmSelect2() {
            var selectedValues = new Array();

            for (var i = 0; i < Object.keys(firmantesselected).length; i++) {
                selectedValues[i] = firmantesselected[i].ID
                $("#cargo_firmante").val(firmantesselected[i].FIRMANTE_CARGO)
                if (firmantesselected[i].FIRMANTE_CARGO == undefined) {
                    $("#cargo_firmante").val(firmantesselected[i].USUA_CARGO)
                }

            }
            var cargo = $('#cargo_firmante').val();
            if (optSelectedNewBorrador != SALIDA_OFICIO &&
                cargo != undefined && (configsHasValue['CARGO_EDITABLE'] == 'NO') && (cargo.length > 3)) {

                $('#cargo_firmante').prop('readonly', true);
            } else {
                $('#cargo_firmante').prop('readonly', false);
            }

            setSelectedUsersFirmantes(selectedValues);
        }

        /**
         * aqui entra cada vez que se quiere subir una plantilla, bien sea por SALIDA(plantilla),
         * o las otras internas y reporte 4
         */
        function mostrarDivDescargaPlant() {
            editoroplantilla = "Plantilla";
            $("#editoroplantilla").val('Plantilla');

            if (optSelectedNewBorrador == INTERNA_SIN_PLANTILLA) {
                editoroplantilla = "SinPlantilla";
                $("#editoroplantilla").val('SinPlantilla');
            }

            $("#div_loader").css("display", "block");
            $("#edit").prop("disabled", true);

            organizarMostrarDivEditor('link2');
            if (borradorActual.DOCUMENTO_RUTA != undefined && borradorActual.DOCUMENTO_RUTA != '' &&
                borradorActual.DOCUMENTO_RUTA != null && borradorActual.DOCUMENTO_RUTA != 'NULL') {

            } else {

                mostrarInputPlant();
            }


        }


        //cuando presionan sobre una de las opciones el dropdown del boton Aprobar en el modal raiz de borrador
        function clickdropdownaprobar(event, accion) {

            if (accion == 'aprobar') {
                event.preventDefault();
                $('#comentarioaprobar').maxlength({
                    alwaysShow: true,
                    threshold: 9,
                    appendToParent: true,
                    warningClass: "label label-success",
                    limitReachedClass: "label label-danger",
                    preText: 'Mínimo ' + configsHasValue['COMENTARIOS_MINLENGTH'] + ' caracteres. ',
                    validate: true
                });
                $("#comentarioaprobar").val('');
                $("#comentarioaprobar").html('');
                $("#modal_comentario_aprobar").modal('show');
                $("#modal_comentario_aprobar").css('z-index', '9999');
                setTimeout(function () {
                    $("#comentarioaprobar").focus();

                }, 500)
            } else {
                revisar(event);
            }

        }

        //cuando le da click al boton de agregar comentario al borrador, despliega el modal.
        function modalAddComentBorrad(event) {
            event.preventDefault();
            //ejecuto el ajax que busca los datos

            if (borrador_id == ''){
                Swal.fire("Error", "Primero guardar el borrador, para hacer comentarios", "error");
                return false;
            }
            var retorno = ajaxGetBorrador(borrador_id);

            if (retorno[1] != undefined && retorno[1].success != undefined) {
                if (retorno[1].success.borrador[0].ID_ESTADO == '7'
                    || retorno[1].success.borrador[0].ID_ESTADO == '11') {
                    alertModal('Alerta', 'El borrador ' + borrador_id + ', ya fue Radicado con el número ' +
                        retorno[1].success.borrador[0].RADI_NUME, 'warning', 30000)
                    cerrarmodborrador();
                    return false;
                } else {

                    $('#comentarioaborrador').maxlength({
                        alwaysShow: true,
                        threshold: 9,
                        appendToParent: true,
                        warningClass: "label label-success",
                        limitReachedClass: "label label-danger",
                        preText: 'Mínimo ' + configsHasValue['COMENTARIOS_MINLENGTH'] + ' caracteres. ',
                        validate: true
                    });
                    $("#comentarioaborrador").val('');
                    $("#comentarioaborrador").html('');
                    $("#modal_comentario_borrador").modal('show');
                    $("#modal_comentario_borrador").css('z-index', '9999');
                    setTimeout(function () {
                        $("#comentarioaborrador").focus();

                    }, 500)
                }
            }


        }

        //cuando presionan sobre uno de los usuarios el dropdown del boton Enviar en el modal raiz de borrador
        function clickdropdownenviar(esto, usr) {

            //le digo que va a buscar en el select a este usuario adicional
            usr_especific_select_enviar = usr;
            modalEnviar();

            /**
             * Lo siguiente, agrega al select del modal de enviar, el usuario seleccionado
             */
            setTimeout(function () {
                var dropdown = $('#enviara');
                var option = new Option($(esto).attr('data-nombre_con_dependencia'), usr, true, true);
                dropdown.append(option).trigger('change');
            }, 50)

        }


        /**
         * Funcion que se ejecuta cuando hay un usuario anterior, por lo cual lo inserto antes de los demas usuarios
         * al dropdown de enviar
         */
        function add_anterior_dropdown_enviar_a() {
            var html = '<li id="li_devolver_a">' +
                ' <a href="#" onclick="btn_devolver_dropdown()">Devolver a (' + nombre_usu_anterior + ')</a>' +
                ' </li>' +
                '<li class="divider"> ' +
                '</li>';
            addUsersToDropdownEnviar(html);
        }


        /**
         * Agrega usuarios al dropdown de enviar,
         * recibe dos variables:
         *
         *  -html_to_paste_before: es un html para concatenar antes de lo que voy a agregar (se usa en agregar al usuario anterior
         *  para devolver).
         *
         *  -users_to_add: arreglo de usuarios a agregar
         */
        function addUsersToDropdownEnviar(html_to_paste_before='', users_to_add=new Array()) {
            console.log('usuarios que agrega');
            console.log(users_to_add);
            var html = '';
            html = html + html_to_paste_before;
            var ya_existe_user = false;
            var tempdropdown_enviar = dropdown_enviar;
            var cont = Object.keys(tempdropdown_enviar).length;

            /**
             * recorro los usuarios que quiero agregar, si es que el arreglo viene con datos
             */
            if (Object.keys(users_to_add).length > 0) {
                for (var i = 0; i < Object.keys(users_to_add).length; i++) {

                    ya_existe_user = false;
                    /**
                     *valido que ya existan usuarios en tempdropdown_enviar(dropdown_enviar), para asi
                     * verificar que no exista ya el usuario que quiero estoy agregando en la fila.
                     * Si el usuario no existe, entonces lo añado al arreglo tempdropdown_enviar, de ultimo.
                     */
                    if (Object.keys(tempdropdown_enviar).length > 0) {

                        for (var z = 0; z < Object.keys(tempdropdown_enviar).length; z++) {
                            if (tempdropdown_enviar[z].ID == users_to_add[i].ID) {
                                ya_existe_user = true;
                                break;
                            }
                        }
                    }

                    if (ya_existe_user == false) {
                        tempdropdown_enviar[cont] = users_to_add[i];
                        cont++;
                    }
                }
            }

            /**
             * le vuelvo a asignar el valor a dropdown_enviar, y entonces si armo el html con todos los usuarios
             */
            dropdown_enviar = tempdropdown_enviar;
            for (var i = 0; i < Object.keys(tempdropdown_enviar).length; i++) {
                html += '    <li><a href="#"  data-nombre_con_dependencia="' + dropdown_enviar[i].NOMBRE + '" ' +
                    'onclick="clickdropdownenviar(this,' + dropdown_enviar[i].ID + ')">' +
                    'Enviar a (' + dropdown_enviar[i].USUA_NOMB + ') </a>' +
                    '</li>';
            }


            $("#dropdown_enviar_a").html('');
            $("#dropdown_enviar_a").html(html);

            return html;
        }


        /**
         * funcion que elimina a un usuario del arreglo dedropdown_enviar
         */
        function delUserFromDropdownEnviar(usuario) {
            var tempdropdown = dropdown_enviar
            dropdown_enviar = new Array();
            var cont = 0;
            if (Object.keys(tempdropdown).length > 0) {
                for (var i = 0; i < Object.keys(tempdropdown).length; i++) {
                    if (tempdropdown[i].ID == usuario.ID) {
                    } else {
                        dropdown_enviar[cont] = tempdropdown[i]
                        cont++;
                    }
                }
            }
            return dropdown_enviar;
        }


        /**
         * trae los usuarios que se mostrarán en el campo Destinatario del modal de Borradores
         */
        function traerUsersDestinatarios(user_to_search='') {

            $.ajax({
                url: "process.php?operation=get_dest&borrador_id=" + borrador_id + "&optSelectedNewBorrador=" + optSelectedNewBorrador + "&search=&user_to_search=" + user_to_search + "&customB=" +customBorradorSel,
                type: 'GET',
                dataType: 'json',
                async: false,
                success: function (response) {
                    allUsersDestinatarios = response;
                },
                error: function (response) {
                    Swal.fire("Error", "Ha ocurrido un error al buscar los destinatarios", "error");
                }
            });
        }

        /**
         * Del arreglo de allUsersFirmantes, verifico si existen los id que le paso en un arreglo, y armo
         * los usuarios firmantesselected
         */

        function buildFirmSelectedByAllUsersFirmantes(arreglo) {

            firmantesselected = new Array();
            var cont = 0;
            if (Object.keys(arreglo).length > 0) {
                for (var i = 0; i < Object.keys(arreglo).length; i++) {

                    for (var z = 0; z < Object.keys(allUsersFirmantes).length; z++) {

                        if (arreglo[i] == allUsersFirmantes[z].ID) {
                            firmantesselected[cont] = allUsersFirmantes[z];
                            cont++;
                        }
                    }
                }
            }

            return firmantesselected;

        }

        function retornaConIdDestinatario(destinatario) {
            var id = destinatario.ID;
            var buscarId = id.split("-");
            destinatario.ID = buscarId[0];

            return destinatario;
        }

        /**
         * cada vez que selecciona en el select de destinatario en el modal de borradores
         */
        function clickDestinatario(destinatario) {

            destinatario = retornaConIdDestinatario(destinatario)
            if (optSelectedNewBorrador == REPORTE4) {
                /**
                 * al momento de marcar un destinarario, si el borrador==REPORTE4, mando a actualizar el
                 * arreglo de allUsersFirmantes (va a venir en la lista, el usuario seleccionado),
                 * luego los coloco como firmantesselected (el que esta en firmantes y el destinatario),
                 * Luego re-defino el select de firmantes y los marco como selecteds
                 */
                var user_to_search = new Array();
                var cont = 0;
                var userLogEstaEnFirmante = false;
                if (Object.keys(firmantesselected).length > 0) {
                    for (var i = 0; i < Object.keys(firmantesselected).length; i++) {

                        if (codusuario == firmantesselected[i].ID) {
                            userLogEstaEnFirmante = true;
                        }
                        user_to_search[cont] = firmantesselected[i].ID;
                        cont++;
                    }
                }

                if (configsHasValue['AGREGAR_PARA_EN_DE'] == 'SI') {

                user_to_search[cont] = destinatario.ID
                }
                traerUsersEnviar(user_to_search);
                buildFirmSelectedByAllUsersFirmantes(user_to_search);

                /**
                 * Lo siguiente refresca la data que tiene id_usuario_firmante
                 */
                $('#id_usuario_firmante').empty();
                $('#id_usuario_firmante').select2({
                    data: allUsersFirmantes
                });

                selectedsFirmSelect2();
                quitOponerCargFirman();
            }

            setearTipoRadicado(destinatario.TIPO_RADICADO)


            /**
             *Si optSelectedNewBorrador==REPORTE4, entonces el tipo_radicado= a lo que venga de configuracion
             */


            $("#id_destinatario").val(destinatario.ID)
            $("#tipo_destinatario").val(destinatario.TIPO_DESTINATARIO)
            $('#dest').val(destinatario.TIPO_RADICADO + " " + destinatario.NOMBRE)
            $("#cargo").val('')
            if (destinatario.USUA_CARGO != undefined) {
                $("#cargo").val(destinatario.USUA_CARGO)
            }
            var cargo = $('#cargo').val();
            if (optSelectedNewBorrador != SALIDA_OFICIO &&
                cargo != undefined && (configsHasValue['CARGO_EDITABLE'] == 'NO') && (cargo.length > 3)) {

                $('#cargo').prop('readonly', true);
            } else {
                $('#cargo').prop('readonly', false);
            }

            //este dato viene solo cuando es INTERNO, trae la dependencia del usuario
            if (destinatario.DEPE_CODI != undefined) {
                dependenDestinatario = destinatario.DEPE_CODI
            }
            var texto = destinatario.text
            var myarr = texto.split(" ");
            //Then read the values from the array where 0 is the first
            //si es externo empresa muestro para ingresa cargo
            definirPersona(myarr[0]);
        }

        /**
         * marca los usuarios en el select de firmantes (#id_usuario_firmante), que se le pasen. Puede ser 1 o varios
         */
        function setSelectedUsersFirmantes(selectedValues) {
            $("#id_usuario_firmante").val(null)
            $('#id_usuario_firmante').trigger('change.select2');

            $("#id_usuario_firmante").val(selectedValues).trigger('change.select2');
            //console.log('selectedValues', selectedValues)
        }

        /**
         * marca un usuario como seleccionado en la lista de usuario en PARA: (Destinatario)
         */
        function setSelectedUserDestinatario(selectedValues) {
            $("#selectDest").val(null)
            $('#selectDest').trigger('change.select2');

            $("#selectDest").val(selectedValues).trigger('change.select2');
            //console.log('selectedValues', selectedValues)
        }


        /**
         * funcion que verifica si un usuario esta en el arreglo de todos firmantes, y retorna su posicion y sus datos
         */
        function userIsInFirmantes(user_id) {
            var retorno = new Array();
            for (var i = 0; i < Object.keys(allUsersFirmantes).length; i++) {

                if (user_id == allUsersFirmantes[i].ID) {
                    if (Object.keys(retorno).length < 1) {
                        retorno['index'] = i;
                        retorno['datos'] = allUsersFirmantes[i];
                    }
                }
            }
            return retorno;
        }

        function definirSelectDest() {


            $("#selectDest").select2(
                {
                    width: "100%",
                    dropdownParent: $("#all_modal"),
                    allowClear: true,
                    data: allUsersDestinatarios,
                    ajax: {
                        url: "process.php?operation=get_dest&borrador_id=" + borrador_id + "&optSelectedNewBorrador=" + optSelectedNewBorrador +"&customB=" +customBorradorSel,
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                search: params.term,
                                type: 'public'
                            };
                        },
                        processResults: function (data, params) {
                            return {
                                results: data,
                            };
                        },
                        cache: true
                    },
                    placeholder: 'Buscar Usuarios',
                    escapeMarkup: function (markup) {
                        return markup;
                    }, // let our custom formatter work
                    language: languageseelct2
                }).on('select2:selecting', function (e) {

                if (optSelectedNewBorrador == REPORTE4) {
                    /**
                     * si optSelectedNewBorrador==REPORTE4, mando a desmarcar del select de firmantes,
                     * el usuario previamente seleccionado, esta almacenado en $("#id_destinatario").val()
                     */
                    var destinatario = []
                    destinatario.ID = $("#id_destinatario").val()
                    delFirmanteFromArraySelected(destinatario);
                    selectedsFirmSelect2();
                    quitOponerCargFirman();
                    /**
                     * elimino al usuario que estaba antes de seleccionar este usuario, del drowpdown de enviar
                     * y como abajo refresco el dropdown, entonces abajo se actualiza en el front
                     * */
                    delUserFromDropdownEnviar(destinatario);


                    /**
                     * Si es REPORTE4, agrego el usuario seleccionado al drowpdown de enviar
                     */
                    destinatario = retornaConIdDestinatario(e.params.args.data);
                    addUsersToDropdownEnviar('', new Array(destinatario));
                }

                //antes de que seleccione al usuario marcado(before)
                clickDestinatario(e.params.args.data);
            }).on("select2:unselecting", function (e) {
                //cada vez que desmarco una opcion
                limpiarDestinatario(e.params.args.data);
            }).on("select2:unselect", function (e) {
                //despues de que ya haya sido desmaracado una opcion

            }).trigger('change');
        }

        function definirSelectFirmantes() {

            if ($('#id_usuario_firmante').data('select2')) {
                $('#id_usuario_firmante').select2('destroy');
            }

            var urlusuariosfirm = "buscarUsuariosComen.php?traerfirmantes=SI&borrador=" + borrador_id + "&optSelectedNewBorrador=" + optSelectedNewBorrador;

            $("#id_usuario_firmante").select2(
                {
                    width: "100%",
                    multiple: true,
                    dropdownParent: $("#all_modal"),
                    allowClear: true,
                    data: allUsersFirmantes,
                    ajax: {
                        url: urlusuariosfirm,
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                search: params.term,
                                type: 'public'
                            };
                        },
                        processResults: function (data, params) {
                            return {
                                results: data,
                            };
                        },
                        cache: true
                    },
                    placeholder: 'Buscar Usuarios',
                    escapeMarkup: function (markup) {
                        return markup;
                    }, // let our custom formatter work
                    language: languageseelct2
                }).on('select2:selecting', function (e) {
                //cada vez que selecciono una opcion
                guardarfirmantes(e.params.args.data)

            }).on("select2:unselecting", function (e) {

                //se ejecuta antes de desmarcar la opcion
                quitarfirmante(e.params.args.data)

            }).on("select2:unselect", function (e) {
                //despues de que ya haya sido desmaracado una opcion
                selectedsFirmSelect2();

            }).trigger('change');
        }

        /**
         * funcion que elimina un usuario del arreglo de firmantes seleccionados
         */
        function delFirmanteFromArraySelected(firmante) {
            var tempfirmantes = firmantesselected
            firmantesselected = new Array();
            var cont = 0;
            for (var i = 0; i < Object.keys(tempfirmantes).length; i++) {
                if (tempfirmantes[i].ID == firmante.ID) {
                } else {
                    firmantesselected[cont] = tempfirmantes[i]
                    cont++;
                }
            }

            return firmantesselected;
        }

        /**
         * cuando desmarco un destinatario
         * @param destinatario
         */
        function limpiarDestinatario(destinatario) {

            destinatario = retornaConIdDestinatario(destinatario);
            if ((codusuario == borradorActual.ID_USUARIO_ACTUAL) || (borradorActual.ID_USUARIO_ACTUAL == undefined)) {
                $("#id_destinatario").val('');
                $("#tipo_destinatario").val('')

                /**
                 * si optSelectedNewBorrador==REPORTE4, elimino al usuario que acabo de desmarcar, del select de firmantes
                 */
                if (optSelectedNewBorrador == REPORTE4) {
                    delFirmanteFromArraySelected(destinatario);
                    selectedsFirmSelect2();

                    /**
                     * elimino al usuario de la lista del dropdown de enviar
                     * */
                    delUserFromDropdownEnviar(destinatario);
                    addUsersToDropdownEnviar('', new Array());

                    quitOponerCargFirman();
                }

            } else {
                $("#selectDest").val($("#id_destinatario").val() + '-' + $("#tipo_destinatario").val()).trigger('change.select2');
            }
        }

        /**
         * Setea el tipo de radicado
         * recibe:
         *  -Externo|Ciudadano
         *  -Externo|Empresa
         *  -Interno |
         */
        function setearTipoRadicado(tipo_en_text) {

            /**
             * Para los internos, el tipo_radicado= 3
             * Para los Externo es 1
             */
            var tipo_radicado = 3;

            if (tipo_en_text == 'Externo|Ciudadano' || tipo_en_text == 'Externo|Empresa') {
                tipo_radicado = 1;
            }
            
             if (customBorradorSel == true){
                 //var keytipo_rad = Object.keys(datosCustomBorrador.tipo_radicado);
                 console.log(datosCustomBorrador);
                 tipo_radicado = datosCustomBorrador.tipo_radicado[0];
             }

            $("#tipo_radicado").val(tipo_radicado);

            if (optSelectedNewBorrador == REPORTE4) {
                $("#tipo_radicado").val(reporte4_tipo)
            }
        }

        /**
         * Coloca los folios anexos, dependiendo de la cantidad de anexos que se haya subido para el borrador
         */
        function putFoliosAnexosFromCountAnexos(habilitar_input_folios_anexos) {
            var valor = '0';

            if (Object.keys(anexosBorrador).length > 0) {
                valor = Object.keys(anexosBorrador).length;
            }
            $('#folios_anexos_raiz').val(valor);
            $('#folios_anexos').val(valor);

            if(habilitar_input_folios_anexos==true){
                $('#folios_anexos_raiz').prop('readonly', true)
                $('#folios_anexos').prop('readonly', true)
            }else{
                $('#folios_anexos_raiz').prop('readonly', false)
                $('#folios_anexos').prop('readonly', false)
            }

        }

        /**
         * Retorna el id de un estado del borrador, por nombre
         */
        function getIdEstadoFromName(name) {
            var id = false;
            for (var i = 0; i < Object.keys(borradorEstados).length; i++) {
                if (borradorEstados[i]['NOMBRE_ESTADO'] == name) {
                    id = borradorEstados[i]['ID_BORRADOR_ESTADO'];
                }

                if (borradorEstados[i]['NOMBRE_TRANSACCION'] == name) {
                    id = borradorEstados[i]['ID_BORRADOR_ESTADO'];
                }
            }

            return id
        }

        /**
         *Verifica, si un usuario, le hizo algun movimiento al borrador,
         * (en enviar, se envia el usuario actual del borrador, para verificar si yan lo revisó)
         */
        function checkUsuBorradEstatus(usuario, id_estado) {

            var encontro = false;
            if (Object.keys(borradorActual.historico).length > 0) {
                for (var i = 0; i < Object.keys(borradorActual.historico).length; i++) {

                    if (
                        borradorActual.historico[i]['ID_ESTADO'] == id_estado
                        &&
                        borradorActual.historico[i]['ID_USUARIO_ORIGEN'] == usuario
                    ) {
                        encontro = true;
                    }
                }
            }

            return encontro;

        }


    </script>
    <!--  <link rel="stylesheet" type="text/css" href="https://editor-latest.s3.amazonaws.com/css/froala_editor.pkgd.min.css">
      <script type="text/javascript" src="https://editor-latest.s3.amazonaws.com/js/froala_editor.pkgd.min.js"></script>
      <link rel="stylesheet" type="text/css" href="https://editor-latest.s3.amazonaws.com/css/froala_style.min.css">-->

@endsection