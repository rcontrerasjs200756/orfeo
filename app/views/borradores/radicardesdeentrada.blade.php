@extends('layouts.app')

@section('content')


    <link href="{{$include_path}}pages/css/search.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{$include_path}}pages/css/invoice.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{$include_path}}global/plugins/typeahead/typeahead.css" rel="stylesheet" type="text/css"/>

    <link href="{{$include_path}}global/plugins/x-editable/bootstrap-editable.css"
          rel="stylesheet"/>
    <link href="{{$include_path}}global/plugins/typeahead/typeahead.js-bootstrap.css" rel="stylesheet" type="text/css"/>
    <style>
        table.dataTable thead tr th {
            word-wrap: break-word;
            word-break: break-all;
        }


    </style>


    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title" style="border-bottom: 0px !important;">
                    <div class="caption font-dark">

                        <table>
                            <tr>
                                <td>
                                    <i class="font-dark"></i>
                                    <span class="caption-subject bold uppercase">

                            Archivar <button class="btn green-haze" type="button"
                                             style=" border-radius: 20px !important; "><?= count($radicado) ?></button> <?= (count($radicado) > 1) ? "radicados" : "radicado:" ?>

                        </span>
                                    &nbsp; &nbsp;
                                </td>


                                <?php if(count($radicado) > 1){ ?>
                                <td>
                                    <select>
                                        <option>Radicados...</option>
                                        <?php
                                        foreach($radicado as $row){ ?>
                                        <option><?=  $row['radicado'] ?> <?=  substr($row['asunto'], 0, 90); ?></option>
                                        <?php }   ?>
                                    </select>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <label style="font-style: oblique;">Todos los radicados se incluirán en el mismo
                                        expediente.</label>
                                </td>
                            </tr>
                            <?php }elseif(isset($radicado[0])){ ?>
                            <td>
                                <span class="bold" style="font-size: 14pt"><?= $radicado[0]['radicado'] ?></span> <span
                                        class=""
                                        style="padding-left:1px !important;font-size: 12pt"><?= substr($radicado[0]['asunto'], 0, 90); ?></span>
                                &nbsp;
                            </td> 
                            </tr>
                            <?php }

                            ?>
                        </table>

                    </div>
                    <span class="caption-subject bold uppercase" style="float:right !important; display:none"
                          id="btn_volver_radicado">
                            <a class="btn green-haze" type="button" onclick="volverAlRadicado()"
                               style=" border-radius: 20px !important; ">Volver al Radicado</a>

                        </span>
                </div>


                <div class="portlet-body">
                    <input type="hidden"
                           id="expedincluidcoma">
                    <input type="hidden"
                           id="radicadosconcoma" value="<?=  $radicadosconcoma; ?>">
                    <form action="" method="POST" class="form-horizonal" id="forml">
                        <div class="form-body modal-body todo-task-modal-body">
                            <div class="form-actions">
                                <div class="row"></div>
                            </div>
                            <div class="row" style="">
                                <div class="col-md-12 ">

                                    <div>
                                        <div>
                                            <div class="row">
                                                <div class="col-md-12 search-page search-content-2"
                                                     id="divinputexpediente"
                                                     style="height:100px !important;min-height:100px !important">
                                                    <div class="input-group search-bar bordered">
                                                        <input type="text"
                                                               style="color: #5E6373 !important;font-size: 16px !important"
                                                               id="inputexpedientes" class="form-control typeahead"
                                                               placeholder="Buscar por número de expediente o titulo en los 2 últimos años">
                                                        <span class="input-group-btn">
                        <button class="btn blue uppercase bold" type="button" id="search-expedientes"
                                onclick="buscartodoTrue(event)">Ampliar Búsqueda</button>
                    </span>
                                                    </div>

                                                </div>

                                                <div class="col-md-12" id="divnohayexpedientes">
                                                    <div class="input-group ">

                                                    </div>

                                                </div>

                                                <div class="row" id="divbuttonbusquedaAv">
                                                    <div class="col-md-12" style="text-align: left">
                                                        <div class=" col-md-12 input-group has-warning"
                                                             style="text-align: left;display: inline-block;"
                                                             id="divApendBusqAvan">


                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="divBusquedaAvanzada" style="display:none">

                                                    <div class="form-group">
                                                        <label class=" col-md-2">Dependencia: </label>
                                                        <div class="col-md-10">
                                                            <div class="input-group input-icon right"
                                                                 style="margin-bottom: 20px !important;">


                                                                <select name="selectdependencia" class="form-control"
                                                                        id="iselectdependencia">

                                                                </select>

                                                            </div>

                                                        </div>
                                                        <label class=" col-md-2">Serie: </label>
                                                        <div class="col-md-10">
                                                            <div class="input-group input-icon right"
                                                                 style="margin-bottom: 20px !important;">

                                                                <select name="serieavanzada" class="form-control"
                                                                        id="iserieavanzada">

                                                                </select>

                                                            </div>

                                                        </div>
                                                        <label class=" col-md-2">Subserie: </label>
                                                        <div class="col-md-10">
                                                            <div class="input-group input-icon right"
                                                                 style="margin-bottom: 20px !important;">

                                                                <select name="subserieavanzada" class="form-control"
                                                                        id="isubserieavanzada">

                                                                </select>

                                                            </div>

                                                        </div>
                                                        <label class=" col-md-2">Año: </label>
                                                        <div class="col-md-10">
                                                            <div class="input-group input-icon right"
                                                                 style="margin-bottom: 20px !important;">


                                                                <select name="selectanio" class="form-control"
                                                                        id="iselectanio">

                                                                </select>

                                                            </div>

                                                        </div>

                                                        <button type="button" class="btn green"
                                                                onclick="resultAvanzados()">Buscar
                                                        </button>
                                                        <button type="button" class="btn default"
                                                                onclick="limpiarBusquedaAvanz()">Limpiar
                                                        </button>

                                                        <button type="button" class="btn green"
                                                                onclick="modalNuevoexpediente()">Nuevo Expediente
                                                        </button>
                                                        <button type="button" class="btn default"
                                                                onclick="cerrarBusquedaAvanz()">Cerrar
                                                        </button>
                                                    </div>
                                                </div>

                                                <div style="display:none" id="divbtnvariastipol">
                                                    <a href="#" onclick="modaltipologiavarios()" style="width: 100%;"
                                                       class="btn default"> Asignar tipos documentales </a>
                                                </div>
                                                <br>

                                                <div style="display:none" id="divinputtipologia">
                                                    <div class="col-md-12 search-page search-content-2">
                                                        <div class="search-bar bordered">
                                                            <div class="input-group" style="width: 100%;"
                                                                 id="divpreviotipologia">
                                                                <input type="text"
                                                                       style="color: #5E6373 !important;font-weight: 900 !important;font-size: 16px !important; width:100%;
                                                            border: 1px solid red !important;box-shadow:0 0 3px red !important;margin:10px !important"
                                                                       class="form-control typeahead"
                                                                       id="inputtipologias"
                                                                       placeholder="Elegir tipo documental">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>


                                        </div>
                                    </div>

                                    <div class="table-responsive  " id="open_table_expediente"
                                         style="position: relative;">
                                        <table class="table table-striped table-bordered table-hover
                                 table-checkable order-column dataTable no-footer" id="tablaIncexpedientes">
                                            <thead class="bg-blue font-white bold">
                                            <tr role="row">

                                                <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1"
                                                    colspan="1"
                                                    aria-label=" Id : activate to sort column ascending"
                                                    style="width: 20%;text-align:center"> Archivar
                                                </th>

                                                <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1"
                                                    colspan="1"
                                                    aria-label=" Asunto : activate to sort column ascending"
                                                    style="width: 40%;text-align:center"> Expediente
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1"
                                                    colspan="1"
                                                    aria-label=" Estado : activate to sort column ascending"
                                                    style="width: 40%;text-align:center">
                                                    Dependencia y TRD
                                                </th>

                                            </tr>
                                            </thead>
                                            <tbody id="tbodyexpediente"></tbody>
                                        </table>
                                    </div>

                                </div>

                            </div>
                        </div>


                    </form>

                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

@endsection

@section('page-modal')

    <div id="modal_nuevoexpediente" class="modal fade" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-header bg-blue-chambray bg-font-yellow text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><strong>Solicitar Nuevo Expediente</strong></h4>
                </div>
                <div class="modal-body" id="radicated_info">
                    <form action="" method="POST" class="form-horizonal" id="formk">
                        <div class="form-body modal-body todo-task-modal-body">
                            <div class="form-actions">
                                <div class="row">
                                    <span style="color:#0d0d0d;font-weight: 900;">Copiar el siguiente texto y enviarlo al correo electrónico: <?= $_SESSION['soporte_funcional'] ?></span>
                                </div>
                            </div>
                            <div class="row" style="margin-top:40px">
                                <div class="col-md-12 ">
                                    <div class="form-group has-error">

                                        <label class=" col-md-12" id="dependencianueva"> </label><br>

                                        <label class=" col-md-12" id="serienueva"></label><br>

                                        <label class=" col-md-12" id="subserienueva"></label><br>

                                        <label class=" col-md-12" id="anionuevo"></label> <br>

                                        <label class=" col-md-12"></label> <br>

                                        <label class=" col-md-12" id="nombrenuevo"></label><br>
                                        <label class=" col-md-12" id="descripcionnueva"></label>

                                        <br>
                                        <label class=" col-md-12"></label> <br>

                                        <label class=" col-md-12" id="asuntoborranuevo"></label><br>

                                        <label class=" col-md-12" id="radicadosincluirnue"></label>
                                        <br>
                                        <label class=" col-md-12"></label> <br>
                                        <label class=" col-md-12" id="usuarionuevo"></label>


                                    </div>
                                </div>

                            </div>
                        </div>


                    </form>
                </div>
                <div class="modal-footer">
                    <div class="form-actions">
                        <div class="row">


                            <div class="col-md-4">
                                <button type="button" class="btn btn-lg red" data-dismiss="modal"
                                        style="border-radius:20px !important">
                                    Cerrar
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="modalexcluirvarios" class="modal fade" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-header bg-blue-chambray bg-font-yellow text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span style="font-size: 14pt;">Seleccione los radicados que serán excluidos del expediente:</span><br>

                </div>
                <div class="modal-body">
                    <div class="form-actions">

                        <h4 class="modal-title" style="" id="spanExcVarExped"><strong>Excluir varios radicados</strong>
                        </h4>

                    </div>
                    <br>

                    <table role="presentation" class="table table-striped" border="1">
                        <thead>
                        <th>Radicado</th>
                        <th>Asunto</th>
                        <th><input type="checkbox" id="marcartodosexcluir" onclick="marcarTodosCheckExc()"></th>
                        </thead>
                        <tbody id="tbodyexcluirvarios"></tbody>
                    </table>
                    <br>
                    <div class="form-actions">

                        <span style="color: #44b6ae;" id="cuantosradexcluidos"></span>

                    </div>

                </div>
                <div class="modal-footer">
                    <div class="form-actions">
                        <div class="row">

                            <div class="col-md-4">
                                <button type="button" class="btn btn-lg blue"
                                        onclick="excluirVarios()"
                                        style="border-radius:20px !important">
                                    <i class="fa fa-arrow-right"></i>
                                    Excluir
                                </button>
                            </div>

                            <div class="col-md-4">
                                <button type="button" class="btn btn-lg red" data-dismiss="modal"
                                        style="border-radius:20px !important">
                                    Cerrar
                                </button>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bs-modal-lg in" id="newmodalexcluir" tabindex="-1" role="dialog" aria-hidden="true"
         style="display: none; padding-right: 16px;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Excluir</h4>
                </div>
                <div class="modal-body" id="modalbodynewexcluir">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-transparent red btn-outline btn-sm active"
                            onclick="beforeExcluirExpednew()">Excluir
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <div id="modaltipologiavarios" class="modal fade" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-blue-chambray bg-font-yellow text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><strong>Seleccione la tipología de cada radicado</strong></h4>
                </div>
                <div class="modal-body">

                    <br>
                    <div class="row">

                        <div class="col-md-12">
                            <table role="presentation" class="table table-striped table-bordered table-hover
                                 order-column dataTable no-footer" id="tablatipologias">
                                <thead>
                                <th>Radicado</th>
                                <th>Asunto</th>
                                <th>Tipolog&iacute;a</th>
                                </thead>
                                <tbody id="tbodytipologias"></tbody>
                            </table>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-4">
                                <button type="button" class="btn btn-lg red" data-dismiss="modal"
                                        style="border-radius:20px !important">
                                    Cerrar
                                </button>

                            </div>

                            <div class="col-md-8" style="display:none !important;" id="divputTodasTipolog">
                                <a href='#' class='putTodasTipolog' data-original-title='Ingrese una tipología'
                                   data-type='select2' data-value=''
                                   data-name='vtipologias'
                                   data-radicado='' data-title='Seleccione'></a>&nbsp;&nbsp;&nbsp;&nbsp; Aplicar la
                                misma tipolog&iacute;a a todos los radicados
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('page-css')
    <style>
        .tab-pane {
            min-height: 420px;
        }

        .tabbable-line > .nav-tabs > li.active {
            border-bottom: 4px solid #FF9900 !important;
        }

        .tabbable-line > .nav-tabs > li:hover {
            border-bottom: 4px solid rgba(255, 154, 0, 0.49) !important;
        }

        .modal-content {

            width: 1000px;
            margin: 0 auto 0 auto;
            margin-left: -150px;
        }

        .divBusquedaAvanzada {
            background: #eee;
            padding: 3px 30px;
            max-height: 400px;
            overflow-y: scroll;
        }

        .divBusquedaAvanzada p {
            cursor: pointer;
        }


    </style>
    <link href="{{$include_path}}global/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="{{$include_path}}global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"
          rel="stylesheet" type="text/css"/>

    <link href="{{$include_path}}global/plugins/froala/css/froala_editor.css" rel="stylesheet" type="text/css"/>
    <link href="{{$include_path}}global/plugins/froala/css/froala_editor.pkgd.css" rel="stylesheet" type="text/css"/>
    <link href="{{$include_path}}global/plugins/froala/css/froala_style.css" rel="stylesheet" type="text/css"/>

@endsection

@section('page-js')

    <script src="{{$include_path}}global/plugins/bootstrap-table/bootstrap-table.min.js"
            type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/bootstrap-table/locale/bootstrap-table-es-SP.js"
            type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"
            type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.es.js"
            type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/moment.min.js" type="text/javascript"></script>

    <!--  datatable -->
    <script src="{{$include_path}}global/plugins/datatable/datatable.js" type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/datatable/datatables.bootstrap.js" type="text/javascript"></script>
    <!--  datatable -->

    <script src="{{$include_path}}global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js"
            type="text/javascript"></script>

    <script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.ui.widget.js"></script>
    <!-- The Templates plugin is included to render the upload/download listings -->
    <script src="{{$include_path}}global/plugins/blueimp/js/tmpl.min.js"></script>
    <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
    <script src="{{$include_path}}global/plugins/blueimp/js/load-image.all.min.js"></script>
    <!-- The Canvas to Blob plugin is included for image resizing functionality -->
    <script src="{{$include_path}}global/plugins/blueimp/js/canvas-to-blob.min.js"></script>

    <!-- PROBANDO SIN ESTO Bootstrap JS is not required, but included for the responsive demo navigation
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>-->

    <!-- blueimp Gallery script -->
    <script src="{{$include_path}}global/plugins/blueimp/js/jquery.blueimp-gallery.min.js"></script>

    <script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.iframe-transport.js"
            type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.fileupload.js" type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.fileupload-process.js"
            type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.fileupload-image.js"
            type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.fileupload-audio.js"
            type="text/javascript"></script>

    <script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.fileupload-video.js"
            type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.fileupload-validate.js"
            type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.fileupload-ui.js"
            type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/jquery-upload/js/form-fileupload.min.js"
            type="text/javascript"></script>


    <script src="{{$include_path}}global/plugins/typeahead/typeahead.bundle.min.js"></script>
    <script src="{{$include_path}}global/plugins/x-editable/bootstrap-editable.min.js"></script>
    <script src="{{$include_path}}global/plugins/typeahead/typeaheadjs.js"></script>

    <script id="template-upload" type="text/x-tmpl"> {% for (var i=0, file; file=o.files[i]; i++) { %}
                            <tr class="template-upload fade">
                                <td>
                                    <span class="preview"></span>
                                </td>
                                <td>
                                    <p class="name">{%=file.name%}</p>
                                    <strong class="error label label-danger"></strong>
                                </td>
                                <td>
                                    <p class="size">Processing...</p>
                                    <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                                        <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                                    </div>
                                </td>
                                <td> {% if (!i && !o.options.autoUpload) { %}
                                    <button class="btn blue start" disabled>
                                        <i class="fa fa-upload"></i>
                                        <span>Subir</span>
                                    </button> {% } %} {% if (!i) { %}
                                    <button class="btn red cancel">
                                        <i class="fa fa-ban"></i>
                                        <span>Cancelar</span>
                                    </button> {% } %} </td>
                            </tr> {% } %}




















    </script>
    <script>

        <?php   $fechah = date("dmy_h_m_s") . " " . time("h_m_s");
        $session_id = session_id();
        $phpsession = session_name() . "=" . trim(session_id());
        $ent = $_SESSION['entidad'];
        $dependencia = $_SESSION['dependencia'];
        $krd = $_SESSION["krd"];
        $encabezado = "$phpsession&krd=$krd&fechah=$fechah&primera=1&ent=$ent&depende=$dependencia";
        //$encabezado2="$phpsession&krd=$krd&fechah=$fechah&primera=1&ent=$ent&depende=$dependencia";
        $usua_nomb = $_SESSION["usua_nomb"];
        $depe_nomb = $_SESSION["depe_nomb"];
        $anioactual = date('Y');
        $permisocrear = $_SESSION["usuaPermExpediente"]; //variable que indica si tiene permiso de crear expedientes
        $usua_doc = $_SESSION["usua_doc"];
        ?>

        //la siguiente variable, almacena si esta vista fue llamada desde borrador(desde la info del radicado),
        //para luego hacer el volver.
        var desdeborrador = '<?= $desde_borrador ?>'

        var adminsistema = '<?php echo $_SESSION['usua_admin_sistema'] ?>';
        var permisocrear = '<?php echo $permisocrear ?>'
        var usua_nomb = '<?php  echo $usua_nomb ?>';
        var depe_nomb = '<?php  echo $depe_nomb ?>';
        var midependencia = '<?php echo $dependencia ?>'
        var session_id = '<?php echo session_id()  ?>';
        var krd = '<?php echo $_SESSION["krd"] ?>';
        var codusuario = '<?php echo $_SESSION["usuario_id"] ?>'
        var usua_doc = '<?php  echo $usua_doc ?>';
        var radicados = new Array();
        var radicadosconcoma = "<?php  echo $radicadosconcoma ?>"; //radicados con la coma intercalada
        var radSelectedTbl = false; //este guardara el radicado que se excluira o se icluira cuando es 1 solo o se especifica 1 solo
        var base_url = '<?php echo $_SESSION['base_url']; ?>';
        var expedientesincluidos = new Array(); //almacena los expedientes incluidos
        var expedientesincluidosComa = ""; //almacena los expedientes incluidos, separados por coma

        var usua_perm_aprobar = '<?php echo $_SESSION["usua_perm_aprobar"] ?>';

        var inputexpediente = "";
        var inputtipologia = "";
        var buscartodo = false;
        var seleccexped = false; //para saber si ya hizo click al select de los expedientes
        var selecttipologia = false;//para saber si ya hizo click al select de las tipologias
        var tipologiaselected = "";
        var expedienteselected = ""; //arreglo que almacena el expediente seleccionado en el cuadro gris de buscar expediente
        var expedienteTemp = ""; //expediente seleccionado al excluir o al incluir
        var tituloExpTemp = ""//titulo del expediente seleccionado al excluir o al incluir
        var expedientestabla = new Array();
        var yaincluyouno = false; //para saber si ya incluyo uno, y habilitar la caja de tiplogias
        var tablalistacexpedientes = null;
        var misRadicadosAsociados = new Array();
        var pdf = "'pdf'"
        var tipologiaselectedVarios = ""; //tipologia seleccionada cuando se selecciona al darle click una de las tipologias
        //de cualquier select den el modal de incluir varios
        var allTipolExpedsInclu = new Array(); //cuando se abre el modal de incluir varios, busco toda slas tipologias
        //que esten en los expedientes que esten incluidos en los radicados

        //puede excluir cualquier radicado de cualquier expediente, si >0
        var usuaPermExpediente='<?= $_SESSION["usuaPermExpediente"]?>';

        var textHtmlAvanzadoUno = '<span class="help-block" style="display: inline; " >No se encontraron expedientes que coincidan con</span> ' +
            '<span style="color:#00b300;display: inline;" >' + $("#inputexpedientes").val() + '.</span>' +
            ' <span class="help-block" style="display: inline;">Puede usar la <a href="#" class="" style=""   ' +
            'onclick="modalBusquedaAvanz()">B&uacute;squeda Avanzada</a>' +
            ' o solicitar la creaci&oacute;n de un expediente ' +
            ' <a href="#" class="" style=""   ' +
            'onclick="modalNuevoexpediente()">Nuevo</a></span>';

        var textHtmlAvanzadoDos = '<span class="help-block" style="display: inline;float:right"><a href="#" class="" style=""   ' +
            'onclick="modalBusquedaAvanz()">B&uacute;squeda Avanzada</a>' +
            ' o solicitar la creaci&oacute;n de un expediente ' +
            ' <a href="#" class="" style=""   ' +
            'onclick="modalNuevoexpediente()">Nuevo</a></span>';


        //variable que almacena el texto de la ultima opcion del desplegable, cuando se escribe sobre
        //el input de buscar expedientes
        var texMasResultExped = "Para mas resultados y buscar en todos los años, clic en [Ampliar Búsqueda]";

        //almacena lo que se va escribiendo en el input de buscar expedientes
        var textKeyupInputExped = "";

        $(document).ready(function () {

            $(document).on('click', '.get_all', function () {

                resetForm();
                $.ajax({
                    url: base_url + "app/borradores/process.php",
                    method: "POST",
                    data: {operation: "get_all_info"},
                    dataType: "html",
                    success: function (response) {

                        if (response != "ERROR") {
                            var radicated_info = $('#all_info');
                            radicated_info.html('');
                            radicated_info.html(response);
                            $('#all_modal').modal({show: true, keyboard: false, backdrop: 'static'});
                        }
                    }
                }).done(function () {
                }).fail(function (error) {
                    console.log(error);
                });


            });

            llegaRadicados(<?php  echo json_encode($radicado) ?>)
            $(".dataTables_paginate").css('color', '#666 !important')
            showBtnAvanzado(textHtmlAvanzadoDos);
            modalexpediente();


        });

        /*funcion que almacena los radicados que llegan desde arriba*/
        function llegaRadicados(rads) {
            radicados = rads
        }

        //valida si un radicado fue asociado a un borraodr
        function validaYaEstaAsociado(radi_nume_radi) {
            var encontro = false;

            for (var i = 0; i < misRadicadosAsociados.length; i++) {

                if (misRadicadosAsociados[i]['RADI_NUME_RADI'] == radi_nume_radi) {
                    encontro = true;
                }

            }
            return encontro;
        }


        function resultAvanzados() {
            setTimeout(function () {
                App.blockUI({
                    boxed: !0,
                    message: 'Buscando expedientes… espere'
                })
            }, 0)
            setTimeout(function () {


                if ($("#iselectdependencia").val() != "") {

                    for (var i = 0; i < Object.keys(radicados).length; i++) {

                        //el boton buscar de busqueda avanzada
                        $.ajax({
                            url: base_url + "app/borradores/generalborrador.php",
                            method: "POST",
                            async: false,
                            data: {
                                dependencia: $("#iselectdependencia").val(),
                                serie: $("#iserieavanzada").val(),
                                subserie: $("#isubserieavanzada").val(),
                                anio: $("#iselectanio").val(),
                                function_call: "resultAvanzadosEntrada",
                                radicado: radicados[i].radicado
                            },
                            dataType: "json",
                            success: function (response) {
                                if (response.length > 0) {
                                    definirTablaExpediente();
                                    expedientestabla = new Array();
                                    buscarMisExped();
                                    recibeExpedientes(response);
                                } else {
                                    Swal.fire({
                                            title: "Alerta",
                                            text: "No se encontraron expedientes que coincidan con la búsqueda, desea solicitar la " +
                                                "creación de un expediente Nuevo",
                                            type: "warning",
                                            showCancelButton: true,
                                            confirmButtonText: "Solicitar Nuevo",
                                            cancelButtonText: "Cerrar",
                                            closeOnConfirm: true,
                                            closeOnCancel: true
                                        }).then((result) => {
                                        if (result.value) {
                                        modalNuevoexpediente();
                                    } else if (
                                        /* Read more about handling dismissals below */
                                        result.dismiss === Swal.DismissReason.cancel
                                    ) {

                                    }
                                });
                                }


                            }
                        }).done(function () {

                        }).fail(function (error) {
                            Swal.fire("Error", "Ocurrió un error al verificar si está incluido al borrador", "error");
                        });

                    }


                } else {
                    Swal.fire("Alerta", "Debe elegir como mínimo la Dependencia a buscar", "warning");
                }
                App.unblockUI()
            }, 1000)

        }

        function recibeExpedientes(response) {

            var yaestaentabla = "";

            var estado = "";
            var tamano = "";

            for (var i = 0; i < response.length; i++) {
                expedienteselected = response[i];
                yaestaentabla = validarYaestaEnTabla() //valido si ya lo agregue en la tabla


                if (expedienteselected['tipologia'] != undefined) {
                    if (expedienteselected['tipologia'] != "") {
                        inputtipologia.typeahead('val', expedienteselected['tipologia']);
                        yaincluyouno = true;
                        tipologiaselected = expedienteselected['tipologiacodigo'];

                    }
                }
                if (yaestaentabla == false) {
                    tamano = Object.keys(expedientestabla).length;
                    //lo guardo en el arreglo de expedientes que he agregado a la tabla
                    if (tamano < 1) {
                        expedientestabla[0] = {};
                        expedientestabla[0] = response[i];
                        //expedientestabla[0].TIPOLOGIA=tipologiaselected;
                    } else {
                        expedientestabla[tamano] = {};
                        expedientestabla[tamano] = response[i];
                        //expedientestabla[tamano].TIPOLOGIA=tipologiaselected;
                    }

                    estado = "";
                    if (response[i]['incluiExpe'] == "incluido") {
                        estado = "incluido";

                    } else {

                        estado = response[i].ESTADO;
                        if (response[i].ESTADO == null) {
                            estado = "Abierto"
                        }
                    }
                    armartablaexpediente(response[i], estado)
                }
            }

        }

        function resetearVariables() {

            if (inputexpediente == "") {
                inputexpediente = "";
                inputtipologia = "";
            }

            buscartodo = false;
            seleccexped = false; //para saber si ya hizo click al select de los expedientes
            selecttipologia = false;//para saber si ya hizo click al select de las tipologias
            tipologiaselected = "";
            expedienteselected = "";
            expedientestabla = new Array();
            yaincluyouno = false; //para saber si ya incluyo uno, y habilitar la caja de tiplogias
            tablalistacexpedientes = null;
        }


        function modalexpediente() {
            resetearVariables();
            cerrarBusquedaAvanz();
            buscartodo = false;
            $("#inputexpedientes").val('');
            $("#inputtipologias").val('');
            $("#tbodyexpediente").html('');
            //$("#divApendBusqAvan").css('display','none');//el enace de busqueda avanzada


            yaincluyouno = false;
            tipologiaselected = "";

            var nbaTeamsCustomSource = function (query, callback) {
                var bhQueryFn = users.ttAdapter();

                var addMyCustomResultsCB = function (results) {
                    // We modify the results from bloodhoung. It is a simple array so we can
                    // modify in any way we want
                    results.push({
                        team: 'APPENDED: My own result'
                    });

                    // and once we are done with the modifications, we call the original
                    // callback
                    callback(results);
                };

                // Call original Bloodhound query and instead of calling directly the CB,
                // we call to our own one to modify the results
                bhQueryFn(query, addMyCustomResultsCB);
            };

            var ourCustomDataSetSource = function (query, callback) {
                callback([{
                    name: 'Para mas resultados y buscar en todos los años, clic en [Ampliar Búsqueda]'
                }]);
            };
            if (inputexpediente == "") {
                inputexpediente = $('#inputexpedientes');
                inputexpediente.typeahead({
                        hint: false,
                        highlight: false,
                        minLength: 3
                    },
                    {
                        name: 'nombre',
                        source: users.ttAdapter(),
                        display: function (data) {

                            return data.NUMERO_EXPEDIENTE + ' ' + data.TITULO_NOMBRE + ' ' + data.ESTADO + ' ' + data.DEPENDENCIA + ' ' + data.TRD;
                        },
                        limit: 50
                    },
                    {
                        name: 'fixed',
                        displayKey: 'name',
                        source: ourCustomDataSetSource,
                        templates: {
                            header: '<div style="border-top: solid 1px gray; width: 100%;"></div>'
                        }
                    });

                /*
                 *    return {NUMERO_EXPEDIENTE: item.NUMERO_EXPEDIENTE, DEPENDENCIA: item.DEPENDENCIA,
                 TITULO_NOMBRE: item.TITULO_NOMBRE,DESCRIPCION:item.DESCRIPCION,
                 TRD:item.TRD,ESTADO:item.ESTADO};
                 */

                inputexpediente.bind('typeahead:change', function (ev) {

                    var selected = false;
                    var value = ev.target.value;
                    $('.tt-suggestion.tt-selectable').each(function () {
                        if (value === $(this).text()) {
                            selected = true;
                        }
                    });
                    if (!selected) {
                        //inputexpediente.typeahead('val', '');
                    }
                });


                inputexpediente.bind('typeahead:selected', function (obj, datum, name) {
                    //aqui pasa cuando se selecciona un item

                    //alert(JSON.stringify(obj)); // object
                    // outputs, e.g., {"type":"typeahead:selected","timeStamp":1371822938628,"jQuery19105037956037711017":true,"isTrigger":true,"namespace":"","namespace_re":null,"target":{"jQuery19105037956037711017":46},"delegateTarget":{"jQuery19105037956037711017":46},"currentTarget":

                    //alert(JSON.stringify(name)); // contains dataset name
                    // outputs, e.g., "my_dataset"

                    console.log('datum', datum); // contains datum value, tokens and custom fields
                    // outputs, e.g., {"redirect_url":"http://localhost/test/topic/test_topic","image_url":"http://localhost/test/upload/images/t_FWnYhhqd.jpg","description":"A test description","value":"A test value","tokens":["A","test","value"]}
                    // in this case I created custom fields called 'redirect_url', 'image_url', 'description'
                    if (datum.TITULO_NOMBRE != undefined) {
                        seleccexped = true;

                        expedienteselected = datum;
                        validarYaestaIncluido(datum);
                        inputexpediente.typeahead('val', '');
                        showBtnAvanzado(textHtmlAvanzadoDos);
                        /*if(selecttipologia==true && seleccexped==true){

                         validarYaestaIncluido(datum);
                         inputexpediente.typeahead('val', '');
                         }*/

                    } else {
                        //si entra aqui, es porque selecciono la opcion de mas resultados para busqueda avanzada,

                        $("#search-expedientes").click();
                        inputexpediente.typeahead('val', '');
                    }
                });
            }
            $('#inputexpedientes').css('background-color', '#dad8d9');

            definirinputtipologia();
            definirTablaExpediente();
            expedientestabla = new Array();
            buscarMisExped();
            //buscarExpedRadAso();


            showOpcSelecTipologia();
            //busco los incluidos
            var inludAndExclu = cuantosEnExped();
            // arreglo=0=> incluidos, 1=> cerrados, 2=>abiertos
            if (inludAndExclu[0] > 0 && desdeborrador == "1") {
                //si ya hay al menos 1 incluido, y esta ventana de radicad desdeentrada,
                //fue lamada desde borrador(desde la info del radicado), entonces habilito el boton de volver
                $("#btn_volver_radicado").css('display', 'block');

            }

        }

        /*retorna cuantos hay incluidos, abiertos y cerrados, en la tabla principal*/

        function cuantosEnExped() {

            var incluidos = 0;
            var cerrados = 0;
            var abiertos = 0;
            var retorno = new Array();
            expedientesincluidos = new Array();
            var contExpedincluidos = 0;
            expedientesincluidosComa = "";
            for (var i = 0; i < Object.keys(expedientestabla).length; i++) {

                //si el elemento i trae el indice incluiExpe es porque ya esta incluido
                if (expedientestabla[i]['incluiExpe'] != undefined && expedientestabla[i]['incluiExpe'] == "incluido") {
                    incluidos++;
                    expedientesincluidos[contExpedincluidos] = expedientestabla[i].NUMERO_EXPEDIENTE

                    expedientesincluidosComa += "'" + expedientestabla[i].NUMERO_EXPEDIENTE + "'";
                    contExpedincluidos++;
                    expedientesincluidosComa += ",";

                } else {
                    //si entra aqui es porque no esta incluido, y puede estar cerrado o abierto para incluir
                    if (expedientestabla[i]['ESTADO'] == "Abierto") {
                        abiertos++;
                    }
                    if (expedientestabla[i]['ESTADO'] == "Cerrado") {
                        cerrados++;
                    }
                }

            }
            retorno[0] = incluidos;
            retorno[1] = cerrados;
            retorno[2] = abiertos;
            $("#expedincluidcoma").val(expedientesincluidosComa);
            return retorno;
        }

        /*esta funcion valida si muestro el input oel btn de seleccionar tipologias*/
        function showOpcSelecTipologia() {
            // arreglo=0=> incluidos, 1=> cerrados, 2=>abiertos
            var incluidosExcluidos = cuantosEnExped();
            if (Object.keys(radicados).length < 2) {

                if (tipologiaselected != "") {

                    var textotipologiaactual = inputtipologia.typeahead('val');
                    $("#divinputtipologia").css('display', 'block');
                    $("#divbtnvariastipol").css('display', 'none');
                    tipologiasinborde();
                    definirinputtipologia();
                    inputtipologia.typeahead('val', textotipologiaactual);
                } else {
                    if (incluidosExcluidos[0] > 0) {
                        $("#divinputtipologia").css('display', 'block');
                        $("#divbtnvariastipol").css('display', 'none');
                        definirinputtipologia();
                    }
                }
            } else {
                if (incluidosExcluidos[0] > 0) {
                    $("#divbtnvariastipol").css('display', 'block');
                    $("#divinputtipologia").css('display', 'none');

                }
            }
        }

        function tipologiasinborde() { //pone el input de tipologia sin el borde ojo, ya que hay
            // qeu volverlo a definir para que tome el quita el borde rojo
            $("#divinputtipologia").html('');
            $("#divf").html('<div class="col-md-12 search-page search-content-2"> ' +
                '<div class="search-bar bordered"> ' +
                '<div class="input-group" style="width: 100%;" id="divpreviotipologia"> ' +
                '<input type="text" style=" color: #5E6373 !important;font-size: 16px !important; width:100%;"' +
                ' class="form-control typeahead"' +
                'id="inputtipologias" placeholder="Elegir tipo documental"> </div> </div> </div>');

        }

        //define el input que tiene las tpologias
        function definirinputtipologia() {

            inputtipologia = $('#inputtipologias');
            inputtipologia.typeahead({
                    hint: true,
                    highlight: true,
                    minLength: 0,
                },
                {
                    name: 'nombre',
                    source: tipologias.ttAdapter(),
                    display: function (data) {
                        return data.SGD_TPR_DESCRIP;
                    },
                    limit: 10000
                });

            inputtipologia.bind('typeahead:change', function (ev) {

                var selected = false;
                var value = ev.target.value;
                $('.tt-suggestion.tt-selectable').each(function () {
                    if (value === $(this).text()) {
                        selected = true;
                    }
                });
                if (!selected) {
                    // inputtipologia.typeahead('val', '');
                }
            });

            inputtipologia.bind('typeahead:selected', function (obj, datum, name) {
                //cuando se selecciona una tiplogia en la pantalla principal
                inputtipologia.typeahead('val', datum.SGD_TPR_DESCRIP);
                tipologiaselected = datum.SGD_TPR_CODIGO;
                selecttipologia = true;
                if (selecttipologia == true) {
                    var response = "";
                    for (var i = 0; i < Object.keys(radicados).length; i++) {
                        response = incluircontipologia(tipologiaselected, radicados[i].radicado);
                    }
                    if (response.success) {
                        Swal.fire(response.success, "", "success");

                        var textotipologiaactual = inputtipologia.typeahead('val');
                        tipologiasinborde();
                        definirinputtipologia();
                        inputtipologia.typeahead('val', textotipologiaactual);

                    } else {
                        Swal.fire("Error", response.error, "error");
                    }
                }
            });

            $('#inputtipologias').css('background-color', '#dad8d9');


        }

        function definirTablaExpediente() {

            $("#open_table_expediente").html('');

            $("#open_table_expediente").append('   <table class="table table-striped table-bordered table-hover ' +
                'table-checkable order-column dataTable no-footer" id="tablaIncexpedientes" > <thead class="bg-blue font-white bold"> ' +
                '<tr role="row"> <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" ' +
                'aria-label=" Id : activate to sort column ascending" style="width: 20%;text-align:center"> Archivar </th>' +
                '<th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1"' +
                ' aria-label=" Asunto : activate to sort column ascending" style="width: 40%;text-align:center"> Expediente </th> ' +
                '<th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1"' +
                ' aria-label=" Estado : activate to sort column ascending" style="width: 40%;text-align:center">' +
                'Dependencia y TRD </th> </tr> </thead> <tbody id="tbodyexpediente"></tbody> </table>');

            tablalistacexpedientes = null;
            console.log('definicion de tabla');
            tablalistacexpedientes = $('#tablaIncexpedientes').DataTable({
                autoWidth: false,
                "columns": [
                    {"width": "20%"},
                    {"width": "40%"},
                    {"width": "40%"},
                ],
                "order": [],
                "columnDefs": [{
                    "targets": 'no-sort',
                    "orderable": false,
                }],
                "language": {
                    "emptyTable": "No se encontraron registros",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ resultados",
                    "infoEmpty": "Mostrando 0 a 0 de 0 resultados",
                    "infoFiltered": "(filtrado de _MAX_ total resultados)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ resultados",
                    "loadingRecords": "Cargando...",
                    "processing": "Buscando...",
                    "search": "Buscar en los resultados:",
                    "zeroRecords": false,
                    "paginate": {
                        "first": "<<",
                        "last": ">>",
                        "next": "<",
                        "previous": ">"
                    },
                    "aria": {
                        "sortAscending": ": activar ordenar columnas ascendente",
                        "sortDescending": ": activar ordenar columnas descendente"
                    }
                }
            });
        }

        function buscarMisExped() {
            //si el borrador tiene radicados asociados, busca a ver si esos radicados estan en expedientes,
            //para mostrarlos como sugerencia en el modal de expedientes

            for (var i = 0; i < Object.keys(radicados).length; i++) {

                $.ajax({
                    url: base_url + "app/borradores/generalborrador.php",
                    method: "POST",
                    async: false,
                    data: {
                        radicado: radicados[i]['radicado'],
                        function_call: "buscarMisExpedEntrada"
                    },
                    dataType: "json",
                    success: function (response) {
                        recibeExpedientes(response);
                    }
                }).done(function () {

                }).fail(function (error) {
                    Swal.fire("Error", "Ocurrió un error al verificar si está incluido al borrador", "error");
                });
            }
        }

        function buscarExpedRadAso() {
            //si el borrador tiene radicados asociados, busca a ver si esos radicados estan en expedientes,
            //para mostrarlos como sugerencia en el modal de expedientes

            $.ajax({
                url: base_url + "app/borradores/generalborrador.php",
                method: "POST",
                async: false,
                data: {
                    borrador_id: borrador_id,
                    function_call: "buscarExpedRadAso"
                },
                dataType: "json",
                success: function (response) {
                    recibeExpedientes(response);
                }
            }).done(function () {

            }).fail(function (error) {
                Swal.fire("Error", "Ocurrió un error al verificar si está incluido al borrador", "error");
            });
        }

        function validarYaestaEnTabla() {
            console.log('expedientestabla', expedientestabla)
            //valido si ya seleccione un expediente con una tipologia ,para no duplciarlos en la tabla mostrada
            var yaestaentabla = false;
            var tamano = Object.keys(expedientestabla).length;

            if (tamano > 0) {

                for (var i = 0; i < Object.keys(expedientestabla).length; i++) {

                    if (expedientestabla[i].NUMERO_EXPEDIENTE == expedienteselected.NUMERO_EXPEDIENTE
                    ) {
                        yaestaentabla = true
                    }
                }
            }
            return yaestaentabla;

        }

        //cuando le voy a incluir una tipologia a todos los radicados
        function addTipolAtodosRad(tipologia) {
            var retorno = "";
            $.ajax({
                url: base_url + "app/borradores/generalborrador.php",
                method: "POST",
                data: {
                    radicados: radicados,
                    idtipologia: tipologia,
                    function_call: "updateTipoloTodosRad"
                },
                dataType: "json",
                async: false,
                success: function (response) {
                    retorno = response
                    if (response.success) {
                        for (var i = 0; i < Object.keys(radicados).length; i++) {
                            radicados[i]['tipologia'] = tipologia;
                        }
                        modaltipologiavarios()
                    }
                }
            }).done(function () {

            }).fail(function (error) {
                Swal.fire("Error", "Ocurrió un error al actualizar las tipologías", "error");
            });

            return retorno;
        }


        //cuando el doy click a una seleccion de tipologia, actualizo los radicados seleccionados
        function incluircontipologia(tipologia, radic) {


            var retorno = "";
            $.ajax({
                url: base_url + "app/borradores/generalborrador.php",
                method: "POST",
                data: {
                    radicado: radic,
                    idtipologia: tipologia,
                    function_call: "incluircontipologiaEntrada"
                },
                dataType: "json",
                async: false,
                success: function (response) {
                    retorno = response

                    if (response.success) {
                        for (var i = 0; i < Object.keys(radicados).length; i++) {
                            if (radic == radicados[i]['radicado']) {
                                radicados[i]['tipologia'] = tipologia;
                            }
                        }
                    }
                }
            }).done(function () {

            }).fail(function (error) {
                Swal.fire("Error", "Ocurrió un error al actualizar la tipología", "error");
            });

            return retorno;

        }

        //retorna cuantos radicados tienen tipologias asignadas
        function cuantosTienenTipolog() {
            var arr = new Array();
            var conttienen = 0;
            var contnotienen = 0;
            arr['tienen'] = conttienen;
            arr['notienen'] = contnotienen;
            console.log('radicados', radicados)
            for (var i = 0; i < Object.keys(radicados).length; i++) {
                if (radicados[i]['tipologia'] != undefined && radicados[i]['tipologia'] > 0) {
                    conttienen = parseInt(conttienen) + parseInt(1);
                    arr['tienen'] = conttienen;
                } else {
                    contnotienen = parseInt(contnotienen) + parseInt(1);
                    arr['notienen'] = contnotienen
                }
            }
            return arr;
        }

        /*
         validarYaestaIncluido
         se ejecuta cuandos e le da click a una opcion, del desplegable de expedientes
         */

        function validarYaestaIncluido() {
            console.log('radicadosradicados', radicados)
            for (var i = 0; i < Object.keys(radicados).length; i++) {
                //valido si esta en la tabla
                var yaestaentabla = validarYaestaEnTabla()
                console.log('yaestaentabla', yaestaentabla)

                if (yaestaentabla == false) {

                    $.ajax({
                        url: base_url + "app/borradores/generalborrador.php",
                        method: "POST",
                        async: false,
                        data: {
                            radicado: radicados[i]['radicado'],
                            expediente: expedienteselected.NUMERO_EXPEDIENTE,
                            //idtipologia: tipologiaselected,
                            function_call: "estaincluidoexpedienteEntrada"
                        },
                        dataType: "json",
                        success: function (response) {


                            //si no esta en la tabla entonces lo muestro, indepddientemente de
                            // si ya fue incluido el expediente al radicado
                            if (response.success == "incluido") {
                                armartablaexpediente(expedienteselected, "incluido")

                            } else {
                                armartablaexpediente(expedienteselected, expedienteselected.ESTADO)
                            }


                        }
                    }).done(function () {

                    }).fail(function (error) {
                        Swal.fire("Error", "Ocurrió un error al verificar si está incluido al radicado", "error");
                    });

                    //updatetablaexpediente();

                } else {

                    Swal.fire("Alerta", "Ya seleccionó este expediente", "warning");

                }

            }
            var tamano = Object.keys(expedientestabla).length;
            //lo guardo en el arreglo
            if (tamano < 1) {
                expedientestabla[0] = {};
                expedientestabla[0] = expedienteselected;
                //expedientestabla[0].TIPOLOGIA=tipologiaselected;
            } else {
                expedientestabla[tamano] = {};
                expedientestabla[tamano] = expedienteselected;
                //expedientestabla[tamano].TIPOLOGIA=tipologiaselected;
            }
            console.log('expedientestabla', expedientestabla)
        }

        function armartablaexpediente(datum, estado) {
            var newrow = {};
            var count = 1;

            var expediente = "'" + datum.NUMERO_EXPEDIENTE + "'";
            var tituloexped = "'" + datum.TITULO_NOMBRE + "'";
            console.log(estado);
            if (estado == "Abierto") {

                var estatus = "'Abierto'";
                newrow[0] = '<button type="button" id="add'+makeid()+'" class="btn  " ' +
                    'style="background-color: Transparent;background-repeat:no-repeat;border: none;cursor:pointer;overflow: hidden;' +
                    'outline:none;" ' +
                    'onclick="addRadExped(event,' + estatus + ',' + expediente + ')">' +
                    ' <i class="fa fa-2x fa-share font-green-jungle"></i>' +
                    '<i class="fa fa-2x fa-folder-open-o font-blue"></i>' +
                    '</button><br> ' +
                    '<a href="#" onclick="addRadExped(event,' + estatus + ',' + expediente + ')" style="font-size:13pt !important;"><span  class="font-blue font-lg bold">Incluir</span><br>Abierto</a>';
            } else if (estado == "Cerrado") {
                var estatus = "'Cerrado'";
                newrow[0] = '<button type="button" id="" class="btn btn-lg " ' +
                    'style="background-color: Transparent;background-repeat:no-repeat;border: none;cursor:pointer;overflow: hidden;' +
                    'outline:none;" ' +
                    ' disabled>' +
                    ' <i class="fa fa-2x fa-close "></i>' +
                    '<i class="fa fa-2x fa-folder "></i>' +
                    '</button><br> ' +
                    'Cerrado';
            } else if (estado == "incluido") {
                var estatus = "'incluido'";

                newrow[0] = '<span style="font-weight: 900;font-size:14pt;color: #00802b" >Ya est&aacute; incluido</span> </br>' +
                    '' +
                    ' <i class="fa fa-2x fa-reply font-yellow-gold"></i>' +
                    '<i class="fa fa-2x fa-folder-open-o font-grey-silver"></i>' +
                    '<br>' +
                    '<a href="#" onclick="validarExcluir(' + estatus + ',' + expediente + ',' + tituloexped + ')" ' +
                    ' style="font-size:13pt !important;"><span  class="font-blue font-lg bold">Excluir</span></a><br>';
            }

            newrow[count] = ' <a href="../../expediente/detalles_exp.php?num_exp=' + datum.NUMERO_EXPEDIENTE + '&par=' + encodeURIComponent(datum.TITULO_NOMBRE) + '" target="_blank"> <span class="font-blue bold">' + datum.NUMERO_EXPEDIENTE + '</span><br> ' +
                '<span class="font-grey-silver" style="color: #8f8a8d !important">' + datum.TITULO_NOMBRE + '<br>' +
                '' + datum.DESCRIPCION + '</span> </a> ';
            count++;

            newrow[count] = '<a href="../../expediente/detalles_exp.php?num_exp=' + datum.NUMERO_EXPEDIENTE + '&par=' + encodeURIComponent(datum.TITULO_NOMBRE) + '" target="_blank"> <span class="font-blue bold">' + datum.DEPENDENCIA + '</span><br> ' +
                '<span class="font-grey-silver" style="color:#8f8a8d !important">' + datum.TRD + '</span> </a> ';

            var arreglo = new Array();
            var tr = new Array();
            tr = Object.assign({}, newrow, arreglo);
            console.log(tr);
            var rowNode = tablalistacexpedientes.row.add(tr).draw().node();
        }

        function updatetablaexpediente() {
            expedienteselected = "";
            inputexpediente.typeahead('val', '');
            selecttipologia = false;
            seleccexped = false;

        }

        function cerrarBusquedaAvanz() {

            //$("#divinputtipologia").css('display','block');//muestro el input de tipologia
            $("#divbuttonbusquedaAv").css('display', 'block');//muestro el enlace de busqueda avanzada
            $(".divBusquedaAvanzada").css('display', 'none');//el div completo lo oculto
            $("#divinputexpediente").css('display', 'block')

            showOpcSelecTipologia();

        }

        function changeBusquedaAvanzada(dependencia, queactualizar) {  //cuando se abre el modal de busqueda avanzada, o cuando se hace un change de esos select

            $.ajax({
                url: base_url + "app/borradores/generalborrador.php",
                method: "POST",
                data: {
                    function_call: "busqAvaExpediente",
                    dependencia: dependencia,
                    serie: $("#iserieavanzada").val(),
                    subserie: $("#isubserieavanzada").val(),
                    anio: $("#iselectanio").val(),
                },
                dataType: "json",
                async: false,
                success: function (response) {
                    //$("#divinputtipologia").css('display','none');//oculto el input de tipologia
                    $("#divbuttonbusquedaAv").css('display', 'none');//oculto el enlace de busqueda avanzada
                    $("#divinputexpediente").css('display', 'none');

                    for (var j = 0; j < queactualizar.length; j++) {


                        if (queactualizar[j] == "dependencia") {

                            var html = "<option value=''></option>";
                            for (var i = 0; i < response.dependencias.length; i++) {
                                html += "<option value='" + response.dependencias[i]['DEPE_CODI'] + "'>" +
                                    response.dependencias[i]['DEPENDENCIA'] + " - " + response.dependencias[i]['DEPE_CODI'] + "</option>"
                            }
                            $("#iselectdependencia").html(html);
                        }


                        if (queactualizar[j] == "anio") {
                            html = "<option value=''></option>";
                            for (var i = 0; i < response.anios.length; i++) {
                                html += "<option value='" + response.anios[i]['AÑO'] + "'> " +
                                    "" + response.anios[i]['AÑO'] + "</option>"
                            }
                            $("#iselectanio").html(html);
                            //$("#selectanio").val(anioactual);
                        }


                        if (queactualizar[j] == "serie") {
                            html = "<option value=''></option>";
                            for (var i = 0; i < response.series.length; i++) {
                                html += "<option value='" + response.series[i]['SGD_SRD_CODIGO'] + "'> " +
                                    "" + response.series[i]['SERIE'] + "</option>"
                            }
                            $("#iserieavanzada").html(html);
                        }

                        if (queactualizar[j] == "subserie") {
                            html = "<option value=''></option>";
                            for (var i = 0; i < response.subseries.length; i++) {
                                html += "<option value='" + response.subseries[i]['SGD_SBRD_CODIGO'] + "'> " +
                                    "" + response.subseries[i]['SUBSERIE'] + "</option>"
                            }
                            $("#isubserieavanzada").html(html);
                        }

                        /*$('#selectdependencia').trigger('change.select2');
                         $('#selectanio').trigger('change.select2');
                         $('#serieavanzada').trigger('change.select2');
                         $('#subserieavanzada').trigger('change.select2');*/

                    }
                }
            })
        }

        function modalBusquedaAvanz() { //cuando se presiona el enlace de busqueda avanzada

            //abre la pantalla de busqueda avanzada de expedientes
            $("#iselectdependencia").html('').change()
            $("#iselectdependencia").html('').change()
            $("#iserieavanzada").val('');
            $("#isubserieavanzada").val('');

            var resultDropdown = $(".divBusquedaAvanzada");
            resultDropdown.css('display', 'block');

            $("#divinputtipologia").css('display', 'none');

            queactualizar = new Array();
            queactualizar[0] = "dependencia";
            queactualizar[1] = "serie";
            queactualizar[2] = "subserie";
            queactualizar[3] = "anio";
            definirselect2Avanzadas(); //defino los select2
            changeBusquedaAvanzada(midependencia, queactualizar); //jhago la busqueda

            $("#iselectdependencia").val(midependencia);
            $('#iselectdependencia').trigger('change.select2');

            $("#iselectanio").val($('#iselectanio option:eq(1)').val())
            $('#iselectanio').trigger('change.select2');

        }

        function definirselect2Avanzadas() { //define los select2 de los selectd e busqueda avanzada

            $("#iselectdependencia").select2(
                {
                    width: '70%',
                    allowClear: true,

                    placeholder: 'Buscar Dependencias',
                    escapeMarkup: function (markup) {
                        return markup;
                    }, // let our custom formatter work

                    language: {
                        inputTooShort: function () {
                            return 'Ingrese una dependencia para buscar';
                        },
                        noResults: function () {
                            return "Sin resultados";
                        },
                        searching: function () {
                            return "Buscando...";
                        },
                        errorLoading: function () {
                            return 'El resultado aún no se ha cargado.';
                        },
                        loadingMore: function () {
                            return 'Cargar mas resultados...';
                        },
                    }
                }).on('select2:select', function (e) {

                queactualizar = new Array();
                queactualizar[0] = "serie";
                queactualizar[1] = "subserie";
                queactualizar[2] = "anio";

                changeBusquedaAvanzada($("#iselectdependencia").val(), queactualizar);
            }).on("select2:unselect", function (e) {

                $('#iserieavanzada').html('');
                $('#isubserieavanzada').html('');
                $('#iselectanio').html('');

            }).trigger('change');

            $("#iselectanio").select2(
                {
                    width: '70%',
                    allowClear: true,

                    placeholder: 'Buscar por Año',
                    escapeMarkup: function (markup) {
                        return markup;
                    }, // let our custom formatter work

                    language: {
                        inputTooShort: function () {
                            return 'Ingrese un año para buscar';
                        },
                        noResults: function () {
                            return "Sin resultados";
                        }
                        ,
                        searching: function () {
                            return "Buscando...";
                        },
                        errorLoading: function () {
                            return 'El resultado aún no se ha cargado.';
                        },
                        loadingMore: function () {
                            return 'Cargar mas resultados...';
                        },
                    }
                }).on('select2:select', function (e) {
                queactualizar = new Array();
                changeBusquedaAvanzada($("#iselectdependencia").val(), queactualizar);
            });

            $("#iserieavanzada").select2(
                {
                    width: '70%',
                    allowClear: true,

                    placeholder: 'Buscar por serie',
                    escapeMarkup: function (markup) {
                        return markup;
                    }, // let our custom formatter work

                    language: {
                        inputTooShort: function () {
                            return 'Ingrese una serie';
                        },
                        noResults: function () {
                            return "Sin resultados";
                        },
                        searching: function () {
                            return "Buscando...";
                        },
                        errorLoading: function () {
                            return 'El resultado aún no se ha cargado.';
                        },
                        loadingMore: function () {
                            return 'Cargar mas resultados...';
                        },
                    }
                }).on('select2:select', function (e) {
                queactualizar = new Array();
                queactualizar[0] = "subserie";
                queactualizar[1] = "anio";
                $('#isubserieavanzada').html('');
                $('#iselectanio').html('');
                changeBusquedaAvanzada($("#iselectdependencia").val(), queactualizar);
            }).on("select2:unselect", function (e) {

                $('#isubserieavanzada').html('');
                $('#iselectanio').html('');
            }).trigger('change');

            $("#isubserieavanzada").select2(
                {
                    width: '70%',
                    allowClear: true,

                    placeholder: 'Buscar por sub serie',
                    escapeMarkup: function (markup) {
                        return markup;
                    }, // let our custom formatter work

                    language: {
                        inputTooShort: function () {
                            return 'Ingrese una sub serie';
                        },
                        noResults: function () {
                            return "Sin resultados";
                        },
                        searching: function () {
                            return "Buscando...";
                        },
                        errorLoading: function () {
                            return 'El resultado aún no se ha cargado.';
                        },
                        loadingMore: function () {
                            return 'Cargar mas resultados...';
                        },
                    }
                }).on('select2:select', function (e) {
                queactualizar = new Array();
                queactualizar[0] = "anio";
                $('#iselectanio').html('');
                changeBusquedaAvanzada($("#iselectdependencia").val(), queactualizar);
            }).on("select2:unselect", function (e) {
                $('#iselectanio').html('');
            }).trigger('change');
        }


        function cerrarModalExpediente() {
            //cierra el modal de expediente, pero antes valida si ya se selecciono una tipologia  si ya incluyo un expediente
            if (tipologiaselected == "" && yaincluyouno == true) {

                var encontroincluido = false;
                for (var i = 0; i < Object.keys(expedientestabla).length; i++) {
                    if (expedientestabla[i]['incluiExpe'] == "incluido") {
                        encontroincluido = true;
                    }
                }

                if (encontroincluido == true) {
                    Swal.fire("Alerta", "Debe seleccionar una tipología", "warning");
                } else {
                    $("#modal_expediente").modal('hide');
                }

            } else {
                $("#modal_expediente").modal('hide');
            }
        }

        function ajaxTienePermExcluir(numRad, expediente) {
            var tienepermiso = "";
            if(usuaPermExpediente>0){
                return true;
            }
            $.ajax({
                url: base_url + "app/expedientes/generalexpediente.php",
                method: "POST",
                async: false,
                data: {
                    radicado: numRad,
                    expediente: expediente,
                    function_call: "tienePermExcluir"
                },
                dataType: "json",
                success: function (response) {

                    if (response.success) {
                        var data = response.success
                        if (data[0].USUA_DOC == usua_doc) {
                            tienepermiso = true;
                        }

                        if (data[0].DEPE_CODI == midependencia) {
                            tienepermiso = true;
                        }

                    } else {
                        Swal.fire("Error", "Ocurrió un error al verificar si tiene el permiso para excluir", "error");
                        tienepermiso = false;
                    }

                }
            }).done(function () {

            }).fail(function (error) {
                Swal.fire("Error", "Ocurrió un error al verificar si tiene permiso, por favor comunicarse con soporte", "error");
            });

            return tienepermiso;

        }

        /*esta funcion valida si se tiene permiso para excluir este expediente*/
        //radExpecifico es por si se quiere saber de un radicado expecifico, ya que por defect recorre todo el arreglo de radicados
        function tienePermExcluir(expediente, radExpecifico = false) {

            var tienepermiso = "";
            for (var i = 0; i < Object.keys(radicados).length; i++) {

                if (radExpecifico != false) {
                    if (radicados[i]['radicado'] == radExpecifico) {
                        tienepermiso = ajaxTienePermExcluir(radExpecifico, expediente);
                        break;
                    }
                } else {
                    // si no expecifico algun radicado, hago el proceso normal
                    tienepermiso = ajaxTienePermExcluir(radicados[i]['radicado'], expediente);
                }
            }

            return tienepermiso;
        }

        /*
         * validarExcluir
         * llama esta funcion cuando se presiona sobre el boton excluir de la tabla principal, la que tiene la flecha
         * */
        function validarExcluir(estatus, expediente, tituloexped) {

            //almaceno temporalmente el expediente
            expedienteTemp = expediente;
            tituloExpTemp = tituloexped;
            if (Object.keys(radicados).length < 2) {

                radSelectedTbl = radicados[0].radicado;
                /*solo entrara aqui si solo tenemos un solo radicado*/
                if (tienePermExcluir(expediente) == true) {
                    showmodalexcluir(expediente, tituloExpTemp);
                } else {
                    Swal.fire("Alerta", "No tiene permiso para excluir el radicado del expediente. Solicitar soporte", "warning");
                }
            } else {
                /*entra aqui  cuando son mas de 1 radicados*/
                var radicadosAesteExpe = getRadicadosXexped();
                if (radicadosAesteExpe.success) {
                    procesarRadxExp(radicadosAesteExpe);
                }
            }
        }

        /*despues de buscar los radicados que pertenecen a x expediente
         * los proceso*/
        function procesarRadxExp(response) {
            var data = response.success;
            //si hay mas de dos radicados en este expediente, de los que envie, muestro el modal de excluir varios
            if (Object.keys(data).length > 1) {
                modalexcluirvarios(data);
            } else {
                if (tienePermExcluir(expedienteTemp, data[0].RADI_NUME_RADI) == true) {
                    //loalmaceno de forma global para poderlo enviar al excluir
                    radSelectedTbl = data[0].RADI_NUME_RADI;
                    showmodalexcluir(expedienteTemp, tituloExpTemp, data[0].RADI_NUME_RADI);
                } else {
                    Swal.fire("Alerta", "No tiene permiso para excluir el radicado del expediente. Solicitar soporte", "warning");
                }
            }
        }

        /*sta funcion busca cuantos radicados estan en este expediente seleccionado*/

        function getRadicadosXexped() {
            var data = false;
            $("#newmodalexcluir").modal('hide');

            $.ajax({
                url: base_url + "app/expedientes/generalexpediente.php",
                method: "POST",
                async: false,
                data: {
                    radicados: radicados, //envio todos los radicados para hacerle un IN en el query
                    expediente: expedienteTemp,
                    function_call: "getRadicadosXexped"
                },
                dataType: "json",
                success: function (response) {

                    if (response.success) {
                        data = response;
                    } else {
                        Swal.fire("Error", "Ocurrió un error al buscar los radicados asociados a este expediente", "error");
                    }

                }
            }).done(function () {

            }).fail(function (error) {
                Swal.fire("Error", "Ocurrió un error al verificar si tiene permiso, por favor comunicarse con soporte", "error");
            });

            return data;

        }

        /*cuando se confirma que se va a excluir varios*/
        function excluirVarios() {
            var response = false;
            var encontrouno = false;
            var contador = 0;
            $("#cuantosradexcluidos").html('')
            $("input[name*=checkboxexcluir]").each(function (i, valor) {
                /*recorro todos los checkde seleccion de radicados*/
                if ($(this).is(':checked')) {
                    /*si esa chekeado lo mando a excluir*/
                    response = delRadOfExped($(this).val(), expedienteTemp)
                    encontrouno = true;
                    contador++;

                }
                if (response != false) {

                }

            });
            if (encontrouno == true) {
                var radicadosAesteExpe = getRadicadosXexped();
                if (radicadosAesteExpe.success) {
                    modalexcluirvarios(radicadosAesteExpe.success);
                }
                if (contador > 0) {
                    $("#cuantosradexcluidos").html('Se excluyeron (' + contador + ') radicados con éxito');
                }
                modalexpediente();
            }

        }

        /*entra aqui cuando son varios radicados y se quieren excluir*/
        function modalexcluirvarios(data) {
            $("#spanExcVarExped").html('')
            $("#spanExcVarExped").html('<i class="fa fa-2x fa-reply font-yellow-gold"></i>' +
                '<i class="fa fa-2x fa-folder-open-o font-grey-silver"></i>' + expedienteTemp + " " + tituloExpTemp)
            $("#cuantosradexcluidos").html('')
            var html = "";
            for (var i = 0; i < Object.keys(radicados).length; i++) {

                for (var j = 0; j < Object.keys(data).length; j++) {
                    if (radicados[i]['radicado'] == data[j].RADI_NUME_RADI) {
                        html += "<tr>"
                        html += "<td>" + radicados[i]['radicado'] + "</td>"
                        html += "<td>" + radicados[i]['asunto'].substring(0, 90) + "</td>"
                        html += "<td><input type='checkbox' name='checkboxexcluir[]' value='" + radicados[i]['radicado'] + "' ></td>"
                        html += "</tr>"
                    }
                }
            }
            $("#tbodyexcluirvarios").html('');
            $("#tbodyexcluirvarios").html(html)
            $("#modalexcluirvarios").modal('show')
        }


        function showmodalexcluir(expediente, tituloexped, radExpecifico = false) {
            //levanta el modal de excluir nuevo, que tiene los colores, aqui recorro radicados, pero si especifico que es 1 solo
            // radicado pues envio al radicado especifico,

            var html = "";
            var consiguRadEspecif = false;
            for (var i = 0; i < Object.keys(radicados).length; i++) {
                if (radExpecifico != false) {
                    if (radicados[i]['radicado'] == radExpecifico) {
                        radicados[i]['radicado'] = radExpecifico;
                        consiguRadEspecif = true;
                    }
                }
                html = "<strong>El radicado </strong> <span class='label label-info label-sm'>" + radicados[i]['radicado'] + "</span> " + radicados[i]['asunto'] + " " +
                    "<strong>será Excluido del expediente: </strong><br>" +
                    "<span class='label label-warning label-sm'>" + expediente + "</span> " + tituloexped + "<br><br>" +
                    "Si el documento y sus anexos ya fueron archivados físicamente, se reportará a Gestión Documental para que " +
                    "sean retirados de dicha carpeta (expediente)."
                /*si era un rad expecifico, detengo el bucle*/
                if (consiguRadEspecif == true) {
                    break;
                }
            }
            $("#modalbodynewexcluir").html(html)
            $("#newmodalexcluir").modal('show')
        }


        /*
         * cuando confirmo  y excluyo desde el boton nuevo del modal excluir nuevo
         * */

        function confirmExcluirExpednew() {
            var response = false;
            response = delRadOfExped(radSelectedTbl, expedienteTemp)
            if (response != false) {

                if (response.success) {
                    Swal.fire(response.success, "", "success");
                } else {
                    Swal.fire("Error", response.error, "error");
                }

                setTimeout(function () {
                    excluirNew(response);
                    modalexpediente();
                }, 500)

            }
        }

        /*cuando presiono el boton excluir del modal nuevo, cuando e s1 solo radicado*/
        function beforeExcluirExpednew() {

            Swal.fire({
                    title: "Alerta",
                    text: "El radicado será excluido del expediente, está seguro?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Excluir",
                    cancelButtonText: "Cancelar",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }).then((result) => {
                if (result.value) {
                setTimeout(function () {
                    confirmExcluirExpednew();
                }, 500)
            } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
            ) {

            }
        });
        }

        /*
         * esconde el modal de excluir nuevo, (cuando e sun solo radicado)
         * */
        function excluirNew(response) {
            $("#tablaprinciexped").bootstrapTable('destroy');
            $("#newmodalexcluir").modal('hide');
            $("#tablaprinciexped").bootstrapTable('destroy');
            var ret_pay_cont = $("#tableopcontainer");
            ret_pay_cont.html('');
            ret_pay_cont.removeClass('hide');
            $('#loader_tableopcontainer').addClass('hide');
        }

        /*
         excluye un radicado de un expediente
         */
        function delRadOfExped(radicado, expediente) {
            var retorno = false;
            $.ajax({
                url: base_url + "/app/expedientes/generalexpediente.php",
                method: "POST",
                async: false,
                data: {
                    radicado: radicado, expediente: expediente,
                    function_call: "escluirRadExped"
                },
                dataType: "json",
                success: function (response) {
                    retorno = response
                }
            }).done(function () {

            }).fail(function (error) {
                Swal.fire("Error", "Ocurrio un error al incluir el expediente al borrador", "error");
            });

            return retorno;

        }

        //levanta  arma el modal de incluir varias tipologias
        function modaltipologiavarios() {
            $("#divputTodasTipolog").css('display', 'none')
            var html = "";
            for (var i = 0; i < Object.keys(radicados).length; i++) {
                html += "<tr>"
                html += "<td>" + radicados[i]['radicado'] + "</td>"
                html += "<td>" + radicados[i]['asunto'] + "</td>"

                html += "<td>"

                html += "<a href='#'  class='aEditables'" +
                    " data-original-title='Ingrese una tipología' data-type='select2'" +
                    "  data-value='" + radicados[i]['tipologia'] + "' " +
                    " data-name='vtipologias'" +
                    "  data-radicado='" + radicados[i]['radicado'] + "' " +
                    "  data-title='Seleccione'></a></td>"
                html += "</tr>"
            }

            var test = {};
            //esta funcion guarda todas las tipologias de los expdeintes de los radicados seleccionados
            test = getTipolExpedIncluidos();

            $("#tbodytipologias").html('');
            $("#tbodytipologias").html(html)

            $('#tablatipologias').DataTable({
                retrieve: true,

                columnDefs: [
                    {width: '2%', targets: 0}
                ],
                "iDisplayLength": -1,
                fixedColumns: true,
                fixedHeader: {
                    header: true,
                    footer: true
                },
                "bStateSave": false,
                "aLengthMenu": [[10, 20, 30, -1], [10, 20, 30, "Todos"]],
                //"scrollY": "300px",
                //"scrollX": true,
                // "scrollCollapse": true,,
                "language": {
                    "emptyTable": "No se encontraron registros",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ resultados",
                    "infoEmpty": "Mostrando 0 a 0 de 0 resultados",
                    "infoFiltered": "(filtrado de _MAX_ total resultados)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ resultados",
                    "loadingRecords": "Cargando...",
                    "processing": "Buscando...",
                    "search": "Búsqueda:",
                    "zeroRecords": "No se encontraron resultados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    },
                    "aria": {
                        "sortAscending": ": activar ordenar columnas ascendente",
                        "sortDescending": ": activar ordenar columnas descendente"
                    }


                },
                "fnInitComplete": function () {

                    setTimeout(function () {
                        $('div.dataTables_filter input').focus();
                    }, 5);


                },
            });

            // editable
            /*recorro cada item del editable, para definirle su selected*/
            $('a.aEditables').each(function () {

                $(this).editable({
                    inputclass: "form-control input-medium",
                    placeholder: 'Seleccione la tipología',
                    allowClear: true,
                    minimumInputLength: 0,
                    emptytext: "Seleccione",
                    source: allTipolExpedsInclu, //esta variable la llame en la funcion de arriba
                    params: function (params) {  //params already contain `name`, `value` and `pk`
                        var data = {};
                        data[0] = params.id;
                        return data;
                    },
                    //MUST be there - it won't work otherwise.
                    tpl: '<select></select>',
                    ajaxOptions: {
                        type: 'put'
                    },
                    select2: {
                        allowClear: true,
                        width: '400px',
                        //tricking the code to think its in tags mode (it isn't)
                        language: {
                            inputTooShort: function () {
                                return 'Ingrese una tipología';
                            },
                            noResults: function () {
                                return "Sin resultados";
                            },
                            searching: function () {
                                return "Buscando..";
                            },
                            errorLoading: function () {
                                return 'El resultado aún no se ha cargado.';
                            },
                            loadingMore: function () {
                                return 'Cargar mas resultados...';
                            },
                        },
                        ajax: {
                            url: base_url + 'app/borradores/gettipologia.php?function_call=tipolByExpedAndRad&expedientes='
                                + expedientesincluidosComa + '&radicados=' + radicadosconcoma,
                            dataType: "json",
                            type: 'GET',
                            processResults: function (item) {
                                test = item;
                                return {
                                    results: item
                                };
                            }
                        },
                        formatResult: function (item) {
                            return item.text;
                        },
                        templateResult: function (item) {
                            return item.text;
                        },
                        templateSelection: function (item) {
                            //cada vez que selecciono una opcion si presionar el ceck azul
                            tipologiaselectedVarios = item.SGD_TPR_CODIGO
                            return item.text;
                        },

                    },
                    success: function (response, id) {


                        //aqui etra cuando le doy al boton del check azul.
                        //response viene undefined
                        //pero id es el id del  elemeto seleccionado, con esto hago el ajax para actualizar las tipologias


                        incluircontipologia(tipologiaselectedVarios, $(this).context.dataset.radicado);
                        var contConTipolog = cuantosTienenTipolog();
                        if (Object.keys(radicados).length > 2 && contConTipolog['notienen'] >= 2) {
                            $("#divputTodasTipolog").css('display', 'block')
                        } else {
                            $("#divputTodasTipolog").css('display', 'none')
                        }


                        /* var editable = $(this).data('editable');
                         var option = editable.input.$input.find('option[value="VALUE"]'.replace('VALUE',newValue));
                         var newText = option.text();
                         $(this).attr('data-pk', newText);*/
                    },

                });
            })

            $(".putTodasTipolog").editable({
                inputclass: "form-control input-medium",
                placeholder: 'Seleccione la tipología',
                allowClear: true,
                minimumInputLength: 0,
                emptytext: "Seleccione",
                source: allTipolExpedsInclu, //esta variable la llame en la funcion de arriba
                params: function (params) {  //params already contain `name`, `value` and `pk`
                    var data = {};
                    data[0] = params.id;
                    return data;
                },
                //MUST be there - it won't work otherwise.
                tpl: '<select></select>',
                ajaxOptions: {
                    type: 'put'
                },
                select2: {
                    allowClear: true,
                    width: '400px',
                    //tricking the code to think its in tags mode (it isn't)
                    language: {
                        inputTooShort: function () {
                            return 'Ingrese una tipología';
                        },
                        noResults: function () {
                            return "Sin resultados";
                        },
                        searching: function () {
                            return "Buscando..";
                        },
                        errorLoading: function () {
                            return 'El resultado aún no se ha cargado.';
                        },
                        loadingMore: function () {
                            return 'Cargar mas resultados...';
                        },
                    },
                    ajax: {
                        url: base_url + 'app/borradores/gettipologia.php?function_call=tipolByExpedAndRad&expedientes='
                            + expedientesincluidosComa + '&radicados=' + radicadosconcoma,
                        dataType: "json",
                        type: 'GET',
                        processResults: function (item) {
                            test = item;
                            return {
                                results: item
                            };
                        }
                    },
                    formatResult: function (item) {
                        return item.text;
                    },
                    templateResult: function (item) {
                        return item.text;
                    },
                    templateSelection: function (item) {
                        //cada vez que selecciono una opcion si presionar el ceck azul
                        tipologiaselectedVarios = item.SGD_TPR_CODIGO
                        return item.text;
                    },
                },
                success: function (response, id) {
                    if (id != undefined && id != "") {
                        addTipolAtodosRad(tipologiaselectedVarios);
                    }
                },
            });


            $("#modaltipologiavarios").modal('show');
            var contConTipolog = cuantosTienenTipolog();
            console.log('contConTipolog', contConTipolog)
            if (Object.keys(radicados).length > 2 && contConTipolog['notienen'] >= 2) {
                $("#divputTodasTipolog").css('display', 'block')
            }
        }

        //todas las tipologias que esten en los expedientes que pertenezcan a los radicados seleccionados
        //en el modal de incluir varios
        function getTipolExpedIncluidos() {

            var retorno = false;
            $.ajax({
                url: base_url + "app/borradores/gettipologia.php?function_call=tipolByExpedAndRad&expedientes=" +
                    expedientesincluidosComa + '&radicados=' + radicadosconcoma,
                method: "GET",
                async: false,
                dataType: "json",
                success: function (response) {
                    //guardo todas las tipologias, para pasassela al select
                    allTipolExpedsInclu = response;
                }
            }).done(function () {

            }).fail(function (error) {
                Swal.fire("Error", "Ocurrio un error al incluir el expediente al borrador", "error");
            });

            return retorno;

        }


        /*cuando se confirma que se agregaran los radicados al o a los expedientes*/
        function ajaxAsociarExped() {
            for (var i = 0; i < Object.keys(radicados).length; i++) {

                $.ajax({
                    url: base_url + "app/borradores/generalborrador.php",
                    method: "POST",
                    async: false,
                    data: {
                        radicado: radicados[i]['radicado'], expediente: expedienteTemp, idtipologia: tipologiaselected,
                        function_call: "addexpedienteborraEntrada"
                    },
                    dataType: "json",
                    success: function (response) {
                        if (response.success) {

                                if (tipologiaselected != "") {
                                    alertModal('Incluido en expediente',response.success, 'success', '10000')
                                } else {
                                    /**
                                     * Entra aqui si no ha seleccionado la tipologia
                                     */
                                    alertModal('Incluido en expediente', 'Ahora elija el Tipo Documental', 'success', '10000')

                                }

                            yaincluyouno = true;
                            $("#divinputtipologia").css('display', 'block');
                        } else {
                            Swal.fire("Error", response.error, "error");
                        }
                    }
                }).done(function () {

                }).fail(function (error) {
                    Swal.fire("Error", "Ocurrio un error al incluir el expediente al borrador", "error");
                });
            }
        }

        /*
         * addRadExped
         * se ejecuta cuand se presiona sobre el boton incluir de la tabla principal, la que tine la flecha
         */
        function addRadExped(evento, estatus, expediente) {
            expedienteTemp = expediente
            evento.preventDefault();
            if (estatus == "Cerrado") {
                Swal.fire("Espere", "Expediente Cerrado - Debe solicitar soporte para su apertura o la creación de " +
                    "un nuevo expediente", "warning");
            } else {

                if (Object.keys(radicados).length > 1) {
                    Swal.fire({
                            title: "Advertencia",
                            text: "Se incluirán (" + Object.keys(radicados).length + ") radicados en este Expediente",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonText: "Incluir Todos",
                            cancelButtonText: "Cancelar",
                            closeOnConfirm: true,
                            closeOnCancel: true
                        }).then((result) => {
                        if (result.value) {
                        ajaxAsociarExped();
                        modalexpediente();
                        setTimeout(function () {
                            $("#inputtipologias").css('border', '1px solid red !important');
                            $("#inputtipologias").css('box-shadow', '0 0 3px red !important');
                            $("#inputtipologias").css('margin', '10px !important');
                        }, 500);
                        afterAsocExped();
                    } else if (
                        /* Read more about handling dismissals below */
                        result.dismiss === Swal.DismissReason.cancel
                    ) {

                    }
                });
                } else {
                    ajaxAsociarExped();
                    modalexpediente();
                    setTimeout(function () {
                        $("#inputtipologias").css('border', '1px solid red !important');
                        $("#inputtipologias").css('box-shadow', '0 0 3px red !important');
                        $("#inputtipologias").css('margin', '10px !important');
                    }, 500);
                }

            }

        }

        function afterAsocExped() {
            if (Object.keys(radicados).length > 1) {
                modaltipologiavarios()
            }
        }

        function limpiarBusquedaAvanz() {

            //cuandos e presiona el boton limpiar  en el modal de expedientes avanzados
            queactualizar = new Array();
            queactualizar[0] = "dependencia";
            //queactualizar[1] = "serie";
            //queactualizar[2]="subserie";
            //queactualizar[3]="anio";

            $('#iselectdependencia').html('');
            $('#iserieavanzada').html('');
            $('#isubserieavanzada').html('');
            $('#iselectanio').html('');
            changeBusquedaAvanzada(midependencia, queactualizar); //hago la busqueda

            //defino la tabla nuevaente y vuelvo a cargar los datos de la tabla
            definirTablaExpediente();
            expedientestabla = new Array();
            buscarMisExped();
        }

        function modalNuevoexpediente() {

            $("#modal_nuevoexpediente").modal('show');
            $("#dependencianueva").html("<span style='color:#2458E1'>Dependencia:  </span>" + $("#iselectdependencia option:selected").text());
            $("#serienueva").html("<span style='color:#2458E1'>Serie: </span>" + $("#iserieavanzada option:selected").text());
            $("#subserienueva").html("<span style='color:#2458E1'>Subserie: </span>" + $("#isubserieavanzada option:selected").text());
            $("#anionuevo").html("<span style='color:#2458E1'>Año:</span>  ")
            $("#nombrenuevo").html("<span style='color:#2458E1'>Nombre del expediente: <span style='color:#0d0d0d;font-weight: 900'>Escriba aquí la información correspondiente</span>");
            $("#descripcionnueva").html("<span style='color:#2458E1'>Borrador asunto: </span> ");
            $("#radicadosincluirnue").html("<span style='color:#2458E1'>Radicados:</span> Escribir aquí los radicados que desea incluir en el nuevo expediente (si aplica)");
            $("#usuarionuevo").html("<span style='color:#2458E1'>Usuario:</span>  " + usua_nomb + " - " + depe_nomb);

        }

        //esta accion vuelve a la ventana anterior, en caso de ser uniframe, devuelve al iframe anterior
        //usado para cuando desde entrada, no tiene un expediente,
        //se incluye y luego aparece el boton de volver
        function volverAlRadicado() {

            window.history.back();

        }


        function marcarTodosCheckExc() {

            $("input[name*=checkboxexcluir]").each(function (i, valor) {
                /*recorro todos los checkde seleccion de radicados*/
                $(this).prop('checked', true);
            });
        }


    </script>


    <script>

        //esto busca los xpedientes
        var users = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace,
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: base_url + "app/borradores/getexpedientes.php?buscartodo=false&key=%QUERY",//"getexpedientes.php?buscartodo=&key=%QUERY",
                /* ajax : {
                 beforeSend: function(jqXhr, settings){
                 settings.data = $.param({function_call:"buscardatosexpediente",key:"%QUERY"})
                 },
                 type: "POST"

                 },*/
                /* prepare: function (settings) {
                 settings.type = "POST";
                 settings.contentType = "application/json; charset=UTF-8";
                 settings.data = JSON.stringify(query);
                 return settings;
                 },*/
                filter: function (x) {
                    if (x.length < 1) {
                        showBtnAvanzado(textHtmlAvanzadoUno)

                    } else {
                        // $("#divbuttonbusquedaAv").css('display', 'none');
                    }
                    return $.map(x, function (item) {


                        var opciones = {
                            NUMERO_EXPEDIENTE: item.NUMERO_EXPEDIENTE, DEPENDENCIA: item.DEPENDENCIA,
                            TITULO_NOMBRE: item.TITULO_NOMBRE, DESCRIPCION: item.DESCRIPCION,
                            TRD: item.TRD, ESTADO: item.ESTADO
                        }

                        return opciones;

                    });
                },
                replace: function () {
                    textKeyupInputExped = inputexpediente.typeahead('val')
                    var q = base_url + "app/borradores/getexpedientes.php?buscartodo=" + getbuscartodo() + "&key=" + encodeURIComponent(inputexpediente.typeahead('val'));
                    console.log(inputexpediente.typeahead('val'));
                    return q;
                },

                wildcard: "%QUERY"
            }

        });

        var tipologias = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace,
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: base_url + "app/borradores/gettipologia.php?function_call=tipolByExpedAndRad&expedientes=",
                replace: function (query, tipeando) {
                    //tipeando es lo que estoy escribiendo sobre el input

                    var q = query; //la url quee sta arriba
                    //asi es que le paso los expedientes incluidos
                    if ($('#expedincluidcoma').val()) {
                        q += encodeURIComponent($('#expedincluidcoma').val());
                    }

                    q += '&radicados=' + $('#radicadosconcoma').val()
                    q += "&q=" + encodeURIComponent(tipeando);
                    return q;
                },
                filter: function (x) {
                    return $.map(x, function (item) {

                        var opciones = {SGD_TPR_CODIGO: item.SGD_TPR_CODIGO, SGD_TPR_DESCRIP: item.SGD_TPR_DESCRIP}

                        return opciones;

                    });
                },

                wildcard: "%QUERY"
            }

        });
        users.initialize();
        tipologias.initialize();

        function getbuscartodo() {
            return buscartodo;
        }

        /*muestra los botons de hacer la busqueda avanzada*/
        function showBtnAvanzado(html) {
            $("#divApendBusqAvan").html(html)
            $("#divApendBusqAvan").css('display', 'block');
            $("#divbuttonbusquedaAv").css('display', 'block')
        }


        /*cuando se presioan sobre el boton ampliar busqueda*/
        function buscartodoTrue(evento) {
            evento.preventDefault();
            if (inputexpediente.typeahead('val') == texMasResultExped) {
                inputexpediente.typeahead('val', textKeyupInputExped)
            }
            buscartodo = true;
            var valor = $("#inputexpedientes").val();
            Swal.fire("Espere", "Por favor Espere mientras buscamos los resultados", "warning");
            var url = base_url + "app/borradores/getexpedientes.php?buscartodo=true&key=" + encodeURIComponent(inputexpediente.typeahead('val'));
            $.ajax({
                url: url,
                method: "GET",
                dataType: "json",
                async: false,
                success: function (response) {
                    if (response.length > 0) {
                        recibeExpedientes(response);

                    } else {

                        showBtnAvanzado(textHtmlAvanzadoUno)
                    }
                    setTimeout(function () {
                        swal.close();
                    }, 1000)


                }
            }).done(function () {

            }).fail(function (error) {
                Swal.fire("Error", "Ocurrió un error al verificar si está incluido al borrador", "error");
            });
            buscartodo = false;
            $("#divApendBusqAvan").css('display', 'block');
        }

        $(document).on('click', '#deactivate-user', function (e) {
            e.preventDefault();
            return false;
        });

        $(document).on('click', '.show-search', function () {
            inputexpediente.typeahead('val', '');
            $('.search-page.search-content-2').removeClass('hide');
            $('.invoice').html('').addClass('hide');
        });

        function makeid() {
            var text = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < 5; i++){
            text += possible.charAt(Math.floor(Math.random() * possible.length));
            return text; }
        }


    </script>






    <!--  <link rel="stylesheet" type="text/css" href="https://editor-latest.s3.amazonaws.com/css/froala_editor.pkgd.min.css">



      <script type="text/javascript" src="https://editor-latest.s3.amazonaws.com/js/froala_editor.pkgd.min.js"></script>



      <link rel="stylesheet" type="text/css" href="https://editor-latest.s3.amazonaws.com/css/froala_style.min.css">-->

@endsection