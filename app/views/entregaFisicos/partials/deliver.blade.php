<div class="row margin-top-20">
    <div class="col-md-6 col-md-offset-3 text-center">
        <div class="hide" id="rad_num">
            @if(isset($type) && ($type == "a" || $type == "anexed"))
                {{$data["RADICADO"].'00001'}}
            @elseif(isset($type) && ($type == "b1" || $type == "b1_state" || $type == "b2"))
                {{$data["ANEXO_CODIGO"]}}
            @endif
        </div>
        @if(isset($obs))
        <div class="hide" id="b1_state_obs">{{$obs}}</div>
        @endif
        <div class="hide" id="op_type">{{$type}}</div>
        <!-- BEGIN Portlet PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-green-sharp">
                    <span class="caption-subject"> {{$data["RADICADO"]}}</span>
                    <span class="caption-helper">{{' del '.\Carbon\Carbon::parse($data["FECHA_RADI"])->format('Y-m-d H:i') }}</span>
                </div>
                <div class="actions">
                    <div class="btn-group">
                        <a href="javascript:;" id="deliver-btn" class="btn btn-circle red-sunglo bold" data-toggle="tooltip" data-title="Agregar al listado de documentos a entregar">
                            <i class="fa fa-plus"></i> Entregar
                        </a>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                {{ucfirst(strtolower($data["ASUNTO"]))}}
            </div>
        </div>
        <!-- END Portlet PORTLET-->
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        @if(isset($anex_count) && count($anex_count) == 0)
            <div class="note note-warning">
                <h4 class="block">El radicado No tiene ningún anexo actualmente.</h4>
            </div>
        @elseif(isset($type) && $type != "anexed")
            <div class="mt-element-step">
                <div class="row step-line">
                    <div class="col-md-{{(\App\Helpers\MiscFunctions::check_scanned($data["RADICADO"]) > 0)?'2 col-md-offset-1': '3'}} first mt-step-col {{((isset($data['ESTADO']) && $data['ESTADO'] <= 1) || (isset($data['ESTADO']) && $data['ESTADO'] > 1))?'active': ''}}">
                        <div class="mt-step-number bg-white {{((isset($data['ESTADO']) && $data['ESTADO'] <= 1) || (isset($data['ESTADO']) && $data['ESTADO'] > 1))?'': 'font-grey-steel'}}">
                            <i class="glyphicon glyphicon-check"></i>
                        </div>
                        <div class="mt-step-title uppercase font-grey-cascade">Anexado</div>
                        <div class="mt-step-content font-grey-cascade">Plantilla</div>
                    </div>
                    <div class="col-md-{{(\App\Helpers\MiscFunctions::check_scanned($data["RADICADO"]) > 0)?'2': '3'}} mt-step-col {{((isset($data['ESTADO']) && $data['ESTADO'] < 3) || (isset($data['ESTADO']) && $data['ESTADO'] > 2))?'active': ''}}">
                        <div class="mt-step-number bg-white {{((isset($data['ESTADO']) && $data['ESTADO'] < 3) || (isset($data['ESTADO']) && $data['ESTADO'] > 2))?'': 'font-grey-steel'}}">
                            <i class="glyphicon glyphicon-barcode"></i>
                        </div>
                        <div class="mt-step-title uppercase font-grey-cascade">Radicado</div>
                        <div class="mt-step-content font-grey-cascade">Combinado</div>
                    </div>
                    <div class="col-md-{{(\App\Helpers\MiscFunctions::check_scanned($data["RADICADO"]) > 0)?'2': '3'}} mt-step-col {{((isset($data['ESTADO']) && $data['ESTADO'] > 2) && (isset($data['ESTADO']) && $data['ESTADO'] <= 4))?'active': ''}}">
                        <div class="mt-step-number bg-white {{((isset($data['ESTADO']) && $data['ESTADO'] > 2) && (isset($data['ESTADO']) && $data['ESTADO'] <= 4))?'': 'font-grey-steel'}}">
                            <i class="glyphicon glyphicon-list-alt"></i>
                        </div>
                        <div class="mt-step-title uppercase font-grey-cascade">Impreso</div>
                        <div class="mt-step-content font-grey-cascade">Físico Entregado</div>
                    </div>
                    <div class="col-md-{{(\App\Helpers\MiscFunctions::check_scanned($data["RADICADO"]) > 0)?'2': '3 last'}} mt-step-col {{(isset($data['ESTADO']) && $data['ESTADO'] == 4)?'active': ''}}">
                        <div class="mt-step-number bg-white {{(isset($data['ESTADO']) && $data['ESTADO'] == 4)?'': 'font-grey-steel'}}">
                            <i class="glyphicon glyphicon-envelope"></i>
                        </div>
                        <div class="mt-step-title uppercase font-grey-cascade">Enviado</div>
                        <div class="mt-step-content font-grey-cascade">Destinatario</div>
                    </div>
                    @if(\App\Helpers\MiscFunctions::check_scanned($data["RADICADO"]) > 0)
                        <div class="col-md-2 mt-step-col active last">
                            <div class="mt-step-number bg-white">
                                <i class="glyphicon glyphicon-file"></i>
                            </div>
                            <div class="mt-step-title uppercase font-grey-cascade">Escaneado</div>
                        </div>
                    @endif
                </div>
            </div>
        @endif
    </div>
</div>