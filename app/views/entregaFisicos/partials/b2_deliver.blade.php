<div class="row margin-bottom-10 margin-top-20">
    <div class="col-md-10 col-md-offset-1 text-center">
        <h4 class="bold">Listado de Anexos, elija un Formato, Plantilla o Anexo Principal</h4>
    </div>
</div>
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <table id="rad_table"
               data-striped="true"
               data-pagination="true"
               data-page-size="5">
            <thead>
            <tr>
                <th>Radicado</th>
                <th>Código de Anexo</th>
                <th>Fecha</th>
                <th>Descripción de Anexo</th>
                <th>Formato</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($data as $key => $row)
                <tr>
                    <td>{{$row->RADICADO}}</td>
                    <td>{{$row->ANEXO_CODIGO}}</td>
                    <td>{{date_format(date_create($row->FECHA_RADI),'Y-m-d H:i')}}</td>
                    <td>{{$row->DESCRIPCION_ANEXO}}</td>
                    <td>{{\App\Helpers\MiscFunctions::get_anex_type($row->ANEX_TIPO)}}</td>
                    <td>
                        <button type="button" class="btn btn-xs blue-sharp deliver-confirm" anex_code="{{$row->ANEXO_CODIGO}}" data-toggle="confirmation" data-singleton="true" data-popout="true" data-btn-ok-label="Si" data-btn-cancel-label="No" data-title="Formato, Plantilla o Anexo Principal">
                            Agregar
                        </button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
<div class="row margin-bottom-40"></div>