<div class="col-md-8 col-md-offset-2">
    <!-- BEGIN Portlet PORTLET-->
    <div class="portlet light">
        <div class="portlet-body form">
            <form action="process.php" method="POST" class="form-horizonal" id="search-form">
                <div class="form-body">
                    <div class="row margin-top-40">
                        <div class="col-md-12 text-center">
                            <i class="fa fa-barcode fa-4x"></i>
                        </div>
                    </div>
                    <div class="row margin-top-20">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="form-group">
                                <label class="control-label col-md-2">Radicado: </label>
                                <div class="col-md-10">
                                    <div class="input-icon right">
                                        <i class="fa"></i>
                                        <input type="text" name="number" value="" id="number" class="form-control" placeholder="Lectura del número de radicado..."/>
                                        <input type="hidden" name="process_type" id="process_type" value=""  />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-5 col-md-4">
                            <button type="submit" class="btn blue-sharp">
                                <i class="fa fa-search"></i>
                                Buscar
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- END Portlet PORTLET-->
</div>