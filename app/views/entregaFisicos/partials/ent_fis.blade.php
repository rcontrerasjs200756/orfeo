@if(count($delivered) == 0)
    <div class="note note-danger hide" id="search_notice">
        <h4 class="block">Este es un radicado de Entrada</h4>
        <p>No se puede modificar la imagen principal, sólo se permite entrega de Anexos.</p>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            <a href="javascript:;" id="deliver" class="btn font-white deliver-button hide" data-toggle="tooltip" data-title="Proceder a firmar la entrega de todos los elementos seleccionados" data-placement="right">
                <div class="font-white margin-top-15"><strong> Entregar </strong></div>
                <span class="font-hg bold" id="delivered-count">0</span>
            </a>
        </div>
    </div>
    <div class="row" id="searchbox-container">
        <div class="col-md-8 col-md-offset-2">
            <!-- BEGIN Portlet PORTLET-->
            <div class="portlet light">
                <div class="portlet-body form">
                    <form action="process.php" method="POST" class="form-horizonal" id="search-form">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <i class="fa fa-barcode fa-4x"></i>
                                    <i class="fa fa-barcode fa-4x"></i>
                                    <i class="fa fa-barcode fa-4x"></i>
                                    <i class="fa fa-barcode fa-4x"></i>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8 col-md-offset-2">
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Radicado: </label>
                                        <div class="col-md-10">
                                            <div class="input-icon right">
                                                <i class="fa"></i>
                                                <input type="text" name="number" value="" id="number" class="form-control" placeholder="Lectura del número a buscar..."/>
                                                <span class="help-block font-red hide" id="process-error"></span>
                                                <input type="hidden" name="process_type" id="process_type" value=""  />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-5 col-md-4">
                                    <button type="submit" class="btn blue-sharp">
                                        <i class="fa fa-search"></i>
                                        Buscar
                                    </button>
                                </div>
                            </div>
                        </div>
                        -->
                    </form>
                </div>
            </div>
            <!-- END Portlet PORTLET-->
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="mt-element-step">
                <div class="row step-background">
                    <div class="col-md-4 bg-grey-steel mt-step-col col-md-offset-2 margin-right-10 rounded-4 process" process_type="1">
                        <a style="text-decoration:none; !important">
                            <div class="mt-step-number" style="font-size: 150px !important;">
                                <i class="glyphicon glyphicon-file"></i>
                            </div>
                            <div class="mt-step-title uppercase font-grey-cascade">Radicado</div>
                            <div class="mt-step-content font-grey-cascade">Documento principal</div>
                        </a>
                    </div>
                    <div class="col-md-4 bg-grey-steel mt-step-col rounded-4 process" process_type="2">
                        <a style="text-decoration:none; !important">
                            <div class="mt-step-number" style="font-size: 150px !important; right: 10px;">
                                <i class="glyphicon glyphicon-paperclip"></i>
                            </div>
                            <div class="mt-step-title uppercase font-grey-cascade">Anexo</div>
                            <div class="mt-step-content font-grey-cascade">Anexo de un radicado</div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="delivered-count-container hide">
        <form id="delivered-count-form" method="POST" action="process.php"></form>
    </div>
    <div id="radicated-container"></div>
@elseif(count($delivered) > 0)
    <div class="note note-warning">
        <h4 class="block">Documentos <strong>entregados</strong> correctamente</h4>
        <p> Ahora Gestión Documental debe confirmar la <strong>recepción</strong> de los documentos.</p>
    </div>
@endif