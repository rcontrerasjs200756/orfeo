@if(count($data) == 0)
    <div class="note note-warning" id="document-mng">
        <h4 class="block note-title">No hay documentos pendientes por recibir.</h4>
    </div>
@else
<div class="row">
    <div class="col-md-12 text-center">
        @if(count($data) > 0)
        <h3>
            Documentos entregados por: {{(isset($data) && isset($data[0]['USUARIO']))? '<strong>'.\App\Helpers\MiscFunctions::get_user_data($data[0]['USUARIO'])['USUA_NOMBRE'].'</strong>': ''}}
        </h3>
        @endif
    </div>
</div>
<div class="row margin-top-40">
    <div class="col-md-10 col-md-offset-1 margin-bottom-20">
        <table id="doc_mng_table"
               data-striped="true"
               data-pagination="true"
               data-page-size="5"
               data-search="true"
               data-sort-name="codigo-anexo"
               data-sort-order="asc"
               data-locale="es-SP">
            <thead>
            <tr>
                <th data-width="10%" data-align="center">Radicado</th>
                <th data-field="codigo-anexo" data-align="center" data-sortable="true" data-width="20%">Código de Anexo</th>
                <th data-width="20%" data-align="center">Fecha</th>
                <th data-width="30%" data-align="center">Descripción de Anexo</th>
                <th data-width="9%" data-align="center">Fisico Radicado</th>
                <th data-width="9%" data-align="center">Fisico Anexo</th>
                <th data-width="2%" data-align="center"></th>
            </tr>
            </thead>
            <tbody>
            @foreach($data as $key => $row)
                <tr>
                    <td>{{$row["RADICADO"]}}</td>
                    <td>{{$row["ANEX_CODIGO"]}}</td>
                    <td>{{\Carbon\Carbon::parse($row["FECHA_ENTREGA"])->format('d-m-Y H:i')}}</td>
                    <td>{{$row["DESCRIPCION"]}}</td>
                    <td>
                        @if($row["PROCESO"] == "radicated")
                            <i class="fa fa-check fa-2x"></i>
                        @else
                            -
                        @endif
                    </td>
                    <td>
                        @if($row["PROCESO"] == "anexed")
                            <i class="fa fa-check fa-2x"></i>
                        @else
                            -
                        @endif
                    </td>
                    <td>
                        <input type="checkbox" class=".receive" value="{{$row['ANEX_CODIGO']}}" name="receive[]" checked/>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div class="row margin-top-40">
        <div class="col-md-12 text-center">
            <button type="button" class="btn btn-primary" id="receive-anex">
                Recibir
            </button>
            <button type="button" class="btn red" id="cancel-receive" data-toggle="confirmation" data-singleton="true" data-popout="true" data-btn-ok-label="Si" data-btn-cancel-label="No" data-title="Cancelar?">
                Cancelar Entrega
            </button>
        </div>
    </div>
</div>
@endif