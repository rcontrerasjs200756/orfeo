<div id="a-condition" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue-chambray bg-font-yellow text-center">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><strong>Atencion</strong></h4>
            </div>
            <form id="a_cond_form" method="POST" action="process.php">
                <div class="modal-body text-center">
                    <h4>El radicado no tiene anexos, desea hacer la entrega <strong>Sin Plantilla</strong>?</h4>
                    <input type="hidden" name="data" id="data" value="">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline process-cancel">Cancelar</button>
                    <button type="submit" class="btn green a-accept">Aceptar</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div id="b1-condition" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue-chambray bg-font-yellow text-center">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><strong>Atencion</strong></h4>
            </div>
            <form id="b1_cond_form" method="POST" action="process.php">
                <div class="modal-body text-center">
                    <h4>Ya fue entregado el documento principal del radicado. Desea modificarlo?</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline process-cancel">Cancelar</button>
                    <button type="submit" class="btn green b1-accept">Aceptar</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div id="b1-mod-obs" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue-chambray bg-font-yellow text-center">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><strong>Por favor, ingrese descripción para la modificación</strong></h4>
            </div>
            <form id="b1_obs_form" method="POST" action="process.php">
                <div class="modal-body text-center">
                    <div class="form-group">
                        <label>Descripción</label>
                        <textarea class="form-control" rows="3" name="b1_obs" id="b1_obs" required></textarea>
                    </div>
                    <input type="hidden" name="data" id="b1_data" value="">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline process-cancel">Cancelar</button>
                    <button type="submit" class="btn green b1-obs-accept">Agregar</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div id="anexed-obs" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue-chambray bg-font-yellow text-center">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><strong>Por favor, ingrese descripción para entrega de anexo</strong></h4>
            </div>
            <form id="anexed_obs_form" method="POST" action="process.php">
                <div class="modal-body text-center">
                    <div class="form-group">
                        <label>Descripción</label>
                        <textarea class="form-control" rows="3" name="anex_obs" id="anex_obs" required></textarea>
                    </div>
                    <input type="hidden" name="anex_number" id="anex_number" value="">
                    <input type="hidden" name="anex_op" id="anex_op" value="">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline process-cancel">Cancelar</button>
                    <button type="submit" class="btn green a-accept">Agregar</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div id="deliver_signature" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue-chambray bg-font-yellow text-center">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><strong>Ingrese usuario y contraseña para firmar la entrega</strong></h4>
            </div>
            <form id="adeliver_signature_form" method="POST" action="process.php" class="form-horizontal">
                <div class="modal-body text-center">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="user" id="user" placeholder="Usuario"/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input class="form-control" type="password" name="pass" id="pass" placeholder="Contraseña"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h5 class="hide font-red" id="signature-error">Las credenciales proporcionadas son incorrectas</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline process-cancel">Cancelar</button>
                    <button type="submit" class="btn green a-accept">Firmar Entrega</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div id="receive_confirmation" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue-chambray bg-font-yellow text-center">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><strong>Estos documentos serán recibidos por:</strong></h4>
            </div>
                <div class="modal-body text-center">
                    <h3><strong>{{\App\Helpers\MiscFunctions::get_user_data()['USUA_NOMBRE']}}</strong></h3>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn green btn-block" id="receive-signature">Firmar Recibido</button>
                </div>
        </div>
    </div>
</div>