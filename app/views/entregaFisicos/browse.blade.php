@extends('layouts.app')

@section('breadcrum')
    <li>
        <a href="#">Home</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span>Entrega Físicos</span>
    </li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <!-- BEGIN Portlet PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title text-center">
                    <div class="caption font-blue-sharp">
                        <i class="icon-notebook font-green-sharp"></i>
                        <span class="caption-subject bold uppercase"> Entrega de Documentos Físicos</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form action="process.php" method="POST" class="form-horizonal">
                        <div class="form-body">
                            <div class="row margin-top-40">
                                <div class="col-md-12 text-center">
                                    <i class="fa fa-barcode fa-4x"></i>
                                </div>
                            </div>
                            <div class="row margin-top-20">
                                <div class="col-md-8 col-md-offset-2">
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Radicado: </label>
                                        <div class="col-md-10">
                                            <input type="text" name="number" value="" id="number" class="form-control" placeholder="Lectura del número de radicado..." autofocus/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-5 col-md-4">
                                    <button type="submit" class="btn blue-sharp">
                                        <i class="fa fa-search"></i>
                                        Buscar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END Portlet PORTLET-->
        </div>
    </div>
@endsection

@section('page-css')
@endsection

@section('page-js')
    <script>
        $(document).ready(function(){
            $('#number').focus();
        });
    </script>
@endsection