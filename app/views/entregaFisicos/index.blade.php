@extends('layouts.app')

@section('breadcrum')
    <li>
        <a href="#">Home</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span>Entrega Físicos</span>
    </li>
@endsection

@section('content')
    <div class="tabbable-custom">
        <ul class="nav nav-tabs">
            <li class="{{(isset($delivered) && count($delivered) == 0)? 'active' : ''}}">
                <a href="#tab_1" data-toggle="tab"> Entrega Impresos </a>
            </li>
            <li class="{{(isset($delivered) && count($delivered) > 0)? 'active' : ''}}">
                <a href="#tab_2" data-toggle="tab" id="gestion-doc"> Gestión Documental</a>
            </li>
        </ul>
        <div class="tab-content" style="min-height: 480px;">
            <div class="tab-pane {{(isset($delivered) && count($delivered) == 0)? 'active' : ''}}" id="tab_1">
                @include('entregaFisicos.partials.ent_fis')
            </div>
            <div class="tab-pane {{(isset($delivered) && count($delivered) > 0)? 'active' : ''}}" id="tab_2">
                @if(count($delivered) == 0)
                    <div class="note note-warning" id="document-mng">
                        <h4 class="block note-title">No hay documentos pendientes por recibir.</h4>
                    </div>
                @else
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h3>
                            Documentos entregados por: {{(isset($delivered) && count($delivered))? '<strong>'.\App\Helpers\MiscFunctions::get_user_data($delivered[0]['USUARIO'])['USUA_NOMBRE'].'</strong>': ''}}
                        </h3>
                    </div>
                </div>
                <div class="row margin-top-40">
                    <div class="col-md-10 col-md-offset-1 margin-bottom-20">
                        <table id="doc_mng_table"
                               data-striped="true"
                               data-pagination="true"
                               data-page-size="5"
                               data-search="true"
                               data-sort-name="codigo-anexo"
                               data-sort-order="asc"
                               data-locale="es-SP">
                            <thead>
                            <tr>
                                <th data-width="10%" data-align="center">Radicado</th>
                                <th data-field="codigo-anexo" data-align="center" data-sortable="true" data-width="20%">Código de Anexo</th>
                                <th data-width="20%" data-align="center">Fecha</th>
                                <th data-width="30%" data-align="center">Descripción de Anexo</th>
                                <th data-width="9%" data-align="center">Fisico Radicado</th>
                                <th data-width="9%" data-align="center">Fisico Anexo</th>
                                <th data-width="2%" data-align="center"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($delivered as $key => $row)
                                <tr>
                                    <td>{{$row["RADICADO"]}}</td>
                                    <td>{{$row["ANEX_CODIGO"]}}</td>
                                    <td>{{date_format(date_create($row["FECHA_ENTREGA"]),'Y-m-d H:i')}}</td>
                                    <td>{{$row["DESCRIPCION"]}}</td>
                                    <td>
                                        @if($row["PROCESO"] == "radicated")
                                            <i class="fa fa-check fa-2x"></i>
                                        @else
                                            -
                                        @endif
                                    </td>
                                    <td>
                                        @if($row["PROCESO"] == "anexed")
                                            <i class="fa fa-check fa-2x"></i>
                                        @else
                                            -
                                        @endif
                                    </td>
                                    <td>
                                        <input type="checkbox" class=".receive" value="{{$row['ANEX_CODIGO']}}" name="receive[]" checked/>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row margin-top-40">
                        <div class="col-md-12 text-center">
                            <button type="button" class="btn btn-primary" id="receive-anex">
                                Recibir
                            </button>
                            <button type="button" class="btn red" id="cancel-receive" data-toggle="confirmation" data-singleton="true" data-popout="true" data-btn-ok-label="Si" data-btn-cancel-label="No" data-title="Cancelar?">
                                Cancelar Entrega
                            </button>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
@endsection

@section('page-modal')
    @include('entregaFisicos.partials.modals')
@endsection

@section('page-css')
    <style>
        .deliver-button{
            position: absolute;
            z-index: 10103;
            top: 0;
            left: 30px;
            height: 85px;
            width: 85px;
            overflow: hidden;
            white-space: nowrap;
            border-radius: 50% !important;
            color: transparent;
            background-color: #FF9900 !important;
        }
        #doc_mng_table td{
            font-size: 13px !important;
        }
        .popover.confirmation{
            min-width: 112px;
        }
    </style>
    <link href="{{$include_path}}global/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet" type="text/css" />
@endsection

@section('page-js')
    <script src="{{$include_path}}global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/bootstrap-table/bootstrap-table.min.js" type="text/javascript"></script>
    <script src="{{$include_path}}/global/plugins/bootstrap-table/locale/bootstrap-table-es-SP.js" type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js" type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/jquery.numeric.min.js" type="text/javascript"></script>
    <script>
        $('[data-toggle="confirmation"]').confirmation();
        /**
         * FUNCIONES
         */

        //Validation rules
            var search_validation_rules= {
            errorElement: 'span',
            errorClass: 'help-block help-block-error',
            focusInvalid: false,
            ignore: "",
            rules: {
                "number": {
                    minlength: 14,
                    maxlength:14,
                    required: true
                }
            },
            messages: {
                "number": {
                    required: "El número de radicado es requerido",
                    minlength: $.validator.format("El radicado debe tener más de {0} digitos."),
                    maxlength: $.validator.format("El radicado debe tener menos de {0} digitos.")
                }
            },
            errorPlacement: function (error, element) {
                if (element.parent(".input-icon").size() > 0) {
                    error.insertAfter(element.parent(".input-icon"));
                } else {
                    error.insertAfter(element);
                }
                var icon = $(element).parent('.input-icon').children('i');
                icon.removeClass('fa-check').addClass("fa-warning");
                icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
            },

            highlight: function (element) {
                var process_error= $('#process-error');
                process_error.text("");
                process_error.addClass("hide");
                $(element).closest('.form-group').removeClass("has-success").addClass('has-error');
                var icon = $(element).parent('.input-icon').children('i');
                icon.removeClass('fa-check').addClass("fa-warning");
            },

            success: function (label, element) {
                var icon = $(element).parent('.input-icon').children('i');
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                icon.removeClass("fa-warning").addClass("fa-check");
            },

            submitHandler: function (form) {
                var number      = $('#number').val();
                var process_type= $('#process_type').val();
                $('#process-error').addClass('hide');
                if(process_type == 1){
                    if(number.slice(number.length - 1) == 2){
                        $('#search_notice').removeClass('hide').addClass('show');
                        App.scrollTo($('#search_notice'), -200);
                    }else{
                        $('#search_notice').removeClass('show').addClass('hide');
                        send_search();
                    }
                }else{
                    $('#search_notice').removeClass('show').addClass('hide');
                    send_search();
                }
                return false;
            }
        };
        //Validate the search form
        var form = $('#search-form');
        form.validate(search_validation_rules);

        //Update the document management section
        function update_doc_mang(received){
            $.ajax({
                url: "process.php",
                method: "POST",
                data: {operation:"update_doc_mng"},
                dataType: "html",
                success: function(response) {
                    $("#doc_mng_table").bootstrapTable('destroy');
                    var doc_mng_content= $("#tab_2");
                    if(response != "ERROR") {
                        doc_mng_content.html('');
                        doc_mng_content.html(response);
                        if(received){
                            $('#document-mng .note-title').text('Documentos recibidos correctamente.');
                            $('#document-mng').removeClass('note-warning').addClass('note-success');
                        }
                        App.scrollTo(doc_mng_content, -200);
                    }
                }
            }).done(function(){
                var doc_table= $("#doc_mng_table");
                doc_table.bootstrapTable();
                $('[data-toggle="confirmation"]').confirmation();
            }).fail(function(error){
                console.log(error);
            });
        }
        function update_ent_fis(){
            $.ajax({
                url: "process.php",
                method: "POST",
                data: {operation:"update_ent_fis"},
                dataType: "html",
                success: function(response) {
                    var ent_fis_content= $("#tab_1");
                    if(response != "ERROR") {
                        ent_fis_content.html('');
                        ent_fis_content.html(response);
                    }
                }
            }).done(function(){
                var form = $('#search-form');
                form.validate(search_validation_rules);
                $('[data-toggle="confirmation"]').confirmation();
            }).fail(function(error){
                console.log(error);
            });
        }
        function deliver(){
            var rad_number= $('.rad_anex_code').serialize();
            var op= $('.op').serialize();
            var obs= $('.obs').serialize();
            var user= $('#user').val().toUpperCase();
            $.ajax({
                url: "process.php",
                method: "POST",
                data: {
                    rad_number:rad_number,
                    op:op,
                    obs:obs,
                    user:user,
                    operation:"deliver_process"
                },
                success: function(response) {
                    update_ent_fis();
                    $('#deliver_signature').modal('hide');
                }
            }).done(function(){

            }).fail(function(error){
                console.log(error.responseText);
            });
        }

        //Manage the process buttons Radicados & Anexos
        $('#tab_1').on('click', '.process', function(){
            $('#radicated-container').html('');
            $('#process-error').addClass('hide');
            var process_type= $(this).attr('process_type');
            var searchbox= $('#searchbox-container');
            var search_container= searchbox.hasClass('hide');
            //remove active class from the actual active button
            $('.step-background').find('.active').removeClass('active');
            //add the active class to the clicked button
            $(this).addClass('active');
            $("#process_type").val(process_type);
            $("#search-form").trigger('submit');
        });

        //Function that sends the search form
        function send_search(){
            var number= $('#number').val();
            var process_type= $('#process_type').val();
            var error_block= $('#process-error');
            $.ajax({
                url: "process.php",
                method: "POST",
                data: {number:number, process_type:process_type ,operation:"process"},
                dataType: "json",
                success: function(response) {
                    var process_error= $('#process-error');
                    if(response == null){
                        process_error.text("El número de radicado no existe o ha sido Anulado");
                        process_error.removeClass("hide");
                        return false;
                    }else{
                        process_error.text("");
                        process_error.addClass("hide");
                    }
                    var radi_content= $("#radicated-container");
                    radi_content.html('');
                    if(response.type == "a"){
                        $("#type").val(response.type);
                        $("#data").val(JSON.stringify(response.data));
                        $("#a-condition").modal('show');
                    }
                    if(response.type == "b1"){
                        console.log(response.data.ESTADO);
                        if(response.data.ESTADO > 2){
                            $("#b1_data").val(JSON.stringify(response.data));
                            $("#b1-condition").modal('show');
                        }else{
                            b1_process(JSON.stringify(response.data));
                        }
                    }
                    if(response.type == "b2"){
                        b2_process(JSON.stringify(response.data));
                    }
                    if(response.type == "anexed"){
                        if(response != "ERROR") {
                            radi_content.html('');
                            radi_content.html(response.data);
                            App.scrollTo(radi_content, -200);
                        }
                    }
                }
            }).done(function(){

            }).fail(function(error){
                console.log(error.responseText);
            });
        }

        function anexed_process($data){
            $.ajax({
                url: "process.php",
                method: "POST",
                data: {data:data, operation:"anexed_process"},
                dataType: "html",
                success: function(response) {
                    var radi_content= $("#radicated-container");
                    if(response != "ERROR") {
                        radi_content.html('');
                        radi_content.html(response);
                        App.scrollTo(radi_content, -200);
                    }
                }
            }).done(function(){
                $('[data-toggle="tooltip"]').tooltip();
            }).fail(function(error){
                console.log(error);
            });
        }
        function b1_process(data){
            $.ajax({
                url: "process.php",
                method: "POST",
                data: {data:data, operation:"b1_process"},
                dataType: "html",
                success: function(response) {
                    var radi_content= $("#radicated-container");
                    if(response != "ERROR") {
                        radi_content.html('');
                        radi_content.html(response);
                        App.scrollTo(radi_content, -200);
                    }
                }
            }).done(function(){
                $('[data-toggle="tooltip"]').tooltip();
            }).fail(function(error){
                console.log(error);
            });
        }
        function b2_process(data){
            $.ajax({
                url: "process.php",
                method: "POST",
                data: {data:data, operation:"b2_process"},
                dataType: "html",
                success: function(response) {
                    $("#rad_table").bootstrapTable('destroy');
                    var radi_content= $("#radicated-container");
                    if(response != "ERROR") {
                        radi_content.html('');
                        radi_content.html(response);
                        App.scrollTo(radi_content, -200);
                    }
                }
            }).done(function(){
                var rad_table= $("#rad_table");
                rad_table.bootstrapTable();
                $('[data-toggle="confirmation"]').confirmation();
                rad_table.on('all.bs.table', function (e, name, args) {
                    $('[data-toggle="confirmation"]').confirmation();
                    $('.deliver-confirm').on('confirmed.bs.confirmation', function(){
                        show_b2_selection($(this).attr('anex_code'));
                    });
                });
                $('.deliver-confirm').on('confirmed.bs.confirmation', function(){
                    show_b2_selection($(this).attr('anex_code'));
                });
            }).fail(function(error){
                console.log(error);
            });
        }
        function show_b2_selection(anex_code){
            $.ajax({
                url: "process.php",
                method: "POST",
                data: {anex_code:anex_code, operation:"b2_show_selection"},
                dataType: "html",
                success: function(response) {
                    $("#rad_table").bootstrapTable('destroy');
                    var radi_content= $("#radicated-container");
                    if(response != "ERROR") {
                        radi_content.html('');
                        radi_content.html(response);
                        App.scrollTo(radi_content, -200);
                    }
                }
            }).done(function(){
                $('[data-toggle="tooltip"]').tooltip();
            }).fail(function(error){
                console.log(error);
            });
        }

        /*****************************************/

        /**
         * EVENTOS
         */
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
            $("#doc_mng_table").bootstrapTable();
            $(document).on('click', '.process-cancel', function(){
                $(this).closest('div.modal.fade.in').modal('hide');
                $("#radicated-container").html('');
            });
            $(document).on("submit", "#a_cond_form", function(e){
                e.preventDefault();
                var data= $("#data").val();
                $.ajax({
                    url: "process.php",
                    method: "POST",
                    data: {data:data, operation:"a_process"},
                    dataType: "html",
                    success: function(response) {
                        var radi_content= $("#radicated-container");
                        if(response != "ERROR") {
                            $("#a-condition").modal('hide');
                            radi_content.html('');
                            radi_content.html(response);
                            App.scrollTo(radi_content, -200);
                        }
                    }
                }).done(function(){
                    $('[data-toggle="tooltip"]').tooltip();
                }).fail(function(error){
                    console.log(error);
                });
            });
            $(document).on("submit", "#b1_cond_form", function(e){
                e.preventDefault();
                $("#b1-condition").modal("hide");
                $("#b1-mod-obs").modal("show");
            });
            $(document).on("submit", "#b1_obs_form", function(e){
                e.preventDefault();
                var data= $("#b1_data").val();
                var obs= $("#b1_obs").val();
                $.ajax({
                    url: "process.php",
                    method: "POST",
                    data: {data:data, obs:obs, operation:"b1_state_process"},
                    dataType: "html",
                    success: function(response) {
                        var radi_content= $("#radicated-container");
                        if(response != "ERROR") {
                            $("#b1-mod-obs").modal('hide');
                            radi_content.html('');
                            radi_content.html(response);
                            App.scrollTo(radi_content, -200);
                        }
                    }
                }).done(function(){
                    $('[data-toggle="tooltip"]').tooltip();
                }).fail(function(error){
                    console.log(error);
                });
            });
            $(document).on("submit", "#adeliver_signature_form", function(e){
                e.preventDefault();
                var user= $('#user').val().toUpperCase();
                var pass= $('#pass').val();
                $.ajax({
                    url: "process.php",
                    method: "POST",
                    data: {user:user, pass:pass, operation:"signature_check_process"},
                    success: function(response) {
                        if(response){
                            deliver();
                            if(!$('#signature-error').hasClass('hide')){
                                $('#signature-error').addClass('hide');
                            }
                        }else{
                            $('#signature-error').removeClass('hide');
                        }
                    }
                }).done(function(){

                }).fail(function(error){
                    console.log(error);
                });
            });
            //Event that triggers the receive process after confirmation
            $(document).on('click','#receive-signature', function(){
                var data= $('input[name="receive[]"]').serialize();
                $.ajax({
                    url: "process.php",
                    method: "POST",
                    data: {data:data, operation:"receive_doc"},
                    success: function(response) {
                        if(response != "ERROR") {
                            $('#receive_confirmation').modal('hide');
                            update_doc_mang(1);
                            update_ent_fis();
                        }
                    }
                }).done(function(){
                }).fail(function(error){
                    console.log(error);
                });
            });
        });
        $('#number').numeric();
        //Event that shows the receive confirmation modal
        $(document).on('click', '#receive-anex', function(){
            if($('input[name="receive[]"]').serialize().length > 0) {
                $('#receive_confirmation').modal('show');
            }
        });
        $(document).on('click', '#deliver',function(){
            if(parseInt($('#delivered-count').text()) == 0){
                return false;
            }
            $('#deliver_signature').modal('show');
        });
        $(document).on('click','#gestion-doc', function(){
            update_doc_mang(0);
        });
        $(document).on('click', '#deliver-btn', function(){
            var operation, delivered_count, obs, op_type, delivered_form, rad_num, selected;
            op_type         = $('#op_type').text();
            rad_num         = $('#rad_num').text();
            delivered_form  = $('#delivered-count-form');
            selected        = 0;
            $('.rad_anex_code').each(function(){
                if($.trim($(this).val()) == $.trim(rad_num)){
                    selected= 1;
                }
            });
            if(selected){
                $('#process-error').text('Este numero no puede agregarse. Ya ha sido seleccionado para entregar.').removeClass('hide');
                App.scrollTo($('#search-form'), -200);
                return false;
            }
            if(op_type == "a" || op_type == "b1" || op_type == "b2"){
                operation= "radicated";
            }
            if(op_type == "anexed"){
                operation= "anexed";
            }
            if(op_type == "b1_state"){
                obs         = $('#b1_state_obs').text();
                operation   = "b1_state";
                delivered_form.append('<input type="hidden" name="obs[]" class="obs" value="'+encodeURIComponent(obs)+'" />');
            }
            if(operation == "anexed"){
                $('#anex_number').val(rad_num);
                $('#anex_op').val(operation);
                $('#anexed-obs').modal('show');
                return false;
            }
            //Append the new radicated to the form
            delivered_form.append('<input type="hidden" name="rad_anex_code[]" class="rad_anex_code" value="'+$.trim(rad_num)+'" />');
            delivered_form.append('<input type="hidden" name="op[]" class="op" value="'+$.trim(operation)+'" />');
            //update to the delivered counter
            delivered_count= parseInt($('#delivered-count').text());
            $('#delivered-count').text(delivered_count+1);
            if($('#delivered-count').parent('#deliver').hasClass('hide')){
                $('#delivered-count').parent('#deliver').removeClass('hide');
            }
            //resetting the search process
            $('#search-form')[0].reset();
            $('#radicated-container').html('');
            $('.step-background').find('.active').removeClass('active');
        });
        $(document).on('submit', '#anexed_obs_form',function(e){
            e.preventDefault();
            $('#anexed-obs').modal('hide');
            var delivered_form= $('#delivered-count-form');
            var number= $('#anex_number').val();
            var obs= $('#anex_obs').val();
            var op= $('#anex_op').val();
            $('#anexed_obs_form')[0].reset();
            //Append the new radicated to the form
            delivered_form.append('<input type="hidden" name="rad_anex_code[]" class="rad_anex_code" value="'+$.trim(number)+'" />');
            delivered_form.append('<input type="hidden" name="op[]" class="op" value="'+$.trim(op)+'" />');
            delivered_form.append('<input type="hidden" name="obs[]" class="obs" value="'+encodeURIComponent(obs)+'" />');
            //update to the delivered counter
            delivered_count= parseInt($('#delivered-count').text());
            $('#delivered-count').text(delivered_count+1);
            if($('#delivered-count').parent('#deliver').hasClass('hide')){
                $('#delivered-count').parent('#deliver').removeClass('hide');
            }
            //resetting the search process
            $('#search-form')[0].reset();
            $('#radicated-container').html('');
            $('.step-background').find('.active').removeClass('active');
        });

        //Cancel the attachments reception
        $(document).on('click', '#cancel-receive', function(e){
            e.preventDefault();
            $.ajax({
                url: "process.php",
                method: "POST",
                data: {operation:"cancel_receive"},
                success: function(response) {
                    if(response != "ERROR") {
                        update_doc_mang(0);
                        update_ent_fis();
                    }
                }
            }).done(function(){
                $('[data-toggle="confirmation"]').confirmation();
            }).fail(function(error){
                console.log(error);
            });
        });
    </script>
@endsection