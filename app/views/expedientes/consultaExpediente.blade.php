@extends('layouts.app')

@section('content')

<link href="{{$include_path}}pages/css/search.min.css" rel="stylesheet" type="text/css"/>
<link href="{{$include_path}}pages/css/invoice.min.css" rel="stylesheet" type="text/css"/>
<link href="{{$include_path}}global/plugins/typeahead/typeahead.css" rel="stylesheet" type="text/css"/>
<link href="{{$include_path}}global/plugins/typeahead/typeahead.css" rel="stylesheet" type="text/css"/>

<style>
    table.dataTable thead tr th {
        word-wrap: break-word;
        word-break: break-all;
    }

    .botonestabla {
        border-radius: 5px 5px 5px 5px !important;
    }

    .tt-input {
        font-size: 50px;
    }


</style>
<?php if ($msg == "") { ?>

<?php
} else {

}
?>

<div class="row">
    <div class="col-md-12" id="consultaExpediente">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title" style="border-bottom: 0px !important;">
                <div class="caption font-dark">
                    @include('expedientes.partials.badgesNotificacionTitulo') 
                </div>
            </div>
            <form action="" method="POST" class="form-horizonal" id="">

                <div class="form-body modal-body todo-task-modal-body">

                    <div class="form-actions">
                        <div class="row"></div>
                    </div>

                    <div class="row" style="margin-top:40px">
                        <div class="col-md-12 ">
                            @include('expedientes.partials.busquedaExpedientes') 
                        </div>
                    </div> 
                    @include('expedientes.partials.tablaExpedientes',['titulo' => 'Accion'] )
                </div>
        </div>

    </div>

</div>


@include('expedientes.partials.modalSeguridadEntidad' , ['nivelBloqueado' => 'Expediente'])
@section('page-css')
<style>
    .tab-pane {
        min-height: 420px;
    }

    .tabbable-line > .nav-tabs > li.active {
        border-bottom: 4px solid #FF9900 !important;
    }

    .tabbable-line > .nav-tabs > li:hover {
        border-bottom: 4px solid rgba(255, 154, 0, 0.49) !important;
    }

    .assoc {
        font-size: 14px;
        font-weight: 600;
        color: #9eacb4;
        margin-top: 25px;
    }

    .as {
        border: 1px solid #e0e6e9;
        background-color: #fff;
        color: #c1cbd1;
        text-decoration: none;
        padding: 0 .4em;
        font-size: 20px;
        margin: -.3em 0 0 .5em;
        float: none;
    }

    .divBusquedaAvanzada {
        background: #eee;
        padding: 3px 30px;
        max-height: 400px;
        overflow-y: scroll;
    }

    .divBusquedaAvanzada p {
        cursor: pointer;
    }

    .resultEnviar {
        background: #eee;
        padding: 3px 30px;
        max-height: 400px;
        overflow-y: scroll;
        margin-bottom: 20px;
    }

    .resultEnviar p {
        cursor: pointer;
    }

    .as:hover {
        background-color: #4db3a4;
        color: #fff;
        text-decoration: none
    }

    .as:focus {
        color: #c1cbd1;
        text-decoration: none
    }

</style>

<link href="{{$include_path}}global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"
      rel="stylesheet" type="text/css"/>

@endsection

@section('page-js')
<script src="{{$include_path}}global/plugins/bootstrap-table/bootstrap-table.min.js"
type="text/javascript"></script>
<script src="{{$include_path}}global/plugins/bootstrap-table/locale/bootstrap-table-es-SP.js"
type="text/javascript"></script>
<script src="{{$include_path}}global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"
type="text/javascript"></script>
<script src="{{$include_path}}global/plugins/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.es.js"
type="text/javascript"></script>
<script src="{{$include_path}}global/plugins/moment.min.js" type="text/javascript"></script>

<!--  datatable -->
<script src="{{$include_path}}global/plugins/datatable/datatable.js" type="text/javascript"></script>
<script src="{{$include_path}}global/plugins/datatable/datatables.min.js" type="text/javascript"></script>
<script src="{{$include_path}}global/plugins/datatable/datatables.bootstrap.js" type="text/javascript"></script>
<!--  datatable -->

<script src="{{$include_path}}global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js"
type="text/javascript"></script>

<script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.ui.widget.js"></script>
<!-- The Templates plugin is included to render the upload/download listings -->
<script src="{{$include_path}}global/plugins/blueimp/js/tmpl.min.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="{{$include_path}}global/plugins/blueimp/js/load-image.all.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="{{$include_path}}global/plugins/blueimp/js/canvas-to-blob.min.js"></script>

<!-- PROBANDO SIN ESTO Bootstrap JS is not required, but included for the responsive demo navigation
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>-->

<!-- blueimp Gallery script -->
<script src="{{$include_path}}global/plugins/blueimp/js/jquery.blueimp-gallery.min.js"></script>

<script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.iframe-transport.js"
type="text/javascript"></script>
<script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.fileupload.js" type="text/javascript"></script>
<script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.fileupload-process.js"
type="text/javascript"></script>
<script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.fileupload-image.js"
type="text/javascript"></script>
<script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.fileupload-audio.js"
type="text/javascript"></script>

<script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.fileupload-video.js"
type="text/javascript"></script>
<script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.fileupload-validate.js"
type="text/javascript"></script>
<script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.fileupload-ui.js"
type="text/javascript"></script>
<script src="{{$include_path}}global/plugins/jquery-upload/js/form-fileupload.min.js"
type="text/javascript"></script>


<script src="{{$include_path}}global/plugins/typeahead/typeahead.bundle.min.js"></script>
<script src="{{$include_path}}global/plugins/x-editable/bootstrap-editable.min.js"></script>
<script src="{{$include_path}}global/plugins/typeahead/typeaheadjs.js"></script>


<script src="{{$include_path}}apps/scripts/nuevoExpediente.js?v=<?= date('Ymdis') ?>"
type="text/javascript"></script>




<script>
//puede excluir cualquier radicado de cualquier expediente, si >0
var usuaPermExpediente = '<?= $_SESSION["usuaPermExpediente"] ?>';

/*var textHtmlAvanzadoUno = '<span class="help-block" style="display: inline; " >No se encontraron expedientes que coincidan con</span> ' +
 '<span style="color:#00b300;display: inline;" >' + $("#inputexpedientes").val() + '.</span>' +
 ' <span class="help-block" style="display: inline;">Puede usar la <a href="#" class="" style=""   ' +
 'onclick="modalBusquedaAvanz()">B&uacute;squeda Avanzada</a>' +
 ' o solicitar la creaci&oacute;n de un expediente ' +
 ' <a href="#" class="" style=""   ' +
 'onclick="modalNuevoexpediente()">Nuevo</a></span>';
 
 var textHtmlAvanzadoDos = '<span class="help-block" style="display: inline;float:right"><a href="#" class="" style=""   ' +
 'onclick="modalBusquedaAvanz()">B&uacute;squeda Avanzada</a>' +
 ' o solicitar la creaci&oacute;n de un expediente ' +
 ' <a href="#" class="" style=""   ' +
 'onclick="modalNuevoexpediente()">Nuevo</a></span>';*/


//variable que almacena el texto de la ultima opcion del desplegable, cuando se escribe sobre
//el input de buscar expedientes
var texMasResultExped = "Para mas resultados y buscar en todos los años, clic en [Ampliar Búsqueda]";

//almacena lo que se va escribiendo en el input de buscar expedientes
var textKeyupInputExped = "";
var onlyUsersSelected = new Array();
var onlyDependSelected = new Array();
var allUsers = new Array(); //arreglo que almacena todos los usuarios a mostrar
 var allDepend = new Array(); //arreglo que almacena todas las dependencias a mostrar
 var expedientePermS = '<?php echo $expedientePerm ?>'; 
 var usuarioArchS = '<?php echo $usuarioArch ?>';
 var codusuario = '<?php echo $_SESSION["usuario_id"] ?>'
var segModeLectura = true;
var expedienteSelected;
var accionExpediente = 'consulta';
var borrador_id = 0;
var valorOriginal = false;
var $elementSelected;

 
/*muestra los botons de hacer la busqueda avanzada*/
function showBtnAvanzado(html) {
    console.log(html);
    $("#divApendBusqAvan").html(html);
    $("#divApendBusqAvan").css('display', 'block');
    $("#divbuttonbusquedaAv").css('display', 'block')
}

    function yaEstaUserOrDepend(seleccion, temp) {
        var encontro = false;
        for (var i = 0; i < Object.keys(temp).length; i++) {
            if (temp[i].id == seleccion.id) {
                encontro = true;
            }
        }
        return encontro;
    }

 function guardarUserSeg(data) {
        var temp = new Array();
        var tipo = 'USUARIO'
        console.log('data', data)
        if (data.usua_nomb != undefined) {
            temp = onlyUsersSelected
        } else {
            temp = onlyDependSelected
            tipo = 'DEPENDENCIA'
        }
        var yaesta = yaEstaUserOrDepend(data, temp)
        console.log('yaesta', yaesta);
         console.log(tipo);
        if (yaesta == false) {
            if (data.usua_nomb != undefined) {
                onlyUsersSelected[Object.keys(onlyUsersSelected).length] = data;
            } else {
                onlyDependSelected[Object.keys(onlyDependSelected).length] = data;
            }
            updateSeguridad('agregar', tipo, data)
        }

    }

    //cada vez que se desmarca a un firmante, vuelvo a rellner el arreglo se los selecteds
    function quitarUserSeg(data) {

        var tipo = 'USUARIO'
        var temp = new Array();
        console.log(data.id);
        if (data.usua_nomb != undefined) {
            temp = onlyUsersSelected
            onlyUsersSelected = new Array();
        } else {
            tipo = 'DEPENDENCIA'
            temp = onlyDependSelected
            onlyDependSelected = new Array();
        }

        var cont = 0;
        for (var i = 0; i < Object.keys(temp).length; i++) {
            if (temp[i].id == data.id) {
            } else {
                if (data.usua_nomb != undefined) {
                    onlyUsersSelected[cont] = temp[i]
                } else {
                    onlyDependSelected[cont] = temp[i]
                }
                cont++;
            }
        }
        updateSeguridad('quitar', tipo, data)

    }
    
     function updateSeguridad(accion, tipo, seleccion) {
         var transaccion = 'AUTORIZAR';
         if (accion == 'quitar'){
             transaccion = 'DESAUTORIZAR';
         }else{
             transaccion = 'AUTORIZAR';
         }
         var paramsAcceso = {
             'numero_expediente': expedienteSelected,
             'tipo_entidad_autorizada': tipo,
             'tipo_entidad' : 'expediente',
             'entidad_autorizada' : seleccion.id,
             'usuario_id' : codusuario,
             'transaccion' : transaccion
         }

         var ajax = SeguridadService.autorizacion(paramsAcceso);
         ajax.success(function (response) {
             if(response.isSuccess){

                 alertModal('Actualizado!', capitalizarPrimeraLetra(response.tipo_entidad_aut) +' '+ response.accion_entidad +': '+retornarParteCadenaSplit(seleccion.text,'(',0), 'success', '5000');

                 return true;
             }else{
                 alertModal('Advertencia', 'La seguridad del acceso al expediente NO se logro modificar', 'warning', '5000');
                 return false;
             }
         }).error(function (error) {
             alertModal('Advertencia', 'La seguridad del acceso al expediente NO se logro modificar', 'warning', '5000');
             return false;
         });
         
     }
     
     function traerUsers(quebuscar) {

        //?type=public&radicado="+radicado+"&quebuscar="+quebuscar
        $.ajax({
            url: configsHasValue['WS_APPSERVICE_URL'] + '/seguridadRad/usersToAuthorize',
            headers: {
                "Authorization": 'Bearer '+configsHasValue['WS_APPSERVICE_TOKEN']
            },
            type: 'GET',
            dataType: 'json',

            async: false,
            data: {
                type: 'public',
                quebuscar: quebuscar 

            },
            success: function (response) {
                if (quebuscar == "usuarios") {
                    allUsers = response.objects;
                } else {
                    allDepend = response.objects;
                }

            },
            error: function (xhr, ajaxOptions, thrownError) {

                if(xhr.responseJSON.message){
                    Swal.fire("Error", xhr.responseJSON.message, "error");
                }else{
                    Swal.fire("Error", "Ha ocurrido un error al buscar los datos", "error");
                }
            }
        });
    }

buscartodo = false;
//$('#modal_expediente').modal({show: true, keyboard: false, backdrop: 'static'});

$("#inputexpediente").val('');
$("#tbodyexpediente").html('');
$("#divApendBusqAvan").css('display', 'block');//el enace de busqueda avanzada
$("#divbuttonbusquedaAv").css('display', 'block')
$("#span_coincidencia_exped").html($("#inputexpediente").val())
mostrabusquedaAvanz(textHtmlAvanzadoDos);
yaincluyouno = false;

$(document).ready(function () {
    
$('#swich_seguridad').on('switchChange.bootstrapSwitch', function (event, state) {
    
    if (valorOriginal == false){
        if (updateAcceso(state) == false){
            return false;
        }
    }else{
        valorOriginal = false;
    }
    
            if (state){
            $('#textoNivelBloqueo').text('publico. Sin restricción de acceso');
            $('#icBloq').html('');
            $('#icBloq').html('<i style="padding-left: 0.5em;padding-right: 0.5em;" class="fa fa-2x fa-unlock font-green"></i>');

                $('.bootstrap-switch-label').attr('data-original-title', 'Restringir acceso al expediente')
                $('.bootstrap-switch-label').attr('data-content', 'Solo tendrá acceso al expediente el usuario actual ' +
                    'y usuarios o dependencias autorizadas!')
                $('.bootstrap-switch-label').attr('data-placement', 'top')
                $('.bootstrap-switch-label').attr('data-trigger', 'hover')
                $('.bootstrap-switch-label').attr('data-container', 'body')
                popoverRestringidoSeg();
        }else{
             $('#textoNivelBloqueo').text('reservado. con restricción de acceso');
             $('#icBloq').html('');
             $('#icBloq').html('<i style="padding-left: 0.5em;padding-right: 0.5em;" class="fa fa-2x fa-lock font-red"></i>');

                popoverPublicoSeg();
        }
   
});

//console.log('hola');
//showBtnAvanzado(textHtmlAvanzadoUno);
console.log(expedientePermS,usuarioArchS);
console.log(configsHasValue['EXPEDIENTE_ALERT_ACCESO']);

    var ourCustomDataSetSource = function (query, callback) {
        callback([{
                name: 'Para mas resultados y buscar en todos los años, clic en [Ampliar Búsqueda]'
            }]);
    };
    if (inputexpediente == "") {
        inputexpediente = $('#inputexpediente');
        inputexpediente.typeahead({
            hint: true,
            highlight: true,
            minLength: 3
        },
                {
                    name: 'nombre',
                    source: users.ttAdapter(),
                    templates: {
                        header: '<div style="border-button: solid 1px black; width: 100%;"></div>'
                    },
                    display: function (data) {

                        return data.NUMERO_EXPEDIENTE + ' ' + data.TITULO_NOMBRE + ' ' + data.DEPENDENCIA + ' ' + data.TRD;
                    },
                    limit: 50
                },
                {
                    name: 'fixed',
                    displayKey: 'name',
                    source: ourCustomDataSetSource,
                    templates: {
                        header: '<div style="border-top: solid 1px gray; width: 100%;"></div>'
                    }
                });

        /*
         *    return {NUMERO_EXPEDIENTE: item.NUMERO_EXPEDIENTE, DEPENDENCIA: item.DEPENDENCIA,
         TITULO_NOMBRE: item.TITULO_NOMBRE,DESCRIPCION:item.DESCRIPCION,
         TRD:item.TRD,ESTADO:item.ESTADO};
         */

        inputexpediente.bind('typeahead:change', function (ev) {

            var selected = false;
            var value = ev.target.value;
            $('.tt-suggestion.tt-selectable').each(function () {
                if (value === $(this).text()) {
                    selected = true;
                }
            });
            if (!selected) {
//inputexpediente.typeahead('val', '');
            }
        });


        inputexpediente.bind('typeahead:selected', function (obj, datum, name) {
//aqui pasa cuando se selecciona un item

//alert(JSON.stringify(obj)); // object
// outputs, e.g., {"type":"typeahead:selected","timeStamp":1371822938628,"jQuery19105037956037711017":true,"isTrigger":true,"namespace":"","namespace_re":null,"target":{"jQuery19105037956037711017":46},"delegateTarget":{"jQuery19105037956037711017":46},"currentTarget":

//alert(JSON.stringify(name)); // contains dataset name
// outputs, e.g., "my_dataset"

            console.log('datum', datum); // contains datum value, tokens and custom fields
// outputs, e.g., {"redirect_url":"http://localhost/test/topic/test_topic","image_url":"http://localhost/test/upload/images/t_FWnYhhqd.jpg","description":"A test description","value":"A test value","tokens":["A","test","value"]}
// in this case I created custom fields called 'redirect_url', 'image_url', 'description'
        console.log('antes de definir la tabla');
            if (datum.TITULO_NOMBRE != undefined) {
                DeleteExpedientestabla;
                console.log('despues de definir la tabla');
                expedientestabla.length  = 0;
                definirTablaExpediente('Accion');
                seleccexped = true;

                estoyencrearsinradicado = 'CONSULTAEXPEDIENTE';
                expedienteselected = datum;
                validarYaestaIncluido(datum);
                inputexpediente.typeahead('val', '');
                mostrabusquedaAvanz(textHtmlAvanzadoDos);
                /*if(selecttipologia==true && seleccexped==true){
                 
                 validarYaestaIncluido(datum);
                 inputexpediente.typeahead('val', '');
                 }*/
            } else {
//si entra aqui, es porque selecciono la opcion de mas resultados para busqueda avanzada,

                $("#search-expediente").click();
                inputexpediente.typeahead('val', '');
            }
        });
    }
    $('#inputexpediente').css('background-color', '#dad8d9');

    definirTablaExpediente('Accion');

    var entablatemp = expedientestabla;
    expedientestabla = null;
    expedientestabla = new Array();
//definirinputtipologia();
//var retornoMisExpe = buscarMisExped();
//buscarExpedRadAso();

    $("#id_select_users").select2(
            {
                width: '100%',
                multiple: true,
                dropdownParent: $(document.body),
                allowClear: true,
                data: allUsers,
                ajax: {
                    url: configsHasValue['WS_APPSERVICE_URL'] + '/seguridadRad/usersToAuthorize',
                    dataType: 'json',
                    delay: 250,
                    headers: {
                        "Authorization": 'Bearer '+configsHasValue['WS_APPSERVICE_TOKEN']
                    },
                    data: function (params) { 
                        return {
                            search: params.term,
                            type: 'public',
                            quebuscar: 'usuarios' 
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: data.objects,
                        };
                    },
                    cache: true,
                    error: function(xhr, ajaxOptions, thrownError){
                         console.log(xhr);
                        if(typeof(xhr.responseJson) != 'undefined'){
                            
                            if (typeof(xhr.responseJson.message) != 'undefined'){
                                Swal.fire("Error", xhr.responseJSON.message, "error");
                            }
                        }
                        
                        //else{
                        //    Swal.fire("Error", "Ha ocurrido un un error al buscar los datos", "error");
                        //}
                    }
                },
                placeholder: 'Buscar Usuarios',
                escapeMarkup: function (markup) {
                    return markup;
                }, // let our custom formatter work
                language: languageseelct2
            }).on('select2:selecting', function (e) {
            //cada vez que selecciono una opcion
            guardarUserSeg(e.params.args.data)

        }).on("select2:unselecting", function (e) {

            //cada vez que desmarco una opcion
            quitarUserSeg(e.params.args.data)

        }).trigger('change');
        
        
      $("#select_dependencias").select2(
            {
                width: '100%',
                multiple: true,
                dropdownParent: $(document.body),
                allowClear: true,
                data: allDepend,
                ajax: {
                    url: configsHasValue['WS_APPSERVICE_URL'] + '/seguridadRad/usersToAuthorize',
                    dataType: 'json',
                    delay: 250,
                    headers: {
                        "Authorization": 'Bearer '+configsHasValue['WS_APPSERVICE_TOKEN']
                    },
                    data: function (params) {
                        return {
                            search: params.term,
                            type: 'public',
                            quebuscar: 'dependencias'
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: data.objects,
                        };
                    },
                    cache: true,
                    error: function(xhr, ajaxOptions, thrownError){
                        if(typeof(xhr.responseJson) != 'undefined'){
                            
                            if (typeof(xhr.responseJson.message) != 'undefined'){
                                Swal.fire("Error", xhr.responseJSON.message, "error");
                            }
                        }
                    }
                },
                placeholder: 'Buscar Dependencias',
                escapeMarkup: function (markup) {
                    return markup;
                }, // let our custom formatter work
                language: languageseelct2
            }).on('select2:selecting', function (e) {
            //cada vez que selecciono una opcion
            guardarUserSeg(e.params.args.data)

        }).on("select2:unselecting", function (e) {

            //cada vez que desmarco una opcion
            quitarUserSeg(e.params.args.data)

        }).trigger('change');

    $(document).on('click', '.get_all', function () {


        console.log('hola');
        showBtnAvanzado(textHtmlAvanzadoUno);

    })
});
</script>
@endsection