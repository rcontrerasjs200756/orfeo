@extends('layouts.app')

@section('content')

    <link href="{{$include_path}}pages/css/search.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{$include_path}}pages/css/invoice.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{$include_path}}global/plugins/typeahead/typeahead.css" rel="stylesheet" type="text/css"/>
    <link href="{{$include_path}}global/plugins/typeahead/typeahead.css" rel="stylesheet" type="text/css"/>

    <style>
        table.dataTable thead tr th {
            word-wrap: break-word;
            word-break: break-all;
        }

        .botonestabla {
            border-radius: 5px 5px 5px 5px !important;
        }

        .tt-input {
            font-size: 50px;
        }


    </style>
    <?php if($msg == ""){ ?>

    <?php if($tieneexpedientes < 1){ ?>
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="portlet-title tabbable-line">
                        <div class="col-md-3 caption font-dark">

                            <strong>RADICADO</strong> <span class='label label-info label-sm'><?php echo $radicado;?></span>
                        </div>
                        <div class="col-md-6 caption font-dark">
                            <span class="caption-subject bold">No ha sido incluido en expediente</span>
                        </div>


                        <div class="col-md-2">
                            <div class="caption" style="float:right;">
                                <button id="btnIncluirExped" onclick="modalexpediente()"
                                        style="border-radius:20px !important"
                                        name=""
                                        class="btn green">Incluir en Expediente
                                </button>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="caption" style="float:right;">
                                <button id="btnCrearSinExped" onclick="validaCrear()"
                                        style="border-radius:20px !important"
                                        name=""
                                        class="btn btn-default">Crear
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="note note-warning">
                                <h4 class="block">Importante!</h4>
                                <p> Al incluir los documentos en las carpetas (Expedientes), según el tema o proceso al
                                    que
                                    pertenecen, automáticamente adquieren su clasificación documental, según las
                                    Tablas de Retención Documental
                                </p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <?php }else{ ?>
    <div class="portlet light portlet-fit portlet-datatable bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-settings font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase"
                > RADICADO</span>

                <span class='label label-info label-sm'> <?php echo $radicado ?> </span>
                <span class="caption-subject font-dark sbold uppercase"
                > incluido en </span>
                <span class='label label-info label-sm'
                      id="spancantexped"><?php echo $tieneexpedientes ?></span>
                <span class="caption-subject font-dark sbold uppercase"
                >expedientes
                                            </span>
            </div>
            <div class="actions">
                <div class="btn-group">
                    <a class="btn green btn-outline btn-circle" href="javascript:;" data-toggle="dropdown">
                        <i class="fa fa-share"></i>
                        <span class="hidden-xs" id="spanlistaexped">Expedientes</span>
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu pull-right" id="ulexpedientes">
                        <?php
                        if(isset($expedientes) && count($expedientes) > 0){

                        foreach($expedientes as $row){
                        ?>
                        <li>
                            <a href="javascript:;" data-expednumero="<?php echo $row['EXPEDIENTE'] ?>"
                               onclick="buscarinfoexpediente('<?php echo $row['EXPEDIENTE'] ?>')"><?php echo $row['EXPEDIENTE'] ?>
                                / <?php echo $row['TITULO'] ?></a>
                        </li>

                        <?php   }
                        } ?>
                    </ul>
                </div>
                <div class="btn-group btn-group-devided" data-toggle="buttons">
                    <label class="btn btn-transparent green btn-outline btn-circle btn-sm active"
                           onclick="modalexpediente();" data-toggle="tooltip"
                           title="Incluye el radicado <?php echo $radicado ?> en otra carpeta o expediente.">
                        <input type="radio" name="options" class="toggle" id="option1">Incluir en otro
                        expediente</label>

                    <label class="btn btn-transparent red btn-outline btn-circle btn-sm"
                           onclick="modalexcluir();" id="lblbtnexcluirnew" data-toggle="tooltip">
                        <input type="radio" name="options" class="toggle" id="option2">Excluir</label>

                    <label class="btn btn-default btn-outline btn-circle btn-sm active" onclick="accioncrear();"
                           data-toggle="tooltip" id="lblbtncrearnew">
                        <input type="radio" name="options" class="toggle" id="option1">Crear</label>

                </div>
            </div>
        </div>

        <div id="loader_tableopcontainer" class="hide">
            <div class="row">
                <div class="col-md-12 text-center margin-top-40">
                    <i class="fa fa-spinner fa-pulse fa-3x fa-fw font-blue-steel"></i>
                    <span class="sr-only">Loading...</span>
                </div>
            </div>
        </div>
        <div class="portlet-body">
            <div class="tabbable-line">
                <ul class="nav nav-tabs nav-tabs-lg">
                    <li class="active">
                        <a href="#tab_1" data-toggle="tab" aria-expanded="true"  onclick="tablaprinciexped()">Expediente</a>
                    </li>

                    <li class="" data-toggle="tooltip" id="liradicadosrelacionados">
                        <a href="#tab_3" data-toggle="tab" aria-expanded="false"
                           onclick="ventanaRadSelec()">Radicados Relacionados

                        </a>
                    </li>
                    <li class="" data-toggle="tooltip" id="lihistorico">
                        <a href="#tab_2" data-toggle="tab" aria-expanded="false"
                           onclick="ventanahistorial()">Histórico Expediente</a>
                    </li>



                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">

                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet green-meadow box">
                                    <div class="portlet-title">
                                        <div class="caption" id="inftituloborder">
                                            <i class="fa folder-open-o"></i>
                                        </div>
                                        <div class="actions">
                                            <a href="javascript:;" class="btn btn-default btn-sm">
                                                <i class="fa fa-pencil"></i> </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="row static-info">
                                            <div class="col-md-5 name"> T&iacute;tulo - Nombre:</div>
                                            <div class="col-md-7 value" id="inftitulo">
                                            </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-5 name"> Descripci&oacute;n:</div>
                                            <div class="col-md-7 value" id="infdescripcion"></div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-5 name"> Estado:</div>
                                            <div class="col-md-7 value">
                                                <span class="label label-success" id="infestado"></span>
                                            </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-5 name"> Clasificaci&oacute;n TRD:</div>
                                            <div class="col-md-7 value" id="inftrd"></div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-5 name"> Proceso:</div>
                                            <div class="col-md-7 value" id="infproceso"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="portlet-body">
                                <div class="tab-content">
                                    <div id="sample_3_wrapper" class="dataTables_wrapper no-footer">

                                        <div id="tableopcontainer" class=" table-scrollable" >
                                            @include('expedientes.partials.tablaprinciexpediente')
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-body">

                                        <div class="note note-info">
                                            <h4 class="block">Ubicaci&oacute;n F&iacute;sica de los documentos</h4>
                                            <p>La organización de los documentos en los expedientes físicos, deben conservar el mismo orden del expediente virtual.
                                            </p>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab_2">
                        <div class="table-container" style="">

                            <div class="row">
                                <div class="portlet-body">
                                    <div class="tab-content">
                                        <div id="" class="dataTables_wrapper no-footer">

                                            <div id="tablehistorialcontainer" class=" table-scrollable" >
                                                @include('expedientes.partials.tablahistorico')
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab_3">
                        <div class="table-container" style="">

                            <div class="row">
                                <div class="portlet-body">
                                    <div class="tab-content">
                                        <div id="" class="dataTables_wrapper no-footer">

                                            <div id="tableradseleccontainer" class=" table-scrollable" >
                                                @include('expedientes.partials.tablaradselec')
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <?php } ?>

    <?php } else{ //ocurrio un error ?>

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="font-dark"></i>
                        <span class="caption-subject bold uppercase">Error</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="note note-danger">
                                <h4 class="block">Error</h4>
                                <p> <?php echo $msg;?>
                                </p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <?php } ?>
@endsection

@section('page-modal')

    <div id="modal_expediente" class="modal fade" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog " style="width:80% !important;">

            <div class="modal-content">
                <div class="modal-header bg-blue-chambray bg-font-yellow text-center">
                    <button type="button" title="Cerrar" onclick="cerrarModalExpediente()"
                            class="btn btn-lg "
                            style="float: right;background-color: #2C3E50!important; border-radius:20px !important">
                        <i class="fa fa-remove"></i></button>
                    <h4 class="modal-title"><strong>Buscar Expedientes</strong></h4>
                </div>


                <div class="modal-body" id="radicated_info">
                    <form action="" method="POST" class="form-horizonal" id="">
                        <div class="form-body modal-body todo-task-modal-body">
                            <div class="form-actions">
                                <div class="row"></div>
                            </div>

                            <div class="row">
                                <div class="col-md-12" style="">
                                    <div class="form-group">
                                        <STRONG class=" col-md-12" id="lblnumradexped"></STRONG>
                                    </div>
                                </div>
                            </div>

                            <div class="row" style="margin-top:40px">
                                <div class="col-md-12 ">

                                    <div>
                                        <div>
                                            <div class="row">


                                                <div class="col-md-12 search-page search-content-2"
                                                     id="divinputexpediente"
                                                     style="height:100px !important;min-height:100px !important">
                                                    <div class="input-group search-bar bordered">
                                                        <input type="text"
                                                               style="color: #5E6373 !important;font-size: 16px !important"
                                                               id="inputexpediente" class="form-control typeahead"
                                                               placeholder="Buscar por número de expediente, nombre, dependencia, serie o subserie...">
                                                        <span class="input-group-btn">
                        <button class="btn blue uppercase bold" type="button" id="search-user"
                                onclick="buscartodoTrue()">Ampliar Búsqueda</button>
                    </span>
                                                    </div>

                                                </div>

                                                <div class="col-md-12" id="divnohayexpedientes">
                                                    <div class="input-group ">

                                                    </div>

                                                </div>

                                                <div class="row" id="divbuttonbusquedaAv">
                                                    <div class="col-md-12" style="text-align: left">
                                                        <div class=" col-md-12 input-group has-warning"
                                                             style="text-align: left;display: inline-block;"
                                                             id="divApendBusqAvan">


                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="divBusquedaAvanzada" style="display:none">

                                                    <div class="form-group">
                                                        <label class=" col-md-2">Dependencia: </label>
                                                        <div class="col-md-10">
                                                            <div class="input-group input-icon right"
                                                                 style="margin-bottom: 20px !important;">


                                                                <select name="selectdependencia"
                                                                        class="form-control"
                                                                        id="selectdependencia">

                                                                </select>

                                                            </div>

                                                        </div>
                                                        <label class=" col-md-2">Serie: </label>
                                                        <div class="col-md-10">
                                                            <div class="input-group input-icon right"
                                                                 style="margin-bottom: 20px !important;">

                                                                <select name="serieavanzada" class="form-control"
                                                                        id="serieavanzada">

                                                                </select>

                                                            </div>

                                                        </div>
                                                        <label class=" col-md-2">Subserie: </label>
                                                        <div class="col-md-10">
                                                            <div class="input-group input-icon right"
                                                                 style="margin-bottom: 20px !important;">

                                                                <select name="subserieavanzada" class="form-control"
                                                                        id="subserieavanzada">

                                                                </select>

                                                            </div>

                                                        </div>
                                                        <label class=" col-md-2">Año: </label>
                                                        <div class="col-md-10">
                                                            <div class="input-group input-icon right"
                                                                 style="margin-bottom: 20px !important;">


                                                                <select name="selectanio" class="form-control"
                                                                        id="selectanio">

                                                                </select>

                                                            </div>

                                                        </div>
                                                        <label class=" col-md-2"><input type="hidden"></label>
                                                        <label class=" col-md-2">
                                                            <button type="button" class="btn green"
                                                                    onclick="resultAvanzados()">Buscar
                                                            </button>
                                                        </label>
                                                        <label class=" col-md-2">
                                                            <button type="button" class="btn default"
                                                                    onclick="limpiarBusquedaAvanz()">Limpiar
                                                            </button>
                                                        </label>
                                                        <label class=" col-md-2">
                                                            <button type="button" class="btn green"
                                                                    onclick="modalNuevoexpediente()">Nuevo
                                                                Expediente
                                                            </button>
                                                        </label>

                                                        <label class=" col-md-3"
                                                               style="float:center;text-align: center ">
                                                            <button type="button" class="btn default"
                                                                    onclick="cerrarBusquedaAvanz()">Cerrar
                                                            </button>
                                                        </label>


                                                    </div>
                                                </div>


                                                <div style="display:none" id="divinputtipologia">
                                                    <div class="col-md-12 search-page search-content-2">
                                                        <div class="search-bar bordered">
                                                            <div class="input-group" style="width: 100%;"
                                                                 id="divpreviotipologia">
                                                                <input type="text"
                                                                       style="color: #5E6373 !important;font-weight: 900 !important;font-size: 16px !important; width:100%;
                                                            border: 1px solid red !important;box-shadow:0 0 3px red !important;margin:10px !important"
                                                                       class="form-control typeahead"
                                                                       id="inputtipologia"
                                                                       placeholder="Elegir tipo documental">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>


                                        </div>
                                    </div>

                                    <div class="table-responsive  " id="open_table_expediente"
                                         style="position: relative;">
                                        <table class="table table-striped table-bordered table-hover
                                 table-checkable order-column dataTable no-footer" id="tablaexpedientes">
                                            <thead class="bg-blue font-white bold">
                                            <tr role="row">

                                                <th class="sorting" tabindex="0" aria-controls="sample_1"
                                                    rowspan="1"
                                                    colspan="1"
                                                    aria-label=" Id : activate to sort column ascending"
                                                    style="width: 20%;text-align:center"> Archivar
                                                </th>

                                                <th class="sorting" tabindex="0" aria-controls="sample_1"
                                                    rowspan="1"
                                                    colspan="1"
                                                    aria-label=" Asunto : activate to sort column ascending"
                                                    style="width: 40%;text-align:center"> Expediente
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="sample_1"
                                                    rowspan="1"
                                                    colspan="1"
                                                    aria-label=" Estado : activate to sort column ascending"
                                                    style="width: 40%;text-align:center">
                                                    Dependencia y TRD
                                                </th>

                                            </tr>
                                            </thead>
                                            <tbody id="tbodyexpediente"></tbody>
                                        </table>
                                    </div>

                                </div>

                            </div>
                        </div>


                    </form>
                </div>
                <div class="modal-footer"></div>

            </div>
        </div>
    </div>

    <div id="modal_nuevoexpediente" class="modal fade" tabindex="-2" aria-hidden="true">
        <div class="modal-dialog " style="width:80% !important;">
            <div class="modal-content">
                <div class="modal-header bg-blue-chambray bg-font-yellow text-center">

                    <button type="button" title="Cerrar" onclick="cerrarModalNuevoExped()"
                            class="btn btn-lg btnclosemodalNuevoExped"
                            style="float: right;background-color: #2C3E50!important; border-radius:20px !important" >
                        <i class="fa fa-remove"></i></button>
                    <h4 class="modal-title"><strong>Solicitar Nuevo Expediente</strong></h4>
                </div>
                <div class="modal-body" id="radicated_info">
                    <form action="" method="POST" class="form-horizonal" id="">
                        <div class="form-body modal-body todo-task-modal-body">
                            <div class="form-actions">
                                <div class="row">
                                    <span style="color:#0d0d0d;font-weight: 900;">Copiar el siguiente texto y enviarlo al correo electrónico: <?= $_SESSION['soporte_funcional'] ?></span>
                                </div>
                            </div>
                            <div class="row" style="margin-top:40px">
                                <div class="col-md-12 ">
                                    <div class="form-group has-error">

                                        <label class=" col-md-12" id="dependencianueva"> </label><br>

                                        <label class=" col-md-12" id="serienueva"></label><br>

                                        <label class=" col-md-12" id="subserienueva"></label><br>

                                        <label class=" col-md-12" id="anionuevo"></label> <br>

                                        <label class=" col-md-12"></label> <br>

                                        <label class=" col-md-12" id="nombrenuevo"></label><br>
                                        <label class=" col-md-12" id="descripcionnueva"></label>

                                        <br>
                                        <label class=" col-md-12"></label> <br>

                                        <label class=" col-md-12" id="asuntoborranuevo"></label><br>

                                        <label class=" col-md-12" id="radicadosincluirnue"></label>
                                        <br>
                                        <label class=" col-md-12"></label> <br>
                                        <label class=" col-md-12" id="usuarionuevo"></label>


                                    </div>
                                </div>

                            </div>
                        </div>


                    </form>
                </div>
                <div class="modal-footer">
                    <div class="form-actions">
                        <div class="row">


                            <div class="col-md-4">
                                <button type="button" class="btn btn-lg red btnclosemodalNuevoExped" data-dismiss="modal"
                                        style="border-radius:20px !important" onclick="cerrarModalNuevoExped()">
                                    Cerrar
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modaldescargarop" class="modal fade" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog " style="width:80% !important;"></div>
    </div>

    <div id="month_report_modal" class="modal fade" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog"></div>
    </div>
    <div id="rad_img_modal" class="modal fade" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog"></div>
    </div>

    <div class="modal fade bs-modal-lg in" id="newmodalexcluir" tabindex="-1" role="dialog" aria-hidden="true"
         style="display: none; padding-right: 16px;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Excluir</h4>
                </div>
                <div class="modal-body" id="modalbodynewexcluir">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-transparent red btn-outline btn-sm active" onclick="beforeExcluirExpednew()">Excluir</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

@endsection

@section('page-css')
    <style>
        .tab-pane {
            min-height: 420px;
        }

        .tabbable-line > .nav-tabs > li.active {
            border-bottom: 4px solid #FF9900 !important;
        }

        .tabbable-line > .nav-tabs > li:hover {
            border-bottom: 4px solid rgba(255, 154, 0, 0.49) !important;
        }

        .assoc {
            font-size: 14px;
            font-weight: 600;
            color: #9eacb4;
            margin-top: 25px;
        }

        .as {
            border: 1px solid #e0e6e9;
            background-color: #fff;
            color: #c1cbd1;
            text-decoration: none;
            padding: 0 .4em;
            font-size: 20px;
            margin: -.3em 0 0 .5em;
            float: none;
        }

        .divBusquedaAvanzada {
            background: #eee;
            padding: 3px 30px;
            max-height: 400px;
            overflow-y: scroll;
        }

        .divBusquedaAvanzada p {
            cursor: pointer;
        }

        .resultEnviar {
            background: #eee;
            padding: 3px 30px;
            max-height: 400px;
            overflow-y: scroll;
            margin-bottom: 20px;
        }

        .resultEnviar p {
            cursor: pointer;
        }

        .as:hover {
            background-color: #4db3a4;
            color: #fff;
            text-decoration: none
        }

        .as:focus {
            color: #c1cbd1;
            text-decoration: none
        }

    </style>

    <link href="{{$include_path}}global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"
          rel="stylesheet" type="text/css"/>

@endsection

@section('page-js')

    <script src="{{$include_path}}global/plugins/bootstrap-table/locale/bootstrap-table-es-SP.js"
            type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"
            type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.es.js"
            type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/moment.min.js" type="text/javascript"></script>

    <script src="{{$include_path}}global/plugins/jquery.pulsate.min.js" type="text/javascript"></script>

    <!--  datatable -->
    <script src="{{$include_path}}global/plugins/datatable/datatable.js" type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/datatable/datatables.bootstrap.js" type="text/javascript"></script>
    <script src="{{$include_path}}pages/scripts/table-datatables-responsive.js" type="text/javascript"></script>

    <!--  datatable -->
    <script src="{{$include_path}}global/plugins/typeahead/handlebars.min.js" type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/typeahead/typeahead.bundle.min.js" type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js"
            type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
            type="text/javascript"></script>
    <!-- $includepacth/pages/scripts/ecommerce-orders-view.min.js -->



    <!--  datatable -->
    <?php
    $fechah = date("dmy_h_m_s") . " " . time("h_m_s");
    $session_id = session_id();
    $phpsession = session_name() . "=" . trim(session_id());
    $ent = $_SESSION['entidad'];
    $dependencia = $_SESSION['dependencia'];
    $krd = $_SESSION["krd"];
    $encabezado = "$phpsession&krd=$krd&fechah=$fechah&primera=1&ent=$ent&depende=$dependencia";
    //$encabezado2="$phpsession&krd=$krd&fechah=$fechah&primera=1&ent=$ent&depende=$dependencia";
    $usua_nomb = $_SESSION["usua_nomb"];
    $depe_nomb = $_SESSION["depe_nomb"];
    $anioactual = date('Y');
    $permisocrear = $_SESSION["usuaPermExpediente"]; //variable que indica si tiene permiso de crear expedientes
    $usua_doc=$_SESSION["usua_doc"];

    ?>


    <script type="text/javascript">

        var adminsistema = '<?php echo $_SESSION['usua_admin_sistema'] ?>';
        var usuaPermExpediente = '<?php echo $_SESSION["usuaPermExpediente"] ?>';
        var usua_nomb = '<?php  echo $usua_nomb ?>';
        var depe_nomb = '<?php  echo $depe_nomb ?>';
        var anioactual = '<?php  echo $anioactual ?>';
        var usua_doc='<?php  echo $usua_doc ?>';

        var session_name='<?php echo session_name() ?>';
        var trim_session_id='<?php echo trim(session_id()) ?>';
        var session_name_string="'"+session_name+"'"
        var trim_session_id_string="'"+trim_session_id+"'"

        var phpsession = '<?php  echo $phpsession ?>'
        var midependencia = '<?php echo $dependencia ?>'
        var session_id = '<?php echo session_id()  ?>';
        var krd = '<?php echo $_SESSION["krd"] ?>';
        var codusuario = '<?php echo $_SESSION["usuario_id"] ?>'

        var radicado = "<?php echo $radicado ?>";
        var asuntoradicadoselected="<?php echo trim($inforadicado['ASUNTO']) ?>";

        var inputexpediente = ""; //el input de los expedientes
        var inputtipologia = ""; //el input de las tipologias
        var buscartodo = false; //para saber si va a buscar todo en el modal de expedientes
        var seleccexped = false; //para saber si ya hizo click al select de los expedientes
        var selecttipologia = false;//para saber si ya hizo click al select de las tipologias
        var tipologiaselected = "";
        var expedienteselected = ""; //expediente seleccionado en el modal de agregar a expedientes
        var expedientestabla = new Array();
        var yaincluyouno = false; //para saber si ya incluyo uno, y habilitar la caja de tiplogias
        var tablalistacexpedientes = null;
        var base_url = '<?php echo $_SESSION['base_url']; ?>'
        var permisocrear = '<?php echo $permisocrear ?>'
        var globaltieneexped = '<?php echo $tieneexpedientes ?>' //la variable que viene desde el index

        var expedSelectedBtnAzul = "";//expediente seleccionado cada vez e el boton de arriba azul
        var tituloexpedselected="" //titulo dele xpediente seleccionado

        //esta variable que esta en el config.php ubicado en la raiz,  valida si pido las validaciones
        // al momento de querer excluir
        var VALIDAEXCLUYE_EXPED='<?php echo $validarexcluir ?>';
        var expedmasreciente='<?php echo $expedmasreciente ?>';//el expediente as reciente

        var expedientebyget='<?php echo $expedientebyget ?>';//si viene distinto de vacio es porque se mando
        // a buscar un expediente expecifico viene del index


        if(expedientebyget!=""){
            expedmasreciente=expedientebyget;
            //esto es por si llega un expediente especifico, lo asigno para
            //que abajo busque la informacion
        }

        //puede excluir cualquier radicado de cualquier expediente, si >0
        var usuaPermExpediente='<?= $_SESSION["usuaPermExpediente"]?>';

        console.log('pulsate test roger');

        $('#inputtipologia').pulsate({
            color: $(this).css("background-color","red"), // set the color of the pulse
            reach: 20,                              // how far the pulse goes in px
            speed: 1000,                            // how long one pulse takes in ms
            pause: 1000,                               // how long the pause between pulses is in ms
            glow: true,                             // if the glow should be shown too
            repeat: true,                           // will repeat forever if true, if given a number will repeat for that many times
            onHover: false                          // if true only pulsate if user hovers over the element
        });

        /*funciones que tienen que ver con expedientes*/
        function cerrarBusquedaAvanz() {
            setTimeout(function () {
                //$("#divinputtipologia").css('display','block');//muestro el input de tipologia
                $("#divbuttonbusquedaAv").css('display', 'block');//muestro el enlace de busqueda avanzada
                $(".divBusquedaAvanzada").css('display', 'none');//el div completo lo oculto
                $("#divinputexpediente").css('display', 'block')

                if (tipologiaselected != "") {
                    $("#divinputtipologia").css('display', 'block');
                }

            }, 500)

        }

        function cerrarModalNuevoExped() {
            $("#modal_nuevoexpediente").modal('hide');

            //si inputexpedientees ="" es porque no ha levantado por primera vez el modal de expedientes
            //por lo que hago la solicitud
            //si ya lo levanto al menos una vez, lo que hago es mostrar el modal, porque ya esta definido
            if (inputexpediente == "") {
                modalexpediente();
            } else {
                $("#modal_expediente").modal('show');
            }

        }

        function changeBusquedaAvanzada(dependencia, queactualizar) {  //cuando se abre el modal de busqueda avanzada, o cuando se hace un change de esos select

            $.ajax({
                url: base_url + "/app/borradores/generalborrador.php",
                method: "POST",
                data: {
                    function_call: "busqAvaExpediente",
                    dependencia: dependencia,
                    serie: $("#serieavanzada").val(),
                    subserie: $("#subserieavanzada").val(),
                    anio: $("#selectanio").val(),
                },
                dataType: "json",
                async: false,
                success: function (response) {
                    //$("#divinputtipologia").css('display','none');//oculto el input de tipologia
                    $("#divbuttonbusquedaAv").css('display', 'none');//oculto el enlace de busqueda avanzada
                    $("#divinputexpediente").css('display', 'none');

                    for (var j = 0; j < queactualizar.length; j++) {


                        if (queactualizar[j] == "dependencia") {

                            var html = "<option value=''></option>";
                            for (var i = 0; i < response.dependencias.length; i++) {
                                html += "<option value='" + response.dependencias[i]['DEPE_CODI'] + "'>" +
                                    response.dependencias[i]['DEPENDENCIA'] + " - " + response.dependencias[i]['DEPE_CODI'] + "</option>"
                            }
                            $("#selectdependencia").html(html);
                        }


                        if (queactualizar[j] == "anio") {
                            html = "<option value=''></option>";
                            for (var i = 0; i < response.anios.length; i++) {
                                html += "<option value='" + response.anios[i]['AÑO'] + "'> " +
                                    "" + response.anios[i]['AÑO'] + "</option>"
                            }
                            $("#selectanio").html(html);
                            //$("#selectanio").val(anioactual);
                        }


                        if (queactualizar[j] == "serie") {
                            html = "<option value=''></option>";
                            for (var i = 0; i < response.series.length; i++) {
                                html += "<option value='" + response.series[i]['SGD_SRD_CODIGO'] + "'> " +
                                    "" + response.series[i]['SERIE'] + "</option>"
                            }
                            $("#serieavanzada").html(html);
                        }

                        if (queactualizar[j] == "subserie") {
                            html = "<option value=''></option>";
                            for (var i = 0; i < response.subseries.length; i++) {
                                html += "<option value='" + response.subseries[i]['SGD_SBRD_CODIGO'] + "'> " +
                                    "" + response.subseries[i]['SUBSERIE'] + "</option>"
                            }
                            $("#subserieavanzada").html(html);
                        }

                        /* $('#selectdependencia').trigger('change.select2');
                         $('#selectanio').trigger('change.select2');
                         $('#serieavanzada').trigger('change.select2');
                         $('#subserieavanzada').trigger('change.select2');*/

                    }
                }
            })
        }

        function modalBusquedaAvanz() { //cuando se presiona el enlace de busqueda avanzada

            //abre la pantalla de busqueda avanzada de expedientes
            $("#selectdependencia").html('').change()
            $("#selectdependencia").html('').change()
            $("#serieavanzada").val('');
            $("#subserieavanzada").val('');

            var resultDropdown = $(".divBusquedaAvanzada");
            resultDropdown.css('display', 'block');

            $("#divinputtipologia").css('display', 'none');

            queactualizar = new Array();
            queactualizar[0] = "dependencia";
            queactualizar[1] = "serie";
            //queactualizar[2]="subserie";
            //queactualizar[3]="anio";
            definirselect2Avanzadas(); //defino los select2
            changeBusquedaAvanzada(midependencia, queactualizar); //jhago la busqueda

            $("#selectdependencia").val(midependencia);
            $('#selectdependencia').trigger('change.select2');

            $("#selectanio").val()
            $('#selectanio').trigger('change.select2');

        }

        function limpiarBusquedaAvanz() {

            //cuandos e presiona el boton limpiar  en el modal de expedientes avanzados
            queactualizar = new Array();
            queactualizar[0] = "dependencia";
            //queactualizar[1] = "serie";
            //queactualizar[2]="subserie";
            //queactualizar[3]="anio";

            $('#selectdependencia').html('');
            $('#serieavanzada').html('');
            $('#subserieavanzada').html('');
            $('#selectanio').html('');
            changeBusquedaAvanzada(midependencia, queactualizar); //hago la busqueda

            //defino la tabla nuevaente y vuelvo a cargar los datos de la tabla
            definirTablaExpediente();
            expedientestabla = new Array();
            buscarMisExped();
        }


        function definirselect2Avanzadas() { //define los select2 de los selectd e busqueda avanzada

            $("#selectdependencia").select2(
                {
                    width: '70%',
                    dropdownParent: $("#modal_expediente"),
                    allowClear: true,

                    placeholder: 'Buscar Dependencias',
                    escapeMarkup: function (markup) {
                        return markup;
                    }, // let our custom formatter work

                    language: {
                        inputTooShort: function () {
                            return 'Ingrese una dependencia para buscar';
                        },
                        noResults: function () {
                            return "Sin resultados";
                        }
                    }
                }).on('select2:select', function (e) {

                queactualizar = new Array();
                queactualizar[0] = "serie";
                queactualizar[1] = "subserie";
                queactualizar[2] = "anio";

                changeBusquedaAvanzada($("#selectdependencia").val(), queactualizar);
            }).on("select2:unselect", function (e) {

                $('#serieavanzada').html('');
                $('#subserieavanzada').html('');
                $('#selectanio').html('');

            }).trigger('change');

            $("#selectanio").select2(
                {
                    width: '70%',
                    dropdownParent: $("#modal_expediente"),
                    allowClear: true,

                    placeholder: 'Buscar por Año',
                    escapeMarkup: function (markup) {
                        return markup;
                    }, // let our custom formatter work

                    language: {
                        inputTooShort: function () {
                            return 'Ingrese un año para buscar';
                        },
                        noResults: function () {
                            return "Sin resultados";
                        }
                    }
                }).on('select2:select', function (e) {
                queactualizar = new Array();
                changeBusquedaAvanzada($("#selectdependencia").val(), queactualizar);
            });

            $("#serieavanzada").select2(
                {
                    width: '70%',
                    dropdownParent: $("#modal_expediente"),
                    allowClear: true,

                    placeholder: 'Buscar por serie',
                    escapeMarkup: function (markup) {
                        return markup;
                    }, // let our custom formatter work

                    language: {
                        inputTooShort: function () {
                            return 'Ingrese una serie';
                        },
                        noResults: function () {
                            return "Sin resultados";
                        }
                    }
                }).on('select2:select', function (e) {
                queactualizar = new Array();
                queactualizar[0] = "subserie";
                queactualizar[1] = "anio";
                $('#subserieavanzada').html('');
                $('#selectanio').html('');
                changeBusquedaAvanzada($("#selectdependencia").val(), queactualizar);
            }).on("select2:unselect", function (e) {

                $('#subserieavanzada').html('');
                $('#selectanio').html('');
            }).trigger('change');

            $("#subserieavanzada").select2(
                {
                    width: '70%',
                    dropdownParent: $("#modal_expediente"),
                    allowClear: true,

                    placeholder: 'Buscar por sub serie',
                    escapeMarkup: function (markup) {
                        return markup;
                    }, // let our custom formatter work

                    language: {
                        inputTooShort: function () {
                            return 'Ingrese una sub serie';
                        },
                        noResults: function () {
                            return "Sin resultados";
                        }
                    }
                }).on('select2:select', function (e) {
                queactualizar = new Array();
                queactualizar[0] = "anio";
                $('#selectanio').html('');
                changeBusquedaAvanzada($("#selectdependencia").val(), queactualizar);
            }).on("select2:unselect", function (e) {
                $('#selectanio').html('');
            }).trigger('change');
        }
        /*fin de funciones de busqueda avanzada*/


        /*funciones que tienen que ver con expedientes*/
        function resetearVariables() {

            if (inputexpediente == "") {
                inputexpediente = "";
                inputtipologia = "";
            }

            buscartodo = false;
            seleccexped = false; //para saber si ya hizo click al select de los expedientes
            selecttipologia = false;//para saber si ya hizo click al select de las tipologias
            tipologiaselected = "";
            expedienteselected = "";
            expedientestabla = new Array();
            yaincluyouno = false; //para saber si ya incluyo uno, y habilitar la caja de tiplogias
            tablalistacexpedientes = null;
        }

        function modalexcluir(){

            if(expedSelectedBtnAzul==""){
                Swal.fire("Alerta", "Dele seleccionar un expediente", "warning");
                return false;
            }
            //colocar condicion debajo tienePermExcluir()==true
            if(adminsistema==1 || permisocrear>0 || tienePermExcluir()==true){
                showmodalexcluir();
            }else{
                Swal.fire("Alerta", "No tiene permiso para excluir el radicado del expediente. Solicitar soporte", "warning");
            }

        }

        function showmodalexcluir(){
            //cuando se presiona el boton de excluir, pasa por unas validaciones antes, y si tiene permisos, llega aqui,

            var html="";
            html+="<strong>El radicado </strong> <span class='label label-info label-sm'>"+radicado+"</span> "+asuntoradicadoselected+" "+
                "<strong>será Excluido del expediente: </strong><br>"+
                "<span class='label label-warning label-sm'>"+expedSelectedBtnAzul+"</span> "+tituloexpedselected+"<br><br>"+
                "Si el documento y sus anexos ya fueron archivados físicamente, se reportará a Gestión Documental para que" +
                "sean retirados de dicha carpeta (expediente)."
            $("#modalbodynewexcluir").html(html)
            $("#newmodalexcluir").modal('show')
        }

        function beforeExcluirExpednew(){
            //cuando excluyo desde el boton nuevo del modal  excluir nuevo
            var response=excluirexpednew(expedSelectedBtnAzul);
            if(response!=false)
            {
                excluirNew(response);
            }

        }

        function excluirNew(response){
            expedSelectedBtnAzul="";
            limpiarcuadroverde();

            $("#tablaprinciexped").bootstrapTable('destroy');
            $("#newmodalexcluir").modal('hide');
            //actualizo la lista del boton azul en al aprte superior derecha
            actualizarlistaexped(response.misexped);
            $("#tablaprinciexped").bootstrapTable('destroy');
            var ret_pay_cont = $("#tableopcontainer");
            ret_pay_cont.html('');
            ret_pay_cont.removeClass('hide');
            $('#loader_tableopcontainer').addClass('hide');
        }

        function excluirexpednew(expediente){
            //boton de excluir en el modal nuevo de excluir
            //ya para escluirlo definitivamente
            var response=false;
            $.ajax({
                url: base_url + "/app/expedientes/generalexpediente.php",
                method: "POST",
                async: false,
                data: {
                    radicado: radicado,
                    expediente:expediente,
                    function_call: "escluirRadExped"
                },
                dataType: "json",
                success: function (data) {
                    if(data.error){
                        Swal.fire("Error", "Ocurrió un error al excluir el expediente", "error");
                    }else{
                        if(data.tieneexpedientes<1){
                            window.location.href = base_url + "/app/expedientes/index.php?radicado=" + radicado
                        }else{
                            Swal.fire("Excluido con éxito", "", "success");
                            response=data;
                        }

                        $("#spancantexped").html('')
                        $("#spancantexped").html(data.tieneexpedientes)
                    }
                }
            }).done(function () {

            }).fail(function (error) {
                Swal.fire("Error", "Ocurrió un error al validar, por favor comunicarse con soporte", "error");
            });

            return response
        }

        function tienePermExcluir(){
            //busco
            var tienepermiso = "";
            if(usuaPermExpediente>0){
                return true;
            }
            $.ajax({
                url: base_url + "/app/expedientes/generalexpediente.php",
                method: "POST",
                async: false,
                data: {
                    radicado: radicado,
                    expediente:expedienteselected,
                    function_call: "tienePermExcluir"
                },
                dataType: "json",
                success: function (response) {

                    if(response.success){
                        var data=response.success
                        if(data[0].USUA_DOC==usua_doc){
                            tienepermiso=true;
                        }

                        if(data[0].DEPE_CODI==midependencia){
                            tienepermiso=true;
                        }

                    }else{
                        Swal.fire("Error", "Ocurrió un error al validar si tiene el permiso para excluir", "error");
                        tienepermiso=false;
                    }

                }
            }).done(function () {

            }).fail(function (error) {
                Swal.fire("Error", "Ocurrió un error al validar si tiene permiso, por favor comunicarse con soporte", "error");
            });
            return tienepermiso;
        }

        function solocloseModNuevExped(){
            //solo cierra el modal de pedir nuevo expediente, y se usa cuando se presiona sobre el boton crear
            $("#modal_nuevoexpediente").modal('hide');
        }

        function accioncrear(){

            //cuandos se presiona el botn crear
            //si tiene permiso lo redirecciono
            if(permisocrear>0){
                window.location.href = base_url + "/expediente/tipificarExpediente.php?rad="+radicado
            }else{
                $(".btnclosemodalNuevoExped").attr('onclick','solocloseModNuevExped()')
                //sino entra aqui y levato el modal
                modalNuevoexpediente();
            }

        }

        function modalNuevoexpediente() {

            $("#modal_nuevoexpediente").modal('show');
            $("#dependencianueva").html("<span style='color:#2458E1'>Dependencia:  </span>" + $("#selectdependencia option:selected").text());
            $("#serienueva").html("<span style='color:#2458E1'>Serie: </span>" + $("#serieavanzada option:selected").text());
            $("#subserienueva").html("<span style='color:#2458E1'>Subserie: </span>" + $("#subserieavanzada option:selected").text());
            $("#anionuevo").html("<span style='color:#2458E1'>Año:</span>  ")
            $("#nombrenuevo").html("<span style='color:#2458E1'>Nombre del expediente: <span style='color:#0d0d0d;font-weight: 900'>Escriba aquí el Título o Nombre del expediente</span>");
            $("#descripcionnueva").html("<span style='color:#2458E1'>Borrador asunto: </span> " + $("#asunto").val());
            $("#radicadosincluirnue").html("<span style='color:#2458E1'>Radicados:</span> Escribir aquí los radicados que desea incluir en el nuevo expediente (si aplica)");
            $("#usuarionuevo").html("<span style='color:#2458E1'>Usuario:</span>  " + usua_nomb + " - " + depe_nomb);

        }

        function modalexpediente() {
            $("#lblnumradexped").html('RADICADO: '+radicado);
            ///con esto mando a llamar la funcion cerrarModalNuevoExped para que cuando se cierre este modal
            //abra el modal de expedientes
            $(".btnclosemodalNuevoExped").attr('onclick','cerrarModalNuevoExped()')
            resetearVariables();
            cerrarBusquedaAvanz();
            buscartodo = false;
            $('#modal_expediente').modal({show: true, keyboard: false, backdrop: 'static'});
            $("#inputexpediente").val('');
            $("#inputtipologia").val('');
            $("#tbodyexpediente").html('');
            $("#divApendBusqAvan").css('display', 'none');//el enace de busqueda avanzada


            yaincluyouno = false;
            tipologiaselected = "";
            if (inputexpediente == "") {
                inputexpediente = $('#inputexpediente');
                inputexpediente.typeahead({
                        hint: false,
                        highlight: false,
                        minLength: 3
                    },
                    {
                        name: 'nombre',
                        source: users.ttAdapter(),
                        display: function (data) {

                            return data.NUMERO_EXPEDIENTE + ' ' + data.TITULO_NOMBRE + ' ' + data.ESTADO + ' ' + data.DEPENDENCIA + ' ' + data.TRD;
                        },
                        limit: 50
                    });

                /*
                 *    return {NUMERO_EXPEDIENTE: item.NUMERO_EXPEDIENTE, DEPENDENCIA: item.DEPENDENCIA,
                 TITULO_NOMBRE: item.TITULO_NOMBRE,DESCRIPCION:item.DESCRIPCION,
                 TRD:item.TRD,ESTADO:item.ESTADO};
                 */

                inputexpediente.bind('typeahead:change', function (ev) {

                    var selected = false;
                    var value = ev.target.value;
                    $('.tt-suggestion.tt-selectable').each(function () {
                        if (value === $(this).text()) {
                            selected = true;
                        }
                    });
                    if (!selected) {
                        //inputexpediente.typeahead('val', '');
                    }
                });


                inputexpediente.bind('typeahead:selected', function (obj, datum, name) {
                    //aqui pasa cuando se selecciona un item
                    console.log('datum', datum); // contains datum value, tokens and custom fields
                    seleccexped = true;
                    expedienteselected = datum;
                    validarYaestaIncluido(datum);
                    inputexpediente.typeahead('val', '');
                });
            }
            $('#inputexpediente').css('background-color', '#dad8d9');

            definirinputtipologia();
            definirTablaExpediente();

            var entablatemp = expedientestabla;
            expedientestabla = null;
            expedientestabla = new Array();

            var retornoMisExpe = buscarMisExped();
            // buscarExpedRadAso();
            if (validaSeleccionoTipologia(retornoMisExpe) == false) {
                alertaTipologia();
            }

            if (tipologiaselected != "") {
                var textotipologiaactual = inputtipologia.typeahead('val');
                $("#divinputtipologia").css('display', 'block');
                tipologiasinborde();
                definirinputtipologia();
                inputtipologia.typeahead('val', textotipologiaactual);
            }

        }

        function validaSeleccionoTipologia(retornoMisExpe) {
            //esto lo hago para que verifique si no estan incluidos los expeientes que ya ha agregado
            var tienetipologia = true;
            for (var i = 0; i < retornoMisExpe.length; i++) {
                if (retornoMisExpe[i]['tipologia'] == "") {
                    tienetipologia = false;
                    yaincluyouno = true;

                    break;
                }
            }

            return tienetipologia;

        }

        function alertaTipologia() {

            $("#divinputtipologia").css('display', 'block');
            setTimeout(function () {
                $("#inputtipologia").css('border', '1px solid red !important');
                $("#inputtipologia").css('box-shadow', '0 0 3px red !important');
                $("#inputtipologia").css('margin', '10px !important');
            }, 500);
        }

        function tipologiasinborde() { //pone el input de tipologia sin el borde ojo, ya que hay
            // qeu volverlo a definir para que tome el quita el borde rojo
            $("#divinputtipologia").html('');
            $("#divinputtipologia").html('<div class="col-md-12 search-page search-content-2"> ' +
                '<div class="search-bar bordered"> ' +
                '<div class="input-group" style="width: 100%;" id="divpreviotipologia"> ' +
                '<input type="text" style=" color: #5E6373 !important;font-size: 16px !important; width:100%;"' +
                ' class="form-control typeahead"' +
                'id="inputtipologia"placeholder="Elegir tipo documental"> </div> </div> </div>');

        }

        //define el input que tiene las tpologias
        function definirinputtipologia() {

            inputtipologia = $('#inputtipologia');
            inputtipologia.typeahead({
                    hint: false,
                    highlight: false,
                    minLength: 3
                },
                {
                    name: 'nombre',
                    source: tipologias.ttAdapter(),
                    display: function (data) {
                        return data.SGD_TPR_DESCRIP;
                    },
                    limit: 50
                });

            inputtipologia.bind('typeahead:change', function (ev) {

                var selected = false;
                var value = ev.target.value;
                $('.tt-suggestion.tt-selectable').each(function () {
                    if (value === $(this).text()) {
                        selected = true;
                    }
                });
                if (!selected) {
                    // inputtipologia.typeahead('val', '');
                }
            });

            inputtipologia.bind('typeahead:selected', function (obj, datum, name) {

                inputtipologia.typeahead('val', datum.SGD_TPR_DESCRIP);
                tipologiaselected = datum.SGD_TPR_CODIGO;
                selecttipologia = true;
                if (selecttipologia == true) {
                    incluircontipologia();
                }
            });

            $('#inputtipologia').css('background-color', '#dad8d9');

        }

        function definirTablaExpediente() {

            $("#open_table_expediente").html('');

            $("#open_table_expediente").append('   <table class="table table-striped table-bordered table-hover ' +
                'table-checkable order-column dataTable no-footer" id="tablaexpedientes" > <thead class="bg-blue font-white bold"> ' +
                '<tr role="row"> <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" ' +
                'aria-label=" Id : activate to sort column ascending" style="width: 20%;text-align:center"> Archivar </th>' +
                '<th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1"' +
                ' aria-label=" Asunto : activate to sort column ascending" style="width: 40%;text-align:center"> Expediente </th> ' +
                '<th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1"' +
                ' aria-label=" Estado : activate to sort column ascending" style="width: 40%;text-align:center">' +
                'Dependencia y TRD </th> </tr> </thead> <tbody id="tbodyexpediente"></tbody> </table>');

            tablalistacexpedientes = null;

            tablalistacexpedientes = $('#tablaexpedientes').DataTable({
                autoWidth: false,
                "columns": [
                    {"width": "20%"},
                    {"width": "40%"},
                    {"width": "40%"},
                ],
                "order": [],
                "columnDefs": [{
                    "targets": 'no-sort',
                    "orderable": false,
                }],
                "language": {
                    "emptyTable": "No se encontraron registros",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ resultados",
                    "infoEmpty": "Mostrando 0 a 0 de 0 resultados",
                    "infoFiltered": "(filtrado de _MAX_ total resultados)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ resultados",
                    "loadingRecords": "Cargando...",
                    "processing": "Buscando...",
                    "search": "Búsqueda:",
                    "zeroRecords": false,
                    "paginate": {
                        "first": "<<",
                        "last": ">>",
                        "next": "<",
                        "previous": ">"
                    },
                    "aria": {
                        "sortAscending": ": activar ordenar columnas ascendente",
                        "sortDescending": ": activar ordenar columnas descendente"
                    }
                }
            });
        }

        function buscarMisExped(armar=true) {
            //si el borrador tiene radicados asociados, busca a ver si esos radicados estan en expedientes,
            //para mostrarlos como sugerencia en el modal de expedientes

            var retornoMisExpe = "";
            $.ajax({
                url: base_url + "/app/borradores/generalborrador.php",
                method: "POST",
                async: false,
                data: {
                    radicado: radicado,
                    function_call: "buscarMisExpedEntrada"
                },
                dataType: "json",
                success: function (response) {
                    retornoMisExpe = response
                    if (armar == true) {
                        recibeExpedientes(response);
                    }

                }
            }).done(function () {

            }).fail(function (error) {
                Swal.fire("Error", "Ocurrió un error al validar si está incluido al borrador", "error");
            });
            return retornoMisExpe;
        }

        function recibeExpedientes(response) {

            var yaestaentabla = "";

            var estado = "";
            var tamano = "";

            for (var i = 0; i < response.length; i++) {
                expedienteselected = response[i];
                yaestaentabla = validarYaestaEnTabla() //valido si ya lo agregue en la tabla


                if (expedienteselected['tipologia'] != undefined) {
                    if (expedienteselected['tipologia'] != "") {
                        inputtipologia.typeahead('val', expedienteselected['tipologia']);
                        yaincluyouno = true;
                        tipologiaselected = expedienteselected['tipologiacodigo'];

                    }
                }
                if (yaestaentabla == false) {
                    tamano = Object.keys(expedientestabla).length;
                    //lo guardo en el arreglo de expedientes que he agregado a la tabla
                    if (tamano < 1) {
                        expedientestabla[0] = {};
                        expedientestabla[0] = response[i];
                        //expedientestabla[0].TIPOLOGIA=tipologiaselected;
                    } else {
                        expedientestabla[tamano] = {};
                        expedientestabla[tamano] = response[i];
                        //expedientestabla[tamano].TIPOLOGIA=tipologiaselected;
                    }

                    estado = "";
                    if (response[i]['incluiExpe'] == "incluido") {
                        estado = "incluido";

                    } else {

                        estado = response[i].ESTADO;
                        if (response[i].ESTADO == null) {
                            estado = "Abierto"
                        }
                    }
                    armartablaexpediente(response[i], estado)
                }
            }

        }

        function cerrarModalExpediente() {
            //cierra el modal de expediente, pero antes valida si ya se selecciono una tipologia  si ya incluyo un expediente
            if (tipologiaselected == "" && yaincluyouno == true) {

                var encontroincluido = false;
                for (var i = 0; i < Object.keys(expedientestabla).length; i++) {
                    if (expedientestabla[i]['incluiExpe'] == "incluido") {
                        encontroincluido = true;
                    }
                }

                if (encontroincluido == true) {
                    Swal.fire("Alerta", "Debe seleccionar una tipología", "warning");
                } else {
                    $("#modal_expediente").modal('hide');
                    window.location.href = base_url + "/app/expedientes/index.php?radicado=" + radicado
                }

            } else {
                $("#modal_expediente").modal('hide');
            }
        }

        function buscarExpedRadAso() {
            //si el borrador tiene radicados asociados, busca a ver si esos radicados estan en expedientes,
            //para mostrarlos como sugerencia en el modal de expedientes

            $.ajax({
                url: base_url + "/app/borradores/generalborrador.php",
                method: "POST",
                async: false,
                data: {
                    borrador_id: borrador_id,
                    function_call: "buscarExpedRadAso"
                },
                dataType: "json",
                success: function (response) {
                    recibeExpedientes(response);
                }
            }).done(function () {

            }).fail(function (error) {
                Swal.fire("Error", "Ocurrió un error al validar si está incluido al borrador", "error");
            });
        }

        function validarYaestaEnTabla() {

            //valido si ya seleccione un expediente con una tipologia ,para no duplciarlos en la tabla mostrada
            var yaestaentabla = false;
            var tamano = Object.keys(expedientestabla).length;

            if (tamano > 0) {
                for (var i = 0; i < expedientestabla.length; i++) {
                    if (expedientestabla[i].NUMERO_EXPEDIENTE == expedienteselected.NUMERO_EXPEDIENTE
                    ) {
                        yaestaentabla = true
                    }
                }
            }
            return yaestaentabla;

        }


        //cuando el doy click a una seleccion de tipologia, actualizo a los expedientes
        //seleccionados
        function incluircontipologia() {

            $.ajax({
                url: base_url + "/app/borradores/generalborrador.php",
                method: "POST",
                data: {
                    radicado: radicado,
                    idtipologia: tipologiaselected,
                    function_call: "incluircontipologiaEntrada"
                },
                dataType: "json",
                success: function (response) {

                    if (response.success) {
                        Swal.fire(response.success, "", "success");

                        var textotipologiaactual = inputtipologia.typeahead('val');
                        tipologiasinborde();
                        definirinputtipologia();
                        inputtipologia.typeahead('val', textotipologiaactual);

                    } else {
                        Swal.fire("Error", response.error, "error");
                    }

                }
            }).done(function () {

            }).fail(function (error) {
                Swal.fire("Error", "Ocurrió un error al validar si está incluido al borrador", "error");
            });

        }

        function validarYaestaIncluido() {

            //valido si esta en la tabla
            var yaestaentabla = validarYaestaEnTabla()
            if (yaestaentabla == false) {
                $.ajax({
                    url: base_url + "/app/borradores/generalborrador.php",
                    method: "POST",
                    data: {
                        radicado: radicado,
                        expediente: expedienteselected.NUMERO_EXPEDIENTE,
                        //idtipologia: tipologiaselected,
                        function_call: "estaincluidoexpedienteEntrada"
                    },
                    dataType: "json",
                    success: function (response) {

                        //si no esta en la tabla entonces lo muestro, indepddientemente de
                        // si ya fue incluido el expediente al borrador
                        if (response.success == "incluido") {
                            armartablaexpediente(expedienteselected, "incluido")

                        } else {
                            armartablaexpediente(expedienteselected, expedienteselected.ESTADO)
                        }

                        var tamano = Object.keys(expedientestabla).length;
                        //lo guardo en el arreglo
                        if (tamano < 1) {
                            expedientestabla[0] = {};
                            expedientestabla[0] = expedienteselected;
                            //expedientestabla[0].TIPOLOGIA=tipologiaselected;
                        } else {
                            expedientestabla[tamano] = {};
                            expedientestabla[tamano] = expedienteselected;
                            //expedientestabla[tamano].TIPOLOGIA=tipologiaselected;
                        }

                        updatetablaexpediente();

                    }
                }).done(function () {

                }).fail(function (error) {
                    Swal.fire("Error", "Ocurrió un error al validar si está incluido al borrador", "error");
                });
            } else {
                Swal.fire("Alerta", "Ya seleccionó este expediente", "warning");
            }
        }

        function armartablaexpediente(datum, estado) {
            var newrow = {};
            var count = 1;

            var expediente = "'" + datum.NUMERO_EXPEDIENTE + "'";

            if (estado == "Abierto") {

                var estatus = "'Abierto'";
                newrow[0] = '<button type="button" id="" class="btn  " ' +
                    'style="background-color: Transparent;background-repeat:no-repeat;border: none;cursor:pointer;overflow: hidden;' +
                    'outline:none;" ' +
                    'onclick="addBorraExped(event,' + estatus + ',' + expediente + ')">' +
                    ' <i class="fa fa-2x fa-share font-green-jungle"></i>' +
                    '<i class="fa fa-2x fa-folder-open-o font-blue"></i>' +
                    '</button><br> ' +
                    ' <a href="#" onclick="addBorraExped(event,' + estatus + ',' + expediente + ')" style="font-size:13pt !important;"><span  class="font-blue font-lg bold">Incluir</span><br>Abierto</a>';
            } else if (estado == "Cerrado") {
                var estatus = "'Cerrado'";
                newrow[0] = '<button type="button" id="" class="btn btn-lg " ' +
                    'style="background-color: Transparent;background-repeat:no-repeat;border: none;cursor:pointer;overflow: hidden;' +
                    'outline:none;" ' +
                    ' disabled>' +
                    ' <i class="fa fa-2x fa-close "></i>' +
                    '<i class="fa fa-2x fa-folder "></i>' +
                    '</button><br> ' +
                    '<span style="font-size:13pt !important;">Cerrado</span>';
            } else if (estado == "incluido") {
                var estatus = "'incluido'";

                newrow[0] = '<span style="font-weight: 900;font-size:14pt;color: #00802b" >Ya est&aacute; incluido</span>';

                //esta variable que esta en el config.php ubicado en la raiz,  valida si pido las validaciones al momento de
                //querer excluir

                if(VALIDAEXCLUYE_EXPED=='SI'){
                    newrow[0] +=' ' +
                        '<button type="button" id="" class="btn btn-lg " ' +
                        'onclick="delBorraExped(' + estatus + ',' + expediente + ')" ' +
                        'style="background-color: Transparent;background-repeat:no-repeat;border: none;cursor:pointer;overflow: hidden;' +
                        'outline:none;" ' +
                        '>' +
                        ' <i class="fa fa-2x fa-reply font-yellow-gold"></i>' +
                        '<i class="fa fa-2x fa-folder-open-o font-grey-silver"></i>' +
                        '</button><br>' +
                        '<a href="#" onclick="delBorraExped(' + estatus + ',' + expediente + ')" ' +
                        ' style="font-size:13pt !important;"><span  class="font-blue font-lg bold">Excluir</span></a><br>';
                }

            }

            var descripcion = datum.DESCRIPCION;
            if (descripcion == undefined) {
                descripcion = "";
            }

            newrow[count] = ' <a ' +
                'href="'+base_url+'/app/expedientes/index.php?radicado='+radicado+'&expediente='+datum.NUMERO_EXPEDIENTE+'" ' +
                'target="_blank"> <span class="font-blue bold">' + datum.NUMERO_EXPEDIENTE + '</span><br> ' +
                '<span class="font-grey-silver" style="color: #8f8a8d !important">' + datum.TITULO_NOMBRE + '<br>' +
                '' + descripcion.substring(0, 50) + '</span> </a> ';
            count++;

            newrow[count] = '<a href="'+base_url+'/app/expedientes/index.php?radicado='+radicado+'&expediente='+datum.NUMERO_EXPEDIENTE+'" ' +
                'target="_blank">  <span class="font-blue bold">' + datum.DEPENDENCIA + '</span><br> ' +
                '<span class="font-grey-silver" style="color:#8f8a8d !important">' + datum.TRD + '</span> </a> ';

            var arreglo = new Array();
            var tr = new Array();
            tr = Object.assign({}, newrow, arreglo);
            var rowNode = tablalistacexpedientes.row.add(tr).draw().node();
        }

        function addBorraExped(evento, estatus, expediente) {

            evento.preventDefault();
            if (estatus == "Cerrado") {
                Swal.fire("Espere", "Expediente Cerrado - Debe solicitar soporte para su apertura o la creación de " +
                    "un nuevo expediente", "warning");
            } else {

                $.ajax({
                    url: base_url + "/app/borradores/generalborrador.php",
                    method: "POST",
                    data: {
                        radicado: radicado, expediente: expediente, idtipologia: tipologiaselected,
                        function_call: "addexpedienteborraEntrada"
                    },
                    dataType: "json",
                    success: function (response) {


                        if (response.success) {


                            if (tipologiaselected != "") {
                                alertModal('Incluido en expediente',response.success, 'success', '10000')
                            } else {
                                /**
                                 * Entra aqui si no ha seleccionado la tipologia
                                 */
                                alertModal('Incluido en expediente', 'Ahora elija el Tipo Documental', 'success', '10000')

                            }

                            yaincluyouno = true;

                            $("#divinputtipologia").css('display', 'block');

                            //defino la tabla nuevaente y vuelvo a cargar los datos de la tabla
                            definirTablaExpediente();
                            var entablatemp = expedientestabla;
                            expedientestabla = new Array();
                            recibeExpedientes(response.misexped);
                            recibeExpedientes(entablatemp);
                            // buscarExpedRadAso();

                            setTimeout(function () {
                                $("#inputtipologia").css('border', '1px solid red !important');
                                $("#inputtipologia").css('box-shadow', '0 0 3px red !important');
                                $("#inputtipologia").css('margin', '10px !important');
                            }, 500);

                            //actualizo la lista azul superior derecha
                            actualizarlistaexped(response.misexped);

                        } else {
                            Swal.fire("Error", response.error, "error");
                        }

                    }
                }).done(function () {

                }).fail(function (error) {
                    Swal.fire("Error", "Ocurrio un error al incluir el expediente al borrador", "error");
                });
            }

        }

        function actualizarlistaexped(response){
            //actualiza la lista de expedientes a los que esta asociado el radicado,
            //pero en el boton azul que esta en la parte superior derecha
            var html="";
            var string="";

            for (var i = 0; i < response.length; i++) {
                string="'"+response[i]['NUMERO_EXPEDIENTE']+"'"
                html+=' <li> <a href="javascript:;" data-expednumero="'+response[i]['NUMERO_EXPEDIENTE']+'" ' +
                    ' onclick="buscarinfoexpediente('+string+')">'+response[i]['NUMERO_EXPEDIENTE']+ '' +
                    '/ '+response[i]['TITULO_NOMBRE']+'</a> </li>';
            }

            $("#ulexpedientes").html('');
            $("#ulexpedientes").html(html);

        }

        function resultAvanzados() {

            if ($("#selectdependencia").val() != "") {

                //el boton buscar de busqueda avanzada
                $.ajax({
                    url: base_url + "/app/borradores/generalborrador.php",
                    method: "POST",
                    data: {
                        dependencia: $("#selectdependencia").val(),
                        serie: $("#serieavanzada").val(),
                        subserie: $("#subserieavanzada").val(),
                        anio: $("#selectanio").val(),
                        function_call: "resultAvanzadosEntrada",
                        radicado: radicado
                    },
                    dataType: "json",
                    success: function (response) {
                        if (response.length > 0) {
                            definirTablaExpediente();
                            expedientestabla = new Array();
                            buscarMisExped();
                            recibeExpedientes(response);
                        } else {
                            Swal.fire({
                                    title: "Alerta",
                                    text: "No se encontraron expedientes que coincidan con la búsqueda, desea solicitar la " +
                                    "creación de un expediente Nuevo",
                                    type: "warning",
                                    showCancelButton: true,
                                    confirmButtonText: "Solicitar Nuevo",
                                    cancelButtonText: "Cerrar",
                                    closeOnConfirm: true,
                                    closeOnCancel: true
                                }).then((result) => {
                                if (result.value) {
                                modalNuevoexpediente();
                            } else if (
                                result.dismiss === Swal.DismissReason.cancel
                            ) {

                            }
                        });
                        }
                    }
                }).done(function () {

                }).fail(function (error) {
                    Swal.fire("Error", "Ocurrió un error al validar si está incluido al borrador", "error");
                });
            } else {
                Swal.fire("Alerta", "Debe elegir como mínimo la Dependencia a buscar", "warning");
            }
        }
        function updatetablaexpediente() {
            expedienteselected = "";
            inputexpediente.typeahead('val', '');
            selecttipologia = false;
            seleccexped = false;

        }


        //esto busca los xpedientes
        var users = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace,
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: base_url + "/app/borradores/getexpedientes.php?buscartodo=false&key=%QUERY",//"getexpedientes.php?buscartodo=&key=%QUERY",
                /* ajax : {
                 beforeSend: function(jqXhr, settings){
                 settings.data = $.param({function_call:"buscardatosexpediente",key:"%QUERY"})
                 },
                 type: "POST"

                 },*/
                /* prepare: function (settings) {
                 settings.type = "POST";
                 settings.contentType = "application/json; charset=UTF-8";
                 settings.data = JSON.stringify(query);
                 return settings;
                 },*/
                filter: function (x) {
                    if (x.length < 1) {


                        var html = '<span class="help-block" style="display: inline;" >No se encontraron expedientes que coincidan con</span> ' +
                            '<span style="color:#00b300;display: inline;" >' + $("#inputexpediente").val() + '.</span>' +
                            ' <span class="help-block" style="display: inline;">Puede usar la <a href="#" class="" style=""   ' +
                            'onclick="modalBusquedaAvanz()">B&uacute;squeda Avanzada</a>' +
                            ' o solicitar la creaci&oacute;n de un expediente ' +
                            ' <a href="#" class="" style=""   ' +
                            'onclick="modalNuevoexpediente()">Nuevo</a></span>';

                        $("#divApendBusqAvan").html(html)
                        $("#divApendBusqAvan").css('display', 'block');
                        $("#divbuttonbusquedaAv").css('display', 'block')

                    } else {
                        $("#divbuttonbusquedaAv").css('display', 'none');
                    }
                    return $.map(x, function (item) {


                        var opciones = {
                            NUMERO_EXPEDIENTE: item.NUMERO_EXPEDIENTE, DEPENDENCIA: item.DEPENDENCIA,
                            TITULO_NOMBRE: item.TITULO_NOMBRE, DESCRIPCION: item.DESCRIPCION,
                            TRD: item.TRD, ESTADO: item.ESTADO
                        }

                        return opciones;

                    });
                },
                replace: function () {
                    var q = base_url + "/app/borradores/getexpedientes.php?buscartodo=" + getbuscartodo() + "&key=" + encodeURIComponent(inputexpediente.typeahead('val'));

                    return q;
                },

                wildcard: "%QUERY"
            }

        });

        var tipologias = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace,
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: base_url + "/app/borradores/gettipologia.php?function_call=todas_tipologias&key=%QUERY",

                filter: function (x) {
                    return $.map(x, function (item) {

                        var opciones = {SGD_TPR_CODIGO: item.SGD_TPR_CODIGO, SGD_TPR_DESCRIP: item.SGD_TPR_DESCRIP}

                        return opciones;

                    });
                },

                wildcard: "%QUERY"
            }

        });
        users.initialize();
        tipologias.initialize();

        function getbuscartodo() {
            return buscartodo;
        }

        function buscartodoTrue() {
            buscartodo = true;
            var valor = $("#inputexpediente").val();
            Swal.fire("Espere", "Por favor Espere mientras buscamos los resultados", "warning");
            var url = base_url + "/app/borradores/getexpedientes.php?buscartodo=true&key=" + encodeURIComponent(inputexpediente.typeahead('val'));
            $.ajax({
                url: url,
                method: "GET",
                dataType: "json",
                success: function (response) {
                    if (response.length > 0) {
                        recibeExpedientes(response);

                    } else {

                        var html = '<span class="help-block" style="display: inline;" >No se encontraron expedientes que coincidan con</span> ' +
                            '<span style="color:#00b300;display: inline;" >' + $("#inputexpediente").val() + '.</span>' +
                            ' <span class="help-block" style="display: inline;">Puede usar la <a href="#" class="" style=""   ' +
                            'onclick="modalBusquedaAvanz()">B&uacute;squeda Avanzada</a>' +
                            ' o solicitar la creaci&oacute;n de un expediente ' +
                            ' <a href="#" class="" style=""   ' +
                            'onclick="modalNuevoexpediente()">Nuevo</a></span>';

                        $("#divApendBusqAvan").html(html)
                        $("#divApendBusqAvan").css('display', 'block');
                        $("#divbuttonbusquedaAv").css('display', 'block')
                    }
                    setTimeout(function () {
                        swal.close();
                    }, 1000)


                }
            }).done(function () {

            }).fail(function (error) {
                Swal.fire("Error", "Ocurrió un error al validar si está incluido al borrador", "error");
            });

            $("#divApendBusqAvan").css('display', 'block');
        }

        function afterExcluirModalViejo(){
            for(var i=0; i<Object.keys(expedientestabla).length;i++){
                if(expedientestabla[i]['NUMERO_EXPEDIENTE']==expediente){
                    expedientestabla[i]['incluiExpe']="no incluido"
                }
            }

            //defino la tabla nuevaente y vuelvo a cargar los datos de la tabla
            definirTablaExpediente();
            var entablatemp=expedientestabla;
            expedientestabla=null;
            expedientestabla=new Array();
            recibeExpedientes(response.misexped);
            recibeExpedientes(entablatemp);
            //actualizo la lista azul superior derecha
            actualizarlistaexped(response.misexped);
        }

        function delBorraExped(estatus,expediente){
            //excluye un borrador de un expediente, desde el modal AGREGAR EXPEDIENTES
            $.ajax({
                url: base_url+"/app/expedientes/generalexpediente.php",
                method: "POST",
                data: {
                    radicado: radicado, expediente: expediente,
                    function_call: "escluirRadExped"
                },
                dataType: "json",
                success: function (response) {

                    if (response.success) {
                        Swal.fire(response.success, "", "success");

                        for(var i=0; i<Object.keys(expedientestabla).length;i++){
                            if(expedientestabla[i]['NUMERO_EXPEDIENTE']==expediente){
                                expedientestabla[i]['incluiExpe']="no incluido"
                            }
                        }

                        //defino la tabla nuevaente y vuelvo a cargar los datos de la tabla
                        definirTablaExpediente();
                        var entablatemp=expedientestabla;
                        expedientestabla=null;
                        expedientestabla=new Array();
                        recibeExpedientes(response.misexped);
                        recibeExpedientes(entablatemp);
                        //actualizo la lista azul superior derecha
                        actualizarlistaexped(response.misexped);

                    } else {
                        Swal.fire("Error",response.error, "error");
                    }

                }
            }).done(function () {

            }).fail(function (error) {
                Swal.fire("Error", "Ocurrio un error al incluir el expediente al borrador", "error");
            });

        }



        function validaCrear() {
            //cuando se presiona el boton crear, valida si tiene el permiso o no
            //para redireccionar o mostrar el modal

            if (permisocrear == 0) {
                window.location.href = base_url + "/expediente/tipificarExpediente.php";
            } else {
                modalNuevoexpediente();
            }

        }

        function getInfExpediente(expediente) {
            //busca la informacio deun expediente

            $.ajax({
                url: base_url + "/app/expedientes/generalexpediente.php",
                method: "POST",
                data: {
                    expediente: expediente,
                    function_call: "getInfExpediente",
                },
                dataType: "json",
                success: function (response) {
                    if (response.infexpediente != undefined) {
                        if (response.infexpediente[0]['TITULO'] != undefined) {
                            //guardo el titulo por si mas adelante lo quuero excluir
                            tituloexpedselected=response.infexpediente[0]['TITULO'];
                            //mando a mostrar la info en el cuadro vere
                            mostrarInfExped(response.infexpediente);
                            $("#inftituloborder").html(' <i class="fa fa-folder-open-o"></i>' + expediente)
                            tablaprinciexped();
                        } else {
                            Swal.fire("Espere", "Ha ocurrido un error buscando la informacion del expediente o " +
                                "el expediente no tiene información", "warning");
                        }

                    } else {
                        Swal.fire("Error", response.error, "error");
                    }
                }
            }).done(function () {

            }).fail(function (error) {
                Swal.fire("Error", "Ocurrió un error, por favor contactar con soporte", "error");
            });
        }

        function limpiarcuadroverde(){
            //limpia el primer cuadro verde donde muestro la infor de l expediente seleccionado
            $("#inftituloborder").html('')
            $("#inftitulo").html('')
            $("#infdescripcion").html('')
            $("#infestado").html('')
            $("#inftrd").html('')
            $("#infproceso").html('')
        }

        function mostrarInfExped(infexpediente) {
            //muestra la info del expediente en el cuadro verde

            limpiarcuadroverde();

            $("#inftitulo").html(infexpediente[0]['TITULO'])
            $("#infdescripcion").html(infexpediente[0]['DESCRIPCION']);

            if (infexpediente[0]['ESTADO'] == "Abierto") {

                $("#infestado").removeClass('label-default')
                $("#infestado").addClass('label-success')
            } else {
                $("#infestado").removeClass('label-success')
                $("#infestado").addClass('label-default')
            }

            $("#infestado").html(infexpediente[0]['ESTADO'])
            $("#inftrd").html(infexpediente[0]['SERIE'] + " / " + infexpediente[0]['SUBSERIE'])
            $("#infproceso").html(infexpediente[0]['PROCESO'])

        }

        function buscarinfoexpediente(expediente) {
            //cuandos e presiona sobre alguno de los expedientes en el boton azul
            expedSelectedBtnAzul = expediente;
            getInfExpediente(expediente);
            $('#spanlistaexped').text(expediente);
            actualizartoltips();//actualizo los toltips
        }


        function tablaprinciexped() {

            $('#loader_tableopcontainer').removeClass('hide');

            $.ajax({
                url: base_url + "/app/expedientes/generalexpediente.php",
                method: "POST",
                data: {
                    function_call: "tablaprinciexped",
                },
                dataType: "html",
                success: function (response) {

                    $("#tablaprinciexped").bootstrapTable('destroy');
                    var ret_pay_cont = $("#tableopcontainer");
                    if (response != "ERROR") {
                        ret_pay_cont.html('');
                        ret_pay_cont.html(response);
                        ret_pay_cont.removeClass('hide');
                    }
                    $('#loader_tableopcontainer').addClass('hide');
                }
            }).done(function () {
                var data = {}
                data.expediente = expedSelectedBtnAzul;
                data.radicado = radicado; //envio el radicado para mostrar un asterizco el radicado actual
                var url = "datostablaexped.php"

                declarardatatableExp("tableop", url, data);
                $('#loader_tableopcontainer').addClass('hide');
            }).fail(function (error) {
                console.log(error);
                $('#loader_tableopcontainer').addClass('hide');
            });
            $('#beforetableopcontainer').addClass('hide');
        }


        function format(tr) {
            //mando a buscar la informacion y me retorna el html
            var a = mostrarinfor(tr.attr('data-radicado'), tr.attr('data-dependencia'), tr.attr('data-usuario_actual'),
                tr.attr('data-contratista_identif'))
            return a;
        }

        function armarInfo(response) {

            var anexcodigo="";
            var html='<ul role="group" class="jstree-children" style="">'
            for (var i = 0; i < response.anexos.length; i++) {
                anexcodigo="'"+response.anexos[i].ANEX_CODIGO+"'";
                html += '<li role="treeitem" aria-selected="true" aria-level="2" ' +
                    'aria-labelledby="node_153688394773911_anchor" id="node_1536883947739' +
                    '11" class="jstree-node  jstree-leaf"><i class="jstree-icon jstree-themeicon fa fa-file icon-state-warning icon-lg' +
                    ' jstree-themeicon-custom" role="presentation"></i>';
                if(response.anexos[i].TIENE_PERMISO=="SI"){
                    html += '<a class="jstree-anchor jstree-clicked" href="#" ' +
                        'onclick="funlinkArchivo('+anexcodigo+')" tabindex="-1" id="node_153688394773911_anchor">' +
                        '' + response.anexos[i].ANEX_NOMB_ARCHIVO + '</a>'
                }else{
                    html += '<a class="jstree-anchor jstree-clicked" href="javascript:noPermisoFiles()" ' +
                        ' tabindex="-1" id="node_153688394773911_anchor">' +
                        '' + response.anexos[i].ANEX_NOMB_ARCHIVO + '</a>'
                }
                html += ' </li>';

            }
            html+="</ul>";
            return html;

        }

        function mostrarinfor(radicadotr) {
            //muestra la informacion de la tabla principal
            var resultado = "";

            $.ajax({
                url: base_url + "/app/expedientes/generalexpediente.php",
                method: "POST",
                data: {radicado: radicadotr, function_call: "getAnexosRad"},
                dataType: "json",
                async: false,
                success: function (response) {
                    //armo el html del detalle
                    resultado = armarInfo(response)// resultDropdown.html();

                }
            }).done(function () {
            }).fail(function (error) {

            });
            return resultado;
        }
        function declarardatatableExp(tablaid, url, data) {

            var table = $('#tablaprinciexped').DataTable();
            table.destroy();
            var tableoptions = {
                "destroy": true,
                "processing": true,
                "serverSide": true,
                "ajax": {
                    "url": url,
                    "data": function (d) {
                        d.data = data
                    },
                },
                rowId: 'DT_RowId',
                retrieve: false,
                "aLengthMenu": [[50, 100, 500, -1], [50, 100, 500, "Todos"]],
                "scrollX": true,
                "scrollCollapse": true,

                /********para poder mover con las flechas*********/
                keys: true,
                "searching": true,
                "ordering": true,
                "bPaginate": true,
                fixedHeader: {
                    header: true,
                    footer: true
                },
                paging: true,
                info: true,
                "sScrollX": "100%",

                autoWidth: false,
                "columns": [
                    {
                        "width": "3%",
                        "class": "details-control"
                    },
                    {"width": "10%"},
                    {"width": "30%"},
                    {"width": "27%"},
                    {"width": "30%"},
                ],
                "order": [],
                "columnDefs": [
                    {"targets": 'no-sort', "orderable": false,"width": "3%"},
                    { "width": "20%", "targets": 1 },
                    { "width": "10%", "targets": 2 },
                    { "width": "40%", "targets": 3 },
                    { "width": "20%", "targets": 4 },

                ],
                'createdRow': function (row, data, dataIndex) {
                    $(row).attr('tabindex', dataIndex);
                    $(row).attr('data-radicado', data[5]);

                },
                "language": {
                    "emptyTable": "No se encontraron radicados",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ radicados",
                    "infoEmpty": "Mostrando 0 a 0 de 0 radicados",
                    "infoFiltered": "(filtrado de _MAX_ total radicados)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ radicados",
                    "loadingRecords": "Cargando...",
                    "processing": "Buscando...",
                    "search": "Búsqueda:",
                    "zeroRecords": "No se encontraron radicados",
                    "paginate": {
                        "first": "<<",
                        "last": ">>",
                        "next": "<",
                        "previous": ">"
                    },
                    "aria": {
                        "sortAscending": ": activar ordenar columnas ascendente",
                        "sortDescending": ": activar ordenar columnas descendente"
                    },
                    buttons: {
                        copyTitle: 'Copiar al portapapeles',
                        copySuccess: {
                            1: "Copiado al portapapeles",
                            _: "%d filas copiadas al portapapeles"
                        }
                    }
                },
                "buttons": [
                    {
                        extend: 'pdfHtml5',
                        extension: '.pdf',
                        filename: 'Expedientes',
                        title: 'ORFEO Expediente ',
                        footer: true,
                        exportOptions: {
                            stripNewlines: false
                        },
                        orientation: 'landscape',
                        pageSize: 'LEGAL',
                        /*customize: function (doc) {
                         doc.styles.title = {

                         alignment: 'center'
                         }
                         }*/

                    },
                    {
                        extend: 'csv',
                        extension: '.csv',
                        filename: 'Reporte',
                        footer: true

                    },
                    {
                        extend: 'excelHtml5',
                        extension: '.xlsx',
                        filename: 'Reporte',
                        footer: true,
                        customize: function (xlsx) {
                            console.log(xlsx);
                            var sheet = xlsx.xl.worksheets['sheet1.xml'];
                            var downrows = 3;
                            var clRow = $('row', sheet);
                            //update Row
                            clRow.each(function () {
                                var attr = $(this).attr('r');
                                var ind = parseInt(attr);
                                ind = ind + downrows;
                                $(this).attr("r", ind);
                            });

                            // Update  row > c
                            $('row c ', sheet).each(function () {
                                var attr = $(this).attr('r');
                                var pre = attr.substring(0, 1);
                                var ind = parseInt(attr.substring(1, attr.length));
                                ind = ind + downrows;
                                $(this).attr("r", pre + ind);
                            });

                            function Addrow(index, data) {
                                msg = '<row r="' + index + '">'
                                for (i = 0; i < data.length; i++) {
                                    var key = data[i].k;
                                    var value = data[i].v;
                                    msg += '<c t="inlineStr" r="' + key + index + '" s="42">';
                                    msg += '<is>';
                                    msg += '<t>' + value + '</t>';
                                    msg += '</is>';
                                    msg += '</c>';
                                }
                                msg += '</row>';
                                return msg;
                            }

                        },
                        exportOptions: {
                            stripNewlines: true,
                            columns: ':visible',
                            format: {
                                body: function (data, row, column, node) {
                                    data = $('<p>' + data + '</p>').text();
                                    return $.isNumeric(data.replace(',', '.')) ? data.replace(',', '.') : data;
                                },
                                footer: function (data, row, column, node) {
                                    // Strip $ from salary column to make it numeric
                                    data = $('<p>' + data + '</p>').text();
                                    return $.isNumeric(data.replace(',', '.')) ? data.replace(',', '.') : data;
                                }
                            }
                        }
                    },

                    {
                        extend: 'print',
                        text: 'Imprimir'
                    },
                    {
                        extend: 'copyHtml5',
                        text: 'Copiar',
                        copySuccess: {
                            1: "Copiado al portapapeles",
                            _: "%d filas copiadas al portapapeles"
                        },
                        copyTitle: 'Copiar al portapapeles',
                        copyKeys: 'Presionar <i>ctrl</i> or <i>\u2318</i> + <i>C</i> para copiar los datos de la tabla<br>a tu portapapeles del sistema.<br><br>To cancel, click this message or press escape.'
                    }],
                "dom":'<"row"<"pull-left"f ><"pull-right"l>>rt<"row"<"pull-left"i><"pull-right"p>><"row"<"pull-left"B>>',
                /************************************/
                "fnInitComplete": function (settings, json) {

                    setTimeout(function () {
                        $('div.dataTables_filter input').focus();
                    }, 5);
                },

                "fnDrawCallback": function (datos) {
                    //esto funciona cuando se hace el paginate, ya que con fnInitComplete solo lo hace una vez

                    setTimeout(function () {
                        $('div.dataTables_filter input').focus();
                    }, 5);

                },

                "footerCallback": function (row, data, start, end, display) {

                }


            };

            var table = $('#tablaprinciexped').DataTable(tableoptions);
            $(".dataTables_paginate").css('color', '#666 !important')

            // Array to track the ids of the details displayed rows
            var detailRows = [];

            //al darle click al primer td del priemr tr, mando a mostrar el detalle
            $('#tablaprinciexped tbody').on('click', 'tr td.details-control', function () {
                var tr = $(this).closest('tr');
                var row = table.row(tr);
                var idx = $.inArray(tr.attr('id'), detailRows);

                $('#loader_tableopcontainer').removeClass('hide');
                if (row.child.isShown()) {
                    //busco el i que es el boton azul para cambiar e el + o -
                    //entra aqui cuando lo mando a cerrar
                    var iconoazul = tr.find("i")
                    if (iconoazul.length > 0) {
                        iconoazul.removeClass('glyphicon-minus')
                        iconoazul.removeClass('icon-minus')
                        iconoazul.addClass('glyphicon-plus')
                        iconoazul.addClass('icon-minus')
                    }
                    tr.removeClass('details');
                    row.child.hide();

                    // Remove from the 'open' array
                    detailRows.splice(idx, 1);
                }
                else {

                    //entra aqui cuando voy a desplegar el detalle
                    tr.addClass('details');
                    //mando el tr
                    row.child(format(tr)).show();
                    var iconoazul = tr.find("i")
                    if (iconoazul.length > 0) {
                        iconoazul.removeClass('glyphicon-plus')
                        iconoazul.removeClass('icon-plus')
                        iconoazul.addClass('glyphicon-minus')
                        iconoazul.addClass('icon-minus')
                    }
                    // Add to the 'open' array
                    if (idx === -1) {
                        detailRows.push(tr.attr('id'));
                    }

                }
                $('#loader_tableopcontainer').addClass('hide');

            });

            // On each draw, loop over the `detailRows` array and show any child rows
            table.on('draw', function () {
                $.each(detailRows, function (i, id) {
                    $('#' + id + ' td.details-control').trigger('click');
                });
            });
        }


        function ventanahistorial(){
            //cuando se le da click a la pestana historial
            if(expedSelectedBtnAzul!=""){
                tablahistorial();
            }else{
                Swal.fire("Alerta", "Debe seleccionar un expediente, y presionar NUEVAMENTE el boton de Histórico", "warning");
                return false;
            }

        }

        function ventanaRadSelec(){
            //cuando se le da click a la Radicados Seleccionados
            tablaradselec();
        }

        function borrarTablas(){
            $("#tablaprinciexped").bootstrapTable('destroy');
            $("#tablaprincihistorial").bootstrapTable('destroy');
            $("#tablaradselec").bootstrapTable('destroy');
        }
        function tablaradselec() {

            $('#loader_tableopcontainer').removeClass('hide');

            borrarTablas();
            $.ajax({
                url: base_url + "/app/expedientes/generalexpediente.php",
                method: "POST",
                data: {
                    function_call: "tablaradselec",
                },
                dataType: "html",
                success: function (response) {

                    $("#tablaradselec").bootstrapTable('destroy');
                    var ret_pay_cont = $("#tableradseleccontainer");
                    if (response != "ERROR") {
                        ret_pay_cont.html('');
                        ret_pay_cont.html(response);
                        ret_pay_cont.removeClass('hide');
                    }
                    $('#loader_tableopcontainer').addClass('hide');
                }
            }).done(function () {
                var data = {}
                data.radicado=radicado
                var url = "datostablaradselec.php"


                decladartableradselec(url, data);
                $('#loader_tableopcontainer').addClass('hide');
            }).fail(function (error) {
                console.log(error);
                $('#loader_tableopcontainer').addClass('hide');
            });

        }

        function tablahistorial() {

            $('#loader_tableopcontainer').removeClass('hide');

            $.ajax({
                url: base_url + "/app/expedientes/generalexpediente.php",
                method: "POST",
                data: {
                    function_call: "tablahistorial",
                },
                dataType: "html",
                success: function (response) {

                    $("#tablaprincihistorial").bootstrapTable('destroy');
                    var ret_pay_cont = $("#tablehistorialcontainer");
                    if (response != "ERROR") {
                        ret_pay_cont.html('');
                        ret_pay_cont.html(response);
                        ret_pay_cont.removeClass('hide');
                    }
                    $('#loader_tableopcontainer').addClass('hide');
                }
            }).done(function () {
                var data = {}
                data.expediente=expedSelectedBtnAzul
                var url = "datostablahistorial.php"

                declarardatatableHistor(url, data);
                $('#loader_tableopcontainer').addClass('hide');
            }).fail(function (error) {
                console.log(error);
                $('#loader_tableopcontainer').addClass('hide');
            });
        }

        function declarardatatableHistor(url, data) {

            var table = $('#tablaprincihistorial').DataTable();
            table.destroy();
            var tableoptions = {
                "destroy": true,
                "processing": true,
                "serverSide": true,
                "ajax": {
                    "url": url,
                    "data": function (d) {
                        d.data = data
                    },
                },
                rowId: 'DT_RowId',
                retrieve: false,
                "aLengthMenu": [[50, 100, 500, -1], [50, 100, 500, "Todos"]],
                "scrollX": true,
                "scrollCollapse": true,

                /********para poder mover con las flechas*********/
                keys: true,
                "searching": true,
                "ordering": true,
                "bPaginate": true,
                fixedHeader: {
                    header: true,
                    footer: true
                },
                paging: true,
                info: true,
                "sScrollX": "100%",

                autoWidth: true,
                "order": [],
                "language": {
                    "emptyTable": "No se encontraron registros",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ resultados",
                    "infoEmpty": "Mostrando 0 a 0 de 0 resultados",
                    "infoFiltered": "(filtrado de _MAX_ total resultados)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ resultados",
                    "loadingRecords": "Cargando...",
                    "processing": "Buscando...",
                    "search": "Búsqueda:",
                    "zeroRecords": "No se encontraron resultados",
                    "paginate": {
                        "first": "<<",
                        "last": ">>",
                        "next": "<",
                        "previous": ">"
                    },
                    "aria": {
                        "sortAscending": ": activar ordenar columnas ascendente",
                        "sortDescending": ": activar ordenar columnas descendente"
                    }
                },
                /************************************/
                "fnInitComplete": function (settings, json) {

                    setTimeout(function () {
                        $('div.dataTables_filter input').focus();
                    }, 5);
                },

                "fnDrawCallback": function (datos) {
                    //esto funciona cuando se hace el paginate, ya que con fnInitComplete solo lo hace una vez
                    setTimeout(function () {
                        $('div.dataTables_filter input').focus();
                    }, 5);

                },

                "footerCallback": function (row, data, start, end, display) {

                }
            };

            var table = $('#tablaprincihistorial').DataTable(tableoptions);
            $(".dataTables_paginate").css('color', '#666 !important')

            // Array to track the ids of the details displayed rows
            var detailRows = [];

            // On each draw, loop over the `detailRows` array and show any child rows
            table.on('draw', function () {
                $.each(detailRows, function (i, id) {
                    $('#' + id + ' td.details-control').trigger('click');
                });
            });
        }


        function decladartableradselec(url, data) {

            var table = $('#tablaradselec').DataTable();
            table.destroy();
            var tableoptions = {
                "destroy": true,
                "processing": true,
                "serverSide": true,
                "ajax": {
                    "url": url,
                    "data": function (d) {
                        d.data = data
                    },
                },
                rowId: 'DT_RowId',
                retrieve: false,
                "aLengthMenu": [[50, 100, 500, -1], [50, 100, 500, "Todos"]],
                "scrollX": true,
                "scrollCollapse": true,

                /********para poder mover con las flechas*********/
                keys: true,
                "searching": true,
                "ordering": true,
                "bPaginate": true,
                fixedHeader: {
                    header: true,
                    footer: true
                },
                paging: true,
                info: true,
                "sScrollX": "100%",
                "columns": [
                    {
                        "width": "10%"
                    },
                    {"width": "40%"},
                    {"width": "20%"},
                    {"width": "30%"},
                ],
                "order": [],
                "columnDefs": [
                    {"width": "10%","targets": 0},
                    { "width": "40%", "targets": 1 },
                    { "width": "20", "targets": 2 },
                    { "width": "30%", "targets": 3 },

                ],
                autoWidth: false,
                "order": [],
                "language": {
                    "emptyTable": "No se encontraron registros",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ resultados",
                    "infoEmpty": "Mostrando 0 a 0 de 0 resultados",
                    "infoFiltered": "(filtrado de _MAX_ total resultados)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ resultados",
                    "loadingRecords": "Cargando...",
                    "processing": "Buscando...",
                    "search": "Búsqueda:",
                    "zeroRecords": "No se encontraron resultados",
                    "paginate": {
                        "first": "<<",
                        "last": ">>",
                        "next": "<",
                        "previous": ">"
                    },
                    "aria": {
                        "sortAscending": ": activar ordenar columnas ascendente",
                        "sortDescending": ": activar ordenar columnas descendente"
                    }
                },
                /************************************/
                "fnInitComplete": function (settings, json) {


                },

                "fnDrawCallback": function (datos) {

                },
                "footerCallback": function (row, data, start, end, display) {

                }
            };

            var table = $('#tablaradselec').DataTable(tableoptions);

        }

        function getTieneExpedientes(){
            //valida si el radicado tiene expedientes
            $.ajax({
                url: base_url + "/app/expedientes/generalexpediente.php",
                method: "POST",
                async: false,
                data: {
                    radicado: radicado,
                    function_call: "getTieneExpedientes"
                },
                dataType: "json",
                success: function (data) {
                    if(data.error){
                        Swal.fire("Error", "Ocurrió un error al vaidar si tiene expedientes", "error");
                    }else{
                        $("#spancantexped").html('')
                        $("#spancantexped").html(data.tieneexpedientes)
                        //si no tiene expedientes cuando cierro el modal de agregar expedientes
                        //entonces rtambien actualizo la vista
                        if(data.tieneexpedientes<1){
                            window.location.href = base_url + "/app/expedientes/index.php?radicado=" + radicado
                        }

                        //si no tenia ningun expediente y acabo de cerrar el modal de agregar expedientes, y alli le agregue expedientes
                        //entonces hago el redirect tambien para que actualice la vista
                        if(globaltieneexped<1 && data.tieneexpedientes>0){
                            window.location.href = base_url + "/app/expedientes/index.php?radicado=" + radicado
                        }
                    }
                }
            }).done(function () {

            }).fail(function (error) {
                Swal.fire("Error", "Ocurrió un error al validar, por favor comunicarse con soporte", "error");
            });
        }

        function actualizartoltips(){
            //actualiza los toltips de los botones
            $("#lblbtnexcluirnew").attr('title','Retira el radicado '+radicado+' del expediente '+expedSelectedBtnAzul)
            $("#lblbtncrearnew").attr('title','Crear un nuevo expediente para incluir el radicado '+radicado+' en el mismo.')
            $("#liradicadosrelacionados").attr('title','otros radicados asociados con '+radicado)
            $("#lihistorico").attr('title','muestra el historial completo del expediente '+expedSelectedBtnAzul)
        }

        $(document).ready(function () {

            $('.dropdown-menu a').click(function () {
                $('#spanlistaexped').text($(this).attr('data-expednumero'));

            });
            $(document).on('click', '.show-search', function () {
                inputexpediente.typeahead('val', '');
                $('.search-page.search-content-2').removeClass('hide');
                $('.invoice').html('').addClass('hide');
            });

            jQuery('#modal_expediente').on('hidden.bs.modal', function (e) {
                //cuando se cierra el modal de agregar expedientes
                getTieneExpedientes();
            });

            if(expedmasreciente!=""){
                buscarinfoexpediente(expedmasreciente)

            }
            actualizartoltips();

        })

    </script>



@endsection