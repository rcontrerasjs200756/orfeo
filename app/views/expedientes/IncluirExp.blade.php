<?php

use Philo\Blade\Blade;

require '../../vendor/autoload.php';
require_once '../../config.php';

if((isset($_SESSION) && !isset($_SESSION['codusuario'])) || (!isset($_SESSION) ))
{
    session_start();
}

$ruta_raiz = "../../";
if (isset($db)) unset($db);
include_once("$ruta_raiz/include/db/ConnectionHandler.php");
$db = new ConnectionHandler("$ruta_raiz");

ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
error_reporting(E_ALL);

$views = $ABSOL_PATH.'app/views';
$cache = $ABSOL_PATH.'app/cache';

//Clearing the views cache
\App\Helpers\MiscFunctions::clear_view_cache();

$blade          = new Blade($views, $cache);
$borradorHelper = new \App\Helpers\Borradores($db);


require_once("$ruta_raiz/include/tx/Radicacion.php");
require_once("$ruta_raiz/include/tx/Tx.php");

$msg="";
$radicado=$_GET['radicado'];

//si se quiere saber info de la siguiente variable, busqulo en la vista blade
$desde_borrador=0;
if(isset($_GET['desdeborrador']) && $_GET['desdeborrador']=="1"){
    $desde_borrador=1;
}
$radicadosconcoma=$radicado; //para poder enviarlo cuando sea varios radicados al seleccionar la tipologia
$radicado=explode(',',$_GET['radicado']);
//aqui pueden llegar varios radicados separados por ","
//por lo que le hago un explode y elimino el ultimo indice, ya que el ultimo siempre viene una coma sola
if(count($radicado)>0){
    unset($radicado[count($radicado)-1]);
}
$cont=0;
/*recorro los radicados para buscarle el asunto paratbodyexpediente mostrarlo en la siguiente vista*/
foreach ($radicado as $item) {
    $radicado[$cont]=Array();
    $radicado[$cont]['radicado']=$item;
    $infradicado=$borradorHelper->getRadicadoByNume_radi($item);
    $radicado[$cont]['asunto']= $infradicado[0]['RA_ASUN'];
    $radicado[$cont]['tipologia']= $infradicado[0]['TDOC_CODI'];
    $cont++;
}



$include_path=$include_path;

return $blade->view()->make('borradores.radicardesdeentrada', compact(
    'msg','radicado','radicadosconcoma','desde_borrador','include_path'

))->render();



?>