@extends('layouts.app')

@section('content')

    <?php
    $ruta_raiz = "../../..";
    include_once "$ruta_raiz/include/db/ConnectionHandler.php";
    $ruta_raiz = "../..";
    $db = new ConnectionHandler("$ruta_raiz");
    //$db->conn->debug=true;
    if (!defined('ADODB_FETCH_ASSOC'))	define('ADODB_FETCH_ASSOC',2);
    $ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
    // $ADODB_COUNTRECS = false;
    if(!$carpeta) $carpeta=0;
    if(!$estado_sal)   {$estado_sal=2;}
    if(!$estado_sal_max) $estado_sal_max=3;

    if($estado_sal==3) {
        $accion_sal = "Envio de Documentos";
        $pagina_sig = "cuerpoEnvioNormal.php";
        $nomcarpeta = "Radicados Para Envio";
        if(!$dep_sel) $dep_sel = $dependencia;

        $dependencia_busq1 = " and c.radi_depe_radi = $dep_sel "; //and depe_estado=1 ";
        $dependencia_busq2 = " and c.radi_depe_radi = $dep_sel "; //and depe_estado=1 ";
    }

    if ($orden_cambio==1)  {
        if (!$orderTipo)  {
            $orderTipo="desc";
        }else  {
            $orderTipo="";
        }
    }
    $encabezado = "".session_name()."=".session_id()."&krd=$krd&estado_sal=$estado_sal&estado_sal_max=$estado_sal_max&accion_sal=$accion_sal&dependencia_busq2=$dependencia_busq2&dep_sel=$dep_sel&filtroSelect=$filtroSelect&tpAnulacion=$tpAnulacion&nomcarpeta=$nomcarpeta&orderTipo=$orderTipo&orderNo=";
    $linkPagina = "$PHP_SELF?$encabezado&orderNo=$orderNo";
    $swBusqDep = "si";
    $carpeta = "nada";
    $varBuscada = "radi_nume_salida";
    //if ($busqRadicados) {//GTS
    $pagina_sig = "../envios/envia.php";
    //$pagina_sig = "../envios/envia.php";
    /*  GENERACION LISTADO DE RADICADOS
     *  Aqui utilizamos la clase adodb para generar el listado de los radicados
     *  Esta clase cuenta con una adaptacion a las clases utiilzadas de orfeo.
     *  el archivo original es adodb-pager.inc.php la modificada es adodb-paginacion.inc.php
     */

    ?>
    <div class="tabbable-custom">
        <ul class="nav nav-tabs">
            <li class="{{(isset($delivered) && count($delivered) == 0)? 'active' : ''}}">
                <a href="#tab_1" data-toggle="tab"> Expedientes CON Radicados </a>
            </li>
            <li class="{{(isset($delivered) && count($delivered) > 0)? 'active' : ''}}">
                <a href="#tab_2" data-toggle="tab" id="gestion-doc"> Expedientes SIN Radicados</a>
            </li>
        </ul>
        <div class="tab-content" style="min-height: 480px;">
            <div class="tab-pane {{(isset($delivered) && count($delivered) == 0)? 'active' : ''}}" id="tab_1">
                <div id="spiffycalendar" class="text"></div>
                <br>
                <form name=formEnviar id="formEnviar" action='' method=post>
                    <table width="100%">
                        <tr>
                            <td colspan=5 class="tablas" align="left">
                                Radicados separados por coma ','
                                <input name="busqRadicados" type="text" size="80" class="tex_area" value="<?=$busqRadicados?>">
                                <input type=button value='Buscar ' name=Buscar valign='middle' class='botones' onclick="document.getElementById('formEnviar').action='../../trd/sin_trd.php'; document.getElementById('formEnviar').target=''; document.getElementById('formEnviar').submit(); ">
                            </td>
                        </tr>
                        <tr class="titulos2">
                            <td align="center">
                                <b>Dependencia:</b>
                            </td>
                            <td colspan="3">
                                <?php
                                $queryDependencia = "SELECT (depe_codi ||' - ' || depe_nomb), depe_codi FROM dependencia WHERE depe_estado=1 ORDER BY depe_nomb ASC";
                                $rs=$db->conn->query($queryDependencia);

                                //$depe1=$_SESSION['depecodi'];
                                //$depe1="%";
                                if($_POST['depe_codi']!=""){
                                    $depe1=$_POST['depe_codi'];
                                }
                                print $rs->GetMenu2("depe_codi", $depe1, "%:-- Todas --", false,""," class='select'" );
                                ?>
                            </td>
                        </tr>
                        <tr class="titulos2">
                            <td align="center">
                                <b>Tipo de Radicado:</b>
                            </td>
                            <td>
                                <select id="tipo" name="tipo" class="select">
                                    <option value="%" selected="selected">Todos</option>
                                    <option value="1">Salida</option>
                                    <option value="2">Entrada</option>
                                    <option value="3">Interno</option>
                                </select>
                            </td>
                            <td colspan="2" align="right">
                                <?php
                                if( $_SESSION["usuaPermExpediente"] > 1 ) {
                                ?>
                                <a href='../archive/archivo.php?<?= session_name() . "=" . session_id() . "&dep_sel=$dep_sel&krd=$krd&fechah=$fechah&$orno&adodb_next_page&nomcarpeta&tipo_archivo=$tipo_archivo&carpeta'" ?>"> Menu archivo</a>
                        &nbsp;|&nbsp;
                        <?php
                                }
                                ?>
                                        <a href="../expediente/conExp.php" target="_blank"> Consulta Expedientes</a> &nbsp;|&nbsp;
                        <a href="#" onclick="modalexpediente('CREAREXPEDSINRAD')">Crear Expediente Sin Radicado</a>
                            </td>
                        </tr>
                        <tr class="titulos2">
                            <td>
                                <input type="checkbox" name="incluirTRD" id="incluirTRD" value="1" checked>
                                Documentos sin TRD
                            </td>
                            <td>
                                <input type="checkbox" name="incluirEXP" id="incluirEXP" value="1">
                                Documentos sin expediente
                            </td>
                            <td>
                                <input class="botones" type="button" valign="middle" name="Buscar" value="Buscar " onclick="document.getElementById('formEnviar').action='sin_trd.php'; document.getElementById('formEnviar').target=''; document.getElementById('formEnviar').submit(); "/>
                            </td>
                            <td align="right">
                                <!--se comento este boton ya que no se seguira usando el boton de trd en un futuro se quitara definitivamente 27/11/2020
                                <input 	type="button" value="Asignar TRD" align="right" class='botones_largo' style="width:100px"
                                          onclick="var sw=0; for(var i=1; i<document.formEnviar.elements.length;i++){ if (document.formEnviar.elements[i].checked && document.formEnviar.elements[i].name!='incluirTRD' && document.formEnviar.elements[i].name!='incluirEXP'){ sw=1; } } if (sw==0){ alert ('Debe seleccionar uno o mas radicados'); return;} document.getElementById('formEnviar').action='tipificar_varios.php'; document.getElementById('formEnviar').target='_blank'; document.getElementById('formEnviar').submit();">-->
                                <input type="button" value="Incluir en Expediente" align="right" class='botones_largo' style="width:140px"
                                       onclick="var sw=0; for(var i=1; i<document.formEnviar.elements.length;i++){ if (document.formEnviar.elements[i].checked && document.formEnviar.elements[i].name!='incluirTRD' && document.formEnviar.elements[i].name!='incluirEXP'){ sw=1; } } if (sw==0){ alert ('Debe seleccionar uno o mas radicados'); return;}document.getElementById('formEnviar').action='../expediente/incluir_varios.php'; document.getElementById('formEnviar').target='_blank'; document.getElementById('formEnviar').submit();">
                                <?php
                                if( $_SESSION["usuaPermExpediente"] > 1 ) {
                                //if( 1 == 1 ) {
                                ?>
                                <input type="button" value="Crear Expediente" align="right" class='botones_largo' style="width:140px"
                                       onclick="var sw=0; for(var i=1; i<document.formEnviar.elements.length;i++){ if (document.formEnviar.elements[i].checked && document.formEnviar.elements[i].name!='incluirTRD' && document.formEnviar.elements[i].name!='incluirEXP'){ sw=1; } } if (sw==0){ alert ('Debe seleccionar uno o mas radicados'); return;}document.getElementById('formEnviar').action='../expediente/crear_varios.php'; document.getElementById('formEnviar').target='_blank'; document.getElementById('formEnviar').submit();">
                                <?php
                                }
                                ?>
                            </td>
                        </tr>
                    </table>

                    <?

                    if($_POST['incluirTRD']=='' && $_POST['incluirEXP']==''){
                        $where=" r.TDOC_CODI<=0";
                    }else{
                        if($_POST['incluirTRD']!=''){
                            $where=" r.TDOC_CODI<=0";
                            if($_POST['incluirEXP']!=''){
                                $where.=" OR e.sgd_exp_numero is null";
                            }
                        }elseif($_POST['incluirEXP']!=''){
                            $where=" e.sgd_exp_numero is null";
                        }
                    }

                    if($_POST['depe_codi']!=''){
                        $depe_codi=$_POST['depe_codi'];
                    }else{
                        $depe_codi="0";
                        //$depe_codi=$_SESSION['dependencia'];
                    }
                    $sqlFecha = $db->conn->SQLDate("Y-m-d H:i A","r.RADI_FECH_RADI");

                    $isql='SELECT
                         distinct(r.radi_nume_radi) AS "IMG_Radicado" ,
                         r.RADI_PATH AS "HID_RADI_PATH" ,
                         (CASE WHEN(SELECT count(e2.SGD_EXP_NUMERO) FROM SGD_EXP_EXPEDIENTE e2 WHERE e2.radi_nume_radi= r.RADI_NUME_RADI AND e2.SGD_EXP_ESTADO!=2 group by e2.radi_nume_radi)>0 THEN ' . "'SI'" . ' END) AS "EXP",
                         (CASE WHEN(SELECT r2.TDOC_CODI FROM radicado r2 where r.radi_nume_radi=r2.radi_nume_radi)<>0 THEN ' . "'SI'" . ' END) AS "TRD",

                        ' . $sqlFecha . ' as "DAT_Fecha_Radicado",
                          r.RADI_NUME_RADI as "HID_RADI_NUME_RADI",

                          cast(r.radi_nume_deri as text) AS "IMG_Radicado_Asociado",
                          a.anex_nomb_archivo AS "HID_RADICADO_SALIDA" ,
                          r.ra_asun AS "Descripcion" ,
                          d.DEPE_NOMB AS "Generado_Por" ,
                          CONCAT(CONCAT(0, r.radi_depe_radi),(SELECT u2.USUA_NOMB FROM usuario u2
                                                WHERE r.radi_depe_radi=u2.depe_codi AND usua_codi=1)) AS "Radicado por",
                          CONCAT(CONCAT(0, r.radi_depe_actu), u.USUA_NOMB) AS "Usuario Actual",
                          r.radi_nume_radi AS "CHK_RADI_NUME_SALIDA"
                        FROM
                          RADICADO r
                          LEFT JOIN dependencia d ON r.RADI_DEPE_RADI=d.DEPE_CODI
                          LEFT JOIN anexos a ON a.radi_nume_salida=r.radi_nume_radi
                          LEFT JOIN sgd_exp_expediente e ON r.radi_nume_radi=e.radi_nume_radi
                          LEFT JOIN usuario u ON u.usua_codi=r.radi_usua_actu and u.depe_codi=r.RADI_DEPE_ACTU
                        WHERE ';
                    $tipo=$_POST['tipo'];
                    if($tipo==""){
                        $tipo=2;
                    }
                    if($_POST['busqRadicados']!=''){
                        $isql.=" r.RADI_NUME_RADI IN (".$_POST['busqRadicados'].") AND to_char(r.RADI_NUME_RADI,'99999999999999999999') LIKE '%".$tipo."' ";
                        $isql3="$isql";
                    }else{
                        //

                        $isql.="(RADI_DEPE_RADI = '".$depe_codi."' AND (
                              ".$where."))";

                        $isql3="$isql ";
                    }


                    ?>
                </form>
                <font size="1" color="White" ><?=$time?></font>
            </div>
            <div class="tab-pane {{(isset($delivered) && count($delivered) > 0)? 'active' : ''}}" id="tab_2">
                <br>
                <label class=" col-md-2">
                    <button type="button" class="btn red"
                            onclick="modalexpediente('CREAREXPEDSINRAD')">Crear Expediente sin Radicado
                    </button>
                </label>
            </div>
        </div>
    </div>





@endsection


@section('page-modal')
    @include('expedientes.partials.modalnuevoexpediente')
    @include('expedientes.partials.modal_addexpedientesinrad')
@endsection



@section('page-js')

    <?php

    session_start();
    $verrad = "";
    $ruta_raiz = "..";
    $dependencia=$_SESSION['dependencia'];
    $krd=$_SESSION['krd'];

    foreach ($_GET as $key => $valor)   ${$key} = $valor;
    foreach ($_POST as $key => $valor)   ${$key} = $valor;

    //echo "LoginKrd: ".$krd;
    //if (!$dependencia)   include "$ruta_raiz/rec_session.php";
    //if(!$dependencia or !$tpDepeRad) include "$ruta_raiz/rec_session.php";
    //if($_SESSION['usua_perm_envios'] !=1 ) die(include "$ruta_raiz/errorAcceso.php");
    if (!$dep_sel) $dep_sel = $dependencia;
    include_once "$ruta_raiz/js/funtionImage.php";
    function microtime_float()
    {
        list($usec, $sec) = explode(" ", microtime());
        return ((float)$usec + (float)$sec);
    }
    ?>

    <script src="../resources/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="../resources/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../resources/global/plugins/bootstrap-table/bootstrap-table.js"  type="text/javascript"></script>

    <script type="text/javascript">
        var base_url = '<?php echo $_SESSION['base_url']; ?>';
    </script>

    <script src="../resources/global/plugins/respond.min.js"></script>
    <script src="../resources/global/plugins/excanvas.min.js"></script>
    <script src="../resources/global/plugins/ie8.fix.min.js"></script>
    <![endif]-->
    <!-- BEGIN CORE PLUGINS -->
    <script src="../resources/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="../resources/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../resources/global/plugins/js.cookie.min.js" type="text/javascript"></script>
    <script src="../resources/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="../resources/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="../resources/global/plugins/bootstrap-switch/js/bootstrap-switch.js" type="text/javascript"></script>


    <!-- END CORE PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="../resources/global/scripts/app.min.js" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="../resources/pages/scripts/components-bootstrap-switch.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->

    <!-- BEGIN THEME LAYOUT SCRIPTS -->
    <script src="../resources/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
    <script src="../resources/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
    <script src="../resources/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
    <script src="../resources/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
    <script src="../resources/global/plugins/sweetalert2-9.10.2/package/dist/sweetalert2.js" type="text/javascript"></script>
    <script src="../resources/global/plugins/chosen.jquery.min.js" type="text/javascript"></script>
    <!--  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>-->
    <script src="../resources/global/plugins/select2.full.min.js" type="text/javascript"></script>
    <script src="../resources/global/plugins/editor.js" type="text/javascript"></script>

    <script src="../resources/global/plugins/tinymce/tinymce.min.js" type="text/javascript"></script>

    <script src="../resources/global/plugins/html-docx.js" type="text/javascript"></script>
    <script src="../resources/global/plugins/file-saver/FileSaver.js" type="text/javascript"></script>

    <script src="../resources/global/plugins/bootstrap-table/bootstrap-table.js" type="text/javascript"></script>

    <!--  datatable -->
    <script src="../resources/global/plugins/datatable/datatable.js" type="text/javascript"></script>
    <script src="../resources/global/plugins/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="../resources/global/plugins/datatable/datatables.bootstrap.js" type="text/javascript"></script>
    <!--  datatable -->

    <script src="../resources/pages/scripts/toastr.min.js" type="text/javascript"></script>
    <script src="../resources/pages/scripts/ui-toastr.min.js" type="text/javascript"></script>

    <script src="../resources/global/plugins/progressladabtn/spin.min.js" type="text/javascript"></script>
    <script src="../resources/global/plugins/progressladabtn/ladda.min.js" type="text/javascript"></script>
    <script src="../resources/global/plugins/progressladabtn/ui-buttons-spinners.min.js?v=<?= date('YmdHis') ?>"
            type="text/javascript"></script>

    <script src="../resources/global/plugins/typeahead/handlebars.min.js" type="text/javascript"></script>
    <script src="../resources/global/plugins/typeahead/typeahead.bundle.min.js" type="text/javascript"></script>
    <script src="../resources/global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js"
            type="text/javascript"></script>

    <script src="../resources/global/plugins/x-editable/bootstrap-editable.min.js"></script>
    <script src="../resources/global/plugins/typeahead/typeaheadjs.js"></script>

    <script src="../resources/apps/scripts/funciones_globales.js?v=<?= date('YmdHis') ?>"
            type="text/javascript"></script>

    <?php
    $permisocrear = $_SESSION["usuaPermExpediente"]; //variable que indica si tiene permiso de crear expedientes
    $fechah = date("dmy_h_m_s") . " " . time("h_m_s");
    $session_id = session_id();
    $phpsession = session_name() . "=" . trim(session_id());
    $ent = $_SESSION['entidad'];
    $dependencia = $_SESSION['dependencia'];
    $krd = $_SESSION["krd"];
    $encabezado = "$phpsession&krd=$krd&fechah=$fechah&primera=1&ent=$ent&depende=$dependencia";
    //$encabezado2="$phpsession&krd=$krd&fechah=$fechah&primera=1&ent=$ent&depende=$dependencia";
    $usua_nomb = $_SESSION["usua_nomb"];
    $depe_nomb = $_SESSION["depe_nomb"];
    $anioactual = date('Y');

    ?>

    <script>
        var correosoporte = '<?= $_SESSION['soporte_funcional'] ?>'
        var dependencia_user_log = '<?php echo $dependencia ?>'
        var usua_nomb = '<?php  echo $usua_nomb ?>';
        var depe_nomb = '<?php  echo $depe_nomb ?>';
        var anioactual = '<?php  echo $anioactual ?>';
        var usuaPermExpediente='<?= $_SESSION["usuaPermExpediente"]?>';
    </script>
    <script src="../resources/apps/scripts/nuevoExpediente.js?v=<?= date('Ymdis') ?>"
            type="text/javascript"></script>




@endsection


@section('page-css')

    <link rel="stylesheet" href="../../estilos/orfeo.css">

    <link href="../resources/global/plugins/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="../resources/global/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet"
          type="text/css"/>

    <link href="../resources/global/plugins/font-awesome/css/fonts.googleapis.css" rel="stylesheet" type="text/css" />
    <link href="../resources/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="../resources/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="../resources/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN THEME GLOBAL STYLES
    el siguiente archivo css, les quita la propiedad de borderradios a los input de form control, por eso lo comente.
    <link href="./resources/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    -->
    <link href="../resources/global/css/components-rounded.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="../resources/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="../resources/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
    <link href="../resources/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />


    <link href="../resources/pages/css/search.min.css" rel="stylesheet" type="text/css"/>
    <link href="../resources/pages/css/invoice.min.css" rel="stylesheet" type="text/css"/>
    <link href="../resources/global/plugins/typeahead/typeahead.css" rel="stylesheet" type="text/css"/>
    <link href="../resources/global/plugins/x-editable/bootstrap-editable.css" rel="stylesheet"/>
    <link href="../resources/global/plugins/typeahead/typeahead.js-bootstrap.css" rel="stylesheet" type="text/css"/>



    <link href="../resources/global/css/select2.min.css" rel="stylesheet" />
    <link href="../resources/global/css/select2-bootstrap.min.css"  rel="stylesheet"/>
    <link href="../resources/global/plugins/sweetalert2-9.10.2/package/dist/sweetalert2.min.css" rel="stylesheet" type="text/css" />
    <link href="../resources/global/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" type="text/css" href="../../js/spiffyCal/spiffyCal_v2_1.css">

    <style>
        @font-face {
            font-family: 'Glyphicons Halflings';
            src: url("../resources/global/plugins/bootstrap/fonts/bootstrap/glyphicons-halflings-regular.eot");
            src: url("../resources/global/plugins/bootstrap/fonts/bootstrap/glyphicons-halflings-regular.eot?#iefix") format("embedded-opentype"), url(../resources/global/plugins/bootstrap/fonts/bootstrap/glyphicons-halflings-regular.woff2") format("woff2"), url(../resources/global/plugins/bootstrap/fonts/bootstrap/glyphicons-halflings-regular.woff") format("woff"), url(../resources/global/plugins/bootstrap/fonts/bootstrap/glyphicons-halflings-regular.ttf") format("truetype"), url(../resources/global/plugins/bootstrap/fonts/bootstrap/glyphicons-halflings-regular.svg#glyphicons_halflingsregular") format("svg");
        }

        .glyphicon {
            position: relative;
            top: 1px;
            display: inline-block;
            font-family: 'Glyphicons Halflings';
            font-style: normal;
            font-weight: normal;
            line-height: 1;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
        }

        .glyphicon-paperclip:before {
            content: "\e142";
        }

        .borde_tab {
            border: 1px solid #377584;
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
        }


    </style>

    <style>

        .borde_tab {
            border: thin solid #377584 !important;
            border-collapse: initial !important;
            border-spacing: 2px 2px !important;
        }

        .popover, .tooltip {
            z-index: 9999999 !important;
        }

        .page-content-wrapper .page-content{
            padding: 0px !important;
        }

    </style>
@endsection