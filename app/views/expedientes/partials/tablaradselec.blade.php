<style>
    table.dataTable tbody td {
        word-break: break-word;
        vertical-align: top;
    }
</style>
<table id="tablaradselec"
       class="table table-striped table-bordered table-hover
                                                dataTable no-footer dtr-inline ">

    <thead>
    <tr>
        <th data-width="10%" data-align="center">Radicado</th>
        <th data-width="40%" data-align="center">Asunto</th>
        <th data-width="20%" data-align="center">Fecha</th>
        <th data-width="30%" data-align="center">Estado</th>
    </tr>
    </thead>
    <tbody></tbody>
</table>
