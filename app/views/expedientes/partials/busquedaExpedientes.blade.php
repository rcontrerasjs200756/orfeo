<div>
    <div>
        <div id="buttonRadExp" class="row" style="display: none;">
            <div class="col-md-9"></div>
            <div class="col-md-3">
                <button type="button" class="btn btn-lg" id="div_radicar"
                        style="border-radius:20px !important; margin-right:10px;
                        background-color: orange;float:right" onclick="formRadicar()">
                    <i class="fa fa-barcode"></i>
                    Radicar
                </button>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 search-page search-content-2"
                 id="divinputexpediente"
                 style="height:100px !important;min-height:100px !important">
                <div class="input-group search-bar bordered">
                    <input type="text"
                           style="color: #5E6373 !important;font-size: 16px !important"
                           id="inputexpediente" class="form-control typeahead"
                           title="Busca en los últimos 2 años, en el titulo, descripción y número. Clic en [Ampliar Búsqueda] para más resultados"
                           placeholder="Buscar por número de expediente o titulo en los 2 últimos años">
                    <span class="input-group-btn"
                          title="Clic para buscar en todos los campos del expediente y años.">
                        <button class="btn blue uppercase bold" type="button" id="search-expediente"
                                onclick="buscartodoTrue(event)">Buscar Más</button>
                    </span>
                </div>

            </div>

            <div class="col-md-12" id="divnohayexpedientes">
                <div class="input-group ">

                </div>

            </div>

            <div class="row" id="divbuttonbusquedaAv">
                <div class="col-md-12" style="text-align: left">
                    <div class=" col-md-12 input-group has-warning"
                         style="text-align: right;display: inline-block;"
                         id="divApendBusqAvan">


                    </div>
                </div>
            </div>

            <div class="divBusquedaAvanzada" style="display:none">

                <div class="form-group">
                    <label class=" col-md-2">Dependencia: </label>
                    <div class="col-md-10">
                        <div class="input-group input-icon right"
                             style="margin-bottom: 20px !important;">


                            <select name="selectdependencia" class="form-control"
                                    id="selectdependencia">

                            </select>

                        </div>

                    </div>
                    <label class=" col-md-2">Serie: </label>
                    <div class="col-md-10">
                        <div class="input-group input-icon right"
                             style="margin-bottom: 20px !important;">

                            <select name="serieavanzada" class="form-control"
                                    id="serieavanzada">

                            </select>

                        </div>

                    </div>
                    <label class=" col-md-2">Subserie: </label>
                    <div class="col-md-10">
                        <div class="input-group input-icon right"
                             style="margin-bottom: 20px !important;">

                            <select name="subserieavanzada" class="form-control"
                                    id="subserieavanzada">

                            </select>

                        </div>

                    </div>
                    <label class=" col-md-2">Año: </label>
                    <div class="col-md-10">
                        <div class="input-group input-icon right"
                             style="margin-bottom: 20px !important;">


                            <select name="selectanio" class="form-control"
                                    id="selectanio">

                            </select>

                        </div>

                    </div>
                    <label class=" col-md-2"><input type="hidden"></label>
                    <label class=" col-md-2">
                        <button type="button" class="btn green"
                                onclick="resultAvanzados()">Buscar
                        </button>
                    </label>
                    <label class=" col-md-2">
                        <button type="button" class="btn default"
                                onclick="limpiarBusquedaAvanz()">Limpiar
                        </button>
                    </label>
                    <label class=" col-md-2">
                        <button type="button" class="btn red"
                                onclick="createNewExp(event)">Crear Nuevo Expediente
                        </button>
                    </label>

                    <label class=" col-md-3"
                           style="float:center;text-align: center ">
                        <button type="button" class="btn default"
                                onclick="cerrarBusquedaAvanz()">Cerrar
                        </button>
                    </label>


                </div>
            </div>



        </div>


    </div>
</div>