<style>
    table.dataTable tbody td {
        word-break: break-word;
        vertical-align: top;
    }
</style>
<table id="tablaprinciexped"
       class=" dt-responsive table table-striped dataTable table-bordered table-hover table-featured no-footer">

    <thead>
    <tr>
        <th data-width="3%" data-align="center"></th>
        <th data-width="10%" data-align="center">Radicado</th>
        <th data-width="30%" data-align="center">Asunto</th>
        <th data-width="27%" data-align="center">Fecha</th>
        <th data-width="30%" data-align="center">Estado</th>
    </tr>
    </thead>
    <tbody></tbody>
</table>
