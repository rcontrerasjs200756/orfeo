<div id="modal_expediente" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog " style="width: 70%">
        <div class="modal-content">
            <div class="modal-header bg-blue-chambray bg-font-yellow text-center">
                <button type="button" title="Cerrar" onclick="cerrarModalExpediente()"
                        class="btn btn-lg "
                        style="float: right;background-color: #2C3E50!important; border-radius:20px !important">
                    <i class="fa fa-remove"></i></button>
                <h4 class="modal-title"><strong>Buscar Expedientes</strong></h4>
            </div>


            <div class="modal-body" id="radicated_info">

                <form action="" method="POST" class="form-horizonal" id="">

                    <div class="form-body modal-body todo-task-modal-body">

                        <div class="form-actions">
                            <div class="row"></div>
                        </div>

                        <div class="row" style="margin-top:40px">
                            <div class="col-md-12 ">


                                <input type="hidden"
                                       id="expedincluidcoma">

                                <div>
                                    <div>
                                        <div class="row">
                                            <div class="col-md-12 search-page search-content-2"
                                                 id="divinputexpediente"
                                                 style="height:100px !important;min-height:100px !important">
                                                <div class="input-group search-bar bordered">
                                                    <input type="text"
                                                           style="color: #5E6373 !important;font-size: 16px !important"
                                                           id="inputexpediente" class="form-control typeahead"
                                                           title="Busca en los últimos 2 años, en el titulo, descripción y número. Clic en [Ampliar Búsqueda] para más resultados"
                                                           placeholder="Buscar por número de expediente o titulo en los 2 últimos años">
                                                    <span class="input-group-btn"
                                                          title="Clic para buscar en todos los campos del expediente y años.">
                        <button class="btn blue uppercase bold" type="button" id="search-expediente"
                                onclick="buscartodoTrue(event)">Ampliar Búsqueda</button>
                    </span>
                                                </div>

                                            </div>

                                            <div class="col-md-12" id="divnohayexpedientes">
                                                <div class="input-group ">

                                                </div>

                                            </div>

                                            <div class="row" id="divbuttonbusquedaAv">
                                                <div class="col-md-12" style="text-align: left">
                                                    <div class=" col-md-12 input-group has-warning"
                                                         style="text-align: left;display: inline-block;"
                                                         id="divApendBusqAvan">


                                                    </div>
                                                </div>
                                            </div>

                                            <div class="divBusquedaAvanzada" style="display:none">

                                                <div class="form-group">
                                                    <label class=" col-md-2">Dependencia: </label>
                                                    <div class="col-md-10">
                                                        <div class="input-group input-icon right"
                                                             style="margin-bottom: 20px !important;">


                                                            <select name="selectdependencia" class="form-control"
                                                                    id="selectdependencia">

                                                            </select>

                                                        </div>

                                                    </div>
                                                    <label class=" col-md-2">Serie: </label>
                                                    <div class="col-md-10">
                                                        <div class="input-group input-icon right"
                                                             style="margin-bottom: 20px !important;">

                                                            <select name="serieavanzada" class="form-control"
                                                                    id="serieavanzada">

                                                            </select>

                                                        </div>

                                                    </div>
                                                    <label class=" col-md-2">Subserie: </label>
                                                    <div class="col-md-10">
                                                        <div class="input-group input-icon right"
                                                             style="margin-bottom: 20px !important;">

                                                            <select name="subserieavanzada" class="form-control"
                                                                    id="subserieavanzada">

                                                            </select>

                                                        </div>

                                                    </div>
                                                    <label class=" col-md-2">Año: </label>
                                                    <div class="col-md-10">
                                                        <div class="input-group input-icon right"
                                                             style="margin-bottom: 20px !important;">


                                                            <select name="selectanio" class="form-control"
                                                                    id="selectanio">

                                                            </select>

                                                        </div>

                                                    </div>
                                                    <label class=" col-md-2"><input type="hidden"></label>
                                                    <label class=" col-md-2">
                                                        <button type="button" class="btn green"
                                                                onclick="resultAvanzados()">Buscar
                                                        </button>
                                                    </label>
                                                    <label class=" col-md-2">
                                                        <button type="button" class="btn default"
                                                                onclick="limpiarBusquedaAvanz()">Limpiar
                                                        </button>
                                                    </label>
                                                    <label class=" col-md-2">
                                                        <button type="button" class="btn red"
                                                                onclick="createNewExp(event)">Crear Nuevo Expediente
                                                        </button>
                                                    </label>

                                                    <label class=" col-md-3"
                                                           style="float:center;text-align: center ">
                                                        <button type="button" class="btn default"
                                                                onclick="cerrarBusquedaAvanz()">Cerrar
                                                        </button>
                                                    </label>


                                                </div>
                                            </div>


                                            <div style="display:none" id="divinputtipologia">
                                                <div class="col-md-12 search-page search-content-2">
                                                    <div class="search-bar bordered">
                                                        <div class="input-group" style="width: 100%;"
                                                             id="divpreviotipologia">
                                                            <input type="text"
                                                                   style="color: #5E6373 !important;font-weight: 900 !important;font-size: 16px !important; width:100%;
                                                            border: 1px solid red !important;box-shadow:0 0 3px red !important;margin:10px !important"
                                                                   class="form-control typeahead"
                                                                   id="inputtipologia"
                                                                   placeholder="Elegir tipo documental">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>


                                    </div>
                                </div>

                                <div class="table-responsive  " id="open_table_expediente"
                                     style="position: relative;">
                                    <table class="table table-striped table-bordered table-hover
                                 table-checkable order-column dataTable no-footer" id="tablaexpedientes">
                                        <thead class="bg-blue font-white bold">
                                        <tr role="row">

                                            <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1"
                                                colspan="1"
                                                aria-label=" Id : activate to sort column ascending"
                                                style="width: 20%;text-align:center"> Archivar
                                            </th>

                                            <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1"
                                                colspan="1"
                                                aria-label=" Asunto : activate to sort column ascending"
                                                style="width: 40%;text-align:center"> Expediente
                                            </th>
                                            <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1"
                                                colspan="1"
                                                aria-label=" Estado : activate to sort column ascending"
                                                style="width: 40%;text-align:center">
                                                Dependencia y TRD
                                            </th>

                                        </tr>
                                        </thead>
                                        <tbody id="tbodyexpediente"></tbody>
                                    </table>
                                </div>

                            </div>

                        </div>
                    </div>


                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-lg" data-dismiss="modal"
                        style="border-radius:20px !important">
                    Cerrar
                </button>
            </div>
        </div>
    </div>
</div>