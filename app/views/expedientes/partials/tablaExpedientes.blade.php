<div class="table-responsive  " id="open_table_expediente"
     style="position: relative;">
    <table class="table table-striped table-bordered table-hover
           table-checkable order-column dataTable no-footer" id="tablaexpedientes">
        <thead class="bg-blue font-white bold">
            <tr role="row">

                <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1"
                    colspan="1"
                    aria-label=" Asunto : activate to sort column ascending"
                    style="width: 40%;text-align:center"> <?php echo $titulo ?>
                </th>

                <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1"
                    colspan="1"
                    aria-label=" Asunto : activate to sort column ascending"
                    style="width: 40%;text-align:center"> Expediente
                </th>
                <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1"
                    colspan="1"
                    aria-label=" Estado : activate to sort column ascending"
                    style="width: 40%;text-align:center">
                    Dependencia y TRD
                </th> 

            </tr>
        </thead>
        <tbody id="tbodyexpediente"></tbody>
    </table>
</div>

