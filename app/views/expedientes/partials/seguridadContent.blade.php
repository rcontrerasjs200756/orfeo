<div class="row" id="search-filter-container" style="text-align: center">
    <div class="col-md-12">
        <div class="search-bar bordered">
            <form action="#" class="form-horizontal">
                <div class="form-body">
                    <div class="form-group" id="divprevioswiche" style="height: 40px !important">

                        <input type="checkbox" id="swich_seguridad" class="make-switch "
                               data-on-text="P&uacute;blico" data-off-text="Restringir"
                               checked data-on-color="primary" data-off-color="danger"
                               >

                    </div>
                    <label class=" col-md-12" style="color:#4B77BE !important" id="labeldocpublico">
                        <span id="icCarp"></span><span id="icBloq"><i style="padding-left: 0.5em;padding-right: 0.5em;" class="fa fa-2x fa-unlock font-green"></i></span><?php echo $nivelBloqueado ?>  <span id="textoNivelBloqueo"></span></label>
                </div>
            </form>
        </div>
    </div>
</div>

<br>
<div id="listadosAutorizaciones">
<div class="row divshiden"  style="display:none">
    <div class="col-md-12">Lista de control de acceso a USUARIOS autorizados para ver este <?php echo $nivelBloqueado ?>:</div>
    <div class="col-md-12">
        <div class="input-icon right" id="div_prev_select_users">

            <select class="js-data-example-ajax" name="id_select_users[]" multiple id="id_select_users"
                    style="width: 100%"></select>
            <br>
        </div>
    </div>
</div>

<div class="row divshiden" style="display:none">
    <div class="col-md-12" style="line-height: 4pt"><p>Lista de control de acceso a DEPENDENCIAS autorizadas para ver este
            <?php echo $nivelBloqueado ?>:</p> 
        <p style="font-size: 10pt;font-style: oblique">Todos los usuarios de las dependencias elegidas
            podrán ver</p></div>
    <div class="col-md-12">
        <div class="input-icon right" id="div_prev_select_dependencias">
            <select class="js-data-example-ajax" name="select_dependencias[]" multiple
                    id="select_dependencias"
                    style="width: 100%"></select>
            <br>
        </div>
    </div>
</div>
</div>