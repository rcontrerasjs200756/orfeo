
<div id="modal_nuevoexpediente" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog " style="width:65%;">
        <div class="modal-content">
            <div class="modal-header bg-blue-chambray bg-font-yellow text-center">
                <button type="button" title="Cerrar"
                        class="btn btn-lg " onclick="closeModalNuevoExpediente(event)"
                        style="float: right;background-color: #2C3E50!important; border-radius:20px !important">
                    <i class="fa fa-remove"></i></button>
                <h4 class="modal-title"><strong id="titulomodalnuevoexped">Crear expediente sin radicado</strong></h4>

            </div>
            <div class="modal-body" id="radicated_info">

                <div class="form-actions">
                    <div class="row" style="align-content: center">
                        <label class=" col-md-3" > </label>
                        <div class="col-md-6" id="span_nro_exped"></div>
                        <label class=" col-md-3"> </label><span ></span>

                    </div>
                </div>
                <form action="" method="POST" class="form-horizonal" id="">
                    <div class="form-body modal-body todo-task-modal-body">

                        <div class="row" style="margin-top:40px">
                            <div class="col-md-12 ">
                                <div class="form-group">

                                    <label class=" col-md-6" id="dependencianueva"> </label>

                                    <label class=" col-md-2">Responsable:</label>
                                    <div class=" col-md-4" >
                                        <select name="selectresponsable" class="form-control"
                                                id="selectresponsable">

                                        </select>
                                    </div><br>

                                    <label class=" col-md-12" id="serienueva"></label><br>
                                    <label class=" col-md-12" id="subserienueva"></label><br>
                                    <label class=" col-md-12" id="anionuevo"></label> <br>
                                    <br>

                                    <label class=" col-md-12 " id="labelcargo">Título</label>
                                    <div class=" col-md-12" >
                                        <input type="text" class="form-control" min="15" max="1000" id="titulonuevoexped"
                                               onkeyup="validaTituloNuevoExped(event,this)">
                                    </div><br>

                                    <label class=" col-md-12 " id="labelcargo">Descripción</label>
                                    <div class=" col-md-12" >
                                        <textarea class="form-control" id="descripNuevoExped" ></textarea>
                                    </div><br>

                                    <label class=" col-md-12 " id="labelcargo">Otros Metadatos</label>
                                    <div class=" col-md-12" >
                                        <input type="text" class="form-control" id="otrosMetadNuevoExped">
                                    </div><br>
                                    <br>
                                    <div class="col-md-12">
                                        <div class="m-heading-1 border-yellow m-bordered">
                                            <p>Después de crear un expediente, no se puede modificar su número.<br>
                                                Gestión Documental creará una carpeta física con estos datos del expediente
                                            </p>
                                        </div>
                                    </div>


                                </div>
                            </div>

                        </div>
                    </div>


                </form>
            </div>
            <div class="modal-footer">
                <div class="form-actions">
                    <div class="row">


                        <div class="col-md-4" id="divBtnAccionCrearExped">
                            <button type="button" class="btn btn-lg red" id="btnAccionCrearExped"
                                    style="border-radius:20px !important" onclick="validarCrearExpediente(event)">
                                Crear Expediente
                            </button>
                        </div>
                        <div class="col-md-4">
                            <button type="button" class="btn btn-lg" onclick="closeModalNuevoExpediente(event)"
                                    style="border-radius:20px !important">
                                Cerrar
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>