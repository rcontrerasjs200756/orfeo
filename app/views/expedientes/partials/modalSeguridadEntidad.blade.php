<div id="modal_seguridad_exp" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="">
            <div class="modal-header bg-blue-chambray bg-font-yellow text-center">
                <button type="button" data-dismiss="modal" title="Cerrar" aria-hidden="true"
                        class="btn btn-lg "
                        style="float: right;background-color: #2C3E50!important; border-radius:20px !important">
                    <i class="fa fa-remove"></i></button>
                <h4 class="modal-title"><strong>Control de Acceso al Expediente</strong></h4>
            </div> 

            <div class="modal-body" id="body_seguridad_rad">

             
                @include('expedientes.partials.seguridadContent') 
              

            </div>

            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>

</div>

