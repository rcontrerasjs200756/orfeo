<div id="modalIncExpediente" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog " style="width: 70%">
        <div class="modal-content">
            <div class="modal-header bg-blue-chambray bg-font-yellow text-center">
                <button type="button" title="Cerrar" onclick="cerrarModalIncExpediente()"
                        class="btn btn-lg "
                        style="float: right;background-color: #2C3E50!important; border-radius:20px !important">
                    <i class="fa fa-remove"></i></button>
                <h4 class="modal-title"><strong>Incluir en Expediente</strong></h4>
            </div>


            <div class="modal-body" id="expe_inclusion_info">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-lg" data-dismiss="modal"
                        style="border-radius:20px !important">
                    Cerrar
                </button>
            </div>
        </div>
    </div>
</div>