<style>
    table.dataTable tbody td {
        word-break: break-word;
        vertical-align: top;
    }
</style>
<table id="tablaprincihistorial"
       class="table table-striped table-bordered table-hover
                                                dataTable no-footer dtr-inline ">

    <thead>
    <tr>
        <th data-width="10%" data-align="center">FECHA </th>
        <th data-width="10%" data-align="center">USUARIO </th>
        <th data-width="20%" data-align="center">DEPENDENCIA </th>
        <th data-width="20%" data-align="center">TRANSACCIÓN </th>
        <th data-width="10%" data-align="center">RADICADO </th>
        <th data-width="30%" data-align="center">OBSERVACIÓN </th>
    </tr>
    </thead>
    <tbody></tbody>
</table>
