@extends('layouts.app')

@section('content')

    <link href="{{$include_path}}pages/css/search.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{$include_path}}pages/css/invoice.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{$include_path}}global/plugins/typeahead/typeahead.css" rel="stylesheet" type="text/css"/>
    <link href="{{$include_path}}global/plugins/typeahead/typeahead.css" rel="stylesheet" type="text/css"/>

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div style="margin: 2em;" class="portlet light portlet-fit portlet-form bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-settings font-red"></i>
                        <span class="caption-subject font-red sbold uppercase">Tipos Documentales</span>
                    </div>

                </div>
                <div class="portlet-body">
                    <form action="#" id="form_sample_1" class="form-horizontal" novalidate="novalidate">
                        <div class="form-body">
                            <div class="alert alert-danger display-hide">
                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                            <div class="alert alert-success display-hide">
                                <button class="close" data-close="alert"></button> Your form validation is successful! </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Codigo
                                </label>
                                <div class="col-md-4">
                                    <input type="text" name="codigo" id="codigo"   class="form-control"> </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Nombre
                                </label>
                                <div class="col-md-4">
                                    <input name="nombre" id="nombre" type="text" class="form-control"> </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" id="buscar_tipologia" class="btn green">Buscar</button>
                                    <!--boton cancelar no es necesario POR AHORA-->
                                    <!--<button type="button" class="btn grey-salsa btn-outline">Cancel</button>-->
                                </div>
                            </div>
                        </div>
                    </form>


                </div></div></div></div>

    @include('expedientes.partials.tablaConsultaTipologia' )






                @section('page-js')
                    <script src="{{$include_path}}global/plugins/bootstrap-table/bootstrap-table.min.js"
                            type="text/javascript"></script>
                    <script src="{{$include_path}}global/plugins/bootstrap-table/locale/bootstrap-table-es-SP.js"
                            type="text/javascript"></script>
                    <script src="{{$include_path}}global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"
                            type="text/javascript"></script>
                    <script src="{{$include_path}}global/plugins/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.es.js"
                            type="text/javascript"></script>
                    <script src="{{$include_path}}global/plugins/moment.min.js" type="text/javascript"></script>

                    <!--  datatable -->
                    <script src="{{$include_path}}global/plugins/datatable/datatable.js" type="text/javascript"></script>
                    <script src="{{$include_path}}global/plugins/datatable/datatables.min.js" type="text/javascript"></script>
                    <script src="{{$include_path}}global/plugins/datatable/datatables.bootstrap.js" type="text/javascript"></script>
                    <!--  datatable -->

                    <script src="{{$include_path}}global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js"
                            type="text/javascript"></script>

                    <script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.ui.widget.js"></script>
                    <!-- The Templates plugin is included to render the upload/download listings -->
                    <script src="{{$include_path}}global/plugins/blueimp/js/tmpl.min.js"></script>
                    <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
                    <script src="{{$include_path}}global/plugins/blueimp/js/load-image.all.min.js"></script>
                    <!-- The Canvas to Blob plugin is included for image resizing functionality -->
                    <script src="{{$include_path}}global/plugins/blueimp/js/canvas-to-blob.min.js"></script>

                    <!-- PROBANDO SIN ESTO Bootstrap JS is not required, but included for the responsive demo navigation
                    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>-->

                    <!-- blueimp Gallery script -->
                    <script src="{{$include_path}}global/plugins/blueimp/js/jquery.blueimp-gallery.min.js"></script>

                    <script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.iframe-transport.js"
                            type="text/javascript"></script>
                    <script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.fileupload.js" type="text/javascript"></script>
                    <script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.fileupload-process.js"
                            type="text/javascript"></script>
                    <script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.fileupload-image.js"
                            type="text/javascript"></script>
                    <script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.fileupload-audio.js"
                            type="text/javascript"></script>

                    <script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.fileupload-video.js"
                            type="text/javascript"></script>
                    <script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.fileupload-validate.js"
                            type="text/javascript"></script>
                    <script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.fileupload-ui.js"
                            type="text/javascript"></script>
                    <script src="{{$include_path}}global/plugins/jquery-upload/js/form-fileupload.min.js"
                            type="text/javascript"></script>


                    <script src="{{$include_path}}global/plugins/typeahead/typeahead.bundle.min.js"></script>
                    <script src="{{$include_path}}global/plugins/x-editable/bootstrap-editable.min.js"></script>
                    <script src="{{$include_path}}global/plugins/typeahead/typeaheadjs.js"></script>




                    <script>
                    var tablalistaTipologias = null;
                    var htmlTabla = '';
                    function addFilaTipologia(ind,item){
                        htmlTabla = htmlTabla + '<tr><td>'+ item.sgd_tpr_codigo +'</td><td>'+  item.sgd_tpr_descrip +'</td><td>' +item.sgd_tpr_termino + '</td></tr>';
                    }


                    function consultarServicioTipologia(){
                    var parametros = '';
                    if ($('#codigo').val() != '' ){
                        parametros = (parametros != '') ? parametros+'&' : '';
                        parametros = parametros + 'codigo_tipologia='+$('#codigo').val();
                    }

                    if ($('#nombre').val() != '' ){
                        parametros = (parametros != '') ? parametros+'&' : '';
                        parametros = parametros + 'nombre_tipologia='+$('#nombre').val();
                    }



                        $.ajax({
                            url: WS_APPSERVICE_URL + '/integration/expedientes/tipologia/consulta?' + parametros,
                            headers: {
                                "Authorization": 'Bearer '+configsHasValue['WS_APPSERVICE_TOKEN']
                            },
                            method: "GET",
                            dataType: "json",
                            async: false,
                            success: function (response) {

                                htmlTabla = '';
                                var tipologias = response.Tipologias;
                                if (tipologias.length > 0) {
                                    $.each(tipologias, function (index, elem) {
                                        addFilaTipologia(index, elem);
                                    });


                                    $('#tbodytipologia').html('');
                                    $('#tbodytipologia').html(htmlTabla);
                                }else{
                                    tablalistaTipologias.destroy();
                                    definirTablaTipologia();
                                }



                            }

                        }).done(function () {

                        }).fail(function (error) {
                            Swal.fire("Error", "Ocurrió un error al validar si está incluido al borrador", "error");
                        });

                    }

                    $(document).ready(function () {
                        definirTablaTipologia();


                        $('#buscar_tipologia').click( function (event){
                            event.preventDefault();
                            consultarServicioTipologia();
                        });



                    });
                    </script>


@endsection
