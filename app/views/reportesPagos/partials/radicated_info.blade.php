<div class="row">
    <div class="col-md-6">
        <h4><strong>Número: </strong>{{$rad_info["RADICADO"]}}</h4>
    </div>
    <div class="col-md-6">
        <h4><strong>Fecha de Radicado: </strong>{{\Carbon\Carbon::parse($rad_info["FECHA_RADICADO"])->format('d-m-Y')}}</h4>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <h4 class="bold">Asunto</h4>
        <p>
            {{$rad_info["ASUNTO"]}}
        </p>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <h4><strong>Usuario Actual: </strong>{{$rad_info["USUARIO_ACTUAL"]}}</h4>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <h4><strong>Dependencia Actual: </strong>{{$rad_info["RADI_DEPE_ACTU"].' - '.$rad_info["DEPE_NOMB"]}}</h4>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <h4 class="bold">
            Radicado Escaneado?:
            @if(\App\Helpers\ReportesPagos::get_scanned($rad_info["RADICADO"]) > 0)
                <i class="fa fa-check-circle font-green-dark bold"></i>
            @else
                <i class="fa fa-times-circle font-red-thunderbird bold"></i>
            @endif
        </h4>
    </div>
</div>