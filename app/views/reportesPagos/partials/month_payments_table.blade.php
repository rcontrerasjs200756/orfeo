
<table id="month_payment_table"
       data-toggle="table"
       data-striped="true"
       data-pagination="true"
       data-page-size="10"
       data-search="true"
       data-locale="es-SP">
    <thead>
    <tr>
        <th data-width="15%" data-align="center">Radicado</th>
        <th data-width="15%" data-align="center">Fecha Radicado</th>
        <th data-width="40%" data-align="center">Asunto</th>
        <th data-width="10%" data-align="center">Dependencia Actual</th>
        <th data-width="15%" data-align="center">Usuaro Actual</th>
        <th data-width="5%" data-align="center">Opciones</th>
    </tr>
    </thead>
    <tbody>
    @foreach($month_payments as $key => $row)
        <tr>
            <td>
                @if(strlen($row["RADI_PATH"]) > 0 && file_exists($_SESSION['base_url'].'bodega/'.$row["RADI_PATH"]))
                    <a href="<?= $_SESSION['base_url']?>bodega/{{$row["RADI_PATH"]}}" target="_blank" >{{$row["RADICADO"]}}</a>
                @else
                    {{$row["RADICADO"]}}
                @endif
            </td>
            <td>{{\Carbon\Carbon::parse($row["FECHA_RADICADO"])->format('Y-m-d')}}</td>
            <td>
                <a href="../../verradicado.php?verrad={{$row["RADICADO"]}}&<?=$session_name?>​=<?=$session_id?>​&menu_ver_tmp=3" target="_blank">{{$row["ASUNTO"]}}</a>
            </td>

            <td>{{\App\Helpers\ReportesPagos::get_subject($row["RADI_DEPE_ACTU"], $row["DEPE_NOMB"])}}</td>
            <td>{{$row["USUARIO_ACTUAL"]}}</td>
            <td>
                <a class="btn btn-circle btn-icon-only blue-steel get_info" title="Detalles" data-rad-num="{{$row["RADICADO"]}}">
                    <i class="fa fa-info-circle bold"></i>
                </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>