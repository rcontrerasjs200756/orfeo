
<style >

    .resultA {
        background: #eee;
        padding: 3px 30px;
        max-height: 400px;
        overflow-y: scroll;
        margin-bottom: 20px;
        cursor: pointer;
    }
    table.dataTable tbody td {
        word-break: break-word;
        vertical-align: top;
    }
</style>
<table id="tableop"
        class="table table-striped dataTable table-bordered                                              table-hover table-featured no-footer">

    <thead>
    <tr>
        <th data-width="3%" data-align="center"></th>
        <th data-width="10%" data-align="center">Radicado</th>
        <th data-width="20%" data-align="center">Asunto</th>
        <th data-width="40%" data-align="center">Contratista</th>
        <th data-width="20%" data-align="center">Usuario Actual</th>
        <th data-width="7%" data-align="center">Opciones</th>
    </tr>
    </thead>
    <tbody></tbody>
</table>

<div class="resultA" style="display:none"></div>