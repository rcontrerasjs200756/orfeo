<div id="modal_reasignar" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog "  style="width:60% !important;">
        <div class="modal-content">
            <div class="modal-header bg-blue-chambray bg-font-yellow text-center">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><strong>REASIGNAR</strong></h4>
            </div>
            <div class="modal-body" id="radicated_info">
                <form action="" method="POST" class="form-horizonal" id="">
                    <div class="form-body modal-body todo-task-modal-body">
                        <div class="form-actions">
                            <div class="row"></div>
                        </div>
                        <div class="row" style="margin-top:40px">
                            <div class="col-md-12 ">
                                <div class="form-group has-error">
                                    <label class=" col-md-2">Reasignar a: </label>
                                    <div class="col-md-10">
                                        <div class="input-icon right">

                                            <select class="js-data-example-ajax" id="enviara" name="">

                                            </select>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 " style="margin-top:10px">
                                <div class="form-group has-error">
                                    <label class=" col-md-2">Comentario: </label>
                                    <div class="col-md-10">
                                        <div class="input-icon right">
                                                <textarea required="required" name="comentarioenviar"
                                                          id="comentarioenviar" maxlength="999" class="form-control"
                                                          style="border: solid grey 1px; height:90px"
                                                          placeholder=""></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>


                </form>
            </div>
            <div class="modal-footer">
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-4">
                            <button type="button" class="btn btn-lg blue"
                                    id="guardarEnviar" onclick="OrdenesDePago.clickModalReasignar(event)"
                                    style="border-radius:20px !important">
                                <i class="fa fa-arrow-right"></i>
                                Reasignar
                            </button>

                        </div>

                        <div class="col-md-4">
                            <button type="button" class="btn btn-lg red" data-dismiss="modal"
                                    style="border-radius:20px !important">
                                Cancelar
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>