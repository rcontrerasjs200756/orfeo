<table id="returned_payment_table"
       data-toggle="table"
       data-striped="true"
       data-pagination="true"
       data-page-size="10"
       data-search="true"
       data-locale="es-SP"
       data-detail-view="true"
       data-detail-formatter="detailReturned">
    <thead>
    <tr>
        <th data-width="15%" data-align="center">Radicado</th>
        <th data-width="15%" data-align="center">Fecha Radicado</th>
        <th data-width="40%" data-align="center">Asunto</th>
        <th data-width="10%" data-align="center">Devolución</th>
        <th data-width="20%" data-align="center">Fecha Devolución</th>
        <th data-visible="false">Comentario</th>
        <th data-visible="false">Desde</th>
        <th data-visible="false">Para</th>
        <th data-visible="false">Asunto</th>
        <th data-visible="false">Usuario Actual</th>
    </tr>
    </thead>
    <tbody>
    @foreach($returned_payments as $key => $row)
        <tr>
            <td>
                @if(strlen($row["RADI_PATH"]) > 0 && file_exists($_SESSION['base_url'].'bodega/'.$row["RADI_PATH"]))
                    <a href="<?= $_SESSION['base_url']?>bodega/{{$row["RADI_PATH"]}}" target="_blank" >{{$row["RADICADO"]}}</a>
                @else
                    {{$row["RADICADO"]}}
                @endif
            </td>
            <td>{{\Carbon\Carbon::parse($row["FECHA_RADICADO"])->format('Y-m-d')}}</td>
            <td>
                <a href="../../verradicado.php?verrad={{$row["RADICADO"]}}&<?=$session_name?>​​=<?=$session_id?>​&menu_ver_tmp=3"
                   target="_blank">{{$row["ASUNTO"]}}</a>
            </td>
            <td>{{\App\Helpers\ReportesPagos::get_transaction_label($row["TRANSACCION"])}}</td>
            <td>
                {{\Carbon\Carbon::parse($row["FECHA_TRANSACCION"])->format('Y/m/d H:i')}}
            </td>
            <td>{{$row["HISTORICO_COMENTARIO"]}}</td>
            <td>{{$row["DESDE"]}}</td>
            <td>{{$row["PARA"]}}</td>
            <td>{{\App\Helpers\ReportesPagos::get_subject($row["RADI_DEPE_ACTU"], $row["DEPE_NOMB"])}}</td>
            <td>{{$row["USUARIO_ACTUAL"]}}</td>
        </tr>
    @endforeach
    </tbody>
</table>