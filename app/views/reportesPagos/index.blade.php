@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN TAB PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title tabbable-line">

                    <ul class="nav nav-tabs" style="float:left">

                        <li id="tab_ordenpago" class="active">
                            <a href="#ordenes_pago" data-toggle="tab" class="font-green  bold">Ordenes de Pago - OP </a>

                        </li>

                        <li class="">
                            <a href="#returned_payment" data-toggle="tab" class="font-green  bold"> Pagos Devueltos </a>
                        </li>
                        <li>
                            <a href="#month_payment" data-toggle="tab" class="font-green bold"> Pagos del Mes </a>
                        </li>
                    </ul>

                    <div class="caption" style="float:right">
                        <i class="icon-doc font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Reportes de Pagos</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="tab-content">


                        <div class="tab-pane active" id="ordenes_pago">
                            <div class="row">
                                <div class="col-md-3">
                                    <a class="btn btn-sm blue-steel" id="rangofechaop">
                                        <i class="fa fa-filter"></i> Rango de Fecha
                                    </a>
                                </div>

                                <div class="col-md-2">
                                    <div class="margin-bottom-10" data-toggle="tooltip"
                                         title="Subir órdenes de pago a mi cargo">


                                        <input id="Financiera" type="radio"
                                               name="radio1" value="Financiera" class="switch-radio1"
                                        <?= $swichedefault == "Financiera" ? "selected" : "" ?>
                                        >
                                        <br>
                                        <label for="Financiera">Financiera</label>

                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="margin-bottom-10" data-toggle="tooltip"
                                         title="Ver mis contratistas y descargar órden de pago">

                                        <input id="Supervisor" type="radio" name="radio1" value="Supervisor"
                                               class="switch-radio1"
                                        <?= $swichedefault == "Supervisor" ? "selected" : "" ?>>

                                        <br>
                                        <label for="Supervisor">Supervisor</label>

                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="margin-bottom-10" data-toggle="tooltip"
                                         title="Ver mis informes y órdenes de pago">

                                        <input id="Contratista" type="radio"
                                               name="radio1" value="Contratista"
                                               class="switch-radio1"
                                        <?= $swichedefault == "Contratista" ? "selected" : "" ?> >
                                        <br>
                                        <label for="Contratista">Contratista</label>

                                    </div>

                                </div>

                                <div class="col-md-2">
                                    <div class="margin-bottom-10" data-toggle="tooltip"
                                         title="Asignar supervisor a contratistas">

                                        <button id="Administracion"
                                                name="btnadministracion"
                                                class="btn green" onclick="tablaAdmin();"
                                        >
                                            <span aria-hidden="true" class="icon-settings"></span>
                                            <span aria-hidden="true" class="icon-user"></span></button>
                                        <br>

                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <form action="#" id="formfechaop"
                                          class="form-horizontal bordered margin-top-10 well hide">
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Desde:</label>
                                                <div class="col-md-10">
                                                    <div class="input-group date returned-from">
                                                        <input type="text" name="datefromop" id="datefromop" size="10"
                                                               readonly class="form-control"
                                                               value="<?= date("d-m-Y H:i:s", strtotime(date('01-m-Y') . "00:00:00")) ?>">
                                                        <span class="input-group-btn">
                                            <button class="btn default date-set" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Hasta:</label>
                                                <div class="col-md-10">
                                                    <div class="input-group date returned-to">
                                                        <input type="text" name="datetoop" id="datetoop" size="10"
                                                               readonly class="form-control"
                                                               value="<?= date('d-m-Y H:i:s') ?>">
                                                        <span class="input-group-btn">
                                            <button class="btn default date-set" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-12 text-center">
                                                    <button type="submit" class="btn green">Enviar</button>
                                                    <button type="reset" class="btn default">Limpiar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>


                            <div id="loader_tableopcontainer" class="hide">
                                <div class="row">
                                    <div class="col-md-12 text-center margin-top-40">
                                        <i class="fa fa-spinner fa-pulse fa-3x fa-fw font-blue-steel"></i>
                                        <span class="sr-only">Loading...</span>
                                    </div>
                                </div>
                            </div>

                            <div id="tableopcontainer" class=" " >
                                @include('reportesPagos.partials.tablaprincipalop')
                            </div>
                        </div>

                        <!--  segunda pestana  --->
                        <div class="tab-pane" id="returned_payment">
                            <div class="row">
                                <div class="col-md-3">
                                    <a class="btn btn-sm blue-steel" id="returned-filter-toggle">
                                        <i class="fa fa-filter"></i> Filtros de Fecha
                                    </a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <form action="#" id="returned-date-form"
                                          class="form-horizontal bordered margin-top-10 well hide">
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Desde:</label>
                                                <div class="col-md-10">
                                                    <div class="input-group date returned-from">
                                                        <input type="text" name="returned_from" id="returned-from"
                                                               size="10" readonly class="form-control" value="">
                                                        <span class="input-group-btn">
                                            <button class="btn default date-set" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Hasta:</label>
                                                <div class="col-md-10">
                                                    <div class="input-group date returned-to">
                                                        <input type="text" name="returned_to" id="returned-to" size="10"
                                                               readonly class="form-control" value="">
                                                        <span class="input-group-btn">
                                            <button class="btn default date-set" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-12 text-center">
                                                    <button type="submit" class="btn green">Enviar</button>
                                                    <button type="reset" class="btn default">Limpiar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div id="loader_returned_container" class="hide">
                                <div class="row">
                                    <div class="col-md-12 text-center margin-top-40">
                                        <i class="fa fa-spinner fa-pulse fa-3x fa-fw font-blue-steel"></i>
                                        <span class="sr-only">Loading...</span>
                                    </div>
                                </div>
                            </div>
                            <div id="returned_table_container">
                                @include('reportesPagos.partials.returned_payments_table')
                            </div>
                        </div>
                        <div class="tab-pane" id="month_payment">
                            <div class="row">
                                <div class="col-md-3">
                                    <a class="btn btn-sm blue-steel" id="month-filter-toggle">
                                        <i class="fa fa-filter"></i> Filtros de Fecha
                                    </a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <form action="#" id="month-date-form"
                                          class="form-horizontal bordered margin-top-10 well hide">
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Desde:</label>
                                                <div class="col-md-10">
                                                    <div class="input-group date month-from">
                                                        <input type="text" name="month_from" id="month-from" size="10"
                                                               readonly class="form-control" value="">
                                                        <span class="input-group-btn">
                                            <button class="btn default date-set" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Hasta:</label>
                                                <div class="col-md-10">
                                                    <div class="input-group date month-to">
                                                        <input type="text" name="month_to" id="month-to" size="10"
                                                               readonly class="form-control" value="">
                                                        <span class="input-group-btn">
                                            <button class="btn default date-set" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-12 text-center">
                                                    <button type="submit" class="btn green">Enviar</button>
                                                    <button type="reset" class="btn default">Limpiar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div id="loader_month_container" class="hide">
                                <div class="row">
                                    <div class="col-md-12 text-center margin-top-40">
                                        <i class="fa fa-spinner fa-pulse fa-3x fa-fw font-blue-steel"></i>
                                        <span class="sr-only">Loading...</span>
                                    </div>
                                </div>
                            </div>

                            <div id="month_table_container" class="table-responsive">
                                @include('reportesPagos.partials.month_payments_table')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END TAB PORTLET-->
        </div>
    </div>
@endsection


@section('page-modal')

    @include('expedientes.partials.modalnuevoexpediente')
    @include('expedientes.partials.modal_addexpedienteconrad')


    <div id="modal_vista_previa" class="modal fade" tabindex="-1" aria-hidden="true" style="z-index: 9999999">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-blue-chambray bg-font-yellow text-center" id="header_vista_previa">

                    <button type="button" title="Cerrar" aria-hidden="true" class="btn btn-lg " data-dismiss="modal"
                            style="float: right;background-color: #2C3E50!important; border-radius:20px !important"
                    >
                        <i class="fa fa-remove"></i></button>
                    <h4 class="modal-title"><strong>Vista Previa</strong></h4>
                </div>
                <div class="modal-body" id="modal_body_vista_previa">

                    <embed src="" id="embed"
                           frameborder="0" style="width:100%; height:100%">
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-lg" data-dismiss="modal"
                            style="border-radius:20px !important">
                        Cerrar
                    </button>
                </div>
            </div>
        </div>
    </div>


    <div id="modal_vista_previa_validar" class="modal fade" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-blue-chambray bg-font-yellow text-center" id="header_vista_previa_validar">

                    <button type="button" title="Cerrar" aria-hidden="true" class="btn btn-lg " data-dismiss="modal"
                            style="float: right;background-color: #2C3E50!important; border-radius:20px !important"
                    >
                        <i class="fa fa-remove"></i></button>
                    <h4 class="modal-title"><strong>Vista Previa</strong></h4>
                </div>
                <div class="modal-body" id="modal_body_vista_previa_validar">

                    <embed src="" id="embed_validar"
                           frameborder="0" style="width:100%; height:100%">
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-lg btn-success" id="btn_descargar_pdf"
                            style="border-radius:20px !important">
                        Descargar
                    </button>

                    <button type="button" class="btn btn-lg" data-dismiss="modal"
                            style="border-radius:20px !important">
                        Cerrar
                    </button>
                </div>
            </div>
        </div>
    </div>


    <div id="modal_reasignar" class="modal fade" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog "  style="width:60% !important;">
            <div class="modal-content">
                <div class="modal-header bg-blue-chambray bg-font-yellow text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><strong>REASIGNAR</strong></h4>
                </div>
                <div class="modal-body" id="radicated_info">
                    <form action="" method="POST" class="form-horizonal" id="">
                        <div class="form-body modal-body todo-task-modal-body">
                            <div class="form-actions">
                                <div class="row"></div>
                            </div>
                            <div class="row" style="margin-top:40px">
                                <div class="col-md-12 ">
                                    <div class="form-group has-error">
                                        <label class=" col-md-2">Reasignar a: </label>
                                        <div class="col-md-10">
                                            <div class="input-icon right">

                                                <select class="js-data-example-ajax" id="enviara" name="">

                                                </select>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 " style="margin-top:10px">
                                    <div class="form-group has-error">
                                        <label class=" col-md-2">Comentario: </label>
                                        <div class="col-md-10">
                                            <div class="input-icon right">
                                                <textarea required="required" name="comentarioenviar"
                                                          id="comentarioenviar" maxlength="999" class="form-control"
                                                          style="border: solid grey 1px; height:90px"
                                                          placeholder=""></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>


                    </form>
                </div>
                <div class="modal-footer">
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-4">
                                <button type="button" class="btn btn-lg blue"
                                        id="guardarEnviar" onclick="OrdenesDePago.clickModalReasignar(event)"
                                        style="border-radius:20px !important">
                                    <i class="fa fa-arrow-right"></i>
                                    Reasignar
                                </button>

                            </div>

                            <div class="col-md-4">
                                <button type="button" class="btn btn-lg red" data-dismiss="modal"
                                        style="border-radius:20px !important">
                                    Cancelar
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="modCargosFirmValid" class="modal fade" aria-hidden="true">
        <div class="modal-dialog ">
            <div class="modal-content modal-lg">
                <div class="modal-header bg-blue-chambray bg-font-yellow text-center">
                    <button type="button" class="close"
                            onclick="cerrarModalFirmantes('VALIDAR','modCargosFirmValid');"></button>
                    <h4 class="modal-title"><strong>Escriba el cargo y seleccione el firmante principal</strong></h4>
                </div>
                <div class="modal-body">

                    <br>

                    <table role="presentation" class="table table-bordered table-striped" border="1">
                        <thead>
                        <th>Nombre</th>
                        <th>Cargo</th>
                        <th>Remitente Principal</th>
                        <th>Firmantes</th>
                        </thead>
                        <tbody id="tbodyCargFirmValid"></tbody>
                    </table>

                </div>
                <div class="modal-footer">
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-6">
                                <button type="button" class="btn btn-lg red"
                                        style="border-radius:20px !important"
                                        onclick="cerrarModalFirmantes('VALIDAR','modCargosFirmValid');">
                                    Guardar y cerrar
                                </button>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $conjugacionValidar = json_decode($_SESSION['NOMBRE_BOTON_VALIDAR_CONJUGADO'], true);  ?>
    <div id="modal_aprobar_documento" class="modal fade" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog " style="width:63%;">
            <div class="modal-content">

                <div class="modal-header bg-blue-chambray bg-font-yellow text-center">

                    <button type="button" title="Cerrar" aria-hidden="true"
                            class="btn btn-lg "
                            style="float: right;background-color: #2C3E50!important; border-radius:20px !important"
                            onclick="cerrarmodalvalidar()">
                        <i class="fa fa-remove"></i></button>

                    <h4 class="modal-title"><strong
                                id="titulomodal"><?= strtoupper($conjugacionValidar[10]) ?> DOCUMENTO</strong>
                    </h4>
                </div>
                <div class="modal-body" id="modalbodyaprobar_documento">


                    <div class="col-md-12 " style="float: left">
                        <div class="note note-info" style="padding: 2px 30px 15px 15px !important;">
                            <h4 class="block"><?= $conjugacionValidar[10] ?>!</h4>
                            <p><?= $_SESSION["NOTA_VALIDAR"] ?> </p>
                        </div>

                    </div>


                    <div id="div_apend_aprob_doc">

                    </div>

                    <div class="progress progress-striped active" id="myProgress" style="display: none">
                        <div class="progress-bar progress-bar-success" role="progressbar" id="myBar"
                             aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 1%">
                            <span class="sr-only" id="label"> </span>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div id="modalListaContratista" class="modal fade" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog " style="width:80% !important;">
            <div class="modal-content">
                <div class="modal-header bg-blue-chambray bg-font-yellow text-center">

                    <button type="button" title="Cerrar" aria-hidden="true" class="btn btn-lg "
                            style="float: right;background-color: #2C3E50!important; border-radius:20px !important"
                            onclick="cerrarMdalListaContratista()">
                        <i class="fa fa-remove"></i></button>
                    <h4 class="modal-title"><strong>Administraci&oacute;n</strong></h4>
                </div>
                <div class="modal-body" id="">

                    <div id="" class="row">


                        <div class="col-md-4">
                            <button type="button" id="div_enviar" class="btn btn-lg blue"
                                    style="border-radius:20px !important;"
                                    onclick="modalAsignaSuper()">

                                Asignar Supervisores
                                <i class="fa fa-user"></i>
                            </button>
                        </div>


                        <div class="col-md-3 text-center">
                            <span id="nro_contra_selected">(0 contratistas seleccionados)</span>
                        </div>
                        <?php

                        if($_SESSION['usua_admin_sistema'] == 0 || $_SESSION['usua_admin_sistema'] == null
                        || $_SESSION['usua_admin_sistema'] == ""){ ?>
                        <div class="col-md-2 text-center margin-top-40"
                             data-toggle="tooltip"
                             title="Mostrar todos los contratistas">
                            <input id="radiotodos" type="checkbox" data-on-text="Si" data-off-text="No"
                                   name="radiotodos" value="Mostrar todos los contratistas"
                                   class="">
                            <br>
                            <label for="option2">Todos</label>

                        </div>
                        <?php  } ?>
                    </div>

                    <div id="tableAdminContainer">
                        @include('reportesPagos.partials.tablaprincipalop')
                    </div>

                </div>
                <div class="modal-footer" style="">
                    <div class="form-actions" style="text-align: right; float:right">
                        <div class="col-md-2">
                            <button type="button" class="btn btn-lg btn-outline" data-dismiss="modal"
                                    style="border-radius:20px !important">
                                Cerrar
                            </button>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal_asignarsuper" class="modal fade" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog " style="width:60% !important;">
            <div class="modal-content">
                <div class="modal-header bg-blue-chambray bg-font-yellow text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><strong>Asignar Supervisores</strong></h4>
                </div>
                <div class="modal-body" id="radicated_info">
                    <form action="" method="POST" class="form-horizonal" id="">
                        <div class="form-body modal-body todo-task-modal-body">
                            <div class="form-actions">
                                <div class="row"></div>
                            </div>
                            <div class="row" style="margin-top:30px" id="div_msj_no_admin" style="display:none">
                                <div class="col-md-12 ">
                                    <label class=" col-md-6" id="labelmodalasignsuperv"></label>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 text-center">
                                    <span id="contra_selected_2">(0 contratistas seleccionados)</span>
                                </div>
                            </div>
                            <br>
                            <div class="row">

                                <div class="col-md-12 ">
                                    <div class="form-group has-error">
                                        <label class=" col-md-3">Supervisor Principal:</label>
                                        <div class="col-md-9">
                                            <div class="input-icon right">

                                                <select class="js-data-example-ajax" id="superprinci" name="">

                                                </select>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 ">
                                    <div class="form-group has-error">
                                        <label class=" col-md-3">Supervisor de apoyo:</label>
                                        <div class="col-md-9">
                                            <div class="input-icon right">

                                                <select class="js-data-example-ajax" id="superapoyo" name="">

                                                </select>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </form>
                </div>
                <div class="modal-footer">
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-4">
                                <button type="button" class="btn btn-lg btn-primary"
                                        id="guardarEnviar" onclick="guardarSupervisores(event)"
                                        style="border-radius:20px !important">
                                    Guardar
                                </button>

                            </div>

                            <div class="col-md-4">
                                <button type="button" class="btn btn-lg btn-outline" data-dismiss="modal"
                                        style="border-radius:20px !important">
                                    Cerrar
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modalsubirop" class="modal fade" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog " style="width:80% !important;">
            <div class="modal-content ">
                <div class="modal-header bg-blue-chambray bg-font-yellow text-center">

                    <button type="button" title="Cerrar" aria-hidden="true" class="btn btn-lg "
                            style="float: right;background-color: #2C3E50!important; border-radius:20px !important"
                            onclick="cerrarModalSubirOp()">
                        <i class="fa fa-remove"></i></button>
                    <h4 class="modal-title"><strong>SUBIR OP</strong></h4>
                </div>
                <div class="modal-body" id="radicated_info">

                    <div class="form-body modal-body todo-task-modal-body">

                        <div class="row" >
                        <div class="col-md-12 " >
                            <div class="form-group has-error">
                                <label class="col-md-2">Radicado #:</label>
                                <div class="col-md-3"><strong id="mostrarradicadoanexo"> </strong></div>
                            </div>
                        </div>
                            </div>
                        <div class="row" >
                        <div class="col-md-12 " >
                            <div class="form-group has-error">
                                <label class="col-md-2">Asunto</label>
                                <div class="col-md-10"><span id="mostrarasuntoanexo"> </span></div>
                            </div>
                        </div>
                        </div>
                        <div class="row" >
                        <div class="col-md-12 " style="margin-top:10px">
                            <div class="form-group has-error">
                                <input type="hidden" name="numeroradicado" id="numeroradicado">
                                <label class="col-md-3">Ingrese número de OP:</label>
                                <div class="col-md-4">
                                    <div class="input-icon right">
                                        <input type="text"
                                               name="numeroop"
                                               id="numeroop"
                                               class="form-control" placeholder="Ingrese número de OP">
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>

                    </div>
                    <br>
                    <div class="form-body modal-body todo-task-modal-body">
                    <div class="row" >
                    <div id="appendformanexo"></div>
                    <form id="formanexo" action="https://jquery-file-upload.appspot.com/" method="POST"
                          enctype="multipart/form-data">
                        <!-- Redirect browsers with JavaScript disabled to the origin page -->
                        <noscript><input type="hidden" name="redirect"
                                         value="https://blueimp.github.io/jQuery-File-Upload/"></noscript>
                        <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
                        <div class="row fileupload-buttonbar">
                            <div class="col-lg-7">
                                <!-- The fileinput-button span is used to style the file input field as button -->


                                <!-- The fileinput-button span is used to style the file input field as button -->
                                <span class="btn green fileinput-button">
                                                <i class="fa fa-plus"></i>
                                                <span>Agregar archivos</span>
                                                <input type="file" name="files[]" multiple=""
                                                       style="position: absolute;
    top: 0;
    left: 0;
    margin: 0;
    opacity: 0;
    -ms-filter: 'alpha(opacity=0)';
    font-size: 5px;
    direction: ltr;
    cursor: pointer;
height: 30px;"> </span>

                                <input type="hidden" name="id_borrador_anexo" id="id_borrador_anexo">


                                <button type="submit" class="btn btn-primary start">
                                    <i class="glyphicon glyphicon-upload"></i>
                                    <span>Subir todos</span>
                                </button>
                                <button type="reset" class="btn btn-warning cancel">
                                    <i class="glyphicon glyphicon-ban-circle"></i>
                                    <span>Cancelar todo</span>
                                </button>

                                <!-- The global file processing state -->
                                <span class="fileupload-process"></span>
                            </div>
                            <!-- The global progress state -->
                            <div class="col-lg-5 fileupload-progress fade">
                                <!-- The global progress bar -->
                                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0"
                                     aria-valuemax="100">
                                    <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                                </div>
                                <!-- The extended global progress state -->
                                <div class="progress-extended">&nbsp;</div>
                            </div>
                        </div>
                        <!-- The table listing the files available for upload/download -->
                        <table role="presentation" class="table table-striped" id="tablaanexosop">
                            <tbody class="files"></tbody>
                        </table>
                    </form>
                    </div>
                    </div>
                    <br>

                    <!-- The blueimp Gallery widget -->
                    <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
                        <div class="slides"></div>
                        <h3 class="title"></h3>
                        <a class="prev">‹</a>
                        <a class="next">›</a>
                        <a class="close">×</a>
                        <a class="play-pause"></a>
                        <ol class="indicator"></ol>
                    </div>

                    <script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td>
            <span class="preview"></span>
        </td>
        <td>
            <p class="name">{%=file.name%}</p>
            <strong class="error text-danger"></strong>
        </td>
        <td>
            <p class="size">Procesando...</p>
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
        </td>
        <td>
            {% if (!i && !o.options.autoUpload) { %}
                <button class="btn btn-primary start" disabled>
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Subir</span>
                </button>
            {% } %}
            {% if (!i) { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancelar</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}





                    </script>
                    <!-- The template to display files available for download -->
                    <script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        <td>
            <span class="preview">
                {% if (file.thumbnailUrl) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.gallery?'data-gallery':''%} ><img src="{%=file.thumbnailUrl%}"></a>
                {% } %}
            </span>
        </td>
        <td>
            <p class="name">
                {% if (file.url) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.gallery?'data-gallery':''%}>{%=file.name%}</a>
                {% } else { %}
                    <span>{%=file.name%}</span>
                {% } %}
            </p>
            {% if (file.error) { %}
                <div><span class="label label-danger">Error</span> {%=file.error%}</div>
            {% } %}
        </td>
        <td>
            <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </td>
        <td>
            {% if (file.delete_url) { %}
                <button class="btn btn-danger delete" onclick="borrarimg('{%=file.name%}','{%=file.delete_url%}','{%=file.delete_urlthumbnail%}')"  {% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Eliminar</span>
                </button>

            {% } else { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cerrar</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}





                    </script>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" onclick="cerrarModalSubirOp()">Cerrar</button>
                </div>
            </div>
        </div>
    </div>


    <div id="modaldescargarop" class="modal fade" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog " style="width:80% !important;">
            <div class="modal-content ">
                <div class="modal-header bg-blue-chambray bg-font-yellow text-center">

                    <button type="button" title="Cerrar" aria-hidden="true" class="btn btn-lg "
                            style="float: right;background-color: #2C3E50!important; border-radius:20px !important"
                            onclick="cerrarModalDescargarOp()">
                        <i class="fa fa-remove"></i></button>
                    <h4 class="modal-title"><strong>DESCARGAR OP</strong></h4>
                </div>
                <div class="modal-body" id="radicated_info">


                    <form id="formdescargarOp" action="https://jquery-file-upload.appspot.com/" method="POST"
                          enctype="multipart/form-data">
                        <!-- Redirect browsers with JavaScript disabled to the origin page -->
                        <noscript><input type="hidden" name="redirect"
                                         value="https://blueimp.github.io/jQuery-File-Upload/"></noscript>
                        <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
                        <div class="row fileupload-buttonbar"></div>
                        <!-- The table listing the files available for upload/download -->
                        <table role="presentation" class="table table-striped" id="tabladescargaranexos">
                            <tbody class="files"></tbody>
                        </table>
                    </form>

                    <br>

                    <!-- The blueimp Gallery widget -->
                    <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
                        <div class="slides"></div>
                        <h3 class="title"></h3>
                        <a class="prev">‹</a>
                        <a class="next">›</a>
                        <a class="close">×</a>
                        <a class="play-pause"></a>
                        <ol class="indicator"></ol>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" onclick="cerrarModalDescargarOp()">Cerrar
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div id="month_report_modal" class="modal fade" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-blue-chambray bg-font-yellow text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><strong>Detalle de Radicado</strong></h4>
                </div>
                <div class="modal-body" id="radicated_info"></div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="rad_img_modal" class="modal fade" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                </div>
                <div class="modal-body">
                    <img src="" class="img-responsive" width="100%">
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-css')
    <style>
        .tab-pane {
            min-height: 420px;
        }

        .tabbable-line > .nav-tabs > li.active {
            border-bottom: 4px solid #FF9900 !important;
        }

        .tabbable-line > .nav-tabs > li:hover {
            border-bottom: 4px solid rgba(255, 154, 0, 0.49) !important;
        }
    </style>
    <link href="{{$include_path}}global/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="{{$include_path}}global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"
          rel="stylesheet" type="text/css"/>
@endsection

@section('page-js')
    <script src="{{$include_path}}global/plugins/bootstrap-table/bootstrap-table.min.js"
            type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/bootstrap-table/locale/bootstrap-table-es-SP.js"
            type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"
            type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.es.js"
            type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/moment.min.js" type="text/javascript"></script>

    <script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.ui.widget.js"></script>
    <!-- The Templates plugin is included to render the upload/download listings -->
    <script src="{{$include_path}}global/plugins/blueimp/js/tmpl.min.js"></script>
    <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
    <script src="{{$include_path}}global/plugins/blueimp/js/load-image.all.min.js"></script>
    <!-- The Canvas to Blob plugin is included for image resizing functionality -->
    <script src="{{$include_path}}global/plugins/blueimp/js/canvas-to-blob.min.js"></script>
    <!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
    <script src="{{$include_path}}global/plugins/bootstrap/js/bootstrap.js"></script>
    <!-- blueimp Gallery script -->
    <script src="{{$include_path}}global/plugins/blueimp/js/jquery.blueimp-gallery.min.js"></script>

    <script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.iframe-transport.js"
            type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.fileupload.js" type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.fileupload-process.js"
            type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.fileupload-image.js"
            type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.fileupload-audio.js"
            type="text/javascript"></script>

    <script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.fileupload-video.js"
            type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.fileupload-validate.js"
            type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/jquery-upload/js/jquery.fileupload-ui.js"
            type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/jquery-upload/js/form-fileupload.min.js"
            type="text/javascript"></script>

    <!--  datatable -->
    <script src="{{$include_path}}global/plugins/datatable/datatable.js" type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/datatable/datatables.bootstrap.js" type="text/javascript"></script>



    <!--  datatable -->
    <?php
    $phpsession = session_name() . "=" . trim(session_id());
    ?>
    <script>

        var session_name='<?=$session_name?>​';
        var session_id='<?=trim($session_id)?>'
        var swicheselected = "";

        var contratistasarray = new Array();
        var nroContraSelected = 0;
        var swichetodos = false;
        var swicheselected = '<?php echo $swichedefault ?>';
        var adminsistema = '<?php echo $_SESSION['usua_admin_sistema'] ?>';
        var financiera = '<?php echo $_SESSION['usua_financiero']; ?>';
        var supervisor = '<?php echo $supervisor ?>';
        var contratista = '<?php echo $contratista ?>';
        var radicadoselected = "";
        var swicheanteriorAdmin = ""; //swiche que se tiene marcado antes de presionar el boton de administracion
        var phpsession='<?php  echo $phpsession ?>'
        var totalanexosadd=0;
        var radicados_sin_regenerar=null;
        var radicados_selecteds_v = []; //lista de radicados seleccionados
        var anexos_selecteds_v = []; //lista de id de anexos seleccionados
        var asuntosRadicados = new Array();
        var radi_padre = '';
        var url_app = window.location.href;
        var pestana_document = false;
        var borrador_id = "";
        var showBtnValidarModal = false;
        var disableSelectValidadores = true;
        var rutaraiz='../../';

        function ValRadPermi(rad,ruta){
            console.log('permisoRadTest');
            $.ajax({
                url: base_url+"app/radicados/generalradicado.php?function_call=verPermisologia&radicado=" + rad,
                type: 'GET',
                dataType: 'json',
                async: false,
                success: function (response) {
                    permisoRad = response;
                    console.log(permisoRad);
                    if (permisoRad.verImg == 'SI'){
                        funlinkArchivo(rad,ruta);
                    }else{
                        Swal.fire('Advertencia',permisoRad.verImgMsg,'warning');
                    }
                },
                error: function (response) {
                    Swal.fire("Error", "Ha ocurrido un error al consultar los permisos", "error");
                }
            });


        }

        function ValRadPermiGoTo(rad,ruta){
            console.log('permisoRadTest');
            $.ajax({
                url: base_url+"app/radicados/generalradicado.php?function_call=verPermisologia&radicado=" + rad,
                type: 'GET',
                dataType: 'json',
                async: false,
                success: function (response) {
                    permisoRad = response;
                    console.log(permisoRad);
                    if (permisoRad.verImg == 'SI'){
                        window.open("<?=$ruta_raiz?>/verradicado.php?verrad="+rad+"&PHPSESSID=<?=session_id()?>&krd=<?=$krd?>&nomcarpeta=Busquedas&tipo_carp=0&menu_ver_tmp=3");
                    }else{
                        Swal.fire('Advertencia',permisoRad.verImgMsg,'warning');
                    }
                },
                error: function (response) {
                    Swal.fire("Error", "Ha ocurrido un error al consultar los permisos", "error");
                }
            });


        }
        
        function armarAnexosOP(file) {
            //arma los anexos en el modal de subir OP

            var htmljavascript = '';

            for (var i = 0; i < file.length; i++) {

                htmljavascript += '<tr class="template-download fade in"> <td> <span class="preview">';

                if (file[i].thumbnailUrl != undefined) {

                    htmljavascript += '<a href="' + file[i].url + '" title="' + file[i].name + '" download="' + file[i].name + '" ';
                    //para que solo las imagees las muestre como galeria al darle click
                    if (file[i].type == "jpg" || file[i].type == "jpeg" || file[i].type == "png" | file[i].type == "gif") {
                        htmljavascript += ' data-gallery><img src="' + file[i].thumbnailUrl + '"></a>';
                    }

                }
                htmljavascript += '</span> </td> <td> <p class="name">';
                if (file[i].url != undefined) {
                    htmljavascript += '<a href="' + file[i].url + '" title="' + file[i].name + '" download="' + file[i].name + '" ';
                    if (file[i].thumbnailUrl != undefined) {

                        //para que solo las imagees las muestre como galeria al darle click
                        if (file[i].type == "jpg" || file[i].type == "jpeg" || file[i].type == "png" | file[i].type == "gif") {
                            htmljavascript += 'data-gallery'
                        }

                    }
                    htmljavascript += '> ' + file[i].name + '</a>';
                } else {
                    htmljavascript += '<span>' + file[i].name + '</span>';
                }
                htmljavascript += '</p>';
                if (file[i].error != undefined) {
                    htmljavascript += '<div><span class="label label-danger">Error</span> ' + file[i].error + '</div>';
                }
                htmljavascript += '</td> <td> <span class="size">' + file[i].size + '</span>';
                htmljavascript += '</td> ';

                var strname = "'" + file[i].name + "'";
                var strdelete_url = "'" + file[i].delete_url + "'";
                var strdelete_urlthumbnail = "'" + file[i].delete_urlthumbnail + "'";
                htmljavascript += '<td> <button class="btn btn-danger delete" ' +
                    'onclick="borrarimg(' + strname + ',' + strdelete_url + ',' + strdelete_urlthumbnail + ')" > ' +
                    '<i class="glyphicon glyphicon-trash"></i> <span>Eliminar</span> </button>' +
                    '</td></tr>';
                //'{%=file.name%}','{%=file.delete_url%}','{%=file.delete_urlthumbnail%}'
            }
            $("#tablaanexosop").append(htmljavascript)
        }

        function declararformanexo(){
            $("#formanexo").remove();
            $("#appendformanexo").html('');

            var html='';
            html+='<form id="formanexo" action="https://jquery-file-upload.appspot.com/" method="POST"' +
                ' enctype="multipart/form-data"> ' +
                '<noscript><input type="hidden" name="redirect" ' +
                'value="https://blueimp.github.io/jQuery-File-Upload/"></noscript> ' +
                '<div class="row fileupload-buttonbar">  ' +
                '<div class="col-lg-7"> <span class="btn green fileinput-button"> ' +
                '<i class="fa fa-plus"></i> <span>Agregar archivos</span>  <input type="file" ' +
                ' name="files[]" multiple="" ' +
                'style="position: absolute;top: 0;left: 0;margin: 0;opacity: 0; -ms-filter: alpha(opacity=0);font-size: 5px;direction: ltr;cursor: pointer;height: 30px;"> </span> ' +
            '<input type="hidden" name="id_borrador_anexo" id="id_borrador_anexo"> ' +
            '<button type="submit" class="btn btn-primary start"> <i class="glyphicon glyphicon-upload"></i> ' +
            '<span>Subir todos</span> </button> ' +
            '<button type="reset" class="btn btn-warning cancel"> <i class="glyphicon glyphicon-ban-circle"></i> ' +
            '<span>Cancelar todo</span> </button> <span class="fileupload-process"></span> </div> ' +
                '<div class="col-lg-5 fileupload-progress fade"> ' +
                '<div class="progress progress-striped active" role="progressbar" aria-valuemin="0"' +
                ' aria-valuemax="100"> <div class="progress-bar progress-bar-success" style="width:0%;"></div> ' +
                '</div> <div class="progress-extended">&nbsp;</div> </div> </div> ' +
                '<table role="presentation" class="table table-striped" id="tablaanexosop"> <tbody class="files"></tbody> ' +
                '</table> </form>';
            $("#appendformanexo").append(html);

        }

        function modalsubirop(numeroradicado,asunto) {
            //contadorarchivos es la cantidad de archivos que tiene
            //despliega el modal para subr el archivo

            radicadoselected = numeroradicado;
            $("#numeroradicado").val(numeroradicado);
            $("#numeroop").val('');

            //defino el formulario cada vez que abro el modal
            declararformanexo();

            $("#mostrarasuntoanexo").html('');
            $("#mostrarradicadoanexo").html('');
            $("#mostrarasuntoanexo").html(asunto);
            $("#mostrarradicadoanexo").html(numeroradicado);
            //busco los anexos, por si tiene, los muestro para que los puedan eliminar
            $.ajax({
                url: "process.php",
                method: "POST",
                data: {
                    radicado: numeroradicado, operation: "buscarAnexosOP",
                },
                dataType: "json",
                success: function (response) {

                    if (response.length > 0) {
                        armarAnexosOP(response);
                    }

                    $('#formanexo').fileupload({
                        url: 'subirop.php',
                        acceptFileTypes: /(\.|\/)(gif|jpe?g|png|docx|doc|pdf|ppt)$/i,
                        // formData: {numeroop: $("#numeroop").val(),numeroradicado: $("#numeroradicado").val()}
                    }).bind('fileuploadsubmit', function (e, data) {

                        //aqui entra ante sde hacer el submit
                        if ($("#numeroop").val() != "") {
                            data.formData = {numeroop: $("#numeroop").val(), numeroradicado: $("#numeroradicado").val()}
                        } else {
                            $("#numeroop").focus();
                            //busco a todos los botones que se ponen disabled
                            $("#formanexo > table").find(".start").removeAttr('disabled');
                            Swal.fire("Alerta", "Debe ingresar número de OP", "warning");
                            e.preventDefault();
                            return false;
                        }

                    }).bind('fileuploadcompleted', function (e, data) {
                        e.preventDefault();

                       //cuando se termina de subir cada archivo pasa por aqui
                        //aqui pregunto si es ==0 es decir el ultimo archivo que esta subiendo que tiene el boton se start
                        //para que en el ultimo sea que abra el radicado
                        if ($("#formanexo > table").find(".start").length ==0) {
                            //despues de subir los anexos, me envia al radicado
                            //window.open("../../verradicado.php?verrad=" + $("#numeroradicado").val() + "&" + phpsession + "&menu_ver_tmp=3")
                        }
                    });

                    // Enable iframe cross-domain access via redirect option:
                    $('#formanexo').fileupload(
                        'option',
                        'redirect',
                        window.location.href.replace(
                            /\/[^\/]*$/,
                            '/cors/result.html?%s'
                        )
                    );

                }
            }).done(function (response) {

            }).fail(function (error) {

            });

            $("#modalsubirop").modal({show: true, keyboard: false, backdrop: 'static'});
        }

        function descargarop(evento,numeroradicado, ophabilitadas, tieneop, primerfaltandias, primerfechapegadp) {
            //despliega el modal para subr el archivo


            if (tieneop < 1) {
                Swal.fire("Alerta", "No ha sido cargada la Órden de Pago OP", "warning");
                return false;
            }

            if (ophabilitadas < 1) {
                Swal.fire("Alerta", "Faltan " + primerfaltandias + " días hábiles para el pago (" + primerfechapegadp + ")", "warning");
                return false;
            }
            $.ajax({
                url: "process.php",
                method: "POST",
                dataType: "json",
                data: {
                    radicado: numeroradicado, operation: "buscarAnexosOP",
                },
                success: function (response) {

                    //si tiene un solo anexos, lo mando a descargar de una vez
                    if (response.length == 1) {
                        for (var j = 0; j < response.length; j++) {
                            //openanexoventana( false,response[j]['ID'])
                            funlinkArchivo(response[j]['ANEX_CODIGO']);
                            break;
                        }
                    } else if (response.length > 1) {
                        //si no, muestro el modal
                        modaldescargarop(response);

                    } else {
                        Swal.fire("Error", "No tiene OP o no puede descargar el archivo", "error");
                    }

                }
            }).done(function () {
            }).fail(function (error) {
                console.log(error);
            });

        }

        function openanexoventana(evento,idanexo){
            if(evento){
                evento.preventDefault();
            }
            window.open("descargarop.php?&anexoid=" + idanexo)
        }

        function modaldescargarop(response) {
            //contadorarchivos es la cantidad de archivos que tiene
            //despliega el modal para descargar los archivos, solo cuando son mas de 1
            $(".files").html('');
            armarDescargarAnexos(response);
            $('#formdescargarOp').fileupload({
                url: 'filefake.php',
                acceptFileTypes: /(\.|\/)(gif|jpe?g|png|docx|doc|pdf|ppt)$/i,
                messages : {
                    maxNumberOfFiles: 'Número máximo de archivos excedidos',
                    acceptFileTypes: 'Tipo de archivo no permitido',
                    maxFileSize: 'Tamaño de archivo No permitido',
                    minFileSize: 'El archivo es demasiado pequeño',
                    uploadedBytes : 'Los bytes cargados exceden el tamaño del archivo'
                },
                // formData: {numeroop: $("#numeroop").val(),numeroradicado: $("#numeroradicado").val()}
            }).bind('fileuploadsubmit', function (e, data) {
                Swal.fire("Alerta", "No tiene permitida esta opcion", "warning");
                e.preventDefault();
                return false;
            });
            // Enable iframe cross-domain access via redirect option:
            $('#formdescargarOp').fileupload(
                'option',
                'redirect',
                window.location.href.replace(
                    /\/[^\/]*$/,
                    '/cors/result.html?%s'
                )
            );
            $("#modaldescargarop").modal({show: true, keyboard: false, backdrop: 'static'});

        }

        function armarDescargarAnexos(file){
            //arma los anexos en el modal de subir OP

            var htmljavascript = '';
            var anexcodigo="";
            for (var i = 0; i < file.length; i++) {

                htmljavascript += '<tr class="template-download fade in"> <td> <span class="preview">';

                anexcodigo="'"+file[i].ANEX_CODIGO+"'";
                if (file[i].thumbnailUrl != undefined) {

                    htmljavascript += '<a href="#"' +
                        ' onclick="funlinkArchivo('+anexcodigo+');" ' +
                        'title="' + file[i].name + '"  >';
                    //para que solo las imagees las muestre como galeria al darle click

                        htmljavascript += ' <img src="' + file[i].thumbnailUrl + '"></a>';


                }
                htmljavascript += '</span> </td> <td> <p class="name">';
                if (file[i].url != undefined) {
                    htmljavascript += '<a  href="#" ' +
                        'onclick="funlinkArchivo('+anexcodigo+');" title="' + file[i].name + '"  ';

                    htmljavascript += '> ' + file[i].name + '</a>';
                } else {
                    htmljavascript += '<span>' + file[i].name + '</span>';
                }
                htmljavascript += '</p>';
                if (file[i].error != undefined) {
                    htmljavascript += '<div><span class="label label-danger">Error</span> ' + file[i].error + '</div>';
                }
                htmljavascript += '</td> <td> <span class="size">' + file[i].size + '</span>';
                htmljavascript += '</td> ';

                htmljavascript += '<td> <input type="button" value="Descargar"' +
                    ' onclick="funlinkArchivo('+anexcodigo+');" class="btn blue"> ' +
                    '</td></tr>';
            }
            $("#tabladescargaranexos").append(htmljavascript)

        }

        function validarcheck(datos) {
            //aqui valido si los que se muestran en pantalla, alguno ya se le fue marcado el check
            //datos.contratistas son los productos que se estan mostrando actualmente en la pagina, ejemplo: los 10 que estan
            var cedula = "";
            var tipo = "";
            var contratistas = datos.contratistas;

            var posicion = "";
            for (var i = 0; i < contratistas.length; i++) {
                posicion = "";
                cedula = contratistas[i].identificacion;
                tipo = contratistas[i].tipo;

                //valido si ya lo guarde en el arreglo  contratistasarray
                for (var j = 0; j < contratistasarray.length; j++) {
                    if (contratistasarray[j].cedula == cedula &&
                        contratistasarray[j].tipo == tipo &&
                        contratistasarray[j].activo == true) {
                        posicion = j;
                        break;
                    }
                }
                if (posicion !== "") { //coloco checked el input type checkbox
                    $("#identif_" + cedula).attr('checked', true);
                }
            }

        }

        function tablaprincipalOP() {

            $('#loader_tableopcontainer').removeClass('hide');

            var datatype = "json";
            if (swicheselected == "Contratista" || swicheselected == "Financiera" || swicheselected == "Supervisor") {
                datatype = "html"

                $.ajax({
                    url: "process.php",
                    method: "POST",
                    data: {
                        from: $("#datefromop").val(), to: $("#datetoop").val(), operation: "tablaprincipalOP",
                        swiche: swicheselected
                    },
                    dataType: datatype,
                    success: function (response) {
                        $("#tableop").bootstrapTable('destroy');
                        var ret_pay_cont = $("#tableopcontainer");
                        if (response != "ERROR") {
                            ret_pay_cont.html('');
                            ret_pay_cont.html(response);
                            ret_pay_cont.removeClass('hide');
                        }
                        $('#loader_tableopcontainer').addClass('hide');
                    }
                }).done(function () {
                    var data = {}
                    data.swiche = swicheselected;
                    data.from = $("#datefromop").val();
                    data.to = $("#datetoop").val();
                    var url = "datostablaop.php"
                    declarardatatableOp("tableop", url, data);
                    $('#loader_tableopcontainer').addClass('hide');
                }).fail(function (error) {
                    console.log(error);
                    $('#loader_tableopcontainer').addClass('hide');
                });
                $('#beforetableopcontainer').addClass('hide');
            }

        }

        //accion de asignar supervisores a contratistas
        function guardarSupervisores(e) {
            e.preventDefault();
            $('#loader_returned_container').removeClass('hide');

            if (($("#superprinci").val() != "" && $("#superprinci").val() != null &&
                $("#superprinci").val()!="null") ||
                ( $("#superapoyo").val() != "" && $("#superapoyo").val() != null && $("#superapoyo").val()!="null" )) {

                $.ajax({
                    url: "process.php",
                    method: "POST",
                    data: {
                        principal: $("#superprinci").val(), apoyo: $("#superapoyo").val(),
                        contratistas: contratistasarray,
                        operation: "guardarSupervisores"
                    },
                    dataType: "json",
                    success: function (response) {

                        if (response) {
                            $("#nro_contra_selected").html("0 contratistas seleccionados)")
                            $("#contra_selected_2").html("0 contratistas seleccionados)")
                            contratistasarray = new Array();
                            nroContraSelected = 0;
                            tablaAdmin();
                            $("#modal_asignarsuper").modal('hide');
                            Swal.fire("Asignar", "Se han asignado con éxito", "success");
                        } else {
                            Swal.fire("Error", "No se pudo asignar los supervisores", "error");
                        }

                        $('#loader_returned_container').addClass('hide');
                    }
                }).done(function () {
                    $('#loader_returned_container').addClass('hide');
                }).fail(function (error) {
                    $('#loader_returned_container').addClass('hide');
                });
            } else {
                $('#loader_returned_container').addClass('hide');
                Swal.fire("Alerta", "Debe seleccionar al menos un supervisor", "warning");
            }

        }

        function modalAsignaSuper() { //el boton azul que dice Asignar Supervisores

            //solo pueden asignar supervisores los supervisores o administradores
            if (supervisor > 0 || adminsistema == '1') {

                $("#div_msj_no_admin").css('display', 'none');
                //levanta el modal para asignar supervisores a los cotratistas
                if (contratistasarray.length < 1) {
                    Swal.fire("Alerta", "Debe seleccionar al menos un contratista", "warning");
                    return false;
                }

                $(".js-data-example-ajax").val('');
                //busca los datos para la caja de supervisor apoyo
                $("#superprinci").select2(
                    {
                        dropdownParent: $("#modal_asignarsuper"),
                        data: {},
                        ajax: {
                            url: "buscarSupervisores.php",
                            dataType: 'json',
                            delay: 250,
                            data: function (params) {
                                return {
                                    search: params.term,
                                    type: 'public'
                                };
                            },
                            processResults: function (data, params) {
                                return {
                                    results: data,

                                };
                            },
                            cache: true
                        },
                        placeholder: 'Buscar supervisor principal',
                        escapeMarkup: function (markup) {
                            return markup;
                        }, // let our custom formatter work
                        language: {
                            inputTooShort: function () {
                                return 'Ingrese un nombre para buscar';
                            }
                        }
                    }).on('select2:selecting', function (e) {
                    //ese id es el numero de radicado
                    //cuando selecciono una opcion
                    if (e.params.args.data.id == $("#superapoyo").val()) {
                        e.preventDefault();
                        Swal.fire("Alerta", "Debe seleccionar un supervisor principal distinto", "warning");
                        return false;
                    }

                });
                ;

                $("#superapoyo").select2(
                    {
                        dropdownParent: $("#modal_asignarsuper"),
                        data: {},
                        ajax: {
                            url: "buscarSupervisorApoyo.php",
                            dataType: 'json',
                            delay: 250,
                            data: function (params) {
                                return {
                                    search: params.term,
                                    type: 'public'
                                };
                            },
                            processResults: function (data, params) {
                                return {
                                    results: data,

                                };
                            },
                            cache: true
                        },
                        placeholder: 'Buscar supervisor de apoyo',
                        escapeMarkup: function (markup) {
                            return markup;
                        }, // let our custom formatter work
                        language: {
                            inputTooShort: function () {
                                return 'Ingrese un nombre para buscar';
                            }
                        }
                    }).on('select2:selecting', function (e) {
                    //ese id es el numero de radicado
                    //cuando selecciono una opcion
                    if (e.params.args.data.id == $("#superprinci").val()) {
                        e.preventDefault();
                        Swal.fire("Alerta", "Debe seleccionar un supervisor de apoyo distinto", "warning");
                        return false;
                    }

                });
                $("#modalAsignaSuper").modal('hide');
                $("#modal_asignarsuper").modal('show');

                if (adminsistema == 0) {
                    $("#div_msj_no_admin").css('display', 'block');
                }
            } else {
                Swal.fire("Alerta", "Sólo los Supervisores o Administrador del sistema pueden asignar. " +
                    "Solicitar soporte", "warning");
                return false;
            }

        }

        function cerrarMdalListaContratista() {
            $("#modalListaContratista").modal('hide');
        }

        function tablaAdmin() {

            $("#modalListaContratista").modal('show');
            //esta es la tabla admin
            if (swicheselected != "Administracion") {
                swicheanteriorAdmin = swicheselected;
                $('#' + swicheselected).bootstrapSwitch('state', false);
            }

            swicheselected = 'Administracion'
            $("#nro_contra_selected").html("0 contratistas seleccionados)")
            $("#contra_selected_2").html("0 contratistas seleccionados)")
            contratistasarray = new Array();
            nroContraSelected = 0;
            $.ajax({
                url: "process.php",
                method: "POST",
                data: {
                    from: $("#datefromop").val(), to: $("#datetoop").val(), operation: "tablaprincipalOP",
                    swiche: swicheselected
                },
                dataType: "html",
                success: function (response) {
                    $("#tableopAdmin").bootstrapTable('destroy');
                    var ret_pay_cont = $("#tableAdminContainer");
                    if (response != "ERROR") {
                        ret_pay_cont.html('');
                        ret_pay_cont.html(response);
                        ret_pay_cont.removeClass('hide');
                    }

                    $('#loader_tableopcontainer').addClass('hide');

                }
            }).done(function (response) {
                var data = {}
                data.swichetodos = swichetodos;
                var url = "datostablaadmin.php"
                declarardatatableAdmin("tableopAdmin", url, data);
                $('#loader_tableopcontainer').addClass('hide');
            }).fail(function (error) {
                console.log(error);
                $('#loader_tableopcontainer').addClass('hide');
            });


        }


        function declarardatatableAdmin(tablaid, url, data) {


            var table = $('#' + tablaid).DataTable();
            table.destroy();
            //$('#tableop').remove();


            var tableoptions = {
                "destroy": true,
                "processing": true,
                "serverSide": true,
                "ajax": {
                    "url": url,
                    "data": function (d) {
                        d.data = data
                    },
                },
                rowId: 'DT_RowId',

                retrieve: false,
                "aLengthMenu": [[10, 20, 30, -1], [10, 20, 30, "Todos"]],

                "scrollY": "300px",
                "scrollX": true,
                "scrollCollapse": true,

                /********para poder mover con las flechas*********/
                keys: true,
                "searching": true,
                "ordering": true,
                "bPaginate": true,
                fixedHeader: {
                    header: true,
                    footer: true
                },
                paging: true,
                info: true,
                "sScrollX": "100%",

                autoWidth: false,
                "columns": [
                    {"width": "5%"},
                    {"width": "30%"},
                    {"width": "15%"},
                    {"width": "30%"},
                    {"width": "20%"}
                ],
                "order": [],
                "columnDefs": [{
                    "targets": 'no-sort',
                    "orderable": false,
                }],
                "language": {
                    "emptyTable": "No se encontraron registros",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ resultados",
                    "infoEmpty": "Mostrando 0 a 0 de 0 resultados",
                    "infoFiltered": "(filtrado de _MAX_ total resultados)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ resultados",
                    "loadingRecords": "Cargando...",
                    "processing": "Buscando...",
                    "search": "Búsqueda:",
                    "zeroRecords": "No se encontraron resultados",
                    "paginate": {
                        "first": "<<",
                        "last": ">>",
                        "next": "<",
                        "previous": ">"
                    },
                    "aria": {
                        "sortAscending": ": activar ordenar columnas ascendente",
                        "sortDescending": ": activar ordenar columnas descendente"
                    }
                },
                /************************************/
                "fnInitComplete": function (settings, json) {


                    //validarcheck(json);

                    setTimeout(function () {
                        $('div.dataTables_filter input').focus();
                    }, 5);
                },

                "fnDrawCallback": function (datos) {
                    //esto funciona cuando se hace el paginate, ya que con fnInitComplete solo lo hace una vez
                    validarcheck(datos.json);
                    setTimeout(function () {
                        $('div.dataTables_filter input').focus();
                    }, 5);


                },

                "footerCallback": function (row, data, start, end, display) {
                    var api = this.api();

                    var i = 0;
                    api.columns().every(function (index) {

                        if (i > 0) {
                            var sum = this
                                .data()
                                .reduce(function (a, b) {

                                    var x = parseFloat(a) || 0;

                                    if (isNaN(x)) {
                                        x = 0;
                                    }
                                    //   console.log(x);
                                    // console.log(b);
                                    if (b != null) {
                                        if (isNaN(b)) {
                                            b = b.replace(/[,.]/g, function (m) {
                                                // m is the match found in the string
                                                // If `,` is matched return `.`, if `.` matched return `,`
                                                return m === ',' ? '.' : '';
                                            });
                                        }

                                        // console.log(b);
                                        // b=parseFloat(b.replace('.','').replace(',','.'));
                                        var y = parseFloat(b) || 0;


                                        //  console.log(x + y);
                                        return x + y;
                                    }
                                }, 0);

                            if (sum != undefined) {

                                $(this.footer()).html(sum.toLocaleString('de-DE', {
                                    maximumFractionDigits: 2,
                                    minimumFractionDigits: 2,
                                }));
                            }

                        }
                        i++;


                    });

                }


            };


            $('#' + tablaid).DataTable(tableoptions);

            $(".dataTables_paginate").css('color', '#666 !important')
        }
        function format(tr) {
            //mando a buscar la informacion y me retorna el html
            var a = mostrarinfor(tr.attr('data-radicado'), tr.attr('data-dependencia'), tr.attr('data-usuario_actual'),
                tr.attr('data-contratista_identif'),tr.attr('data-dependencia_actual_numero'))
            return a;
        }
        function declarardatatableOp(tablaid, url, data) {
            var table = $('#tableop').DataTable();
            table.destroy();

            var tableoptions = {
                "destroy": true,
                "processing": true,
                "serverSide": true,
                "ajax": {
                    "url": url,
                    "data": function (d) {
                        d.data = data
                    },
                },
                rowId: 'DT_RowId',

                retrieve: false,
                "aLengthMenu": [[10, 20, 30, -1], [10, 20, 30, "Todos"]],

                "scrollX": true,
                "scrollCollapse": true,

                /********para poder mover con las flechas*********/
                keys: true,
                "searching": true,
                "ordering": true,
                "bPaginate": true,
                fixedHeader: {
                    header: true,
                    footer: true
                },
                paging: true,
                info: true,
                "sScrollX": "100%",

                autoWidth: false,
                "columns": [
                    {
                        "width": "3%",
                        "class": "details-control"
                    },
                    {"width": "20%"},
                    {"width": "35%"},
                    {"width": "20%"},
                    {"width": "15%"},
                    {"width": "7%"}
                ],
                "order": [],
                "columnDefs": [
                    {"targets": 'no-sort', "orderable": false,"width": "3%"},
                    { "width": "20%", "targets": 1 },
                    { "width": "10%", "targets": 2 },
                    { "width": "40%", "targets": 3 },
                    { "width": "20%", "targets": 4 },
                    { "width": "7%", "targets": 5 }

                    ],
                'createdRow': function (row, data, dataIndex) {
                    $(row).attr('tabindex', dataIndex);
                    $(row).attr('data-radicado', data['RADICADO']);
                    $(row).attr('data-dependencia', data['DEPENDENCIA_ACTUAL']);
                    $(row).attr('data-usuario_actual', data['USUARIO_ACTUAL']);
                    $(row).attr('data-contratista_identif', data['CONTRATISTA_IDENTIF']);
                    $(row).attr('data-dependencia_actual_numero', data['DEPENDENCIA_ACTUAL_NUMERO']);

                },
                "language": {
                    "emptyTable": "No se encontraron registros",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ resultados",
                    "infoEmpty": "Mostrando 0 a 0 de 0 resultados",
                    "infoFiltered": "(filtrado de _MAX_ total resultados)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ resultados",
                    "loadingRecords": "Cargando...",
                    "processing": "Buscando...",
                    "search": "Búsqueda:",
                    "zeroRecords": "No se encontraron resultados",
                    "paginate": {
                        "first": "<<",
                        "last": ">>",
                        "next": "<",
                        "previous": ">"
                    },
                    "aria": {
                        "sortAscending": ": activar ordenar columnas ascendente",
                        "sortDescending": ": activar ordenar columnas descendente"
                    }
                },
                /************************************/
                "fnInitComplete": function (settings, json) {

                    setTimeout(function () {
                        $('div.dataTables_filter input').focus();
                    }, 5);
                },

                "fnDrawCallback": function (datos) {
                    //esto funciona cuando se hace el paginate, ya que con fnInitComplete solo lo hace una vez

                    if (tablaid == "tableopAdmin") {
                        validarcheck(datos.json);
                    }

                    setTimeout(function () {
                        $('div.dataTables_filter input').focus();
                    }, 5);


                },

                "footerCallback": function (row, data, start, end, display) {

                }


            };

            var table = $('#tableop').DataTable(tableoptions);
            $(".dataTables_paginate").css('color', '#666 !important')

            // Array to track the ids of the details displayed rows
            var detailRows = [];

            //al darle click al primer td del priemr tr, mando a mostrar el detalle
            $('#tableop tbody').on('click', 'tr td.details-control', function () {
                var tr = $(this).closest('tr');
                var row = table.row(tr);
                var idx = $.inArray(tr.attr('id'), detailRows);

                $('#loader_tableopcontainer').removeClass('hide');
                if (row.child.isShown()) {
                    //busco el i que es el boton azul para cambiar e el + o -
                    //entra aqui cuando lo mando a cerrar
                    var iconoazul = tr.find("i")
                    if (iconoazul.length > 0) {
                        iconoazul.removeClass('glyphicon-minus')
                        iconoazul.removeClass('icon-minus')
                        iconoazul.addClass('glyphicon-plus')
                        iconoazul.addClass('icon-minus')
                    }
                    tr.removeClass('details');
                    row.child.hide();

                    // Remove from the 'open' array
                    detailRows.splice(idx, 1);
                }
                else {

                    //entra aqui cuando voy a desplegar el detalle
                    tr.addClass('details');
                    //mando el tr
                    row.child(format(tr)).show();
                    $('.btn_popovers').popover();
                    var iconoazul = tr.find("i")
                    if (iconoazul.length > 0) {
                        iconoazul.removeClass('glyphicon-plus')
                        iconoazul.removeClass('icon-plus')
                        iconoazul.addClass('glyphicon-minus')
                        iconoazul.addClass('icon-minus')
                    }
                    // Add to the 'open' array
                    if (idx === -1) {
                        detailRows.push(tr.attr('id'));
                    }

                }
                $('#loader_tableopcontainer').addClass('hide');

            });

            // On each draw, loop over the `detailRows` array and show any child rows
            table.on('draw', function () {
                $.each(detailRows, function (i, id) {
                    $('#' + id + ' td.details-control').trigger('click');
                });
            });
        }

        function addSupervisores(cedula, esto, tipo) {
            //cada vez que se presiona en un checkbox de la lista de contratistas, en el modal
            //de asignar supervisores
            var posicion = "";
            if ($(esto).is(':checked')) {

                for (var j = 0; j < contratistasarray.length; j++) {
                    if (contratistasarray[j].cedula == cedula &&
                        contratistasarray[j].tipo == tipo) {
                        posicion = j;
                        break;
                    }
                }

                if (posicion === "") {

                    var length = contratistasarray.length
                    contratistasarray[length] = {}
                    contratistasarray[length].cedula = cedula;
                    contratistasarray[length].activo = true;
                    contratistasarray[length].tipo = tipo;
                } else {
                    contratistasarray[posicion].activo = true;
                }

                nroContraSelected = parseInt(nroContraSelected) + parseInt(1);

            } else {

                for (var j = 0; j < contratistasarray.length; j++) {
                    if (contratistasarray[j].cedula == cedula) {
                        posicion = j;
                        break;
                    }
                }


                if (posicion === "") {

                    var length = contratistasarray.length
                    contratistasarray[length] = {}
                    contratistasarray[length].cedula = cedula;
                    contratistasarray[length].activo = false;
                    contratistasarray[length].tipo = tipo;
                } else {

                    contratistasarray[posicion].activo = false;
                }

                nroContraSelected = parseInt(nroContraSelected) - parseInt(1);
            }

            $("#nro_contra_selected").html("(" + nroContraSelected + " contratistas seleccionados)")
            $("#contra_selected_2").html("(" + nroContraSelected + " contratistas seleccionados)")
            $("#labelmodalasignsuperv").html("Asignar supervisores a (" + nroContraSelected + ") contratista(s)")

        }

        /**
         * Aqui va a entrar, si la cantidad de anexos es menor a 4, e incluso si es ==0.
         * - Si Object.keys(response.anexos).length===0, entonces no entró en el for(i=0) de arriba
         *      por lo tanto, mando a mostrar la primera fila con los botones, y luego mando a imprimir
         *      3 filas mas, del supervisor, apoyo, etc.
         *
         * - Si es mayor a 0 y menor a 4 la cantidad e anexos. entonces, si entró en el for(i=0) de arriba.
         *      Por lo tanto, si la cantidad de anexos ==1, solo recorrió una vez arriba, por lo tanto imprimió
         *      los botones, pero no imprimió los supervisores, apoyo, etc, por lo tanto,
         *      con la funcion: armarContApoSupRowVacio(), le envio la cantidad de filas que quiero que cree
         *      para los supervidores, apoyo, etc
         */

        function armarContApoSupRowVacio(response,nroeach,radicado){
            var html='';
            var contador= nroeach
            for (var i = 0; i < nroeach; i++) {


                if(i==0 && contador==2){
                    html+="<tr>";

                    /**
                     * mando a imprimir el historial, para meterlo todo en una sola fila
                     */
                    html+= armarHistorialOp(response);

                    html+="<td colspan='2'  style='width:35%'>";
                    html+='<div class="col-md-12 " style="float: left" id="">\n' +
                        '                    <div class="note note-success" ' +
                        ' style="background-color: rgb(245, 248, 253) !important">\n' +
                        '                        <h5 class="block"><strong style="float:left;margin-left: 1px">' +  response.supervisor  +'</strong></h5>\n' +
                        '                        <p>Supervisor</p></div>\n' +
                        '                </div>' +
                        "</td>";
                    html += ' </tr>';
                    contador--;
                }


                if(i==1 && contador==1){
                    html+="<tr>";
                    html += armarVerHistoricoCompleto(radicado);

                    html+="<td colspan='2'  style='width:35%'>";
                    html+='<div class="col-md-12 " style="float: left" id="">\n' +
                        '                    <div class="note note-danger"' +
                        ' style="background-color: rgb(245, 248, 253) !important">\n' +
                        '                        <h5 class="block"><strong style="float:left;margin-left: 1px">' + response.apoyo +'</strong></h5>\n' +
                        '                        <p>Apoyo a la Supervisión</p></div>\n' +
                        '                </div>' +
                        "</td>";
                    html += ' </tr>';
                    contador--;
                }

            }

            return html;
        }

        function armarHistorialOp(response){

            var html='';

            html+='<td colspan="2" style="width:65%; "><div class="portlet-body" id="chats">\n' +
                '                                                            <div class="slimScrollDiv"\n' +
                '                                                                 style="position: relative; overflow: hidden; width: auto; height: auto;">\n' +
                '                                                                <div class="scroller"\n' +
                '                                                                     style="height: auto; overflow: hidden; width: auto;"\n' +
                '                                                                     data-always-visible="1" data-rail-visible1="1"\n' +
                '                                                                     data-initialized="1">\n' +
                '                                                                    <ul class="chats" id="ulchat">';

            var fecha = "";
            var comentario = '';
            for (var i = 0; i < response.historial.length; i++) {

                fecha = response.historial[i]["HIST_FECH"];

                html += ' <li class="in"> <div class="message"> <span class="arrow"> </span>' +
                    '<a href="#" class="name">' + response.historial[i]["USUA_NOMB"] + '</a>' +
                    '<span class="datetime"> ' + fecha + ' </span>' +
                    '<a href="#" class="name">';

                comentario = response.historial[i]["HIST_OBSE"];
                html += '</a>' +
                    ' <span class="body">' + comentario + '</span>' +
                    ' </div> </li>';


            }
            html+="</ul></div></div></div></td>";

            return html;
        }

        function armarVerHistoricoCompleto(radicado){
            var html='';
            var urlopne = '"../../verradicado.php?verrad=' + radicado + '&<?php echo session_name()?>​=<?php echo trim(session_id())?>​&menu_ver_tmp=3"'

            html += "<td style='font-size: 10pt; width:65%; text-align: center;' colspan='2'><a href='#' style='float:center; border:1px solid blue;  '​ ​" +
                " onclick='window.open(" + urlopne + ")'>" +
                "Ver Historial Completo y datos del radicado</a>" +
                "</td>";

            return html;

        }

        function armarInfo(response, dependencia, usuarioactual, radicado,dependActualNum) {


            var anexcodigo="";

            var html = "<table border='0' width='100%' heigth='pixels|%' style=''> " ;
            var titulopopover= '';
            var contentpopover = '';
            var colortbn='';
            var urlcomillas='';
            var urlSinComillas='';
            var extension= '';
            for (var i = 0; i <  Object.keys(response.anexos).length; i++) {

                urlSinComillas= rutaraiz+'bodega/'+response.anexos[i].anio+'/'+response.anexos[i].dependencia+'/docs/';
                urlSinComillas+= response.anexos[i].ANEX_NOMB_ARCHIVO;
                urlcomillas = "'"+rutaraiz+'bodega/'+response.anexos[i].anio+'/'+response.anexos[i].dependencia+'/docs/';
                urlcomillas+= response.anexos[i].ANEX_NOMB_ARCHIVO+"'";

                extension= "'"+response.anexos[i].extension+"'";
                html+="<tr>" +
                "<td style='font-size: 10pt;width:20%; '>";

                anexcodigo="'"+response.anexos[i].ANEX_CODIGO+"'";
                html += '<i class="jstree-icon jstree-themeicon fa fa-file icon-state-warning icon-lg' +
                    ' jstree-themeicon-custom" role="presentation"></i>';

                if(response.anexos[i].extension== "pdf" || response.anexos[i].extension == "PDF"){
                html += '<a class="jstree-anchor jstree-clicked" tabindex="-1" ' +
                    '  href="#" onclick="muestraDocVistaPrevia(' + urlcomillas + ',' + extension + ')" ' +
                    'style="">' + response.anexos[i].ANEX_NOMB_ARCHIVO + '</a>';
            } else {

                html += '<a class="jstree-anchor jstree-clicked" tabindex="-1" ' +
                    ' href="' + urlSinComillas + '" target="_blank" ' +
                    'style="">' + response.anexos[i].ANEX_NOMB_ARCHIVO + '</a>';
            }

                html += ' </td>';

                html+="<td style='width:45%;'>";
                html += response.anexos[i].ANEX_DESC
                html += ' </td>';
                var radi_nume_salida =response.anexos[i].RADI_NUME_SALIDA!=undefined &&
                    response.anexos[i].RADI_NUME_SALIDA!='' &&  response.anexos[i].RADI_NUME_SALIDA!=null &&
                    response.anexos[i].RADI_NUME_SALIDA!='null'?response.anexos[i].RADI_NUME_SALIDA:0;


                if(i<1){


                    titulopopover= conjugacionValidar[5];
                    contentpopover = 'Ver '+conjugacionValidar[5];
                    colortbn='blue';

                    if(Object.keys(response.document_metadata).length<1){
                        contentpopover = 'Sin '+conjugacionValidar[5];
                        colortbn='grey-salt';
                    }


                    html+="<td colspan='2'  style='width:35%'>";
                    html+='<button type="button"  class=" btn_popovers btn btn-circle btn-sm '+colortbn+' popovers"' +
                        ' data-original-title="'+titulopopover+'" ' +
                        ' data-content="'+contentpopover+'" ' +
                        ' data-placement="top" data-trigger="hover" data-container="body" ' +
                        'onclick="doc_clickvalidarraiz('+radi_nume_salida+','+response.anexos[i].ID+')">  <i class="fa fa-pencil"></i></button>';

                    titulopopover= 'EXPEDIENTE';
                    contentpopover = 'Ver Expediente';
                    colortbn='yellow';

                    if(Object.keys(response.expedientes).length<1){
                        contentpopover = 'Sin archivar en Expediente';
                        colortbn='grey-salt';
                    }
                    var string="'NOCREAREXPEDSINRAD'";

                    html+='<button type="button"  ' +
                        ' data-original-title="'+titulopopover+'" ' +
                        ' data-content="'+contentpopover+'" ' +
                        ' data-placement="top" data-trigger="hover" data-container="body" ' +
                        'class="btn btn-circle btn-sm '+colortbn+' popovers btn_popovers"   ' +
                        '> <i class="fa fa-folder-open-o"></i></button>';

                    titulopopover= 'ANULADO';
                    contentpopover = 'Radicado Anulado';
                    colortbn='red';
                    /**
                     *Si fue anulado, entonces muestro el boton, si no, no.
                     */

                    if(Object.keys(response.anulado).length>0){
                        html+='<button type="button" class="btn btn-circle btn-sm red"> <i class="fa icon-ban"></i></button>';
                    }

                   html+="</td>";
                }else if(i===1){


                    html+="<td colspan='2'  style='width:35%'>";
                    html+='<div class="col-md-12 " style="float: left" id="">\n' +
                        '                    <div class="note note-success">\n' +
                        '                        <h5 class="block"><strong style="float:left;margin-left: 1px">' + response.supervisor +'</strong></h5>\n' +
                        '                        <p>Supervisor</p></div>\n' +
                        '                </div>' +
                        "</td>";

                }else if(i===2){

                    html+="<td colspan='2'  style='width:35%'>";
                    html+='<div class="col-md-12 " style="float: left" id="">\n' +
                        '                    <div class="note note-danger">\n' +
                        '                        <h5 class="block"><strong style="float:left;margin-left: 1px">' +  response.apoyo +'</strong></h5>\n' +
                        '                        <p>Apoyo a la Supervisión</p></div>\n' +
                        '                </div>' +
                        "</td>";
                }else{
                    html+="<td colspan='2'  style='width:35%'></td>";
                }

                html += ' </tr>';

            }


            if(Object.keys(response.anexos).length<4){

                /**
                 * Aqui va a entrar, si la cantidad de anexos es menor a 4, e incluso si es ==0.
                 * - Si Object.keys(response.anexos).length===0, entonces no entró en el for(i=0) de arriba
                 *      por lo tanto, mando a mostrar la primera fila con los botones, y luego mando a imprimir
                 *      3 filas mas, del supervisor, apoyo, etc.
                 *
                 * - Si es mayor a 0 y menor a 4 la cantidad e anexos. entonces, si entró en el for(i=0) de arriba.
                 *      Por lo tanto, si la cantidad de anexos ==1, solo recorrió una vez arriba, por lo tanto imprimió
                 *      los botones, pero no imprimió los supervisores, apoyo, etc, por lo tanto,
                 *      con la funcion: armarContApoSupRowVacio(), le envio la cantidad de filas que quiero que cree
                 *      para los supervidores, apoyo, etc
                 */

                if(Object.keys(response.anexos).length===0){
                    html+="<tr>" +
                        "<td style='font-size: 10pt;width:20%; '>";
                    html += ' </td>';

                    html += "<td style='width:45%; '>";
                    html += ' </td>';

                    html+="<td colspan='2'  style='width:35%'>";

                    titulopopover= conjugacionValidar[5];
                    contentpopover = 'Ver '+conjugacionValidar[5];
                    colortbn='blue';

                    if(Object.keys(response.document_metadata).length<1){
                        contentpopover = 'Sin '+conjugacionValidar[5];
                        colortbn='grey-salt';
                    }


                    html+="<td colspan='2'  style='width:35%'>";
                    html+='<button type="button"  class=" btn_popovers btn btn-circle btn-sm '+colortbn+' popovers"' +
                        ' data-original-title="'+titulopopover+'" ' +
                        ' data-content="'+contentpopover+'" ' +
                        ' data-placement="top" data-trigger="hover" data-container="body" ' +
                        'onclick="doc_clickvalidarraiz('+radi_nume_salida+','+response.anexos[i].ID+')">  <i class="fa fa-pencil"></i></button>';

                    titulopopover= 'EXPEDIENTE';
                    contentpopover = 'Ver Expediente';
                    colortbn='yellow';

                    if(Object.keys(response.expedientes).length<1){
                        contentpopover = 'Sin archivar en Expediente';
                        colortbn='grey-salt';
                    }

                    html+='<button type="button" ' +
                        ' data-original-title="'+titulopopover+'" ' +
                        ' data-content="'+contentpopover+'" ' +
                        ' data-placement="top" data-trigger="hover" data-container="body" ' +
                        'class="btn btn-circle btn-sm '+colortbn+' popovers"   ' +
                        '> <i class="fa fa-folder-open-o"></i></button>';

                    titulopopover= 'ANULADO';
                    contentpopover = 'Radicado Anulado';
                    colortbn='red';
                    /**
                     *Si fue anulado, entonces muestro el boton, si no, no.
                     */

                    if(Object.keys(response.anulado).length>0){
                        html+='<button type="button" class="btn btn-circle btn-sm red"> <i class="fa icon-ban"></i></button>';
                    }

                    html+="</td>";
                    html += ' </tr>';

                    html+= armarContApoSupRowVacio(response,2,radicado);
                }

                if(Object.keys(response.anexos).length===1){
                    html+= armarContApoSupRowVacio(response,2,radicado);
                }

                if(Object.keys(response.anexos).length===3){
                    html+= armarContApoSupRowVacio(response,1,radicado);
                }
            }else{
                html += "<tr>";
                html+= armarHistorialOp(response);
                html+='<td colspan="2" ></td>';
                html+="</tr>";

                html+="<tr>";
                html += armarVerHistoricoCompleto(radicado);
                html+='<td colspan="2" ></td>';
                html+="</tr>";

            }


            html += "</table>";

            return html;
        }

        function mostrarinfor(radicado, dependencia, usuarioactual, contratista_identifi,dependActualNum) {
            //muestra la informacion de la tabla principal
            var resultado = "";
            $.ajax({
                url: "process.php",
                method: "POST",
                data: {radicado: radicado, operation: "inforadicado", contratista_identifi: contratista_identifi},
                dataType: "json",
                async: false,
                success: function (response) {

                    if(Object.keys(response.anexos).length>0){
                        radi_padre= response.anexos[0].RADI_NUME_SALIDA
                    }

                    if(Object.keys(response.asuntosRadicados).length>0){
                        asuntosRadicados= response.asuntosRadicados;
                    }
                    //armo el html del detalle
                    resultado = armarInfo(response, dependencia, usuarioactual, radicado,dependActualNum)// resultDropdown.html();

                }
            }).done(function () {
            }).fail(function (error) {

            });
            return resultado;
        }

        function swicheTodos() {
            $('#radiotodos').bootstrapSwitch({
                //al cambiar de swiche
                onSwitchChange: function (e, state) {
                    swichetodos = state;
                    tablaAdmin();
                },
                state: false
            });
            swichetodos == false;

        }

        function borrarimg(nombrearchivo, rutaimg, rutathumbnail) { //borra los arcivos del OP al presionar el boton rojo de Eliminar

            if (nombrearchivo != undefined) {
                $.ajax({
                    url: "process.php",
                    method: "POST",
                    data: {
                        radicado: radicadoselected, //numero de radicado seleccionado
                        ruta: rutaimg,
                        rutathumbnail: rutathumbnail,
                        nombrearchivo: nombrearchivo,
                        operation: "borrarFileOp"
                    },
                    dataType: "json",
                    success: function (response) {

                        if (response.success == undefined) {
                            Swal.fire("Error", response.error, "error");
                        }
                    }
                }).done(function () {
                }).fail(function (error) {
                    Swal.fire("Error", "Ha ocurrido un error", "error");
                });
            } else {
                Swal.fire("Error", "Ha ocurrido un error, el archivo no existe", "error");
            }
        }

        function cerrarModalDescargarOp(){

            $('#modaldescargarop').modal('hide');
        }
        function cerrarModalSubirOp() {

            //esto es al querer cerrar el modal de subida de OP, por si tiene archivos que no ha subido,
            //le arroje una alerta
            if ($("#formanexo > table").find(".start").length > 0) {

                Swal.fire({
                        title: "Alerta",
                        text: "No ha subido todos los archivos, desea:",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonText: "Cerrar",
                        cancelButtonText: "Continuar aquí",
                        closeOnConfirm: true,
                        closeOnCancel: true
                    }).then((result) => {
                    if (result.value) {
                    $('#modalsubirop').modal('hide');
                } else if (
                    result.dismiss === Swal.DismissReason.cancel
                ) {

                }
            });
            } else {
                $('#modalsubirop').modal('hide');
            }

        }
        ////////

        $(document).ready(function () {

            
            if (configsHasValue['TAB_OP_VISIBLE'] != 'VISIBLE'){
                $('#tab_ordenpago').hide();
                $('#ordenes_pago').removeClass('active');
                $('#ordenes_pago').hide();
                $('#returned_payment').addClass('active'); 
            }

            jQuery('#modalsubirop').on('hidden.bs.modal', function (e) {
                //cuando se cierra el modal de anexos
               OrdenesDePago.refrescarTablaOp();
            });

            jQuery('#modalListaContratista').on('hidden.bs.modal', function (e) {
                //cuando se cierra el modal de administracion
                swicheselected = swicheanteriorAdmin;
            });

            jQuery('#modal_asignarsuper').on('hidden.bs.modal', function (e) {
                //cuando se cierra el modal de asignar supervisores, muestro la lista de contrstistas
                $("#modalAsignaSuper").modal('show');
            });
            


            swicheTodos();
            //los swiche
            $('input:radio').bootstrapSwitch({
                //al cambiar de swiche
                onSwitchChange: function (e, state) {

                    if (e.currentTarget.value == "Financiera" && financiera < 1) {
                        Swal.fire("Alerta", "No tiene permiso para ver esta opción", "warning");
                        $("#" + swicheselected).bootstrapSwitch('state', true);
                        e.preventDefault();
                        return false;
                    }
                    if (e.currentTarget.value == "Supervisor" && supervisor < 1) {
                        Swal.fire("Alerta", "No tiene permiso para ver esta opción", "warning");
                        $("#" + swicheselected).bootstrapSwitch('state', true);
                        e.preventDefault();
                        return false;
                    }

                    if (e.currentTarget.value == "Contratista" && contratista < 1) {
                        Swal.fire("Alerta", "Usted No es Contratista o no ha radicado pagos", "warning");
                        $("#" + swicheselected).bootstrapSwitch('state', true);
                        e.preventDefault();
                        return false;
                    }
                    swicheselected = e.currentTarget.value;
                    if (swicheselected == "Contratista" || swicheselected == "Financiera" || swicheselected == "Supervisor") {
                        if ($("#datefromop").val() != "" && $("#datetoop").val() != "") {
                            tablaprincipalOP();
                        }
                    } else {
                        /* $('#beforetableopcontainer').removeClass('hide');
                         tablaAdmin();*/
                    }
                }
            });


            /*
             * Evento click para mostrar filtros de fechas en Prdees de Pago OP
             */
            $("#rangofechaop").click(function () {
                var date_form = $("#formfechaop");

                if (date_form.hasClass('hide')) {
                    date_form.removeClass('hide');
                } else {
                    date_form.addClass('hide');
                    $("#formfechaop")[0].reset();
                }

            });

            $('#formfechaop').on('submit', function (e) {
                e.preventDefault();

                if (swicheselected == "Financiera" && financiera < 1) {
                    Swal.fire("Alerta", "No tiene permiso para ver esta opción", "warning");
                    e.preventDefault();
                    return false;
                }
                if (swicheselected == "Supervisor" && supervisor < 1) {
                    Swal.fire("Alerta", "No tiene permiso para ver esta opción", "warning");
                    e.preventDefault();
                    return false;
                }

                if (swicheselected == "Contratista" && contratista < 1) {
                    Swal.fire("Alerta", "No tiene permiso para ver esta opción", "warning");
                    e.preventDefault();
                    return false;
                }


                if ($('#datefromop').val().length == 0 || $('#datetoop').val().length == 0) {
                    return false;
                }

                if (swicheselected == "") {
                    Swal.fire("Alerta", "Debe seleccionar alguna opción", "warning");
                    return false;
                }

                tablaprincipalOP();
            });


            ////////////////////////////////////////////////////////////////////////////////////////
            var from = moment().startOf('month').format('DD-M-YYYY HH:mm:ss');
            var to = moment().format('DD-M-YYYY HH:mm:ss');
            var returned_to = $(".returned-to");
            var returned_from = $(".returned-from");
            var month_to = $(".month-to");
            var month_from = $(".month-from");

            returned_to.datetimepicker({
                autoclose: true,
                isRTL: App.isRTL(),
                format: "dd-mm-yyyy hh:ii:ss",
                pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left"),
                initialDate: to,
                language: 'es'
            });
            returned_from.datetimepicker({
                autoclose: true,
                isRTL: App.isRTL(),
                format: "dd-mm-yyyy hh:ii:ss",
                pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left"),
                initialDate: from,
                language: 'es'
            }).on('changeDate', function (ev) {
                $('.returned-to').datetimepicker('setStartDate', ev.date);
            });
            month_to.datetimepicker({
                autoclose: true,
                isRTL: App.isRTL(),
                format: "dd-mm-yyyy hh:ii:ss",
                pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left"),
                initialDate: to,
                language: 'es'
            });
            month_from.datetimepicker({
                autoclose: true,
                isRTL: App.isRTL(),
                format: "dd-mm-yyyy hh:ii:ss",
                pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left"),
                initialDate: from,
                language: 'es'
            }).on('changeDate', function (ev) {
                $('.month-to').datetimepicker('setStartDate', ev.date);
            });
            $('#returned-to').val(to);
            $('#returned-from').val(from);
            $('#month-to').val(to);
            $('#month-from').val(from);

            //este es el swiche por defecto
            if (swicheselected != "" && swicheselected != "Administracion") { //lo marco visualmente
                $("#" + swicheselected).bootstrapSwitch('state', true);
            }
            //mandoa buscar los datos dependiedo del seleccionado
            if (swicheselected == "Financiera" || swicheselected == "Supervisor" || swicheselected == "Contratista") {

                tablaprincipalOP();

            } else if (swicheselected == "Administracion") {
                tablaAdmin();
            }


        });  //end del document.ready

        $('#returned-date-form').on('submit', function (e) {
            e.preventDefault();
            if ($('#returned-from').val().length == 0 || $('#returned-to').val().length == 0) {
                return false;
            }
            get_returned_payments();
        });

        $('#month-date-form').on('submit', function (e) {
            e.preventDefault();
            if ($('#month-from').val().length == 0 || $('#month-to').val().length == 0) {
                return false;
            }
            get_month_payments();
        });

        /*
         * Función que permite ejecutar la busqueda de registros con filtro de fechas
         * para reporte de pagos devueltos
         */
        function get_returned_payments() {

            var from = $('#returned-from').val();
            var to = $('#returned-to').val();
            $('#returned_table_container').addClass('hide');
            $('#loader_returned_container').removeClass('hide');
            $.ajax({
                url: "process.php",
                method: "POST",
                data: {from: from, to: to, operation: "returned_payments"},
                dataType: "html",
                success: function (response) {
                    $("#returned_payment_table").bootstrapTable('destroy');
                    var ret_pay_cont = $("#returned_table_container");
                    if (response != "ERROR") {
                        ret_pay_cont.html('');
                        ret_pay_cont.html(response);
                        $('#loader_returned_container').addClass('hide');
                        ret_pay_cont.removeClass('hide');
                    }
                }
            }).done(function () {
                var returned_payment_table = $("#returned_payment_table");
                returned_payment_table.bootstrapTable();
            }).fail(function (error) {

            });
        }

        /*
         * Función que permite ejecutar la busqueda de registros con filtro de fechas
         * para reporte de pagos del mes
         */
        function get_month_payments() {
            var from = $('#month-from').val();
            var to = $('#month-to').val();
            $('#month_table_container').addClass('hide');
            $('#loader_month_container').removeClass('hide');
            $.ajax({
                url: "process.php",
                method: "POST",
                data: {from: from, to: to, operation: "month_payments"},
                dataType: "html",
                success: function (response) {
                    $("#month_payment_table").bootstrapTable('destroy');
                    var ret_month_cont = $("#month_table_container");
                    if (response != "ERROR") {
                        ret_month_cont.html('');
                        ret_month_cont.html(response);
                        $('#loader_month_container').addClass('hide');
                        ret_month_cont.removeClass('hide');
                    }
                }
            }).done(function () {
                var month_payment_table = $("#month_payment_table");
                month_payment_table.bootstrapTable();
            }).fail(function (error) {
                console.log(error);
            });
        }

        /*
         * Evento click para mostrar filtros de fechas
         */
        $("#returned-filter-toggle").click(function () {
            var date_form = $("#returned-date-form");
            if (date_form.hasClass('hide')) {
                date_form.removeClass('hide');
            } else {
                date_form.addClass('hide');
                $("#returned-date-form")[0].reset();
            }
        });

        $("#month-filter-toggle").click(function () {
            var date_form = $("#month-date-form");
            if (date_form.hasClass('hide')) {
                date_form.removeClass('hide');
            } else {
                date_form.addClass('hide');
                $("#month-date-form")[0].reset();
            }
        });

        /*
         * Función que renderiza vista de detalles de cada fila.
         */
        function detailReturned(index, row) {
            var html = [];
            var user = '';
            var depe = '';
            $.each(row, function (key, value) {
                if (key == 5) {
                    html.push('<p><b>Comentario de historial:</b> ' + value + '</p>');
                }
                if (key == 6) {
                    html.push('<p><b>Desde:</b> ' + value + '</p>');
                }
                if (key == 7) {
                    html.push('<p><b>Para:</b> ' + value + '</p>');
                }
                if (key == 9) {
                    user = value;
                }
                if (key == 8) {
                    depe = value;
                }
            });
            html.push('<p><b>Actualmente en:</b> ' + depe + ', ' + user + '</p>');
            return html.join('');
        }

        /*
         * Función que muestra el modal con información del radicado
         */
        $(document).on('click', '.get_info', function () {
            var rad_num = $(this).data('rad-num');
            $.ajax({
                url: "process.php",
                method: "POST",
                data: {rad_num: rad_num, operation: "get_radicated_info"},
                dataType: "html",
                success: function (response) {
                    if (response != "ERROR") {
                        var radicated_info = $('#radicated_info');
                        radicated_info.html('');
                        radicated_info.html(response);
                        $('#month_report_modal').modal('show');
                    }
                }
            }).done(function () {
            }).fail(function (error) {
                console.log(error);
            });
        });

        $(document).on('click', '.open-img', function (e) {
            e.preventDefault();
            var img_url = $(this).attr('href');
            $('#rad_img_modal .modal-body img').attr('src', img_url);
            $('#rad_img_modal').modal('show');
        });
    </script>
    <script src="../../js/funtionImage.js" type="text/javascript"></script>
@endsection