@if(\App\Helpers\PazYSalvo::loan_state($user_data['USUARIO']) == 1)
<div class="note note-warning hidden-print">
    <h4 class="block">Atención</h4>
    <p> Existen solicitudes de documentos por parte de este usuario, los cuales No fueron entregados. Dichas solicitudes deben ser canceladas por Gestión Documental.</p>
</div>
@endif
@if(\App\Helpers\PazYSalvo::userIsActive($user_data['USUARIO']) == 0)
    <div class="note note-danger hidden-print">
        <h4 class="block">Atención</h4>
        <p><strong> Este usuario se encuentra inactivo. </strong></p>
    </div>
@endif
<div class="row">
    <div class="col-xs-12">
        <a class="btn default btn-outline pull-left hidden-print show-search">Nueva Busqueda</a>
    </div>
</div>
<div class="row invoice-logo margin-top-10">
    <div class="col-xs-6 invoice-logo-space">
        <img src="<?php  echo $_SESSION['base_url'].'png/logo2_'.strtolower($_SESSION['entidad'])?>.jpg" width="130" height="150"  alt="" />

    </div>
    <div class="col-xs-6">
        <p> {{\Carbon\Carbon::now()->format('d-m-Y H:i:s')}}
            <span class="muted"> {{$_SESSION["entidad_largo"]}} </span>
        </p>
    </div>
</div>
<hr/>
<div class="row">
    <div class="col-xs-7">
        <h3 class="font-blue-dark"><strong>Datos de la Persona:</strong></h3>
        <ul class="list-unstyled">
            <li> <strong>Nombre:</strong> {{$user_data['NOMBRE']}} </li>
            <li> <strong>Usuario:</strong> {{$user_data['USUARIO']}} </li>
            <li> <strong>Identificación:</strong> {{$user_data['DOCUMENTO']}} </li>
            <li> <strong>Correo electrónico:</strong> {{$user_data['EMAIL']}} </li>
            <li> <strong>Dependencia actual:</strong> {{$user_data['DEPENDENCIA']}} </li>
        </ul>
    </div>
    <div class="col-xs-5">
        <h3 class="font-blue-dark"><strong>Perfil:</strong></h3>
        <ul class="list-unstyled">
            <li> <strong>Usuario Jefe:</strong> {{($user_data['ROL'] == 1)? 'SI' : 'NO'}} </li>
            <li> <strong>Nivel:</strong> {{$user_data['NIVEL']}} </li>
            <li> <strong>Último ingreso a Orfeo:</strong> {{\Carbon\Carbon::parse($user_data['ULTIMO_INGRESO'])->format('d-m-Y H:i:s')}} </li>
            <li> <strong>Estado:</strong>
                @if(\App\Helpers\PazYSalvo::userIsActive($user_data['USUARIO']) == 1)
                    Activo
                @else
                    Inactivo
                @endif
            </li>
        </ul>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th> # </th>
                <th> Tipo </th>
                <th> Descripción </th>
                <th> Cantidad </th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td> 1 </td>
                <td> Radicados </td>
                <td> Radicados en carpetas del usuario sin tramitar </td>
                <td> {{$user_data['RADICADOS']}} </td>
            </tr>
            <tr>
                <td> 2 </td>
                <td> Préstamos </td>
                <td> Radicados físicos solicitados en préstamo y que no han sido devueltos </td>
                <td> {{$user_loans}} </td>
            </tr>
            <tr>
                <td> 3 </td>
                <td> Informados </td>
                <td> Radicados informados actualmente (Para el Paz y Salvo, no requiere estar en 0) </td>
                <td> {{$user_data['INFORMADOS']}} </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-xs-4">
        <div class="well">
            <address>
                <br/> {{$_SESSION['usua_nomb']}}
                <br/> {{$_SESSION['depe_nomb']}}
            </address>
        </div>
    </div>
    <div class="col-xs-8 invoice-block">
        <ul class="list-unstyled amounts">
            <li class="font-lg">
                <strong>
                    Paz y Salvo:
                    @if($user_data['RADICADOS'] == 0 && $user_loans == 0)
                        <span class="font-blue-steel">SI</span>
                    @elseif($user_data['RADICADOS'] > 0 || $user_loans > 0)
                        <span class="font-red-thunderbird">NO</span>
                    @endif
                </strong>
            </li>
        </ul>
        <br/>
        @if($user_data['RADICADOS'] == 0 && $user_loans == 0 && \App\Helpers\PazYSalvo::userIsActive($user_data['USUARIO']) == 1)
            <a class="btn red btn-outline hidden-print margin-bottom-5" usr="{{$user_data['USUARIO']}}" id="deactivate-user" data-title="¿Desactivar Usuario?" data-popout="true" data-btn-cancel-label="No" data-btn-ok-label="Si" data-placement="left" data-on-confirm="deactivateUser">Desactivar Usuario</a>
        @endif
        <a class="btn btn-lg dark hidden-print margin-bottom-5" href="process.php?op=export&user={{urlencode($user_data['NOMBRE']." - ".$user_data['USUARIO'])}}" target="_blank"> Exportar
        <i class="fa fa-file-pdf-o"></i>
        </a>
        <a class="btn btn-lg blue hidden-print margin-bottom-5" onclick="print_page();"> Imprimir
            <i class="fa fa-print"></i>
        </a>
        <a class="btn btn-lg green hidden-print margin-bottom-5 show-search"> Cancelar </a>
    </div>
</div>