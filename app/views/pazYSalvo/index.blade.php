@extends('layouts.app')

@section('content')
<div class="note note-success hide" id="user-deactivated">
    <h4 class="block">Usuario ":user de :dep" desactivado exitosamente</h4>
</div>
<div class="search-page search-content-2">
    <div class="search-bar bordered">
        <div class="row">
            <div class="col-md-12">
                <div class="input-group">
                    <input type="text" class="form-control typeahead" placeholder="Buscar usuario por nombre, login, documento o dependencia...">
                    <span class="input-group-btn">
                        <button class="btn blue uppercase bold" type="button" id="search-user">Aceptar</button>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="invoice">
</div>
@endsection

@section('page-modal')
@endsection

@section('page-css')
    <link href="{{$include_path}}pages/css/search.min.css" rel="stylesheet" type="text/css" />
    <link href="{{$include_path}}pages/css/invoice.min.css" rel="stylesheet" type="text/css" />
    <link href="{{$include_path}}global/plugins/typeahead/typeahead.css" rel="stylesheet" type="text/css" />
    <style>
        .tt-input{
            font-size: 20px;
        }
    </style>
@endsection

@section('page-js')
    <script src="{{$include_path}}global/plugins/typeahead/handlebars.min.js" type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/typeahead/typeahead.bundle.min.js" type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js" type="text/javascript"></script>
    <script>
        var users = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace    ,
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: "process.php?operation=user&key=%QUERY",
                filter: function(x) {
                    return $.map(x, function(item) {
                        return {nombre: item.NOMBRE, login: item.LOGIN, depe: item.DEPE};

                    });
                },
                wildcard: "%QUERY"
            }
        });
        users.initialize();
        var typeahead = $('.typeahead');
        typeahead.typeahead({
                    hint: false,
                    highlight: false,
                    minLength: 3
                },
                {
                    name: 'nombre',
                    source: users.ttAdapter(),
                    display: function(s){
                        return s.nombre +' - '+ s.login +' - '+ s.depe;
                    },
                    limit: 20
                });
        $(document).on('click', '#search-user', function(){
            var user    = typeahead.typeahead('val');
            $('#user-deactivated h4').text('Usuario ":user de :dep" desactivado exitosamente');
            var msg = $('#user-deactivated h4').text();
            var s = user.split('-');
            msg = msg.replace(':user', s[0]);
            msg = msg.replace(':dep', s[2]);
            $('#user-deactivated h4').text(msg);
            if(user.length == 0 || user.length < 3){
                return false;
            }
            var selected= false;
            $('.tt-suggestion.tt-selectable').each(function(){
                if(user === $(this).text()){
                    selected= true;
                }
            });
            if(!selected){
                typeahead.typeahead('val', '');
                return false;
            }
            $.ajax({
                url: "process.php",
                method: "POST",
                data: {user:user, operation:"get_user_resume"},
                dataType: "html",
                success: function(response) {
                    if(response != "ERROR") {
                        var user_resume= $('.invoice');
                        user_resume.html('');
                        user_resume.html(response);
                        user_resume.removeClass('hide');
                        $('#user-deactivated').addClass('hide');
                        $('.search-page.search-content-2').addClass('hide');
                    }
                }
            }).done(function(){
                $('#deactivate-user').confirmation();
            }).fail(function( jqXHR, textStatus ) {
                alert( "Request failed: " + textStatus );
            });
        });

        $(document).on('click', '#deactivate-user', function(e){
            e.preventDefault();
            return false;
        });

        $(document).on('click', '.show-search', function(){
            typeahead.typeahead('val', '');
            $('.search-page.search-content-2').removeClass('hide');
            $('.invoice').html('').addClass('hide');
        });

        typeahead.bind('typeahead:change', function(ev) {
            var selected= false;
            var value= ev.target.value;
            $('.tt-suggestion.tt-selectable').each(function(){
               if(value === $(this).text()){
                   selected= true;
               }
            });
            if(!selected){
                typeahead.typeahead('val', '');
            }
        });

        function print_page()
        {
            window.print();
        }

        function deactivateUser() {
            var user = $('#deactivate-user').attr('usr');
            $.ajax({
                url: "process.php",
                method: "POST",
                data: {user:user, operation:"deactivate_user"},
                dataType: "html",
                success: function(response) {
                    if(response != "ERROR") {
                        typeahead.typeahead('val', '');
                        $('.search-page.search-content-2').removeClass('hide');
                        $('.invoice').html('').addClass('hide');
                        if (response == "TRUE") {
                            $('#user-deactivated').removeClass('hide');
                        }
                    }
                }
            }).done(function(){
            }).fail(function( jqXHR, textStatus ) {
                alert( "Request failed: " + textStatus );
            });
        }
    </script>
@endsection