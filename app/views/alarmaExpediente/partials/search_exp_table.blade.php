<div class="search-table table-responsive">
    <table id="alert-table"
           data-toggle="table"
           data-striped="true"
           data-pagination="true"
           data-page-size="10"
           data-locale="es-SP">
        <thead class="bg-blue font-white bold" style="height: 60px;">
        <tr>
            <th data-width="16%" data-align="center" data-valign="middle" data-sortable="true" >Alerta</th>
            <th data-width="37%" data-align="center" data-valign="middle" data-sortable="true">Asunto</th>
            <th data-width="37%" data-align="center" data-valign="middle" data-sortable="true">Expediente</th>
            <th data-width="10%" data-align="center" data-valign="middle" data-sortable="true">Archivado</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($data as $key => $row)
            <tr>
                <td>
                    @if ($row['ALERTA'] == 0 || $row['ALERTA'] == 1)
                        <i class="fa fa-2x fa-share font-green-jungle"></i>
                        <i class="fa fa-2x fa-folder-open-o font-blue"></i><br>
                        <span class="font-blue font-lg bold">Incluido</span><br>
                        <span class="font-grey-silver">Hace: {{\App\Helpers\AlarmaExpediente::getRecordTime($row['FECHA_ALERTA'])}}</span><br>
                        <span>{{\Carbon\Carbon::parse($row['FECHA_ALERTA'])->format('d-m-Y')}}</span>
                    @elseif ($row['ALERTA'] == 2)
                        <i class="fa fa-2x fa-reply font-yellow-gold"></i>
                        <i class="fa fa-2x fa-folder-open-o font-grey-silver"></i><br>
                        <span class="font-blue font-lg bold">Excluido</span><br>
                        <span class="font-grey-silver">Hace: {{\App\Helpers\AlarmaExpediente::getRecordTime($row['FECHA_ALERTA'])}}</span><br>
                        <span>{{\Carbon\Carbon::parse($row['FECHA_ALERTA'])->format('d-m-Y')}}</span>
                    @else
                        <i class="fa fa-2x fa-folder-open-o font-grey-silver"></i><br>
                        <span class="font-grey-silver">Hace: {{\App\Helpers\AlarmaExpediente::getRecordTime($row['FECHA_ALERTA'])}}</span><br>
                        <span>{{\Carbon\Carbon::parse($row['FECHA_ALERTA'])->format('d-m-Y')}}</span>
                    @endif
                </td>
                <td>
                    <p>
                        <span class="font-blue bold">{{$row['RADICADO']}} de {{\App\Helpers\AlarmaExpediente::getRadicatedDate($row['FECHA_RADICADO'])}}</span><br>
                        <span class="font-grey-silver">{{$row['ASUNTO']}}</span>
                    </p>
                </td>
                <td>
                    <span class="font-blue bold">
                        <a href="../../verradicado.php?verrad={{$row['RADICADO']}}&{{session_name()}}={{session_id()}}&krd={{$_SESSION['usua_login']}}&menu_ver_tmp=4" target="_blank">
                            {{$row['NUMERO_EXPEDIENTE']}}
                        </a>
                    </span>
                    <br>
                    <span class="font-grey-silver">{{$row['DESCRIPCION']}}</span>
                </td>
                <td>
                    @if (!is_null($row['USUARIO_CREA']) && !is_null($row['USUARIO_ACTUALIZA']))
                        <span class="bold">Archivado:</span><br>
                        <span class="font-grey-silver">{{$row['USUARIO_CREA']}}</span><br>
                        <span class="font-grey-silver">{{$row['FECHA_CREACION']}}</span><br>
                        <span class="bold">Actualizado:</span><br>
                        <span class="font-grey-silver">{{$row['USUARIO_ACTUALIZA']}}</span><br>
                        <span class="font-grey-silver">{{$row['FECHA_ACTUALIZACION']}}</span>
                    @elseif (!is_null($row['USUARIO_CREA']))
                        <span class="bold">{{$row['USUARIO_CREA']}}</span><br>
                        <span class="font-grey-silver">{{$row['FECHA_CREACION']}}</span><br>
                    @elseif (is_null($row['USUARIO_CREA']))
                        <span class="bold">Sin Archivar</span><br>
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>