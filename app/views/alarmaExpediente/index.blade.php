@extends('layouts.app')

@section('content')
<div class="alert bg-yellow-crusta text-center">
    <strong class="font-blue-steel">ALARMAS DE EXPEDIENTES</strong>
</div>
<div class="search-page search-content-4">
    <div class="row hide" id="seach-filter-loader" style="height: 130px">
        <div class="col-md-2 col-md-offset-5 text-center margin-top-40">
            <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
        </div>
    </div>
    <div class="row" id="search-filter-container">
        <div class="col-md-12">
            <div class="search-bar bordered">
                <form action="#" class="form-horizontal">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3 bold font-blue-sharp font-lg">Incluidos</label>
                            <div class="col-md-3">
                                <input type="checkbox" id="included" class="make-switch" checked data-on-color="primary" data-off-color="default">
                            </div>
                            <label class="control-label col-md-3 bold font-blue-sharp font-lg">Excluidos</label>
                            <div class="col-md-3">
                                <input type="checkbox" id="excluded" class="make-switch" checked data-on-color="primary" data-off-color="default">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-offset-9 col-md-3 margin-bottom-10">
            <form class="sidebar-search" id="search-exp-alarm" action="process.php" method="POST">
                <div class="input-group">
                    <input type="text" id="query" class="form-control" placeholder="Buscar...">
                    <span class="input-group-btn">
                        <a href="javascript:;" class="btn blue submit">
                            <i class="icon-magnifier"></i>
                        </a>
                    </span>
                </div>
            </form>
        </div>
    </div>
    <div id="search-exp-results">
        @include('alarmaExpediente.partials.search_exp_table')
    </div>
</div>
@endsection

@section('page-modal')
@endsection

@section('page-css')
    <link href="{{$include_path}}global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css" />
    <link href="{{$include_path}}pages/css/search.min.css" rel="stylesheet" type="text/css" />
    <link href="{{$include_path}}global/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet" type="text/css" />
    <link href="{{$include_path}}global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
@endsection

@section('page-js')
    <script src="{{$include_path}}global/plugins/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/bootstrap-table/bootstrap-table.min.js" type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/bootstrap-table/locale/bootstrap-table-es-SP.js" type="text/javascript"></script>
    <script src="{{$include_path}}global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <script>
        $(document).ready(function(){
            setInterval(search_exp, 300000);
        });
        $(document).on('submit', '#search-exp-alarm', function(e){
            e.preventDefault();
            $('#seach-filter-loader').removeClass('hide');
            $('#search-filter-container').addClass('hide');
            $('#included').bootstrapSwitch('disabled', true);
            $('#excluded').bootstrapSwitch('disabled', true);
            $('#search-exp-alarm a').addClass('disabled');
            search_exp();
        });

        $('#included').on('switchChange.bootstrapSwitch', function(event, state) {
            $('#seach-filter-loader').removeClass('hide');
            $('#search-filter-container').addClass('hide');
            $('#included').bootstrapSwitch('disabled', true);
            $('#excluded').bootstrapSwitch('disabled', true);
            search_exp();
        });

        $('#excluded').on('switchChange.bootstrapSwitch', function(event, state) {
            $('#seach-filter-loader').removeClass('hide');
            $('#search-filter-container').addClass('hide');
            $('#included').bootstrapSwitch('disabled', true);
            $('#excluded').bootstrapSwitch('disabled', true);
            search_exp();
        });

        function search_exp(){
            var query   = $("#query").val();
            var included= $('#included').bootstrapSwitch('state');
            var excluded= $('#excluded').bootstrapSwitch('state');
            $.ajax({
                url: "process.php",
                method: "POST",
                data: {
                    query:query,
                    included:included,
                    excluded:excluded,
                    operation:"search-exp"
                },
                dataType: "html",
                success: function(response) {
                    $("#alert-table").bootstrapTable('destroy');
                    var search_exp= $("#search-exp-results");
                    if(response != "ERROR") {
                        search_exp.html('');
                        search_exp.html(response);
                    }
                }
            }).done(function(){
                var alert_table= $("#alert-table");
                alert_table.bootstrapTable();
                $('#included').bootstrapSwitch('disabled', false);
                $('#excluded').bootstrapSwitch('disabled', false);
                $('#seach-filter-loader').addClass('hide');
                $('#search-filter-container').removeClass('hide');
                $('#search-exp-alarm a').removeClass('disabled');
            }).fail(function(error){
                console.log(error);
                $('#seach-filter-loader').addClass('hide');
                $('#search-filter-container').removeClass('hide');
            });
        }
    </script>
@endsection