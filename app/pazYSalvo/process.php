<?php

use App\Helpers\PDOconfig;
use Aura\SqlQuery\QueryFactory;
use Philo\Blade\Blade;

require '../../vendor/autoload.php';

try {
    if(array_key_exists('operation', $_GET) && $_GET['operation'] == 'user')
    {
        $response= get_user($_GET['key']);
    }
    if(array_key_exists('op', $_GET) && $_GET['op'] == 'export') {
        $response = export_pdf($_GET['user']);
    }
    if($_POST['operation'] == "returned_payments") {
        $response = get_returned_payments($_POST['from'], $_POST['to']);
    }
    if($_POST['operation'] == "get_user_resume"){
        $response = get_user_resume($_POST['user']);
    }
    if($_POST['operation'] == "deactivate_user"){
        $response = deactivate_user($_POST['user']);
    }
}catch(Exception $e){
    echo "Error: ".$e->getMessage();
    $response= "ERROR";
}
echo $response;
exit;

function get_user($query)
{
    try{
        require_once '../../config.php';
        $views  = $ABSOL_PATH . 'app/views';
        $cache  = $ABSOL_PATH . 'app/cache';

        //Clearing the views cache
        \App\Helpers\MiscFunctions::clear_view_cache();

        $blade = new Blade($views, $cache);

        $pazysalvo_repo = new \App\Helpers\PazYSalvo();
        $response       =  $pazysalvo_repo->get_user($query);

        return json_encode($response);

    }catch(\Exception $e){
        return $e->getMessage();
    }
}

function get_user_resume($user)
{
    try{

        require_once '../../config.php';
        $views  = $ABSOL_PATH . 'app/views';
        $cache  = $ABSOL_PATH . 'app/cache';

        //Clearing the views cache
        \App\Helpers\MiscFunctions::clear_view_cache();

        $blade = new Blade($views, $cache);

        $pazysalvo_repo = new \App\Helpers\PazYSalvo();
        $user_data      =  $pazysalvo_repo->get_user_data($user);
        $user_loans     =  $pazysalvo_repo->get_loans($user);

        return $blade->view()->make('pazYSalvo.partials.user_resume',
            compact('user_data', 'user_loans')
        )->render();

    }catch(Exception $e){
        return $e->getMessage();
    }
}

function deactivate_user($user) {
    include_once "../../config.php";
    include_once "../../include/db/ConnectionHandlerNew.php";
    $db = new \ConnectionHandler("../..");
    $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
    session_start();

    //Updating the user state
    $query = "UPDATE usuario SET usua_esta='0' WHERE usua_login='$user'";
    $resultDown = $db->conn->execute($query);

    if ($resultDown) {
        //Retrieving the user
        $query = "SELECT
                u.USUA_CODI,
                u.DEPE_CODI,
                u.USUA_DOC,
                U.USUA_LOGIN
                FROM USUARIO u
                WHERE USUA_LOGIN='$user'";
        $user = $db->conn->GetRow($query);

        //Inserting registries on user history table
        $query = "INSERT INTO SGD_USH_USUHISTORICO
          (SGD_USH_ADMCOD,
          SGD_USH_ADMDEP,
          SGD_USH_ADMDOC,
          SGD_USH_USUCOD,
          SGD_USH_USUDEP,
          SGD_USH_USUDOC,
          SGD_USH_MODCOD,
          SGD_USH_FECHEVENTO,
          SGD_USH_USULOGIN)
          VALUES
          (" . $_SESSION["codusuario"] . ",
          " . $_SESSION["dependencia"] . ",
          '" . $_SESSION["usua_doc"] . "',
          " . $user['USUA_CODI'] . ",
          " . $user['DEPE_CODI'] . ",
          '" . $user['USUA_DOC'] . "',
          9,
          TO_TIMESTAMP('" . date("Y-m-d H:i:s") . "','YYYY-MM-DD HH24:MI:SS'),
          '" . $user['USUA_LOGIN'] . "'
          )";

        $result = $db->conn->execute($query);

        $query = "INSERT INTO SGD_USH_USUHISTORICO
          (SGD_USH_ADMCOD,
          SGD_USH_ADMDEP,
          SGD_USH_ADMDOC,
          SGD_USH_USUCOD,
          SGD_USH_USUDEP,
          SGD_USH_USUDOC,
          SGD_USH_MODCOD,
          SGD_USH_FECHEVENTO,
          SGD_USH_USULOGIN)
          VALUES
          (" . $_SESSION["codusuario"] . ",
          " . $_SESSION["dependencia"] . ",
          '" . $_SESSION["usua_doc"] . "',
          " . $user['USUA_CODI'] . ",
          " . $user['DEPE_CODI'] . ",
          '" . $user['USUA_DOC'] . "',
          2,
          TO_TIMESTAMP('" . date("Y-m-d H:i:s") . "','YYYY-MM-DD HH24:MI:SS'),
          '" . $user['USUA_LOGIN'] . "'
          )";

        $result = $db->conn->execute($query);

        return 'TRUE';
    }
        return 'FALSE';
}

function export_pdf($user)
{
    try{
        require_once '../../config.php';
        $views  = $ABSOL_PATH . 'app/views';
        $cache  = $ABSOL_PATH . 'app/cache';

        //Clearing the views cache
        \App\Helpers\MiscFunctions::clear_view_cache();

        $blade = new Blade($views, $cache);

        $pazysalvo_repo = new \App\Helpers\PazYSalvo();
        $user_data      =  $pazysalvo_repo->get_user_data($user);
        $user_loans     =  $pazysalvo_repo->get_loans($user);

        $boss_user = ($user_data['ROL'] == 1)? 'SI' : 'NO';
        $state = (\App\Helpers\PazYSalvo::userIsActive($user_data['USUARIO']) == 1)? 'Activo':'Inactivo';
        $pyz = '';
        if ($user_data['RADICADOS'] == 0 && $user_loans == 0) {
            $pyz = '<span style="color:#4B77BE;font-weight: bold;font-size: 20px">SI</span>';
        }
        if ($user_data['RADICADOS'] > 0 || $user_loans > 0) {
            $pyz = '<span style="color:red;font-weight: bold;font-size: 20px">NO</span>';
        }

        // instantiate and use the dompdf class
        $dompdf = new mPDF();
        $dompdf->WriteHTML("
        <!DOCTYPE html>
        <html>
            <head>
                <title>Paz y Salvo - ".$user_data['NOMBRE']."</title>
                <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
            </head>
            <body>
                <table>
                    <tr>
                        <td width='30%'>
                            <img src='$include_path../../logoEntidad.png' alt=''/>
                        </td>
                        <td width='70%' align='right' style=''>
                            <p style='font-size:30px;margin-left:80%;'>".\Carbon\Carbon::now()->format('d-m-Y H:i:s')."</p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan='2' width='100%' align='right'>
                            <p style='font-size:20px;'>".$_SESSION['entidad_largo']."</p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan='2' width='100%'>
                            <hr>
                        </td>
                    </tr>
                    <tr>
                        <td width='100%' style='padding-top:10%'>
                            <p style='font-size:20px;font-weight:bold'>Datos de la Persona:</p>
                        </td>
                        <td width='100%' style='padding-top:10%'>
                            <p style='font-size:20px;font-weight:bold'>Perfil:</p>
                        </td>
                    </tr>
                    <tr>
                        <td width='100%'>
                            <p><strong>Nombre:</strong> ".$user_data['NOMBRE']."</p>
                        </td>
                        <td width='100%'>
                            <p><strong>Usuario jefe:</strong> $boss_user</p>
                        </td>
                    </tr>
                    <tr>
                        <td width='100%'>
                            <p><strong>Usuario:</strong> ".$user_data['USUARIO']."</p>
                        </td>
                        <td width='100%'>
                            <p><strong>Nivel:</strong> ".$user_data['NIVEL']."</p>
                        </td>
                    </tr>
                    <tr>
                        <td width='100%'>
                            <p><strong>Identificación:</strong> ".$user_data['DOCUMENTO']."</p>
                        </td>
                        <td width='100%'>
                            <p><strong>Último ingreso a Orfeo:</strong> ".\Carbon\Carbon::parse($user_data['ULTIMO_INGRESO'])->format('d-m-Y H:i:s')."</p>
                        </td>
                    </tr>
                    <tr>
                        <td width='100%'>
                            <p><strong>Correo electrónico:</strong> ".$user_data['EMAIL']."</p>
                        </td>
                        <td width='100%'>
                            <p><strong>Estado:</strong> ".$state."</p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan='2' width='100%'>
                            <p><strong>Dependencia actual:</strong> ".$user_data['DEPENDENCIA']."</p>
                        </td>
                    </tr>
                    <tr>
                        <td width='200%' colspan='2' style='padding-top:10%'>
                            <table width='100%' style='border:0;border-collapse: collapse;'>
                                <tr>
                                    <td width='10%'>#</td>
                                    <td width='20%'>Tipo</td>
                                    <td width='60%'>Descripción</td>
                                    <td width='10%'>Cantidad</td>
                                </tr>
                                <tr bgcolor='#DDDDDD'>
                                    <td width='10%'>1</td>
                                    <td width='20%'>Radicados</td>
                                    <td width='60%'>Radicados en carpetas del usuario sin tramitar</td>
                                    <td width='10%'>".$user_data['RADICADOS']."</td>
                                </tr>
                                <tr>
                                    <td width='10%'>2</td>
                                    <td width='20%'>Prestamos</td>
                                    <td width='60%'>Radicados físicos solicitados en préstamo y que no han sido devueltos</td>
                                    <td width='10%'>".$user_loans."</td>
                                </tr>
                                <tr bgcolor='#DDDDDD'>
                                    <td width='10%'>3</td>
                                    <td width='20%'>Informados</td>
                                    <td width='60%'>Radicados informados actualmente (Para el Paz y Salvo, no requiere estar en 0)</td>
                                    <td width='10%'>".$user_data['INFORMADOS']."</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td width='50%' style='padding-top:10%'>
                            <p>".$_SESSION['usua_nomb']."</p>
                            <p>".$_SESSION['depe_nomb']."</p>
                        </td>
                        <td width='50%' align='right' style='padding-top:10%'>
                            <p>Paz y Salvo: ".$pyz."</p>
                        </td>
                    </tr>
                </table>
            </body>
        </html>
        ");

        $dompdf->Output('pazYsalvo - '.$user_data['NOMBRE'], 'I');
        exit;
    }catch(Exception $e){
        return $e->getMessage();
    }
}

?>

