<?php

use Philo\Blade\Blade;

require '../../vendor/autoload.php';
require_once '../../config.php';

$views = $ABSOL_PATH.'app/views';
$cache = $ABSOL_PATH.'app/cache';

//Clearing the views cache
\App\Helpers\MiscFunctions::clear_view_cache();

$blade          = new Blade($views, $cache);
$reporte_pagos  = new \App\Helpers\ReportesPagos();

echo $blade->view()->make('pazYSalvo.index', compact('include_path'))->render();

?>