<?php 
session_start();

    $ruta_raiz = ".";
    if (!$_SESSION['dependencia'])
        header ("Location: $ruta_raiz/cerrar_session.php");

foreach ($_POST as $key => $valor) ${$key} = $valor;

$direcTor = "$ruta_raiz/bodega/plantillas/";
$archivo1 = $direcTor."combiSencilla.xml";
$archivo2 = $direcTor."combiMasiva.xml";
$archivo3 = $direcTor."plantillas.xml";

$plantilla_memo =$direcTor."Plantilla_Comunicacion_Oficial_Interna_v6.docx";
$plantilla_emp =$direcTor."Plantilla_Comunicacion_Oficial_Externa_EMPRESAS_v6.docx";
$plantilla_ciu =$direcTor."Plantilla_Comunicacion_Oficial_Externa_CIUDADANOS_v6.docx";
$plantilla_masiva =$direcTor."Plantilla_Comunicacion_Masiva_v6.odt";
$plantilla4 =$direcTor."Formato solicitud de desembolso.docx";
$plantilla5 =$direcTor."Formato solicitud de facturacion.docx";
$plantilla6 =$direcTor."Formato supervisión.docx";
$plantilla7 =$direcTor."solicitud CDP.docx";
$plantilla8 =$direcTor."Supervision_personas_juridicas.docx";

$plantilla101 =$direcTor."Plantilla_Comunicacion_Masiva_CCL_v6.odt";
$plantilla102 =$direcTor."Formato-ficha-ingreso-publicacion-almacen-v3.docx";
$plantilla103 =$direcTor."Formato_Listado_Destinatarios_Masiva.ods";

$fuente1 =	$ruta_raiz."/bodega/tools/Digitalizador.zip";

$doc    = new DOMDocument();


/*
if(file_exists($archivo3)){
    $doc->load($archivo3);
    $campos     = $doc->getElementsByTagName("campo");
    foreach($campos as $campo){
        $campTemp1 = $campo->getElementsByTagName("nombre");
        $campTemp2 = $campo->getElementsByTagName("ruta");
        $temp1     = $campTemp1->item(0)->nodeValue;
        $temp2     = $campTemp2->item(0)->nodeValue;
        
        $plantill  .= "&nbsp; &nbsp;<a href='".$direcTor.$temp2."'>".$temp1."</a><br/>";
    }
}else{
    $msg  .= " No se abrio el archivo $archivo3 generado desde la administracion de plantillas</br>";
}
/*
if(file_exists($archivo2)){
    $doc->load($archivo2);
    $campos     = $doc->getElementsByTagName("campo");
    foreach($campos as $campo){
        $campTemp = $campo->getElementsByTagName("nombre");
        $valor    = $campTemp->item(0)->nodeValue;
        $nombMa   .= empty($nombSe)? " &nbsp; &nbsp; $valor" : "&nbsp; &nbsp;  &nbsp; &nbsp; $valor ";
    }
}else{
    $msg  .= " No se abrio el archivo $archivo2 generado desde la administracion de plantillas</br>";
}

if(file_exists($archivo1)){
    $doc->load($archivo1);
    $campos     = $doc->getElementsByTagName("campo");
    foreach($campos as $campo){
        $campTemp = $campo->getElementsByTagName("nombre");
        $valor    = $campTemp->item(0)->nodeValue;
        $nombSe   .= empty($nombSe)? "&nbsp; &nbsp;$valor" : " &nbsp; &nbsp; $valor ";
    }
}else{
    $msg  .= " No se abrio el archivo $archivo1 generado desde la administracion de plantillas</br>";
}
*/
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <title> :) Plantillas A Usar en Orfeo :)</title>
    <link rel="stylesheet" href="<?=$ruta_raiz."/estilos/".$_SESSION["ESTILOS_PATH"]?>/orfeo.css">
    </head>
    <body>
        <table width="100%" border="0" cellspacing="5" cellpadding="0" align="center" class="borde_tab">

            <tr align="center"  class="titulos4"> 
                <td height="12">
                    <b> Listado de plantillas</b>
                </td>
            </tr>
            <tr align="left" class="etextomenu"> 
                <td class='listado_big_font'>
		<a href='<?=$plantilla_memo?>'>Plantilla Comunicación Oficial Interna </a>
                </td>

            </tr>
            <tr align="left" class="etextomenu"> 
                <td class='listado_big_font'>
		<a href='<?=$plantilla_emp?>'>Plantilla Comunicación Oficial Externa Empresas</a> 
                </td>
            </tr>
            <tr align="left" class="etextomenu"> 
                <td class='listado_big_font'>
                <a href='<?=$plantilla_ciu?>'>Plantilla Comunicación Oficial Externa Ciudadanos</a> 
                </td>
            </tr>

            <tr align="left" class="etextomenu"> 
                <td class='listado2'>
		<a href='<?=$plantilla_masiva?>'>Plantilla Comunicaciones Masivas</a>
                </td>
            </tr>
            
            <tr align="left" class="etextomenu"> 
                <td class='listado2'>
		<a title='Plantilla Masiva con logos de CCL: Cámara Colombiana del Libro' href='<?=$plantilla101?>'>Plantilla Comunicaciones Masivas (CCL: Cámara Colombiana del Libro) </a>
                </td>
            </tr>
            <tr align="left" class="etextomenu"> 
                <td class='listado2'>
		<a title='Hoja de cálculo para cada uno de los destinatarios' href='<?=$plantilla103?>'>Formato Listado Destinatarios Masiva </a>
                </td>
            </tr> 
            <tr align="left" class='titulos2'> 
                <td height="12">
                    <b> OTROS FORMATOS - ESPECIALES</b>
                </td>
            </tr>
            <tr align="left" class="etextomenu"> 
                <td class='listado2'>
		<a href='<?=$plantilla102?>'>Formato Ingreso Publicación Almacén </a>
                </td>
            </tr>   
			<!--tr align="left" class="etextomenu"> 
                <td class='listado2'>
		<a href='<?=$plantilla4?>'>Solicitud de Desembolso</a> 
                </td>
            </tr>
			<tr align="left" class="etextomenu"> 
                <td class='listado2'>
		<a href='<?=$plantilla5?>'>Solicitud de Facturación</a> 
                </td>
            </tr>

			<tr align="left" class="etextomenu"> 
                <td class='listado2'>
		<a href='<?=$plantilla6?>'>Supervisión persona Natural</a> 
                </td>
            </tr>
			
			<tr align="left" class="etextomenu"> 
                <td class='listado2'>
		<a href='<?=$plantilla8?>'>Supervisión Persona Juridica</a> 
                </td>
            </tr>
			<tr align="left" class="etextomenu"> 
                <td class='listado2'>
		<a href='<?=$plantilla7?>'>Solicitud de CDP</a> 
                </td>
            </tr-->
            <tr align="center"  class="titulos4"> 
                <td height="25">
                   CAMPOS DE MASIVA COMBINACI&Oacute;N Y PLANTILLAS 
                </td>
            </tr>
            <tr align="left"> 
                <td>
                    <?=$msg?>
                </td>
            </tr>
            <tr align="left" class='titulos2'> 
                <td height="12">
                    <b> Campos de combinaci&oacute;n Masiva</b>
                </td>
            </tr>
            <tr align="center" class="etextomenu"> 
                <td class='listado2'>
                    <?=$nombSe?>
                </td>
            </tr>
            <tr class='titulos2'> 
                <td align="center" height="12">
                    <b> Campos que se pueden usar en una combinaci&oacute;n sencilla</b>
                </td>
            </tr>
            <tr align="justify" class="etextomenu"> 
                <td  class='listado2'>
                    <p><?=$nombMa?></p>
                </td>
            </tr>
            <tr align="center"  class="titulos4"> 
                <td height="25">
                   FUENTES DE CÓDIGO DE BARRAS 
                </td>
            </tr>
            <tr align="justify" class="etextomenu">
                <td  class='listado2'>
                    <p>Listado de Fuentes de Codigo de barras que debe tener instalado en el PC.<br>
			1. Descargar la fuente con click derecho "Guardar enlace como ...".  <br>
                        2. Instalar: <br>
			Windows: Opcion 1) Clic derecho Instalar. Opcion 2) Tambien puede copiar estos archivos en la carpeta c:\windows\fonts</br>
			Linux: Copiar archivo de fuente en: /usr/share/fonts  o en:  /home/nombreusuario/.fonts <br> </p>
		<a href='include/fuentes/code3of9_regular.ttf'>Code3of9_regular.ttf</a>;  
		<a href='include/fuentes/FRE3OF9X.TTF'>FREE3OF9X.TTF</a> -
		<a href='include/fuentes/FREE3OF9.TTF'   class="e"> FREE3OF9.TTF</a> - 
		<a href='include/fuentes/free3of9.ttf'>free3of9.ttf</a> - 
                <a href='include/fuentes/FREE3OF9.TXT'>Licencia </a>
                </td>
            </tr>

		<?php
		if ($_SESSION['usua_admin_sistema']=='1') { 
		?>
            <!--tr align="left" class="etextomenu"> 
                <td class='listado2'>
						<a href='<?php echo $fuente1 ?>'>Digitalizador OrfeoScan Uploader </a>
                </td>

	    </tr-->

        <tr align="center"  class="titulos4"> 
          <td height="12">
          <b> Manuales Editables (Administradores) </b>
          </td>
        </tr>
  
        <tr align="left" class="etextomenu"> 
         <td class='listado_big_font'>
		<a href='<?="$ruta_raiz/bodega/manuales/manuales_editables/Manual radicacion Interna.odt"?>'>Manual radicacion Interna.odt </a>
         </td>
		</tr>

	   <tr align="left" class="etextomenu"> 
         <td class='listado_big_font'>
		<a href='<?="$ruta_raiz/bodega/manuales/manuales_editables/Manual radicación salida y respuesta a uno de entrada.odt"?>'>Manual radicación salida y respuesta a uno de entrada.odt </a>
         </td>
		</tr>

	   <tr align="left" class="etextomenu"> 
         <td class='listado_big_font'>
		<a href='<?="$ruta_raiz/bodega/manuales/manuales_editables/Creación expediente.odt"?>'>Creación expediente.odt </a>
         </td>
		</tr>

	   <tr align="left" class="etextomenu"> 
         <td class='listado_big_font'>
		<a href='<?="$ruta_raiz/bodega/manuales/manuales_editables/Incluir documentos en expediente.odt"?>'>Incluir documentos en expediente.odt </a>
         </td>
		</tr>

	   <tr align="left" class="etextomenu"> 
         <td class='listado_big_font'>
		<a href='<?="$ruta_raiz/bodega/manuales/manuales_editables/Modificación de un documento.odt"?>'>Modificación de un documento.odt </a>
         </td>
		</tr>

		<?php
		}
		?>
		
        </table>
    </body>
</html>

